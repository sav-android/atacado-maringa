package br.net.sav.ComboController;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;


import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.comboControllerSav.factorySav.*;
import br.net.sav.dao.ComboDAO;
import br.net.sav.dao.ComboXClienteDAO;
import br.net.sav.dao.RegraComboDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.RegraCombo;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.RegraExcecao;
import br.net.sav.modelo.RegraGanhe;
import br.net.sav.modelo.RegraLimite;

public class ComboControle extends br.net.sav.comboControllerSav.ComboControle {
    @Override
    public List<RegraAplicavel> getComboValidos(Context context, Object cliente, RegraComboFactory regraComboFactory) {
        List<RegraAplicavel> combos = new ArrayList<RegraAplicavel>();

        List<Combo> combosValidos = new ComboDAO(context).buscarCombosValidos(null);

        for(Combo combo: combosValidos) {
            List<RegraCombo> regraCombos = new RegraComboDAO(context).buscarRegrasPor(combo.getIdCombo(), null);
            setarRegraCompra(combo, regraCombos, context, regraComboFactory);
            setarRegraExcecao(combo, regraCombos, context, regraComboFactory);
            setarRegraGanhe(combo, regraCombos, context, regraComboFactory);
            setarRegraLimite(combo, regraCombos, context, regraComboFactory);
            combos.add(combo);
        }

        return combos;
    }

    @Override
    public boolean validarClienteCombo(Context ctx, Object combo, Object cliente) {
        Cliente cli =(Cliente) cliente;
        Combo combo1 =(Combo) combo;
        boolean retorno = false;

        if (new ComboXClienteDAO(ctx).buscarComboXclientePorId(cli.getIdCliente(), combo1.getIdCombo())== null){
            retorno= true;
        }

        if (new ComboXClienteDAO(ctx).buscarPorIdCombo(combo1.getIdCombo())== null){
            retorno= false;
        }

        return retorno;
    }

    protected void setarRegraLimite(Object combo, List<?> regraCombos, Context context, RegraComboFactory comboFactory) {
        Combo combo1 = (Combo) combo;
        List<?> regras = comboFactory.gerarRegraLimite(context, regraCombos);
        List<RegraLimite> regraLimites = (List<RegraLimite>) regras;
        for(RegraLimite regraLimite: regraLimites)
            combo1.addRegraLimite(regraLimite);
    }

    protected void setarRegraGanhe(Object combo, List<?> regraCombos, Context context, RegraComboFactory comboFactory) {
        Combo combo1 = (Combo) combo;
        List<?> regras = comboFactory.gerarRegraGanhe(context, regraCombos);
        List<RegraGanhe> regraGanhes = (List<RegraGanhe>) regras;
        for(RegraGanhe regraGanhe: regraGanhes)
            combo1.addRegraGanhe(regraGanhe);
    }

    protected void setarRegraCompra(Object combo, List<?> regraCombos, Context context, RegraComboFactory comboFactory) {
        Combo combo1 = (Combo) combo;
        List<?> regras = comboFactory.gerarRegraCompra(context, regraCombos);
        List<RegraCompra> regraCompras = (List<RegraCompra>) regras;
        for(RegraCompra regraCompra: regraCompras)
            combo1.addRegraCompra(regraCompra);
    }
    protected void setarRegraExcecao(Object combo, List<?> regraCombos, Context context, RegraComboFactory comboFactory) {
        Combo combo1 = (Combo) combo;
        List<?> regras = comboFactory.gerarRegraExcecao(context, regraCombos);
        List<RegraExcecao> regraExecoes = (List<RegraExcecao>) regras;

        for(RegraExcecao regraExcecao: regraExecoes)
            combo1.addRegraExcecao(regraExcecao);
    }


}
