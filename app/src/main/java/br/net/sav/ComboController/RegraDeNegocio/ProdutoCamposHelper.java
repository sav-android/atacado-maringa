package br.net.sav.ComboController.RegraDeNegocio;

import java.lang.reflect.Method;

import br.net.sav.modelo.Produto;

public class ProdutoCamposHelper extends br.net.sav.comboControllerSav.regraDeNegocioSav.ProdutoCamposHelper {
    @Override
    public long buscarIdCampo(String campo, Object produto) {
        Produto prod =(Produto) produto;

        Long idComparacao =ZERO;
        if(campo.equalsIgnoreCase(ID_PRODUTO)) {
            idComparacao = prod.getIdProduto();
        }else if(campo.equalsIgnoreCase(ID_GRUPO)){
            idComparacao = Long.valueOf(prod.getIdGrupo());
        }else if(campo.equalsIgnoreCase(ID_FORNECEDOR)){
            idComparacao = Long.valueOf(prod.getIdFornecedor());
        }else if(campo.equalsIgnoreCase(ID_SUBGRUPO)){
            idComparacao = Long.valueOf(prod.getIdSubGrupo());
        }else if(campo.equalsIgnoreCase(ID_CATEGORIA)){
            idComparacao = ZERO; //Long.valueOf(prod.getIdCategoria());
        }else {
            //Caso n?o encontre nenhum campo, percorrer todos os campo do produto para ver se encontra algum campo que d? certo com o campo procurado
            for(Method method: Produto.class.getDeclaredMethods()){
                if(method.getName().contains(campo)
                        && method.getName().contains("get")){
                    try {
                        idComparacao = Long.valueOf(String.valueOf(method.invoke(produto)));
                    }catch (Exception e){
                        idComparacao = Long.valueOf(0);
                    }
                }
            }
        }


        return idComparacao;
    }

    @Override
    public Short buscarIdCampoPorTabela(String tabela, Object produto) {
        Produto prod =(Produto) produto;

        if (tabela.equalsIgnoreCase(GRUPO))
            return prod.getIdGrupo();

        if (tabela.equalsIgnoreCase(SUB_GRUPO))
            return prod.getIdSubGrupo();

        if (tabela.equalsIgnoreCase(CATEGORIA))
            return (short)0;// prod.getIdCategoria();

        if (tabela.equalsIgnoreCase(PRODUTO))
            return (short) prod.getIdProduto();

        for(Method method: Produto.class.getDeclaredMethods()){
            if(method.getName().contains(tabela) &&
                    method.getName().contains("get")){
                try {
                    return (Short) method.invoke(produto);
                }catch (Exception e){
                    return 0;
                }
            }
        }

        return 0;
    }
}
