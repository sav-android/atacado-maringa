package br.net.sav.ComboController.RegraDeNegocio;

import br.net.sav.ComboController.RegraDeNegocio.regraGanheBonificacao.RegraItemBonificacao;
import br.net.sav.ComboController.RegraDeNegocio.regraGanheDesconto.RegraDescontoNoItemProdutoPercentual;
import br.net.sav.ComboController.RegraDeNegocio.regraGanheDesconto.RegraDescontoNoItemValUni;
import br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.regraGanheCombos.RegraBonificaoCombo;
import br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.regraGanheDescontoItem.RegraDescontoCombo;

public class RegraGanheFactory extends br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.RegraGanheFactory {
    @Override
    public RegraDescontoCombo instanciarRegraDescontoNoItemProdutoPercentual() {
        return new RegraDescontoNoItemProdutoPercentual();
    }

    @Override
    public RegraDescontoCombo instanciarRegraDescontoNoItemValUni() {
        return new RegraDescontoNoItemValUni();
    }

    @Override
    public RegraBonificaoCombo instanciarRegraItemBonificacao() {
        return new RegraItemBonificacao();
    }
}
