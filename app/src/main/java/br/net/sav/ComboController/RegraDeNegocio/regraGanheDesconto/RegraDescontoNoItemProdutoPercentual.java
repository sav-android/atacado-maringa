package br.net.sav.ComboController.RegraDeNegocio.regraGanheDesconto;

import br.net.sav.Utils;
import br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.regraGanheDescontoItem.RegraDescontoCombo;
import br.net.sav.modelo.ItemDigitacao;

import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.RegraGanhe;

import static br.net.sav.comboControllerSav.ModeloDTO.RegraGanhe.ZERO;

public class RegraDescontoNoItemProdutoPercentual implements RegraDescontoCombo{
    @Override
    public void darDesconto(int quantidadeItens, Object item, Object regraGanhe) {
        float valorModificado = ZERO;
        ItemDigitacao item1 = (ItemDigitacao) item;
        RegraGanhe ganhe = (RegraGanhe) regraGanhe;
        long idProduto = item1.getProduto().getIdProduto();


        if(ganhe.listIdCampo.contains(idProduto)) {
            valorModificado = (float) Utils.arredondar((ganhe.getValor()/quantidadeItens), 2);
            if (item1.getDesconto() < 0){
                item1.setDesconto(ZERO);
            }

           if (item1.getItemComboAtivado()>0 && getQuantdadeItem(item1) >= item1.getQuantidadeItemLimiteCombo()){
                item1.setDesconto( item1.getDesconto()+ valorModificado);
                item1.setPercentualDescontoCombo( item1.getPercentualDescontoCombo() + valorModificado );
                item1.setValorUnit(Utils.arredondar(item1.getValorUnitOriginal() * (1 - (item1.getDesconto()  / 100)),2));
               item1.setItemComboAtivado(item1.getItemComboAtivado() + 1);
            }else {
                item1.setValorUnit(Utils.arredondar(item1.getValorUnitOriginal() * (1 - ((valorModificado ) / 100)),2));
                item1.setDesconto(valorModificado );
                item1.setPercentualDescontoCombo(valorModificado );
               item1.setItemComboAtivado(1);
           }
            item1.setValorBruto(item1.getValorUnit());
            item1.setValorLiquido(item1.getValorUnit());
        }
    }

    private double getQuantdadeItem(ItemDigitacao item) {
        double qtdCaixa= item.getProduto().getQuantidadeEmbalagem() == 0? 1:
                item.getProduto().getQuantidadeEmbalagem();
        return (item.getQuantidade() * qtdCaixa);
    }
}
