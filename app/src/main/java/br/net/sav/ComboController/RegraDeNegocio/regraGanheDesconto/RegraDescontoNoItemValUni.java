package br.net.sav.ComboController.RegraDeNegocio.regraGanheDesconto;

import br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.regraGanheDescontoItem.RegraDescontoCombo;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.RegraGanhe;

public class RegraDescontoNoItemValUni implements RegraDescontoCombo {
    @Override
    public void darDesconto(int quantidadeItens, Object item, Object regraGanhe) {
        ItemDigitacao item1 = (ItemDigitacao) item;
        RegraGanhe ganhe = (RegraGanhe) regraGanhe;
        String idProduto = String.valueOf(item1.getProduto().getIdProduto());

        if(ganhe.getIdCampo().contains(idProduto)) {
            item1.setValorUnit(ganhe.getValor());
            item1.setValorBruto(item1.getValorUnit());
            item1.setValorLiquido(item1.getValorUnit());
        }
    }
}
