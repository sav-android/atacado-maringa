package br.net.sav.ComboController.comboUtils;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.ItemTabActivity;
import br.net.sav.atacadomaringa.PedidoTabActivity;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ComboXPedidoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.TipoPedido;

import static br.net.sav.modelo.Combo.comboParaCheckboxVisivel;

public class DescontoComboUtils implements Serializable {
    public static int REMOVER = 0, ADICIONAR = 1 ;
    private final Context ctx;
    private final Combo combo;
    private final Pedido pedido;
    private final List<ItemDigitacao> itensPedido;
    private final Parametro parametro;


    public DescontoComboUtils(Context ctx, Combo combo, Pedido pedido, List<ItemDigitacao> itensPedido){

        this.ctx = ctx;
        this.combo = combo;
        this.pedido = pedido;
        this.itensPedido = itensPedido;
        this.parametro = new ParametroDAO(ctx,null).get();
    }

    public void DescontoDoComboNosItens(ItemDigitacao itemVendido, TipoPedido tipoPedido, int adicionaOuRemove) {
        List<?> itensDoDesconto = null;

        if (adicionaOuRemove == ADICIONAR) {
            desativarCombosComProdutosIdenticos(itemVendido);
            itensDoDesconto = combo.getItensComDesconto(ctx, this.itensPedido);
        }
        if (adicionaOuRemove == REMOVER){
            itensDoDesconto = combo.getItensSemDesconto(ctx, this.itensPedido);
        }
        List<ItemDigitacao> itensComboDoDesconto= (List<ItemDigitacao>) itensDoDesconto ;
        List<ItemDigitacao> itensParaRemover = new ArrayList<ItemDigitacao>();

        for(ItemDigitacao itemDigitacao: this.itensPedido){
            for(ItemDigitacao itemDigitacaoComDesconto: itensComboDoDesconto){
                if(itemDigitacao.getProduto().getIdProduto() == itemDigitacaoComDesconto.getProduto().getIdProduto()){
                    pedido.retirarProdutoPedido(itemDigitacao,itensPedido);
                    pedido.inserirProdutoPedido(itemDigitacaoComDesconto, parametro,tipoPedido, itensPedido);
                    itensParaRemover.add(itemDigitacao);
                }
            }
        }

        removerItens(itensParaRemover);
        inserirItens(itensComboDoDesconto);
        serializarPedidoEItens();
    }

    private void desativarCombosComProdutosIdenticos(ItemDigitacao itemVendido) {
        boolean ativarBotao = false;
        int posicao = 0;
        Combo comboParaRemover = null;
        RegraCompra compra2 = null;
        if (!pedido.getCombos().isEmpty()) {
            for (Combo combo1 : pedido.getCombos()) {
                RegraCompra compra = RegraCompra.buscaRegraCompra(ctx, combo1.getIdCombo());
                 compra2 = RegraCompra.buscaRegraCompra(ctx, combo.getIdCombo());

                if (!pedido.comboAtivado(combo)) {

                    if (compra.listIdCampo.size() >= compra2.listIdCampo.size()) {
                        for (int i = 0; i < compra.listIdCampo.size(); i++) {
                            if (compra2.listIdCampo.contains(compra.listIdCampo.get(i))) {
                                ativarBotao = true;
                                comboParaRemover = combo1;
                            }
                        }
                    }else if (compra.listIdCampo.size() < compra2.listIdCampo.size()){
                        for (int i = 0; i < compra2.listIdCampo.size(); i++) {
                            if (compra.listIdCampo.contains(compra2.listIdCampo.get(i))) {
                                ativarBotao = true;
                                comboParaRemover = combo1;
                            }
                        }
                    }
                }

            }

            if(comboParaRemover != null){
                int soma = 0;
                double quantidade = 0;
                RegraCompra compra = RegraCompra.buscaRegraCompra(ctx, comboParaRemover.getIdCombo());
                    for (ItemDigitacao itemDigitacao : this.itensPedido) {
                        soma++;
                        if (compra.listIdCampo.contains(itemDigitacao.getProduto().getIdProduto())) {
                            quantidade = quantidade + getQuantdadeItem(itemDigitacao);
                            posicao = soma - 1;
                        }
                    }

                if (quantidade >= (compra.getQuantidadeMinima() + compra2.getQuantidadeMinima())) {
                    ativarBotao = false;
                }
            }

            if (ativarBotao) {
                RegraCompra compra = RegraCompra.buscaRegraCompra(ctx, comboParaRemover.getIdCombo());
                if (compra.listIdCampo.contains(itensPedido.get(posicao).getProduto().getIdProduto())) {
                    ItemDigitacao itemDigitacao = itensPedido.get(posicao);
                    itensPedido.get(posicao).setQuantidadeItemLimiteCombo(0);
                    itensPedido.get(posicao).setItemComboAtivado(0);
                }
                pedido.removeCombo(comboParaRemover);
                removerComboXPedido(comboParaRemover);
            }

        }
    }

    private double getQuantdadeItem(ItemDigitacao item) {
        double qtdCaixa= item.getProduto().getQuantidadeEmbalagem() == 0? 1:
                item.getProduto().getQuantidadeEmbalagem();
        double v = item.getQuantidade() * qtdCaixa;
        return (item.getQuantidade() * qtdCaixa);
    }

    private void removerItens(List<ItemDigitacao> itensParaRemover) {
        for(ItemDigitacao itemDigitacao: itensParaRemover)
            itensPedido.remove(itemDigitacao);
    }

    private void inserirItens(List<ItemDigitacao> itensComboDesconto) {
        for(ItemDigitacao itemDigitacao: itensComboDesconto)
            itensPedido.add(itemDigitacao);
    }

    public void serializarPedidoEItens() {
        Utils.serializarObjeto(ctx, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        Utils.serializarObjeto(ctx, ItemTabActivity.ITENSPEDIDO_SERIALIZAR, itensPedido);
    }

    private void removerComboXPedido(RegraAplicavel combo) {
        if(pedido.isAlterarPedido()) {
            new ComboXPedidoDAO(ctx).deletePorIdPedido(combo.getIdCombo(), pedido.getIDPedido(), pedido.getIDCliente(),pedido.getDataPedido());
        }
    }

    public static void setComboMsg(List<Combo> combos, RegraAplicavel combo, boolean checkVisivel) {
        Combo combo1 = new Combo();
        combo1.setIdCombo(combo.getIdCombo());
        combo1.setComboAtivadoMsg(false);
        combo1.setDescricao(combo.getDescricao());
        combos.add(combo1);
        comboParaCheckboxVisivel = checkVisivel;
    }
}
