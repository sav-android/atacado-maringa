package br.net.sav.ComboController.comboUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import br.net.sav.adapter.DialogComboAdapter;
import br.net.sav.atacadomaringa.R;

public class DialogAlertComboUltils {

    public static final int CONDICAO_COMBO_ATIVADO = 1;
    public static final int CONDICAO_COMBO = 2;
    private Context ctx;

    public DialogAlertComboUltils(Context ctx) {
        this.ctx = ctx;
    }

    public void msgAoSelecionarCondicaoPagamentoComboAtivado(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.atencao)
                .setMessage(R.string.selecionar_condicao_combo_ativado);

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    public void msgAoAtivarComboComCondicaoDiferente(final DialogComboAdapter dialogComboAdapter, int msgCondicao){
        int MSG = 0;

        if (msgCondicao == CONDICAO_COMBO_ATIVADO){
            MSG = R.string.condicao_diferente_combo;
        }else if(msgCondicao == CONDICAO_COMBO){
            MSG = R.string.verifcar_condicao_combo;
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx).setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.atencao)
                .setMessage(MSG);

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialogComboAdapter!= null)
                    dialogComboAdapter.notifyDataSetChanged();

                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }


}
