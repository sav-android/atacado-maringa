package br.net.sav.ComboController.factory;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import br.net.sav.dao.DBHelper;
import br.net.sav.modelo.RegraCombo;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.RegraExcecao;
import br.net.sav.modelo.RegraGanhe;
import br.net.sav.modelo.RegraLimite;

public class RegraComboFactory extends br.net.sav.comboControllerSav.factorySav.RegraComboFactory {


    @Override
    public List<?> gerarRegraCompra(Context context, List<?> regraCombos) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        List<RegraCombo> regraCombo1 = (List<RegraCombo>)regraCombos;
        ArrayList<RegraCompra> regraRetorno = new ArrayList<RegraCompra>();
        for(RegraCombo regraCombo: regraCombo1){
            if(regraCombo.getTipoRegra().toUpperCase().equals(RegraCombo.TIPO_COMPRA)) {
                Cursor cursor = database.rawQuery("select " + regraCombo.getRegra(), null);
                if (cursor.moveToFirst()) {
                    RegraCompra regraCompra = new RegraCompra(cursor);
                    regraRetorno.add(regraCompra);
                }
            }
        }
       // database.close();
        return regraRetorno;
    }

    @Override
    public List<?> gerarRegraGanhe(Context context, List<?> regraCombos) {
        List<RegraCombo> regraCombo1 = (List<RegraCombo>)regraCombos;
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        ArrayList<RegraGanhe> regraRetorno = new ArrayList<RegraGanhe>();
        for(RegraCombo regraCombo: regraCombo1){
            if(regraCombo.getTipoRegra().toUpperCase().contains(RegraCombo.TIPO_GANHE)) {
                Cursor cursor = database.rawQuery("select " + regraCombo.getRegra(), null);
                if (cursor.moveToFirst()) {
                    RegraGanhe regraGanhe = new RegraGanhe(cursor);
                    regraRetorno.add(regraGanhe);
                }
            }
        }
      //  database.close();
        return regraRetorno;
    }

    @Override
    public List<?> gerarRegraLimite(Context context, List<?> regraCombos) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        List<RegraCombo> regraCombo1 = (List<RegraCombo>)regraCombos;
        ArrayList<RegraLimite> regraRetorno = new ArrayList<RegraLimite>();
        for(RegraCombo regraCombo: regraCombo1){
            if(regraCombo.getTipoRegra().toUpperCase().equals(RegraCombo.TIPO_LIMITE)) {
                Cursor cursor = database.rawQuery("select " + regraCombo.getRegra(), null);
                if (cursor.moveToFirst()) {
                    RegraLimite regraLimite = new RegraLimite(cursor);
                    regraRetorno.add(regraLimite);
                }
            }
        }
       // database.close();
        return regraRetorno;
    }

    @Override
    public List<?> gerarRegraExcecao(Context context, List<?> regraCombos) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        List<RegraCombo> regraCombo1 = (List<RegraCombo>)regraCombos;
        ArrayList<RegraExcecao> regraRetorno = new ArrayList<RegraExcecao>();
        for(RegraCombo regraCombo: regraCombo1){
            if(regraCombo.getTipoRegra().toUpperCase().equals(RegraCombo.TIPO_EXCECAO)) {
                Cursor cursor = database.rawQuery("select " + regraCombo.getRegra(), null);
                if (cursor.moveToFirst()) {
                    RegraExcecao regraExcecao = new RegraExcecao(cursor);
                    regraRetorno.add(regraExcecao);
                }
            }
        }
        //database.close();
        return regraRetorno;
    }
}
