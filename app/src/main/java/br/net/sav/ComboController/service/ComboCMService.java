package br.net.sav.ComboController.service;

import android.util.Log;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.ComboDTO;
import br.net.sav.IntegradorWeb.services.ComboAtualizaServise;
import br.net.sav.Utils;
import br.net.sav.dao.ComboDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Combo;
import br.net.sav.utils.ToastUtils;

public class ComboCMService extends ComboAtualizaServise {
    @Override
    protected void quandoReqSucesso(List<ComboDTO> resultado) {
        List<ComboDTO> recebeListaCombo = resultado;

        if (recebeListaCombo.isEmpty()){
            ToastUtils.mostrarMensagem(getBaseContext(), "Não Existe combo para ser Atualizado");
            return;
        }

        new Combo().sincronizaCombo(getBaseContext(), recebeListaCombo);
        Log.i("RetornoCombo Sucesso:", resultado.toString());


    }

    @Override
    protected void quandoReqErro(String mensagemDeErro) {
        Utils.gravarLog("RetornoCombo ERRO: "+mensagemDeErro);
        Log.i("RetornoCombo ERRO: ", mensagemDeErro);
    }

    @Override
    protected String gettoken() {
        return new ParametroDAO(this,null).get()==null?"":new ParametroDAO(this,null).get().getTokenApi();
    }

    @Override
    protected String getDataAtualizacao() {
        String dataUtimaAtualizacaoCombo = new ComboDAO(this).existeDataUtimaAtualizacaoCombo();

        if(new ComboDAO(this).existeDataUtimaAtualizacaoCombo()!= null){
            try {
                Date dataConvertida = Utils.sdfDataHoraDb.parse(dataUtimaAtualizacaoCombo);
                return Utils.sdfDataHoraDb.format(dataConvertida);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return null;
    }
}
