package br.net.sav.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Combo;

import static br.net.sav.modelo.Combo.comboParaCheckboxVisivel;

public class DialogComboAdapter extends BaseAdapter {

    private final Context ctx;
    private final List<Combo> dadosCombo;
    private LayoutInflater inflater;
    public static boolean ativou = false;

    public DialogComboAdapter(Context ctx, List<Combo> dadosCombo){

        this.ctx = ctx;
        this.dadosCombo = dadosCombo;
        inflater = LayoutInflater.from(ctx);
    }
    @Override
    public int getCount() {
        return dadosCombo.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = this.inflater.inflate(R.layout.combo_text_dialog, null);

        final Combo combo = dadosCombo.get(i);

        TextView textView = (TextView)v.findViewById(R.id.txv_codigo);
        TextView textView1 = (TextView)v.findViewById(R.id.txv_descricao);


        textView.setText("Codigo: " +String.valueOf(combo.getIdCombo()));
        textView1.setText(ctx.getString(R.string.descr) +" "+combo.getDescricao());
        if (comboParaCheckboxVisivel){

        }
        final CheckBox checkBox = (CheckBox)v.findViewById(R.id.checkbox_combo);
        if (comboParaCheckboxVisivel){
            checkBox.setVisibility(View.VISIBLE);
        }else {
            checkBox.setVisibility(View.GONE);
        }
        checkBox.setOnCheckedChangeListener(null);
        checkBox.setChecked(combo.isComboAtivadoMsg());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        checkBox.setChecked(isChecked);
                        combo.setComboAtivadoMsg(isChecked);

                for (int i = 0; i < dadosCombo.size(); i++) {
                    if (dadosCombo.get(i).getIdCombo() != combo.getIdCombo()){
                        dadosCombo.get(i).setComboAtivadoMsg(false);
                        checkBox.setChecked(dadosCombo.get(i).isComboAtivadoMsg());
                        notifyDataSetChanged();
                    }
                }

                notifyDataSetChanged();

            }
        });
        return v;
    }


}
