package br.net.sav.atacadomaringa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import br.net.sav.dao.ContatoDAO;
import br.net.sav.modelo.Contato;

public class AniversarianteListaActivity extends ListActivity {
	public static final String ACTION = "Aniversariantes" ;
	private List<Contato> listaAniversariante;
	private AniversarianteListaAdapter adapter ;
	
	private Date DataInicial = new Date();
	private Date DataFinal = new Date();

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
		listaAniversariante = new ContatoDAO(getBaseContext(), null).listaAniversariantes(DataInicial, DataFinal);

		setListAdapter(new AniversarianteListaAdapter(getBaseContext(), listaAniversariante));		
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		
		if(listaAniversariante != null) {
			if(adapter == null) {
				setContentView(R.layout.listaaniversariante);
				adapter = new AniversarianteListaAdapter(getBaseContext(), listaAniversariante);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(listaAniversariante);
			}
		}
		
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if (title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);		
			setTitle(String.format(getString(R.string.resumo_aniversariantes), sdf.format(DataInicial), sdf.format(DataFinal), listaAniversariante.size()));
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.filtro_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idfiltromenu.filtro:
			try {
				showMenuData();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void showMenuData() throws ParseException {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.menudataaniversariante);
		
		final DatePicker dtpDataInicial = (DatePicker) dialog.findViewById(R.idMenuDataAniversariante.dtpInicial);
		final DatePicker dtpDataFinal = (DatePicker) dialog.findViewById(R.idMenuDataAniversariante.dtpFinal);
		
		final Button btnPesquisar = (Button) dialog.findViewById(R.idMenuDataAniversariante.btnPesquisar);
		btnPesquisar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {									
				DataInicial.setDate(dtpDataInicial.getDayOfMonth());
				DataInicial.setMonth(dtpDataInicial.getMonth());
				
				DataFinal.setDate(dtpDataFinal.getDayOfMonth());
				DataFinal.setMonth(dtpDataFinal.getMonth());

				listaAniversariante = new ContatoDAO(getBaseContext(), null).listaAniversariantes(DataInicial, DataFinal);
				
				onResume();				

				setListAdapter(new AniversarianteListaAdapter(getBaseContext(), listaAniversariante));
				
				dialog.dismiss();
			}
		});
		
		dialog.setTitle("Filtrar Aniversariantes");
		dialog.show();
	}
}
