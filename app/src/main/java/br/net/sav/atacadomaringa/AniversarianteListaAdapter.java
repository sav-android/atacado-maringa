package br.net.sav.atacadomaringa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Contato;

public class AniversarianteListaAdapter extends BaseAdapter {
	private List<Contato> lista;
	private Context ctx;
	private ClienteDAO clienteDAO;
	
	public AniversarianteListaAdapter(Context ctx, List<Contato> lista) {
		this.lista = lista;
		this.ctx = ctx;
		this.clienteDAO = new ClienteDAO(ctx, null);
	}

	@Override
	public int getCount() {
		return lista.size();
	}
	
	public void changeData(List<Contato> lista) {
		this.lista = lista ;
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Contato contatoCliente = lista.get(position);
		
		LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = layout.inflate(R.layout.listaaniversariante, null);
		
		Cliente cliente = null;
		try {
            cliente = clienteDAO.get(contatoCliente.getIdCliente());
        } catch (ParseException e) {
            e.printStackTrace();
        }
		
		TextView txvData = (TextView) v.findViewById(R.idlistaaniversariante.txvData);
		txvData.setTextSize(13);
		txvData.setTextColor(Color.YELLOW);
		txvData.setText(ctx.getString(R.string.aniversario) + new SimpleDateFormat("dd/MM/yyyy").format(contatoCliente.getAniversario()));
		
		TextView txvNomeContato = (TextView) v.findViewById(R.idlistaaniversariante.txvNomeContato);
		txvNomeContato.setText("Nome: " + contatoCliente.getNome());
		txvNomeContato.setTextSize(13);
		txvNomeContato.setSingleLine(true);
		txvNomeContato.setTextColor(Color.YELLOW);

		TextView txvNomeCliente = (TextView) v.findViewById(R.idlistaaniversariante.txvNomeCliente);
		txvNomeCliente.setText("Nome Cliente: " + cliente.getRazao());
		txvNomeCliente.setTextSize(13);

		TextView txvTelefone = (TextView) v.findViewById(R.idlistaaniversariante.txvTelefone);
		txvTelefone.setText("Telefone: " + contatoCliente.getDDD() + " " + contatoCliente.getTelefone());
		txvTelefone.setTextSize(13);

		TextView txvCargo = (TextView) v.findViewById(R.idlistaaniversariante.txvCargo);
		txvCargo.setText("Cargo: " + contatoCliente.getCargo());
		txvCargo.setTextSize(13);

		TextView txvHobby = (TextView) v.findViewById(R.idlistaaniversariante.txvHobby);
		txvHobby.setText("Hobby: " + contatoCliente.getHobby());
		txvHobby.setTextSize(13);

		TextView txvTime = (TextView) v.findViewById(R.idlistaaniversariante.txvTime);
		txvTime.setText("Time/Futebol: " + contatoCliente.getTime());
		txvTime.setTextSize(13);

		TextView txvIdade = (TextView) v.findViewById(R.idlistaaniversariante.txvIdade);
		txvIdade.setText("Idade: " + (new Date().getYear() - contatoCliente.getAniversario().getYear()));
		txvIdade.setTextSize(13);

		return v;
	}

}
