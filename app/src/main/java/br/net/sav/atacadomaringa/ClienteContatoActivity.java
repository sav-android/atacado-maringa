package br.net.sav.atacadomaringa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ContatoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Contato;
import br.net.sav.modelo.Parametro;
import br.net.sav.utils.ManipulacaoEmail;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class ClienteContatoActivity extends Activity{
	
	private ListView listContatos;
	private ImageButton imgNovoContato;
	private static ContatoDAO contatoDAO = null;
	private Contato contato = null;
	private List<Contato> listContatosCliente = new ArrayList<Contato>();
	private static Long idCliente;
	
	//Componentes da Dialog
	private EditText edtNome;
	private EditText edtHobby;
	private EditText edtTime;
	private EditText edtDDD;
	private EditText edtTelefone;
	private EditText edtAniversario;
	private EditText edtCargo;
	private EditText edtEmail;
	private Button btnSalvar;
	private Button btnCancelar;
	private boolean visualizarDetalhesContato = false;
	
	private ClienteDAO clienteDAO = null;
	boolean alterarContato = false;
	boolean novoContato = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cliente_contato);
		recuperarLayout();
		Intent intent = getIntent();
		idCliente = intent.getLongExtra("IdCliente", 0);
		
		if ( clienteDAO == null){
			clienteDAO = new ClienteDAO(this, null);
		}
		
		if(statusCliente(idCliente)){ //Cliente enviado
			imgNovoContato.setVisibility(View.GONE);
			listContatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			    @Override
			    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			    	contato = getContato(position);
					visualizarDetalhesContato = true;
					showDialogNovoContato(ClienteContatoActivity.this);
			    }
			});
		}else{
			imgNovoContato.setVisibility(View.VISIBLE);
			listContatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			    @Override
			    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			    	contato = getContato(position);
			    	showDialogMenuOpcoes(ClienteContatoActivity.this);
			    }
			});
		}
		
		atualizarListView();
		imgNovoContato.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				novoContato = true;
				showDialogNovoContato(ClienteContatoActivity.this);
			}
		});
		

	}
	
	private Contato getContato(int position){
		if(listContatosCliente != null){
			Contato contadoAux = new Contato();
			contadoAux.setIdCliente(listContatosCliente.get(position).getIdCliente());
			contadoAux.setNome(listContatosCliente.get(position).getNome());
			contadoAux.setHobby(listContatosCliente.get(position).getHobby());
			contadoAux.setTime(listContatosCliente.get(position).getTime());
			contadoAux.setDDD(listContatosCliente.get(position).getDDD());
			contadoAux.setTelefone(listContatosCliente.get(position).getTelefone());
			contadoAux.setAniversario(listContatosCliente.get(position).getAniversario());
			contadoAux.setCargo(listContatosCliente.get(position).getCargo());
			contadoAux.setEmail(listContatosCliente.get(position).getEmail());
			return contadoAux;
		}else{
			return null;
		}
	}

	private void recuperarLayout() {
		listContatos = (ListView) findViewById(R.idDetalheCliente.listContatos);
		imgNovoContato = (ImageButton) findViewById(R.idDetalheCliente.imgNovoContato);
	}

	private boolean statusCliente(Long idCliente) {
		return clienteDAO.clienteEnviado(idCliente);
	}
	
	public void atualizarListView(){
		try{
			if (idCliente > 0){
				listContatosCliente.clear();
				if(contatoDAO == null){
					contatoDAO = new ContatoDAO(this, null);
				}
				listContatosCliente = contatoDAO.getAllContatoCliente(idCliente);
		        ArrayList<String> listaNomeContatos = new ArrayList<String>();
		        if(listContatosCliente.size() > 0){
		        	listaNomeContatos.clear();
			        for(int i = 0; i < listContatosCliente.size() ; i++){
			        	listaNomeContatos.add(listContatosCliente.get(i).Nome.toString());
			        }
		        }
		        ArrayAdapter<String> adapterLista = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, listaNomeContatos);
		        listContatos.setAdapter(adapterLista);
		        
			}	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void recuperarComponentesDialog (Dialog dialog){
		edtNome = (EditText) dialog.findViewById(R.idDetalheCliente.txtNomeContato);
		edtHobby = (EditText) dialog.findViewById(R.idDetalheCliente.txtHobbyContato);
		edtTime = (EditText) dialog.findViewById(R.idDetalheCliente.txtTimeContato);
		edtDDD = (EditText) dialog.findViewById(R.idDetalheCliente.txtDddContato);
		edtTelefone = (EditText) dialog.findViewById(R.idDetalheCliente.txtTelefoneContato);
		edtAniversario = (EditText) dialog.findViewById(R.idDetalheCliente.txtAniversarioContato);
		edtCargo = (EditText) dialog.findViewById(R.idDetalheCliente.txtCargoContato);
		edtEmail = (EditText) dialog.findViewById(R.idDetalheCliente.txtEmailContato);
		btnSalvar = (Button) dialog.findViewById(R.id.DialogContatoDetalhes_btnSalvar);
		btnCancelar = (Button) dialog.findViewById(R.id.DialogContatoDetalhes_btnCancelar);
		
		if ( visualizarDetalhesContato ){
			preencherEditTextComObjeto();
			escolherCamposVisiveis();
        	camposEnable(false);
		}
	}
	
	private void camposEnable(boolean value){
		edtNome.setEnabled(value);
		edtHobby.setEnabled(value);
		edtTime.setEnabled(value);
		edtDDD.setEnabled(value);
		edtTelefone.setEnabled(value);
		edtAniversario.setEnabled(value);
		edtCargo.setEnabled(value);
		edtEmail.setEnabled(value);
	}
	
	private void escolherCamposVisiveis(){
		btnSalvar.setVisibility(View.GONE);
		btnCancelar.setVisibility(View.GONE);
	}
	
	private boolean verificarCampos() {
		int qntCamposInvalidos = 0;
		if (edtNome.getText().toString().isEmpty()){
			edtNome.setError(getString(R.string.campo_obrigatorio));
			qntCamposInvalidos++;
		}
		if (edtDDD.getText().toString().isEmpty()){
			edtDDD.setError(getString(R.string.campo_obrigatorio));
			qntCamposInvalidos++;
		}
		if (edtTelefone.getText().toString().isEmpty()){
			edtTelefone.setError(getString(R.string.campo_obrigatorio));
			qntCamposInvalidos++;
		}
		if (edtEmail.getText().toString().isEmpty()){
			edtEmail.setError(getString(R.string.campo_obrigatorio));
			qntCamposInvalidos++;
		}
//		if (edtAniversario.getText().toString().isEmpty()){
//			edtAniversario.setError("Campo obrigat�rio");
//			qntCamposInvalidos++;
//		}
		if ( !ManipulacaoEmail.isValidarEmail(this.edtEmail.getText().toString())){
			edtEmail.setError(getString(R.string.campo_obrigatorio_invalido));
			qntCamposInvalidos++;
		}
		if ( qntCamposInvalidos > 0 ){
			return false;
		}else{
			return true;
		}
	}
	
	private Contato preencherObjetoComEditText() {
		try{
			Contato con = new Contato();
			con.setIdCliente(idCliente);
			con.setNome(edtNome.getText().toString());
			con.setHobby(edtHobby.getText().toString());
			con.setTime(edtTime.getText().toString());
			con.setDDD(edtDDD.getText().toString());
			con.setTelefone(edtTelefone.getText().toString());
			try{
				con.setAniversario(Utils.sdfDataHoraDb.parse("2017-12-18 08:21:30"));
			}catch(Exception e){
				Toast.makeText(this, "setAniversario", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
			con.setCargo(edtCargo.getText().toString());

			con.setEmail(edtEmail.getText().toString());
			
			try {
				ParametroDAO parametroDAO = new ParametroDAO(
						ClienteContatoActivity.this, null);
				Parametro parametro = parametroDAO.get();
				if (parametro != null) {
					con.setCodRepresentante(parametro.getIdVendedor());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return con;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	private void preencherEditTextComObjeto(){
		if (contato != null){
			edtNome.setText(contato.getNome());
			edtHobby.setText(contato.getHobby());
			edtTime.setText(contato.getTime());
			edtDDD.setText(contato.getDDD());
			edtTelefone.setText(contato.getTelefone());
//			edtAniversario.setText((ChaSequence) contato.getAniversario());
			edtAniversario.setText(new SimpleDateFormat("dd/MM/yyyy").format(contato.getAniversario()));
			edtCargo.setText(contato.getCargo());
			edtEmail.setText(contato.getEmail());
			
			
		}
	}
	
	public void showDialogNovoContato(final Activity pContext) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.novo_contato_cliente);
            recuperarComponentesDialog(dialog);
            
            if(novoContato){
            	dialog.setCancelable(false);
            }else if(visualizarDetalhesContato){
            	dialog.setCancelable(true);
            	visualizarDetalhesContato = false;
            }
        	
            
            dialog.setTitle("Novo contato");
            btnSalvar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
                	if (verificarCampos()){
                		if(preencherObjetoComEditText() != null){
                			Contato con = preencherObjetoComEditText();
                			if (con != null){
	                			try{
	                				if (contatoDAO.insert(con, null)){
	                					contatoDAO.atualizarIdRepresentante(Long.toString(con.getIdCliente()), con.getNome(), 
	                							Long.toString(con.getCodRepresentante())); 
		                				contato = null;
		                				Toast.makeText(pContext, "Sucesso ao inserir contato!", Toast.LENGTH_LONG).show();
		                				atualizarListView();
		                				dialog.dismiss();
	                				}else{
	                					Toast.makeText(pContext, "pcontatoDAO.insert(contato, null) = false", Toast.LENGTH_LONG).show();
	                				}
	                			}catch(Exception e){
	                				e.printStackTrace();
	                				Toast.makeText(pContext, "Erro ao inserir contato!", Toast.LENGTH_LONG).show();
	                				Log.d("ContatoInsert","Erro: "+e.getMessage());
	                			}
                			}else{
                				Toast.makeText(pContext, "con = null", Toast.LENGTH_LONG).show();
                			}
                		}else{
                			Toast.makeText(pContext, "preencherObjetoComEditText = null", Toast.LENGTH_LONG).show();
                		}
                	}
				}
			});
			
			btnCancelar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					showDialogCancelarClienteNovo();
				}
			});
					

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.show();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
	
	private void showDialogAlterar(final Activity pContext) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.novo_contato_cliente);
            recuperarComponentesDialog(dialog);
            preencherEditTextComObjeto();
            dialog.setCancelable(false);
            edtNome.setEnabled(false);
            dialog.setTitle("Alterar contato");
			btnSalvar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
                	if (verificarCampos())
                	{
                		if(preencherObjetoComEditText() != null){
                			Contato contatoAux = preencherObjetoComEditText();
                			try{
                				if (contatoDAO.update(contatoAux)){
	                				Toast.makeText(pContext, "Sucesso ao atualizar contato!", Toast.LENGTH_LONG).show();
	                				atualizarListView();
	                				dialog.dismiss();
                				}else{
                					Toast.makeText(pContext, "contatoDAO.update(contato, null) = false", Toast.LENGTH_LONG).show();
                				}
                			}catch(Exception e){
                				e.printStackTrace();
                				Toast.makeText(pContext, "Erro ao atualizar contato!", Toast.LENGTH_LONG).show();
                				Log.d("ContatoInsert","Erro: "+e.getMessage());
                			}
                		}else{
            				Toast.makeText(pContext, "con = null", Toast.LENGTH_LONG).show();
            			}
                	}else{
            			Toast.makeText(pContext, "preencherObjetoComEditText = null", Toast.LENGTH_LONG).show();
            		}
				}
			});
			
			btnCancelar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					showDialogCancelarClienteNovo();
				}
			});
					

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.show();	
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
	}
	
	@Override
	public void onBackPressed() {
		if (alterarContato){
			showDialogCancelarClienteNovo();
		}else{
			super.onBackPressed();
		}
		
	}
	
	private void showDialogCancelarClienteNovo() {
		new AlertDialog.Builder(this)
	        .setTitle("Deseja realmente sair?")
	        .setMessage(R.string.as_informacoes_serao_perdidas)
	        .setNegativeButton(R.string.nao, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
					
				}
			})
	        .setPositiveButton("Sair", new OnClickListener() {
	            public void onClick(DialogInterface arg0, int arg1) {
	                ClienteContatoActivity.this.finish();
	            }
        }).create().show();
	}
	
	public void showDialogMenuOpcoes(final Activity pContext) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_menu_contato);
		Button btnDetalhes = (Button) dialog.findViewById(R.id.DialogMenuContato_detalhes);
		Button btnAlterar = (Button) dialog.findViewById(R.id.DialogMenuContato_alterar);
		Button btnExcluir = (Button) dialog.findViewById(R.id.DialogMenuContato_excluir);
		dialog.setTitle(R.string.opcoes);
		
		btnDetalhes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				visualizarDetalhesContato = true;
				showDialogNovoContato(ClienteContatoActivity.this);
				dialog.dismiss();
			}
		});
		
		btnAlterar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				alterarContato = true;
				showDialogAlterar(ClienteContatoActivity.this);
				dialog.dismiss();
			}
		});
		
		btnExcluir.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showDialogExcluirCliente();
				dialog.dismiss();
			}
		});
		
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }
	
	private void showDialogExcluirCliente() {
		new AlertDialog.Builder(this)
	        .setTitle("Deseja realmente excluir este contato?")
	        .setMessage(getString(R.string.as_informacoes_serao_perdidas))
	        .setNegativeButton(R.string.nao, new OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
					
				}
			})
	        .setPositiveButton("Excluir", new OnClickListener() {
	            public void onClick(DialogInterface arg0, int arg1) {
	                excluirContato();
	            }
        }).create().show();
	}
	
	private void excluirContato() {
		if(contatoDAO.delete(contato)){
			atualizarListView();
			Toast.makeText(this, "Contato excluido com sucesso!", Toast.LENGTH_SHORT).show();
		}
		
	}
	
}
