package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SectionIndexer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.net.sav.UIHelper;
import br.net.sav.Utils;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.LimiteDAO;
import br.net.sav.dao.ObjetivoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.PendenciaDAO;
import br.net.sav.dialog.DialogCustom;
import br.net.sav.dialog.GerarBonificacaoDialog;
import br.net.sav.dialog.NaoVendaDialog;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Limite;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Pendencia;

import static android.graphics.Color.rgb;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR_ALTERAR;
import static br.net.sav.dao.ClienteDAO.VALOR_ID_CLIENTE_NOVO;

public class ClienteListaActivity extends ListActivity {
	private static final int REQUEST_OBJETIVOS = 6;
	private static final int REQUEST_PEDIDO = 10;

	private List<Cliente> listaClientes;
	private ClienteDAO clienteDAO;

	private Adaptador adaptador;
	private String[] arrayCidades;

	private int savedIndex, savedY;
	public static final int TROCA_TAB = 20 ;
	public static final short PENDENCIA = 3;
	public static final short ULTIMAS_COMP = 2;
	private Parametro parametro ;
	private Cliente mClienteAux;
	public static boolean isSalvo = false;
	public static boolean isSair = false;

	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		try {
			parametro = new ParametroDAO(getBaseContext(), null).get();
			clienteDAO = new ClienteDAO(getBaseContext(), null);
			arrayCidades = clienteDAO.arrayCidades();
		} catch (Exception e) {
			new UIHelper(ClienteListaActivity.this).showErrorDialog(e.getMessage());
		}
	}

	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putSerializable("listaClientes", (Serializable) listaClientes);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		listaClientes =(ArrayList<Cliente>) savedInstanceState.getSerializable("listaClientes");
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			if (!ControleAcesso.validarConexaoDiaria(ClienteListaActivity.this)) {
				int qtdeDiasPassou = (int) ControleAcesso.quantidadeDiasPassouUltimaTransmissao(ClienteListaActivity.this);
				
				String mensagem = String.format(getString(R.string.faz_n_dias_que_voce_nao_faz_transmissao) +
						getString(R.string.faca_uma_transmissao_para_utilizar_o_sistema), qtdeDiasPassou, Utils.sdfDataPtBr.format(ControleAcesso.getDataUltimaRecepcao(ClienteListaActivity.this)), Utils.sdfDataHoraPtBr.format(new Date()));

				DialogInterface.OnClickListener listener = new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				};
				
				UIHelper.showDialogConfirmacao(this, listener, mensagem, getString(R.string.app_name), false);
			}

			if(!parametro.isAtivo()){
				DialogInterface.OnClickListener listener = new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				};
				UIHelper.showDialogConfirmacao(this, listener, getString(R.string.vendedor_desativado_favor_fazer_uma_trnasicao), getString(R.string.app_name), false);
			}

			SharedPreferences prefs = getSharedPreferences(Cliente.CONSULTA_CLIENTE_PREFS, MODE_PRIVATE);
			int opcaoPesquisa = prefs.getInt("opcaoPesquisa", -1);
			String pesquisa = prefs.getString("pesquisa", "");
			String cidade = prefs.getString("cidade", "");
			OpcaoPesquisa opcaoEscolhida = getOpcao(opcaoPesquisa);
			
			if (!pesquisaCliente(opcaoEscolhida, pesquisa, cidade)) {
				listaClientes = new ArrayList<Cliente>();
				atualizarLista();
			}

		} catch (Exception e) {
			new UIHelper(ClienteListaActivity.this).showErrorDialog(e.getMessage());
		}

		if (isSalvo || isSair) {
			Utils.apagarObjetoSerializado(getBaseContext());
			isSalvo = false;
			isSair = false;
		}

		if (Utils.existeObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR)) {
            recuperacaoPedido();
        }else{
		    resetPreferencesSpinner();
        }
	}

	public void atualizarLista() {
		if (listaClientes != null) {
			if (adaptador == null) {
				setContentView(R.layout.cliente_lista);
				adaptador = new Adaptador(getBaseContext());
				setListAdapter(adaptador);
				getListView().setFastScrollEnabled(true);
			} else {
				adaptador.changeData(listaClientes);
				getListView().setSelectionFromTop(savedIndex, savedY);
				getListView().setFastScrollEnabled(true);
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		savedIndex = getListView().getFirstVisiblePosition();
		View v1 = getListView().getChildAt(0);
		savedY = (v1 == null) ? 0 : v1.getTop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_consultacliente, menu);
		MenuItem item0 = menu.getItem(0);
		MenuItem item1 = menu.getItem(1);
		MenuItem item2 = menu.getItem(2);
		setaCorMenuItem("Adicionar Cliente", item0);
		setaCorMenuItem("Filtrar Clientes", item1);
		setaCorMenuItem("Remover Filtros", item2);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.idMenuConsultaCliente.novo_cliente:{
			startActivity(new Intent(ClienteListaActivity.this, ClienteTabDetalhesActivity.class));
			break;
		}
		
		case R.idMenuConsultaCliente.filtrar_clientes:{
			showDialogPesquisaCliente();
			break;
		}

		case R.idMenuConsultaCliente.remover_filtros:{
			SharedPreferences prefs = getSharedPreferences(Cliente.CONSULTA_CLIENTE_PREFS, MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.clear();
			editor.commit();
			listaClientes = clienteDAO.filtroCliente("", "", null, true);
			atualizarLista();
			break;
		}

		}
		return super.onOptionsItemSelected(item);
	}
	


	private void showDialogPesquisaCliente() {
		savedIndex = getListView().getFirstVisiblePosition();
		View v1 = getListView().getChildAt(0);
		savedY = (v1 == null) ? 0 : v1.getTop();
		
		final SharedPreferences prefs = getSharedPreferences(Cliente.CONSULTA_CLIENTE_PREFS, MODE_PRIVATE);
		int opcaoPesquisa = prefs.getInt("opcaoPesquisa", -1);
		String pesquisa = prefs.getString("pesquisa", "");
		String cidade = prefs.getString("cidade", "");

		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_filtrocliente);
		dialog.setTitle("Pesquisa de Clientes");

		final RadioGroup rgOpcoes = (RadioGroup) dialog.findViewById(R.idFiltroCliente.rgOpcoes);
		final EditText edtPesquisa = (EditText) dialog.findViewById(R.idFiltroCliente.edtPesquisa);
		final Spinner spnCidade = (Spinner) dialog.findViewById(R.idFiltroCliente.spnCidade);
		final Button btnOk = (Button) dialog.findViewById(R.idFiltroCliente.btnOK);
		edtPesquisa.setSelectAllOnFocus(true);
		
		final RadioButton rCodigo=(RadioButton) dialog.findViewById(R.idFiltroCliente.rdCodigo);
	
		rgOpcoes.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(rCodigo.isChecked())
					edtPesquisa.setInputType(InputType.TYPE_CLASS_NUMBER);
				else
					edtPesquisa.setInputType(InputType.TYPE_CLASS_TEXT);
			}		
		});
		if (opcaoPesquisa != -1)
			rgOpcoes.check(opcaoPesquisa);

		edtPesquisa.setText(pesquisa);

		spnCidade.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, arrayCidades));
		if (!cidade.equals("")) {
			for (int i = 0; i < arrayCidades.length; i++) {
				if (arrayCidades[i].equals(cidade)) {
					spnCidade.setSelection(i);
					break;
				}
			}
		}

		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					String pesquisa = edtPesquisa.getText().toString().trim().toUpperCase();
					String cidade = (spnCidade.getSelectedItemPosition() == 0) ? "" : arrayCidades[spnCidade.getSelectedItemPosition()];

					OpcaoPesquisa opcaoEscolhida = getOpcao(rgOpcoes.getCheckedRadioButtonId());

					if (opcaoEscolhida == OpcaoPesquisa.CODIGO && !pesquisa.equals("") && !pesquisa.matches("^[0-9]*$")) {
						Toast.makeText(getBaseContext(), R.string.para_pesquisa_por_codigo_informe_apenas_numeros, Toast.LENGTH_LONG).show();
						return;
					}

					if (pesquisaCliente(opcaoEscolhida, pesquisa, cidade)) {
						// Gravar filtros no SharedPreferences
						SharedPreferences.Editor editor = prefs.edit();
						editor.putInt("opcaoPesquisa", rgOpcoes.getCheckedRadioButtonId());
						editor.putString("pesquisa", edtPesquisa.getText().toString());
						if (spnCidade.getSelectedItemPosition() > 0)
							editor.putString("cidade", arrayCidades[spnCidade.getSelectedItemPosition()]);
						else
							editor.putString("cidade", "");

						editor.commit();
					} else {
						Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_cliente_com_os_filtros_informados, Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					Toast.makeText(getBaseContext(), "Erro durante a pesquisa de clientes.\n" + e.getMessage(), Toast.LENGTH_LONG).show();
				}

				dialog.dismiss();
			}
		});
		dialog.show();
	}

	public OpcaoPesquisa getOpcao(int idRadio) {
		OpcaoPesquisa opcaoEscolhida = null;

		switch (idRadio) {
		case R.idFiltroCliente.rdCodigo:
			opcaoEscolhida = OpcaoPesquisa.CODIGO;
			break;
		case R.idFiltroCliente.rdFantasia:
			opcaoEscolhida = OpcaoPesquisa.FANTASIA;
			break;
		case R.idFiltroCliente.rdRazao:
			opcaoEscolhida = OpcaoPesquisa.RAZAO;
			break;
		case R.idFiltroCliente.rdEndereco:
			opcaoEscolhida = OpcaoPesquisa.ENDERECO;
			break;
		}

		return opcaoEscolhida;
	}

	public enum OpcaoPesquisa {
		CODIGO, FANTASIA, RAZAO, ENDERECO
	}

	private boolean pesquisaCliente(OpcaoPesquisa opcaoEscolhida, String pesquisa, String cidade) {
		List<Cliente> listaClientePesquisa = clienteDAO.filtroCliente(pesquisa, cidade, opcaoEscolhida, false);
		if (listaClientePesquisa.size() > 0) {
			this.listaClientes = new ArrayList<Cliente>(listaClientePesquisa);

			atualizarLista();
			return true;
		}

		return false;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Cliente cliente = listaClientes.get(position);
		mClienteAux = cliente;

		if (this.getCallingActivity() != null && this.getCallingActivity().getShortClassName().equals(".PedidoListaActivity")) {

			Intent intent = this.getIntent();
			intent.putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente());

			this.setResult(RESULT_OK, intent);
			this.finish();
		} else {

			if (cliente.getIdCliente() >= VALOR_ID_CLIENTE_NOVO){
				DialogCustom.showCustomDialogMensagem(R.layout.dialog_cliente_novo, getString(R.string.nao_possivel_fazer_pedido_cliente_novo), this,true,
						new DialogCustom.IResultadoDialog() {
							@Override
							public void execultar(Dialog dialog) {
								dialog.dismiss();

							}

							@Override
							public void cancelar(Dialog dialog) {
								dialog.cancel();
							}

						});
			}else {
				registerForContextMenu(l);
				v.showContextMenu();
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		menu.setHeaderTitle(getString(R.string.selecione_opcao_desejada));

		menu.add(0, 0, 0, R.string.pedido);
		menu.add(1, 1, 1, R.string.consulta_pedidos);
		menu.add(2, 2, 2, R.string.detalhes);
		menu.add(3, 3, 3, R.string.pendencias);
		menu.add(4, 4, 4, R.string.precos);
		menu.add(5, 5, 5, R.string.ultimas_compras);
		menu.add(6, 6, 6, R.string.motivo_de_nao_venda);
		menu.add(7, 7, 7, R.string.excluir_cliente);

		MenuItem item0 = menu.getItem(0);
		MenuItem item1 = menu.getItem(1);
		MenuItem item2 = menu.getItem(2);
		MenuItem item3 = menu.getItem(3);
		MenuItem item4 = menu.getItem(4);
		MenuItem item5 = menu.getItem(5);
		MenuItem item6 = menu.getItem(6);
		MenuItem item7 = menu.getItem(7);

		setaCorMenuItem("Pedido", item0);
		setaCorMenuItem("Consulta Pedidos", item1);
		setaCorMenuItem("Detalhes", item2);
		setaCorMenuItem(getString(R.string.pendencias), item3);
		setaCorMenuItem(getString(R.string.precos), item4);
		setaCorMenuItem(getString(R.string.ultimas_compras), item5);
		setaCorMenuItem(getString(R.string.motivo_de_nao_venda), item6);
		setaCorMenuItem("Excluir Cliente", item7);

		menu.setGroupVisible(6, parametro.isPermitidoInformarNaoVenda());
		menu.setGroupVisible(5, true);
		menu.setGroupVisible(7, isPermitidoExcluir(mClienteAux.getIdCliente()));

//		menu.add(6, 6, 6, "Troca");
//		menu.setGroupVisible(6, parametro.getUtilizaTroca() == (short) 1 || parametro.getUtilizaTroca() == (short) 2) ;
	}

	private void setaCorMenuItem(String string, MenuItem item) {
		SpannableString s = new SpannableString(string);
		s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, s.length(), 0);
		item.setTitle(s);
	}

	private boolean isPermitidoExcluir(Long idCliente){
		if(!clienteDAO.clienteEnviado(idCliente)){
			if (!clienteDAO.clientePossuiPedido(idCliente)){
				return true;
			}else{
				Log.d("TAG","NAO ENTROU 2");
				return false;
			}
		}else{
			Log.d("TAG","NAO ENTROU 2");
			return false;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		final Cliente cliente = listaClientes.get(info.position);		

		switch (item.getItemId()) {
		case 0: // PEDIDO
			List<Pendencia> pendencias = null;
			try {
				Utils.apagarObjetoSerializado(getBaseContext());
				pendencias = new PendenciaDAO(getBaseContext(), null).listaPendencia(cliente.getIdCliente(), (short) 1);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (pendencias != null  && !pendencias.isEmpty()){
				AlertPendencia(this, cliente);

			}else {
				iniciarPedido(cliente, true);
			}
			break;

		case 1: // CONSULTA PEDIDOS

				startActivity(new Intent(PedidoListaActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()).addCategory(Principal.CATEGORIA));

			break;

		case 2: // DETALHES			
			startActivity(new Intent(ClienteTabHostActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()).addCategory(Principal.CATEGORIA));
			break;

		case 3: // PEND?NCIAS
			startActivityForResult(new Intent(PendenciasListaActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()).addCategory(Principal.CATEGORIA), PENDENCIA);
			break;

		case 4: // PRE?OS			
			startActivity(new Intent(ConsultaPrecoListActivity.INTENT).addCategory(Principal.CATEGORIA).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()));
			break;

		case 5: // ?LTIMAS COMPRAS
			startActivityForResult(new Intent(UltimasComprasListaActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()).addCategory(Principal.CATEGORIA), ULTIMAS_COMP);
			break;
			
//		case 6: // TROCA
//			iniciarTroca(cliente, true);
//			break;
			
		case 6: // N?O VENDA
			showDialogMotivoNaoVenda();
			break;

		case 7: // EXCLUIR
			showDialogExcluirCliente();
			break;
		default:
			break;
		}
		return true;
	}

	public void AlertPendencia(Activity activity, final Cliente cliente ){
		final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(R.string.atencao);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage("Este cliente Possui Pendecias Finaceiras, Deseja Continuar? ")
				.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						iniciarPedido(cliente, true);
					}
				})

		.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		// Create the AlertDialog object and return it
		 builder.create().show();
	}
	
	private void showDialogExcluirCliente() {
		new AlertDialog.Builder(this)
	        .setTitle("Deseja realmente excluir este cliente?")
	        .setMessage(R.string.as_informacoes_serao_perdidas)
	        .setNegativeButton(R.string.nao, new OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
					
				}
			})
	        .setPositiveButton("Excluir", new OnClickListener() {
	            public void onClick(DialogInterface arg0, int arg1) {
	                excluirCliente();
	            }
        }).create().show();
	}
	
	private void excluirCliente() {
		
		LimiteDAO limiteDAO = new LimiteDAO(ClienteListaActivity.this, null);
		Limite limite = limiteDAO.get(mClienteAux.getIdCliente());
		
		if(clienteDAO.delete(mClienteAux)){
			if(limiteDAO.delete(limite)){
				Toast.makeText(ClienteListaActivity.this, "Cliente excluido com sucesso!", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
		
	}

	private void iniciarPedido(final Cliente cliente, boolean verificarObjetivos) {
		if (isGeolocalizacaoAtiva()) {
//			if (verificarObjetivos && new ObjetivoDAO(getBaseContext(), null).existemRegistrosBanco(true)) {
//				startActivityForResult(new Intent(ObjetivoMetaListActivity.ACTION).addCategory(Principal.CATEGORIA), REQUEST_OBJETIVOS);
//			} else {
				startActivityForResult(new Intent(PedidoTabHost.INTENT).addCategory(Principal.CATEGORIA).putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente()), REQUEST_PEDIDO);
//			}
		}
	}
	
	@SuppressWarnings("unused")
	private void iniciarTroca(final Cliente cliente, boolean verificarPendencia) {
		if (isGeolocalizacaoAtiva()) {
			Intent it = new Intent("TrocaTabHost");
			it.addCategory(Principal.CATEGORIA);
			it.putExtra(Cliente.EXTRA_IDCLIENTE, cliente.getIdCliente());
	
			if (verificarPendencia && (possuiPendencia(cliente.getIdCliente())) && clienteBloqueado(cliente.getIdCliente())) {
				clienteInadimplente(it, cliente);
			} else {
				startActivityForResult(it,TROCA_TAB);
			}
		}
	}
	
	private boolean clienteBloqueado(long idCliente) {
		try {
			int dias = new PendenciaDAO(getBaseContext(), null).getMaiorVencimentocliente(idCliente);
			Parametro par = new ParametroDAO(getBaseContext(), null).get();

			return dias >= par.getQtdeDiasBloquearInadimplente();

		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}


		return false;
	}
	
	private boolean possuiPendencia(long idCliente) {		
		return new PendenciaDAO(getApplicationContext(), null).possuiPendencia(idCliente);
	}
	
	private void clienteInadimplente(final Intent it, final Cliente cliente)  {
		final AlertDialog alerta=new AlertDialog.Builder(this).create();	
		final EditText edtSenha=new EditText(getBaseContext());	
		int qtdDias;

		try {
			Parametro par=new ParametroDAO(getBaseContext(), null).get();
			alerta.setTitle("Senha Cliente Bloqueado. Repr: "+par.getIdVendedor());
			qtdDias = new PendenciaDAO(getBaseContext(), null).getMaiorVencimentocliente(cliente.getIdCliente());
			String msg=String.format("Cliente com titulos vencidos a %d dia(s), informe\n senha para atender: Cliente CDG: %d",qtdDias,cliente.getIdCliente());
			alerta.setMessage(msg);
			alerta.setView(edtSenha);
		} catch (java.text.ParseException e1) {
			e1.getMessage();
		}
		
		alerta.setButton(AlertDialog.BUTTON_POSITIVE, "Confirmar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					Parametro par = new ParametroDAO(getApplicationContext(), null).get();
					int idParametro=Integer.parseInt(String.valueOf(par.getIdVendedor()));
					int idCliente=Integer.parseInt(String.valueOf(cliente.getIdCliente()));
					String senhaCorreta=Utils.senhaClienteBloqueadoSupremacia(idParametro,idCliente);
					String senhaDigitada=edtSenha.getText().toString();
					if(!senhaCorreta.equals(senhaDigitada)){
						Toast.makeText(getApplicationContext(),"Senha incorreta!",Toast.LENGTH_SHORT).show();
						edtSenha.setText("");
						edtSenha.requestFocus();
					} else {
						startActivityForResult(it,TROCA_TAB);
					}
				} catch (Exception e) {							
					Log.e("ClienteLista","SenhaClienteBloqueado "+e.getMessage());
				}

			}
		});
		
		alerta.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				alerta.dismiss();
			}
		});
	
		alerta.show();
	}

	private void recuperacaoPedido() {
		if (Utils.existeObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR)) {

			AlertDialog.Builder msg = new AlertDialog.Builder(this);
			msg.setTitle(getString(R.string.atencao));
			msg.setMessage(getString(R.string.recuperar_pedido_lista_clientes));
			msg.setCancelable(false);
			msg.setIcon(android.R.drawable.ic_dialog_alert);
			msg.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					Pedido pedidoRecuperado = (Pedido) Utils.recuperarObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR);
					Intent it = new Intent(getBaseContext(), PedidoTabHost.class);

					// Implementar o CnpjCpf do cliente caso necess?rio . . .
					it.putExtra(Pedido.EXTRA_ID, pedidoRecuperado.getIDPedido());
					it.putExtra(Cliente.EXTRA_IDCLIENTE, pedidoRecuperado.getIDCliente());
					it.putExtra("RecuperacaoPedido", true);
					startActivityForResult(it, REQUEST_PEDIDO);
				}
			});
			msg.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int id) {
					Utils.apagarObjetoSerializado(getBaseContext());
					resetPreferencesSpinner();
					dialog.cancel();
				}
			});
			AlertDialog alert = msg.create();
			alert.show();
		}
	}

	private void resetPreferencesSpinner() {
		sharedPreference("clicouTipoPedido", false);
		sharedPreference("clicouCondPagamento", false);
		sharedPreference("clicouFormaPagamento", false);
	}

	private void sharedPreference(String keyChild, boolean value){
		SharedPreferences.Editor editor = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE).edit();
		editor.putBoolean(keyChild, value);
		editor.apply();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_PEDIDO) {
			if (data == null || data.getLongExtra(Pedido.EXTRA_ID, 0) == 0) {
				showDialogMotivoNaoVenda();				
			}else{
				gerarBonificacao(data);
			}
		} else if (requestCode == PENDENCIA && resultCode == RESULT_OK) {
			Toast.makeText(getApplicationContext(), R.string.cliente_nao_possui_pendencias, Toast.LENGTH_SHORT).show();

		} else if (requestCode == ULTIMAS_COMP && resultCode == RESULT_OK) {
			Toast.makeText(getApplicationContext(), R.string.cliente_nao_possui_ultimas_compras, Toast.LENGTH_SHORT).show();
		} else if (requestCode == REQUEST_OBJETIVOS) {
			iniciarPedido(mClienteAux, false);
		}
	}

	private void gerarBonificacao(Intent it) {
		Pedido pedido = getPedido(it);
		final Pedido finalPedido = pedido;

		if (pedido!= null)
		if(!pedido.isBonificacao(getBaseContext()) && pedido.getVinculoBonificacao()==0) {
			GerarBonificacaoDialog.gerarDialog(parametro, pedido, this, new GerarBonificacaoDialog.OnClickListener() {
				@Override
				public void onClick() {
					iniciaPedidoDeBonificacao(finalPedido);
				}
			});
		}

	}

	private void iniciaPedidoDeBonificacao(Pedido pedido) {
		Bundle extras = new Bundle();
		extras.putLong(PedidoTabActivity.PEDIDO_ID_BONIFICACAO, pedido.getIDPedido());
		extras.putLong(Cliente.EXTRA_IDCLIENTE, pedido.getIDCliente());

		Intent intent = new Intent(PedidoTabHost.INTENT).addCategory(Principal.CATEGORIA);
		intent.putExtras(extras);
		startActivity(intent);
	}


	private Pedido getPedido(Intent it) {
		final long idPedido = it.getLongExtra(Pedido.EXTRA_ID, -1);
		Pedido pedido = new Pedido();
		pedido.setIDPedido(idPedido);
		pedido = new PedidoDAO(this,null).get(pedido.getIDPedido(), null);
		return pedido;
	}

	/* ADAPTER */
	private class Adaptador extends BaseAdapter implements SectionIndexer {
		private Context ctx;

		List<String> myElements = null;
		HashMap<String, Integer> alphaIndexer;
		String[] sections;

		public Adaptador(Context ctx) {
			this.ctx = ctx;
			atualizaBarraLetras();
		}

		public void atualizaBarraLetras() {
			myElements = new ArrayList<String>(listaClientes.size());

			for (int i = 0; i < listaClientes.size(); i++) {
				myElements.add(listaClientes.get(i).getRazao());
			}

			alphaIndexer = new HashMap<String, Integer>();

			int size = listaClientes.size();

			for (int i = size - 1; i >= 0; i--) {
				String element = myElements.get(i);
				alphaIndexer.put(element.substring(0, 1), i);
			}

			Set<String> keys = alphaIndexer.keySet();

			Iterator<String> it = keys.iterator();
			ArrayList<String> keyList = new ArrayList<String>();

			while (it.hasNext()) {
				String key = it.next();
				keyList.add(key);
			}

			Collections.sort(keyList);

			sections = new String[keyList.size()];

			keyList.toArray(sections);
		}

		@Override
		public int getCount() {
			return listaClientes.size();
		}

		@Override
		public Object getItem(int position) {
			return listaClientes.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void changeData(List<Cliente> lista) {
			listaClientes = lista;
			notifyDataSetChanged();
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.cliente_lista, null);
			}

			if (position % 2 == 0) {
				view.setBackgroundResource(R.drawable.alterselector1);
			} else {
				view.setBackgroundResource(R.drawable.alterselector2);
			}

			Cliente cli = listaClientes.get(position);

			TextView txtCodigo = (TextView) view.findViewById(R.idListaCliente.codigo);
			txtCodigo.setText(String.format("%s - %s", cli.getIdCliente(), cli.getRazao()));
			txtCodigo.setTextColor(cli.isPedidoPendente() ? rgb(28, 134, 238) : Color.GREEN);
			txtCodigo.setTextSize(16);

			TextView txtFantasia = (TextView) view.findViewById(R.idListaCliente.fantasia);
			txtFantasia.setText(cli.getFantasia());
			txtFantasia.setTextColor(Color.WHITE);

			TextView txtCnpjCpf = (TextView) view.findViewById(R.idListaCliente.cnpjCpf);
			txtCnpjCpf.setText(String.format("CnpjCpf : %s", cli.getCnpjCpf().length() == 11 ? formataCpf(cli.getCnpjCpf()) : formataCnpj(cli.getCnpjCpf())));
			txtCnpjCpf.setTextColor(Color.WHITE);
			
			TextView endereco=(TextView) view.findViewById(R.idListaCliente.endereco);
			endereco.setTextColor(Color.WHITE);
			endereco.setText(cli.getEndereco());

			return view;
		}

		@Override
		public int getPositionForSection(int section) {
			String letter = sections[section];
			return alphaIndexer.get(letter);
		}

		@Override
		public int getSectionForPosition(int position) {
			return 0;
		}

		@Override
		public Object[] getSections() {
			return sections;
		}

		private String formataCpf(String cpf) {
			return cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
		}

		private String formataCnpj(String cnpj) {
			return cnpj.substring(0, 2) + "." + cnpj.substring(2, 5) + "." + cnpj.substring(5, 8) + "/" + cnpj.substring(8, 12) + "-" + cnpj.substring(12, 14);
		}
	}
	
	private boolean isGeolocalizacaoAtiva() {
//		if (!Utils.existeProvedorMapeamentoAtivo(getBaseContext())) {
//			Utils.showDialogAtivarGps(getBaseContext(), this);
//			return false;
//		}
		return true;
	}
	
	private void showDialogMotivoNaoVenda() {
		// N?o permite informar?
		if (!parametro.isPermitidoInformarNaoVenda()){
			return;
		}
		
		// Iniciar servi?o de localiza??o
		if (!isGeolocalizacaoAtiva()) {
			return;
		}
		
		NaoVendaDialog dialog = new NaoVendaDialog(this, mClienteAux);
		dialog.show();
	}
}
