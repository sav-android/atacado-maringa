package br.net.sav.atacadomaringa;

import java.text.ParseException;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.modelo.ClienteSemCompra;

public class ClienteSemCompraActivity extends ListActivity {
	public static final String ACTION = "ClienteSemCompra" ;
	private ClienteSemCompraAdapter adapter ;
	private EditText txtQtde ;
	private Button btnPesquisar ; 	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		showCustomDialog();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		ClienteDAO clienteDAO = new ClienteDAO(getBaseContext(), null) ;
		List<ClienteSemCompra> listaSemCompra = null ;		

		try {					
			if(txtQtde.getText().toString().trim().length() != 0)
				listaSemCompra = clienteDAO.listaSemCompra(Integer.parseInt(txtQtde.getText().toString().trim())) ;
			else 
				listaSemCompra = clienteDAO.listaSemCompra(7);
		} catch (ParseException e) {
			Log.d(getString(R.string.cliente_sem_compra_activity), e.getMessage());
		}
		
		if(listaSemCompra != null) {
			if(adapter == null) {
				setContentView(R.layout.clientesemcompra);
				adapter = new ClienteSemCompraAdapter(getBaseContext(), listaSemCompra) ;
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(listaSemCompra);
				setListAdapter(adapter);
			}
		}		
		
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if(title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			setTitle("Total de Clientes sem compra: " + listaSemCompra.size());
		}
	}
	
	public void showCustomDialog() {
		final Dialog dialog = new Dialog(this);
		
		dialog.setContentView(R.layout.popupclientesemcompra);
		dialog.setTitle("Digite a quantidade de dias");
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		txtQtde = (EditText)dialog.findViewById(R.idpopupClienteSemCompra.txtSemCompra) ;
		btnPesquisar = (Button)dialog.findViewById(R.idpopupClienteSemCompra.btnPesquisar);		
		dialog.show();
		
		btnPesquisar.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {								
				onResume();				
				dialog.dismiss() ;
			}
		}) ; 									
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.clientesemcompra_menu, menu);
		return true;		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuClienteSemCompra.qtdeDias :
			showCustomDialog() ;
			return true ; 
		default :
			return super.onOptionsItemSelected(item);			
		}		
	}
}
