package br.net.sav.atacadomaringa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import br.net.sav.modelo.ClienteSemVisita;

public class ClienteSemVisitaAdapter extends BaseAdapter implements SectionIndexer {
	private List<ClienteSemVisita> lista ;
	private Context ctx ;
	
	List<String> myElements = null ;
	HashMap<String, Integer> alphaIndexer ;
	String[] sections ;
	
	public ClienteSemVisitaAdapter(Context ctx, List<ClienteSemVisita> lista) {	
		this.lista = lista ;
		this.ctx = ctx ;
		
		myElements = new ArrayList<String>(lista.size());
		
		for(int i=0; i< lista.size() ; i++ ) {
			myElements.add(lista.get(i).getRazao()); 
		}
		
		alphaIndexer = new HashMap<String, Integer>();
		
		int size = lista.size() ;
		
		for(int i = size - 1; i>= 0 ; i-- ) {
			String element = myElements.get(i);
			alphaIndexer.put(element.substring(0,1), i);
		}
		
		Set<String> keys = alphaIndexer.keySet();
		
		Iterator<String> it = keys.iterator() ;
		ArrayList<String> keyList =  new ArrayList<String>();
		
		while(it.hasNext()) {
			String key = it.next();
			keyList.add(key);
		}
		
		Collections.sort(keyList);
		
		sections = new String[keyList.size()];
		
		keyList.toArray(sections);

	}
	
	public void changeData(List<ClienteSemVisita> lista) {
		this.lista = lista ;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public int getPositionForSection(int section) {	
		String letter = sections[section];
		return alphaIndexer.get(letter);
	}

	@Override
	public int getSectionForPosition(int position) {
		return 0;
	}

	@Override
	public Object[] getSections() {
		return sections;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		if(view == null) {
			LayoutInflater layout = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
			view = layout.inflate(R.layout.clientesemcompra, null);
		}	
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		ClienteSemVisita clienteSemVisita = lista.get(position);
		
		TextView txtRazao = (TextView) view.findViewById(R.idClienteSemCompra.txtRazao);
		txtRazao.setText(String.valueOf(clienteSemVisita.getIdCliente()) + " - " + clienteSemVisita.getRazao()) ;
		txtRazao.setTextColor(Color.GREEN);		
		
		TextView txtNDias = (TextView)view.findViewById(R.idClienteSemCompra.txtNDias);
		TextView txtUltimaVisita = (TextView)view.findViewById(R.idClienteSemCompra.txtUltimaCompra);
		
		if(clienteSemVisita.getQtdeDias() == -1) {
			txtNDias.setVisibility(View.VISIBLE);
			txtUltimaVisita.setText(R.string.ultima_visita_nunca);
		} else {					
			txtNDias.setText("Qtde Dias: " + String.valueOf(clienteSemVisita.getQtdeDias()));
			txtUltimaVisita.setText(String.format(ctx.getString(R.string.ultima_visita), sdf.format(clienteSemVisita.getUltimaVisita())));
		}								
		return view;		
	}	
}