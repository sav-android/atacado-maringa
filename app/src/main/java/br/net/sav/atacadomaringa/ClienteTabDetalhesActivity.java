package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.dao.CidadeDAO;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ContatoDAO;
import br.net.sav.dao.LimiteDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Limite;
import br.net.sav.modelo.Parametro;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.service.EnderecoApiService;
import br.net.sav.util.CepUtil;
import br.net.sav.util.modelo.Address;
import br.net.sav.utils.ManipulacaoCpfCnpj;
import br.net.sav.utils.ManipulacaoEmail;


import static br.net.sav.atacadomaringa.ProcuraCepActivity.ENDERECO_INTEIRO;

public class ClienteTabDetalhesActivity extends Activity implements
        View.OnTouchListener, View.OnClickListener {
    private EditText txtIdCliente;
    private EditText txtFantasia;
    private EditText txtRazao;
    private EditText txtCnpjCpf;
    private EditText txtIeRg;
    private AutoCompleteTextView txtCidade;
    private EditText txtUf;
    private EditText txtDDD;
    private EditText txtTelefone;
    private EditText txtLimiteCredito;
    private EditText txtLimiteDisponivel;
    private EditText txtEndereco;
    private EditText txtNumero;
    private EditText txtTipoEndereco;
    private EditText txtComplemento;
    private EditText txtBairro;
    private EditText txtCep;
    private EditText txtEmail;
    private Spinner spnTipoPessoa;
    private Button btnCep;
    private TextView txvNaoSeiMeuCep;

    private EditText txtSetor;
    private EditText txtZona;
    private Button btnContato;

    private Cliente cliente;
    private ClienteDAO clienteDao;
    private LimiteDAO limiteDAO;
    private Limite limite;

    private boolean isClienteNovo = false;
    private boolean spnDesativado = false;
    private boolean cpfCnpjValido = false;
    private boolean isPossivelAlteracao = false;

    // Menu
    private MenuItem itemMenuAlterar;
    private MenuItem itemMenuExcluir;
    private MenuItem itemMenuSalvar;
    private MenuItem itemMenuUpdate;

    private long idCliente;
    private boolean clicouEmAlterarCliente = false;
    public ArrayList<String> cidades = null;
    private Parametro mParametro = null;
    private CepUtil util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cliente_detalhes);
        mParametro = new ParametroDAO(getBaseContext(), null).get();

        recuperarLayout();

        acrescentaUtils();

        idCliente = getIntent().getLongExtra("IdCliente", 0);

        try {
            cliente = new ClienteDAO(getBaseContext(), null).get(idCliente);
            btnContato.setOnClickListener(this);


        } catch (ParseException e) {
            Toast.makeText(getBaseContext(),
                    "Problemas ao carregar dados do cliente", Toast.LENGTH_LONG)
                    .show();
            Log.e(getClass().getName(), e.getMessage());
        }

        if (idCliente > 0) {
            if (!statusCliente(idCliente)) { // Se ainda n�o foi realizado
                // transmiss�o e o cliente n�o
                // foi enviado, sendo assim �
                // poss�vel realizar altera��o
                // nos dados do cliente;
                isPossivelAlteracao = true;
            }
            ativarEdicaoEditText(false);
            //limite = new LimiteDAO(getBaseContext(), null).get(idCliente);
            carregarDados();
        } else {
            if (cliente == null) {
                isClienteNovo = true;
                instanciarObjetos();
                popularCampos();
            }
        }
    }

    private boolean statusCliente(Long idCliente) {
        if (clienteDao == null) {
            clienteDao = new ClienteDAO(ClienteTabDetalhesActivity.this, null);
        }
        return clienteDao.clienteEnviado(idCliente);
    }

    private void popularCampos() {
        try {
            txtIdCliente.setText(String.valueOf(clienteDao.gerarIdCliente()));
        } catch (Exception e) {
            Toast.makeText(getBaseContext(),
                    "Problemas ao gerar ID do cliente", Toast.LENGTH_LONG)
                    .show();
            e.printStackTrace();
        }
        txtIeRg.setOnTouchListener(this);
        txtCidade.setOnTouchListener(this);
        txtCnpjCpf.setOnTouchListener(this);
        carregarSpnTipoPessoa();
        carregarAutoCompleteCidade();
    }

    private void instanciarObjetos() {
        clienteDao = new ClienteDAO(ClienteTabDetalhesActivity.this, null);
        limiteDAO = new LimiteDAO(ClienteTabDetalhesActivity.this, null);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void carregarDados() {
        try {
            txtIdCliente.setText(String.valueOf(cliente.getIdCliente()));
            txtFantasia.setText(cliente.getFantasia());
            txtRazao.setText(cliente.getRazao());
            txtCnpjCpf.setText(cliente.getCnpjCpf());
            txtIeRg.setText(String.valueOf(cliente.getIeRg()));
            txtCidade.setText(cliente.getCidade());
            txtUf.setText(cliente.getUF());
            txtDDD.setText(String.valueOf(cliente.getDDD()));
            txtTelefone.setText(String.valueOf(cliente.getTelefone()));
            txtLimiteCredito.setText(""/*String.format("%.2f",
					limite != null ? limite.getLimiteTotal() : 0.0)*/);
            txtLimiteDisponivel.setText(""/*String.format("%.2f",
					limite != null ? limite.getLimiteDisponivel() : 0.0)*/);
            txtTipoEndereco.setText(cliente.getTipoEndereco());
            txtEndereco.setText(String.valueOf(cliente.getEndereco()));
            txtNumero.setText(String.valueOf(cliente.getNumero()));
            txtComplemento.setText(String.valueOf(cliente.getComplemento()));
            txtBairro.setText(cliente.getBairro());
            txtCep.setText(String.format("%s", cliente.getCEP()));
            txtEmail.setText(cliente.getEmail());
            txtZona.setText(cliente.getZona());
            txtSetor.setText(cliente.getSetor());
            String[] tipoPessoa = new String[]{cliente.getTipoPessoa() == (short) 1 ? getString(R.string.fisica)
                    : getString(R.string.juridica)};
            spnTipoPessoa.setAdapter(new ArrayAdapter<String>(getBaseContext(),
                    android.R.layout.simple_dropdown_item_1line, tipoPessoa));
        } catch (Exception e) {
            Log.e(getString(R.string.cliente_tab_detalhes_carregar_dados),
                    "carregarDados - " + e.getMessage());
        }
    }

    public void recuperarLayout() {
        txtIdCliente = (EditText) findViewById(R.idDetalheCliente.txtIdCliente);
        txtFantasia = (EditText) findViewById(R.idDetalheCliente.txtFantasia);
        txtRazao = (EditText) findViewById(R.idDetalheCliente.txtRazao);
        txtCnpjCpf = (EditText) findViewById(R.idDetalheCliente.txtCnpjCpf);
        txtIeRg = (EditText) findViewById(R.idDetalheCliente.txtIeRg);
        txtCidade = (AutoCompleteTextView) findViewById(R.idDetalheCliente.txtCidade);
        txtUf = (EditText) findViewById(R.idDetalheCliente.txtUf);
        txtDDD = (EditText) findViewById(R.idDetalheCliente.txtDdd);
        txtTelefone = (EditText) findViewById(R.idDetalheCliente.txtTelefone);
        txtLimiteCredito = (EditText) findViewById(R.idDetalheCliente.txtLimiteCredito);
        txtLimiteDisponivel = (EditText) findViewById(R.idDetalheCliente.txtLimiteDisponivel);
        txtEndereco = (EditText) findViewById(R.idDetalheCliente.txtEndereco);
        txtTipoEndereco = (EditText) findViewById(R.idDetalheCliente.txtTipoEndereco);
        txtComplemento = (EditText) findViewById(R.idDetalheCliente.txtComplemento);
        txtBairro = (EditText) findViewById(R.idDetalheCliente.txtBairro);
        txtCep = (EditText) findViewById(R.idDetalheCliente.txtCep);
        txtEmail = (EditText) findViewById(R.idDetalheCliente.txtEmail);
        spnTipoPessoa = (Spinner) findViewById(R.idDetalheCliente.spnTipoPessoa);
        btnCep = (Button) findViewById(R.id.btnCep);
        txvNaoSeiMeuCep = (TextView) findViewById(R.id.txvNaoSeiMeuCep);

        txtSetor = (EditText) findViewById(R.idDetalheCliente.txtSetor);
        txtZona = (EditText) findViewById(R.idDetalheCliente.txtZona);
        btnContato = (Button) findViewById(R.idDetalheCliente.btnContato);
        txtNumero = (EditText) findViewById(R.id.edtNumero);

        if (mParametro.isVisualizarBotaoContato()) {
            btnContato.setVisibility(View.VISIBLE);
        } else {
            btnContato.setVisibility(View.GONE);
        }
    }

    protected void setAdapterAutoCompleteTextViewFiltroCidade(AutoCompleteTextView pActCidade, ArrayList<String> pListaCidades) {
        if (pListaCidades != null && !pListaCidades.isEmpty()) {
            pActCidade.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, pListaCidades));
            pActCidade.setValidator(new Validator());
            pActCidade.setOnFocusChangeListener(new FocusListener());
        }
    }

    class FocusListener implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v.getId() == R.idDetalheCliente.txtCidade && !hasFocus) {
                ((AutoCompleteTextView) v).performValidation();
            }
        }
    }

    class Validator implements AutoCompleteTextView.Validator {

        @Override
        public boolean isValid(CharSequence text) {
            Log.v("Test", "Checking if valid" +
                    ": " + text);
            Collections.sort(cidades);
            if (Collections.binarySearch(cidades, text.toString()) > 0) {
                return true;
            }

            return false;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            Log.v("Test", "Returning fixed text");
            txtCidade.setHint("Cidade não encontrada, digite novamente");

            return "";
        }
    }


    // Desabilita os Editext para modifica��es, sendo poss�vel apenas a
    // visualiza��o
    private void ativarEdicaoEditText(boolean value) {
        txtFantasia.setEnabled(value);
        txtRazao.setEnabled(value);
        txtCnpjCpf.setEnabled(value);
        txtIeRg.setEnabled(value);
        txtCidade.setEnabled(value);
        txtUf.setEnabled(value);
        txtDDD.setEnabled(value);
        txtTelefone.setEnabled(value);
        //txtLimiteCredito.setEnabled(value);
        //txtLimiteDisponivel.setEnabled(value);
        txtEndereco.setEnabled(value);
        txtNumero.setEnabled(value);
        //txtTipoEndereco.setEnabled(value);
        txtComplemento.setEnabled(value);
        txtBairro.setEnabled(value);
        txtCep.setEnabled(value);
        txtEmail.setEnabled(value);
        spnTipoPessoa.setEnabled(value);
        //txtSetor.setEnabled(value);
        //txtZona.setEnabled(value);
    }

    // Cria o menu de op��es, quando for adicionar um novo cliente
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isClienteNovo) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_novocliente, menu);
            return true;
        } else if (isPossivelAlteracao) {

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_novocliente, menu);

            itemMenuAlterar = (MenuItem) menu.findItem(R.id.alterar_cliente);
            itemMenuExcluir = (MenuItem) menu.findItem(R.id.excluir_cliente);
            itemMenuSalvar = (MenuItem) menu.findItem(R.id.salvar_cliente);
            itemMenuUpdate = (MenuItem) menu.findItem(R.id.update_cliente);

            itemMenuAlterar.setVisible(true);
            itemMenuExcluir.setVisible(true);
            itemMenuSalvar.setVisible(false);

            return true;
        }
        return false;
    }

    // Defini as op��es do menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.salvar_cliente: {
                salvarClienteNoBanco();
                break;
            }

            case R.id.cancelar_cad_cliente: {
                if (clicouEmAlterarCliente) {
                    showDialogCancelarClienteNovo();
                } else if (isClienteNovo) {
                    showDialogCancelarClienteNovo();
                } else {
                    finish();
                }
                break;
            }
            case R.id.alterar_cliente: {
                clicouEmAlterarCliente = true;

                // Habilita os editext para modifica��es;
                ativarEdicaoEditText(true);
                carregarSpnTipoPessoa();
                carregarAutoCompleteCidade();

                // if (cliente != null){
                // if ( cliente.getTipoPessoa() == 1 ){
                // spnTipoPessoa.setSelection(0);
                // }else if ( cliente.getTipoPessoa() == 2 ){
                // spnTipoPessoa.setSelection(1);
                // }
                // }

                spnTipoPessoa.setEnabled(false);
                spnDesativado = true;
                cpfCnpjValido = true;

                // Menu
                itemMenuAlterar.setVisible(false);
                itemMenuExcluir.setVisible(false);
                itemMenuUpdate.setVisible(true);

                // onTouch para validar o campo CPF
                txtIeRg.setOnTouchListener(this);
                txtCidade.setOnTouchListener(this);
                txtCnpjCpf.setOnTouchListener(this);

                break;
            }
            case R.id.excluir_cliente: {
                showDialogExcluirCliente();
                break;
            }
            case R.id.update_cliente: {
                if (salvarAlteracoes()) {
                    Toast.makeText(ClienteTabDetalhesActivity.this,
                            R.string.alteracoes_salva_com_sucesso, Toast.LENGTH_SHORT)
                            .show();
                    finish();
                } else {
                    Toast.makeText(ClienteTabDetalhesActivity.this,
                            "Falha ao atualizar", Toast.LENGTH_SHORT).show();
                }
                break;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean salvarAlteracoes() {
        Cliente clienteAlterado = preencherObjetoComEditText();
        Limite limiteClienteAlteado = preencherLimite(clienteAlterado);

        if (clienteDao == null) {
            clienteDao = new ClienteDAO(ClienteTabDetalhesActivity.this, null);
        }
        if (limiteDAO == null) {
            limiteDAO = new LimiteDAO(ClienteTabDetalhesActivity.this, null);
        }

        if (clienteDao.update(clienteAlterado)) {
            if (limiteDAO.update(limiteClienteAlteado)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        if (isClienteNovo) {
            showDialogCancelarClienteNovo();
        } else if (clicouEmAlterarCliente) {
            showDialogCancelarClienteNovo();
        } else {
            super.onBackPressed();
        }

    }

    private void showDialogCancelarClienteNovo() {
        new AlertDialog.Builder(this).setTitle("Deseja realmente sair?")
                .setMessage(R.string.as_informacoes_serao_perdidas)
                .setNegativeButton(R.string.nao, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();

                    }
                }).setPositiveButton("Sair", new OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (isClienteNovo) {
                    new ContatoDAO(ClienteTabDetalhesActivity.this, null).excluirContatosSemCliente();
                    limparCampos();

                }
                ClienteTabDetalhesActivity.this.finish();
            }
        }).create().show();
    }

    private void showDialogExcluirCliente() {
        new AlertDialog.Builder(this)
                .setTitle("Deseja realmente excluir este cliente?")
                .setMessage(R.string.as_informacoes_serao_perdidas)
                .setNegativeButton(R.string.nao, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();

                    }
                }).setPositiveButton("Excluir", new OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                excluirCliente();
            }
        }).create().show();
    }

    private void excluirCliente() {
        if (clienteDao.delete(cliente)) {
            if (limiteDAO == null) {
                limiteDAO = new LimiteDAO(ClienteTabDetalhesActivity.this, null);
            }
            if (limiteDAO.delete(limite)) {
                Toast.makeText(ClienteTabDetalhesActivity.this,
                        R.string.cliente_excluido_com_sucesso, Toast.LENGTH_SHORT)
                        .show();
                finish();
            }
        }

    }

    private boolean salvarClienteNoBanco() {
        // objetoPreenchido();
        try {
            if (validarCampos()) {
                if (!isCPFInexistente()) {
                    Cliente clienteNovo = preencherObjetoComEditText();
                    if (clienteNovo != null) {
                        Limite limiteClienteNovo = preencherLimite(clienteNovo);
                        if (limiteClienteNovo != null) {
                            if (clienteDao.insert(preencherObjetoComEditText(),
                                    null)) {

                                clienteDao.atualizarIdRepresentante(Long.toString(clienteNovo.getIdCliente()), Long.toString(clienteNovo.getCodRepresentante()));

                                Log.i("Salvando cliente", "Salvo");

                                if (limiteDAO.insert(limiteClienteNovo)) {
                                    Toast.makeText(
                                            ClienteTabDetalhesActivity.this,
                                            "Cadastro realizado com sucesso!",
                                            Toast.LENGTH_SHORT).show();

                                    finish();
                                    return true;
                                } else {
                                    Toast.makeText(
                                            ClienteTabDetalhesActivity.this,
                                            "Erro ao insert no limite",
                                            Toast.LENGTH_SHORT).show();
                                    return false;
                                }
                            } else {
                                Toast.makeText(ClienteTabDetalhesActivity.this,
                                        "Erro ao insert no banco",
                                        Toast.LENGTH_SHORT).show();
                                return false;
                            }
                        } else {
                            Toast.makeText(ClienteTabDetalhesActivity.this,
                                    "Erro limite == null", Toast.LENGTH_SHORT)
                                    .show();
                            return false;
                        }
                    } else {
                        Toast.makeText(ClienteTabDetalhesActivity.this,
                                "Erro clienteNovo == null", Toast.LENGTH_SHORT)
                                .show();
                        return false;
                    }
                } else {
                    Toast.makeText(ClienteTabDetalhesActivity.this,
                            R.string.cpf_ja_cadastrado, Toast.LENGTH_SHORT).show();
                    this.txtCnpjCpf.requestFocus();
                    return false;
                }
            } else {
                Toast.makeText(ClienteTabDetalhesActivity.this,
                        "Erro ao validarCampos.", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(ClienteTabDetalhesActivity.this, "Exception ",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    // Verifica se os campos obrigat�rios est�o preenchidos
    private boolean validarCampos() {
        verificarAlteracaoCampoCnpjCpf();
        int qntCamposInvalidos = 0;
        if (txtFantasia.getText().toString().isEmpty()) {
            txtFantasia.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }

        if (txtRazao.getText().toString().isEmpty()) {
            txtRazao.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (cpfCnpjValido) {
            cpfCnpjValido = false;
            verificarTipoPessoa();
        }
        if (txtCnpjCpf.getText().toString().isEmpty()) {
            txtCnpjCpf.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        } else if (!cpfCnpjValido) {
            txtCnpjCpf.setError("Preencha corretamente");
            qntCamposInvalidos++;
        }
        if (txtCidade.getText().toString().isEmpty()) {
            txtCidade.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (txtUf.getText().toString().isEmpty()) {
            txtUf.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (!isValidarEmail()) {
            txtEmail.setError(getString(R.string.campo_obrigatorio_invalido));
            qntCamposInvalidos++;

        }
        if (txtEndereco.getText().toString().isEmpty()) {
            txtEndereco.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (txtNumero.getText().toString().isEmpty()) {
            txtNumero.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (txtBairro.getText().toString().isEmpty()) {
            txtBairro.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (txtCep.getText().toString().isEmpty()) {
            txtCep.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        } else if (txtCep.getText().toString().length() < 8) {
            txtCep.setError("Campo CEP Contem 8 caracter");
            qntCamposInvalidos++;
        }
        if (txtDDD.getText().toString().isEmpty()) {
            txtDDD.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (txtTelefone.getText().toString().isEmpty()) {
            txtTelefone.setError(getString(R.string.campo_obrigatorio));
            qntCamposInvalidos++;
        }
        if (qntCamposInvalidos > 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidarEmail() {
        return ManipulacaoEmail.isValidarEmail(txtEmail.getText().toString());
    }

    // Recupera valores do layout
    private Cliente preencherObjetoComEditText() {

        try {
            Cliente clienteNovo = new Cliente();
            clienteNovo.setIdCliente(Long.parseLong(txtIdCliente.getText()
                    .toString().toUpperCase()));

            clienteNovo.setFantasia(txtFantasia.getText().toString()
                    .toUpperCase());

            clienteNovo.setRazao(txtRazao.getText().toString().toUpperCase());

            clienteNovo.setCnpjCpf(txtCnpjCpf.getText().toString()
                    .toUpperCase());

            clienteNovo.setIeRg(txtIeRg.getText().toString().toUpperCase());

            clienteNovo.setCidade(txtCidade.getText().toString().toUpperCase());


            clienteNovo.setUF(txtUf.getText().toString().toUpperCase());

            clienteNovo.setDDD(Short.parseShort(txtDDD.getText().toString()));

            clienteNovo.setTelefone(Long.parseLong(txtTelefone.getText()
                    .toString().toUpperCase()));

            clienteNovo.setEndereco(txtEndereco.getText().toString()
                    .toUpperCase());

            clienteNovo.setTipoEndereco(txtTipoEndereco.getText().toString()
                    .toUpperCase());

            clienteNovo.setComplemento(txtComplemento.getText().toString()
                    .toUpperCase());

            clienteNovo.setBairro(txtBairro.getText().toString().toUpperCase());

            clienteNovo.setCEP(txtCep.getText().toString().toUpperCase());

            clienteNovo.setEmail(txtEmail.getText().toString().toUpperCase());

            clienteNovo.setZona(txtZona.getText().toString());

            clienteNovo.setSetor(txtSetor.getText().toString());
            clienteNovo.setNumero(Integer.valueOf(txtNumero.getText().toString()));

            if (spnTipoPessoa.getSelectedItemPosition() == 0) { // Fisica
                clienteNovo.setTipoPessoa(Short.parseShort("1"));
            } else if (spnTipoPessoa.getSelectedItemPosition() == 1) { // Juridica
                clienteNovo.setTipoPessoa(Short.parseShort("2"));
            }
            // clienteNovo.setTipoPessoa((short)spnTipoPessoa.getSelectedItemPosition());
            // String tipoPessoa = spnTipoPessoa.getSelectedItemPosition() ==
            // (short) 1 ? "F�sica" : "Jur�dica";

            clienteNovo.setPedidoPendente(false);
            clienteNovo.setEnviado(false);
            clienteNovo.setStatusCm(StatusCentralManager.PENDENTE.getCodigo());
            clienteNovo.setDataEnvio(Utils.sdfDataHoraDb
                    .parse("2017-12-18 08:21:30"));
            clienteNovo.setVisivel(false);
            clienteNovo.setStatusBloqueio("");
            clienteNovo.setRoteiro("");

            try {
                ParametroDAO parametroDAO = new ParametroDAO(
                        ClienteTabDetalhesActivity.this, null);
                Parametro parametro = parametroDAO.get();
                if (parametro != null) {
                    clienteNovo.setCodRepresentante(parametro.getIdVendedor());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            clienteNovo.setFlagCliente("");

            return clienteNovo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Limite preencherLimite(Cliente clienteNovo) {
        Limite limiteCliente = new Limite();
        try {
            limiteCliente.setIdCliente(clienteNovo.getIdCliente());

            if (txtLimiteDisponivel.getText().toString().isEmpty()) {
                limiteCliente.setLimiteDisponivel(0d);
            } else {
                limiteCliente.setLimiteDisponivel(Double
                        .parseDouble(txtLimiteDisponivel.getText().toString().replace(",", ".")));
            }

            if (txtLimiteCredito.getText().toString().isEmpty()) {
                limiteCliente.setLimiteTotal(0d);
            } else {
                limiteCliente.setLimiteTotal(Double
                        .parseDouble(txtLimiteCredito.getText().toString().replace(",", ".")));
            }

            return limiteCliente;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Verifica Pessoa Fisica / Juridica
    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        switch (arg0.getId()) {
            case R.idDetalheCliente.txtIeRg: {
                verificarTipoPessoa();
                break;
            }
            case R.idDetalheCliente.txtCidade: {
                verificarTipoPessoa();
                break;
            }
            case R.idDetalheCliente.txtCnpjCpf: {
                verificarAlteracaoCampoCnpjCpf();
                break;
            }
            default:
                break;
        }
        return false;
    }

    private void verificarAlteracaoCampoCnpjCpf() {
        txtCnpjCpf.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                verificarTipoPessoa();

            }
        });
    }

    private void verificarTipoPessoa() {
        if (txtCnpjCpf != null && !txtCnpjCpf.getText().toString().isEmpty()) {
            if (ManipulacaoCpfCnpj.isCnpj(txtCnpjCpf.getText().toString())) {
                if (ManipulacaoCpfCnpj.validaCnpj(txtCnpjCpf.getText()
                        .toString())) {
                    spnTipoPessoa.setSelection(1);
                    //	txtSetor.setEnabled(false);
                    //	txtZona.setEnabled(false);
                    cpfCnpjValido = true;
                } else {
                    txtCnpjCpf.setError(getString(R.string.cnpj_invalido));
                }
            } else if (ManipulacaoCpfCnpj
                    .isCpf(txtCnpjCpf.getText().toString())) {
                if (ManipulacaoCpfCnpj.validaCpf(txtCnpjCpf.getText()
                        .toString())) {
                    spnTipoPessoa.setSelection(0);
                    //	txtSetor.setEnabled(true);
                    //	txtZona.setEnabled(true);
                    cpfCnpjValido = true;
                } else {
                    txtCnpjCpf.setError(getString(R.string.cpf_invalido));
                }
            } else {
                if (txtCnpjCpf.getText().toString().length() < 11) {
                    txtCnpjCpf.setError(getString(R.string.cpf_invalido));
                } else if (txtCnpjCpf.getText().toString().length() > 11
                        && txtCnpjCpf.getText().toString().length() < 14) {
                    txtCnpjCpf.setError(getString(R.string.cnpj_invalido));
                }
            }
            spnTipoPessoa.setEnabled(false);
            spnDesativado = true;
        }
    }

    private boolean isCPFInexistente() {
        return clienteDao.isCPFCNPJExistente(txtCnpjCpf.getText().toString());
    }

    // Defini os campos com valor vazio
    private void limparCampos() {
        txtIdCliente.setText("");
        txtFantasia.setText("");
        txtRazao.setText("");
        txtCnpjCpf.setText("");
        txtIeRg.setText("");
        txtCidade.setText("");
        txtUf.setText("");
        txtDDD.setText("");
        txtTelefone.setText("");
        txtLimiteCredito.setText("");
        txtLimiteDisponivel.setText("");
        txtEndereco.setText("");
        txtTipoEndereco.setText("");
        txtComplemento.setText("");
        txtBairro.setText("");
        txtCep.setText("");
        txtEmail.setText("");
        txtSetor.setText("");
        txtZona.setText("");
        txtNumero.setText("");
    }

    public void carregarSpnTipoPessoa() { // carregarSpnTipoPessoa
        List<String> listaTipoPessoa = new ArrayList<String>();
        listaTipoPessoa.add(getString(R.string.fisica)); // position 0
        listaTipoPessoa.add(getString(R.string.juridica)); // position 1

        spnTipoPessoa.setAdapter(new ArrayAdapter<String>(getBaseContext(),
                android.R.layout.simple_dropdown_item_1line, listaTipoPessoa));

        spnTipoPessoa
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View view,
                                               int position, long id) {
                        spnTipoPessoa.setSelection(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                    }
                });
    }

    private void carregarAutoCompleteCidade() {
        cidades = new CidadeDAO(getBaseContext(), null).getCidades();
        setAdapterAutoCompleteTextViewFiltroCidade(txtCidade, cidades);

    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.idDetalheCliente.btnContato: {
                if (idCliente > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("IdCliente", idCliente);
                    Intent intent = new Intent(this, ClienteContatoActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else if (isClienteNovo) {
                    Bundle bundle = new Bundle();
                    Long idClienteLong = Long.parseLong(txtIdCliente.getText()
                            .toString());
                    bundle.putSerializable("IdCliente", idClienteLong);
                    Intent intent = new Intent(this, ClienteContatoActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        }
    }

    public void enderecoAutomatico(View view) {
        util.lockFields(true);
        new EnderecoApiService().getEnderecoPorCEP(txtCep.getText().toString(), new EnderecoApiService.EnderecoApiCallback<Address>() {
            @Override
            public void quandoSucesso(Address endereco) {
                setDataViews(endereco);
                util.lockFields(false);
            }

            @Override
            public void quandoErro(String mensagemErro) {
                Toast.makeText(getBaseContext(), mensagemErro, Toast.LENGTH_LONG).show();
                util.lockFields(false);
            }
        });
    }

    private void acrescentaUtils() {
        util = new CepUtil(ClienteTabDetalhesActivity.this,
                R.idDetalheCliente.txtCep,
                R.idDetalheCliente.txtCidade,
                R.idDetalheCliente.txtUf,
                R.idDetalheCliente.txtEndereco,
                R.id.edtNumero,
                R.idDetalheCliente.txtComplemento,
                R.idDetalheCliente.txtBairro);
    }

    public void setDataViews(Address endereco) {
        txtCep.setText(endereco.getCep());
        txtEndereco.setText(endereco.getLogradouro());
        txtBairro.setText(endereco.getBairro());
        txtUf.setText(endereco.getUf());
        txtCidade.setText(endereco.getLocalidade());
        txtComplemento.setText(endereco.getComplemento());
    }

    public void searchZipCode(View view) {
        Intent intent = new Intent(this, ProcuraCepActivity.class);
        startActivityForResult(intent, Address.REQUEST_ZIP_CODE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Address.REQUEST_ZIP_CODE_CODE) {
                Address endereco = (Address) data.getSerializableExtra(ENDERECO_INTEIRO);
                setDataViews(endereco);
            }
        }
    }

}
