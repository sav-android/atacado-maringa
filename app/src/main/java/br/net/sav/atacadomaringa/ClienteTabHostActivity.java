package br.net.sav.atacadomaringa;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class ClienteTabHostActivity extends TabActivity {
	public static final String INTENT = "ClienteDetalhesTabHost";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cliente_tabhost);

		Intent it = getIntent();
		long idCliente = it.getLongExtra("IdCliente", 0);

		Resources res = getResources();
		TabHost tabHost = getTabHost();
		TabSpec spec;
		Intent intent;

		intent = new Intent("ClienteTabPrincipal");
		intent.putExtra("IdCliente", idCliente);
		intent.addCategory(Principal.CATEGORIA);
		spec = tabHost.newTabSpec("Principal").setIndicator("Principal", res.getDrawable(R.drawable.tab_principal)).setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent("ListaPendencias");
		intent.putExtra("IdCliente", idCliente);
		intent.putExtra("teste", 1);
		intent.addCategory(Principal.CATEGORIA);
		spec = tabHost.newTabSpec("Pendência").setIndicator("Pendencia", res.getDrawable(R.drawable.tab_pendencia)).setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent("ClienteResumoPendencia");
		intent.putExtra("IdCliente", idCliente);
		intent.addCategory(Principal.CATEGORIA);
		spec = tabHost.newTabSpec("Resumo").setIndicator("Res.Pend.", res.getDrawable(R.drawable.itens_pedido)).setContent(intent);
		tabHost.addTab(spec);
	}
}