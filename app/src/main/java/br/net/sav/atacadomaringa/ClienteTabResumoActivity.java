package br.net.sav.atacadomaringa;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import br.net.sav.dao.PendenciaDAO;
import br.net.sav.modelo.Pendencia;

public class ClienteTabResumoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.resumo_pendencias);

		try {
			List<Pendencia> listaPendencias = new PendenciaDAO(getBaseContext(), null).listaPendencia(getIntent().getLongExtra("IdCliente", 0), (short) 0);

			long lTitulosVencer = 0;
			double dblTotalTitulosVencer = 0;
			long lTitulosVencidos = 0;
			double dblTotalTitulosVencidos = 0;
			NumberFormat nf = NumberFormat.getCurrencyInstance();

			Date dataAtual = new Date();
			String dataFormatada = null;

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			for (Pendencia p : listaPendencias) {
				if (p.getVencimento() != null) {
					dataFormatada = sdf.format(p.getVencimento());

					if ((dataFormatada != null) && (p.getVencimento().before(dataAtual))) {
						lTitulosVencidos++;
						dblTotalTitulosVencidos += p.getValor();
					} else {
						lTitulosVencer++;
						dblTotalTitulosVencer += p.getValor();
					}
				}
			}

			TextView txvQtdeVencidos = (TextView) findViewById(R.idResumoPendencias.txvQtdeVencidos);
			txvQtdeVencidos.setText(String.valueOf(lTitulosVencidos));

			TextView txvQtdeAvencer = (TextView) findViewById(R.idResumoPendencias.txvQtdeAVencer);
			txvQtdeAvencer.setText(String.valueOf(lTitulosVencer));

			TextView txvQtdeTotal = (TextView) findViewById(R.idResumoPendencias.txvQtdeTotal);
			txvQtdeTotal.setText(String.valueOf(lTitulosVencidos + lTitulosVencer));

			TextView txvValorVencidos = (TextView) findViewById(R.idResumoPendencias.txvValorVencidos);
			txvValorVencidos.setText(nf.format((dblTotalTitulosVencidos)));

			TextView txvValorVencer = (TextView) findViewById(R.idResumoPendencias.txvValorAVencer);
			txvValorVencer.setText(nf.format((dblTotalTitulosVencer)));

			TextView txvValorTotal = (TextView) findViewById(R.idResumoPendencias.txvValorTotal);
			txvValorTotal.setText(nf.format((dblTotalTitulosVencer + dblTotalTitulosVencidos)));

		} catch (Exception e) {
			Log.d("ClienteResumoPendencias", e.getMessage());
		}
	}
}
