package br.net.sav.atacadomaringa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.ParseException;
import java.util.List;

import br.net.sav.ComboController.comboUtils.DescontoComboUtils;
import br.net.sav.ComboController.comboUtils.DialogAlertComboUltils;
import br.net.sav.Utils;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComboXPedidoDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ComboXProduto;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.TipoPedido;

import static br.net.sav.ComboController.comboUtils.DescontoComboUtils.ADICIONAR;
import static br.net.sav.ComboController.comboUtils.DescontoComboUtils.REMOVER;
import static br.net.sav.ComboController.comboUtils.DialogAlertComboUltils.CONDICAO_COMBO;
import static br.net.sav.ComboController.comboUtils.DialogAlertComboUltils.CONDICAO_COMBO_ATIVADO;
import static br.net.sav.comboControllerSav.ModeloDTO.RegraExcecao.ZERO;

public class ComboAdapter extends BaseAdapter {

    public static final String DESATIVAR = "Desativar";
    public static final String ATIVAR = "Ativar";
    public static boolean acaoCombo = false;
    private Pedido pedido;
    private Parametro parametro;
    private Context context;
    private List<RegraAplicavel> comboList;
    private List<ItemDigitacao> itensPedido;
    private TipoPedido tipoPedido;
    private Cliente cliente;
    public ToggleButton btnAtivar;
    public static long IdComboAtivado;

    @Override
    public int getCount() {
        return comboList.size();
    }

    @Override
    public Object getItem(int i) {
        return comboList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return comboList.get(i).getIdCombo();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View returnView = LayoutInflater.from(context).inflate(R.layout.linha_combo, null);
        final RegraAplicavel combo = (RegraAplicavel) getItem(i);

        inicialzarComponente(returnView, combo);

        return returnView;
    }

    private void inicialzarComponente(View returnView, RegraAplicavel combo) {
        setarTexto(returnView, combo);
        setarBotao(returnView, combo);

    }

    private void setarTexto(View view, RegraAplicavel combo) {
        ComboXProduto comboXProduto = RegraCompra.buscaComboXProdutoPor(context, (Combo) combo);
        TextView textoLimite    = (TextView) view.findViewById(R.id.item_combo_txt_limite);
        TextView textoData      = (TextView) view.findViewById(R.id.txtDataValidade);
        TextView textoDescricao = (TextView) view.findViewById(R.id.txtDescricao);
        TextView textCondicaoPgto = (TextView) view.findViewById(R.id.item_combo_id_condicao) ;
        TextView txvIdCombo = (TextView) view.findViewById(R.id.txtTitulo) ;
        txvIdCombo.setText(context.getString(R.string.n_combo)+" "+combo.getIdCombo());
        textoData.setText(combo.getDataValidade());
        textoDescricao.setText(combo.getDescricao());

        if (comboXProduto != null) {
            textCondicaoPgto.setVisibility(View.VISIBLE);
            if (comboXProduto.getIdCondicao()> 0) {
                Condicao condicao = new CondicaoDAO(context, null).get((short) comboXProduto.getIdCondicao());
                textCondicaoPgto.setText(condicao.getDescricao());
            }else {
                textCondicaoPgto.setText(R.string.sem_c0ndicao_cadastrada_combo);
            }

        }else {
            textCondicaoPgto.setVisibility(View.GONE);
        }

        if(!combo.satisfazRegraLimite(pedido,context))
            textoLimite.setVisibility(View.VISIBLE);
    }
    private void habilitarBotaoBloqueado(RegraAplicavel combo) {
        if(itensPedido!=null) {
            boolean satifaz = combo.satisfazRegraCompra(itensPedido);
            btnAtivar.setEnabled(satifaz /*&& combo.satisfazRegraLimite(pedido,context)*/);
        }else{
            btnAtivar.setEnabled(false);
            btnAtivar.setTextOn(ATIVAR);
            btnAtivar.setText(ATIVAR);
        }

        if(btnAtivar.getText().toString().equals(DESATIVAR))
            btnAtivar.setEnabled(true);
    }

    private void setarBotao(View view, RegraAplicavel combo) {
         btnAtivar = (ToggleButton) view.findViewById(R.id.btnButton);

        if(!pedido.comboAtivado((Combo) combo) ) {
            btnAtivar.setTextOff(ATIVAR);
            btnAtivar.setText(ATIVAR);
        }else{
            btnAtivar.setTextOn(DESATIVAR);
            btnAtivar.setText(DESATIVAR);
        }

        habilitarBotaoBloqueado(combo);
            btnAtivar.setOnClickListener(!pedido.comboAtivado((Combo) combo) ?
                    ganharBonusClickListener(combo) :
                    removerBonusClickListener(combo));
    }

    public View.OnClickListener ganharBonusClickListener(final RegraAplicavel combo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean condicaoComboAtivado = false;
                ToggleButton btnAtivar = (ToggleButton) v;
                if (!pedido.getCombos().isEmpty()){
                    for (Combo comboAtivado :pedido.getCombos()) {
                        if (comboAtivado.getIdCombo() != combo.getIdCombo()) {
                            if (!RegraCompra.mesmaCondicaoPagamentoDoComboAtivado(context, (Combo) combo, comboAtivado)) {
                                condicaoComboAtivado = true;
                                btnAtivar.setTextOff(ATIVAR);
                                btnAtivar.setText(ATIVAR);
                                new DialogAlertComboUltils( context)
                                        .msgAoAtivarComboComCondicaoDiferente(null, CONDICAO_COMBO_ATIVADO);
                                return;
                            }
                        }
                    }
                }

                if (pedido.getCombos().isEmpty()){
                    if (!pedido.mesmaCondicaoPagamento(context, (Combo) combo) ){
                        condicaoComboAtivado = true;
                        btnAtivar.setTextOff(ATIVAR);
                        btnAtivar.setText(ATIVAR);
                        new DialogAlertComboUltils( context)
                                .msgAoAtivarComboComCondicaoDiferente(null,CONDICAO_COMBO);
                        return;
                    }

                }
                if (!condicaoComboAtivado) {
                    ativacaoCombo(btnAtivar, combo);
                }
            }
        };
    }

    private void ativacaoCombo(ToggleButton btnAtivar, RegraAplicavel combo) {
        setarDescontoDoComboNosItens((Combo) combo, context);
        pedido.addCombo((Combo) combo);
        Utils.serializarObjeto(context, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        btnAtivar.setTextOn(DESATIVAR);
        btnAtivar.setText(DESATIVAR);
        acaoCombo = true;
        btnAtivar.setOnClickListener(removerBonusClickListener(combo));
        notifyDataSetChanged();
    }

    private void setarDescontoDoComboNosItens(Combo combo, Context ctx) {
        IdComboAtivado = combo.getIdCombo();
        new DescontoComboUtils(ctx,combo,pedido, itensPedido).DescontoDoComboNosItens(null,tipoPedido, ADICIONAR);
        IdComboAtivado = ZERO;
    }

    public View.OnClickListener removerBonusClickListener(final RegraAplicavel combo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton btnAtivar = (ToggleButton) v;
                pedido.removeCombo((Combo) combo);
                removerDescontoDoComboNosItens(context, (Combo) combo);
                removerComboXPedido(combo);
                Utils.serializarObjeto(context, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
                btnAtivar.setTextOff(ATIVAR);
                btnAtivar.setText(ATIVAR);
                acaoCombo = true;
                btnAtivar.setOnClickListener(ganharBonusClickListener(combo));
                notifyDataSetChanged();
            }
        };
    }

    private void removerDescontoDoComboNosItens(Context ctx, Combo combo) {
        IdComboAtivado = combo.getIdCombo();
        new DescontoComboUtils(ctx,combo,pedido, itensPedido).DescontoDoComboNosItens(null,tipoPedido, REMOVER);
        IdComboAtivado = ZERO;
    }

    private void removerComboXPedido(RegraAplicavel combo) {
        if(pedido.isAlterarPedido()) {
            new ComboXPedidoDAO(context).deletePorIdPedido(combo.getIdCombo(), pedido.getIDPedido(), pedido.getIDCliente(),pedido.getDataPedido());
        }
    }

    public ComboAdapter(Context context, List<RegraAplicavel> comboList, List<ItemDigitacao> itensPedido, Pedido pedido) {
        this.context = context;
        this.comboList = comboList;
        this.itensPedido = itensPedido;
        this.pedido = pedido;
        short tipoPedido = pedido.getIDTPedido();
        this.tipoPedido = new TipoPedidoDAO(context, null).get(tipoPedido == 0? (short) 0: tipoPedido);
        try {
        this.cliente = new ClienteDAO(context, null).get(pedido.getIDCliente());
            this.parametro = new ParametroDAO(context, null).get();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
