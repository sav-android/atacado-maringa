package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.ComboController.ComboControle;
import br.net.sav.ComboController.factory.RegraComboFactory;
import br.net.sav.Utils;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComboDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Pedido;

public class ComboTabActivity extends Activity {
    private Pedido mPedido;
    private static final String PEDIDO_SERIALIZAR = "pedido.srl";
    private List<ItemDigitacao> itensRecuperados;
    private ListView lvCombos;
    private List<RegraAplicavel> combosValidos= new ArrayList<RegraAplicavel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combo_tab);
        vincularViews();
        recuperarPedidoSerializado();
        setarCombos();
    }

    private void vincularViews() {
        lvCombos = (ListView) findViewById(R.id.list_item_combo);
    }


    @Override
    protected void onResume() {
        super.onResume();
        recuperarPedidoSerializado();
        itensRecuperados = ItemTabActivity.itemPedido;
        setarAdapter(combosValidos);
    }

    private void setarCombos() {
        combosValidos = Combo.buscarCombos(this, mPedido);
    }

    public void setarAdapter(List<RegraAplicavel> combos) {
        if (mPedido.isAlterarPedido()) {
            if(mPedido.getCombos().isEmpty()) {
                List<Combo> combosDoPedido = new ComboDAO(this).buscarCombosPorPedido(mPedido, null);
                mPedido.setCombos(combosDoPedido);
            }
        }
        lvCombos.setAdapter(new ComboAdapter(this,
                combos,
                itensRecuperados,
                mPedido));
    }

    private void recuperarPedidoSerializado() {
        mPedido = (Pedido) Utils.recuperarObjetoSerializado(getBaseContext(),PEDIDO_SERIALIZAR);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
