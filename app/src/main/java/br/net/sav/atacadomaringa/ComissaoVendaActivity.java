package br.net.sav.atacadomaringa;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import br.net.sav.dao.ComissaoDAO;
import br.net.sav.modelo.Comissao;

public class ComissaoVendaActivity extends ListActivity {
	public static final String ACTION = "ComissaoVenda" ;
	private ComissaoVendaAdapter adapter;
	private List<Comissao> lista;

	private short idTipo = -1;
	
	private double totalVendas ;
	private double totalComissao ;
	private double totalRecebimento ;

	private CharSequence[] tipo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		tipo = new CharSequence[] { "Venda", "Recebimento" };
		showCustomDialog();
	}

	@Override
	protected void onResume() {
		super.onResume();

		ComissaoDAO comissaoDAO = new ComissaoDAO(getBaseContext());

		lista = comissaoDAO.getAll();

		if (idTipo > 0)
			lista = comissaoDAO.listaComissao(idTipo);

		if (lista != null) {
			if (adapter == null) {
				setContentView(R.layout.listacomissao);
				adapter = new ComissaoVendaAdapter(getBaseContext(), lista);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(lista);
				setListAdapter(adapter);
			}
		}
		if (idTipo > 0)
			barraTitulo(idTipo);
	}

	public void showCustomDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecione o tipo:");
		builder.setItems(tipo, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				idTipo = (short) (item + 1);
				dialog.dismiss();
				onResume();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.comissao_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuComissaoVenda.filtro:
			showCustomDialog();
			break;
		
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void barraTitulo(short idTipo) {
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);		
		totalVendas = 0 ;
		totalComissao = 0; 
		totalRecebimento = 0 ;
		for(int i = 0; i < lista.size() ; i++) {
			totalVendas		 = lista.get(i).getTotalVendas();
			totalComissao	 = lista.get(i).getTotalComissao();
			totalRecebimento = lista.get(i).getTotalVendas();
		}
		
		if (title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			if (idTipo == 1)
				setTitle(String.format(getString(R.string.total_de_vendas_total_comissao), totalVendas, totalComissao));
			else if (idTipo == 2)
				setTitle(String.format(getString(R.string.total_de_recebimentos_total_comissao), totalRecebimento, totalComissao));
		}
	}
}