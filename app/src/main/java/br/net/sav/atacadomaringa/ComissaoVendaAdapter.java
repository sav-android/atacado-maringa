package br.net.sav.atacadomaringa;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.modelo.Comissao;

public class ComissaoVendaAdapter extends BaseAdapter {
	private List<Comissao> lista ;
	private Context ctx ;	
	
	public ComissaoVendaAdapter (Context ctx, List<Comissao> lista) {
		this.lista = lista ;
		this.ctx = ctx ;
	}

	@Override
	public int getCount() {
		return lista.size();
	}
	
	public void changeData(List<Comissao> lista) {
		this.lista = lista ;
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position ;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.listacomissao, null);
		}		
		
		if (position % 2 == 0) {
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		Comissao comissao = lista.get(position); 
		
		TextView txtPeriodo = (TextView)view.findViewById(R.idListaComissao.txvPeriodo);
		txtPeriodo.setText(ctx.getString(R.string.periodo)+comissao.getPeriodo());
		txtPeriodo.setTextColor(Color.GREEN);
		txtPeriodo.setTextSize(18);
		
		TextView txtTotal   = (TextView)view.findViewById(R.idListaComissao.txvTotal);
		txtTotal.setText(String.format("Total: %.2f", comissao.getTotalVendas()));
		txtTotal.setTextColor(Color.WHITE);
		txtTotal.setTextSize(18);
		
		TextView txtComissao =(TextView)view.findViewById(R.idListaComissao.txvComissao);
		txtComissao.setText(String.format(ctx.getString(R.string.comissao), comissao.getTotalComissao()));
		txtComissao.setTextColor(Color.WHITE);
		txtComissao.setTextSize(18);
		
		TextView txtPercComissao = (TextView)view.findViewById(R.idListaComissao.txvPercComissao);
		txtPercComissao.setText(String.format(ctx.getString(R.string.percentual_comissao), (comissao.getTotalComissao() * 100 ) / comissao.getTotalVendas() ));
		txtPercComissao.setTextColor(Color.WHITE);
		txtPercComissao.setTextSize(18);
		
		return view;
	}
}