package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.net.sav.Utils;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.EmpresaFilialDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.ItemDigitacaoDAO;
import br.net.sav.dao.MarcaDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TabelaPrecoOperacaoTrocaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.enumerador.OrdemListaProdutos;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.EmpresaFilial;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TipoPedido;

public class ConsultaPrecoListActivity extends ListActivity {
	public static final String INTENT = "ConsultaPreco";
	
	private ConsultaPrecoAdapter mAdapter;

	private List<ItemDigitacao> mItemCompleto;
	private List<ItemDigitacao> mItemFiltrado;
	private List<ItemDigitacao> mItemAux;

	private List<EmpresaFilial> mListaEmpresaFilial;
	private List<TipoPedido> mListaTipoPedido;
	private List<Tabela> mListaTabela;
	private List<FormaPagamento> mListaFormaPagamento;
	private List<Condicao> mListaCondicaoPagamento;

	private Parametro mParametro;
	private Pedido mPedido;
	private TipoPedido mTipoPedido;
	private Condicao mCondicao;
	private Tabela mTabela;

	private Spinner mSpnEmpresaFilial;
	private Spinner mSpnTipoPedido;
	private Spinner mSpnTabelaPreco;
	private Spinner mSpnCondicaoPgto;

	private Button mBtnOk;

	private int mUltimoFiltroMarca;
	
	private boolean mCbNormalAnterior = true;
	private boolean mCbProdutosNovosAnterior = true;
	private boolean mCbPromocoesAnterior = true;
	private boolean mCbRecemRecebidosAnterior = true;
	private boolean mIsOperacaoTroca = false;
	
	private double mUltimoDesconto;
	
	private OrdemListaProdutos mOrdemListaProdutos;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			mUltimoDesconto = 0d;
			mUltimoFiltroMarca = 0;
			mOrdemListaProdutos = OrdemListaProdutos.PRODUTOS_NOVOS;
			
			mParametro = new ParametroDAO(getBaseContext(), null).get();
			mPedido = new Pedido();
			carregarListas();
			inicializarPedido();
			new InicializacaoEstruturaTask().execute();
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu_consulta_precos, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuConsultaPrecos.filtrarPrecos:
			showDialogFiltroPreco();
			return true;

		case R.idMenuConsultaPrecos.filtrarProdutos:
			showFiltroDialog();
			return true;

		case R.idMenuConsultaPrecos.removerFiltros:
			mUltimoFiltroMarca = 0 ;
			mItemFiltrado = new ArrayList<ItemDigitacao>(mItemAux);

			atualizaAdapter();
			return true;
		}
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		v.setSelected(true);
		showCustomDialog(position);
	}

	public void carregarFoto(int position) {
		String path = Environment.getExternalStorageDirectory() + "/sav/fotos/" + String.valueOf(mItemFiltrado.get(position).getProduto().getIdProduto()) + ".jpg";
		Intent it = new Intent("Imagem");
		it.addCategory(Principal.CATEGORIA);
		it.putExtra("caminhoImagem", path);
		startActivity(it);
	}

	public void atualizaAdapter() {
		if (mAdapter == null) {
			setContentView(R.layout.itens_preco_lista);
			mAdapter = new ConsultaPrecoAdapter(getBaseContext(), mItemFiltrado);
			setListAdapter(mAdapter);
			getListView().setFastScrollEnabled(true);
		} else {
			mAdapter.changeData(mItemFiltrado);
		}
	}

	public void inicializarPedido() {
		mPedido.setIDCondicao(new CondicaoDAO(getBaseContext(), null).getAll().get(0).getIdCondicao());
		mPedido.setIDEmpresa(new EmpresaFilialDAO(getBaseContext(), null).getAll().get(0).getIdEmpresa());
		mPedido.setIDFilial(new EmpresaFilialDAO(getBaseContext(), null).getAll().get(0).getIdFilial());
		mPedido.setIDTabela(new TabelaDAO(getBaseContext(), null).getAll((short) 0, null).get(0).getIdTabela());
		mPedido.setIDFormaPagamento(new FormaPagamentoDAO(getBaseContext(), null).getAll().get(0).getIdFormaPagamento());
		mPedido.setIDTPedido(new TipoPedidoDAO(getBaseContext(), null).getAll().get(0).getIdTipoPedido());
	}

	public void showDialogFiltroPreco() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_consulta_preco);
		dialog.setTitle(R.string.filtro_de_precos);

		mSpnEmpresaFilial = (Spinner) dialog.findViewById(R.idConsultaPreco.spnEmpresaFilial);
		mSpnTipoPedido = (Spinner) dialog.findViewById(R.idConsultaPreco.spnTipoPedido);
		mSpnTabelaPreco = (Spinner) dialog.findViewById(R.idConsultaPreco.spnTabelaPreco);
		mSpnCondicaoPgto = (Spinner) dialog.findViewById(R.idConsultaPreco.spnCondicaoPgto);
		mBtnOk = (Button) dialog.findViewById(R.idConsultaPreco.btnOk);

		mBtnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				new InicializacaoEstruturaTask().execute();
			}
		});

		carregarSpnEmpresaFilial();

		dialog.show();
	}

	public void showFiltroDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.popupfiltroitem);
		dialog.setTitle("Filtro de Produtos");
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		final EditText edtPesquisa = (EditText) dialog.findViewById(R.idPopupFiltroItem.edtPesquisa);
		final Spinner spnMarca = (Spinner) dialog.findViewById(R.idPopupFiltroItem.spnFornecedor);
		final CheckBox cbOrdemAlfabetica = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbAlfabetica);
		final CheckBox cbNormal = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbNormal);
		final CheckBox cbProdutosNovos = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbProdutosNovos);
		final CheckBox cbPromocoes = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbPromocoes);
		final CheckBox cbRecemRecebidos = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbRecemRecebidos);
		final Button btnPesquisa = (Button) dialog.findViewById(R.idPopupFiltroItem.btnPesquisa);
		final Button btnCancela = (Button) dialog.findViewById(R.idPopupFiltroItem.btnCancela);

		spnMarca.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, new MarcaDAO(getBaseContext(), null).listaNomes()));
		spnMarca.setSelection(mUltimoFiltroMarca);
		
		cbOrdemAlfabetica.setChecked(true);

		cbNormal.setChecked(mCbNormalAnterior);
		cbProdutosNovos.setChecked(mCbProdutosNovosAnterior);
		cbPromocoes.setChecked(mCbPromocoesAnterior);
		cbRecemRecebidos.setChecked(mCbRecemRecebidosAnterior);

		btnPesquisa.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mUltimoFiltroMarca = spnMarca.getSelectedItemPosition() ;
				boolean bPesquisaCodigo = true;
				long codigoProduto = 0;
				int index = -1;
				
				mCbNormalAnterior = cbNormal.isChecked();
				mCbProdutosNovosAnterior = cbProdutosNovos.isChecked();
				mCbPromocoesAnterior = cbPromocoes.isChecked();
				mCbRecemRecebidosAnterior = cbRecemRecebidos.isChecked();

				try {
					codigoProduto = Long.parseLong(edtPesquisa.getText().toString());

				} catch (NumberFormatException e) {
					bPesquisaCodigo = false;
				}

				if (bPesquisaCodigo) {
					mItemFiltrado = new ArrayList<ItemDigitacao>(mItemAux);
					for (int i = 0; i < mItemFiltrado.size(); i++) {
						if (mItemFiltrado.get(i).getProduto().getIdProduto() == codigoProduto) {
							index = i;
							break;
						}
					}
					if (index == -1) {
						Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_produto_com_o_codigo_informado, Toast.LENGTH_SHORT).show();
					}
				} else {
					String pesquisa = edtPesquisa.getText().toString().toUpperCase();
					if (pesquisa.trim().equals("") && spnMarca.getSelectedItemPosition() == 0) {
//						itemFiltrado = new ArrayList<ItemDigitacao>(itemAux);

						mItemFiltrado = new ArrayList<ItemDigitacao>(mItemAux.size());

						boolean achou;
						for (ItemDigitacao item : mItemAux) {
							achou = false;
							
							if (cbNormal.isChecked())
								achou = item.getProduto().getStatus().equals(" ");
							
							if (cbProdutosNovos.isChecked() && !achou)
								achou = item.getProduto().getStatus().equals("N");
							
							if (cbPromocoes.isChecked() && !achou)
								achou =  item.getProduto().getStatus().equals("P");
							
							if (cbRecemRecebidos.isChecked() && !achou)
								achou = item.getProduto().getStatus().equals("R");

							if (achou)
								mItemFiltrado.add(item);
						}
					} else {
						long idMarca = 0;
						if (spnMarca.getSelectedItemPosition() > 0) {
							idMarca = new MarcaDAO(getBaseContext(), null).getIdByName(spnMarca.getSelectedItem().toString());
						}

						String[] trechos = pesquisa.split(",");
						mItemFiltrado = new ArrayList<ItemDigitacao>(mItemAux.size());

						boolean achou;
						for (ItemDigitacao item : mItemAux) {
							achou = false;
							
							if (cbNormal.isChecked())
								achou = item.getProduto().getStatus().equals(" ");
							
							if (cbProdutosNovos.isChecked() && !achou)
								achou = item.getProduto().getStatus().equals("N");
							
							if (cbPromocoes.isChecked() && !achou)
								achou =  item.getProduto().getStatus().equals("P");
							
							if (cbRecemRecebidos.isChecked() && !achou)
								achou = item.getProduto().getStatus().equals("R");
							
							if (achou) {
								if (trechos.length > 0) {
									for (int i = 0; i < trechos.length; i++) {
										achou = item.getProduto().getDescricao().contains(trechos[i].trim());
										if (!achou)
											break;
										if (idMarca> 0)
											achou = item.getProduto().getIdMarca() == idMarca;
										if (!achou)
											break;
									}
								} else {
									achou = item.getProduto().getIdMarca() == idMarca;
								}
							}

							if (achou)
								mItemFiltrado.add(item);
						}
					}

					if (mItemFiltrado.size() == 0) {
						mItemFiltrado = new ArrayList<ItemDigitacao>(mItemCompleto);

						Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_produto_com_o_conteudo_pesquisado, Toast.LENGTH_LONG).show();
					}

					if(cbOrdemAlfabetica.isChecked()) {
						Collections.sort(mItemFiltrado, ItemDigitacao.ItemDigitacaoComparatorOrdemAlfabetica);
						
						mOrdemListaProdutos = OrdemListaProdutos.ORDEM_ALFABETICA;
					} else {
						Collections.sort(mItemFiltrado);

						mOrdemListaProdutos = OrdemListaProdutos.PRODUTOS_NOVOS;
					}
					
					mItemCompleto = new ArrayList<ItemDigitacao>(mItemFiltrado);
				}

				atualizaAdapter();

				if (bPesquisaCodigo && index != -1) { // Achou produto pelo c?digo
					getListView().setSelection(index);
				}

				dialog.dismiss();
			}
		});

		btnCancela.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void carregarListas() throws Exception {
		mListaEmpresaFilial = new EmpresaFilialDAO(getBaseContext(), null).getAll();
		mListaTipoPedido = new TipoPedidoDAO(getBaseContext(), null).getAll();
		mListaTabela = new TabelaDAO(getBaseContext(), null).getAll((short) 0, null);
		mListaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getAll();
		mListaCondicaoPagamento = new CondicaoDAO(getBaseContext(), null).getAll();

		if (mListaEmpresaFilial.size() == 0) {
			throw new Exception(getString(R.string.nao_existem_empresas_filials_disponiveis_para_digitacao_pedido));
		} else if (mListaTipoPedido.size() == 0) {
			throw new Exception(getString(R.string.nao_existem_tipos_de_pedido_disponiveis_para_digitacao_de_pedido));
		} else if (mListaTabela.size() == 0) {
			throw new Exception(getString(R.string.nao_existem_tabelas_de_preco_disponiveis_para_digitacao_de_pedido));
		} else if (mListaFormaPagamento.size() == 0) {
			throw new Exception(getString(R.string.nao_existem_formas_de_pagamento_disponiveis_para_digitacao_de_pedido));
		} else if (mListaCondicaoPagamento.size() == 0) {
			throw new Exception(getString(R.string.nao_existem_condicoes_de_pagamento_disponiveis_para_digitacao_de_pedido));
		}

		Date dataAtual = new Date(); // SE AS TABELAS N?O ESTIVEREM DENTRO DO
										// PRAZO DE VALIDADE
		if (dataAtual.before(mListaTabela.get(0).getDataInicial()) || dataAtual.after(mListaTabela.get(0).getDataFinal())) {
			throw new Exception(String.format(getString(R.string.as_tabelas_de_preco_estao_fora_da_validade), Utils.sdfDataDb.format(mListaTabela.get(0).getDataInicial()), Utils.sdfDataDb.format(mListaTabela.get(0).getDataFinal())));
		}
	}

	public void carregarSpnEmpresaFilial() {
		int pos = 0;
		carregarSpnTipoPedido();

		List<String> lstEmpresaFilial = new ArrayList<String>();

		for (EmpresaFilial eFilial : mListaEmpresaFilial) {
			lstEmpresaFilial.add(eFilial.getDescricao());
			if (mPedido.getIDEmpresa() == eFilial.getIdEmpresa() && mPedido.getIDFilial() == eFilial.getIdFilial())
				pos = lstEmpresaFilial.indexOf(eFilial);
		}

		mSpnEmpresaFilial.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstEmpresaFilial));
		mSpnEmpresaFilial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
				mPedido.setIDEmpresa(mListaEmpresaFilial.get(position).getIdEmpresa());
				mPedido.setIDFilial(mListaEmpresaFilial.get(position).getIdFilial());

				carregarSpnTabelaPreco();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mSpnEmpresaFilial.setSelection(pos);
	}

	public void carregarSpnTipoPedido() {
		int pos = 0;
		List<String> lstTipoPedido = new ArrayList<String>();

		for (TipoPedido tipoPedido : mListaTipoPedido) {
			lstTipoPedido.add(tipoPedido.getDescricao());
			if (mPedido.getIDTPedido() == tipoPedido.getIdTipoPedido())
				pos = mListaTipoPedido.indexOf(tipoPedido);
		}

		mSpnTipoPedido.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstTipoPedido));
		mSpnTipoPedido.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
				mPedido.setIDTPedido(mListaTipoPedido.get(position).getIdTipoPedido());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		mSpnTipoPedido.setSelection(pos);
	}

	public void carregarSpnTabelaPreco() {
		int pos = 0;
		List<String> lstTabelaPreco = new ArrayList<String>();

		for (Tabela tab : mListaTabela) {
			lstTabelaPreco.add(tab.getDescricao());
			if (mPedido.getIDTabela() == tab.getIdTabela())
				pos = mListaTabela.indexOf(tab);
		}

		mSpnTabelaPreco.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstTabelaPreco));
		mSpnTabelaPreco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
				mPedido.setIDTabela(mListaTabela.get(position).getIdTabela());
				carregarSpnCondicaoPgto();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mSpnTabelaPreco.setSelection(pos);
	}

	public void carregarSpnCondicaoPgto() {
		mListaCondicaoPagamento = new CondicaoDAO(getBaseContext(), null).listaCondicaoPedido(mPedido.getIDEmpresa(), mPedido.getIDFilial(), mPedido.getIDTabela());
		List<String> lstCondicaoPgto = new ArrayList<String>();

		int pos = 0;
		for (Condicao con : mListaCondicaoPagamento) {
			lstCondicaoPgto.add(con.getDescricao());
			if (mPedido.getIDCondicao() == con.getIdCondicao())
				pos = mListaCondicaoPagamento.indexOf(con);
		}

		mSpnCondicaoPgto.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstCondicaoPgto));
		mSpnCondicaoPgto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
				mPedido.setIDCondicao(mListaCondicaoPagamento.get(position).getIdCondicao());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mSpnCondicaoPgto.setSelection(pos);
	}
	
	private void verificaIsOperacaoTroca() {
		mIsOperacaoTroca = new TabelaPrecoOperacaoTrocaDAO(getBaseContext(), null).isOperacaoTroca(mTipoPedido);
	}	
	
	public void showCustomDialog(final int position) {
		final ItemDigitacao itemDigitacao = mItemFiltrado.get(position);

		final Dialog dialog = new Dialog(this);
		dialog.setTitle(String.format("%d - %s", itemDigitacao.getProduto().getIdProduto(), itemDigitacao.getProduto().getDescricao()));
		dialog.setContentView(R.layout.popup_layout);		

		final EditText txvQtde = (EditText) dialog.findViewById(R.idPopup.txtQuantidade);
		final EditText txvValor = (EditText) dialog.findViewById(R.idPopup.txtValor);
		final EditText txtDesconto = (EditText) dialog.findViewById(R.idPopup.txtPercDesconto);
		final EditText txvTotalItem = (EditText) dialog.findViewById(R.idPopup.txtTotalItem);
		final Button btnMais = (Button) dialog.findViewById(R.idPopup.btnMais);
		final Button btnMenos = (Button) dialog.findViewById(R.idPopup.btnMenos);
		final Button btnVoltaPreco = (Button) dialog.findViewById(R.idPopup.btnVoltaPreco);
		final Button btnOk = (Button) dialog.findViewById(R.idPopup.btnOk);
		final Button btnFoto = (Button) dialog.findViewById(R.idPopup.btnFoto2);
		final TextView txvDesconto = (TextView) dialog.findViewById(R.idPopup.txvPercDesconto);
		
		btnOk.setVisibility(View.GONE);
		
		dialog.setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				if(itemDigitacao.getQuantidade() == 0) {
					txtDesconto.setText(String.format("%.4f", mUltimoDesconto).replace(",", "."));					
				}	
			}
		});

		txvTotalItem.setText(String.format("%.4f", itemDigitacao.getValorLiquido()));

		if (itemDigitacao.getQuantidade() > 0.0)
			txvQtde.setText(String.format("%.2f", itemDigitacao.getQuantidade()).replace(",", "."));

		if (itemDigitacao.isPossuiFoto()) {
			btnFoto.setVisibility(View.VISIBLE);
		} else {
			btnFoto.setVisibility(View.INVISIBLE);
		}

		btnFoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				carregarFoto(position);
			}
		});

		txvValor.setText(String.format("%.4f", itemDigitacao.getValorUnit()).replace(",", "."));
		txvValor.setEnabled(mParametro.isPermiteAlterarPreco());
		txtDesconto.setText(String.format("%.4f", itemDigitacao.getDesconto()).replace(",", "."));
		txtDesconto.setEnabled(mParametro.isPermiteAlterarPreco());

		if (mParametro.isPermiteAlterarPreco()) {
			txvDesconto.setVisibility(View.VISIBLE);
		} else {
			txvDesconto.setVisibility(View.GONE);
		}	

		txvValor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					EditText edt = (EditText) v;
					double dblDesconto = 0;

					try {
						if (!Utils.getText(edt).equals("")) {
							dblDesconto = Utils.arredondar(100 - ((Double.parseDouble(edt.getText().toString()) / itemDigitacao.getValorUnitOriginal()) * 100), 4);
						}
						txtDesconto.setText(String.format("%.4f", dblDesconto).replace(",", "."));
						if (dblDesconto > itemDigitacao.getProduto().getMaximoDesconto())
							txtDesconto.setTextColor(Color.RED);
						else
							txtDesconto.setTextColor(Color.BLACK);

					} catch (Exception e) {
						Log.e(getString(R.string.consulta_preco_list_activity_set_on_focus_change_listener), "setOnKeyListener." + e.getMessage());
					}
				}
			}
		});

		txtDesconto.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				double dblValor = itemDigitacao.getValorUnitOriginal();
				try {
					double descontoDigitado = 0;
					if (!txtDesconto.getText().toString().equals("")) {
						descontoDigitado = Double.parseDouble(txtDesconto.getText().toString());
						dblValor = Utils.arredondar(itemDigitacao.getValorUnitOriginal() - (itemDigitacao.getValorUnitOriginal() * (descontoDigitado / 100)), 4);
					}
					txvValor.setText(String.format("%.4f", dblValor).replace(",", "."));
					if (descontoDigitado > itemDigitacao.getProduto().getMaximoDesconto())
						txtDesconto.setTextColor(Color.RED);
					else
						txtDesconto.setTextColor(Color.BLACK);

					double qtde = Double.parseDouble(txvQtde.getText().toString().trim());
					txvTotalItem.setText(String.format("%.4f", qtde * dblValor));
				} catch (Exception e) {
					Log.e(getString(R.string.consulta_preco_list_activity_txt_desconto_on_text_changed), "onKeyListener." + e.getMessage());
				}
				return;
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				return;
			}
		});
		
		btnMais.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String qtde = txvQtde.getText().toString();

				try {
					if (qtde.equals("")) {
						if (!mIsOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
							qtde = String.valueOf(itemDigitacao.getProduto().getMultiplo());
						else
							qtde = "1.00";
					} else {
						if (!mIsOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
							qtde = String.format("%.2f", Utils.proximoMultiplo(Float.parseFloat(qtde.replace(",", ".")), itemDigitacao.getProduto().getMultiplo()));
						else
							qtde = String.format("%.2f", Double.parseDouble(qtde.replace(",", ".")) + 1);
					}
					qtde = qtde.replace(",", ".");
					txvQtde.setText(qtde);
					double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
					txvTotalItem.setText(String.format("%.4f", Double.parseDouble(qtde.replace(",", ".")) * valor));
				} catch (Exception e) {
					Log.e(getString(R.string.consulta_preco_list_activity_btn_mais_setonclicklistener), "setOnClickListener. " + e.getMessage());
				}
			}
		});

		btnMenos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String sQtde = txvQtde.getText().toString();

				try {
					if (!sQtde.equals("")) {
						float qtde = Float.parseFloat(sQtde.replace(",", "."));
						
						if (qtde == 0) {
							return;
						} 
							
						if (!mIsOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
							sQtde = String.format("%.2f", Utils.multiploAnterior(qtde, itemDigitacao.getProduto().getMultiplo()));
						else
							sQtde = String.format("%.2f", qtde - 1);
					}
					
					if(Double.parseDouble(sQtde.replace(",", ".")) < 0) {
						sQtde = "0.00";
					}
					
					sQtde = sQtde.replace(",", ".");
					txvQtde.setText(sQtde);
					double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
					txvTotalItem.setText(String.format("%.4f", Double.parseDouble(sQtde) * valor));
				} catch (Exception e) {
					Log.e(getString(R.string.consulta_preco_list_activity_btnmenos_setonclicklistener), "setOnClickListener. " + e.getMessage());
				}
			}
		});

		btnVoltaPreco.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final NumberFormat nf = NumberFormat.getCurrencyInstance();

				AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
				builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.atencao).setMessage(String.format("Deseja realmente voltar o valor original do produto: %s", nf.format(itemDigitacao.getValorUnitOriginal()))).setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						txvValor.setText(String.format("%.4f", itemDigitacao.getValorUnitOriginal()).replace(",", "."));
						txtDesconto.setText("0.00");
						dialog.dismiss();
					}
				}).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				builder.show();
			}
		});
		dialog.show();
	}
	
	class InicializacaoEstruturaTask extends AsyncTask<Void, Void, String> {
		private ProgressDialog progress;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgress();
		}

		@Override
		protected String doInBackground(Void... arg0) {
			try {
				mTipoPedido = new TipoPedidoDAO(getBaseContext(), null).get(mPedido.getIDTPedido());
				mCondicao = new CondicaoDAO(getBaseContext(), null).get(mPedido.getIDCondicao());
				mTabela = new TabelaDAO(getBaseContext(), null).get(mPedido.getIDTabela());

				mItemCompleto = new ItemDigitacaoDAO(getApplicationContext(), null).getEstruturaDigitacaoPedido(mTipoPedido, mTabela, mCondicao);
				mItemAux = new ArrayList<ItemDigitacao>(mItemCompleto);
				mItemFiltrado = new ArrayList<ItemDigitacao>(mItemCompleto);

			} catch (Exception e) {
				return getString(R.string.problemas_ao_carregar_precos) + e.getMessage();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			try {
				if (result != null) {
					Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
				} else {
					verificaIsOperacaoTroca();
					atualizaAdapter();
				}
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
			} finally {
				if (progress != null && progress.isShowing()) {
					progress.dismiss();
				}
			}
		}
		
		private void showProgress() {
			progress = new ProgressDialog(ConsultaPrecoListActivity.this);
			progress.setCancelable(false);
			progress.setCanceledOnTouchOutside(false);
			progress.setIcon(getResources().getDrawable(R.drawable.icon));
			progress.setTitle(getString(R.string.app_name));
			progress.setMessage("Por favor aguarde, carregando estruturas...");
			progress.show();
		}		
	}

	/* ADAPTER */
	private class ConsultaPrecoAdapter extends BaseAdapter implements SectionIndexer {
		private Context ctx;
		private List<ItemDigitacao> lista;

		List<String> myElements = null;
		HashMap<String, Integer> alphaIndexer;
		String[] sections;

		public ConsultaPrecoAdapter(Context ctx, List<ItemDigitacao> lista) {
			this.lista = lista;
			this.ctx = ctx;
			
			if (OrdemListaProdutos.ORDEM_ALFABETICA.equals(mOrdemListaProdutos)) {
				atualizaBarraLetras();
			} else {
				removerBarraLetras();
			}
		}

		public void atualizaBarraLetras() {
			myElements = new ArrayList<String>(lista.size());

			for (int i = 0; i < lista.size(); i++) {
				myElements.add(lista.get(i).getProduto().getDescricao());
			}

			alphaIndexer = new HashMap<String, Integer>();

			int size = lista.size();

			for (int i = size - 1; i >= 0; i--) {
				String element = myElements.get(i);
				alphaIndexer.put(element.substring(0, 1), i);
			}

			Set<String> keys = alphaIndexer.keySet();

			Iterator<String> it = keys.iterator();
			ArrayList<String> keyList = new ArrayList<String>();

			while (it.hasNext()) {
				String key = it.next();
				keyList.add(key);
			}

			Collections.sort(keyList);

			sections = new String[keyList.size()];

			keyList.toArray(sections);
		}
		
		public void removerBarraLetras() {
			ArrayList<String> keyList = new ArrayList<String>();
			sections = new String[keyList.size()];

			keyList.toArray(sections);
		}

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		public void changeData(List<ItemDigitacao> lista) {
			this.lista = lista;
			notifyDataSetChanged();
			
			if (OrdemListaProdutos.ORDEM_ALFABETICA.equals(mOrdemListaProdutos)) {
				atualizaBarraLetras();
			} else {
				removerBarraLetras();
			}
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getPositionForSection(int section) {
			if (sections != null && section < sections.length) {
				String letter = sections[section];
				return alphaIndexer.get(letter);
			}
			return 0;
		}

		@Override
		public int getSectionForPosition(int position) {
			return 0;
		}

		@Override
		public Object[] getSections() {
			return sections;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.itens_preco_lista, null);
			}

			if (position % 2 == 0) {
				view.setBackgroundResource(R.drawable.alterselector1);
			} else {
				view.setBackgroundResource(R.drawable.alterselector2);
			}
			
			ItemDigitacao item = lista.get(position);

			TextView txvDescricao = (TextView) view.findViewById(R.idItensPedido.txvDescricao);
			txvDescricao.setText(String.format("%d - %s", item.getProduto().getIdProduto(), item.getProduto().getDescricao()));
			txvDescricao.setTextSize(16);
			txvDescricao.setTypeface(Typeface.DEFAULT_BOLD);
			txvDescricao.setTextColor(Color.parseColor(item.getCorProduto()));
			txvDescricao.setBackgroundColor(item.getBackgroundProduto());

			TextView txvDescricaoEmbalagem = (TextView) view.findViewById(R.idItensPedido.txvDescricaoEmbalagem);
			txvDescricaoEmbalagem.setText(String.format("Embalagem : %s", item.getProduto().getDescricaoEmbalagem()));
			txvDescricaoEmbalagem.setTextColor(Color.WHITE);

			TextView txvQtde = (TextView) view.findViewById(R.idItensPedido.txvQtde);
			txvQtde.setVisibility(View.INVISIBLE);

			TextView txvQtdeEmbalagem = (TextView) view.findViewById(R.idItensPedido.txvQtdeEmbalagem);
			txvQtdeEmbalagem.setText(String.format("Qtde Embalagem: %d", item.getProduto().getQuantidadeEmbalagem()));
			txvQtdeEmbalagem.setTextColor(Color.WHITE);

			TextView txvMultiplo = (TextView) view.findViewById(R.idItensPedido.txvEstoque);
			txvMultiplo.setText(String.format("Estoque : %s", item.getProduto().getSaldoEstoque()));
			txvMultiplo.setTextColor(Color.WHITE);

			TextView txvValor = (TextView) view.findViewById(R.idItensPedido.txvValor);
			txvValor.setText(String.format("Valor Unit.: R$ %.4f", item.getValorUnit()));
			txvValor.setTextColor(Color.WHITE);
			
			TextView txvMarca = (TextView) view.findViewById(R.idItensPedido.txvMarca);
			txvMarca.setText(String.format("Marca: %s", item.getDescricaoMarca() != null ?  item.getDescricaoMarca() : "N�o encontrada"));
			txvMarca.setTextColor(Color.WHITE);

			TextView txvValorLiquido = (TextView) view.findViewById(R.idItensPedido.txvValorLiquido);
			txvValorLiquido.setVisibility(View.INVISIBLE);

			ImageView imgFoto = (ImageView) view.findViewById(R.idItensPedido.imgFoto);
			imgFoto.setVisibility(item.isPossuiFoto() ? View.VISIBLE : View.INVISIBLE);
			imgFoto.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					carregarFoto(position);
				}
			});
			
			return view;
		}
	}
}
