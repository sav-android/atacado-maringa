package br.net.sav.atacadomaringa;

import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import br.net.sav.Utils;

public class ControleAcesso {
	private static final String KEY_DATA = "DataUltimaRecepcao";

	@SuppressLint("SimpleDateFormat")
	public static void salvarDataUltimaRecepcao(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(KEY_DATA, Utils.sdfDataDb.format(new Date()));
		editor.commit();
	}

	public static long quantidadeDiasPassouUltimaTransmissao(Activity activity) {
		return Utils.qtdeDiasPassou(getDataUltimaRecepcao(activity));
	}

	@SuppressLint("SimpleDateFormat")
	public static Date getDataUltimaRecepcao(Activity activity) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext());
		try {
			String strData = prefs.getString(KEY_DATA, null);
			return Utils.sdfDataDb.parse(strData);
		} catch (Exception e) {
			return new Date();
		}
	}

	public static boolean validarConexaoDiaria(Activity activity) {
		int qtdeDiasPassou = (int) quantidadeDiasPassouUltimaTransmissao(activity);
	
		if (qtdeDiasPassou >= 30) {    //>= 1) {
			return false;
		}
		return true;
	}
}