package br.net.sav.atacadomaringa;

import android.os.Bundle;
import android.widget.Spinner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.GraficoMetas;
import br.net.sav.Utils;
import br.net.sav.dao.ComissaoDAO;
import br.net.sav.dao.ObjetivoDAO;
import br.net.sav.modelo.Comissao;
import br.net.sav.modelo.ObjetivoMeta;

public class GraficoMetasActivity extends GraficoMetas {

    private ObjetivoDAO mObjetivoMeta;
    private ComissaoDAO mComissao;
    private double metaQuantidadeRealizado;
    private double metaQuantidadeObjetivo;
    private String quantidadeRealizado;
    private String quantidadeObjetivo;
    private ArrayList<Double> valoresBarras;
    private List<String> dados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        valoresBarras = new ArrayList<>();
        mComissao = new ComissaoDAO(this);
        mObjetivoMeta = new ObjetivoDAO(this, null);
        recuperaObjetivoMeta();
        recuperaComissao();
        atualizarGraficoSpinnerComissao();
        atualizarGraficoSpinnerMeta();
    }

    @Override
    protected void carregaGraficoDeTorta(float metaAtingido, float metaObjetivo, String valorAtingido, String valorObjetivo) {
        super.carregaGraficoDeTorta(metaAtingido, metaObjetivo, valorAtingido, valorObjetivo);
    }

    @Override
    protected void carregaGraficoDeBarras(ArrayList<Double> valor, String labelVendaOuRecebimento) {
        super.carregaGraficoDeBarras(valor, labelVendaOuRecebimento);
    }

    @Override
    protected void carregarSpnTipo(Spinner spn, List<String> listaTipo, IGraficoResumo grafico) {
        super.carregarSpnTipo(spn, listaTipo, grafico);
    }

    private void recuperaObjetivoMeta() {
        dados = new ArrayList<>();
        List<ObjetivoMeta> objetivos= mObjetivoMeta.getObjetivosPorTipo(-1);
        for (ObjetivoMeta obj: objetivos){
            dados.add(obj.getDescricao());
        }

        carregarSpnTipo(spnObjetivo, dados, new IGraficoResumo() {
            @Override
            public void Atualizar() {
                atualizarGraficoSpinnerMeta();
            }
        });
    }

    private void recuperaComissao() {
        List<String> dados =new ArrayList<>();
        List<Comissao> comissaos = mComissao.getAll();
        for (Comissao com: comissaos){
            if (com.getTipo() == 1){
                dados.add(getString(R.string.label_grafico_barras_venda));
            }else if (com.getTipo() == 2) {
                dados.add(getString(R.string.label_grafico_barras_recebimentos));
            }
        }


        carregarSpnTipo(spnComissao, dados, new IGraficoResumo() {
            @Override
            public void Atualizar() {
                atualizarGraficoSpinnerComissao();
            }
        });
    }

    private void formataObjetivosEMetasEmReais() {
        quantidadeRealizado = Utils.nfMoeda.format(metaQuantidadeRealizado);
        quantidadeObjetivo = Utils.nfMoeda.format(metaQuantidadeObjetivo);
    }
    private void formataObjetivosEMetasEmPercentual() {
        DecimalFormat df2 = new DecimalFormat(" #,##0.00 '%'");
        quantidadeRealizado = df2.format(metaQuantidadeRealizado);
        quantidadeObjetivo = df2.format(metaQuantidadeObjetivo);
    }

    private void atualizarGraficoSpinnerComissao() {
        Object selectedItem = spnComissao.getSelectedItem();
        if (selectedItem != null) {
            String s = selectedItem.toString();
            if (s.equals(getString(R.string.label_grafico_barras_venda))) {
                valoresAoSelecionarSpinnerBarras((short) 1, R.string.label_grafico_barras_recebimentos);

            } else if (s.equals(getString(R.string.label_grafico_barras_recebimentos))) {
                valoresAoSelecionarSpinnerBarras((short) 2, R.string.label_grafico_barras_recebimentos);

            }
        }
    }
    private void atualizarGraficoSpinnerMeta() {
        String descricaoSpinner = "";
        Object selectedItem = spnObjetivo.getSelectedItem();
        if (selectedItem != null) {
            String s = selectedItem.toString();
            for (String dado : dados) {
                if (s.equals(dado)) {
                    descricaoSpinner = dado;
                    break;
                }
            }

            if (s.equals(descricaoSpinner)) {
                valoresAoSelecionarSpinnerMetas((short) -1, descricaoSpinner);

            }
        }
    }

    private void valoresAoSelecionarSpinnerMetas(short i, String descricao) {
        boolean existe = false;
        List<ObjetivoMeta> objetivos = mObjetivoMeta.getObjetivosPorTipo(i);
        if (objetivos != null) {
            for (ObjetivoMeta meta: objetivos) {
                if (meta.getDescricao().equals(descricao)) {
                    metaQuantidadeRealizado = meta.getRealizado();
                    metaQuantidadeObjetivo = meta.getObjetivo();
                    if (meta.getDescricao().toLowerCase().contains("rentabilidade")){
                        existe = true;
                    }
                    break;
                }
            }
        }else {
            metaQuantidadeRealizado = 0;
            metaQuantidadeObjetivo = 0;
        }
        if (!existe) {
            formataObjetivosEMetasEmReais();
        }else {
            formataObjetivosEMetasEmPercentual();
        }
        carregaGraficoDeTorta((float) metaQuantidadeRealizado, (float) metaQuantidadeObjetivo, quantidadeRealizado, quantidadeObjetivo);
    }

    private void valoresAoSelecionarSpinnerBarras(short i, int p) {
        Comissao comissao = mComissao.getTipo(i);
        if (comissao != null) {
            valoresBarras = new ArrayList<>();
            valoresBarras.add(comissao.getTotalComissao());
            valoresBarras.add(comissao.getTotalVendas());
        }else {
            valoresBarras = new ArrayList<>();
            valoresBarras.add(0d);
            valoresBarras.add(0d);
        }
        carregaGraficoDeBarras(valoresBarras, getString(p));
    }
}
