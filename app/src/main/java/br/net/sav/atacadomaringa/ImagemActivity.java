package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class ImagemActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imagem);

		try {
			Intent intent = getIntent();
			Bitmap bitmap = BitmapFactory.decodeFile(intent.getStringExtra("caminhoImagem").toLowerCase());
			ImageView imageView = (ImageView) findViewById(R.idImagem.imagem);
			imageView.setImageBitmap(bitmap);
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
}
