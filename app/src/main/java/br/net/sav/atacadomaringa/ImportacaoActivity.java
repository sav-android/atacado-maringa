package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Display;

import br.net.sav.presenter.ImportacaoPelaAPI;
import br.net.sav.presenter.ImportacaoPorLinhaTexto;

@SuppressLint("DefaultLocale")
public class ImportacaoActivity extends Activity implements Runnable , ImportacaoPorLinhaTexto.IAoImportaDadoPresenter {
    private static final int MENSAGEM = 1;
    private ProgressDialog pd;
    private ImportacaoPelaAPI presenter;
    private long startTime = 0;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new ImportacaoPelaAPI(this,this);
        presenter.getManterTelaAtiva();

        try {
            pd = new ProgressDialog(ImportacaoActivity.this);
            pd.setTitle(getString(R.string.importacao_de_dados));
            pd.setIcon(getResources().getDrawable(R.drawable.icon));
            pd.setMessage("Por favor, aguarde enquanto o sistema importa os dados recebidos.");
            pd.setIndeterminate(true);
            pd.setCancelable(false);
            pd.show();
            new Thread(ImportacaoActivity.this).start();

        } catch (Exception e) {
            Log.d("ImportacaoActivity", e.getMessage());
        }
    }


    @Override
    public void run() {
        try {

            Looper.prepare();
            startTime = System.currentTimeMillis();
            presenter.getIniciarImportacao();
           // presenter.getVerificarFotosFtp();
        } catch (Exception e) {
            e.printStackTrace();
            handler.sendEmptyMessage(2);
        } finally {
            handler.sendEmptyMessage(0);
            finish();
        }
    }


    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (pd != null) {
                switch (msg.what) {
                    case 0:
                        pd.dismiss();
//                        String tempoCronometro = getTempoCronometro(startTime);
//                        UIHelper.showNotification(null, getBaseContext(),
//                                getString(R.string.sincrinizacao_dados), String.format(getString(R.string.tempo_de_processo), tempoCronometro));
                       // setPreferInt(getBaseContext(), "PARAMETRO_ENCONTRADO",0);
                        break;

                    case MENSAGEM:
                        Bundle b = msg.getData();
                        pd.setMessage(b.getString("mensagem"));
                        break;
                    case 2:
                        pd.dismiss();
                        break;

                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public int getOrientation() {
        Display getOrient = getWindowManager().getDefaultDisplay();
        return getOrient.getOrientation();
    }

    @Override
    public Handler getHandler() {
        return handler;
    }
}
