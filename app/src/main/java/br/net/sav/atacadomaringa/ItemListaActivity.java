package br.net.sav.atacadomaringa;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.UltimaItemDAO;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.UltimaItem;

public class ItemListaActivity extends ListActivity {
	public static final String INTENT = "ListaItens";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			Intent it = getIntent(); // retorna o Intent criado por quem invocou
										// essa classe
			long idPedido = it.getLongExtra("IdPedido", 0);
			ArrayList<Item> listaItems = new ItemDAO(getBaseContext(), null).getByPedido(idPedido);
			setListAdapter(new ItemListaAdapter(getBaseContext(), listaItems));

			TextView title = (TextView) getWindow().findViewById(android.R.id.title);

			title.setGravity(Gravity.CENTER);
			title.setBackgroundResource(R.color.Verde);
			setTitle(getString(R.string.itens_do_pedido_nr)+": " + String.valueOf(it.getLongExtra("IdPedido", 0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class ItemListaAdapter extends BaseAdapter {
	private Context ctx;
	private ArrayList<Item> lista;

	public ItemListaAdapter(Context ctx, ArrayList<Item> lista) {
		this.ctx = ctx;
		this.lista = lista;
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.itempedidolista, null);
		}

		if (position % 2 == 0) {
			view.setBackgroundResource(R.drawable.alterselector1);
		} else {
			view.setBackgroundResource(R.drawable.alterselector2);
		}

		Item item = lista.get(position);

		TextView txvProduto = (TextView) view.findViewById(R.idItemPedidoLista.txvProduto);
		txvProduto.setText(String.format("%d - %s", item.getIDProduto(), (item.getDescricao() == null ? ctx.getString(R.string.produto_nao_encontrado) : item.getDescricao())));
		txvProduto.setTextSize(15);
		txvProduto.setTypeface(Typeface.DEFAULT_BOLD);
		txvProduto.setTextColor(Color.GREEN);
		txvProduto.setSingleLine(true);

		TextView txvEmbalagem = (TextView) view.findViewById(R.idItemPedidoLista.txvEmbalagem);
		txvEmbalagem.setText(String.format("Embalagem: %s", (item.getDescricaoEmbalagem() == null ? "" : item.getDescricaoEmbalagem())));
		txvEmbalagem.setTextSize(15);

		TextView txvQuantidade = (TextView) view.findViewById(R.idItemPedidoLista.txvQuantidade);
		txvQuantidade.setText(String.format("Quantidade: %.2f", item.getQuantidade()));
		txvQuantidade.setTextSize(15);

		TextView txvValorUnit = (TextView) view.findViewById(R.idItemPedidoLista.txvValorUnitario);
		txvValorUnit.setText(String.format("Valor Unit: %.4f", item.getValorUnitPraticado()));
		txvValorUnit.setTextSize(15);

		TextView txvPercDesconto = (TextView) view.findViewById(R.idItemPedidoLista.txvPercDesconto);
		txvPercDesconto.setText(String.format("Desconto: %.2f%%", item.getDesconto()));
		txvPercDesconto.setTextSize(15);

		TextView txvValorBruto = (TextView) view.findViewById(R.idItemPedidoLista.txvValorBruto);
		txvValorBruto.setText(String.format("Valor Bruto: %.4f", item.getTotalGeral()));
		txvValorBruto.setTextSize(15);

		TextView txvValorLiquido = (TextView) view.findViewById(R.idItemPedidoLista.txvValorLiquido);
		txvValorLiquido.setText(String.format(ctx.getString(R.string.valor_liquido2), item.getTotalLiquido()));
		txvValorLiquido.setTextSize(15);

		TextView txvValorFaturadoItem = (TextView) view.findViewById(R.id.txvValorTotalFaturadoItem);
		txvValorFaturadoItem.setText(String.format("Valor tot fatu.: %.4f", item.getTotalFaturadoItem()));
		txvValorFaturadoItem.setTextSize(15);

		TextView txvPercDescontoFatu= (TextView) view.findViewById(R.id.txvDescontoFaturadoItem);
		txvPercDescontoFatu.setText(String.format("Desconto fatu.: %.2f%%", item.getDescontoFaturado()));
		txvPercDescontoFatu.setTextSize(15);

		TextView txvQntdCortada = (TextView) view.findViewById(R.id.txvQuantidadeCortada);
		txvQntdCortada.setText(String.format("Qtd cortada: %d", item.getQntdCortada()));
		txvQntdCortada.setTextSize(15);

		TextView txvStatusItem = (TextView) view.findViewById(R.id.txvStatusItem);
		txvStatusItem.setVisibility(View.GONE);
		txvPercDescontoFatu.setVisibility(View.GONE);
		txvValorFaturadoItem.setVisibility(View.GONE);

		if (item.getStatus()!= null){
			if (!item.getStatus().isEmpty() ){
				txvStatusItem.setVisibility(View.VISIBLE);
				txvPercDescontoFatu.setVisibility(View.VISIBLE);
				txvValorFaturadoItem.setVisibility(View.VISIBLE);
				txvStatusItem.setText("Status: " + item.getStatus());
			}
		}

		txvStatusItem.setTextSize(15);

		return view;
	}

}
