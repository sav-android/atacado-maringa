package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import br.net.sav.ComboController.ComboControle;
import br.net.sav.ComboController.comboUtils.DescontoComboUtils;
import br.net.sav.ComboController.comboUtils.DialogAlertComboUltils;
import br.net.sav.ComboController.factory.RegraComboFactory;
import br.net.sav.LocationService;
import br.net.sav.Utils;
import br.net.sav.adapter.DialogComboAdapter;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.ItemDigitacaoDAO;
import br.net.sav.dao.LimiteDAO;
import br.net.sav.dao.MarcaDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.RestricaoTCTDAO;
import br.net.sav.dao.SituacaoEspecialDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TabelaPrecoOperacaoTrocaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.dialog.GerarBonificacaoDialog;
import br.net.sav.dialog.GerarSenhaDialog;
import br.net.sav.enumerador.OrdemListaProdutos;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ComboXPedido;
import br.net.sav.modelo.ComboXProduto;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Limite;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Produto;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.RestricaoTCT;
import br.net.sav.modelo.SituacaoEspecial;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TipoPedido;
import br.net.sav.util.StringUtils;
import br.net.sav.util.dao.MapeamentoDAO;
import br.net.sav.util.modelo.Mapeamento;
import br.net.sav.utils.ConfigSemafaro;
import br.net.sav.utils.MathUtils;
import br.net.sav.utils.ToastUtils;

import static br.net.sav.ComboController.comboUtils.DescontoComboUtils.ADICIONAR;
import static br.net.sav.ComboController.comboUtils.DescontoComboUtils.setComboMsg;
import static br.net.sav.ComboController.comboUtils.DialogAlertComboUltils.CONDICAO_COMBO;
import static br.net.sav.ComboController.comboUtils.DialogAlertComboUltils.CONDICAO_COMBO_ATIVADO;
import static br.net.sav.atacadomaringa.ComboAdapter.IdComboAtivado;
import static br.net.sav.atacadomaringa.ComboAdapter.acaoCombo;
import static br.net.sav.util.StringUtils.converterParaMaiusculo;
import static br.net.sav.util.StringUtils.tirarAcentos;
import static br.net.sav.utils.ShearedPreferenceUltils.getFiltroItens;

@SuppressLint("DefaultLocale")
public class ItemTabActivity extends Activity implements AdapterView.OnItemClickListener {
    static final String TAG = ItemDigitacaoDAO.class.getSimpleName();

    public static final String INTENT = "ItemTabActivity";
    public static final String ITENSPEDIDO_SERIALIZAR = "itenspedido.srl";
    public static final String ITENSPEDIDO_SERIALIZAR_ALTERAR = "itenspedidoalt.srl";

    private ItemDigitacaoAdapter adapter;

    public static List<ItemDigitacao> itemCompleto;
    public static List<ItemDigitacao> itemFiltrado;
    public static List<ItemDigitacao> itemAux;
    public static List<ItemDigitacao> itemPedido;
    private List<ItemDigitacao> itensFiltradoVendidos;
    public static boolean isZerarDesconto = false;

    private Pedido pedido;
    private Pedido pedidoAlteracao;
    private Parametro parametro;
    private TipoPedido tipoPedido;
    private Condicao condicao;
    private Cliente cliente;
    private Tabela tabela;
    private ListView listView;
    FloatingActionMenu menu_floating;
    FloatingActionButton menu_item_filtro, menu_item_legenda, menu_item_remover_filtro, menu_item_save;

    private OrdemListaProdutos ordemListaProdutos;

    @SuppressWarnings("unused")
    private int savedIndex, savedY, position, ultimoFiltroMarca;
    private boolean vendidos, isOperacaoTroca;
    private double valorPedidoSalvo, ultimoDesconto;
    public static boolean mIsInicializandoEstruturas = false;

    private SharedPreferences prefs;
    private List<RestricaoTCT> listaDeRestricoesTCT;

    private boolean cbNormalAnterior = true;
    private boolean cbProdutosNovosAnterior = true;
    private boolean cbPromocoesAnterior = true;
    private boolean cbRecemRecebidosAnterior = true;
    private final double desconto_negativo = -299.99;
    private boolean mModificarValoresEditText = false;
    private String percRentabilidade = "0";
    private Pedido pedidoVinculado;
    private List<RegraAplicavel> combosValidos;
    private List<ComboXProduto> comboXProdutos;
    private boolean ckedVendidos = false;
    public static boolean marcaAposSincronizacao = false;
    private long idMarca = 0;
    private Spinner spnMarca1;
    private CheckBox cbVendidos;
    private EditText edtPesquisa;
    private boolean filtroPesquisa;
    private String regex = ".*[a-zA-Z].*";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_tab_activity);
        listView = (ListView) findViewById(R.id.list_itemTab);

        Utils.naoExibirTeclado(this);
        ordemListaProdutos = OrdemListaProdutos.PRODUTOS_NOVOS;
        prefs = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE);

        try {
            ultimoDesconto = 0.0;
            ultimoFiltroMarca = 0;
            ckedVendidos = false;
            filtroPesquisa = false;
            pedido = (Pedido) Utils.recuperarObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR);
            pedidoAlteracao = new PedidoDAO(getBaseContext(), null).get(pedido.getIDPedido(), null);
            cliente = new ClienteDAO(getBaseContext(), null).get(pedido.getIDCliente());
            tipoPedido = new TipoPedidoDAO(getBaseContext(), null).get(pedido.getIDTPedido());
            condicao = new CondicaoDAO(getBaseContext(), null).get(pedido.getIDCondicao());
            tabela = new TabelaDAO(getBaseContext(), null).get(pedido.getIDTabela());
            parametro = new ParametroDAO(getBaseContext(), null).get();
            combosValidos = Combo.buscarCombos(getBaseContext(), pedido);
            comboXProdutos = RegraCompra.buscarProdutosCombo(getBaseContext());
            geraPedidoVinculadoBonificacao();

        } catch (Exception e) {
            Log.e("ItemTabActivity", e.getMessage());
        }
        inicializarEstruturaDigitacao();

        listView = (ListView) findViewById(R.id.list_itemTab);
        onClickFloatingActionButton();
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        if (edtPesquisa != null) {
            edtPesquisa.setFocusable(false);
        }
        super.onResume();

        if (!mIsInicializandoEstruturas) {
            reapurarPrecos();
        }
        setarAbaDeComboComoVisivel();
    }

    @Override
    protected void onStart() {
        if (edtPesquisa != null) {
            edtPesquisa.setFocusable(false);
        }
        super.onStart();
    }

    private void setarAbaDeComboComoVisivel() {
        List<RegraAplicavel> comboValidos = new ComboControle().getComboValidos(this, cliente, new RegraComboFactory());
        if (!comboValidos.isEmpty()) {
            PedidoTabHost parent = (PedidoTabHost) getParent();
            parent.getTabHost().getTabWidget().getChildAt(2).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        confirmaSaida();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        this.position = position;

        view.setSelected(true);
        showCustomDialog(position);
    }

//	@Override
//	protected void onListItemClick(ListView l, View v, int position, long id) {
//		super.onListItemClick(l, v, position, id);
//		this.position = position;
//
//		v.setSelected(true);
//		showCustomDialog(position);
//	}

    @Override
    protected void onPause() {
        super.onPause();

        savedIndex = listView.getFirstVisiblePosition();
        View v1 = listView.getChildAt(0);
        savedY = (v1 == null) ? 0 : v1.getTop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pedido_itens, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem mi = menu.findItem(R.idMenuPedidoItens.vendidos);
        if (vendidos) {
            mi.setTitle("Ver Todos");
            mi.setIcon(R.drawable.list_all);
        } else {
            mi.setTitle(R.string.so_vendidos);
            mi.setIcon(R.drawable.shopping_cart_menu);
        }
        return true;
    }

    private void sharedPreference(String keyChild, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE).edit();
        editor.putBoolean(keyChild, value);
        editor.apply();
    }

    private boolean isTipoPedidoValido() {
        return prefs.getBoolean("clicouTipoPedido", false) && !(new TipoPedidoDAO(this, null).get(pedido.getIDTPedido()).getDescricao().toUpperCase().contains("MUDAR"));
    }

    private boolean isCondicaoPagamentoValido() {
        return prefs.getBoolean("clicouCondPagamento", false) && !(new CondicaoDAO(this, null).get(pedido.getIDCondicao()).getDescricao().toUpperCase().contains("MUDAR"));
    }

    private boolean isFormaPagamentoValido() {
        return prefs.getBoolean("clicouFormaPagamento", false) && !(new FormaPagamentoDAO(this, null).get(pedido.getIDFormaPagamento()).getDescricao().toUpperCase().contains("MUDAR")
                || pedido.isAlterarPedido() && (new FormaPagamentoDAO(this, null).get(pedido.getIDFormaPagamento()).getDescricao().toUpperCase().contains("MUDAR")));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.idMenuPedidoItens.gravar:
                salvarPedido();

                return true;
            case R.idMenuPedidoItens.sair:
                confirmaSaida();

                return true;
//            case R.idMenuPedidoItens.filtro:
//                showFiltroDialog();
//
//                return true;
//            case R.idMenuPedidoItens.rFiltro:
//                ultimoFiltroMarca = 0;
//                removerFiltro();
//
//                return true;
            case R.idMenuPedidoItens.vendidos:
                if (!vendidos && itemPedido.size() == 0) {
                    Toast.makeText(getBaseContext(), R.string.nao_existem_tens_vendidos_nesse_pedido, Toast.LENGTH_SHORT).show();
                    return true;
                }

                vendidos = !vendidos;
                atualizaAdapter();
                return true;

            case R.idMenuPedidoItens.calculadora:
                Utils.iniciarCalculadora(getBaseContext());

                return true;

//            case R.idMenuPedidoItens.legenda:
//                showDialogLegenda();
//                return true;
            default:
                return true;
        }
    }

    private void salvarPedido() {
        if (isTipoPedidoValido()) {
            if (isCondicaoPagamentoValido()) {
                if (isFormaPagamentoValido()) {
                    if (isSemRestricaoTCT()) {
                        gravarPedido();
                    }
                } else {
                    Toast.makeText(this, R.string.selecione_forma_pagamento, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, R.string.selecione_condicao_pagamento, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.selecione_tipo_pedido, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("ShowToast")
    private boolean isSemRestricaoTCT() {
        listaDeRestricoesTCT = new RestricaoTCTDAO(getBaseContext(), null).getAll();
        boolean retorno = true;

        if (listaDeRestricoesTCT != null && listaDeRestricoesTCT.size() > 0) {
            for (int index = 0; index < listaDeRestricoesTCT.size(); index++) {

                //Verifica se esta condi��o de pagamento possui o valor 0 para tipo de pedido (0 = todos os tipos de pedido)
                // ou se o tipo de pedido em quest�o � igual ao tipo de pedido do atual pedido
                if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == 0
                        || listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {

                    //Verifica se esta restri��o possui o valor 0 para a condi��o de pagamento (0 = todos as condi��es de pagamento)
                    // ou se essa condi��o de pagamento em quest�o � igual a condi��o de pagamento do pedido atual
                    if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == 0
                            || listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {

                        //Verifica se esta restri��o possui o valor 0 para a forma de pagamento (0 = todos as formas de pagamento)
                        // ou se essa forma de pagamento em quest�o � igual a forma de pagamento do pedido atual
                        if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == 0
                                || listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDFormaPagamento()) {
                            if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {
                                if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {
                                    Toast.makeText(this, R.string.tipo_pedido_nao_pode_ser_associado_condicao_pagamento, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDTPedido()) {
                                    Toast.makeText(this, R.string.tipo_pedido_nao_pode_ser_associado_forma_pagamento, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == 0
                                        && listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == 0) {
                                    Toast.makeText(this, R.string.tipo_pedido_invalido, Toast.LENGTH_LONG).show();
                                }
                            } else if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {
                                if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {
                                    Toast.makeText(this, R.string.condicacao_pagamento_nao_pode_ser_associado_tipo_pedido, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDFormaPagamento()) {
                                    Toast.makeText(this, R.string.condicacao_pagamento_nao_pode_ser_associado_forma_pagamento, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(this, R.string.forma_pagamento_invalida_tct, Toast.LENGTH_LONG).show();
                            }
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private void verificaIsOperacaoTroca() {
        isOperacaoTroca = new TabelaPrecoOperacaoTrocaDAO(getBaseContext(), null).isOperacaoTroca(tipoPedido);
    }

    @SuppressLint("LongLogTag")
    public void reapurarPrecos() {
        try {
            pedido = (Pedido) Utils.recuperarObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR);
            TipoPedido tipoPedidoAux = new TipoPedidoDAO(getBaseContext(), null).get(pedido.getIDTPedido());
            if (tipoPedidoAux != null && tipoPedidoAux.getIdTipoPedido() != tipoPedido.getIdTipoPedido()) {
                tipoPedido = tipoPedidoAux;
                pedido.setIDTPedido(tipoPedido.getIdTipoPedido());
            }

            Tabela tabelaAux = new TabelaDAO(getBaseContext(), null).get(pedido.getIDTabela());
            Condicao condicaoAux = new CondicaoDAO(getBaseContext(), null).get(pedido.getIDCondicao());

            if (mudouTabelaPrecoOuCondicaoPagamento(tabelaAux, condicaoAux) || mudouCondicaoPagamentoPedidoAlteracao() || acaoCombo) {
                if (pedidoAlteracao != null) {
                    pedidoAlteracao.setIDCondicao(condicao.getIdCondicao());
                }
                tabela = tabelaAux;
                condicao = condicaoAux;


                inicializarEstruturaDigitacao();

            }
        } catch (Exception e) {
            Log.e("ItemTabActivity.reapurarPrecos", e.getMessage());
        }
    }

    private void recalcularPreco() {
        for (int i = 0; i < itemPedido.size(); i++) {
            ItemDigitacao itemDigitacao = itemPedido.get(i);

            retirarProdutoPedido(itemDigitacao);

            if (!(itemDigitacao.getItemComboAtivado() > 0)) {
                itemDigitacao.setValorUnitOriginal(getNovoValorOriginal(itemDigitacao));
                itemDigitacao.setDesconto(getZerarDesconto(itemDigitacao));
                itemDigitacao.setValorUnit(Utils.arredondar(itemDigitacao.getValorUnitOriginal() * (1 - (itemDigitacao.getDesconto() / 100)), 2));

                itemDigitacao.setValorBruto(itemDigitacao.getValorUnitOriginal() * itemDigitacao.getQuantidade());
                itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade());

                if (itemDigitacao.getPrecoCustoProduto() > 0) {
                    itemDigitacao.setRentabilidade(MathUtils.arredondar((itemDigitacao.getValorUnitOriginal() - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
                }

                itemDigitacao.setValorUtilizaFlex(0.0);
                itemDigitacao.setValorGeraFlex(0.0);
            }

            inserirProdutoPedido(itemDigitacao);
            acaoCombo = false;
        }

        if (itemPedido.isEmpty()) {
            if (pedido != null) {
                pedido.zerar();
            }
            if (pedidoAlteracao != null) {
                pedidoAlteracao.zerar();
            }
            if(isZerarDesconto && !tabela.isPermiteDescontoPorTabela()) {
                ToastUtils.mostrarMensagem(getBaseContext(),getString(R.string.itens_removidos));
            }
        }

        Utils.serializarObjeto(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        Utils.serializarObjeto(getBaseContext(), ITENSPEDIDO_SERIALIZAR, itemPedido);
        // }
        isZerarDesconto = false;
    }

    private void verificarExisteItemTabelaPreco() {
        if (isZerarDesconto) {
            List<ItemDigitacao> itensPedidoExistentes = new ArrayList<>();
            itemPedido = new ArrayList<>();
            for (int i = 0; i < itemCompleto.size(); i++) {
                for (int e = 0; e < itemPedido.size(); e++) {
                    if (itemCompleto.get(i).getProduto().getIdProduto() == itensPedidoExistentes.get(e).getProduto().getIdProduto()) {
                        itensPedidoExistentes.add(itensPedidoExistentes.get(e));
                    }

                }
            }
        }
    }


    private double getZerarDesconto(ItemDigitacao itemDigitacao) {
        return isZerarDesconto ? 0 : itemDigitacao.getDesconto();
    }

    private boolean mudouCondicaoPagamentoPedidoAlteracao() {
        return pedidoAlteracao != null && pedidoAlteracao.getIDCondicao() != condicao.getIdCondicao();
    }

    private boolean mudouTabelaPrecoOuCondicaoPagamento(Tabela tabelaAux, Condicao condicaoAux) {
        return tabelaAux.getIdTabela() != tabela.getIdTabela() || condicaoAux.getIdCondicao() != condicao.getIdCondicao();
    }


    private double getNovoValorOriginal(ItemDigitacao item) {
        for (ItemDigitacao itemNovaLista : itemCompleto) {
            if (itemNovaLista.getProduto().getIdProduto() == item.getProduto().getIdProduto()) {
                return itemNovaLista.getValorUnitOriginal();
            }
        }

        return item.getValorUnitOriginal();
    }

    @SuppressLint("NewApi")
    public void inicializarEstruturaDigitacao() {
        new InicializacaoEstruturaTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private boolean valorFracionado(double qtdeDigitada) {
        return (qtdeDigitada % (int) qtdeDigitada) > 0;
    }

    public void showCustomDialog(final int position) {
        final ItemDigitacao itemDigitacao = itemAtual(position);

        savedIndex = listView.getFirstVisiblePosition();
        View v1 = listView.getChildAt(0);
        savedY = (v1 == null) ? 0 : v1.getTop();

        if (!itemPedido.isEmpty() && !pedido.isAlterarPedido()) {
            for (ItemDigitacao item : itemPedido) {
                if (item.getProduto().getIdProduto() == itemDigitacao.getProduto().getIdProduto()) {
                    itemDigitacao.setQuantidadeItemLimiteCombo(item.getQuantidadeItemLimiteCombo());
                    itemDigitacao.setPercentualDescontoCombo(item.getPercentualDescontoCombo());
                    itemDigitacao.setItemComboAtivado(item.getItemComboAtivado());
                    itemDigitacao.setDesconto(item.getDesconto());
                }
            }
        }

        final Dialog dialog = new Dialog(this);
        dialog.setTitle(String.format("%d - %s", itemDigitacao.getProduto().getIdProduto(), itemDigitacao.getProduto().getDescricao()));
        dialog.setContentView(R.layout.popup_layout);

        final EditText txvQtde = (EditText) dialog.findViewById(R.idPopup.txtQuantidade);
        final EditText txvValor = (EditText) dialog.findViewById(R.idPopup.txtValor);
        final EditText txtDesconto = (EditText) dialog.findViewById(R.idPopup.txtPercDesconto);
        final EditText txvTotalItem = (EditText) dialog.findViewById(R.idPopup.txtTotalItem);
        final Button btnMais = (Button) dialog.findViewById(R.idPopup.btnMais);
        final Button btnMenos = (Button) dialog.findViewById(R.idPopup.btnMenos);
        final Button btnVoltaPreco = (Button) dialog.findViewById(R.idPopup.btnVoltaPreco);
        final Button btnOk = (Button) dialog.findViewById(R.idPopup.btnOk);
        final Button btnFoto = (Button) dialog.findViewById(R.idPopup.btnFoto2);
        final TextView txvDesconto = (TextView) dialog.findViewById(R.idPopup.txvPercDesconto);
        final Button btnCalculadora = (Button) dialog.findViewById(R.id.btnCalculadora);

        final Button btnMaisDesconto = (Button) dialog.findViewById(R.idPopup.btnMaisDesconto);
        final Button btnMenosDesconto = (Button) dialog.findViewById(R.idPopup.btnMenosDesconto);

        final TextView txvValorPago = (TextView) dialog.findViewById(R.id.popupLayoutValorPago);
        final TextView txvQuantidadeComprada = (TextView) dialog.findViewById(R.id.popupLayoutQuantidadeComprada);

        Cursor cursor = null;
        SQLiteDatabase db = new DBHelper(this).getReadableDatabase();
//        Produto produto = new Produto();
//        ProdutoDAO produtoDao = new ProdutoDAO(this, db);
        String sql = this.getString(R.string.sql_estrutura_item_digitacao_pedido);

        String[] args = new String[2];
        args[0] = String.valueOf(pedido.IDCliente);
        args[1] = String.valueOf(itemDigitacao.getProduto().getIdProduto());

        cursor = db.rawQuery(sql, args);

        while (cursor.moveToNext()) {
            try {
                if (!cursor.isNull(cursor.getColumnIndex("Quantidade"))) {
                    txvQuantidadeComprada.setText(cursor.getString(cursor.getColumnIndex("Quantidade")));
                }
                if (!cursor.isNull(cursor.getColumnIndex("ValorUnitPraticado"))) {
                    txvValorPago.setText(cursor.getString(cursor.getColumnIndex("ValorUnitPraticado")));
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }

        if (txvValorPago.getText().toString().isEmpty() || txvQuantidadeComprada.getText().toString().isEmpty()) {
            txvValorPago.setText(R.string.item_tab_activity_sem_venda);
            txvQuantidadeComprada.setText(R.string.item_tab_activity_sem_venda);
        }

        dialog.setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
//                    if (itemDigitacao.getQuantidade() == 0) {
//                        txtDesconto.setText(String.format("%.4f", ultimoDesconto).replace(",", "."));
//                    }

            }
        });

        txvTotalItem.setText(String.format("%.4f", itemDigitacao.getValorLiquido()));

        if (itemDigitacao.getQuantidade() > 0.0 && itemDigitacao.getItemComboAtivado() > 0) {
            //   itemDigitacao.setDesconto(itemDigitacao.getDesconto());
            txvQtde.setText(String.valueOf(itemDigitacao.getQuantidade()));
            btnVoltaPreco.setEnabled(false);
            txvQtde.setEnabled(false);
            txtDesconto.setEnabled(false);
            txvValor.setEnabled(false);
            btnMaisDesconto.setEnabled(false);
            btnMenosDesconto.setEnabled(false);
        } else if (itemDigitacao.getQuantidade() > 0.0) {
            txvQtde.setEnabled(true);
            txvDesconto.setEnabled(true);
            txvValor.setEnabled(true);
            btnMaisDesconto.setEnabled(true);
            btnMenosDesconto.setEnabled(true);
            btnVoltaPreco.setEnabled(true);
            txvQtde.setSelectAllOnFocus(false);
            txvQtde.setText(String.valueOf(itemDigitacao.getQuantidade()));
        } else if (txvQtde.getText().toString().equals("")) {
            txvQtde.setSelectAllOnFocus(true);
            txvQtde.setText("0");
        }

        txvQtde.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txvQtde.removeTextChangedListener(this);

                if (aoAlterarCampoQuantidade(itemDigitacao, txvValor, txvQtde, txvTotalItem)) {
                    if (itemDigitacao.getItemComboAtivado() == 0) {
                        btnMaisDesconto.setEnabled(true);
                    }
                } else {
                    if (txvQtde.getText().toString().equals("0")) {
                        txtDesconto.setText("0.00");
                    }

                    btnMaisDesconto.setEnabled(false);
                }

                txvQtde.addTextChangedListener(this);
            }
        });


        txvQtde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getQuantidadeDigitadaNaTela(txvQtde) == 0d) {
                    EditText edt = ((EditText) view);
                    edt.clearFocus();
                    edt.requestFocus();
                }
            }
        });

        if (itemDigitacao.isPossuiFoto()) {
            btnFoto.setVisibility(View.VISIBLE);
        } else {
            btnFoto.setVisibility(View.INVISIBLE);
        }

        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carregarFoto(position);
            }
        });
        btnCalculadora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.iniciarCalculadora(getBaseContext());
            }
        });

        txvValor.setText(String.format("%.2f", itemDigitacao.getValorUnit()).replace(",", "."));
        txvValor.setEnabled(parametro.isPermiteAlterarPreco() && itemDigitacao.getItemComboAtivado() > 0 ? false : true);
        txtDesconto.setText(String.format("%.2f", itemDigitacao.getDesconto()).replace(",", "."));
        txtDesconto.setEnabled(parametro.isPermiteAlterarPreco() && itemDigitacao.getItemComboAtivado() > 0 ? false : true);

        if (parametro.isPermiteAlterarPreco()) {
            txvDesconto.setVisibility(View.VISIBLE);
        } else {
            txvDesconto.setVisibility(View.GONE);
        }

        txtDesconto.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtDesconto.removeTextChangedListener(this);
                alterarValoresDoDesconto(itemDigitacao, txtDesconto, txvValor, txvQtde, txvTotalItem);
                txtDesconto.addTextChangedListener(this);
            }
        });

        txvValor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                txvValor.removeTextChangedListener(this);
                // formataCampoValor(s, txvValor);
                calcularValoresDoValor(itemDigitacao, txtDesconto, txvValor, txvQtde, txvTotalItem);
                txvValor.addTextChangedListener(this);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double dblValorDigitado;
                String sQtdeDigitada = txvQtde.getText().toString();

                if (sQtdeDigitada.trim().equals(""))
                    sQtdeDigitada = "0";

                // Recuperar qtde digitada
                double qtdeDigitada = 0;
                try {
                    qtdeDigitada = Utils.arredondar(Double.parseDouble(sQtdeDigitada), 2);
                    if (isOperacaoTroca && valorFracionado(qtdeDigitada)) {
                        Toast.makeText(getBaseContext(), R.string.a_qnt_nao_pode_ser_fracionada_em_uma_operacao_troca, Toast.LENGTH_LONG).show();
                        return;
                    }
                } catch (NumberFormatException e) {
                    Toast.makeText(getBaseContext(), R.string.quantidade_invalida, Toast.LENGTH_SHORT).show();
                    return;
                }

                // Recuperar pre?o digitado
                String sValor = txvValor.getText().toString();
                if (sValor.trim().equals(""))
                    sValor = String.valueOf(itemDigitacao.getValorUnitOriginal());
                dblValorDigitado = Double.parseDouble(sValor);

                // Recuperar Percentual de Desconto
                String sDesconto = txtDesconto.getText().toString();
                if (sDesconto.trim().equals("")) {
                    sDesconto = "0.00";
                }
                double dblDescontoDigitado = Double.parseDouble(sDesconto);

//				if(itemDigitacao.getQuantidade() == 0)
//					ultimoDesconto = dblDescontoDigitado ;
                if (qtdeDigitada > 0.0) {
                    // Valida??es da Quantidade

//                    if (Double.valueOf(sValor) != itemDigitacao.getValorUnitOriginal()) {
//                        dblDescontoDigitado = calcularDesconto(itemDigitacao, sValor, 2);
//                    }

                    if (!itemDigitacao.getProduto().isPermiteVender()) {
                        Toast.makeText(getBaseContext(), R.string.produto_sem_permissao_de_venda, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if ((!tipoPedido.getDescricao().toUpperCase().contains("BRINDE")) && (!tipoPedido.getDescricao().toUpperCase().contains("BONIFIC")) && (itemDigitacao.getProduto().getMultiplo() > 0) && !isOperacaoTroca && ((qtdeDigitada % itemDigitacao.getProduto().getMultiplo()) != 0)) {
                        Toast.makeText(getBaseContext(), String.format(getString(R.string.a_quantidade_deve_ser_multipla_de), itemDigitacao.getProduto().getMultiplo()), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if ((qtdeDigitada * itemDigitacao.getProduto().getQuantidadeEmbalagem()) > Double.parseDouble(itemDigitacao.getProduto().getSaldoEstoque())) {
                        Toast.makeText(getBaseContext(), String.format("Saldo de estoque insuficiente. Estoque: %s", itemDigitacao.getProduto().getSaldoEstoque()), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (qtdeDigitada > 9999) {
                        Toast.makeText(getBaseContext(), R.string.quantidade_deve_ser_entre, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (itemDigitacao.getQuantidade() == 0 && itemPedido.size() == parametro.getQtdeMaximaItens() && parametro.getQtdeMaximaItens() > 0) {
                        Toast.makeText(getBaseContext(), R.string.quantidade_maxima_itens_atingido, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // Valida??es do Desconto
                    Double descontoPermitido = 0.0;
                    if (!parametro.isPercDescontoProdutoGeraVerba()) {
                        if ((itemDigitacao.getProduto().getStatus().compareTo("P") == 0) && (dblDescontoDigitado > 0 || dblValorDigitado > itemDigitacao.getValorUnitOriginal())) {
                            Toast.makeText(getBaseContext(), R.string.nao_permitido_conceder_desconto_acrescimo_prod_promocao, Toast.LENGTH_LONG).show();
                            return;
                        }

                        SituacaoEspecial esp = new SituacaoEspecialDAO(getBaseContext(), null).get(itemDigitacao.getProduto().getIdProduto());
                        if (esp != null) {
                            if ((esp.getQuantidade7() > 0) && (qtdeDigitada >= esp.getQuantidade7()))
                                descontoPermitido = esp.getDescontoMaximo7();
                            else if ((esp.getQuantidade6() > 0) && (qtdeDigitada >= esp.getQuantidade6()))
                                descontoPermitido = esp.getDescontoMaximo6();
                            else if ((esp.getQuantidade5() > 0) && (qtdeDigitada >= esp.getQuantidade5()))
                                descontoPermitido = esp.getDescontoMaximo5();
                            else if ((esp.getQuantidade4() > 0) && (qtdeDigitada >= esp.getQuantidade4()))
                                descontoPermitido = esp.getDescontoMaximo4();
                            else if ((esp.getQuantidade3() > 0) && (qtdeDigitada >= esp.getQuantidade3()))
                                descontoPermitido = esp.getDescontoMaximo3();
                            else if ((esp.getQuantidade2() > 0) && (qtdeDigitada >= esp.getQuantidade2()))
                                descontoPermitido = esp.getDescontoMaximo2();
                            else if ((esp.getQuantidade1() > 0) && (qtdeDigitada >= esp.getQuantidade1()))
                                descontoPermitido = esp.getDescontoMaximo1();
                        } else {
                            double percentualDescontoCombo = itemDigitacao.getPercentualDescontoCombo() < 0 ? itemDigitacao.getDesconto() : itemDigitacao.getPercentualDescontoCombo();
                            descontoPermitido = itemDigitacao.getProduto().getMaximoDesconto() + percentualDescontoCombo;
                        }

                        if (pedido.getIDTabela() == tabela.getIdTabela()) {
                            if (!tabela.isPermiteDescontoPorTabela() && dblDescontoDigitado > descontoPermitido) {
                                Toast.makeText(getBaseContext(), String.format(getString(R.string.desconto_nao_pemitido_para_esta_tabela)), Toast.LENGTH_SHORT).show();
                                return;
                            }

                        }

                        if (dblDescontoDigitado > descontoPermitido) {
                            Toast.makeText(getBaseContext(), String.format(getString(R.string.desconto_digitado_acima_do_permitido_maximo_permitido), descontoPermitido), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    Double valorDigitado = calculoValorDigitadoPermtido(itemDigitacao);
                    double percentualDescontoCombo = itemDigitacao.getPercentualDescontoCombo() < 0 ? itemDigitacao.getDesconto() : itemDigitacao.getPercentualDescontoCombo();

                    // Valida??es do Pre?o
                    if (dblDescontoDigitado > (itemDigitacao.getProduto().getMaximoDesconto() + percentualDescontoCombo) || dblDescontoDigitado < desconto_negativo) {
                        Toast.makeText(getBaseContext(), String.format(getString(R.string.valor_ultrapassa_o_maximo_de_desconto_possivel), itemDigitacao.getProduto().getMaximoDesconto()), Toast.LENGTH_SHORT).show();
                        return;
                    } else if (dblValorDigitado < valorDigitado) {
                        Toast.makeText(getBaseContext(), String.format(getString(R.string.valor_ultrapassa_o_maximo_de_desconto_possivel), itemDigitacao.getProduto().getMaximoDesconto()), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if ((dblValorDigitado < itemDigitacao.getValorUnitOriginal()) && (!itemDigitacao.getProduto().getControleGordura().equals("S")) && (tipoPedido.getAbaterSaldoVerba() == 2)) {
                        Toast.makeText(getBaseContext(), R.string.produtos_que_nao_controla_flex_nao_e_permitido_diminuir_o_seu_valor, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Double truncar = MathUtils.truncar(dblDescontoDigitado, 2);
                    venderProduto(itemDigitacao, qtdeDigitada, truncar, dblValorDigitado);

                    if (parametro.isControlaLimiteCredito()) {
                        Limite limite = new LimiteDAO(getBaseContext(), null).get(cliente.getIdCliente());
                        if ((limite.getLimiteDisponivel() + valorPedidoSalvo) < pedido.getTotalLiquido()) {
                            double valorLiquido = pedido.getTotalLiquido();
                            venderProduto(itemDigitacao, qtdeDigitada, dblDescontoDigitado, dblValorDigitado);

                            NumberFormat nf = NumberFormat.getCurrencyInstance();

                            Toast.makeText(getBaseContext(), String.format(getString(R.string.limite_de_credito_excedido_disponivel_valor_pedido), nf.format(limite.getLimiteDisponivel() + valorPedidoSalvo), nf.format(limite.getLimiteTotal()), nf.format(valorLiquido)), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }

                    if (itemDigitacao.getProduto().getEstoqueBaixo() == 1)
                        Toast.makeText(getBaseContext(), "Produto com ESTOQUE BAIXO", Toast.LENGTH_SHORT).show();
                    else if (itemDigitacao.getProduto().getEstoqueBaixo() == 2)
                        Toast.makeText(getBaseContext(), "Produto SEM ESTOQUE", Toast.LENGTH_SHORT).show();
                } else {
                    if (qtdeDigitada == 0)
                        itemDigitacao.setDesconto(0.00);

                    venderProduto(itemDigitacao, qtdeDigitada, itemDigitacao.getDesconto(), itemDigitacao.getValorUnitOriginal());
                }
                dialog.dismiss();
                atualizaCombo();
                atualizaAdapter();
                ativarCombo(itemDigitacao);
            }
        });


        btnMais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qtde = txvQtde.getText().toString();

                try {
                    if (qtde.equals("")) {
                        if (!isOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
                            qtde = String.valueOf(itemDigitacao.getProduto().getMultiplo());
                        else
                            qtde = "1.00";
                    } else {
                        if (itemDigitacao.getItemComboAtivado() == 0) {
                            btnMaisDesconto.setEnabled(true);
                        }
                        if (!isOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
                            qtde = String.format("%.2f", proximoMultiplo(Double.parseDouble(qtde.replace(",", ".")), itemDigitacao.getProduto().getMultiplo()));
                        else
                            qtde = String.format("%.2f", Double.parseDouble(qtde.replace(",", ".")) + 1);
                    }
                    qtde = qtde.replace(",", ".");
                    txvQtde.setText(qtde);
                    double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
                    txvTotalItem.setText(String.format("%.4f", Double.parseDouble(qtde.replace(",", ".")) * valor));
                } catch (Exception e) {
                    Log.e(getString(R.string.item_tab_activity_btn_mais_setonclicklistener), "setOnClickListener. " + e.getMessage());
                }
            }
        });

        //if (!txvQtde.getText().toString().equals(""))
        if (Double.parseDouble(txvQtde.getText().toString()) == 0.00)
            btnMaisDesconto.setEnabled(false);


        btnMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sQtde = txvQtde.getText().toString();

                try {

                    if (!sQtde.equals("")) {
                        double qtde = Double.parseDouble(sQtde.replace(",", "."));

                        if (qtde == 0) {
                            return;
                        }
                        double quantidadeCalculada = (qtde - 1) * itemDigitacao.verificaQuantidadeEmbalagem();
                        if (itemDigitacao.getQuantidadeItemLimiteCombo() > 0 && quantidadeCalculada < itemDigitacao.getQuantidadeItemLimiteCombo()) {
                            ToastUtils.mostrarMensagem(getBaseContext(), "Este item faz parte do Combo, caso queira alterar, tera que desativar o Combo!");
                            return;
                        }

                        if (!isOperacaoTroca && itemDigitacao.getProduto().getMultiplo() > 0)
                            sQtde = String.format("%.2f", multiploAnterior(qtde, itemDigitacao.getProduto().getMultiplo()));
                        else
                            sQtde = String.format("%.2f", qtde - 1);
                    }

                    if (Double.parseDouble(sQtde.replace(",", ".")) < 0) {
                        sQtde = "0.00";
                    }

                    sQtde = sQtde.replace(",", ".");
                    if (sQtde.equals("0.00")) {
                        txtDesconto.setText("0.00");
                        alterarValoresDoDesconto(itemDigitacao, txtDesconto, txvValor, txvQtde, txvTotalItem);
                        btnMaisDesconto.setEnabled(false);
                    }
                    txvQtde.setText(sQtde);
                    double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
                    txvTotalItem.setText(String.format("%.4f", Double.parseDouble(sQtde) * valor));
                } catch (Exception e) {
                    Log.e(getString(R.string.item_tab_activity_btnmenos_setonclicklistener), "setOnClickListener. " + e.getMessage());
                }
            }
        });

        btnMaisDesconto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sDesc = txtDesconto.getText().toString();
                String sqtd = txvQtde.getText().toString();

                try {

                    if (sDesc.equals("")) {
                        sDesc = "1.00";
                    } else {
                        sDesc = String.format("%.2f", Double.valueOf(sDesc) + 1);
                    }


                    sDesc = sDesc.replace(",", ".");
                    txtDesconto.setText(sDesc);

                    alterarValoresDoDesconto(itemDigitacao, txtDesconto, txvValor, txvQtde, txvTotalItem);

                    // double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
                    // txvTotalItem.setText(String.format("%.4f", Double.parseDouble(sDesc.replace(",", ".")) * valor));
                } catch (Exception e) {
                    Log.e(getString(R.string.item_tab_activity_btn_mais_setonclicklistener), "setOnClickListener. " + e.getMessage());
                }
            }
        });

        btnMenosDesconto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sDesc = txtDesconto.getText().toString();
                String sqtd = txvQtde.getText().toString();
                double sqtdd = Double.parseDouble(sqtd.replace(",", "."));

                try {
                    if (!sDesc.equals("")) {
                        double qtdeDesconto = Double.parseDouble(sDesc.replace(",", "."));

                        if (sqtdd == 0) {
                            return;
                        }
                        sDesc = String.format("%.2f", qtdeDesconto - 1);
                    }

//                    if (Double.parseDouble(sDesc.replace(",", ".")) < 0) {
//                        sDesc = "0.00";
//                    }

                    sDesc = sDesc.replace(",", ".");
                    txtDesconto.setText(sDesc);

                    alterarValoresDoDesconto(itemDigitacao, txtDesconto, txvValor, txvQtde, txvTotalItem);
                    // double valor = Double.parseDouble(txvValor.getText().toString().trim().replace(",", "."));
                    //  txvTotalItem.setText(String.format("%.4f", Double.parseDouble(sDesc) * valor));
                } catch (Exception e) {
                    Log.e(getString(R.string.item_tab_activity_btnmenos_setonclicklistener), "setOnClickListener. " + e.getMessage());
                }
            }
        });

        btnVoltaPreco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NumberFormat nf = NumberFormat.getCurrencyInstance();

                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.atencao))
                        .setMessage(String.format("Deseja realmente voltar o valor original do produto: %s", nf.format(itemDigitacao.getValorUnitOriginal())))
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                txvValor.setText(String.format("%.2f", itemDigitacao.getValorUnitOriginal()).replace(",", "."));
                                txtDesconto.setText("0.00");
                                dialog.dismiss();
                            }
                        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        dialog.show();
    }

    private void ativarCombo(final ItemDigitacao itemDigitacao) {
        final List<Combo> combos = new ArrayList<>();
        setarComboParaMensagem(itemDigitacao, combos);

        if (!combos.isEmpty()) {
            View content = getLayoutInflater().inflate(R.layout.combo_listview_dialog, null);
            final DialogComboAdapter dialogComboAdapter = new DialogComboAdapter(this, combos);

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.atencao)
                    .setView(content);

            ListView listView = inicializarComponentesComboMsg(content);
            Button btnAtivar = (Button) content.findViewById(R.id.buttonCombo);
            Button btnCancelar = (Button) content.findViewById(R.id.buttonCombo1);
            listView.setAdapter(dialogComboAdapter);
            if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                    == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                ToastUtils.tamanhoListViewDialogUtils(listView, 145);
            } else {
                ToastUtils.tamanhoListViewDialogUtils(listView, 85);
            }

            final AlertDialog alert = builder.create();
            btnAtivar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ativarComboPorMsg(combos, itemDigitacao, dialogComboAdapter, alert);

                }
            });

            btnCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();

                }
            });
            alert.show();
        }

    }

    private void setarComboParaMensagem(ItemDigitacao itemDigitacao, List<Combo> combos) {
        if (!combosValidos.isEmpty()) {
            for (RegraAplicavel combo : combosValidos) {
                RegraCompra compra = RegraCompra.buscaRegraCompra(getBaseContext(), combo.getIdCombo());
                if (compra.listIdCampo.contains(itemDigitacao.getProduto().getIdProduto())) {
                    if ((itemDigitacao.getQuantidade() * itemDigitacao.getProduto().getQuantidadeEmbalagem()) >= compra.getQuantidadeMinima()) {
                        validaComboAtivadoParaMsg(itemDigitacao, combos, combo);
                    }
                }
            }
        }
    }

    private void validaComboAtivadoParaMsg(ItemDigitacao itemDigitacao, List<Combo> combos, RegraAplicavel combo) {
        if (!pedido.getCombos().isEmpty() && itemDigitacao.getItemComboAtivado() > 0) {
            for (Combo comboAtivo : pedido.getCombos()) {
                RegraCompra compra2 = RegraCompra.buscaRegraCompra(getBaseContext(), comboAtivo.getIdCombo());
                if (comboAtivo.getIdCombo() != combo.getIdCombo() && compra2.listIdCampo.contains(itemDigitacao.getProduto().getIdProduto()) && itemDigitacao.getItemComboAtivado() == 1) {
                    setComboMsg(combos, combo, true);
                }
            }
        } else {
            setComboMsg(combos, combo, true);
        }
    }

    private void ativarComboPorMsg(List<Combo> combos, ItemDigitacao itemDigitacao, DialogComboAdapter dialogComboAdapter, AlertDialog alert) {
        RegraAplicavel reaCombo = null;
        boolean selecionarCombo = false;
        boolean condicaoCombo = false;
        for (Combo combo : combos) {
            if (combo.isComboAtivadoMsg()) {
                reaCombo = getVerificaSeComboIgual(reaCombo, combo);
                if (!pedido.getCombos().isEmpty()) {
                    for (Combo comboAtivado : pedido.getCombos()) {
                        if (comboAtivado.getIdCombo() != reaCombo.getIdCombo() && itemDigitacao.getItemComboAtivado() == 0) {
                            if (!RegraCompra.mesmaCondicaoPagamentoDoComboAtivado(getBaseContext(), (Combo) combo, comboAtivado)) {
                                condicaoCombo = true;
                                combo.setComboAtivadoMsg(false);
                                new DialogAlertComboUltils(ItemTabActivity.this)
                                        .msgAoAtivarComboComCondicaoDiferente(dialogComboAdapter, CONDICAO_COMBO_ATIVADO);
                                return;
                            }
                        }
                    }
                }
                if (reaCombo.getIdCombo() == combo.getIdCombo() && itemDigitacao.getItemComboAtivado() == 0) {
                    if (!pedido.mesmaCondicaoPagamento(getBaseContext(), (Combo) combo)) {
                        condicaoCombo = true;
                        combo.setComboAtivadoMsg(false);
                        new DialogAlertComboUltils(ItemTabActivity.this)
                                .msgAoAtivarComboComCondicaoDiferente(dialogComboAdapter, CONDICAO_COMBO);
                        return;
                    }

                }
                if (!condicaoCombo) {
                    selecionarCombo = isSelecionarCombo(itemDigitacao, alert, (Combo) reaCombo, combo);
                }

            }
        }

        if (!selecionarCombo) {
            ToastUtils.mostrarMensagem(ItemTabActivity.this, "Selecione um combo para ativar!");
        }
    }

    private RegraAplicavel getVerificaSeComboIgual(RegraAplicavel reaCombo, Combo combo) {
        for (RegraAplicavel reCombo : combosValidos) {
            if (reCombo.getIdCombo() == combo.getIdCombo()) {
                reaCombo = reCombo;
            }
        }
        return reaCombo;
    }

    private boolean isSelecionarCombo(ItemDigitacao itemDigitacao, AlertDialog alert, Combo reaCombo, Combo combo) {
        boolean selecionarCombo;
        IdComboAtivado = combo.getIdCombo();
        new DescontoComboUtils(ItemTabActivity.this,
                reaCombo, pedido, itemPedido).DescontoDoComboNosItens(itemDigitacao, tipoPedido, ADICIONAR);
        acaoCombo = true;
        pedido.addCombo(reaCombo);
        selecionarCombo = true;
        Utils.serializarObjeto(ItemTabActivity.this, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        reapurarPrecos();
        alert.dismiss();
        ToastUtils.mostrarMensagem(ItemTabActivity.this, "Combo Ativado Com Sucesso!");
        return selecionarCombo;
    }

    private ListView inicializarComponentesComboMsg(View content) {
        ListView listView = (ListView) content.findViewById(R.id.list_dialog_combo);
        TextView textView = (TextView) content.findViewById(R.id.txv_combo1);
        View viewColor1 = (View) content.findViewById(R.id.color_view1);
        viewColor1.setBackgroundColor(Color.parseColor("#1C86EE"));
        View viewColor2 = (View) content.findViewById(R.id.color_view2);
        viewColor2.setBackgroundColor(Color.parseColor("#1C86EE"));
        textView.setText("Combo(s) Desbloqueado(s), selecione para ativar");
        textView.setTextColor(Color.parseColor("#1C86EE"));
        return listView;
    }

    private void atualizaCombo() {
        if (!pedido.isAlterarPedido()) {
            List<Combo> combosParaRemover = new ArrayList<Combo>();
            for (Combo combo : pedido.getCombos()) {
                if (!combo.satisfazRegraCompra(itemPedido)) {
                    combosParaRemover.add(combo);
                }
            }
            pedido.getCombos().removeAll(combosParaRemover);
            Utils.serializarObjeto(this, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        }
    }

    private double calculoValorDigitadoPermtido(ItemDigitacao itemDigitacao) {
        double percentualDescontoCombo = itemDigitacao.getPercentualDescontoCombo() < 0 ? itemDigitacao.getDesconto() : itemDigitacao.getPercentualDescontoCombo();
        double valorDesconto = MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() * (itemDigitacao.getProduto().getMaximoDesconto() + percentualDescontoCombo / 100), 4);
        double valorUnitario = MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() - valorDesconto, 4);
        return Double.valueOf(String.format("%.2f", valorUnitario).replace(",", "."));
    }


    private void calcularValoresDoValor(ItemDigitacao itemDigitacao, EditText txtDesconto, EditText txvValor, EditText txvQtde, EditText txvTotalItem) {
        double quantidade = 0;
        String stringValorUnitario = MathUtils.getText(txvValor);
        if (!txvQtde.getText().toString().isEmpty())
            quantidade = Double.parseDouble(txvQtde.getText().toString());
        double valorUnitarioDigitado = 0;
        double percentualDesconto = 0;
        double totalItem = 0;
        double valorDesconto = 0;

        if (!mModificarValoresEditText) {
            mModificarValoresEditText = true;

            try {
                valorUnitarioDigitado = setValoresDigitado(stringValorUnitario);
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Valor Invalido", Toast.LENGTH_LONG).show();
                return;
            }
            valorDesconto = MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() - valorUnitarioDigitado, 4);
            percentualDesconto = MathUtils.arredondar((valorDesconto * 100) / itemDigitacao.getValorUnitOriginal(), 3);
            totalItem = MathUtils.arredondar(valorUnitarioDigitado * quantidade, 4);

            txvTotalItem.setText(String.format("%.4f", totalItem));
            txtDesconto.setText(String.format("%.2f", percentualDesconto).replace(",", "."));

            setCoresEditText(itemDigitacao, txtDesconto, percentualDesconto);

        }
        mModificarValoresEditText = false;
    }

    private double setValoresDigitado(String string) {
        double valoresDigitado;
        if (string.startsWith(".")) {
            String stringDecimais = string.length() > 1 ? string.substring(1, string.length()) : "0";

            valoresDigitado = (!stringDecimais.contains(".") && MathUtils.contemSomenteNumeros(stringDecimais)) ?
                    Double.parseDouble(String.format("0.%s", stringDecimais)) : 0;
        } else {
            valoresDigitado = string.equals("") ? 0 : Double.parseDouble(string);
        }
        return valoresDigitado;
    }

    private void alterarValoresDoDesconto(ItemDigitacao itemDigitacao, EditText txtDesconto, EditText txvValor, EditText txvQtde, EditText txvTotalItem) {
        double quantidade = 0;
        String stringPercentualDesconto = MathUtils.getText(txtDesconto);
        if (!txvQtde.getText().toString().isEmpty())
            quantidade = Double.parseDouble(txvQtde.getText().toString());
        double percentualDescontoDigitado = 0;
        double valorDesconto = 0;
        double totalItem = 0;
        double valorUnitario = 0;

        if (!mModificarValoresEditText) {
            mModificarValoresEditText = true;

            try {
                percentualDescontoDigitado = setValoresDigitado(stringPercentualDesconto);
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), "Desconto inválido!", Toast.LENGTH_LONG).show();
                return;
            }

            valorDesconto = MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() * (percentualDescontoDigitado / 100), 4);
            valorUnitario = MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() - valorDesconto, 4);
            totalItem = MathUtils.arredondar(valorUnitario * quantidade, 4);

            txvTotalItem.setText(String.format("%.4f", totalItem));
            txvValor.setText(String.format("%.2f", valorUnitario).replace(",", "."));

            setCoresEditText(itemDigitacao, txvValor, percentualDescontoDigitado);

        }
        mModificarValoresEditText = false;

    }

    private double getQuantidadeDigitadaNaTela(EditText txvQtde) {
        double retorno = 0d;
        try {
            String stringQuantidadeDigitada = Utils.getText(txvQtde);
            if (stringQuantidadeDigitada.startsWith(".")) {
                String stringDecimais = stringQuantidadeDigitada.length() > 1 ? stringQuantidadeDigitada.substring(1, stringQuantidadeDigitada.length()) : "0";
                retorno = (!stringDecimais.contains(".") && Utils.contemSomenteNumeros(stringDecimais)) ?
                        Double.parseDouble(String.format("0.%s", stringDecimais)) : 0;
            } else {
                //retorno = stringQuantidadeDigitada.equals("") ? 0 : Double.parseDouble(stringQuantidadeDigitada.replace(",", "."));
                retorno = stringQuantidadeDigitada.equals("") ? 0 : Double.parseDouble(stringQuantidadeDigitada);
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
        return retorno;
    }

    private boolean aoAlterarCampoQuantidade(ItemDigitacao itemDigitacao, EditText txvValor, EditText txvQtde, EditText txvTotalItem) {
        short multiplo = itemDigitacao.getProduto().getMultiplo() > 1 ? itemDigitacao.getProduto().getMultiplo() : 1;
        double quantidadeDigitada = getQuantidadeDigitadaNaTela(txvQtde);
        double valorUnitario = 0;

        if (!mModificarValoresEditText) {
            mModificarValoresEditText = true;

            if (txvValor == null || Utils.getText(txvValor).isEmpty()) {
                valorUnitario = itemDigitacao.getValorUnit();
            } else {
                //valorUnitario = Double.parseDouble(ManipulacaoComponentes.getText(mEdtValor));
                //valorUnitario = Double.parseDouble(mEdtValor.toString());
                valorUnitario = itemDigitacao.getValorUnitOriginal();
            }
            if (!Utils.isMultiplo(quantidadeDigitada, multiplo)) {
                quantidadeDigitada = Utils.multiploAnterior((float) quantidadeDigitada, (int) multiplo);
                txvQtde.setText(String.valueOf(quantidadeDigitada));
            }
            if (quantidadeDigitada < 0.0) {
                quantidadeDigitada = 0;
                txvQtde.setText(String.valueOf(quantidadeDigitada));
            }


            txvTotalItem.setText(String.format("%.4f", Utils.arredondar(valorUnitario * quantidadeDigitada, 2)));
        }
        mModificarValoresEditText = false;
        return quantidadeDigitada > 0;
    }

    private void setCoresEditText(ItemDigitacao itemDigitacao, EditText txtEdit, double percentualDescontoDigitado) {

        if (percentualDescontoDigitado > itemDigitacao.getProduto().getMaximoDesconto() || percentualDescontoDigitado < desconto_negativo)
            txtEdit.setTextColor(Color.RED);
        else
            txtEdit.setTextColor(Color.BLACK);
    }

    private double calcularDesconto(ItemDigitacao itemDigitacao, String s, int i) {
        return Utils.arredondar(100 - ((Double.parseDouble(s) / itemDigitacao.getValorUnitOriginal()) * 100), i);
    }

    private double digitarDescontoCalcularValorItem(ItemDigitacao itemDigitacao, double s, int i) {
        return Utils.arredondar(itemDigitacao.getValorUnitOriginal() - (itemDigitacao.getValorUnitOriginal() * (s / 100)), i);
    }

    private void formataCampoValor(CharSequence s, EditText txvValor) {
        if (!s.toString().equals("")) {
            double valor = Double.parseDouble(s.toString().replace(".", ""));
            if (s.length() > 0) {
                valor = valor / 100;
                formataValorParaQuatroCasasDecimais(valor, txvValor);
            }
        } else {
            formataValorParaQuatroCasasDecimais(0.0, txvValor);
        }
    }

    private void formataValorParaQuatroCasasDecimais(double valor, EditText txvValor) {
        txvValor.setText(String.format("%.2f", valor).replace(",", "."));
        txvValor.setSelection(txvValor.getText().length());
    }

    public double proximoMultiplo(double valor, int multiplo) {
        double valorSobra = valor % multiplo;

        if (valorSobra == 0)
            return valor + multiplo;
        else {
            return (valor - valorSobra) + multiplo;
        }
    }

    public double multiploAnterior(double valor, int multiplo) {
        double valorSobra = valor % multiplo;

        if (valorSobra == 0) {
            return (float) (valor - multiplo);
        } else {
            return (float) (valor - valorSobra);
        }
    }

    public void carregarFoto(int position) {
        String path = Environment.getExternalStorageDirectory() + "/sav/fotos/" + String.valueOf(itemAtual(position).getProduto().getIdProduto()) + ".jpg";
        File file = new File(path);
        if (!file.exists()) {
            path = Environment.getExternalStorageDirectory() + "/sav/fotos/" + String.valueOf(itemAtual(position).getProduto().getIdProduto()) + ".jpeg";
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            path = Environment.getExternalStorageDirectory() + "/sav/fotos/" + String.valueOf(itemAtual(position).getProduto().getIdProduto()) + ".png";
        }
        Intent it = new Intent("Imagem");
        it.addCategory(Principal.CATEGORIA);
        it.putExtra("caminhoImagem", path);
        startActivity(it);
    }

    public void showFiltroDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.popupfiltroitem);
        dialog.setTitle("Filtro de Produtos");
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        final EditText edtPesquisa = (EditText) dialog.findViewById(R.idPopupFiltroItem.edtPesquisa);
        final Spinner spnMarca = (Spinner) dialog.findViewById(R.idPopupFiltroItem.spnFornecedor);
        final CheckBox cbOrdemAlfabetica = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbAlfabetica);
        final CheckBox cbNormal = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbNormal);
        final CheckBox cbProdutosNovos = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbProdutosNovos);
        final CheckBox cbPromocoes = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbPromocoes);
        final CheckBox cbRecemRecebidos = (CheckBox) dialog.findViewById(R.idPopupFiltroItem.cbRecemRecebidos);
        final Button btnPesquisa = (Button) dialog.findViewById(R.idPopupFiltroItem.btnPesquisa);
        final Button btnCancela = (Button) dialog.findViewById(R.idPopupFiltroItem.btnCancela);

        spnMarca.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, new MarcaDAO(getBaseContext(), null).listaNomes()));
        spnMarca.setSelection(ultimoFiltroMarca);

//		cbOrdemAlfabetica.setChecked(ordemListaProdutos.equals(OrdemListaProdutos.ORDEM_ALFABETICA));
        cbOrdemAlfabetica.setChecked(true);

        cbNormal.setChecked(cbNormalAnterior);
        cbProdutosNovos.setChecked(cbProdutosNovosAnterior);
        cbPromocoes.setChecked(cbPromocoesAnterior);
        cbRecemRecebidos.setChecked(cbRecemRecebidosAnterior);

        btnPesquisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ultimoFiltroMarca = spnMarca.getSelectedItemPosition();
                boolean bPesquisaCodigo = true;
                long codigoProduto = 0;
                int index = -1;

                cbNormalAnterior = cbNormal.isChecked();
                cbProdutosNovosAnterior = cbProdutosNovos.isChecked();
                cbPromocoesAnterior = cbPromocoes.isChecked();
                cbRecemRecebidosAnterior = cbRecemRecebidos.isChecked();

                try {
                    codigoProduto = Long.parseLong(edtPesquisa.getText().toString());

                } catch (NumberFormatException e) {
                    bPesquisaCodigo = false;
                }

                if (bPesquisaCodigo) {
                    itemFiltrado = new ArrayList<ItemDigitacao>(itemAux);
                    for (int i = 0; i < itemFiltrado.size(); i++) {
                        if (itemFiltrado.get(i).getProduto().getIdProduto() == codigoProduto) {
                            index = i;
                            break;
                        }
                    }
                    if (index == -1) {
                        Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_produto_com_o_codigo_informado, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String pesquisa = edtPesquisa.getText().toString().toUpperCase();
                    if (pesquisa.trim().equals("") && spnMarca.getSelectedItemPosition() == 0) {
//						itemFiltrado = new ArrayList<ItemDigitacao>(itemAux);

                        itemFiltrado = new ArrayList<ItemDigitacao>(itemAux.size());

                        boolean achou;
                        for (ItemDigitacao item : itemAux) {
                            achou = false;

                            if (cbNormal.isChecked())
                                achou = item.getProduto().getStatus().equals(" ");

                            if (cbProdutosNovos.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("N");

                            if (cbPromocoes.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("P");

                            if (cbRecemRecebidos.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("R");

                            if (achou)
                                itemFiltrado.add(item);
                        }
                    } else {
                        long idMarca = 0;
                        if (spnMarca.getSelectedItemPosition() > 0) {
                            idMarca = new MarcaDAO(getBaseContext(), null).getIdByName(spnMarca.getSelectedItem().toString());
                        }

                        String[] trechos = pesquisa.split(",");
                        itemFiltrado = new ArrayList<ItemDigitacao>(itemAux.size());

                        boolean achou;
                        for (ItemDigitacao item : itemAux) {
                            achou = false;

                            if (cbNormal.isChecked())
                                achou = item.getProduto().getStatus().equals(" ");

                            if (cbProdutosNovos.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("N");

                            if (cbPromocoes.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("P");

                            if (cbRecemRecebidos.isChecked() && !achou)
                                achou = item.getProduto().getStatus().equals("R");

                            if (achou) {
                                if (trechos.length > 0) {
                                    for (int i = 0; i < trechos.length; i++) {
                                        achou = item.getProduto().getDescricao().contains(trechos[i].trim());
                                        if (!achou)
                                            break;
                                        if (idMarca > 0)
                                            achou = item.getProduto().getIdMarca() == idMarca;
                                        if (!achou)
                                            break;
                                    }
                                } else {
                                    achou = item.getProduto().getIdMarca() == idMarca;
                                }
                            }

                            if (achou)
                                itemFiltrado.add(item);
                        }
                    }

                    if (itemFiltrado.size() == 0) {
                        itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);

                        Toast.makeText(getBaseContext(), R.string.map_foi_encontrado_nenhum_produto_com_conteudo_pesquisado, Toast.LENGTH_LONG).show();
                    }

                    if (cbOrdemAlfabetica.isChecked()) {
                        Collections.sort(itemFiltrado, ItemDigitacao.ItemDigitacaoComparatorOrdemAlfabetica);
                        Collections.sort(itemPedido, ItemDigitacao.ItemDigitacaoComparatorOrdemAlfabetica);

                        ordemListaProdutos = OrdemListaProdutos.ORDEM_ALFABETICA;
                    } else {
                        Collections.sort(itemFiltrado);
                        Collections.sort(itemPedido);

                        ordemListaProdutos = OrdemListaProdutos.PRODUTOS_NOVOS;
                    }

                    itemCompleto = new ArrayList<ItemDigitacao>(itemFiltrado);
                }

                atualizaAdapter();

                if (bPesquisaCodigo && index != -1) { // Achou produto pelo c?digo
                    listView.setSelection(index);
                }

                dialog.dismiss();
            }
        });

        btnCancela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showDialogLegenda() {
        Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.dialog_legenda);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setTitle("LEGENDA");

        dialog.show();
    }

    public void removerFiltro() {
        vendidos = false;
        itemFiltrado = new ArrayList<ItemDigitacao>(itemAux);
        atualizaAdapter();
    }

    private ItemDigitacao itemAtual(int position) {
        ItemDigitacao itemDigitacao = null;
        if (vendidos)
            itemDigitacao = itemPedido.get(position);
        else
            itemDigitacao = itemFiltrado.get(position);

        return itemDigitacao;
    }

    public void geraPedidoVinculadoBonificacao() {
        if (pedido.getVinculoPedido() > 0) {
            pedidoVinculado = new Pedido();
            pedidoVinculado.setIDPedido(pedido.getVinculoPedido());
            pedidoVinculado = new PedidoDAO(getBaseContext(), null).get(pedidoVinculado.getIDPedido(), null);
            pedidoVinculado.setVinculoBonificacao(pedido.getIDPedido());
        }
    }

    public void venderProduto(ItemDigitacao itemDigitacao, double quantidade, double desconto, double precoDigitado) {
        if (parametro.isPermiteAlterarPreco()) {
            if (itemDigitacao.getValorUnit() == 0.0)
                itemDigitacao.setValorUnit(itemDigitacao.getValorUnitOriginal());
        } else {
            itemDigitacao.setValorUnit(itemDigitacao.getValorUnitOriginal());
        }

        if (parametro.isPermiteAlterarPreco()) {
            if (parametro.isPercDescontoProdutoGeraVerba()) {
                double dblDescAux = Utils.arredondar(100 - ((precoDigitado / itemDigitacao.getValorUnitOriginal()) * 100), 4);

                if (dblDescAux != desconto) {
                    if (precoDigitado != Utils.arredondar(itemDigitacao.getValorUnitOriginal() * (1 - (desconto / 100)), 4))
                        desconto = dblDescAux;
                }
            }
        } else {
            precoDigitado = itemDigitacao.getValorUnitOriginal();
        }
        double descontoDigitadoCombo = itemDigitacao.getDesconto() - itemDigitacao.getPercentualDescontoCombo();


        if (itemDigitacao.getPrecoCustoProduto() > 0 && itemDigitacao.getItemComboAtivado() > 0 && descontoDigitadoCombo >= 0) {
            double valorUntarioAlterado = Utils.arredondar(itemDigitacao.getValorUnitOriginal() * (1 - (descontoDigitadoCombo / 100)), 2);
            itemDigitacao.setRentabilidade(MathUtils.arredondar((valorUntarioAlterado - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
        } else {
            itemDigitacao.setRentabilidade(MathUtils.arredondar((precoDigitado - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
        }

        boolean retiraValores = (itemDigitacao.getQuantidade() > 0.0);
        if (retiraValores)
            retirarProdutoPedido(itemDigitacao);
        else if (quantidade > 0.0)
            itemPedido.add(itemDigitacao);

        itemDigitacao.setDesconto(desconto);
        itemDigitacao.setValorFlex(0.0);
        itemDigitacao.setQuantidade(quantidade);

        itemDigitacao.setValorDesconto(0.0);
        itemDigitacao.setValorUtilizaFlex(0.0);
        itemDigitacao.setValorGeraFlex(0.0);
        if (quantidade == 0.0)
            itemDigitacao.setRentabilidade(0.0);
        itemDigitacao.setValorUnit(precoDigitado);

        itemDigitacao.setValorBruto(itemDigitacao.getValorUnitOriginal());
        itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit());

        inserirProdutoPedido(itemDigitacao);

        if (quantidade == 0.0 && retiraValores) {
            itemPedido.remove(itemDigitacao);
            pedido.apurarRentabilidadePedido(itemPedido);
            if (itemDigitacao.getPrecoCustoProduto() > 0 && itemDigitacao.getRentabilidade() == 0) {
                itemDigitacao.setRentabilidade(MathUtils.arredondar((itemDigitacao.getValorUnitOriginal() - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
            }
        } else if (itemDigitacao.getQuantidade() > 0.0 && quantidade > 0.0 && retiraValores) {

            for (int i = 0; i < itemPedido.size(); i++) {
                if (itemDigitacao.getProduto().getIdProduto() == itemPedido.get(i).getProduto().getIdProduto()) {
                    itemPedido.remove(i);
                    break;
                }
            }
            itemPedido.add(itemDigitacao);

        }

        Collections.sort(itemPedido, new Comparator<ItemDigitacao>() {
            @Override
            public int compare(ItemDigitacao itemDigitacao, ItemDigitacao t1) {
                if (itemDigitacao.getProduto().getDescricao().compareTo(t1.getProduto().getDescricao()) == 0) {
                    return Long.compare(itemDigitacao.getProduto().getIdProduto(), t1.getProduto().getIdProduto());
                } else {
                    return itemDigitacao.getProduto().getDescricao().compareTo(t1.getProduto().getDescricao());
                }
            }
        });

        Utils.serializarObjeto(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
        Utils.serializarObjeto(getBaseContext(), ITENSPEDIDO_SERIALIZAR, itemPedido);
    }

    public void inserirProdutoPedido(ItemDigitacao itemDigitacao) {
        pedido.inserirProdutoPedido(itemDigitacao, parametro, tipoPedido, itemPedido);
    }

    public void retirarProdutoPedido(ItemDigitacao itemDigitacao) {
        // Diminuir o n?mero de itens do pedido
        pedido.retirarProdutoPedido(itemDigitacao, itemPedido);
    }

    private void enviarResumo(final Pedido ped, final ArrayList<Item> itens) {

        DialogInterface.OnClickListener eventoBtnSim = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                String nomeArquivo = gerarResumoDoPedido(ped, itens);
                abrirActivityEmailEnviarPdf(nomeArquivo);
            }
        };
        DialogInterface.OnClickListener eventoBtnNeutro = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                String nomeArquivo = gerarResumoDoPedido(ped, itens);
                abrirActivityCompartilhar(nomeArquivo);
            }
        };
        DialogInterface.OnClickListener eventoBtnNao = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        };

        showDialogConfirmacaoCompartilhar(this, eventoBtnSim, eventoBtnNeutro, eventoBtnNao, getString(R.string.pergunta_enviar_resumo_pedido));
    }

    private static Dialog showDialogConfirmacaoCompartilhar(Activity activity, DialogInterface.OnClickListener eventoBtnSim, DialogInterface.OnClickListener eventoBtnNeutro, DialogInterface.OnClickListener eventoBtnNao, String mensagem) {
        return Utils.showDialogConfirmacoes(activity, eventoBtnSim, eventoBtnNeutro, eventoBtnNao, mensagem, "E-mail", "+ Compartilhar", activity.getString(br.net.sav.R.string.nao), activity.getString(br.net.sav.R.string.a_t_e_n_c_a_o));
    }

    private void abrirActivityEmailEnviarPdf(String nomeArquivo) {
        Utils.openGmail(this, new String[]{cliente.getEmail()}, getString(R.string.assunto_email_pedido_pdf), nomeArquivo);
    }

    private void abrirActivityCompartilhar(String nomeArquivo) {
        Utils.abrirActivityEmail(this, new String[]{cliente.getEmail()}, nomeArquivo);
    }

    private String gerarResumoDoPedido(Pedido pedido, List<Item> itens) {
        ArrayList<String> linhas = new PedidoDAO(getBaseContext(), null).getLinhasPdfPedido(pedido, itens);
        return new PedidoDAO(getBaseContext(), null).gerarPdf(linhas, pedido.getIDPedido());
    }

    public void gravarPedido() {

        if (!pedido.getCombos().isEmpty()) {
            for (Combo combo : pedido.getCombos()) {
                if (!pedido.mesmaCondicaoPagamento(getBaseContext(), combo)) {
                    ToastUtils.mostrarMensagem(this, getString(R.string.salvar_combo_com_mesma_condicao_do_pedido));
                    return;
                }
            }
        }

        if (tipoPedido.getValorMinimoPedido() > pedido.getTotalLiquido()) {
            Toast.makeText(this,
                    getString(R.string.valor_minimo_permitido_para_tipo_pedido_selecionado) + String.format("%.2f", tipoPedido.getValorMinimoPedido() < 0 ? 0 : tipoPedido.getValorMinimoPedido()),
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (parametro.getQuantidadeMinimaItensRentabilidade() > pedido.getNrItens() && tabela.isUltilizaRentabilidade()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(getString(R.string.atencao));
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setMessage(getString(R.string.quantidadeMinimaItens) + " " + parametro.getQuantidadeMinimaItensRentabilidade() + " " + getString(R.string.itensDistintos));
            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.atencao));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.deseja_gravar_o_pedido).setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (itemPedido.size() == 0) {
                    Toast.makeText(getBaseContext(), R.string.nao_existem_itens_digitado_nesse_pedido, Toast.LENGTH_LONG).show();
                    return;
                }

                FormaPagamento forma = new FormaPagamentoDAO(getBaseContext(), null).get(pedido.getIDFormaPagamento());
                if (forma.getLimiteMinimo() > 0) {
                    if ((pedido.getTotalGeral() / condicao.getQtdeParcelas()) < forma.getLimiteMinimo()) {
                        Toast.makeText(getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo_da_forma_de_pagamento, Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (((pedido.getTotalGeral() < parametro.getLimiteMinimoPedido()) && parametro.getLimiteMinimoPedido() > 0.0 || pedido.getTotalGeral() <= 0.0) || (pedido.getTotalGeral() < condicao.getLimiteMinimoPedido()) && pedido.getTotalGeral() <= 0.0) {
                    Toast.makeText(getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo, Toast.LENGTH_LONG).show();
                    return;
                }

                Limite limite = new LimiteDAO(getBaseContext(), null).get(cliente.getIdCliente());
                if (parametro.isControlaLimiteCredito() && limite != null) {
                    if ((limite.getLimiteDisponivel() + valorPedidoSalvo) < pedido.getTotalLiquido()) {
                        // TODO VERIFICAR
                    }
                }

                if (parametro.isSolicitarSenhaRentabilidade()) {
                    if (validarRentabilidade())
                        return;
                }


                if (GerarBonificacaoDialog.ehTipoVenda(pedido, getBaseContext()) && parametro.getPercentualBonificacao() > 0) {
                    pedido.calcularSaldoBonificar(parametro.getPercentualBonificacao());

                }

                if (pedidoVinculado == null)
                    if (pedido.isBonificacao(getBaseContext()) && pedido.getVinculoPedido() == 0 && parametro.getPercentualBonificacao() > 0) {
                        GerarBonificacaoDialog.GerarBonificacaoSelecionandoVenda(pedido, parametro, ItemTabActivity.this);
                        return;
                    }

                if (pedidoVinculado != null)
                    if (pedido.isBonificacao(getBaseContext()) && pedidoVinculado.getVinculoBonificacao() > 0) {
                        if (pedidoVinculado.getSaldoBonificar() < pedido.getTotalLiquido()) {
                            ToastUtils.mostrarMensagem(getBaseContext(), String.format("Valor do Pedido ultrapassa o valor do Saldo a Bonificar do Pedido Vinculado: %.2f R$", pedidoVinculado.getSaldoBonificar()));
                            return;
                        }
                    }

                ArrayList<Item> itensPedido = new ArrayList<Item>(itemPedido.size());
                short numeroItem = 1;
                for (ItemDigitacao itemDigitacao : itemPedido) {
                    Item i = new Item();
                    i.setIDPedido(pedido.getIDPedido());
                    i.setIDProduto(itemDigitacao.getProduto().getIdProduto());
                    i.setDescricao(itemDigitacao.getProduto().getDescricao());
                    i.setNrItem(numeroItem++);
                    i.setQuantidade(itemDigitacao.getQuantidade());
                    i.setDesconto(itemDigitacao.getDesconto());
                    i.setValorDesconto(itemDigitacao.getValorDesconto());
                    i.setValorUtilizouFlex(itemDigitacao.getValorUtilizaFlex());
                    i.setValorGeraraFlex(itemDigitacao.getValorGeraFlex());
                    i.setValorUnitOriginal(itemDigitacao.getValorUnitOriginal());
                    i.setValorUnitPraticado(itemDigitacao.getValorUnit());
                    i.setTotalGeral(itemDigitacao.getValorBruto());
                    i.setTotalLiquido(itemDigitacao.getValorLiquido());
                    i.setDataPedido(pedido.getDataPedido());
                    i.setRentabilidade(itemDigitacao.getRentabilidade());
                    i.setCodigoNCM(itemDigitacao.getProduto().getCodigoNCM());
                    i.setCodigoBarra(itemDigitacao.getProduto().getCodigoBarra());
                    i.setPercentualDescontoCombo(itemDigitacao.getPercentualDescontoCombo());
                    i.setQuantidadeItemLimiteCombo(itemDigitacao.getQuantidadeItemLimiteCombo());
                    i.setItemComboAtivado(itemDigitacao.getItemComboAtivado());

                    itensPedido.add(i);
                }

                if (new PedidoDAO(getBaseContext(), null).salvarPedido(pedido, itensPedido, true)) {
                    if (limite != null) {
                        limite.setLimiteDisponivel(limite.getLimiteDisponivel() + valorPedidoSalvo);
                        limite.setLimiteDisponivel(limite.getLimiteDisponivel() - pedido.getTotalLiquido());
                        new LimiteDAO(getBaseContext(), null).update(limite);
                    }
                    ComboXPedido.gravarCombos(getBaseContext(), pedido);

                    if (pedido.isBonificacao(getBaseContext()) && pedidoVinculado != null) {
                        new PedidoDAO(getBaseContext(), null).update(pedidoVinculado, null);
                    }

                    if (pedido.isBonificacao(getBaseContext()) && pedidoVinculado == null && pedido.getVinculoPedido() > 0) {
                        Pedido pedidoVenda = new PedidoDAO(getBaseContext(), null).get(pedido.getVinculoPedido(), null);
                        pedidoVenda.setVinculoBonificacao(pedido.getIDPedido());
                        pedidoVenda.calcularSaldoBonificar(parametro.getPercentualBonificacao());
                        new PedidoDAO(getBaseContext(), null).update(pedidoVenda, null);
                    }

                    // Mapeamento
                    gravarGeoPosicionamento();
                    resetPreferencesSpinner();
                    Toast.makeText(getBaseContext(), R.string.pedido_gravado_com_sucesso, Toast.LENGTH_LONG).show();
                    Utils.apagarObjetoSerializado(getBaseContext());
                    if (!pedido.isAlterarPedido())
                        setResult(RESULT_OK, getIntent().putExtra(Pedido.EXTRA_ID, pedido.getIDPedido()));
                    else
                        setResult(PedidoListaActivity.REQUEST_PEDIDO_ALTERADO, getIntent().putExtra(Pedido.EXTRA_ID, pedido.getIDPedido()));

                    enviarResumo(pedido, itensPedido);
                } else {
                    Toast.makeText(getBaseContext(), R.string.erro_durante_gravacao_do_pedido, Toast.LENGTH_LONG).show();
                }
            }
        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private boolean validarRentabilidade() {
        if (tabela.isUltilizaRentabilidade()) {
            if (Double.valueOf(percRentabilidade) > 0)
                pedido.setPercRentabilidadeLiberado(Double.valueOf(percRentabilidade));

            if (pedido.getRentabilidade() < parametro.getPercentualRentabilidadeMinimaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0
                    || pedido.getPercRentabilidadeLiberado() > 0 && pedido.getRentabilidade() <= pedido.getPercRentabilidadeLiberado()) {

                if (pedido.getPercRentabilidadeLiberado() == 0)
                    msgAvisoPercRentabilidadeAbaixoDoMinimo(mensagem());
                else
                    ToastUtils.mostrarMensagem(getBaseContext(), mensagem());

                return true;

            }
        } else
            pedido.setRentabilidade(0d);
        return false;
    }

    public String mensagem() {
        if (pedido.getPercRentabilidadeLiberado() > 0) {
            return String.format("Rentabilidade do pedido abaixo do minimo permitido (LIBERADO POR SENHA): %.2f%%", pedido.getPercRentabilidadeLiberado());
        } else {
            return String.format("Rentabilidade do pedido abaixo do minimo permitido: %.2f%%, deseja desbloquear por senha?", parametro.getPercentualRentabilidadeMinimaDoPedido());
        }
    }

    private void msgAvisoPercRentabilidadeAbaixoDoMinimo(String mensagem) {
        final AlertDialog.Builder dlog = new AlertDialog.Builder(ItemTabActivity.this);
        dlog.setTitle(getString(R.string.atencao));
        dlog.setMessage(mensagem);
        dlog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDialogSenhaDesbloquearPedidoRentabilidade(pedido);
            }

        });
        dlog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;

            }
        });
        dlog.show();
    }

    private void showDialogSenhaDesbloquearPedidoRentabilidade(final Pedido pedido) {
        String mensagem = String.format("Vendedor: %d\nCliente: %d\nPedido: %d\nPerc. Rentabilidade do Pedido Gerado: %.2f%%",
                parametro.getIdVendedor(),
                pedido.getIDCliente(),
                pedido.getIDPedido(),
                pedido.getRentabilidade());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_inserir_senha, null);

        final EditText edtSenha = (EditText) dialogView.findViewById(R.id.dialogSenha_edtSenha);

        edtSenha.setHint(getString(R.string.informe_a_senha));
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.senha_desbloquear_pedido_rentabilidade));
        dialog.setMessage(mensagem);
        dialog.setView(dialogView);
        dialog.setPositiveButton(getString(R.string.ok), null);
        dialog.setNegativeButton(getString(R.string.cancelar), null);

        final AlertDialog dialogs = dialog.create();
        dialogs.show();
        dialogs.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validarText(edtSenha)) {
                    return;
                }
                percRentabilidade = GerarSenhaDialog.retirarPercentualSenha(edtSenha, pedido.getRentabilidade());

                if (Utils.isSenhaLiberarPedidoBloqueadoPorRentabilidade(StringUtils.getText(edtSenha), parametro.getIdVendedor(), pedido.getIDCliente(), pedido.getIDPedido(), Long.valueOf(percRentabilidade))) {
                    pedido.setPercRentabilidadeLiberado(Double.valueOf(percRentabilidade));
                    ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.pedido_liberado));
                    dialogs.dismiss();
                } else {
                    ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.senha_invalida));
                    dialogs.dismiss();
                }

            }
        });
    }

    private boolean validarText(EditText edtSenha) {
        if (StringUtils.editTextIsEmpty(edtSenha)) {
            edtSenha.setError(getString(R.string.informe_a_senha));
            return false;
        }

        if (StringUtils.getText(edtSenha).length() < 5) {
            edtSenha.setError("Senha possui mais de 5 digitos, digite novamente!");
            return false;
        }


        return true;
    }

    private void resetPreferencesSpinner() {
        sharedPreference("clicouTipoPedido", false);
        sharedPreference("clicouCondPagamento", false);
        sharedPreference("clicouFormaPagamento", false);
    }

    public boolean confirmaSaida() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.atencao));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.cancelar_pedidos).setCancelable(false).setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Utils.apagarObjetoSerializado(getBaseContext());
                resetPreferencesSpinner();
                setResult(Activity.RESULT_OK);
                finish();
            }
        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.show();
        return false;
    }

    public void fitrarProduto() {
        edtPesquisa = (EditText) findViewById(R.id.edtPesquisar);
        spnMarca1 = (Spinner) findViewById(R.id.spnMarcaFilter);
        cbVendidos = (CheckBox) findViewById(R.id.ItensPedido_ckbVendido);
        ImageButton imgButton = (ImageButton) findViewById(R.id.ItensPedido_btnListarTodos);
        edtPesquisa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, new MarcaDAO(getBaseContext(), null).listaNomes());
        spnMarca1.setAdapter(adapter);
        spnMarca1.setSelection(ultimoFiltroMarca);

        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ultimoFiltroMarca = 0;
                ckedVendidos = false;
                filtroPesquisa = false;
                cbVendidos.setChecked(ckedVendidos);
                edtPesquisa.setText("");
                edtPesquisa.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edtPesquisa, InputMethodManager.SHOW_IMPLICIT);
                spnMarca1.setSelection(ultimoFiltroMarca);
                removerFiltro();

            }
        });

        cbVendidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!vendidos && itemPedido.size() == 0) {
                    cbVendidos.setChecked(false);
                    Toast.makeText(getBaseContext(), R.string.nao_existem_tens_vendidos_nesse_pedido, Toast.LENGTH_SHORT).show();
                    return;
                }
                edtPesquisa.setText("");
                ultimoFiltroMarca = 0;
                spnMarca1.setSelection(ultimoFiltroMarca);
                if (cbVendidos.isChecked())
                    vendidos = ckedVendidos = cbVendidos.isChecked();
                else
                    vendidos = ckedVendidos = cbVendidos.isChecked();

                fitroItens();

            }
        });


        cbVendidos.setChecked(ckedVendidos);

        spnMarca1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pPosition, long arg3) {
                List<ItemDigitacao> fitromMarca = new ArrayList<>();
                long idMarcaSelected = 0;

                if (spnMarca1.getSelectedItemPosition() > 0) {
                    idMarcaSelected = new MarcaDAO(getBaseContext(), null).getIdByName(spnMarca1.getSelectedItem().toString());
                    edtPesquisa.setText("");
                }

                idMarca = idMarcaSelected;
                ultimoFiltroMarca = spnMarca1.getSelectedItemPosition();

                fitroItens();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                fitroItens();
            }
        });
    }

    private void fitroItens() {
        List<ItemDigitacao> filtro = new ArrayList<>();
        itensFiltradoVendidos = new ArrayList<>();
        if (ultimoFiltroMarca > 0 && !StringUtils.getText(edtPesquisa).isEmpty() && !vendidos) {
            String busca = tirarAcentos(converterParaMaiusculo(StringUtils.getText(edtPesquisa)));

            if (busca.matches(regex)) {
                boolean achou;
                for (ItemDigitacao item : itemAux) {
                    achou = false;
                    String[] trechos = busca.split(",");
                    if (trechos.length > 0) {
                        for (int i = 0; i < trechos.length; i++) {
                            achou = item.getProduto().getDescricao().contains(trechos[i].trim());
                            if (!achou)
                                break;
                            if (idMarca > 0)
                                achou = item.getProduto().getIdMarca() == idMarca;
                            if (!achou)
                                break;
                        }
                    } else {
                        achou = item.getProduto().getIdMarca() == idMarca;
                    }

                    if (achou)
                        filtro.add(item);

                }
            } else {
                for (int i = 0; i < itemAux.size(); i++) {
                    if (iniciaIdProdutoComTextoDoFiltro(itemAux.get(i).getProduto(), busca) && itemAux.get(i).getProduto().getIdMarca() == idMarca ||
                            String.valueOf(itemAux.get(i).getProduto().getIdProduto()).equals(busca) && itemAux.get(i).getProduto().getIdMarca() == idMarca) {
                        filtro.add(itemAux.get(i));
                    }
                }
            }

            itemFiltrado = filtro;
        } else if (vendidos && !StringUtils.getText(edtPesquisa).isEmpty() && ultimoFiltroMarca == 0) {
            String busca = tirarAcentos(converterParaMaiusculo(StringUtils.getText(edtPesquisa)));
            filtroPesquisa = true;

            if (busca.matches(regex)) {
                boolean achou;
                for (ItemDigitacao item : itemPedido) {
                    achou = false;
                    String[] trechos = busca.split(",");
                    if (trechos.length > 0) {
                        for (int i = 0; i < trechos.length; i++) {
                            achou = item.getProduto().getDescricao().contains(trechos[i].trim());
                            if (!achou)
                                break;
                        }
                    }

                    if (achou)
                        filtro.add(item);

                }
            } else {
                for (int i = 0; i < itemPedido.size(); i++) {
                    if (iniciaIdProdutoComTextoDoFiltro(itemPedido.get(i).getProduto(), busca) ||
                            String.valueOf(itemPedido.get(i).getProduto().getIdProduto()).equals(busca)) {
                        filtro.add(itemPedido.get(i));
                    }
                }

            }

            itensFiltradoVendidos = filtro;
        } else if (StringUtils.getText(edtPesquisa).isEmpty() && ultimoFiltroMarca > 0 && vendidos) {
            for (int i = 0; i < itemPedido.size(); i++) {
                if (itemPedido.get(i).getProduto().getIdMarca() == idMarca) {
                    filtro.add(itemPedido.get(i));
                }
            }
            itensFiltradoVendidos = filtro;
        } else if (!StringUtils.getText(edtPesquisa).isEmpty() && ultimoFiltroMarca > 0 && vendidos) {
            String busca = tirarAcentos(converterParaMaiusculo(StringUtils.getText(edtPesquisa)));

            if (busca.matches(regex)) {
                boolean achou;
                for (ItemDigitacao item : itemPedido) {
                    achou = false;
                    String[] trechos = busca.split(",");
                    if (trechos.length > 0) {
                        for (int i = 0; i < trechos.length; i++) {
                            achou = item.getProduto().getDescricao().contains(trechos[i].trim());
                            if (!achou)
                                break;
                            if (idMarca > 0)
                                achou = item.getProduto().getIdMarca() == idMarca;
                            if (!achou)
                                break;
                        }
                    } else {
                        achou = item.getProduto().getIdMarca() == idMarca;
                    }

                    if (achou)
                        filtro.add(item);

                }
            } else {
                for (int i = 0; i < itemPedido.size(); i++) {
                    if (iniciaIdProdutoComTextoDoFiltro(itemPedido.get(i).getProduto(), busca) && itemPedido.get(i).getProduto().getIdMarca() == idMarca ||
                            String.valueOf(itemPedido.get(i).getProduto().getIdProduto()).equals(busca) && itemPedido.get(i).getProduto().getIdMarca() == idMarca) {
                        filtro.add(itemPedido.get(i));
                    }
                }

            }
            itensFiltradoVendidos = filtro;
        } else if (StringUtils.getText(edtPesquisa).isEmpty() && ultimoFiltroMarca > 0 && !vendidos) {
            idMarca = new MarcaDAO(getBaseContext(), null).getIdByName(spnMarca1.getSelectedItem().toString());

            for (int i = 0; i < itemAux.size(); i++) {
                if (itemAux.get(i).getProduto().getIdMarca() == idMarca) {
                    filtro.add(itemAux.get(i));
                }
            }
            itemFiltrado = filtro;
        } else if (!StringUtils.getText(edtPesquisa).isEmpty() && ultimoFiltroMarca == 0 && !vendidos) {
            String busca = tirarAcentos(converterParaMaiusculo(StringUtils.getText(edtPesquisa)));

            if (busca.matches(regex)) {
                boolean achou;
                for (ItemDigitacao item : itemAux) {
                    achou = false;
                    String[] trechos = busca.split(",");
                    if (trechos.length > 0) {
                        for (int i = 0; i < trechos.length; i++) {
                            achou = item.getProduto().getDescricao().contains(trechos[i].trim());
                            if (!achou)
                                break;
                        }
                    }

                    if (achou)
                        filtro.add(item);

                }
            } else {
                for (int i = 0; i < itemAux.size(); i++) {
                    if (iniciaIdProdutoComTextoDoFiltro(itemAux.get(i).getProduto(), busca) ||
                            String.valueOf(itemAux.get(i).getProduto().getIdProduto()).equals(busca)) {
                        filtro.add(itemAux.get(i));
                    }
                }
            }
            itemFiltrado = filtro;
        } else if (marcaAposSincronizacao) {
            ultimoFiltroMarca = 0;
            filtroPesquisa = false;
            if (itemCompleto != null)
                itemFiltrado = itemCompleto;
        }

        if (marcaAposSincronizacao)
            atualizaAdapter();

    }

    protected boolean iniciaIdProdutoComTextoDoFiltro(Produto pObject, String pFiltro) {
        return tirarAcentos(converterParaMaiusculo(String.valueOf(pObject.getIdProduto()).trim())).startsWith(tirarAcentos(converterParaMaiusculo(pFiltro)));
    }

    public void atualizaAdapter() {
        if (!Utils.existeObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR)) {
            adapter = new ItemDigitacaoAdapter(getBaseContext(), new ArrayList<ItemDigitacao>());
            listView.setAdapter(adapter);
        } else {
            if (adapter == null) {
                //  setContentView(R.layout.item_tab_activity);

                if (vendidos && ultimoFiltroMarca > 0 || vendidos && filtroPesquisa)
                    adapter = new ItemDigitacaoAdapter(getBaseContext(), this.itensFiltradoVendidos);
                else if (vendidos)
                    adapter = new ItemDigitacaoAdapter(getBaseContext(), this.itemPedido);
                else
                    adapter = new ItemDigitacaoAdapter(getBaseContext(), this.itemFiltrado);

                listView.setAdapter(adapter);
                listView.setFastScrollEnabled(true);
            } else {
                if (vendidos && ultimoFiltroMarca > 0 || vendidos && filtroPesquisa) {
                    adapter.changeData(this.itensFiltradoVendidos);
                } else if (vendidos)
                    adapter.changeData(this.itemPedido);
                else
                    adapter.changeData(this.itemFiltrado);
                listView.setFastScrollEnabled(true);
            }
        }
    }

    private void onClickFloatingActionButton() {
        menu_floating = (FloatingActionMenu) findViewById(R.id.floating_action_menus);
        menu_floating.setVisibility(View.GONE);
        menu_item_filtro = (FloatingActionButton) findViewById(R.id.floating_action_menu_item_filtro);
        menu_item_remover_filtro = (FloatingActionButton) findViewById(R.id.floating_action_menu_item_remover_filtro);
        menu_item_save = (FloatingActionButton) findViewById(R.id.floating_action_menu_item_save);

        if (getFiltroItens(getBaseContext()) == 0) {
            menu_item_filtro.setVisibility(View.GONE);
        } else {
            menu_item_filtro.setVisibility(View.VISIBLE);
        }

        menu_item_filtro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu first item clicked
                menu_floating.close(true);
                showFiltroDialog();
            }
        });
        menu_item_remover_filtro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu third item clicked
                menu_floating.close(true);
                ListAdapter adapter = ItemAdapterMenu();
                dialogMenus(adapter);

            }

        });

        menu_item_save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu third item clicked
                menu_floating.close(true);
                salvarPedido();
            }
        });


    }

    private ListAdapter ItemAdapterMenu() {
        final ItemMenu[] items = {
                //new ItemMenu("Remover Filtro", R.drawable.search),
                //new ItemMenu("Itens vendidos", R.drawable.shopping_cart_menu),
                //   new ItemMenu("Calculadora", R.drawable.calculator),
                new ItemMenu("Legenda", R.drawable.info),
                new ItemMenu("Sair", R.drawable.stop),
        };

        return new ArrayAdapter<ItemMenu>(
                ItemTabActivity.this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);

                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };
    }

    private void dialogMenus(ListAdapter adapter) {
        new AlertDialog.Builder(ItemTabActivity.this, (Build.VERSION.SDK_INT >= 28) ? R.style.DialogStyle : 0)
                .setTitle("Menu")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        switch (item) {
//                            case 0:
//                                ultimoFiltroMarca = 0;
//                                removerFiltro();
//
//                                break;
//                            case 1:
//                                if (!vendidos && itemPedido.size() == 0) {
//                                    Toast.makeText(getBaseContext(), R.string.nao_existem_tens_vendidos_nesse_pedido, Toast.LENGTH_SHORT).show();
//                                    return;
//                                }
//
//                                vendidos = !vendidos;
//                                atualizaAdapter();
//                                break;

//                            case 2:
//                                Utils.iniciarCalculadora(getBaseContext());
//
//                                break;

                            case 0:
                                showDialogLegenda();
                                break;
                            case 1:
                                confirmaSaida();

                                break;
                        }
                        //...
                    }
                }).show();
    }

    public static class ItemMenu {
        public final String text;
        public final int icon;

        public ItemMenu(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }


    public boolean dialogConfirmacaoFrete(double value) {
        boolean retorno = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.atencao)).setMessage(String.format(getString(R.string.valor_do_frete_adicionado_deseja_confirmar_as_alteracoes), value)).setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    gravarPedido();
                    dialog.dismiss();
                } catch (Exception e) {
                    Log.e(getString(R.string.item_tab_activity_dialog_confirmacao_frete), "dialogConfirmacaoFrete(value)." + e.getMessage());
                }
            }
        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
        return retorno;
    }

    protected void gravarGeoPosicionamento() {
        try {
            Mapeamento m = (Mapeamento) Utils.recuperarObjetoSerializado(getBaseContext(), LocationService.MAPEAMENTO_SERIALIZADO);
            if (m == null) {
                m = new Mapeamento();
                m.setIdVendedor(parametro.getIdVendedor());
                m.setEnviado(false);
                m.setDataEnvio(new Date());
                m.setDataHora(new Date());
            }
            m.setIdPedido(pedido.getIDPedido());
            m.setValorPedido(pedido.getTotalLiquido());
            m.setIdCliente(cliente.getIdCliente());
            m.setNomeCliente(cliente.getRazao());
            // Inserir
            new MapeamentoDAO(getBaseContext()).insert(m, null);

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    /*
     * --- ADAPTER --- *
     */private class ItemDigitacaoAdapter extends BaseAdapter implements SectionIndexer {

        private List<ItemDigitacao> lista;
        private Context ctx;
        private TextView txvRentabilidadeItem;
        private ImageView mImvRentabilidade;

        List<String> myElements = null;
        HashMap<String, Integer> alphaIndexer;
        String[] sections;

        public ItemDigitacaoAdapter(Context ctx, List<ItemDigitacao> lista) {
            this.lista = lista;
            this.ctx = ctx;

            if (ordemListaProdutos.equals(OrdemListaProdutos.ORDEM_ALFABETICA)) {
                atualizaBarraLetras();
            } else {
                removerBarraLetras();
            }
        }

        public void atualizaBarraLetras() {
            myElements = new ArrayList<String>(lista.size());

            for (int i = 0; i < lista.size(); i++) {
                myElements.add(lista.get(i).getProduto().getDescricao());
            }

            alphaIndexer = new HashMap<String, Integer>();

            int size = lista.size();

            for (int i = size - 1; i >= 0; i--) {
                String element = myElements.get(i);
                alphaIndexer.put(element.substring(0, 1), i);
            }

            Set<String> keys = alphaIndexer.keySet();

            Iterator<String> it = keys.iterator();
            ArrayList<String> keyList = new ArrayList<String>();

            while (it.hasNext()) {
                String key = it.next();
                keyList.add(key);
            }

            Collections.sort(keyList);

            sections = new String[keyList.size()];

            keyList.toArray(sections);
        }

        public void removerBarraLetras() {
            ArrayList<String> keyList = new ArrayList<String>();
            sections = new String[keyList.size()];

            keyList.toArray(sections);
        }

        public void changeData(List<ItemDigitacao> lista) {
            this.lista = lista;
            notifyDataSetChanged();

            if (ordemListaProdutos.equals(OrdemListaProdutos.ORDEM_ALFABETICA)) {
                atualizaBarraLetras();
            } else {
                removerBarraLetras();
            }
        }

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return lista.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getPositionForSection(int section) {
            if (sections != null && section < sections.length) {
                String letter = sections[section];
                return alphaIndexer.get(letter);
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            return sections;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null) {

                LayoutInflater inflater = LayoutInflater.from(getBaseContext());
                //convertView = inflater.inflate(R.layout.row_item, parent, false);
                //LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.itens_pedido_lista, parent, false);
            }

            if (position % 2 == 0) {
                view.setBackgroundResource(R.drawable.alterselector1);
            } else {
                view.setBackgroundResource(R.drawable.alterselector2);
            }

            final ItemDigitacao item = lista.get(position);

            mImvRentabilidade = (ImageView) view.findViewById(R.id.ListaItensPedido_imgRentabilidade);

            TextView txvDescricao = (TextView) view.findViewById(R.idItensPedido.txvDescricao);
            txvDescricao.setText(String.format("%d - %s", item.getProduto().getIdProduto(), item.getProduto().getDescricao()));
            txvDescricao.setTextSize(16);
            txvDescricao.setTypeface(Typeface.DEFAULT_BOLD);
            txvDescricao.setTextColor(Color.parseColor(item.getCorProduto()));
            txvDescricao.setBackgroundColor(item.getBackgroundProduto());

            TextView txvDescricaoEmbalagem = (TextView) view.findViewById(R.idItensPedido.txvDescricaoEmbalagem);
            txvDescricaoEmbalagem.setText(String.format("Embalagem : %s", item.getProduto().getDescricaoEmbalagem()));
            txvDescricaoEmbalagem.setTextColor(Color.WHITE);

            TextView txvCodBarras = (TextView) view.findViewById(R.id.ItensPedido_txtCodBarras);
            txvCodBarras.setText(String.format("Cod.Barras: %s", item.getProduto().getCodigoBarra()));
            txvCodBarras.setTextColor(Color.WHITE);

            txvRentabilidadeItem = (TextView) view.findViewById(R.id.ItensPedido_txtRentabilidadeItem);

            TextView txvQtde = (TextView) view.findViewById(R.idItensPedido.txvQtde);
            txvQtde.setText(String.format("Qtde.: %.2f", item.getQuantidade()));
            txvQtde.setTextColor(Color.WHITE);

            TextView txvQtdeEmbalagem = (TextView) view.findViewById(R.idItensPedido.txvQtdeEmbalagem);
            txvQtdeEmbalagem.setText(String.format("Qtde Embalagem: %d", item.getProduto().getQuantidadeEmbalagem()));
            txvQtdeEmbalagem.setTextColor(Color.WHITE);

            TextView txvMultiplo = (TextView) view.findViewById(R.idItensPedido.txvEstoque);
            txvMultiplo.setText(String.format("Estoque : %s", item.getProduto().getSaldoEstoque()));
            txvMultiplo.setTextColor(Color.WHITE);

            TextView txvValor = (TextView) view.findViewById(R.idItensPedido.txvValor);
            txvValor.setText(String.format("Valor Unit.: R$ %.4f", item.getValorUnit()));

            txvRentabilidadeItem.setText(String.format("Perc. Rentabilidade: %.4f%%", item.getRentabilidade()));
            txvRentabilidadeItem.setTextColor(Color.WHITE);

            TextView txvValorUnitEmb = (TextView) view.findViewById(R.id.ItensPedido_txvValoUniEmb);
            if (item.getProduto().getQuantidadeEmbalagem() > 1) {
                txvValorUnitEmb.setText(String.format("Valor Unit.Emb.: R$ %.4f", item.getValorUnit() / item.getProduto().getQuantidadeEmbalagem()));
                txvValorUnitEmb.setTextColor(Color.WHITE);
                txvValorUnitEmb.setVisibility(View.VISIBLE);
            } else {
                txvValorUnitEmb.setText("");
                txvValorUnitEmb.setVisibility(View.GONE);
            }

            TextView txvValorLiquido = (TextView) view.findViewById(R.idItensPedido.txvValorLiquido);
            txvValorLiquido.setText(String.format(getString(R.string.valor_liquido2), item.getValorLiquido()));
            txvValorLiquido.setTextColor(Color.WHITE);

            TextView txvMarca = (TextView) view.findViewById(R.idItensPedido.txvMarca);
            txvMarca.setText(String.format("Marca : %s", item.getDescricaoMarca() != null ? item.getDescricaoMarca() : getString(R.string.descricao_nao_encontrada)));
            txvMarca.setTextColor(Color.WHITE);

            ImageView imgVendido = (ImageView) view.findViewById(R.idItensPedido.imgVendido);
            ImageView imgFoto = (ImageView) view.findViewById(R.idItensPedido.imgFoto);

            imgFoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    carregarFoto(position);
                }
            });

            imgFoto.setVisibility(item.isPossuiFoto() ? View.VISIBLE : View.INVISIBLE);
            ImageView imageCombo = (ImageView) view.findViewById(R.id.comboVendido);
            imageCombo.setVisibility(View.GONE);

            TextView txvComboVendido = (TextView) view.findViewById(R.id.txvComboVendido);
            txvComboVendido.setText(String.format("Qtde Combo Vendido.: %d", item.getItemComboAtivado()));
            txvComboVendido.setTextColor(Color.WHITE);
            txvComboVendido.setVisibility(View.GONE);

            TextView txvDescCombo = (TextView) view.findViewById(R.id.txvDescontoCombo);
            txvDescCombo.setText(String.format("Desc. Combo: %.2f%%", item.getPercentualDescontoCombo()));
            txvDescCombo.setTextColor(Color.WHITE);
            txvDescCombo.setVisibility(View.GONE);

            if (!combosValidos.isEmpty())
                for (RegraAplicavel combo : combosValidos) {
                    if (!comboXProdutos.isEmpty()) {
                        for (ComboXProduto cXp : comboXProdutos) {
                            if (cXp.getIdCombo() == combo.getIdCombo() && cXp.getIdProduto() == item.getProduto().getIdProduto()) {
                                imageCombo.setVisibility(View.VISIBLE);
                                if (item.getQuantidade() > 0 && item.getItemComboAtivado() > 0) {
                                    txvComboVendido.setVisibility(View.VISIBLE);
                                    txvDescCombo.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                }

            imageCombo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogAvisoCombos(item);
                }
            });
            if (item.getQuantidade() > 0) {
                txvQtde.setVisibility(View.VISIBLE);
                imgVendido.setVisibility(View.VISIBLE);
                txvValorLiquido.setVisibility(View.VISIBLE);
            } else {
                txvQtde.setVisibility(View.INVISIBLE);
                imgVendido.setVisibility(View.INVISIBLE);
                txvValorLiquido.setVisibility(View.INVISIBLE);
            }

            if (pedido!=null && tabela != null)
            if (pedido.getIDTabela() == tabela.getIdTabela()) {
                if (tabela.isUltilizaRentabilidade())
                    visualizarSemafaroItem(item);
                else {
                   // txvRentabilidadeItem.setVisibility(View.);
                    mImvRentabilidade.setVisibility(View.GONE);
                }
                txvRentabilidadeItem.setVisibility(View.VISIBLE);
                txvRentabilidadeItem.setTextColor(ConfigSemafaro.getTextColorSemafaroItem(item));
            }


            return view;
        }

        private void visualizarSemafaroItem(ItemDigitacao item) {
            if (parametro.getVisualizarRentabilidade()) {
                txvRentabilidadeItem.setVisibility(View.GONE);
                mImvRentabilidade.setVisibility(View.VISIBLE);
                mImvRentabilidade.setImageResource(ConfigSemafaro.getImagemSemaforoItem(item));

            } else {
                mImvRentabilidade.setVisibility(View.GONE);
//                txvRentabilidadeItem.setVisibility(View.VISIBLE);
//                txvRentabilidadeItem.setTextColor(ConfigSemafaro.getTextColorSemafaroItem(item));

            }
        }
    }

    private void dialogAvisoCombos(ItemDigitacao item) {
        boolean comboVendido = false;
        if (!combosValidos.isEmpty()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View content = getLayoutInflater().inflate(R.layout.combo_listview_dialog, null);
            builder.setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.atencao)
                    .setView(content);

            TextView textView = (TextView) content.findViewById(R.id.txv_combo1);
            ListView listView = (ListView) content.findViewById(R.id.list_dialog_combo);
            Button btnAtivar = (Button) content.findViewById(R.id.buttonCombo);
            Button btnCancelar = (Button) content.findViewById(R.id.buttonCombo1);
            btnCancelar.setVisibility(View.GONE);
            btnAtivar.setGravity(Gravity.CENTER_HORIZONTAL);
            btnAtivar.setText("Ok");

            List<Combo> combos = new ArrayList<>();
            for (RegraAplicavel combo : combosValidos) {
                RegraCompra compra = RegraCompra.buscaRegraCompra(getBaseContext(), combo.getIdCombo());
                if (compra.listIdCampo.contains(item.getProduto().getIdProduto()) && item.getItemComboAtivado() > 0) {

                    if (!pedido.getCombos().isEmpty()) {
                        for (Combo comboAtivo : pedido.getCombos()) {
                            if (comboAtivo.getIdCombo() == combo.getIdCombo()) {
                                setComboMsg(combos, combo, false);
                                comboVendido = true;
                            }
                        }
                    }
                }
            }
            if (comboVendido) {
                listView.setVisibility(View.VISIBLE);
                textView.setText("Combo(s) Vendido(s): " + item.getItemComboAtivado());
            } else {
                textView.setText("Nenhum Combo Vendido!");
                listView.setVisibility(View.GONE);
            }
            DialogComboAdapter dialogComboAdapter = new DialogComboAdapter(getBaseContext(), combos);
            listView.setAdapter(dialogComboAdapter);

            final AlertDialog alert = builder.create();
            btnAtivar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();

                }
            });
            alert.show();
        }
    }

    /* Task para inicializa??o da estrutura de digita??o do pedido */
    class InicializacaoEstruturaTask extends AsyncTask<Void, Void, String> {
        private ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mIsInicializandoEstruturas = true;
            showProgress();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected String doInBackground(Void... arg0) {
            try {
                itemCompleto = new ItemDigitacaoDAO(getApplicationContext(), null).getEstruturaDigitacaoPedido(tipoPedido, tabela, condicao);

                itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
                itemAux = new ArrayList<ItemDigitacao>(itemCompleto);
                itemPedido = new ArrayList<ItemDigitacao>();

                List<ItemDigitacao> itensRecuperados = null;
                List<Item> itensPedidoAlteracao = null;

                Object obj = Utils.recuperarObjetoSerializado(getBaseContext(), ITENSPEDIDO_SERIALIZAR);
                if (obj != null) {
                    // Caso esteja recuperando um pedido que n?o foi finalizado corretamente
                    itensRecuperados = (ArrayList<ItemDigitacao>) obj;
                } else {
                    // Busca os itens do pedido caso seja uma altera??o, se for um pedido novo essa lista ficar? vazia
                    itensPedidoAlteracao = new ItemDAO(getBaseContext(), null).listaByPedido(pedido.getIDPedido());
                }

                if (itensRecuperados != null || (itensPedidoAlteracao != null && itensPedidoAlteracao.size() > 0)) {
                    if (itensPedidoAlteracao != null) {
                        Item itemAux;
                        int aux = 0;
                        for (ItemDigitacao itemDigitacao : itemFiltrado) {
                            if (aux == itensPedidoAlteracao.size())
                                break;

                            itemAux = null;

                            for (Item itemAlterar : itensPedidoAlteracao) {
                                if (itemAlterar.getIDProduto() == itemDigitacao.getProduto().getIdProduto()) {
                                    itemAux = itemAlterar;
                                    aux++;
                                    break;
                                }
                            }

                            if (itemAux != null) {
                                itemDigitacao.setQuantidade(itemAux.getQuantidade());
                                itemDigitacao.setQuantidadeGravada(itemAux.getQuantidade());
                                itemDigitacao.setDesconto(itemAux.getDesconto());
                                itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
                                // itemDigitacao.setValorUtilizaFlex(itemAux.getValorUtilizouFlex());
                                // itemDigitacao.setValorGeraFlex(itemAux.getValorGeraraFlex());
                                itemDigitacao.setValorUnit(itemAux.getValorUnitPraticado());
                                itemDigitacao.setValorBruto(itemAux.getTotalGeral());
                                itemDigitacao.setValorLiquido(itemAux.getTotalLiquido());
                                itemDigitacao.setRentabilidade(itemAux.getRentabilidade());

                                itemPedido.add(itemDigitacao);
                            }
                        }
                    } else if (itensRecuperados != null) {
                        int aux = 0;
                        ItemDigitacao itemAux = null;

                        for (ItemDigitacao itemDigitacao : itemFiltrado) {
                            if (aux == itensRecuperados.size())
                                break;

                            itemAux = null;

                            for (ItemDigitacao itemRecuperar : itensRecuperados) {
                                if (itemRecuperar.getProduto().getIdProduto() == itemDigitacao.getProduto().getIdProduto()) {
                                    itemAux = itemRecuperar;
                                    aux++;
                                    break;
                                }
                            }

                            if (itemAux != null) {
                                double desconto = itemAux.getDesconto() - itemAux.getPercentualDescontoCombo();

                                if (itemAux.getPrecoCustoProduto() > 0 && itemAux.getItemComboAtivado() > 0 && desconto >= 0) {
                                    double valorUntarioAlterado = Utils.arredondar(itemAux.getValorUnitOriginal() * (1 - (desconto / 100)), 2);
                                    itemAux.setRentabilidade(MathUtils.arredondar((valorUntarioAlterado - itemAux.getPrecoCustoProduto()) / itemAux.getPrecoCustoProduto() * 100, 2));
                                }
                                itemDigitacao.setQuantidade(itemAux.getQuantidade());
                                itemDigitacao.setDesconto(itemAux.getDesconto());
                                itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
                                // itemDigitacao.setValorFlex(itemAux.getValorFlex());
                                // itemDigitacao.setValorUtilizaFlex(itemAux.getValorUtilizaFlex());
                                // itemDigitacao.setValorGeraFlex(itemAux.getValorGeraFlex());
                                itemDigitacao.setValorUnit(itemAux.getValorUnit());
                                itemDigitacao.setValorBruto(itemAux.getValorBruto());
                                itemDigitacao.setValorLiquido(itemAux.getValorLiquido());
                                itemDigitacao.setRentabilidade(itemAux.getRentabilidade());
                                itemDigitacao.setPercentualDescontoCombo(itemAux.getPercentualDescontoCombo());
                                itemDigitacao.setQuantidadeItemLimiteCombo(itemAux.getQuantidadeItemLimiteCombo());
                                itemDigitacao.setItemComboAtivado(itemAux.getItemComboAtivado());


                                itemPedido.add(itemDigitacao);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                return getString(R.string.erro_inicializando_estrutura_digitacao) + (e != null ? e.getMessage() : "Exception null");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            marcaAposSincronizacao = true;
            try {
                if (result != null) {
                    Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
                } else {
                    if (!mIsInicializandoEstruturas || isZerarDesconto) { recalcularPreco();}
                }
            } catch (Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
                if (result == null) {
                    atualizaAdapter();
                    verificaIsOperacaoTroca();
                }
                mIsInicializandoEstruturas = false;
                fitrarProduto();

                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                }
            }
        }

        private void showProgress() {
            progress = new ProgressDialog(ItemTabActivity.this);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.setIcon(getResources().getDrawable(R.drawable.icon));
            progress.setTitle(getString(R.string.app_name));
            progress.setMessage(getString(R.string.por_favor_aguarde_carregando_estruturas));
            progress.show();
        }
    }

}