package br.net.sav.atacadomaringa;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.dao.ItemTrocaDAO;
import br.net.sav.modelo.ItemTroca;

public class ItemTrocaListaActivity extends ListActivity {
	public static final String ACTION = "ItemTrocaLista" ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		try {
			Intent it = getIntent();
			
			ArrayList<ItemTroca> listaItems = new ItemTrocaDAO(getBaseContext()).getByTroca(it.getLongExtra("IdTroca", 0));
			setListAdapter(new ItemTrocaListaAdapter(getBaseContext(), listaItems));
			
			TextView title = (TextView) getWindow().findViewById(android.R.id.title);
			title.setGravity(Gravity.CENTER);
			setTitle(getString(R.string.itens_da_troca_nr) + String.valueOf(it.getLongExtra("IdTroca", 0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class ItemTrocaListaAdapter extends BaseAdapter {
	private Context ctx;
	private ArrayList<ItemTroca> lista;
	
	public ItemTrocaListaAdapter(Context ctx, ArrayList<ItemTroca> lista) {
		this.ctx = ctx;
		this.lista = lista;
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if (view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.itemtrocalista, null);
		}
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		ItemTroca item = lista.get(position);
		
		TextView txvProduto = (TextView) view.findViewById(R.idItemTrocaLista.txvProduto);
		txvProduto.setText(String.format("%d - %s", item.getIdProduto(), (item.getDescricaoProduto() == null ? R.string.produto_nao_encontrado : item.getDescricaoProduto())));
		txvProduto.setTextSize(17);
		txvProduto.setTypeface(Typeface.DEFAULT_BOLD);
		txvProduto.setTextColor(Color.GREEN);
		txvProduto.setSingleLine(true);
		
		TextView txvEmbalagem = (TextView) view.findViewById(R.idItemTrocaLista.txvEmbalagem);
		txvEmbalagem.setText(String.format("Embalagem: %s", (item.getDescricaoEmbalagem() == null ? "" : item.getDescricaoEmbalagem())));
		txvEmbalagem.setTextSize(17);
		
		TextView txvQuantidade = (TextView) view.findViewById(R.idItemTrocaLista.txvQuantidade);
		txvQuantidade.setText(String.format("Quantidade: %d", item.getQuantidade()));
		txvQuantidade.setTextSize(17);
		
		TextView txvValorUnit = (TextView) view.findViewById(R.idItemTrocaLista.txvValorUnitario);
		txvValorUnit.setText(String.format("Valor Unit: %.2f", item.getValorUnitario()));
		txvValorUnit.setTextSize(17);
		
		TextView txvValorLiquido = (TextView) view.findViewById(R.idItemTrocaLista.txvValorTotal);
		txvValorLiquido.setText(String.format("Valor Total: %.2f", item.getValorMercadoriaLiquido()));
		txvValorLiquido.setTextSize(17);
		
		return view;
	}
}
