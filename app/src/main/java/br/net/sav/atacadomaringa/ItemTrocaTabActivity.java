package br.net.sav.atacadomaringa;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.net.sav.Utils;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.FornecedorDAO;
import br.net.sav.dao.ItemDigitacaoDAO;
import br.net.sav.dao.ItemTrocaDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TrocaDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.ItemTroca;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.Troca;

public class ItemTrocaTabActivity extends ListActivity {
	private final String TROCA_SERIALIZAR = "troca.srl";
	private final String ITENSTROCA_SERIALIZAR = "itemtroca.srl";
	
	private ItemTrocaTabAdapter adapter;
	
	private Cliente cliente;
	private Troca troca;
	private Parametro parametro;
	private Tabela tabelaPreco;
	private Condicao condicaoPagamento;
	
	private List<ItemDigitacao> itemCompleto;
	private List<ItemDigitacao> itemFiltrado;
	private List<ItemDigitacao> itemTroca;
	
	private boolean trocados;
	private int savedIndex, savedY, position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		try {
			troca = (Troca) Utils.recuperarObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR);
			cliente = new ClienteDAO(getBaseContext(), null).get(troca.getIdCliente());
			parametro = new ParametroDAO(getBaseContext(), null).get();
			tabelaPreco = new TabelaDAO(getBaseContext(), null).get(troca.getIdTabela());
			condicaoPagamento = new CondicaoDAO(getBaseContext(), null).get(troca.getIdCondicao(), troca.getIdTabela());
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), "Erro ao criar a tela de itens da troca:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
			finish();
		}
		
		inicializaEstruturaDigitacao();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Troca trocaAux = (Troca) Utils.recuperarObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR);
		if (trocaAux.getIdTabela() != troca.getIdTabela() || trocaAux.getIdCondicao() != troca.getIdCondicao()) {
			
			inicializaEstruturaDigitacao();
			
			for (int i = 0; i < itemTroca.size(); i++) {
				ItemDigitacao itemDigitacao = itemTroca.get(i);
				
				retirarProdutoTroca(itemDigitacao);
				
				itemDigitacao.setValorBruto(itemDigitacao.getValorUnit());
				itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit());
				
				itemDigitacao.setValorUnit(Utils.arredondar(itemDigitacao.getValorUnitOriginal() * (1 - (itemDigitacao.getDesconto() / 100)), 2, 2));
				
				itemDigitacao.setValorUtilizaFlex(0.0);
				itemDigitacao.setValorGeraFlex(0.0);
				
				inserirProdutoTroca(itemDigitacao);
			}
			
			if (itemTroca.size() > 0) {
				Utils.serializarObjeto(getBaseContext(), TROCA_SERIALIZAR, troca);
				Utils.serializarObjeto(getBaseContext(), ITENSTROCA_SERIALIZAR, itemTroca);		
			}
		}
		
		troca = (Troca) Utils.recuperarObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR);
		atualizaAdapter();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		savedIndex = getListView().getFirstVisiblePosition();  
		View v1 = getListView().getChildAt(0);
		savedY = (v1 == null) ? 0 : v1.getTop();
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		this.position = position;
		showCustomDialog();		
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem mi = menu.findItem(R.idMenuTroca.quais_mostrar);
		if (trocados) {
			mi.setTitle("Ver Todos");
			mi.setIcon(R.drawable.list_all);
		} else {
			mi.setTitle("Itens p/ troca");
			mi.setIcon(R.drawable.shopping_cart_menu);
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.troca_menu, menu);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuTroca.gravar_troca:
			gravarTroca();
			
			return true;
		case R.idMenuTroca.sair_sem_salvar:
			confirmaSaida();
			
			return true;
		case R.idMenuTroca.filtraprodutos:
			showDialogFiltroItem();
			
			return true;
		case R.idMenuTroca.removerfiltros:
			trocados = false;
			itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
			atualizaAdapter();
			
			return true;
		case R.idMenuTroca.quais_mostrar:
			if (!trocados && itemTroca.size() == 0) {
				Toast.makeText(getBaseContext(), R.string.nao_existem_itens_para_troca_nesse_pedido, Toast.LENGTH_SHORT).show();
				return true;
			}
			trocados = ! trocados;
			atualizaAdapter();
			
			return true;
		case R.idMenuTroca.calculadora:
			Intent it = new Intent();
			it.setClassName("com.android.calculator2", "com.android.calculator2.Calculator");
			startActivity(it);
			
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onBackPressed() {
		confirmaSaida();
	}
	
	private void showCustomDialog() {
		final ItemDigitacao itemDigitacao = itemAtual(position);
		
		savedIndex = getListView().getFirstVisiblePosition();
		View v1 = getListView().getChildAt(0);
		savedY = (v1 == null) ? 0 : v1.getTop();
		
		final Dialog dialog = new Dialog(this);
		
		dialog.setContentView(R.layout.popuptroca_layout);
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		TextView txvTitle = (TextView) dialog.findViewById(android.R.id.title);
		txvTitle.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		txvTitle.setTextSize(16);
		txvTitle.setTextColor(Color.WHITE);
		txvTitle.setText(itemDigitacao.getProduto().getDescricao());
		
		final Button btnMais = (Button) dialog.findViewById(R.idPopupTroca.btnMais);
		final Button btnMenos = (Button) dialog.findViewById(R.idPopupTroca.btnMenos);
		final Button btnOk = (Button) dialog.findViewById(R.idPopupTroca.btnOk);
		final EditText txtQtde = (EditText) dialog.findViewById(R.idPopupTroca.txtQuantidade);

		if (itemDigitacao.getQuantidade() > 0)
			txtQtde.setText(String.valueOf(itemDigitacao.getQuantidade()));
		
		btnMais.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String qtde = txtQtde.getText().toString();
				if (qtde.equals("")) {
					qtde = "1";
				} else {
					qtde = String.valueOf(Integer.parseInt(qtde) + 1);
				}
				txtQtde.setText(qtde);
			}
		});
		
		btnMenos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String sQtde = txtQtde.getText().toString();
				
				if (!sQtde.equals("")) {
					int qtde = Integer.parseInt(sQtde);
					if (qtde == 0)
						return;
					sQtde =  String.valueOf(qtde - 1);
				}
				txtQtde.setText(sQtde);				
			}
		});
		
		btnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String sQtdeDigitada = txtQtde.getText().toString();
				if (sQtdeDigitada.trim().equals(""))
					sQtdeDigitada = "0";
				
				//Recupera a quantidade digitada na tela
				short qtdeDigitada = 0;
				try {
					qtdeDigitada = Short.parseShort(sQtdeDigitada);
				} catch (NumberFormatException e) {
					Toast.makeText(getBaseContext(), R.string.quantidade_invalida, Toast.LENGTH_SHORT).show();
					return;
				}
				
				if (qtdeDigitada > 0) {
					if (itemDigitacao.getQuantidade() == 0 && itemTroca.size() == parametro.getQtdeMaximaItens() && parametro.getQtdeMaximaItens() > 0) {
						Toast.makeText(getBaseContext(), R.string.quantidade_maxima_de_itens_atingida, Toast.LENGTH_SHORT).show();
						return;
					}
				}
				
				venderProdutoTroca(itemDigitacao, qtdeDigitada);
				
				dialog.dismiss();
				atualizaAdapter();
			}
		});
		
		dialog.show();
	}
	
	private void venderProdutoTroca(ItemDigitacao itemDigitacao, short quantidade) {
        boolean retirarValores = (itemDigitacao.getQuantidade() > 0);

        if (retirarValores) {
            troca.setNumeroItens((short) (troca.getNumeroItens() - 1));

            troca.setValorMercadoriaBruto(troca.getValorMercadoriaBruto() - itemDigitacao.getValorBruto());
            troca.setValorMercadoriaLiquido(troca.getValorMercadoriaLiquido() - itemDigitacao.getValorLiquido());
            troca.setValorLiquidoTroca(troca.getValorLiquidoTroca() - Utils.arredondar((itemDigitacao.getValorLiquido() * troca.getIndenizacao()) / 100, 2, 2));
        } else if (quantidade > 0)
            itemTroca.add(itemDigitacao);

        itemDigitacao.setDesconto(0.0);
        itemDigitacao.setQuantidade(quantidade);
        itemDigitacao.setValorDesconto(0.0);
        itemDigitacao.setValorBruto(itemDigitacao.getValorUnit());

        itemDigitacao.setValorLiquido(Utils.arredondar((itemDigitacao.getValorBruto() - itemDigitacao.getValorDesconto()), 2, 2));

        inserirProdutoTroca(itemDigitacao);

        if ((quantidade == 0) && (retirarValores))
            itemTroca.remove(itemDigitacao);
        
		Utils.serializarObjeto(getBaseContext(), TROCA_SERIALIZAR, troca);
		Utils.serializarObjeto(getBaseContext(), ITENSTROCA_SERIALIZAR, itemTroca);        
	}
	
	private void retirarProdutoTroca(ItemDigitacao itemDigitacao) {
		troca.setNumeroItens((short) (troca.getNumeroItens() - 1));
		
		troca.setValorMercadoriaBruto(troca.getValorMercadoriaBruto() - itemDigitacao.getValorBruto());
		troca.setValorMercadoriaLiquido(troca.getValorMercadoriaLiquido() - itemDigitacao.getValorLiquido());
		troca.setValorLiquidoTroca(troca.getValorLiquidoTroca() - Utils.arredondar((itemDigitacao.getValorLiquido() * troca.getIndenizacao()) / 100, 2, 2));
		
		itemDigitacao.setValorUtilizaFlex(0.0);
	}
	
	private void inserirProdutoTroca(ItemDigitacao itemDigitacao) {
		itemDigitacao.setValorBruto(itemDigitacao.getValorUnitOriginal() * itemDigitacao.getQuantidade());
		itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade());

		troca.setValorMercadoriaBruto(troca.getValorMercadoriaBruto() + itemDigitacao.getValorBruto());
		troca.setValorMercadoriaLiquido(troca.getValorMercadoriaLiquido() + itemDigitacao.getValorLiquido());
		troca.setValorLiquidoTroca(troca.getValorLiquidoTroca() + Utils.arredondar((itemDigitacao.getValorLiquido() * troca.getIndenizacao()) / 100, 2, 2));

        if (itemDigitacao.getQuantidade() > 0)
            troca.setNumeroItens((short) (troca.getNumeroItens() + 1));
	}

	private ItemDigitacao itemAtual(int position) {
		ItemDigitacao itemDigitacao = null;
		if (trocados)
			itemDigitacao = itemTroca.get(position);
		else
			itemDigitacao = itemFiltrado.get(position);
		
		return itemDigitacao;
	}	
	
	private void atualizaAdapter() {
		if(!Utils.existeObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR)) {
			adapter = new ItemTrocaTabAdapter(getBaseContext(), new ArrayList<ItemDigitacao>());
			setListAdapter(adapter);
		} else {
			if (adapter == null) {
				setContentView(R.layout.lista_item_troca);								
				
				if (trocados)
					adapter = new ItemTrocaTabAdapter(getBaseContext(), this.itemTroca);
				else
					adapter = new ItemTrocaTabAdapter(getBaseContext(), this.itemFiltrado);
	
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				if (trocados)
					adapter.changeData(this.itemTroca);
				else
					adapter.changeData(this.itemFiltrado);
				
				getListView().setSelectionFromTop(savedIndex, savedY);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void inicializaEstruturaDigitacao() {
		try {
			itemCompleto = new ItemDigitacaoDAO(getBaseContext(), null).getEstruturaDigitacaoPedido(null, tabelaPreco, condicaoPagamento);
			
			itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
			itemTroca = new ArrayList<ItemDigitacao>();
			
			ArrayList<ItemDigitacao> itensRecuperados = null;
			ArrayList<ItemTroca> listaItensTrocaAlteracao = null;
			
			Object obj = Utils.recuperarObjetoSerializado(getBaseContext(), ITENSTROCA_SERIALIZAR);
			if (obj != null) {
				//Caso esteja recuperando um pedido que n?o foi finalizado corretamente
				itensRecuperados = (ArrayList<ItemDigitacao>) obj;
			} else {
				//Busca os itens do pedido caso seja uma altera??o, se for um pedido novo essa lista ficar? vazia
				listaItensTrocaAlteracao = new ItemTrocaDAO(getBaseContext()).getByTroca(troca.getIdTroca());
			}
			
			if (itensRecuperados != null || (listaItensTrocaAlteracao != null && listaItensTrocaAlteracao.size() > 0)) {
				if (listaItensTrocaAlteracao != null) {
					ItemTroca itemAux;
					int aux = 0;
					for (ItemDigitacao itemDigitacao : itemFiltrado) {
						if (aux == listaItensTrocaAlteracao.size())
							break;
						
						itemAux = null;
						for (ItemTroca itemAlterar : listaItensTrocaAlteracao) {
							if (itemAlterar.getIdProduto() == itemDigitacao.getProduto().getIdProduto()) {
								itemAux = itemAlterar;
								aux++;
								break;
							}
						}
						
						if (itemAux != null) {
							itemDigitacao.setQuantidade(itemAux.getQuantidade());
							itemDigitacao.setQuantidadeGravada(itemAux.getQuantidade());
							itemDigitacao.setDesconto(itemAux.getDesconto());
							itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
							itemDigitacao.setValorUnit(itemAux.getValorUnitario());
							itemDigitacao.setValorBruto(itemAux.getValorMercadoriaBruto());
							itemDigitacao.setValorLiquido(itemAux.getValorMercadoriaLiquido());
							
							itemTroca.add(itemDigitacao);
						}
					}
				} else if (itensRecuperados != null) {
					int aux = 0;
					ItemDigitacao itemAux;
					
					for (ItemDigitacao itemDigitacao : itemFiltrado) {
						if (aux == itensRecuperados.size())
							break;
						
						itemAux = null;
						for (ItemDigitacao itemRecuperar : itensRecuperados) {
							if (itemRecuperar.getProduto().getIdProduto() == itemDigitacao.getProduto().getIdProduto()) {
								itemAux = itemRecuperar;
								aux++;
								break;
							}
						}
						
						if (itemAux != null) {
							itemDigitacao.setQuantidade(itemAux.getQuantidade());
							itemDigitacao.setDesconto(itemAux.getDesconto());
							itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
							itemDigitacao.setValorFlex(itemAux.getValorFlex());
							itemDigitacao.setValorUtilizaFlex(itemAux.getValorUtilizaFlex());
							itemDigitacao.setValorGeraFlex(itemAux.getValorGeraFlex());
							itemDigitacao.setValorUnit(itemAux.getValorUnit());
							itemDigitacao.setValorBruto(itemAux.getValorBruto());
							itemDigitacao.setValorLiquido(itemAux.getValorLiquido());
							
							itemTroca.add(itemDigitacao);							
						}
					}
				}
			}
			
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), getString(R.string.erro_inicializando_estrurua_de_digitacao) + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	private void gravarTroca() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setTitle(getString(R.string.atencao));
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage("Deseja GRAVAR a troca?")
		.setCancelable(false)
		.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (itemTroca.size() == 0) {
					Toast.makeText(getBaseContext(), R.string.nao_eixstem_itens_para_troca_digitados_nesse_pedido_para_gravar, Toast.LENGTH_LONG).show();
					return;
				}
				
				if (parametro.getUtilizaTroca() == 2 && troca.getNrPedidoVenda() == 0) {
					Toast.makeText(getBaseContext(), R.string.voce_deve_informar_o_pedido_de_venda_na_capa_da_troca, Toast.LENGTH_LONG).show();
					return;
				}
				
				troca.setEnviado(false);
				troca.setDataEnvio(troca.getData());
				
				TrocaDAO trocaDAO = new TrocaDAO(getBaseContext());
				if ((parametro.getUtilizaTroca() == 2 && troca.getNrPedidoVenda() == 0) || (parametro.getUtilizaTroca() == 2 && trocaDAO.existeTrocaParaPedido(troca.getNrPedidoVenda())) || (parametro.getUtilizaTroca() == 1 && troca.getNrPedidoVenda() > 0 && trocaDAO.existeTrocaParaPedido(troca.getNrPedidoVenda()))) {
					Toast.makeText(getBaseContext(), String.format(getString(R.string.ja_existe_uma_troca_digitada_para_o_pedido), troca.getNrPedidoVenda()), Toast.LENGTH_LONG).show();
					return;
				}
				
				List<ItemTroca> itensTroca = new ArrayList<ItemTroca>(itemTroca.size());
				short numeroItem = 1;
				for (ItemDigitacao itemDigitacao : itemTroca) {
					ItemTroca i = new ItemTroca();
					i.setIdTroca(troca.getIdTroca());
					i.setIdProduto(itemDigitacao.getProduto().getIdProduto());
					i.setNumeroItem(numeroItem++);
					i.setUnitario(true);
					i.setQuantidade(itemDigitacao.getQuantidade());
					i.setValorUnitario(itemDigitacao.getValorUnit());
					i.setValorMercadoriaBruto(itemDigitacao.getValorBruto());
					i.setDesconto(itemDigitacao.getDesconto());
					i.setValorDesconto(itemDigitacao.getValorDesconto());
					i.setValorMercadoriaLiquido(itemDigitacao.getValorLiquido());
					
					itensTroca.add(i);
				}
				
				if (trocaDAO.salvarTroca(troca, itensTroca)) {
					Toast.makeText(getBaseContext(), "Troca gravada com sucesso!", Toast.LENGTH_LONG).show();
					Utils.apagarObjetoSerializado(getBaseContext());
					
					setResult(Activity.RESULT_OK);
					finish();
				} else {
					Toast.makeText(getBaseContext(), R.string.erro_durante_a_gravacao_da_troca, Toast.LENGTH_LONG).show();
				}
			}
		})
		.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		builder.show();
	}
	
	public boolean confirmaSaida() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setTitle(getString(R.string.atencao));
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage(R.string.cancelar_alteracoes_troca)
		.setCancelable(false) 
		.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int id) {
			Utils.apagarObjetoSerializado(getBaseContext());

			setResult(Activity.RESULT_OK);
			finish();
		} 
		})
		.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int id) {
			dialog.cancel();
		}
		});
		
		builder.show(); 
		return false;
	}
	
	private void showDialogFiltroItem() {
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.popupfiltroitem);
		dialog.setTitle("Filtro de Produtos");
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		final EditText edtPesquisa = (EditText) dialog.findViewById(R.idPopupFiltroItem.edtPesquisa);
		final Spinner spnFornecedor = (Spinner) dialog.findViewById(R.idPopupFiltroItem.spnFornecedor);
		final Button btnPesquisa = (Button) dialog.findViewById(R.idPopupFiltroItem.btnPesquisa);
		final Button btnCancela = (Button) dialog.findViewById(R.idPopupFiltroItem.btnCancela);
		
		spnFornecedor.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, new FornecedorDAO(getBaseContext(), null).getListaNomes()));
		
		btnPesquisa.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean bPesquisaCodigo = true;
				long codigoProduto = 0;
				int index = -1;
				
				try {
					codigoProduto = Long.parseLong(edtPesquisa.getText().toString());
					
				} catch (NumberFormatException e) {
					bPesquisaCodigo = false;
				}
				
				if (bPesquisaCodigo) {
					itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
					for (int i = 0; i < itemFiltrado.size(); i++) {
						if (itemFiltrado.get(i).getProduto().getIdProduto() == codigoProduto) {
							index = i;
							break;
						}
					}
					if (index == -1) {
						Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_produto_com_o_codigo_informado, Toast.LENGTH_SHORT).show();
					} 
				} else {
					String pesquisa = edtPesquisa.getText().toString().toUpperCase();
					if (pesquisa.trim().equals("") && spnFornecedor.getSelectedItemPosition() == 0) {
						itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
					} else {
						long idFornecedor = 0;
						if (spnFornecedor.getSelectedItemPosition() > 0) {
							idFornecedor = new FornecedorDAO(getBaseContext(), null).getIdByName(spnFornecedor.getSelectedItem().toString());
						}
						
						String[] trechos = pesquisa.split(",");
						itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto.size());
						
						boolean achou = false;
						for (ItemDigitacao item : itemCompleto) {
							achou = false;
							if (trechos.length > 0) {
								for (int i = 0; i < trechos.length; i++) {
									achou = item.getProduto().getDescricao().contains(trechos[i].trim());
									if (!achou)
										break;
									if (idFornecedor > 0)
										achou = item.getProduto().getIdFornecedor() == idFornecedor;
									if (!achou)
										break;
								}
							} else {
								achou = item.getProduto().getIdFornecedor() == idFornecedor;
							}
							
							if (achou)
								itemFiltrado.add(item);
						}
					}
					
					if (itemFiltrado.size() == 0) {
						itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
						
						Toast.makeText(getBaseContext(), R.string.nao_foi_encontrado_nenhum_produto_com_o_conteudo_pesquisado, Toast.LENGTH_LONG).show();
					}
				}
				
				atualizaAdapter();
				
				if (bPesquisaCodigo && index != -1) { //Achou produto pelo c?digo
					getListView().setSelection(index);
				}
				
				dialog.dismiss();
			}
		});
		
		btnCancela.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
}
