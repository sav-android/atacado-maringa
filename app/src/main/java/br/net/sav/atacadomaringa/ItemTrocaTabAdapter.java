package br.net.sav.atacadomaringa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Produto;

public class ItemTrocaTabAdapter extends BaseAdapter implements SectionIndexer {
	private Context ctx ;
	private List<ItemDigitacao> lista;

	List<String> myElements = null ;
	HashMap<String, Integer> alphaIndexer ;
	String[] sections ;

	public ItemTrocaTabAdapter(Context ctx, List<ItemDigitacao> itemTroca) {
		this.ctx = ctx ;
		this.lista = itemTroca ;
		
		myElements = new ArrayList<String>(itemTroca.size());
		
		for(int i=0; i< itemTroca.size() ; i++ ) {
			myElements.add(itemTroca.get(i).getProduto().getDescricao()); 
		}
		
		alphaIndexer = new HashMap<String, Integer>();
		
		int size = itemTroca.size() ;
		
		for(int i = size - 1; i>= 0 ; i-- ) {
			String element = myElements.get(i);
			alphaIndexer.put(element.substring(0,1), i);
		}
		
		Set<String> keys = alphaIndexer.keySet();
		
		Iterator<String> it = keys.iterator() ;
		ArrayList<String> keyList =  new ArrayList<String>();
		
		while(it.hasNext()) {
			String key = it.next();
			keyList.add(key);
		}
		
		Collections.sort(keyList);
		
		sections = new String[keyList.size()];
		
		keyList.toArray(sections);	
	}
	
	public void changeData(List<ItemDigitacao> itemTroca) {
		this.lista = itemTroca ;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.lista_item_troca, null);
		}
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		ItemDigitacao item = lista.get(position);
		Produto p = item.getProduto();

		TextView txvProduto = (TextView) view.findViewById(R.idItemTroca.txvProduto);
		txvProduto.setText(p.getIdProduto() + " - " + p.getDescricao());
		txvProduto.setTextColor(Color.GREEN);
		txvProduto.setTypeface(Typeface.DEFAULT_BOLD);
		
		TextView txvEmbalagem = (TextView) view.findViewById(R.idItemTroca.txvEmbalagem);
		txvEmbalagem.setText("Emb: " + p.getDescricaoEmbalagem());
		txvEmbalagem.setTypeface(Typeface.DEFAULT_BOLD);
		
		TextView txvQtdeEmb = (TextView) view.findViewById(R.idItemTroca.txvQtdeEmb);
		txvQtdeEmb.setText("Qt.Emb: " + String.valueOf(p.getQuantidadeEmbalagem()));
		txvQtdeEmb.setTypeface(Typeface.DEFAULT_BOLD);
		
		TextView txvValorEmb = (TextView) view.findViewById(R.idItemTroca.txvValorEmb);
		txvValorEmb.setText("Valor R$: " + String.format("%.2f", item.getValorUnitOriginal()));
		txvValorEmb.setTypeface(Typeface.DEFAULT_BOLD);
		
		TextView txvQtdeTroca = (TextView) view.findViewById(R.idItemTroca.txvQtdeTroca);
		if (item.getQuantidade() > 0) {
			txvQtdeTroca.setVisibility(View.VISIBLE);
			txvQtdeTroca.setText("Qt.Troca: " + String.valueOf(item.getQuantidade()));
			txvQtdeTroca.setTypeface(Typeface.DEFAULT_BOLD);
		} else {
			txvQtdeTroca.setVisibility(View.GONE);
		}
		
		TextView txvValorLiquido = (TextView) view.findViewById(R.idItemTroca.txvValorLiquido);
		if (item.getQuantidade() > 0) {
			txvValorLiquido.setVisibility(View.VISIBLE);
			txvValorLiquido.setText(ctx.getString(R.string.valor_liq) + String.format("%.2f", item.getValorLiquido()));
			txvValorLiquido.setTypeface(Typeface.DEFAULT_BOLD);
		} else {
			txvValorLiquido.setVisibility(View.GONE);
		}
		
		ImageView imgTrocado = (ImageView) view.findViewById(R.idItemTroca.imgTroca);
		
		if(item.getQuantidade() > 0)
			imgTrocado.setVisibility(View.VISIBLE);
		else 
			imgTrocado.setVisibility(View.GONE);
		
		return view;
	}

	@Override
	public int getPositionForSection(int section) {
		String letter = sections[section];
		return alphaIndexer.get(letter);
	}

	@Override
	public int getSectionForPosition(int position) {
		return 0;
	}

	@Override
	public Object[] getSections() {
		return sections;
	}

}
