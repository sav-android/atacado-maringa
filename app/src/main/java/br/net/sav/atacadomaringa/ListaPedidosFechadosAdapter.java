package br.net.sav.atacadomaringa;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dialog.ValidaSenhaDialog;
import br.net.sav.modelo.AlterarObjeto;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Tabela;
import br.net.sav.utils.ConfigSemafaro;

public class ListaPedidosFechadosAdapter extends BaseAdapter {

    private Context mContext;
    private Activity parentActivity;
    private Parametro parametro;
    private List<Pedido> listaPedidos;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public ListaPedidosFechadosAdapter(Context mContext, List<Pedido> listaPedidos, Activity parentActivity) {
        this.mContext = mContext;
        this.listaPedidos = listaPedidos;
        this.parentActivity = parentActivity;
    }

    @Override
    public int getCount() {
        return listaPedidos.size();
    }

    public void changeData(List<Pedido> listaPedidos) {
        this.listaPedidos = listaPedidos;
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return listaPedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        parametro = new ParametroDAO(mContext, null).get();

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.linha_pedido_fechado_adapter, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            convertView.setBackgroundResource(R.drawable.alterselector1);
        } else {
            convertView.setBackgroundResource(R.drawable.alterselector2);
        }

        Pedido pedido = listaPedidos.get(position);
        Tabela tabela = new TabelaDAO(mContext, null).get(pedido.getIDTabela());

        popularPedidosFechadosNaView(holder, pedido, tabela);

        notifyDataSetChanged();

        return convertView;
    }

    private void popularPedidosFechadosNaView(final ViewHolder holder, final Pedido pedido, final Tabela tabela) {

        holder.txvPedido.setText(mContext.getString(R.string.pedido_nr) + String.valueOf(pedido.getIDPedido()));
        holder.txvCliente.setText(String.format("%d - %s", pedido.getIDCliente(), pedido.getNomeCliente()));
        holder.txvDataPedido.setText("DataUltimaCompra: " + sdf.format(pedido.getDataPedido()));
        holder.txvTipoPedido.setText(String.format("Tipo Pedido: %s", pedido.getTipoPedido()));
        holder.txvValorBruto.setText(String.format("Valor Bruto: %.4f", pedido.getTotalGeral()));
        holder.txvValorLiquido.setText(String.format(mContext.getString(R.string.valor_liquido2), pedido.getTotalLiquido()));
        holder.txvGeraFlex.setText(String.format(mContext.getString(R.string.gerara_flex), pedido.getValorGeraFlex()));
        holder.txvVinculoPedido.setText(String.format(mContext.getString(R.string.bonificacao_vinculada) + " " + mContext.getString(R.string.pedido_nr) + pedido.getVinculoPedido()));
        holder.txvRentabilidadePedido.setText(String.format("Perc. Rentabilidade Pedido: %.2f%%", pedido.getRentabilidade()));

        if (pedido.isEnviado()) {
            holder.txvDataEnvio.setText("DataUltimaCompra Envio: " + sdf.format(pedido.getDataEnvio()));
        } else {
            holder.txvDataEnvio.setText(R.string.data_envio_nao_enviado);
        }


        holder.checkBoxAberto.setOnCheckedChangeListener(null);
        holder.checkBoxAberto.setChecked(pedido.isAberto());
        holder.checkBoxAberto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (validarRentabilidade(pedido, tabela)) {

                    SelecionarPedidoParaTranmissao(isChecked, holder, pedido);

                } else {
                    SelecionarPedidoParaTranmissao(isChecked, holder, pedido);
                }
            }
        });

        if (pedido.isBonificacao(mContext)) {
            holder.txvVinculoPedido.setVisibility(View.VISIBLE);
        } else
            holder.txvVinculoPedido.setVisibility(View.GONE);

        if (pedido.getIDTabela() == tabela.getIdTabela()) {
            if (tabela.isUltilizaRentabilidade()) {
                visualizarRentabilidadePedido(pedido, holder);
            } else {
                holder.txvRentabilidadePedido.setVisibility(View.GONE);
            }
        }

    }

    private void SelecionarPedidoParaTranmissao(boolean isChecked, ViewHolder holder, Pedido pedido) {

        for (int i = 0; i < listaPedidos.size(); i++) {
            if (listaPedidos.get(i).isBonificacao(mContext) && pedido.getIDPedido() == listaPedidos.get(i).getVinculoPedido()) {
                listaPedidos.get(i).setAberto(isChecked);
                holder.checkBoxAberto.setChecked(listaPedidos.get(i).isAberto());
                notifyDataSetChanged();
            }

            if (!listaPedidos.get(i).isBonificacao(mContext) && pedido.getIDPedido() == listaPedidos.get(i).getVinculoBonificacao()) {
                listaPedidos.get(i).setAberto(isChecked);
                holder.checkBoxAberto.setChecked(listaPedidos.get(i).isAberto());
                notifyDataSetChanged();
            }
        }

        pedido.setAberto(isChecked);
        notifyDataSetChanged();
    }

    private void msgAvisoPercRentabilidadeAbaixoDoMinimo(String mensagem, final Pedido pedido) {
        final AlertDialog.Builder dlog = new AlertDialog.Builder(parentActivity);
        dlog.setTitle(mContext.getString(R.string.atencao));
        dlog.setMessage(mensagem);
        dlog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    new ValidaSenhaDialog(mContext, parentActivity).showDialogSenhaDesbloquearPedidoRentabilidade(pedido, new AlterarObjeto() {
                        @Override
                        public void alterar(Pedido pedido, boolean alterado) {
                            if (!alterado) {
                                pedido.setAberto(false);
                            }
                                notifyDataSetChanged();
                        }
                    });
                dialog.dismiss();

            }

        });
        dlog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pedido.setAberto(false);
                notifyDataSetChanged();
                dialog.dismiss();
                return;

            }
        });
        dlog.setCancelable(false);
        dlog.show();
    }

    public String mensagem(Pedido pedido) {
        if (pedido.getPercRentabilidadeLiberado() > 0) {
            return String.format("Rentabilidade do pedido abaixo do minimo permitido (LIBERADO POR SENHA): %.2f%%", pedido.getPercRentabilidadeLiberado());
        } else {
            return String.format("Rentabilidade do pedido abaixo do minimo permitido: %.2f%%, deseja desbloquear por senha?", parametro.getPercentualRentabilidadeMinimaDoPedido());
        }
    }

    private boolean validarRentabilidade(Pedido pedido, Tabela tabela) {
        if (!parametro.isSolicitarSenhaRentabilidade()) {
            if (tabela.isUltilizaRentabilidade()) {

                if (pedido.getRentabilidade() < parametro.getPercentualRentabilidadeMinimaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0
                        || pedido.getPercRentabilidadeLiberado() > 0 && pedido.getRentabilidade() <= pedido.getPercRentabilidadeLiberado()) {
                    msgAvisoPercRentabilidadeAbaixoDoMinimo(mensagem(pedido), pedido);

                    return true;

                } else {
                    return true;
                }
            } else
                pedido.setRentabilidade(0d);
        }
        return false;
    }
    private void visualizarRentabilidadePedido(Pedido pedido, ViewHolder viewHolder) {
        if (parametro.getVisualizarRentabilidade()) {
            viewHolder.txvRentabilidadePedido.setVisibility(View.GONE);

        } else {
            viewHolder.txvRentabilidadePedido.setVisibility(View.VISIBLE);
            viewHolder.txvRentabilidadePedido.setTextColor(ConfigSemafaro.getTextColorSemafaroPedido(parametro, pedido));
        }
    }

    class ViewHolder {
        TextView txvPedido;
        CheckBox checkBoxAberto;
        TextView txvCliente;
        TextView txvDataPedido;
        TextView txvTipoPedido;
        TextView txvValorBruto;
        TextView txvValorLiquido;
        TextView txvGeraFlex;
        TextView txvDataEnvio;
        TextView txvVinculoPedido, txvRentabilidadePedido;

        public ViewHolder(View item) {
            txvPedido = (TextView) item.findViewById(R.id.textPedidoFechado);
            checkBoxAberto = (CheckBox) item.findViewById(R.id.checkboxFechado);
            txvCliente = (TextView) item.findViewById(R.id.textClientefechado);
            txvDataPedido = (TextView) item.findViewById(R.id.textViewDataFechado);
            txvTipoPedido = (TextView) item.findViewById(R.id.textViewTipoPedido);
            txvValorLiquido = (TextView) item.findViewById(R.id.textValorLiquidoFechado);
            txvValorBruto = (TextView) item.findViewById(R.id.textValorBrutoFechado);
            txvGeraFlex = (TextView) item.findViewById(R.id.textGeraFlex);
            txvDataEnvio = (TextView) item.findViewById(R.id.textDataEnvio);
            txvVinculoPedido = (TextView) item.findViewById(R.id.txvVinculoPedido);
            txvRentabilidadePedido = (TextView) item.findViewById(R.id.txvRentabilidadePedido);

        }
    }
}
