package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import br.net.sav.AtualizacaoActivity;
import br.net.sav.CadastroProvedorActivity;
import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.interfaces.IAoRegistrarVendedor;
import br.net.sav.IntegradorWeb.repository.VendedorRepository;
import br.net.sav.UIHelper;
import br.net.sav.Utils;
import br.net.sav.central_manager.mqtt.MQTTConfig;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dialog.DialogRegistrarVendedor;
import br.net.sav.dialog.GerarSenhaDialog;
import br.net.sav.dialog.UploadBancoDadosDialog;
import br.net.sav.enumerador.TipoSenha;
import br.net.sav.modelo.Parametro;
import br.net.sav.service.VerificaParametro;
import br.net.sav.util.dao.ProvedorDAO;
import br.net.sav.util.modelo.Provedor;
import br.net.sav.utils.ToastUtils;

import static android.support.v7.app.AppCompatDelegate.MODE_NIGHT_YES;
import static br.net.sav.IntegradorWeb.dto.Vendedor.buscarVendedorRegistradoUtils;
import static br.net.sav.IntegradorWeb.dto.Vendedor.salvarRegistroVendedor;
import static br.net.sav.dao.PedidoDAO.buscarHistoricos;
import static br.net.sav.service.VendedorService.verificarVendedor;
import static br.net.sav.service.VerificaAtualizacao.REQUEST_CODE_WRITE_SETTINGS;
import static br.net.sav.service.VerificaAtualizacao.verificaPermissaoDeAtualizacao;
import static br.net.sav.utils.IntentUtils._startActivitySincronizacao;
import static br.net.sav.utils.PermissionUtils.permissionSLL;
import static br.net.sav.utils.ShearedPreferenceUltils.aparelho;
import static br.net.sav.utils.ShearedPreferenceUltils.getPreferInt;
import static br.net.sav.utils.ShearedPreferenceUltils.getPreferString;
import static br.net.sav.utils.ShearedPreferenceUltils.setPreferInt;
import static br.net.sav.utils.ShearedPreferenceUltils.setPreferString;
import static br.net.sav.utils.ToastUtils.showDialogSobreAtacado;


public class MenuPrincipal extends Activity {
    public static final String INTENT = "MenuPrincipal";
    final SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");

    private ArrayList<String> listOpcoes;
    private ArrayList<Integer> listImagens;

    private GridView gridView;
    private GridViewAdapter mAdapter;

    private boolean senhaCorreta;

    private EditText edtSenha;
    private Button btnOk;
    private Button btnCancelar;

    private Parametro parametro;
    private Date dataAtual;
    private Date dataDispositivo;
    private Provedor provedor;
    public static Context context;

    protected static final int ATUALIZAR_VERSAO = 4;

    public static final String USUARIO = "atacadomaringa2";
    public static final String SENHA = "43efSyb1";
    public static final String NOME_ARQUIVO = "Atacado Maringa.apk";
    public static String dadosAparelho;
    private Vendedor vendedorRegistrado;


    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.menuprincipal);
        dadosAparelho = "Sem Dados";

        parametro = new ParametroDAO(getBaseContext(), null).get();
        vendedorRegistrado = buscarVendedorRegistradoUtils(getBaseContext());

        OpcoesMenu();

        mAdapter = new GridViewAdapter(this, listOpcoes, listImagens);

        gridView = (GridView) findViewById(R.id.gridView1);
        gridView.setAdapter(mAdapter);


        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                switch (position) {
                    case 0: // ATENDIMENTO
                        if (verificaSeOVendedorAtivo()) return;

                        _startActivity("ListaClientes", Principal.CATEGORIA);

                        break;

                    case 1: // ACOMPANHAMENTO
                        if (verificaSeOVendedorAtivo()) return;
                        showDialogAcompanamento();
                        break;

                    case 2: // CONSULTAS
                        if (verificaSeOVendedorAtivo()) return;
                        showDialogConsultas();
                        break;

                    case 3: // ENVIA / RECEBE DADOS
                        //TODO COMENTAR PARA ACESSO A API
                       // if (verificaSeOVendedorAtivo()) return;
                        _startActivity("Transmissao", Principal.CATEGORIA);
                        break;

                    case 4: // EMAIL
                        if (verificaSeOVendedorAtivo()) return;

                        showDialogEmail();

                        break;
                    case 5: // UTILIT�RIOS
                        if (verificaSeOVendedorAtivo()) return;
                        showDialogUtilitarios();
                        break;

                    case 6: // SAIR
                        confirmarSaida();
                        break;
                }
            }
        });
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES);
        verificaSeOVendedorAtivo();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal_resumo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.actionMenuPrincipal_resumo) {
            Intent it = new Intent(this, GraficoMetasActivity.class);
            this.startActivity(it);
            Toast.makeText(this, "Deu Certo", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        parametro = new ParametroDAO(getBaseContext(), null).get();
        vendedorRegistrado = buscarVendedorRegistradoUtils(getBaseContext());

        // verificarRegistroVendedor();
        excluirNotificacoesVencidas();
        exibirNotificacoes();

        permissionSLL(this);

        verificarVendedor(getBaseContext(), vendedorRegistrado);

        if (vendedorRegistrado!= null) {
            MQTTConfig.configurar(this, vendedorRegistrado, null);
        }
    }

    private void excluirNotificacoesVencidas() {
        br.net.sav.modelo.Notificacao.excluirNotificacoesVencidas(this);
    }

    private void exibirNotificacoes() {
        br.net.sav.modelo.Notificacao.exibirNotificacoes(this);
    }


    private void verificarRegistroVendedor() {

        new DialogRegistrarVendedor().registra(MenuPrincipal.this, vendedorRegistrado, new IAoRegistrarVendedor() {

            @Override
            public void aoRegistrar(final Vendedor vendedor) {


               if(salvarRegistroVendedor(getBaseContext(),vendedor, Build.MODEL,Integer.toString(Build.VERSION.SDK_INT))) {
                   setPreferString(getBaseContext(), Build.MODEL, aparelho);

                   VerificaParametro.getVerificarParametroVendedorAPI(MenuPrincipal.this, vendedor.getToken(), new ParametroDAO(getBaseContext(), new DBHelper(getBaseContext()).getReadableDatabase()),
                           new VendedorRepository.IAoVerificarParametro() {
                               @Override
                               public void encontrou(boolean ok) {
                                   if (ok) {
                                       setPreferInt(getBaseContext(), "PARAMETRO_ENCONTRADO", 1);
                                       runOnUiThread(new Runnable() {
                                           public void run() {
                                               buscarHistoricos(MenuPrincipal.this, vendedor);
                                           }
                                       });
                                   }
                               }
                           });
                   vendedorRegistrado = buscarVendedorRegistradoUtils(getBaseContext());
               }


            }

            @Override
            public void aoFalhar(String mensagemErro) {
                Toast.makeText(getBaseContext(), mensagemErro, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        confirmarSaida();
    }

    public void OpcoesMenu() {
        listOpcoes = new ArrayList<String>();

        listOpcoes.add("Atendimento");
        listOpcoes.add("Acompanhamento");
        listOpcoes.add("Consultas");
        listOpcoes.add("Envia/recebe dados");
        listOpcoes.add("Email");
        listOpcoes.add(getString(R.string.utilitarios));
        listOpcoes.add("Sair");

        listImagens = new ArrayList<Integer>();

        listImagens.add(R.drawable.atendimento);
        listImagens.add(R.drawable.pendencias);
        listImagens.add(R.drawable.pedidos);
        listImagens.add(R.drawable.email);
        listImagens.add(R.drawable.comunicacao);
        listImagens.add(R.drawable.utilitarios);
        listImagens.add(R.drawable.exit1);
    }

    public boolean confirmarSaida() {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setMessage("Deseja realmente sair do Sistema ?");
        msg.setTitle(R.string.atencao);
        msg.setIcon(android.R.drawable.ic_dialog_alert);
        msg.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
        msg.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = msg.create();
        alert.show();

        return false;
    }

    public void showDialogConsultas() {

        if (verificaSeOVendedorAtivo()) return;

        final CharSequence[] itens = {"Pedidos", getString(R.string.titulos), getString(R.string.precos)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this, (Build.VERSION.SDK_INT >= 28) ? br.net.sav.R.style.DialogStyle : 0);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("Consultas - " + getString(R.string.selecione_opcao_desejada));
        builder.setItems(itens, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        startActivity(new Intent(MenuPrincipal.this, PedidoListaActivity.class));
                        break;

                    //TODO Liberar essa op��o ap�s desenvolver a Troca de Pedido
				/*case 1:
					_startActivity(TrocaListaActivity.ACTION, Principal.CATEGORIA);
					break;*/

                    case 1:
                        _startActivity("ListaPendencias", Principal.CATEGORIA);
                        break;

                    case 2:
                        _startActivity(ConsultaPrecoListActivity.INTENT, Principal.CATEGORIA);
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void buscarHistoricoDePedidos() {
        if (vendedorRegistrado != null) {
            List<br.net.sav.modelo.Pedido> pedidosNaoEnviado = new PedidoDAO(getBaseContext(), null).listaPedidos(PedidoDAO.NAO_ENVIADOS);
            List<br.net.sav.modelo.Pedido> pedidosEnviado = new PedidoDAO(getBaseContext(), null).listaPedidos(PedidoDAO.ENVIADOS);

            if (vendedorRegistrado.getToken() != null && vendedorRegistrado.getId() > 0) {
                if (pedidosEnviado.isEmpty() && pedidosNaoEnviado.isEmpty()) {
                    buscarHistoricos(getBaseContext(), vendedorRegistrado);
                }
            }
        }
    }

    public void showDialogUtilitarios() {
        CharSequence[] itens = {"Marcar dados Reenviar", "Calculadora", "Configurar Provedor", "Configurar Servidor FTP", getString(R.string.verificar_atualizacoes), "Enviar Backup", "Sobre"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, (Build.VERSION.SDK_INT >= 28) ? R.style.DialogStyle : 0);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(getString(R.string.utilitarios2) + getString(R.string.selecione_opcao_desejada));
        if (parametro != null)
            if (parametro.isSupervisor()) {
                itens = new CharSequence[]{"Marcar dados Reenviar", "Calculadora", "Configurar Provedor", "Configurar Servidor FTP", getString(R.string.verificar_atualizacoes), "Enviar Backup", "Sobre", "Admin"};
            }

        builder.setItems(itens, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        showDialogMarcarDadosReenviar();

                        break;
                    case 1:
                        iniciarCalculadora();

                        break;
                    case 2:
                        if (parametro != null)
                            showCustomDialogSenha();
                        else
                            Toast.makeText(getBaseContext(), getString(R.string.voce_precisa_do_arquivo_de_parametros_para_configurar_o_provedor), Toast.LENGTH_LONG).show();

                        break;
                    case 3: // Configurar Servidor FTP
                        Intent i = new Intent("ConfiguracaoFtpPreference");
                        startActivity(i);

                        break;
                    case 4:
                        verificaPermissaoDeAtualizacao(MenuPrincipal.this, getContentResolver());
                        //verificarAtualizacoes();
                        break;
                    case 5:
                        enviarBackup();

                        break;

                    case 6:
                        try {
                            showDialogSobreAtacado(MenuPrincipal.this, "SAV - Atacado Maringa");
                        } catch (NameNotFoundException e) {
                            new UIHelper(MenuPrincipal.this).showErrorDialog(e.getMessage());
                        }

                        break;
                    case 7:
                        showDialogOpcoesAdmin(MenuPrincipal.this);
                        break;
//                    case 8:
//                        buscarHistoricoDePedidos();
//                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public  void verificarAtualizacoes() {
        Intent it = new Intent("Atualizar");
        it.addCategory(Principal.CATEGORIA);
        it.putExtra("USUARIO", USUARIO);
        it.putExtra("SENHA", SENHA);
        it.putExtra("NOME_ARQUIVO", NOME_ARQUIVO);
        startActivityForResult(it, ATUALIZAR_VERSAO);
    }

    private void enviarBackup() {

        String nomeAruivoUpload = USUARIO;
        Parametro parametro = new ParametroDAO(this, null).get();

        new UploadBancoDadosDialog(this,
                "/data/br.net.sav.atacadomaringa/databases/Atacado Maringa.db", nomeAruivoUpload + "-" + parametro.getIdVendedor())
                .show();
    }

    private void showDialogOpcoesAdmin(Context context) {
        AlertDialog.Builder builder;
        final CharSequence[] itens = {
                TipoSenha.RENTABILIDADE_MINIMA.toString(),
                TipoSenha.CLIENTE_BLOQUEADO.toString(),
                TipoSenha.PEDIDO_BLOQUEADO.toString(),
                TipoSenha.TRABALHAR_SEM_CONEXAO.toString(),
                TipoSenha.ACESSO_BLOQUEADO.toString()
        };

        builder = new AlertDialog.Builder(context);

        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle(R.string.admin_selecione_opcao);
        builder.setItems(itens, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                GerarSenhaDialog gerarSenhaDialog = new GerarSenhaDialog(MenuPrincipal.this, TipoSenha.getById(item));
                gerarSenhaDialog.dialogMsgGerarSenha();
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialogMarcarDadosReenviar() {
        final PedidoDAO pedidoDAO = new PedidoDAO(getBaseContext(), null);
        final List<String> listDatas = pedidoDAO.listaDatasEnvio();
        final List<Date> listDatasMarcar = new ArrayList<Date>();
        final CharSequence[] datas = listDatas.toArray(new CharSequence[listDatas.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setTitle("Escolha as datas:");
        builder.setMultiChoiceItems(datas, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item, boolean isChecked) {
                Date dataMarcada = null;
                try {
                    dataMarcada = Utils.sdfDataHoraDb.parse(listDatas.get(item));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (isChecked) {
                    listDatasMarcar.add(dataMarcada);
                } else {
                    listDatasMarcar.remove(dataMarcada);
                }
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listDatasMarcar.size() > 0) {
                    pedidoDAO.marcarRetransmitir(listDatasMarcar);

                    Toast.makeText(getBaseContext(), getString(R.string.dados_marcados_para_retransmissao_com_sucesso), Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.nenhuma_data_foi_selecionada_para_retransmissao), Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showCustomDialogSenha() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup_senhaconfigurar);
        dialog.setTitle(String.format("Confg. Repr.: %d ", parametro.getIdVendedor()));
        edtSenha = (EditText) dialog.findViewById(R.idSenhaConfigurar.txtSenha);
        btnOk = (Button) dialog.findViewById(R.idSenhaConfigurar.btnConfirmar);
        btnCancelar = (Button) dialog.findViewById(R.idSenhaConfigurar.btnCancelar);
        dialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                senhaCorreta = informarSenha(parametro.getIdVendedor(), edtSenha.getText().toString().trim());
                if (!senhaCorreta)
                    edtSenha.setError(getString(R.string.senha_invalida));
                else {
                    dialog.dismiss();

                    Intent it = new Intent("Provedor");
                    it.putExtra(CadastroProvedorActivity.KEY_IDPROVEDOR, 1);
                    it.putExtra(CadastroProvedorActivity.KEY_ISMOBILE, true);
                    it.putExtra("ProvedorCadastrado", provedor);
                    it.putExtra("Bloqueio", true);
                    it.addCategory(Principal.CATEGORIA);
                    startActivity(it);
                }

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        });
    }

    public String currentVersion() throws NameNotFoundException {
        return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
    }

    public void _startActivity(String intent, String category) {
        Intent it = new Intent(intent);
        it.addCategory(category);
        startActivity(it);
    }

    public void iniciarCalculadora() {
        String CALCULATOR_PACKAGE = "com.android.calculator2";
        String CALCULATOR_CLASS = "com.android.calculator2.Calculator";

        try {
            Intent it = new Intent();
            it.setAction(Intent.ACTION_MAIN);
            it.addCategory(Intent.CATEGORY_LAUNCHER);
            it.setComponent(new ComponentName("com.sec.android.app.popupcalculator", "com.sec.android.app.popupcalculator.Calculator"));
            startActivity(it);

        } catch (Exception e) {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_MAIN);
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            i.setComponent(new ComponentName(CALCULATOR_PACKAGE, CALCULATOR_CLASS));
            startActivity(i);
        }
    }

    public boolean informarSenha(long idVendedor, String senhaDigitada) {
        Calendar data = GregorianCalendar.getInstance();

        long senha = idVendedor * 2;
        senha += data.get(Calendar.YEAR) * 3;
        senha += (data.get(Calendar.MONTH) + 1) * 5;
        senha += data.get(Calendar.DATE) * 7;

        String senhaApurada = String.format("%s", String.valueOf(senha) + Utils.digitoVerificador(senha, 11, 5));

        return senhaCorreta = (senhaApurada.trim().equals(senhaDigitada.trim()));
    }

    private void showDialogEmail() {
        if (verificaSeOVendedorAtivo()) return;

        Provedor provedor = new ProvedorDAO(getBaseContext()).get();
        if (provedor != null) {
            Utils.showDialogEmail(this, Principal.CATEGORIA, provedor.getUsuarioPop(), provedor.getSenhaPop(), provedor.getUsuarioPop());
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.os_parametros_do_servidor_nao_foram_cadastrados), Toast.LENGTH_LONG).show();
        }
    }

    private boolean verificaSeOVendedorAtivo() {
        if (vendedorRegistrado == null) {
            vendedorRegistrado = new Vendedor();
        }

        if (vendedorRegistrado.getToken() == null || vendedorRegistrado.getId() == 0 ||
                vendedorRegistrado.getToken().trim().equals("") && vendedorRegistrado.getId() == 0) {
            verificarRegistroVendedor();
            ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.registro_app_msg));
            return true;
        }

        if (!vendedorRegistrado.isAtivo() && vendedorRegistrado.getToken() != null && vendedorRegistrado.getId() > 0) {
            ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.vendedor_desativado_favor_fazer_uma_trnasicao));
            return true;
        }

        if (vendedorRegistrado.isAtivo() && vendedorRegistrado.getToken() != null && vendedorRegistrado.getId() > 0 && !Build.MODEL.trim().equals(getPreferString(getBaseContext(), aparelho, "").trim())) {
            ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.licenca_desativada));
            return true;
        }
        if (parametro == null){
            if(getPreferInt(getBaseContext(), "PARAMETRO_ENCONTRADO",0) > 0){
                _startActivitySincronizacao(this,"Transmissao", Principal.CATEGORIA);
                ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.parametro_encontrado));
            }else {
                ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.parametro_nao_encontrado));
            }
            return true;
        }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ATUALIZAR_VERSAO:
                if (resultCode == 10) {
                    updateVersion();
                }
                break;

            case REQUEST_CODE_WRITE_SETTINGS:
                if (Settings.System.canWrite(this)) {
                    Log.d("TAG", "CODE_WRITE_SETTINGS_PERMISSION success");
                    Utils.setAutoOrientationEnabled(getContentResolver(), false);
                    verificarAtualizacoes();
                }

                break;
        }

    }


    public void updateVersion() {
        Intent it = new Intent("Atualizacao");
        it.putExtra("nomeApk", NOME_ARQUIVO);
        it.putExtra(AtualizacaoActivity.AUTHORITY, "br.net.sav.atacadomaringa.fileprovider");
        startActivity(it);
    }

    private void showDialogAcompanamento() {

        if (verificaSeOVendedorAtivo()) return;

        CharSequence[] itens = new CharSequence[]{"Aniversariantes", getString(R.string.promocoes_da_semana), getString(R.string.relatorios_prontos), "Produto Sem Venda", "Cliente Sem Compra", "Cliente Sem Visita", "Gerencial"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this, (Build.VERSION.SDK_INT >= 28) ? br.net.sav.R.style.DialogStyle : 0)
                .setTitle("Acompanhamento")
                .setItems(itens, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pOpcao) {
                        dialog.dismiss();
                        switch (pOpcao) {
                            case 0: // ANIVERSARIANTES
                                startActivity(new Intent(AniversarianteListaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 1: // PROMO��ES DA SEMANA
                                startActivity(new Intent(PromocaoSemanaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 2: // RELAT�RIOS PRONTOS
                                startActivity(new Intent(RelatoriosProntosListaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 3: // PRODUTO SEM VENDA
                                startActivity(new Intent(ProdutoSemVendaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 4: // CLIENTE SEM COMPRA
                                startActivity(new Intent(ClienteSemCompraActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 5: // CLIENTE SEM VISITA
                                startActivity(new Intent(ClienteSemVisitaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 6: // GERENCIAL
                                showDialogGerencial();
                                break;

                            default:
                                break;
                        }
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_map)
                .setCancelable(true);

        Dialog dialog = builder.create();
        dialog.show();

    }

    private void showDialogGerencial() {
        CharSequence[] itens = new CharSequence[]{"Objetivos e Metas", getString(R.string.venda_por_periodo), getString(R.string.comissao_venda_recebimento)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Acompanhamento -> Gerencial")
                .setItems(itens, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pOpcao) {
                        switch (pOpcao) {
                            case 0: // OBJETIVOS E METAS
                                startActivity(new Intent(ObjetivoMetaListActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 1: // VENDA POR PER�ODO
                                startActivity(new Intent(VendaPorPeriodoListActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            case 2: // COMISS�O VENDA / RECEBIMENTOS
                                startActivity(new Intent(ComissaoVendaActivity.ACTION).addCategory(Principal.CATEGORIA));
                                break;

                            default:
                                break;
                        }

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_dialer)
                .setCancelable(true);

        Dialog dialog = builder.create();
        dialog.show();
    }

}
