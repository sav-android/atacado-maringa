package br.net.sav.atacadomaringa;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.modelo.ObjetivoMeta;

public class ObjetivoMetaAdapter extends BaseAdapter {
	private List<ObjetivoMeta> lista ;
	private Context ctx ;
	
	public ObjetivoMetaAdapter(Context ctx, List<ObjetivoMeta> lista) {
		this.ctx = ctx ;
		this.lista = lista ;
	}
	
	public void changeData(List<ObjetivoMeta> lista) {
		this.lista = lista ;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
			view = layout.inflate(R.layout.listaobjetivometa, null);
		}				
		
		ObjetivoMeta obj = lista.get(position);
		
		TextView txtDescricao = (TextView)view.findViewById(R.idListaObjetivo.txtDescricao);		
		txtDescricao.setText(obj.getDescricao());
		txtDescricao.setTextColor(Color.GREEN);
		
		TextView txtObjetivo = (TextView)view.findViewById(R.idListaObjetivo.txtObjetivo);
		if(String.valueOf(obj.getObjetivo()).toString().trim().length() != 0)
			txtObjetivo.setText(String.format("Objetivo: %.2f",obj.getObjetivo()));
		
		TextView txtRealizado = (TextView)view.findViewById(R.idListaObjetivo.txtRealizado);
		if(String.valueOf(obj.getRealizado()).toString().trim().length() != 0)
			txtRealizado.setText(String.format("Realizado: %.2f",obj.getRealizado()));
		
		TextView txtPercRealizado = (TextView)view.findViewById(R.idListaObjetivo.txtPercRealizado);
		if(String.valueOf(obj.getPercRealizado()).toString().trim().length() != 0)
			txtPercRealizado.setText(String.format("Percentual Realizado: %.2f",obj.getPercRealizado()) + "%");
		
		TextView txtTendencia = (TextView)view.findViewById(R.idListaObjetivo.txtTendencia);
		if(String.valueOf(obj.getTendencia()).toString().length() != 0)
			txtTendencia.setText(String.format(ctx.getString(R.string.tendencia),obj.getTendencia()));
		
		TextView txtPercTendencia = (TextView)view.findViewById(R.idListaObjetivo.txtPercTendencia);
		if(String.valueOf(obj.getPercTendencia()).toString().length() != 0)
			txtPercTendencia.setText(String.format(ctx.getString(R.string.percentual_tendencia),obj.getPercTendencia()) +"%");
		
		TextView txtSugestao = (TextView)view.findViewById(R.idListaObjetivo.txtSugestao);
		if(String.valueOf(obj.getSugestaoDia()).toString().length() !=0)
			txtSugestao.setText(String.format(ctx.getString(R.string.sugestao), obj.getSugestaoDia())) ;

		return view ;
	}	
}
