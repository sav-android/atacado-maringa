package br.net.sav.atacadomaringa;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import br.net.sav.dao.ObjetivoDAO;
import br.net.sav.modelo.ObjetivoMeta;

public class ObjetivoMetaListActivity extends ListActivity {
	public static final String ACTION = "ObjetivoMeta" ;
	private List<ObjetivoMeta> listaObj ;
	private ObjetivoMetaAdapter adapter ;
	private int idSpn = 0 ;
	private CharSequence[] tipo ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);										
		tipo = new CharSequence[] {"Vendas / Quantidade","Vendas / Valor","Vendas / Tonelagem","Visitas"} ;
		showCustomDialog();
	}
	
	@Override
	protected void onResume() {	
		super.onResume();		
		ObjetivoDAO objDAO = new ObjetivoDAO(getBaseContext(), null);
		
		listaObj = objDAO.getObjetivosPorTipo(-1);
		listaObj = objDAO.getObjetivosPorTipo(idSpn + 1) ;		
					
		if(listaObj != null) {
			if(adapter == null) {
				setContentView(R.layout.listaobjetivometa);
				adapter = new ObjetivoMetaAdapter(getBaseContext(), listaObj);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);				
			} else  {
				adapter.changeData(listaObj);
				setListAdapter(adapter);
			}
		}
		
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if(title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, 1);
			title.setTextSize(14);
			setTitle("Total de Objetivos e Metas: " + listaObj.size());
		}
	}
	
	@Override
	protected void onDestroy() {	
		setResult(RESULT_OK);
		super.onDestroy();
	}
	
	public void showCustomDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecione o tipo:");
		builder.setItems(tipo, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	idSpn = item ;
		    	dialog.dismiss();
		    	onResume();
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();
	}				
	
	@Override	
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.objetivometa_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuObjetivoMeta.filtro:			
			showCustomDialog();
			break;						
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}