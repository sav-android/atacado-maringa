package br.net.sav.atacadomaringa;


import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.Pedido;

public class PedidoFechadoListaActivity extends Activity {

    private Button buttonTransmitirPedidosFechados, cancelaTransmitirPedidosFechados;
    private ListaPedidosFechadosAdapter adapter;
    private List<Pedido> listaPedidos;
    private PedidoDAO dbPedido;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_fechado_lista);
        listView = (ListView) findViewById(R.id.listPedidos);
        dbPedido = new PedidoDAO(getBaseContext(), null);
        atualizaAdapter();
        iniciarComponentes();
    }


    public void atualizaAdapter() {
        //   super.onResume();

        listaPedidos = dbPedido.listaPedidos(dbPedido.NAO_ENVIADOS_E_ABERTOS);

        if (listaPedidos != null) {
            if (adapter == null) {
                adapter = new ListaPedidosFechadosAdapter(getBaseContext(), listaPedidos, PedidoFechadoListaActivity.this);
                listView.setAdapter(adapter);
                listView.setFastScrollEnabled(true);
            } else {
                adapter.changeData(listaPedidos);
            }
        }

    }

    private void iniciarComponentes() {
        buttonTransmitirPedidosFechados = (Button) findViewById(R.id.transmitirPedidosFechados);
        cancelaTransmitirPedidosFechados = (Button) findViewById(R.id.cancelarListaPedidos);
        buttonTransmitirPedidosFechados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Pedido pedido: listaPedidos){
                    dbPedido.update(pedido, null);
                }
                setResult(RESULT_OK);
                finish();

            }
        });
        cancelaTransmitirPedidosFechados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Pedido pedido: listaPedidos){
                    if(!pedido.isAberto()) {
                        pedido.setAberto(false);
                        dbPedido.atualizaCampo("aberto", pedido.IDPedido, false);
                    }
                }

                finish();
                startActivity(new Intent(PedidoFechadoListaActivity.this, MenuPrincipal.class));

            }
        });
    }


}
