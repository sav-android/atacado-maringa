package br.net.sav.atacadomaringa;

import android.app.ListActivity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

import br.net.sav.dao.ItemDAO;
import br.net.sav.modelo.Item;

public class PedidoItensListaActivity extends ListActivity {
	public static final String INTENT = "ListaItensPedidos";

	private List<Item> lista;
	private double liquido = 0;
	private double bruto = 0;

	private PedidoVendaProdutoAdapter adapter;

	NumberFormat fm = NumberFormat.getCurrencyInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		lista = new ItemDAO(getBaseContext(), null).listaPedidoVendaProduto();
	}

	@Override
	protected void onResume() {
		super.onResume();

		for (Item i : lista) {
			liquido += i.getTotalLiquido();
			bruto += i.getTotalGeral();
		}

		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if (title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			// setTitle(String.format("Pedidos n�o transmitidos\nQtde.Pedidos: %03d        Qtde.Produtos: %03d\nTotal L�quido: %s    Total Bruto: %s ",new
			// PedidoDAO(getBaseContext(),null).quantidadePedidos(),lista.size(),fm.format(liquido),fm.format(bruto)));
		}

		atualizaAdapter();
	}

	public void atualizaAdapter() {
		if (adapter == null) {
			setContentView(R.layout.listapedidovendaproduto);
			adapter = new PedidoVendaProdutoAdapter();
			setListAdapter(adapter);
			getListView().setFastScrollEnabled(true);
		} else {
			adapter.changeData(lista);
		}
	}

	// -----------------------------ADAPTER------------------------

	public class PedidoVendaProdutoAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void changeData(List<Item> lista) {
			PedidoItensListaActivity.this.lista = lista;
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			if (v == null) {
				LayoutInflater layout = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = layout.inflate(R.layout.listapedidovendaproduto, null);
			}

			Item item = lista.get(position);

			if (position % 2 == 0) {
				v.setBackgroundResource(R.drawable.alterselector1);
			} else {
				v.setBackgroundResource(R.drawable.alterselector2);
			}

			TextView txvProduto = (TextView) v.findViewById(R.idlistapedidovendaproduto.txvProduto);
			txvProduto.setTextSize(17);
			txvProduto.setTextColor(Color.YELLOW);
			txvProduto.setText(String.format("Produto: %s", item.getDescricao()));

			TextView txvQtdeVendida = (TextView) v.findViewById(R.idlistapedidovendaproduto.txvQtdeVendida);
			txvQtdeVendida.setTextSize(13);
			txvQtdeVendida.setText(String.format("Quantidade Vendida: %.2f", item.getQuantidade()));

			TextView txvValorBruto = (TextView) v.findViewById(R.idlistapedidovendaproduto.txvValorBruto);
			txvValorBruto.setTextSize(13);
			txvValorBruto.setText(String.format("Valor Bruto: %.4f", item.getTotalGeral()));

			TextView txvValorLiquido = (TextView) v.findViewById(R.idlistapedidovendaproduto.txvValorLiquido);
			txvValorLiquido.setTextSize(13);
			txvValorLiquido.setText(String.format(getString(R.string.valor_liquido2), item.getTotalLiquido()));

			TextView txvPeso = (TextView) v.findViewById(R.idlistapedidovendaproduto.txvPeso);
			txvPeso.setText(String.format("Peso: %.2f", item.getPeso()));

			return v;
		}
	}
}
