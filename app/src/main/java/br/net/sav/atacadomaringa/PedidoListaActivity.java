package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.net.sav.UIHelper;
import br.net.sav.Utils;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComboDAO;
import br.net.sav.dao.ComboXPedidoDAO;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.PrecoDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.dao.RegraComboDAO;
import br.net.sav.dao.TabelaClienteDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.UltimaDAO;
import br.net.sav.dialog.DialogPickerDataFiltro;
import br.net.sav.dialog.DialogResumoPedido;
import br.net.sav.dialog.GerarBonificacaoDialog;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ComboXPedido;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Preco;
import br.net.sav.modelo.Produto;
import br.net.sav.modelo.RegraCompra;
import br.net.sav.modelo.RegraGanhe;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TabelaCliente;
import br.net.sav.modelo.Ultima;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.service.BonificacaoHelper;
import br.net.sav.utils.ConfigSemafaro;
import br.net.sav.utils.MathUtils;
import br.net.sav.utils.ToastUtils;
import static br.net.sav.atacadomaringa.ClienteListaActivity.isSair;
import static br.net.sav.atacadomaringa.ClienteListaActivity.isSalvo;

import static br.net.sav.atacadomaringa.ComboAdapter.IdComboAtivado;

public class PedidoListaActivity extends ListActivity {
	public static final String INTENT = "ListaPedidos";
	public static final int PARAMETRO_SEQUENCIAL = 1;
	private final CharSequence[] opcoesFiltrar = { "Todos", "Apenas Nao Enviados", "Apenas Enviados" };
	private ArrayList<Pedido> listaPedidos;
	private Parametro parametro;
	private Intent it;
	private int ultimaOpcao;
	private Pedido pedidoAuxiliar;
	private PedidoListaAdapter adapter = null;
	public static final int REQUEST_PEDIDO_ALTERADO = 11;
    private TextView totalPedidos, totalBruto, totalLiquido;
	private EditText mEdtPesquisa, mEdtDataInicio, mEdtDataFim;
	private Calendar mDataInicio, mDataFim;
	private Button btnResumo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.consulta_pedido_principal);
		inicializarDatas();
		atualizaAdapter();
		parametro = new ParametroDAO(getBaseContext(), null).get();
		it = getIntent();
		ultimaOpcao = 0;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (!ControleAcesso.validarConexaoDiaria(PedidoListaActivity.this)) {
			int qtdeDiasPassou = (int) ControleAcesso.quantidadeDiasPassouUltimaTransmissao(PedidoListaActivity.this);
			
			String mensagem = String.format(getString(R.string.faz_dias_que_voce_nao_faz_transmissao_data_do_dispositivos_faca_uma_transmissao_para_utilizar_o_sistema), qtdeDiasPassou, Utils.sdfDataPtBr.format(ControleAcesso.getDataUltimaRecepcao(PedidoListaActivity.this)), Utils.sdfDataHoraPtBr.format(new Date()));

			DialogInterface.OnClickListener listener = new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			};
			
			UIHelper.showDialogConfirmacao(this, listener, mensagem, "SAV - Atacado Maringa", false);
		}
		filtrarPedidos(ultimaOpcao);
		if (isSalvo || isSair) {
			Utils.apagarObjetoSerializado(getBaseContext());
			isSalvo = false;
			isSair = false;
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Pedido pedido = listaPedidos.get(info.position);

		menu.setHeaderTitle(R.string.escolha);
		menu.add(0, 0, 0, "Ver Itens do pedido");
		menu.add(1, 1, 1, "Alterar Pedido");
		menu.add(2, 2, 2, "Excluir Pedido");
		menu.add(3, 3, 3, "Duplicar Pedido");
		menu.add(4, 4, 4, "Enviar resumo pedido");

		menu.setGroupVisible(1, !pedido.isEnviado() && !pedido.isAlterado());
		menu.setGroupVisible(2, !pedido.isEnviado() && !pedido.isAlterado());
		menu.setGroupVisible(3,!pedido.isBonificacao(getBaseContext()));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		registerForContextMenu(l);
		v.showContextMenu();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		final Pedido pedido = listaPedidos.get(info.position);
		Intent it;

		pedidoAuxiliar = pedido;

		switch (item.getItemId()) {
		case 0: // Ver Itens do Pedido
			it = new Intent(ItemListaActivity.INTENT);
			it.addCategory(Principal.CATEGORIA);
			it.putExtra("IdPedido", pedido.getIDPedido());
			startActivity(it);
			break;

		case 1: // Alterar Pedido
			Utils.apagarObjetoSerializado(getBaseContext());
			if(!new BonificacaoHelper(getBaseContext(), pedido).verificarSeExisteBonificacaoVinculado()) {
				Utils.apagarObjetoSerializado(getBaseContext());
				if (!new BonificacaoHelper(getBaseContext(), pedido).verificarSeEhBonificacao(BonificacaoHelper.ALTERAR)) {
					Utils.apagarObjetoSerializado(getBaseContext());
					it = new Intent(PedidoTabHost.INTENT);
					it.putExtra("IdCliente", pedido.getIDCliente());
					it.putExtra("IdPedido", pedido.getIDPedido());
					it.addCategory(Principal.CATEGORIA);
					//startActivity(it);
					startActivityForResult(it, REQUEST_PEDIDO_ALTERADO);
					break;
				}
				break;
			}
			break;

		case 2: // Excluir Pedido
			String msgExcluir ="";
			Utils.apagarObjetoSerializado(getBaseContext());
			if (new BonificacaoHelper(getBaseContext(), pedido).buscarPedidoVinculado().getVinculoPedido() != 0)
				msgExcluir = getString(R.string.deseja_realmente_excluir) ;
			else
				msgExcluir = getString(R.string.deseja_realmente_excluir_o_pedido_nr) + pedido.getIDPedido() + "?";

				DialogInterface.OnClickListener btnSimExcluir = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {

						if (new BonificacaoHelper(getBaseContext(), pedido).buscarPedidoVinculado().getVinculoPedido() != 0) {

							new BonificacaoHelper(getBaseContext(), pedido).excluirPedidoComBonificacao();
							try {
								filtrarPedidos(ultimaOpcao);
							} catch (Exception e) {
								Toast.makeText(getBaseContext(), "Erro:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
							}

						} else if (pedido.isBonificacao(getBaseContext())) {

							new BonificacaoHelper(getBaseContext(), pedido).excluirBonificacao();
							try {
								filtrarPedidos(ultimaOpcao);
							} catch (Exception e) {
								Toast.makeText(getBaseContext(), "Erro:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
							}

						} else if (new PedidoDAO(getBaseContext(), null).excluirPedido(pedido)) {
							Toast.makeText(getBaseContext(), R.string.pedido_excluido_com_sucesso, Toast.LENGTH_SHORT).show();
							boolean deletou = new ComboXPedidoDAO(getBaseContext()).deletePorIds( pedido);

							if (deletou)
								Log.i("Sim", "Deletou: PedidoXCombo");
							else
								Log.i("Nao", "Deletou: PedidoXCombo");

							try {
								filtrarPedidos(ultimaOpcao);
							} catch (Exception e) {
								Toast.makeText(getBaseContext(), "Erro:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
							}
						} else
							Toast.makeText(getBaseContext(), "Erro ao tentar excluir o pedido!", Toast.LENGTH_LONG).show();
					}
				};

				UIHelper.showDialogConfirmacao(this, btnSimExcluir, null, msgExcluir);

			break;
		case 3: // DUPLICAR PEDIDO
			Utils.apagarObjetoSerializado(getBaseContext());
			if (pedido.isBonificacao(getBaseContext())) {
				GerarBonificacaoDialog.mostrarMensagem(getString(R.string.pedido_bonificado_nao_duplicado), getBaseContext());
				break;
			}
			Utils.apagarObjetoSerializado(getBaseContext());
			List<Item> itens = new ItemDAO(getBaseContext(), null).getByPedido(pedidoAuxiliar.getIDPedido());
			List<Combo> combosDoPedido = new ComboDAO(this).buscarCombosPorPedido(pedidoAuxiliar, null);

			if (!combosDoPedido.isEmpty()) {
				Date date = new Date();
				for (Item item2: itens) {
					for (Combo cp : combosDoPedido) {

						if (cp.getPeriodoFinal().before(date)){
							ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.prazo_validade_combo_vencido));
							break;
						}
						RegraCompra compra = RegraCompra.buscaRegraCompra(getBaseContext(), cp.getIdCombo());
						Produto produto = new ProdutoDAO(getBaseContext(), null).get(item2.getIDProduto());
						if (compra != null) {
							if (compra.listIdCampo.contains(item2.getIDProduto())) {
								if (produto == null){
									ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.pedido_nao_duplicado_sem_produto_disponivel));
									break;
								}
							}
						}
					}
				}
			}

			DialogInterface.OnClickListener btnSimDuplicar = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					// Chamar a lista de cliente para selecionar o cliente do novo pedido duplicado.
					startActivityForResult(new Intent(getBaseContext(), ClienteListaActivity.class), 3);
					ToastUtils.mostrarMensagem(getBaseContext(), "Escolha cliente para duplicar pedido");
				}
			};
			UIHelper.showDialogConfirmacao(this, btnSimDuplicar, null, "Deseja duplicar o pedido nº " + pedido.getIDPedido() + "?");
			
			break;
		case 4 : // ENVIAR RESUMO
			try {
				enviarResumo();
			} catch (Exception e) {
				Toast.makeText(getBaseContext(), "Falha ao enviar resumo do pedido: " + e.getMessage(), Toast.LENGTH_LONG).show();
			}
		    break;
		}

		return super.onContextItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.consultapedido_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuConsultaPedido.quais_mostrar:
			showCustomDialogQuaisMostrar();

			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void enviarResumo() throws Exception {
		// Buscar o cliente
		final Cliente cliente = new ClienteDAO(getBaseContext(), null).get(pedidoAuxiliar.getIDCliente());
		// Buscar os itens
		final List<Item> itens = new ItemDAO(getBaseContext(), null).getByPedido(pedidoAuxiliar.getIDPedido());
		
		if (cliente == null)
			throw new Exception(getString(R.string.cliente_nao_encontrado));
		if (itens == null || itens.isEmpty())
			throw new Exception(getString(R.string.itens_do_pedido_nao_encontrados));

		DialogInterface.OnClickListener eventoBtnsSim = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				String nomeArquivo = gerarResumoDoPedido(pedidoAuxiliar, itens);
				abrirActivityEmailEnviarPdf(nomeArquivo, cliente);
			}
		};
		DialogInterface.OnClickListener eventoBtnNeutro = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				String nomeArquivo = gerarResumoDoPedido(pedidoAuxiliar, itens);
				abrirActivityCompartilhar(nomeArquivo, cliente);
			}
		};
		DialogInterface.OnClickListener eventoBtnNao = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				finish();
			}
		};

		showDialogConfirmacaoCompartilhar(this, eventoBtnsSim, eventoBtnNeutro, eventoBtnNao, getString(R.string.pergunta_enviar_resumo_pedido));
	}

	private String gerarResumoDoPedido(Pedido pedido, List<Item> itens) {
		ArrayList<String> linhas = new PedidoDAO(getBaseContext(), null).getLinhasPdfPedido(pedido, itens);
		String nomeArquivo = new PedidoDAO(getBaseContext(), null).gerarPdf(linhas, pedido.getIDPedido());
		return nomeArquivo;
	}

	private static Dialog showDialogConfirmacaoCompartilhar(Activity activity, DialogInterface.OnClickListener eventoBtnSim, DialogInterface.OnClickListener eventoBtnNeutro, DialogInterface.OnClickListener eventoBtnNao, String mensagem) {
		return Utils.showDialogConfirmacoes(activity, eventoBtnSim, eventoBtnNeutro, eventoBtnNao, mensagem, "E-mail","+ Compartilhar",  activity.getString(br.net.sav.R.string.nao), activity.getString(br.net.sav.R.string.a_t_e_n_c_a_o));
	}
	private void abrirActivityEmailEnviarPdf(String nomeArquivo , Cliente cliente) {
		Utils.openGmail(this, new String[]{cliente.getEmail()}, getString(R.string.assunto_email_pedido_pdf), nomeArquivo);
	}
	private void abrirActivityCompartilhar(String nomeArquivo, Cliente cliente) {
		Utils.abrirActivityEmail(this, new String[]{cliente.getEmail()}, nomeArquivo);
	}

	private void showCustomDialogQuaisMostrar() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.escolha_uma_opcao);
		builder.setItems(opcoesFiltrar, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				filtrarPedidos(item);
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void filtrarPedidos(int opcao) {
		PedidoDAO pedidoDAO = new PedidoDAO(getBaseContext(), null);
		ultimaOpcao = opcao;
		long idCliente = it.getLongExtra("IdCliente", 0);
		if(parametro != null)
		pedidoDAO.verificaDataAlterada();

		try {
			switch (opcao) {
			case 0: // TODOS
				listaPedidos = pedidoDAO.listaPedidosPorData(pedidoDAO.TODOS, idCliente, mDataInicio.getTime(), mDataFim.getTime());
				break;
			case 1: // APENAS N�O ENVIADOS
				listaPedidos = pedidoDAO.listaPedidosPorData(pedidoDAO.NAO_ENVIADOS, idCliente, mDataInicio.getTime(), mDataFim.getTime());
				break;
			case 2: // APENAS ENVIADOS
				listaPedidos = pedidoDAO.listaPedidosPorData(pedidoDAO.ENVIADOS, idCliente, mDataInicio.getTime(), mDataFim.getTime());
				break;
			}

			setarTitulo();
		} catch (Exception e) {
			if (parametro == null)
				Toast.makeText(getBaseContext(), R.string.erro_importar_arquivo_de_parametro, Toast.LENGTH_LONG).show();
			else
				Toast.makeText(getBaseContext(), "Erro ao filtrar pedidos:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	private void setarTitulo() {
		atualizaAdapter();
		// TOTAL GERAL
		double dblTotalBruto = 0.0, dblTotalLiquido = 0.0;
		for (Pedido p : listaPedidos) {
			dblTotalBruto += p.getTotalGeral();
			dblTotalLiquido += p.getTotalLiquido();
		}

		// SALDO FLEX
		String saldoMostrar = "";

		saldoMostrar = NumberFormat.getCurrencyInstance().format(parametro.getSaldoVerba());

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		//setListAdapter(new PedidoListaAdapter());
		//TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		//title.setSingleLine(false);
		//title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		//title.setTextSize(16);
		String formatBruto = nf.format(dblTotalBruto);
		String formatQuantidade = String.valueOf(listaPedidos.size());
		String formaLiquido= nf.format(dblTotalLiquido);
		totalPedidos.setText(formatQuantidade);
		totalBruto.setText(formatBruto);
		totalLiquido.setText(formaLiquido);

		//setTitle(String.format(getString(R.string.nr_pedidos_total_bruto_total_liquido_saldo_verba), listaPedidos.size(), nf.format(dblTotalBruto), nf.format(dblTotalLiquido), saldoMostrar));
	}

	public void atualizaAdapter() {

		if (listaPedidos != null) {
			if (adapter == null) {
				setContentView(R.layout.consulta_pedido_principal);
				adapter = new PedidoListaAdapter(listaPedidos);
				inicializarComponentes();
				onClickFiltrodata();

				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(listaPedidos);
				setListAdapter(adapter);
			}
		}

	}

	protected void inicializarDatas() {
		mDataFim = Calendar.getInstance();
		mDataInicio = Calendar.getInstance();
		mDataInicio.add(Calendar.DAY_OF_MONTH, - mDataFim.get(Calendar.DAY_OF_MONTH));
		mDataInicio.add(Calendar.DAY_OF_MONTH, 1);
	}

	private void gerarBonificacao(Intent it) {
		Pedido pedido = getPedido(it);
		final Pedido finalPedido = pedido;

		if(pedido != null)
		if(!pedido.isBonificacao(getBaseContext()) && pedido.getVinculoBonificacao()==0) {
			GerarBonificacaoDialog.gerarDialog(parametro, pedido, this, new GerarBonificacaoDialog.OnClickListener() {
				@Override
				public void onClick() {
					iniciaPedidoDeBonificacao(finalPedido);
				}
			});
		}

	}
	private void inicializarComponentes(){
		totalPedidos = (TextView) findViewById(R.id.txvValorNumeroPedidos);
		totalBruto   = (TextView) findViewById(R.id.txvValorTotalBruto);
		totalLiquido = (TextView) findViewById(R.id.txvValorTotalLiquido);
		mEdtDataInicio = (EditText) findViewById(R.id.ConsultaPedido_edtDataInicio);
		mEdtDataFim = (EditText) findViewById(R.id.ConsultaPedido_edtDataFim);
		btnResumo = (Button) findViewById(R.id.btnResumoPedido);
		btnResumo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogResumoPedido resumo = new DialogResumoPedido(PedidoListaActivity.this, listaPedidos);
				resumo.requestWindowFeature(Window.FEATURE_NO_TITLE);
				resumo.show();

			}
		});
	}

	private void onClickFiltrodata(){
		mEdtDataInicio.setFocusable(false);
		mEdtDataInicio.setText(Utils.sdfDataPtBr.format(mDataInicio.getTime()));
		mEdtDataInicio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new DialogPickerDataFiltro(PedidoListaActivity.this,inicia,mDataInicio, mDataFim,mEdtDataInicio,mEdtDataFim ).showDatePickerDataInicio();
			}
		});

		mEdtDataFim.setFocusable(false);
		mEdtDataFim.setText(Utils.sdfDataPtBr.format(mDataFim.getTime()));
		mEdtDataFim.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				new DialogPickerDataFiltro(PedidoListaActivity.this,inicia,mDataInicio, mDataFim,mEdtDataInicio,mEdtDataFim ).showDatePickerDataFim();
			}
		});
	}

	DialogPickerDataFiltro.IniciarPesquisaData inicia = new DialogPickerDataFiltro.IniciarPesquisaData() {
		@Override
		public void execute() {
			filtrarPedidos(ultimaOpcao);
			atualizaAdapter();
		}
	};



	private void iniciaPedidoDeBonificacao(Pedido pedido) {
		Bundle extras = new Bundle();
		extras.putLong(PedidoTabActivity.PEDIDO_ID_BONIFICACAO, pedido.getIDPedido());
		extras.putLong(Cliente.EXTRA_IDCLIENTE, pedido.getIDCliente());

		Intent intent = new Intent(PedidoTabHost.INTENT).addCategory(Principal.CATEGORIA);
		intent.putExtras(extras);
		startActivity(intent);
	}

	private Pedido getPedido(Intent it) {
		final long idPedido = it.getLongExtra(Pedido.EXTRA_ID, -1);
		Pedido pedido = new Pedido();
		pedido.setIDPedido(idPedido);
		pedido = new PedidoDAO(this,null).get(pedido.getIDPedido(), null);
		return pedido;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case 11:
			Utils.apagarObjetoSerializado(getBaseContext());
			gerarBonificacao(data);
			break;

		case 3:
			if (resultCode == ListActivity.RESULT_OK) {
				long idCliente = 0;
                Tabela tabela = null;
				TabelaCliente tabelaCliente = null;
                List<Item> itensSemPreco = new ArrayList<>();

				idCliente = data.getLongExtra("IdCliente", 0);
				Utils.apagarObjetoSerializado(getBaseContext());

				List<Item> itens = new ItemDAO(getBaseContext(), null).getByPedido(pedidoAuxiliar.getIDPedido());
				List<Combo> combosDoPedido = new ComboDAO(this).buscarCombosPorPedido(pedidoAuxiliar, null);

				List<Item> itensAux = new ArrayList<>();
				pedidoAuxiliar.setIDPedido(new PedidoDAO(getBaseContext(), null).gerarId());
				pedidoAuxiliar.setIDCliente(idCliente);
				pedidoAuxiliar.setDataPedido(new Date());
				pedidoAuxiliar.setDataEnvio(new Date());
				pedidoAuxiliar.setDataFaturamento(new Date());
				pedidoAuxiliar.setEnviado(false);
				pedidoAuxiliar.setAberto(false);
				pedidoAuxiliar.setStatus(false);
				pedidoAuxiliar.setStatusPedidoGWeb(0);
				pedidoAuxiliar.setHistoricoPedidoCM(false);
				pedidoAuxiliar.setRetornoNumeroNotaFiscal("0");
				pedidoAuxiliar.setRetornoStatusPedido("aberto");
				pedidoAuxiliar.setPercRentabilidadeLiberado(0);
				pedidoAuxiliar.setCombos(combosDoPedido);

				List<TabelaCliente> tabelaClientes = new TabelaClienteDAO(getBaseContext(), null).getAll(idCliente);

                if ( !tabelaClientes.isEmpty() && tabelaClientes.size() > 0) {
                	for (TabelaCliente tabelaCliente1 : tabelaClientes ) {
						tabela = new TabelaDAO(getBaseContext(), null).get(tabelaCliente1.getIdTabela());
						Tabela tabelaAux = new TabelaDAO(getBaseContext(), null).get(pedidoAuxiliar.getIDTabela());

						if(tabelaCliente1.getIdTabela() == pedidoAuxiliar.getIDTabela()) {
							if (tabela.isUltilizaRentabilidade() && !tabelaAux.isUltilizaRentabilidade() ||
									!tabela.isUltilizaRentabilidade() && tabelaAux.isUltilizaRentabilidade()) {
								ToastUtils.mostrarMensagem(getBaseContext(), getString(R.string.pedido_nao_duplicado_por_tabela_preco_sem_rentabilidade));
								return;
							}else {
								tabelaCliente = tabelaCliente1;
								break;
							}
						}
					}
				}


                List<ItemDigitacao> listItens = new ArrayList<>();
                Pedido pedido = new Pedido();
                pedido.setNrItens(pedidoAuxiliar.getNrItens());
				Preco preco = new Preco();
				for (Item i : itens) {
					ItemDigitacao itemDigitacao = new ItemDigitacao();

                    preco = getPrecoAtualParaDuplicarPedido(tabelaCliente, i);
					Produto produto = new ProdutoDAO(getBaseContext(), null).get(i.getIDProduto());
					if (preco.getPrecoNormal() == 0) {
						pedido.setNrItens((short) (pedido.getNrItens() - 1));
                    	itensSemPreco.add(i);
						continue;
					}


					double precoCustoProduto  = produto.getPrecoCustoProduto() * produto.getQuantidadeEmbalagem();
					i.setIDPedido(pedidoAuxiliar.getIDPedido());
                    itemDigitacao.setValorUnitOriginal(Utils.arredondar(preco.getPrecoNormal(), 2));
                    itemDigitacao.setPrecoCustoProduto(precoCustoProduto > 0? Utils.arredondar(precoCustoProduto,2) : 0);
					itemDigitacao.setDesconto(i.getDesconto());
					itemDigitacao.setQuantidade(i.getQuantidade());
					itemDigitacao.setItemComboAtivado(i.getItemComboAtivado());
					itemDigitacao.setPercentualDescontoCombo(i.getPercentualDescontoCombo());
					itemDigitacao.setQuantidadeItemLimiteCombo(i.getQuantidadeItemLimiteCombo());
					if (tabelaCliente != null)
						if (pedidoAuxiliar.getIDTabela() != preco.getIdTabela() && !tabela.isPermiteDescontoPorTabela()
								&& itemDigitacao.getDesconto() > 0){
							itemDigitacao.setDesconto(0);
							itemDigitacao.setRentabilidade(0);
						}

					if (itemDigitacao.getDesconto() != 0) {
						itemDigitacao.setValorDesconto(MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() * (itemDigitacao.getDesconto() / 100), 4));
						itemDigitacao.setValorUnit(MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() - itemDigitacao.getValorDesconto(), 2));
						if (itemDigitacao.getPrecoCustoProduto() > 0) {
							itemDigitacao.setRentabilidade(MathUtils.arredondar((itemDigitacao.getValorUnit() - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
						}
					}else {
						itemDigitacao.setValorUnit(itemDigitacao.getValorUnitOriginal());
						itemDigitacao.setValorDesconto(0);
						if (itemDigitacao.getPrecoCustoProduto() > 0) {
							itemDigitacao.setRentabilidade(MathUtils.arredondar((itemDigitacao.getValorUnit() - itemDigitacao.getPrecoCustoProduto()) / itemDigitacao.getPrecoCustoProduto() * 100, 2));
						}
					}

					itemDigitacao.setValorLiquido(MathUtils.arredondar(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade() , 4));
					itemDigitacao.setValorBruto(MathUtils.arredondar(itemDigitacao.getValorUnitOriginal() * itemDigitacao.getQuantidade(), 4));
					i.setValorUnitPraticado(itemDigitacao.getValorUnit());
					i.setValorUnitOriginal(itemDigitacao.getValorUnitOriginal());
					i.setTotalGeral(itemDigitacao.getValorBruto());
					i.setTotalLiquido(itemDigitacao.getValorLiquido());
					i.setValorDesconto(itemDigitacao.getValorDesconto());
					i.setDesconto(itemDigitacao.getDesconto());
					i.setQuantidade(itemDigitacao.getQuantidade());
					i.setRentabilidade(itemDigitacao.getRentabilidade());
					i.setDataPedido(pedidoAuxiliar.getDataPedido());

					pedido.setTotalGeral(pedido.getTotalGeral() + itemDigitacao.getValorBruto());
					pedido.setTotalLiquido(pedido.getTotalLiquido() + itemDigitacao.getValorLiquido());

					itensAux.add(i);
					listItens.add( itemDigitacao);

				}
				itens = itensAux;
 				pedidoAuxiliar.setNrItens(pedido.getNrItens());
				pedidoAuxiliar.setTotalGeral(pedido.getTotalGeral());
				pedidoAuxiliar.setTotalLiquido(pedido.getTotalLiquido());
				if (tabelaCliente!= null && tabelaCliente.getIdTabela() > 0)
				pedidoAuxiliar.setIDTabela(tabela.getIdTabela());
				pedidoAuxiliar.setVinculoBonificacao(0);


				if (!itens.isEmpty()) {
					pedidoAuxiliar.apurarRentabilidadePedido(listItens);
					if (new PedidoDAO(getBaseContext(), null).salvarPedido(pedidoAuxiliar, itens, true)) {
						Toast.makeText(getBaseContext(), "Pedido duplicado com sucesso!", Toast.LENGTH_SHORT).show();


						ComboXPedido.gravarCombos(getBaseContext(), pedidoAuxiliar);
						try {
							filtrarPedidos(ultimaOpcao);
						} catch (Exception e) {
							Toast.makeText(getBaseContext(), "Erro:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
						}
					} else
					Toast.makeText(getBaseContext(), "Erro ao tentar duplicar o pedido!", Toast.LENGTH_LONG).show();
				}
                if (itensSemPreco.size() > 0)
                    msgItensSemPreco(itensSemPreco, itens);
			}
		}
	}

	private void msgItensSemPreco(List<Item> itensSemPreco, List<Item> itens) {
		AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
		String titulo = getString(R.string.itens_sem_preco);
		builderSingle.setIcon(android.R.drawable.ic_dialog_alert);
		CharSequence[] itemCodigo = new CharSequence[itensSemPreco.size()];
		int cont = 0;

		for (Item item : itensSemPreco){
			itemCodigo[cont] = "Item codigo: " + item.getIDProduto();
			cont++;
		}

		if (itens.isEmpty()){
			 titulo = getString(R.string.pedido_nao_duplicado);
		}

		builderSingle.setTitle(titulo);
		builderSingle.setItems(itemCodigo, null);
		builderSingle.setNegativeButton("Ok", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builderSingle.show();
	}

	private Preco getPrecoAtualParaDuplicarPedido(TabelaCliente tabelaCliente, Item i) {
        Preco preco;
        if (tabelaCliente != null && tabelaCliente.getIdTabela() > 0)
            preco = new PrecoDAO(getBaseContext(), null).getPreco(null, i.getIDProduto(), tabelaCliente.getIdTabela());
        else
            preco = new PrecoDAO(getBaseContext(), null).getPreco(null, i.getIDProduto(), pedidoAuxiliar.getIDTabela());

		Produto produto = new ProdutoDAO(getBaseContext(), null).get(i.getIDProduto());
		if (produto == null || produto.getIdProduto() == 0){
			preco.setPrecoNormal(0);
		}else {
			preco.setPrecoNormal(preco.getPrecoNormal() / produto.getDivisaoPreco());
		}

		return preco;
    }

    // -----------------------ADAPTER---------------------

	@SuppressLint({ "SimpleDateFormat", "InflateParams" })
	public class PedidoListaAdapter extends BaseAdapter {
		private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		private SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		private ImageView imgRentabilidade;
		private TextView txvRentabilidadePedido, txvRentabilidadeLiberada, txvVinculoPedido, txvStatusPedido, txvNumeroNF;
		private List<Pedido> listaPedidos;

		public PedidoListaAdapter(List<Pedido> listaPedidos){
			this.listaPedidos = listaPedidos;
		}


		@Override
		public int getCount() {
			return listaPedidos.size();
		}

		@Override
		public Object getItem(int position) {
			return listaPedidos.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup vewG) {
			if (view == null) {
				LayoutInflater layout = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = layout.inflate(R.layout.consultapedido, null);
			}
			Parametro parametro = new ParametroDAO(getBaseContext(), null).get();

			if (position % 2 == 0) {
				view.setBackgroundResource(R.drawable.alterselector1);
			} else {
				view.setBackgroundResource(R.drawable.alterselector2);
			}

			Pedido pedido = listaPedidos.get(position);

			TextView txvPedido = (TextView) view.findViewById(R.idConsultaPedido.txvPedido);
			txvPedido.setText(getString(R.string.pedido_nr) + String.valueOf(pedido.getIDPedido()));
			txvPedido.setTextColor(Color.GREEN);
			txvPedido.setTypeface(Typeface.DEFAULT_BOLD);

			TextView txvConfirmado = (TextView) view.findViewById(R.idConsultaPedido.txvConfirmado);
			txvConfirmado.setText("Status CM: "+ (pedido.isStatus()? StatusCentralManager.ENVIADO.getStatus() : StatusCentralManager.PENDENTE.getStatus()));
			txvConfirmado.setTextColor(pedido.isStatus()? Color.GREEN: Color.RED);
			txvConfirmado.setTypeface(Typeface.DEFAULT_BOLD);

			TextView txvCliente = (TextView) view.findViewById(R.idConsultaPedido.txvCliente);
			if (pedido.getNomeCliente() == null)
				txvCliente.setText(R.string.cliente_nao_encontrado);
			else
				txvCliente.setText(String.format("%d - %s", pedido.getIDCliente(), pedido.getNomeCliente()));



			TextView txvDataPedido = (TextView) view.findViewById(R.idConsultaPedido.txvData);
			txvDataPedido.setText("Data Compra: " + sdf.format(pedido.getDataPedido()));

			TextView txvDataFaturamento = (TextView) view.findViewById(R.id.txvDataFaturamento);
			txvDataFaturamento.setText("Data Faturamento: " + sdf1.format(pedido.getDataFaturamento()));

			TextView txvValorFaturado = (TextView) view.findViewById(R.id.txvValorFaturado);
			txvValorFaturado.setText(String.format("Valor Faturado: %.4f",pedido.getTotalFaturado()));


			TextView txvTipoPedido = (TextView) view.findViewById(R.idConsultaPedido.txvTipoPedido);
			txvTipoPedido.setText(String.format("Tipo Pedido: %s", pedido.getTipoPedido()));


			TextView txvValorBruto = (TextView) view.findViewById(R.idConsultaPedido.txvValorBruto);
			txvValorBruto.setText(String.format("Valor Bruto: %.4f",pedido.getTotalGeral()));


			TextView txvValorLiquido = (TextView) view.findViewById(R.idConsultaPedido.txvValorLiquido);
			txvValorLiquido.setText(String.format(getString(R.string.valor_liquido2), pedido.getTotalLiquido()));


			TextView txvValorGeraraFlex = (TextView) view.findViewById(R.idConsultaPedido.txvValorGeraraFlex);
			txvValorGeraraFlex.setText(String.format(getString(R.string.gerara_flex), pedido.getValorGeraFlex()));


			txvVinculoPedido = (TextView) view.findViewById(R.id.txvVinculoPedido);
			txvVinculoPedido.setText(String.format( getString(R.string.bonificacao_vinculada)+" "+ getString(R.string.pedido_nr) + pedido.getVinculoPedido()));


            txvRentabilidadePedido = (TextView) view.findViewById(R.id.txvRentabilidadePedido);
            txvRentabilidadePedido.setText(String.format( "Perc. Rentabilidade Pedido: %.2f%%" , pedido.getRentabilidade()));


			txvRentabilidadeLiberada = (TextView) view.findViewById(R.id.txvPercRetabilidadeLiberado);
			txvRentabilidadeLiberada.setText(String.format( "Perc. Rentabilidade Pedido (LIBERADO POR SENHA): %.2f%%" , pedido.getPercRentabilidadeLiberado()));

			txvStatusPedido = (TextView) view.findViewById(R.id.txvStatusPedido);
			txvNumeroNF = (TextView) view.findViewById(R.id.txvNumeroNF);
			txvStatusPedido.setText(String.format("Status ERP: %s", pedido.getRetornoStatusPedido() == null? "ABERTO": pedido.getRetornoStatusPedido()));
			txvNumeroNF.setText(String.format( getString(R.string.numero_nota), pedido.getRetornoNumeroNotaFiscal() == null? "0": pedido.getRetornoNumeroNotaFiscal()));
			txvStatusPedido.setTextColor(txvStatusPedido.getText().toString().toUpperCase().contains("ABERTO")
					|| txvStatusPedido.getText().toString().toUpperCase().contains("CANCELADO")? Color.RED :
                    txvStatusPedido.getText().toString().toUpperCase().contains("CONFIRMADO")? Color.YELLOW: Color.GREEN);
			txvStatusPedido.setTypeface(Typeface.DEFAULT_BOLD);

            if (txvStatusPedido.getText().toString().toUpperCase().contains("FATURADO")) {
                txvValorFaturado.setVisibility(View.VISIBLE);
                txvDataFaturamento.setVisibility(View.VISIBLE);
            }else {
                txvValorFaturado.setVisibility(View.GONE);
                txvDataFaturamento.setVisibility(View.GONE);
            }


			if (pedido.getPercRentabilidadeLiberado() > 0)
				txvRentabilidadeLiberada.setVisibility(View.VISIBLE);
			else
				txvRentabilidadeLiberada.setVisibility(View.GONE);

			if (pedido.isBonificacao(getBaseContext())){
				txvVinculoPedido.setVisibility(View.VISIBLE);
			}else
				txvVinculoPedido.setVisibility(View.GONE);

            imgRentabilidade = (ImageView) view.findViewById(R.id.consultaPedido_imgRentabilidade);

			TextView txvDataEnvio = (TextView) view.findViewById(R.idConsultaPedido.txvDataEnvio);
			if (pedido.isEnviado())
				txvDataEnvio.setText("Data Compra Envio: " + sdf.format(pedido.getDataEnvio()));
			else
				txvDataEnvio.setText(R.string.data_envio_nao_enviado);


			Tabela tabela = new TabelaDAO(getBaseContext(), null).get(pedido.getIDTabela());

			if (pedido.getIDTabela() == tabela.getIdTabela()) {
				if (tabela.isUltilizaRentabilidade()&& !pedido.isHistoricoPedidoCM()) {
					visualizarRentabilidadePedido(pedido);
				}
				else {
					txvRentabilidadePedido.setVisibility(View.GONE);
					imgRentabilidade.setVisibility(View.GONE);
				}
			}

			return view;
		}

		private void visualizarRentabilidadePedido(Pedido pedido) {
			if (parametro.getVisualizarRentabilidade()) {
				txvRentabilidadePedido.setVisibility(View.GONE);
				imgRentabilidade.setVisibility(View.VISIBLE);
				imgRentabilidade.setImageResource(ConfigSemafaro.getImagemSemaforoPedido(parametro, pedido));

			}else{
				imgRentabilidade.setVisibility(View.GONE);
				txvRentabilidadePedido.setVisibility(View.VISIBLE);
				txvRentabilidadePedido.setTextColor(ConfigSemafaro.getTextColorSemafaroPedido(parametro,pedido));
			}
		}

		public void changeData(ArrayList<Pedido> listaPedidos) {
			this.listaPedidos = listaPedidos;
			notifyDataSetChanged();
		}
	}
}