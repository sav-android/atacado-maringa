package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.ComboController.comboUtils.DialogAlertComboUltils;
import br.net.sav.LocationService;
import br.net.sav.Utils;
import br.net.sav.adapter.DialogComboAdapter;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComboDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.CondicaoPagamentoClienteDAO;
import br.net.sav.dao.EmpresaFilialDAO;
import br.net.sav.dao.FormaPagamentoClienteDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.TabelaClienteDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TabelaPrecoOperacaoTrocaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.enumerador.TipoOperacao;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.EmpresaFilial;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TabelaCliente;
import br.net.sav.modelo.TabelaPrecoOperacaoTroca;
import br.net.sav.modelo.TipoPedido;
import br.net.sav.utils.ConfigSemafaro;
import br.net.sav.utils.ToastUtils;

import static br.net.sav.ComboController.comboUtils.DescontoComboUtils.setComboMsg;
import static br.net.sav.atacadomaringa.ClienteListaActivity.isSair;
import static br.net.sav.atacadomaringa.ItemTabActivity.isZerarDesconto;
import static br.net.sav.atacadomaringa.PedidoListaActivity.PARAMETRO_SEQUENCIAL;

public class PedidoTabActivity extends Activity {
    public static final String INTENT = "PedidoTabActivity";
    public static final String PEDIDO_SERIALIZAR = "pedido.srl";
    public static final String PEDIDO_SERIALIZAR_OP = "pedidop.srl";
    public static final String PEDIDO_SERIALIZAR_ALTERAR = "pedidoalterar.srl";

    private EditText edtObservacao, edtNrPedido, edtDataPedido, edtTotalBruto, edtTotalLiquido, edtParcelas, edtCliente, edtUltimaCondicaoCliente, txtRetabilidade;
    private Spinner spnEmpresaFilial, spnTipoPedido, spnTabelaPreco, spnFormaCobranca, spnCondicaoPgto;
    private LinearLayout lnlRetabilidadePerc, lnRetabilidadeImg;
    private ImageView pedido_imgRentabilidade;

    public static final String PEDIDO_ID_BONIFICACAO = "PEDIDO_ID_BONIFICACAO";

    private Parametro parametro;
    private Cliente cliente;
    private Pedido pedido;
    private Condicao ultimaCondicaoCliente;
    private Pedido pedidoVinculado;

    /* AUX DOS SPINNERS */
    private Long[] idFormaPagamento;
    private Short[] idCondicao, idTabela, idTipoPedido, idEmpresa, idFilial, idTabela2;
    private List<EmpresaFilial> listaEmpresaFilial;
    private List<TipoPedido> listaTipoPedido;
    private List<Tabela> listaTabela;
    private List<Condicao> listaCondicaoPagamento;
    private List<FormaPagamento> listaFormaPagamento;
    private List<TabelaPrecoOperacaoTroca> listaTabelaPrecoOperacaoTroca;

    private long idCliente, idPedido;
    private boolean aPedido, rPedido, isRotated;
    private boolean clicouTipoPedido, clicouCondicaoPgto, clicouFormaPagto, clicouCondicaoPgtoPelaSegundaVez;
    private List<String> lstTipoPedidoApurados = new ArrayList<String>();

    private int orientacao;
    private boolean pedidoAlterado = false;
    private boolean formaPagamentoDoCliente = false;
    private boolean condicaoPagamentoCliente = false;
    private static int indexTabelaPreco;
    public static boolean itemPedidoRecuperado;

    private boolean mIsDialogCancelou = true;
    private int posicaoAnterior;
    private int posicaoNova;
    private int posicao;
    private Pedido pedidoRotacionado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capa_pedido);

        getRecuperaPedidoAoRotacionartela();

        itemPedidoRecuperado = rPedido = getIntent().getBooleanExtra("RecuperacaoPedido", false);
        idCliente = getIntent().getLongExtra(Cliente.EXTRA_IDCLIENTE, 0);
        indexTabelaPreco = 0;

        try {
            SharedPreferences prefs = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE);
            clicouTipoPedido = prefs.getBoolean("clicouTipoPedido", false);
            clicouCondicaoPgto = prefs.getBoolean("clicouCondPagamento", false);
            clicouFormaPagto = prefs.getBoolean("clicouFormaPagamento", false);
        } catch (Exception e) {
            e.printStackTrace();
        }


        carregarListas();
        recuperarLayout();
        carregarDadosPedido();

        if (pedido.isAlterarPedido()) {
            if (pedido != null) {
                pedido.executarOperacoesParaDigitacao(this, pedido);
            }
            pedidoAlterado = true;
            // itemPedido = (ArrayList<ItemDigitacao>) Utils.recuperarObjetoSerializado(getBaseContext(), ITENSPEDIDO_SERIALIZAR_ALTERAR);
        }
        carregarSpnEmpresaFilial();
        dialogAvisoCombos();

        isRotated = (orientacao != getResources().getConfiguration().orientation);
        orientacao = getResources().getConfiguration().orientation;

        getVerificaSeTelaFoiRotacionadaParaRecuperarPedido();

    }

    private void getVerificaSeTelaFoiRotacionadaParaRecuperarPedido() {
        if (isRotated && pedidoRotacionado != null){
            pedido = pedidoRotacionado;
        }
    }

    private void getRecuperaPedidoAoRotacionartela() {
        pedidoRotacionado = (Pedido) Utils.recuperarObjetoSerializado(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR);
    }

    private void dialogAvisoCombos() {
        List<RegraAplicavel> combosValidos = Combo.buscarCombos(this, pedido);
        if (!combosValidos.isEmpty()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View content = getLayoutInflater().inflate(R.layout.combo_listview_dialog, null);
            builder.setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.atencao)
                    .setView(content);

            TextView textView = (TextView) content.findViewById(R.id.txv_combo1);
            ListView listView = (ListView) content.findViewById(R.id.list_dialog_combo);
            Button btnAtivar = (Button) content.findViewById(R.id.buttonCombo);
            Button btnCancelar = (Button) content.findViewById(R.id.buttonCombo1);
            btnCancelar.setVisibility(View.GONE);
            btnAtivar.setGravity(Gravity.CENTER_HORIZONTAL);
            btnAtivar.setText("Ok");
            textView.setText(R.string.itens_combo_desconto);

            List<Combo> combos = new ArrayList<>();
            for (RegraAplicavel combo : combosValidos) {
                setComboMsg(combos, combo, false);
            }
            DialogComboAdapter dialogComboAdapter = new DialogComboAdapter(getBaseContext(), combos);
            listView.setAdapter(dialogComboAdapter);

            if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
                ToastUtils.tamanhoListViewDialogUtils(listView, 125);
            } else {
                ToastUtils.tamanhoListViewDialogUtils(listView, 85);
            }

            final AlertDialog alert = builder.create();
            btnAtivar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alert.dismiss();

                }
            });
            alert.show();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        try {
            iniciarServicoLocalizacao();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Object obj = Utils.recuperarObjetoSerializado(getBaseContext(), PEDIDO_SERIALIZAR);

        if (obj != null) {
            pedido = (Pedido) obj;
            habilitarCampos(pedido.getNrItens() <= 0);
            edtObservacao.setFocusable(false);
        } else if (isAlteracao()) {
            habilitarCampos(false);
        }
        preencherDadosCampos();
    }

    private boolean isAlteracao() {
        if (aPedido) {
            clicouCondicaoPgto = true;
            sharedPreference("clicouCondPagamento", true);
            clicouFormaPagto = true;
            sharedPreference("clicouFormaPagamento", true);
            clicouTipoPedido = true;
            sharedPreference("clicouTipoPedido", true);
        }
        return aPedido;
    }

    private boolean isRecuperacaoPedido() {
        return rPedido;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (pedido != null) {
                pedido.setObservacao(edtObservacao.getText().toString());
                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
                edtObservacao.setFocusable(false);
            }
        } catch (Exception e) {
            Log.e("PedidoTabActivity", e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        confirmarSaida();
    }

    public void btnTitulos_click(View view) {
        Intent it = new Intent(PendenciasListaActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, idCliente).addCategory(Principal.CATEGORIA);
        startActivity(it);
    }

    public void btnUltimasCompras_click(View view) {
        Intent it = new Intent(UltimasComprasListaActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, idCliente).addCategory(Principal.CATEGORIA);
        startActivity(it);
    }

    public void btnDetalhes_click(View view) {
        Intent it = new Intent(ClienteTabHostActivity.INTENT).putExtra(Cliente.EXTRA_IDCLIENTE, idCliente).addCategory(Principal.CATEGORIA);
        startActivity(it);
    }

    public void btnCancelar_click(View view) {
        confirmarSaida();
    }

    public void confirmarSaida() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle(R.string.atencao).setMessage(R.string.todos_os_dados_serao_perdidos_deseja_realmente_cancelar_o_pedido).setPositiveButton("SIM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.apagarObjetoSerializado(getBaseContext());
                isSair = true;
                sharedPreference("clicouTipoPedido", false);
                sharedPreference("clicouCondPagamento", false);
                sharedPreference("clicouFormaPagamento", false);
                finish();
            }
        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void carregarDadosPedido() {
        Intent it = getIntent();

        try {
            cliente = new ClienteDAO(getBaseContext(), null).getIdRazao(idCliente);
            parametro = new ParametroDAO(getBaseContext(), null).get();
            ultimaCondicaoCliente = new CondicaoDAO(getBaseContext(), null).getUltimaCondicaoCliente(idCliente);
            idPedido = it.getLongExtra("IdPedido", 0);

            if (idPedido == 0) { // PEDIDO NOVO
                pedido = new Pedido();
                pedido.setIDPedido(new PedidoDAO(getBaseContext(), null).gerarId());
                pedido.setIDCliente(idCliente);
                pedido.setDataPedido(new Date());
                pedido.setEnviado(false);
                pedido.setDataEnvio(pedido.getDataPedido());
                pedido.setDataFaturamento(pedido.getDataEnvio());
                pedido.setObservacao("");
                pedido.setPedidoCliente("");
            } else {
                Object obj = null;
                if (isRecuperacaoPedido())
                    obj = Utils.recuperarObjetoSerializado(getBaseContext(), PEDIDO_SERIALIZAR);

                if (obj != null) { // SE EXISTIR PEDIDO SERIALIZADO P/ RECUPERAR
                    pedido = (Pedido) obj;
                    rPedido = true;
                } else { // ALTERAR PEDIDO
                    aPedido = true;
                    pedido = new PedidoDAO(getBaseContext(), null).get(idPedido, null);
                    pedido.setAlterarPedido(aPedido);
                    if (pedido == null) {
                        Toast.makeText(getBaseContext(), String.format(getString(R.string.o_pedido_nao_foi_encontrado_no_banco_de_dados_para_permitir_altera_lo), idPedido), Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        List<Combo> combos = new ComboDAO(this).buscarCombosPorPedido(pedido, null);
                        pedido.setCombos(combos);
                    }
                }
            }
            setarPedidoVinculadoBonificacao();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(getString(R.string.pedidotabactivity_carregardadospedido), e.getMessage());
        }
    }

    private void setarPedidoVinculadoBonificacao() {
        if (isHasExtraPedidoIdBonificacao()) {
            long idPedidoVinculado = getIntent().getLongExtra(PEDIDO_ID_BONIFICACAO, 0);
            pedido.setIDTPedido(getTipoBonificacao().IdTipoPedido);
            pedido.setVinculoPedido(idPedidoVinculado);
        }
    }

    public boolean isHasExtraPedidoIdBonificacao() {
        return getIntent().getLongExtra(PEDIDO_ID_BONIFICACAO, 0) > 0;
    }

    private TipoPedido getTipoBonificacao() {
        List<TipoPedido> operacaoList = new TipoPedidoDAO(this, null).BuscarTodasBonificacoes(TipoOperacao.BONIFICACAO.toString());
        return operacaoList.get(0);
    }

    public void carregarSpnEmpresaFilial() {
        carregarSpnTipoPedido();

        List<String> lstEmpresaFilial = new ArrayList<String>();
        idEmpresa = new Short[listaEmpresaFilial.size()];
        idFilial = new Short[listaEmpresaFilial.size()];
        int cont = 0;

        for (EmpresaFilial eFilial : listaEmpresaFilial) {
            lstEmpresaFilial.add(eFilial.getDescricao());
            idEmpresa[cont] = eFilial.getIdEmpresa();
            idFilial[cont] = eFilial.getIdFilial();
            cont++;
        }

        spnEmpresaFilial.setAdapter(new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, lstEmpresaFilial));
        spnEmpresaFilial.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                pedido.setIDEmpresa(idEmpresa[position]);
                pedido.setIDFilial(idFilial[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        int pos = 0;
        if (aPedido || rPedido || isRotated) {
            for (int i = 0; i < idEmpresa.length; i++) {
                if (idEmpresa[i] == pedido.getIDEmpresa() && idFilial[i] == pedido.getIDFilial()) {
                    pos = i;
                    break;
                }
            }
            spnEmpresaFilial.setSelection(pos);
        }
    }

    public void carregarSpnTipoPedido() {
        List<String> lstTipoPedido = new ArrayList<String>();
        idTipoPedido = new Short[listaTipoPedido.size()];
        int cont = 0;
        int contBonificar = 0;

        if (isHasExtraPedidoIdBonificacao()) {

            for (TipoPedido tipoPedido : listaTipoPedido) {

                if (tipoPedido.getTipoOperacao().equals("b") || tipoPedido.getTipoOperacao().equals("B")) {
                    lstTipoPedido.add(tipoPedido.getDescricao());
                    idTipoPedido[contBonificar] = tipoPedido.getIdTipoPedido();
                    contBonificar++;
                }
            }

        } else {

            for (TipoPedido tipoPedido : listaTipoPedido) {

                lstTipoPedido.add(tipoPedido.getDescricao());
                idTipoPedido[cont] = tipoPedido.getIdTipoPedido();
                cont++;
            }
        }

        if (lstTipoPedido.size() == 0) {
            Toast.makeText(getBaseContext(), R.string.nao_existem_tipos_de_pedido_disponiveis_para_o_cliente_selecionado, Toast.LENGTH_LONG).show();
            finish();
            pedido = null;
        }

        spnTipoPedido.setAdapter(new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, lstTipoPedido));

        spnTipoPedido.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    sharedPreference("clicouTipoPedido", true);
                    Log.d("CliqueSPN", "clicouTipoPedido");
                    clicouTipoPedido = true;
//					spnTipoPedido.setOnItemSelectedListener(PedidoTabActivity.this);
                    setMsgSpinner(spnTipoPedido, true);

                }
                return false;
            }
        });
        int pos = 0;
        for (int i = 0; i < idTipoPedido.length; ++i) {
            if (idTipoPedido[i] > 11) {
                pos = i;
                break;
            }
            pos = i;
        }
        pedido.setIDTPedido(idTipoPedido[pos]);
        spnTipoPedido.setSelection(pos);

        spnTipoPedido.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                if (possuiItemVendido() && selecionadoOperacaoTroca(position) && isTipoPedidoSelecionadoDiferenteDoPedido(idTipoPedido[position])) {
                    showDialogAviso();
                } else {

                    pedido.setIDTPedido(idTipoPedido[position]);
                    Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
                    //         if (!possuiItemVendido() || isAlteracao() || isRecuperacaoPedido() || isRotated) {
                    carregarSpnTabelaPreco();
                    //          }
                    if (idTipoPedido.length > 2) {
                        if (!clicouTipoPedido) {
                            setMsgSpinner(spnTipoPedido, false);
                        }
                    } else {
                        sharedPreference("clicouTipoPedido", true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                Log.d("CliqueSPN", "clicouTipoPedido");
            }

        });

        if (aPedido || rPedido || isRotated) {
            for (int i = 0; i < idTipoPedido.length; ++i) {
                if (idTipoPedido[i] == pedido.getIDTPedido()) {
                    pos = i;
                    break;
                }
            }
            spnTipoPedido.setSelection(pos);
        }
    }

    private boolean selecionadoOperacaoTroca(int position) {
        if (listaTabelaPrecoOperacaoTroca != null) {
            for (TabelaPrecoOperacaoTroca tpo : listaTabelaPrecoOperacaoTroca) {
                if (tpo.getTipoPedido().getIdTipoPedido() == pedido.getIDTPedido() || tpo.getTipoPedido().getIdTipoPedido() == idTipoPedido[position]) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isTipoPedidoSelecionadoDiferenteDoPedido(short idTipoPedido) {
        return pedido.getIDTPedido() == idTipoPedido ? false : true;
    }

    private boolean possuiItemVendido() {
        return pedido.NrItens > 0;
    }

    private void sharedPreference(String keyChild, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE).edit();
        editor.putBoolean(keyChild, value);
        editor.apply();
    }

    private void showDialogAviso() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(PedidoTabActivity.this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("A T E N � � O")
                .setMessage(R.string.para_poder_mudar_o_tipo_de_pedido_e_necessario_remover_todos_os_itens);

        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                spnTipoPedido.setSelection(getPositionTipoPedidoAnterior());
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private int getPositionTipoPedidoAnterior() {
        for (int posicao = 0; posicao < listaTipoPedido.size(); posicao++) {
            if (listaTipoPedido.get(posicao).getIdTipoPedido() == pedido.getIDTPedido()) {
                return posicao;
            }
        }
        return 0;
    }

    public void carregarSpnTabelaPreco() {
        listaTabela = new TabelaDAO(getBaseContext(), null).getAll(getTabelaPrecoOperacaoTroca(), listaTabelaPrecoOperacaoTroca);

        if (listaTabela == null || listaTabela.size() == 0) {
            Toast.makeText(getBaseContext(), R.string.nao_existem_tabelas_de_preco_disponiveis_para_digitacao_de_pedido, Toast.LENGTH_LONG).show();
            finish();
        }
        TabelaClienteDAO tabelaClienteDAO = new TabelaClienteDAO(getBaseContext(), null);
        List<TabelaCliente> tabelaClientes = tabelaClienteDAO.getAll(cliente.getIdCliente());

        List<String> lstTabelaPreco = new ArrayList<String>();
        List<String> lstTabelaCliente = new ArrayList<String>();
        idTabela = new Short[listaTabela.size()];
        int cont = 0;
        int cont2 = 0;


        for (Tabela tab : listaTabela) {
            for (int i = 0; i < listaEmpresaFilial.size(); i++) {
                if (tab.getIdFilial() == listaEmpresaFilial.get(i).getIdFilial() && tab.getIdEmpresa() == listaEmpresaFilial.get(i).getIdEmpresa()) {
                    lstTabelaPreco.add(tab.getDescricao());
                    idTabela[cont] = tab.getIdTabela();
                    cont++;
                }
            }
        }

        if (!tabelaClientes.isEmpty()) {
            Short[] precoCliente = new Short[tabelaClientes.size()];
            for (Tabela tab : listaTabela) {
                for (TabelaCliente tabelaCliente : tabelaClientes) {
                    if (tabelaCliente.getIdTabela() == tab.getIdTabela()) {
                        lstTabelaCliente.add(tab.getDescricao());
                        precoCliente[cont2] = tab.getIdTabela();
                        cont2++;
                    }
                }
            }
            lstTabelaPreco = lstTabelaCliente;
            idTabela = precoCliente;

        }


        if (lstTabelaPreco.size() == 0) {
            Toast.makeText(getBaseContext(), R.string.nao_existem_tabelas_de_preco_disponiveis_para_o_cliente_selecionado, Toast.LENGTH_LONG).show();
            pedido = null;
            finish();
        }

        spnTabelaPreco.setAdapter(new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, lstTabelaPreco));
        spnTabelaPreco.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                posicaoAnterior = spnTabelaPreco.getSelectedItemPosition();
                return false;
            }
        });
        spnTabelaPreco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                posicaoNova = position;
                if (indexTabelaPreco != position) {
                    indexTabelaPreco = position;
                }
                if (pedido.getNrItens() > 0 && !isAlteracao() && position != posicaoAnterior && !rPedido) {
                    confirmaAlteracaoTabela();
                }else {
                    pedido.setIDTabela(idTabela[position]);
                    carregarSpnCondicaoPgto(position);

                }

                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        int pos = 0;
        if (aPedido || rPedido || isRotated) {
            for (int i = 0; i < idTabela.length; ++i) {
                if (idTabela[i] == pedido.getIDTabela()) {
                    pos = i;
                    break;
                }
            }
            spnTabelaPreco.setSelection(pos);
        }
    }

    private short getTabelaPrecoOperacaoTroca() {
        if (listaTabelaPrecoOperacaoTroca != null) {
            for (TabelaPrecoOperacaoTroca tpo : listaTabelaPrecoOperacaoTroca) {
                if (tpo.getTipoPedido().getIdTipoPedido() == pedido.getIDTPedido()) {
                    return tpo.getTabela().getIdTabela();
                }
            }
        }
        return 0;
    }


    public void carregarSpnCondicaoPgto(int index) {
        if (listaCondicaoPagamento == null || listaCondicaoPagamento.size() == 0 || index == indexTabelaPreco) {
            listaCondicaoPagamento = new CondicaoPagamentoClienteDAO(getBaseContext()).getListaCliente(idCliente, null);
            if (listaCondicaoPagamento != null) {
                condicaoPagamentoCliente = true;
            }
        }

        if (listaCondicaoPagamento == null || listaCondicaoPagamento.size() == 0)
            listaCondicaoPagamento = new CondicaoDAO(getBaseContext(), null).listaCondicaoPedido(pedido.getIDEmpresa(), pedido.getIDFilial(), pedido.getIDTabela());

        final List<String> lstCondicaoPgto = new ArrayList<String>();
        idCondicao = new Short[listaCondicaoPagamento.size()];
        int cont = 0;

        for (Condicao con : listaCondicaoPagamento) {
            lstCondicaoPgto.add(con.getDescricao());
            idCondicao[cont] = con.getIdCondicao();
            cont++;
        }

        if (lstCondicaoPgto.size() == 0) {
            Toast.makeText(getBaseContext(), R.string.nao_existem_condicoes_de_pagamento_para_o_cliente_selecionado, Toast.LENGTH_LONG).show();
            finish();
            pedido = null;
        }

        spnCondicaoPgto.setAdapter(new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, lstCondicaoPgto));

        spnCondicaoPgto.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    sharedPreference("clicouCondPagamento", true);
                    Log.d("CliqueSPN", "clicouCondPagamento");
                    clicouCondicaoPgto = true;
                    setMsgSpinner(spnCondicaoPgto, true);

                    setMsgSpinner(spnFormaCobranca, false);
                    clicouCondicaoPgtoPelaSegundaVez = true;
                    sharedPreference("clicouFormaPagamento", false);
                }
                return false;
            }
        });

        spnCondicaoPgto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                pedido.setIDCondicao(idCondicao[position]);
                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
                configurarFormaPagamento(position);

                if (!pedido.getCombos().isEmpty()) {
                    for (Combo combo : pedido.getCombos()) {
                        if (!pedido.mesmaCondicaoPagamento(getBaseContext(), combo)) {
                            new DialogAlertComboUltils(PedidoTabActivity.this).msgAoSelecionarCondicaoPagamentoComboAtivado();
                            return;
                        }
                    }
                }

                if (idCondicao.length > 1) {
                    if (!clicouCondicaoPgto) {
                        setMsgSpinner(spnCondicaoPgto, false);
                    }
                } else {
                    sharedPreference("clicouCondPagamento", true);
                    spnFormaCobranca.setEnabled(true);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        int pos = 0;
        if (aPedido || rPedido || isRotated) {
            for (int i = 0; i < idCondicao.length; i++) {
                if (idCondicao[i] == pedido.getIDCondicao()) {
                    pos = i;
                    break;
                }
            }
            spnCondicaoPgto.setSelection(pos);
        }

    }

    private void configurarFormaPagamento(int position) {

        if (clicouCondicaoPgto && !condicaoPagamentoCliente) {
            Condicao condicao = new CondicaoDAO(getBaseContext(), null).get(pedido.getIDCondicao());
            FormaPagamento formaPagamento = new FormaPagamentoDAO(getBaseContext(), null).get(pedido.getIDFormaPagamento());
            if (!formaPagamentoDoCliente) {
                if (condicao.getQtdeDiasPrazo() == 0 && position > 0 && formaPagamento.getAmarraCondicao() == 0) {
                    listaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getFormaPagamentoPorCondicaoSelecionada(2);
                    carregarSpnFormaCobranca();
                    spnFormaCobranca.setEnabled(true);
                } else if (condicao.getQtdeDiasPrazo() > 0 && formaPagamento.getAmarraCondicao() == 0) {
                    listaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getFormaPagamentoPorCondicaoSelecionada(1);
                    carregarSpnFormaCobranca();
                    spnFormaCobranca.setEnabled(true);
                } else if (condicao.getQtdeDiasPrazo() > 0 && formaPagamento.getAmarraCondicao() != 0) {
                    listaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getFormaPagamentoPorCondicaoSelecionada(1);
                    carregarSpnFormaCobranca();
                    spnFormaCobranca.setEnabled(true);
                    if (formaPagamento.getAmarraCondicao() == 1)
                        spnFormaCobranca.setSelection(0);
                } else if (condicao.getQtdeDiasPrazo() == 0 && position > 0 && formaPagamento.getAmarraCondicao() != 0) {
                    listaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getFormaPagamentoPorCondicaoSelecionada(2);
                    carregarSpnFormaCobranca();
                    spnFormaCobranca.setEnabled(true);
                    if (formaPagamento.getAmarraCondicao() == 2)
                        spnFormaCobranca.setSelection(0);
                } else if (condicao.getQtdeDiasPrazo() == 0 && position == 0 && formaPagamento.getAmarraCondicao() == 0) {
                    listaFormaPagamento = null;
                    carregarListas();
                    carregarSpnFormaCobranca();
                    spnFormaCobranca.setSelection(0);
                    spnFormaCobranca.setEnabled(false);
                } else {
                    spnFormaCobranca.setEnabled(true);
                }
            } else {
                carregarSpnFormaCobranca();
            }
        } else {
            carregarSpnFormaCobranca();
        }

    }

    private void setMsgSpinner(Spinner spinner, boolean isNull) {
        TextView errorText = (TextView) spinner.getSelectedView();
        if (isNull) {
            errorText.setError(null);
        } else {
            if (errorText!= null)
            errorText.setError("");
        }
    }

    public void carregarSpnFormaCobranca() {
        List<String> lstFormaCobranca = new ArrayList<String>();
        idFormaPagamento = new Long[listaFormaPagamento.size()];
        int cont = 0;

        for (FormaPagamento fPgto : listaFormaPagamento) {
            lstFormaCobranca.add(fPgto.getDescricao());
            idFormaPagamento[cont] = fPgto.getIdFormaPagamento();
            cont++;
        }

        if (lstFormaCobranca.size() == 0) {
            Toast.makeText(getBaseContext(), R.string.nao_existem_formas_de_cobranca_disponiveis_para_o_cliente_selecionado, Toast.LENGTH_LONG).show();
            pedido = null;
            finish();
        }

        spnFormaCobranca.setEnabled(false);
        spnFormaCobranca.setAdapter(new ArrayAdapter<String>(getBaseContext(), R.layout.spinner_item, lstFormaCobranca));
        spnFormaCobranca.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    sharedPreference("clicouFormaPagamento", true);
                    Log.d("CliqueSPN", "clicouFormaPagamento");
                    clicouFormaPagto = true;
                    clicouCondicaoPgtoPelaSegundaVez = false;
                    setMsgSpinner(spnFormaCobranca, true);
                }
                return false;
            }
        });
        spnFormaCobranca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                pedido.setIDFormaPagamento(idFormaPagamento[position]);
                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);

                if (idFormaPagamento.length > 1) {
                    if (!clicouFormaPagto || clicouCondicaoPgtoPelaSegundaVez) {
                        setMsgSpinner(spnFormaCobranca, false);
                    }
                } else {
                    sharedPreference("clicouFormaPagamento", true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        int pos = 0;
        if (aPedido || rPedido || isRotated) {
            for (int i = 0; i < idFormaPagamento.length; ++i) {
                if (idFormaPagamento[i] == pedido.getIDFormaPagamento()) {
                    pos = i;
                    break;
                }
            }
            spnFormaCobranca.setSelection(pos);
            aPedido = false;
            rPedido = false;
        }
    }

    public void carregarListas() {
        try {
            listaEmpresaFilial = new EmpresaFilialDAO(getBaseContext(), null).getAll();
            listaTipoPedido = new TipoPedidoDAO(getBaseContext(), null).getByCliente(idCliente);
            if (listaFormaPagamento == null || listaFormaPagamento.size() == 0)
                listaFormaPagamento = new FormaPagamentoClienteDAO(getBaseContext()).getListaCliente(idCliente, null);
            if (listaFormaPagamento != null) {
                formaPagamentoDoCliente = true;
            }
            listaTabelaPrecoOperacaoTroca = new TabelaPrecoOperacaoTrocaDAO(getBaseContext(), null).getAll();

            if (listaFormaPagamento == null || listaFormaPagamento.size() == 0)
                listaFormaPagamento = new FormaPagamentoDAO(getBaseContext(), null).getAll();

            if (listaEmpresaFilial.size() == 0) {
                Toast.makeText(getBaseContext(), R.string.nao_existem_empresas_filiais_disponiveis_para_digitacao_pedido, Toast.LENGTH_LONG).show();
                finish();
            } else if (listaTipoPedido.size() == 0) {
                Toast.makeText(getBaseContext(), R.string.nao_existem_tipos_de_pedido_disponiveis_para_digitacao_de_pedido, Toast.LENGTH_LONG).show();
                finish();
            } else if (listaFormaPagamento.size() == 0) {
                Toast.makeText(getBaseContext(), R.string.nao_existem_formas_de_pagamento_disponiveis_para_digitacao_de_pedido, Toast.LENGTH_LONG).show();
                finish();
            }

			/* Solicita??o dia 24/04/2013 Ticket N?1656. NN?o considerar validade da tabela de pre?o
			 * Date dataAtual = new Date(); // SE AS TABELAS N?o ESTIVEREM DENTRO DO PRAZO DE VALIDADE
			if (dataAtual.before(listaTabela.get(0).getDataInicial()) || dataAtual.after(listaTabela.get(0).getDataFinal())) {
				Toast.makeText(getBaseContext(), String.format("As Tabelas de Pre?o est?o fora da validade que v?o de %s at? %s", sdfDataHora.format(listaTabela.get(0).getDataInicial()), sdfDataHora.format(listaTabela.get(0).getDataFinal())), Toast.LENGTH_LONG).show();
				finish();
			}*/
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "Problemas ao montar Listas", Toast.LENGTH_LONG).show();
        }
    }

//    public void confirmaAlteracaoTabela() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder
//                .setTitle(R.string.atencao)
//                .setMessage("Os itens vendidos terão seus preços reajustados, e seus descontos zerados.\nDeseja realmente prosseguir?")
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Utils.serializarObjeto(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
//                        PedidoTabHost parent = (PedidoTabHost) getParent();
//                        parent.getTabHost().getTabWidget().getChildAt(1).setSelected(true);
//                        parent.getTabHost().getTabWidget().getChildAt(1).performClick();
//                        mIsDialogCancelou = false;
//                        dialog.dismiss();
//                    }
//                })
//                .setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        spnTabelaPreco.destroyDrawingCache();
//                        mIsDialogCancelou = true;
//                        dialog.dismiss();
//                    }
//                })
//                .show();
//    }

    public void confirmaAlteracaoTabela() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.atencao)
                .setMessage(getMsgTabelaComDesconto())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.sim, null)
                .setNegativeButton(R.string.nao, null)
                .setCancelable(false);

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setCancelable(false);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alteraOpcaoSpinnerTabelaPreco(false, posicaoNova);
                pedido.setIDTabela(idTabela[posicao]);
                carregarSpnCondicaoPgto(posicao);
                Utils.serializarObjeto(getBaseContext(), PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
                PedidoTabHost parent = (PedidoTabHost) getParent();
                parent.getTabHost().getTabWidget().getChildAt(1).setSelected(true);
                parent.getTabHost().getTabWidget().getChildAt(1).performClick();
                isZerarDesconto = true;

                dialog.dismiss();
            }
        });
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alteraOpcaoSpinnerTabelaPreco(true, posicaoAnterior);
                isZerarDesconto = false;
                dialog.dismiss();
            }
        });
    }

    private String getMsgTabelaComDesconto() {
        Tabela tabelaSelecionada = new Tabela();
        for(Tabela tabela: listaTabela){
            if (tabela.getIdTabela() == idTabela[posicaoNova]){
                tabelaSelecionada = tabela;
                break;
            }

        }
        return tabelaSelecionada.isPermiteDescontoPorTabela()? getString(R.string.troca_de_tabela_com_desconto): getString(R.string.troca_de_tabela_sem_desconto);
    }

    private void alteraOpcaoSpinnerTabelaPreco(boolean b, int posicaoNova) {
        mIsDialogCancelou = b;
        Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);
        posicao = posicaoNova;
        spnTabelaPreco.setSelection(posicao);

    }

    public void habilitarCampos(boolean enabled) {
        spnEmpresaFilial.setEnabled(enabled);
        spnEmpresaFilial.setFocusable(enabled);
        spnEmpresaFilial.setClickable(enabled);

//        spnTabelaPreco.setEnabled(enabled);
//        spnTabelaPreco.setFocusable(enabled);
//        spnTabelaPreco.setClickable(enabled);
    }

    public void preencherDadosCampos() {
        edtObservacao.setText(pedido.getObservacao());
        edtNrPedido.setText(String.format("%d", pedido.getIDPedido()));
        edtDataPedido.setText(String.format("%s", Utils.sdfDataPtBr.format(pedido.getDataPedido())));
        edtTotalBruto.setText(String.format("%.4f", pedido.getTotalGeral() < 0 ? 0 : pedido.getTotalGeral()));
        edtTotalLiquido.setText(String.format("%.4f", pedido.getTotalLiquido() < 0 ? 0 : pedido.getTotalLiquido()));
        edtCliente.setText(String.format("%d - %s", cliente.getIdCliente(), cliente.getRazao()));
        txtRetabilidade.setText(String.format("%.2f%%", pedido.getRentabilidade() < 0 ? 0 : pedido.getRentabilidade()));
        lnlRetabilidadePerc.setVisibility(View.VISIBLE);
        edtObservacao.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        if (ultimaCondicaoCliente != null) {
            edtUltimaCondicaoCliente.setText(ultimaCondicaoCliente.getDescricao());
        }

        Condicao condicao = new CondicaoDAO(getBaseContext(), null).get(pedido.getIDCondicao());
        if (condicao != null) {
            try {
                edtParcelas.setText(String.format("%.4f", pedido.getTotalLiquido() < 0 ? 0 : pedido.getTotalLiquido() / condicao.getQtdeParcelas()));
            } catch (ArithmeticException e) {
                edtParcelas.setText("0,0000");
            }
        } else {
            edtParcelas.setText("0,0000");
        }
        Tabela tabela = new TabelaDAO(getBaseContext(), null).get(pedido.getIDTabela());

        if (tabela == null) {
//            lnlRetabilidadePerc.setVisibility(View.GONE);
            lnRetabilidadeImg.setVisibility(View.GONE);

        } else if (pedido.getIDTabela() == tabela.getIdTabela()) {
            if (tabela.isUltilizaRentabilidade())
                visualizarRentabilidadePedido();
            else {
             //   lnlRetabilidadePerc.setVisibility(View.GONE);
                lnRetabilidadeImg.setVisibility(View.GONE);
            }
            txtRetabilidade.setTextColor(ConfigSemafaro.getTextColorSemafaroPedido(parametro, pedido));
        }
    }

    private void visualizarRentabilidadePedido() {
        if (parametro.getVisualizarRentabilidade()) {
           // lnlRetabilidadePerc.setVisibility(View.GONE);
            lnRetabilidadeImg.setVisibility(View.VISIBLE);
            pedido_imgRentabilidade.setImageResource(ConfigSemafaro.getImagemSemaforoPedido(parametro, pedido));

        } else {
            lnRetabilidadeImg.setVisibility(View.GONE);
            lnlRetabilidadePerc.setVisibility(View.VISIBLE);
            txtRetabilidade.setTextColor(ConfigSemafaro.getTextColorSemafaroPedido(parametro, pedido));
        }
    }


    public void recuperarLayout() {
        spnEmpresaFilial = (Spinner) findViewById(R.idPedido.spnEmpresaFilial);
        spnTipoPedido = (Spinner) findViewById(R.idPedido.spnTipoPedido);
        spnTabelaPreco = (Spinner) findViewById(R.idPedido.spnTabelaPreco);
        spnCondicaoPgto = (Spinner) findViewById(R.idPedido.spnCondicaoPgto);
        spnFormaCobranca = (Spinner) findViewById(R.idPedido.spnFormaCobranca);

        edtObservacao = (EditText) findViewById(R.idPedido.edtObservacao);
        edtNrPedido = (EditText) findViewById(R.idPedido.edtNrPedido);
        edtDataPedido = (EditText) findViewById(R.idPedido.edtDataPedido);
        edtTotalBruto = (EditText) findViewById(R.idPedido.edtTotalBruto);
        edtTotalLiquido = (EditText) findViewById(R.idPedido.edtTotalLiquido);
        edtParcelas = (EditText) findViewById(R.idPedido.edtParcelas);
        edtCliente = (EditText) findViewById(R.idPedido.edtCliente);
        edtUltimaCondicaoCliente = (EditText) findViewById(R.idPedido.edtUltimaCondicaoCliente);
        lnlRetabilidadePerc = (LinearLayout) findViewById(R.id.lnlRetabilidadePerc);
        lnRetabilidadeImg = (LinearLayout) findViewById(R.id.lnRetabilidadeImg);
        pedido_imgRentabilidade = (ImageView) findViewById(R.id.pedido_imgRentabilidade);
        txtRetabilidade = (EditText) findViewById(R.id.txtRetabilidade);
        edtObservacao.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                pedido.setObservacao(edtObservacao.getText().toString());
                Utils.serializarObjeto(getBaseContext(), PEDIDO_SERIALIZAR, pedido);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edtNrPedido.setKeyListener(null);
        edtDataPedido.setKeyListener(null);
        edtTotalBruto.setKeyListener(null);
        edtTotalLiquido.setKeyListener(null);
        edtParcelas.setKeyListener(null);
        edtCliente.setKeyListener(null);
        edtUltimaCondicaoCliente.setKeyListener(null);
        txtRetabilidade.setKeyListener(null);
    }

    @SuppressWarnings("deprecation")
    public void liberarAbaItens(boolean mostrarAbaItens) {
        PedidoTabHost parentActivity = (PedidoTabHost) getParent();

        if (mostrarAbaItens) {
            parentActivity.getTabHost().getTabWidget().getChildAt(1).setVisibility(View.VISIBLE);
        } else {
            parentActivity.getTabHost().getTabWidget().getChildAt(1).setVisibility(View.GONE);
        }
    }

    private void iniciarServicoLocalizacao() {
        LocationService.iniciar(getBaseContext(), Principal.CATEGORIA,
                cliente.getIdCliente(), cliente.getRazao(),
                (short) parametro.getIdVendedor());
    }
}