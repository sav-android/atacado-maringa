package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import br.net.sav.modelo.EstruturaGravarPedido;

import static br.net.sav.utils.ComponentePedidoUtils.confirmaSaida;
import static br.net.sav.utils.ComponentePedidoUtils.showDialogLegenda;

public class PedidoTabHost extends TabActivity {
	private TabHost tabHost;
	public static final String INTENT = "PedidoTabHost";
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.pedido_tabhost);

		long idPedidoBonificado = 0;

		Intent it = getIntent();
		long idCliente = it.getLongExtra("IdCliente", 0);
		long idPedido = it.getLongExtra("IdPedido", 0);
		boolean rPedido = it.getBooleanExtra("RecuperacaoPedido", false);
		idPedidoBonificado = it.getLongExtra(PedidoTabActivity.PEDIDO_ID_BONIFICACAO, 0);

		Resources res = getResources();
		tabHost = getTabHost();
		TabSpec spec;
		Intent intent;

		intent = new Intent(PedidoTabActivity.INTENT);
		intent.putExtra("IdCliente", idCliente);
		intent.putExtra("RecuperacaoPedido", rPedido);
		intent.putExtra("IdPedido", idPedido);
		intent.putExtra(PedidoTabActivity.PEDIDO_ID_BONIFICACAO, idPedidoBonificado);
		intent.addCategory(Principal.CATEGORIA);
		spec = tabHost.newTabSpec("Capa Pedido").setIndicator("Capa Pedido", res.getDrawable(R.drawable.tab_principal)).setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent(ItemTabActivity.INTENT);
		intent.putExtra("IdCliente", idCliente);
		intent.putExtra(PedidoTabActivity.PEDIDO_ID_BONIFICACAO, idPedidoBonificado);
		intent.addCategory(Principal.CATEGORIA);
		spec = tabHost.newTabSpec("Itens Pedido").setIndicator("Itens Pedido", res.getDrawable(R.drawable.itens_pedido)).setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent(this, ComboTabActivity.class);
		spec = tabHost.newTabSpec("Combos").setIndicator("Combos", res.getDrawable(R.drawable.itens_pedido)).setContent(intent);
		tabHost.addTab(spec);

		tabHost.getTabWidget().getChildTabViewAt(2).setVisibility(View.GONE);
		prefs = getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE);
	}

	@Override
	public void finishFromChild(Activity child) {
		setResult(RESULT_OK, child.getIntent());
		super.finishFromChild(child);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_pedido_itens, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.idMenuPedidoItens.gravar:
				new EstruturaGravarPedido(this.getCurrentActivity(),prefs).salvarPedido();

				break;
			case R.idMenuPedidoItens.legenda:
				showDialogLegenda(this.getCurrentActivity());

				break;
			case R.idMenuPedidoItens.sair:
				confirmaSaida(this.getCurrentActivity());

				break;
			default:
				break;
		}
		return true;
	}




	public void switchTab(int tab) {
		tabHost.setCurrentTab(tab);
	}
}