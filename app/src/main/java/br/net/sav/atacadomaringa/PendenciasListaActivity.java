package br.net.sav.atacadomaringa;

import android.app.ListActivity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.net.sav.dao.PendenciaDAO;
import br.net.sav.modelo.Pendencia;

public class PendenciasListaActivity extends ListActivity {
	public static final String INTENT = "ListaPendencias";

	private List<Pendencia> lista;
	Adaptador adaptador;
	TextView qtdVencer, qtdVencidos, qtdTotal, totVencer, totVencidos, totTotal;
	LinearLayout lnRodape;

	private long idCliente;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		idCliente = getIntent().getLongExtra("IdCliente", 0);

		filtrarPendencias(0); // TODOS
	}

	@Override
	protected void onResume() {
		super.onResume();
		atualizaAdapter();
	}

	public void atualizaAdapter() {
		if (adaptador == null) {
			setContentView(R.layout.pendencias_lista);
			inicializarComponentes();
			adaptador = new Adaptador();
			setListAdapter(adaptador);
			getListView().setFastScrollEnabled(true);
		} else {
			adaptador.changeData(lista);
			getListView().setFastScrollEnabled(true);
		}
		barraTitulo();
	}

	private void filtrarPendencias(int opcao) {
		try {
			switch (opcao) {
				case 0: // Filtrar TODAS
					lista = new PendenciaDAO(getBaseContext(), null).listaPendencia(idCliente, (short) 0);
					break;
				case 1:
					lista = new PendenciaDAO(getBaseContext(), null).listaPendencia(idCliente, (short) 1);
					break;
				case 2:
					lista = new PendenciaDAO(getBaseContext(), null).listaPendencia(idCliente, (short) 2);
					break;
			}
			onResume();
		} catch (Exception e) {
			Toast.makeText(getBaseContext(), "Erro ao filtrar pendencicas:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_filtro_pendencias, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.idMenuPendencias.todos:
				filtrarPendencias(0);
				return true;
			case R.idMenuPendencias.avencer:
				filtrarPendencias(2);
				return true;
			case R.idMenuPendencias.vencidos:
				filtrarPendencias(1);
				return true;

			default:
				return super.onOptionsItemSelected(item);

		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		l.setSelected(true);
	}

	public void barraTitulo() {
		long lTitulosAvencer = 0;
		double dblTotalTitulosVencer = 0;
		long lTitulosVencidos = 0;
		double dblTotalTitulosVencidos = 0;
		long lTitulosGeral = 0;
		double dblTotalGeral = 0;

		Date dataAtual = new Date();
		String dataFormatada = null;

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		for (Pendencia p : lista) {
			if (p.getVencimento() != null) {
				dataFormatada = sdf.format(p.getVencimento());

				if ((dataFormatada != null) && (p.getVencimento().before(dataAtual))) {
					lTitulosVencidos++;
					dblTotalTitulosVencidos += p.getValor();
				} else {
					lTitulosAvencer++;
					dblTotalTitulosVencer += p.getValor();
				}

				lTitulosGeral++;
				dblTotalGeral += p.getValor();
			}
		}

		try {

			if (lista == null || lista.isEmpty()) {
				lnRodape.setVisibility(View.GONE);
			}else {
				lnRodape.setVisibility(View.VISIBLE);
			}
			NumberFormat nf = NumberFormat.getCurrencyInstance();

			String aVencet = String.valueOf(lTitulosAvencer);
			String vencidos = String.valueOf(lTitulosVencidos);
			String total = String.valueOf(lTitulosGeral);
			String formatAvencer = nf.format(dblTotalTitulosVencer);
			String formatVencidos = nf.format(dblTotalTitulosVencidos);
			String formatTotal= nf.format(dblTotalGeral);
			qtdVencer.setText(aVencet);
			totVencer.setText(formatAvencer);
			qtdVencidos.setText(vencidos);
			totVencidos.setText(formatVencidos);
			qtdTotal.setText(total);
			totTotal.setText(formatTotal);
//			TextView titulo = (TextView) findViewById(android.R.id.title);
//			if (titulo != null) {
//				titulo.setSingleLine(false);
//				titulo.setTypeface(Typeface.MONOSPACE);
//				titulo.setText(String.format("A Vencer: %03d     R$ %.2f\n" + "Vencidos: %03d     R$ %.2f\n" + "Total   : %03d     R$ %.2f", lTitulosAvencer, dblTotalTitulosVencer, lTitulosVencidos, dblTotalTitulosVencidos, lTitulosGeral, dblTotalGeral));
//				titulo.setTextSize(14);
//
//				View titleBar = (View) titulo.getParent();
//				titleBar.setBackgroundColor(Color.TRANSPARENT);
//				titleBar.getBackground().setAlpha(40);
//			}
		} catch (Exception e) {
			Log.e(getString(R.string.pendenciaslistactivity_tabhost), e.getMessage());
		}
	}

	private void inicializarComponentes(){
		lnRodape = (LinearLayout) findViewById(R.id.lnRodape);
		qtdVencer = (TextView) findViewById(R.id.ClientePendencia_txvQtdeNaoVencidos);
		qtdVencidos = (TextView) findViewById(R.id.ClientePendencia_txvQtdeVencidos);
		qtdTotal = (TextView) findViewById(R.id.ClientePendencia_txvQtdeTodos);
		totVencer = findViewById(R.id.ClientePendencia_txvValorNaoVencidos);
		totVencidos = findViewById(R.id.ClientePendencia_txvValorVencidos);
		totTotal = findViewById(R.id.ClientePendencia_txvValorTodos);
	}

	// ------------------ADAPTER-----------------
	public class Adaptador extends BaseAdapter {
		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void changeData(List<Pendencia> lista) {
			PendenciasListaActivity.this.lista = lista;
			notifyDataSetChanged();
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.pendencia_linha, null);
			}

			if (position % 2 == 0) {
				view.setBackgroundResource(R.drawable.alterselector1);
			} else {
				view.setBackgroundResource(R.drawable.alterselector2);
			}

			Pendencia pendencia = lista.get(position);

			TextView razao = (TextView) view.findViewById(R.idListaPendencias.razao);
			TextView numero = (TextView) view.findViewById(R.idListaPendencias.numeroTitulo);
			TextView tipo = (TextView) view.findViewById(R.idListaPendencias.tipoDocumento);
			TextView vencimento = (TextView) view.findViewById(R.idListaPendencias.vencimento);
			TextView valor = (TextView) view.findViewById(R.idListaPendencias.valor);
			TextView portador = (TextView) view.findViewById(R.idListaPendencias.portador);
			TextView saldo = (TextView) view.findViewById(R.idListaPendencias.saldo);

			try {
				Date dataAtual = new Date();
				String dataFormatada = null;

				if (pendencia.getVencimento() != null) {

					dataFormatada = sdf.format(pendencia.getVencimento());

					vencimento.setText("Vencimento: " + dataFormatada);

					if ((dataFormatada != null) && (pendencia.getVencimento().before(dataAtual)))
						vencimento.setTextColor(Color.RED);
					else
						vencimento.setTextColor(Color.WHITE);
				}
			} catch (Exception e) {
				Log.d(getString(R.string.pendenciaslistaadapter_data), e.getMessage());
			}

			Pendencia p = lista.get(position);

			razao.setText(String.format("%d - %s", p.getIdCliente(), pendencia.getRazao()));
			razao.setTextColor(Color.GREEN);
			razao.setTypeface(Typeface.DEFAULT_BOLD);

			numero.setText(getString(R.string.numero_do_titulo) + String.valueOf(p.getNrTitulo()));
			tipo.setText("Tipo Documento: " + p.getTipoDocumento());
			vencimento.setText("Vencimento: " + sdf.format(p.getVencimento()));
			valor.setText("Valor: " + String.valueOf(p.getValor()));
			portador.setText("Portador: " + p.getPortador());
			saldo.setText("Saldo: " + String.valueOf(p.getSaldo()));

			return view;
		}
	}
}