package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.renderer.BubbleChartRenderer;

import java.io.File;
import java.util.Date;
import java.util.List;

import br.net.sav.CadastroProvedorActivity;
import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.Utils;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.UsuarioDAO;
import br.net.sav.dialog.GerarSenhaDialog;
import br.net.sav.modelo.AlterarObjeto;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Usuario;
import br.net.sav.service.ConfigProvedor;
import br.net.sav.util.StringUtils;
import br.net.sav.util.dao.DBHelperUtils;
import br.net.sav.util.dao.ProvedorDAO;
import br.net.sav.util.modelo.Provedor;
import br.net.sav.utils.PermissionUtils;
import br.net.sav.utils.ToastUtils;

import static android.os.Build.VERSION.SDK;
import static android.os.Build.VERSION.SDK_INT;
import static br.net.sav.IntegradorWeb.dto.Vendedor.buscarVendedorRegistradoUtils;
import static br.net.sav.service.VendedorService.verificarVendedor;
import static br.net.sav.utils.PermissionUtils.alertAndFinish;
import static br.net.sav.utils.PermissionUtils.permissionSLL;
import static br.net.sav.utils.PermissionUtils.verifyPermissions;


public class Principal extends Activity implements OnClickListener {

    public static final String CATEGORIA = "ATACADOMARINGA";
    public static final String INTENT = "Principal";
    public static Context context;

    public static final int TRANSMISSAO = 6;
    public static final int FALSO = 7;
    public static final int TRANSMISSAO_SENHA = 8;

    private boolean copiarBanco = true;

    public static boolean jaCriado = false;

    public short IDUsuario = 0;
    private Provedor provedor;
    private Parametro parametro;
    private Vendedor vendedorRegistardo;
    public static boolean isPermission = false;

    private Usuario usuario;

    PermissionUtils.IPermission iPermission = new PermissionUtils.IPermission() {
        @Override
        public String[] parametroPemission() {
            return new String[0];
        }

        @Override
        public String titleApp() {
            return getString(R.string.app_name);
        }

        @Override
        public String msgPermissonConsedida() {
            return getString(R.string.permissao_autorizada);
        }

        @Override
        public String msgPemissionNaoConsedida() {
            return getString(R.string.utilizar_aplicativo);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SDK_INT >= 23) {
            PermissionUtils.validate(this, 0, iPermission);
        } else {
            validaPermissoes();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == requestCode) {
            if (verifyPermissions(grantResults) || isPermission) {
                validaPermissoes();

                Toast.makeText(this, iPermission.msgPermissonConsedida(), Toast.LENGTH_SHORT).show();
                isPermission = true;

                return;
            } else {
                alertAndFinish(this);
            }
        }
    }

    private void validaPermissoes() {
        parametro = new ParametroDAO(getBaseContext(), null).get();
        vendedorRegistardo = buscarVendedorRegistradoUtils(getBaseContext());

        SQLiteDatabase db = openOrCreateDatabase(DBHelperUtils.NOME_BANCO_DE_DADOS, Context.MODE_PRIVATE, null);
        new DBHelperUtils(getBaseContext()).onCreate(db);
        //db.close(); singleton();

        db = openOrCreateDatabase(DBHelper.BANCO, Context.MODE_PRIVATE, null);
        new DBHelper(getBaseContext()).onCreate(db);
        //db.close(); singleton();

        Utils.criarDiretorios();

        if (copiarBanco)
            copiarBanco();

        verificarUltimoAcesso();

        if (!new UsuarioDAO(getBaseContext(), null).existeUsuario())
            criarUsuarioPadrao();
        //TODO comentar aqui para Utilização API
//        if (!jaCriado && Utils.existeArquivosParaImportar())
//            startActivity(new Intent(getBaseContext(), ImportacaoActivity.class));

        installShortCut();

        // Seta as prefer�ncias padr�o a primeira vez que entrar
        PreferenceManager.setDefaultValues(this, R.xml.ftp_preferences, false);

        permissionSLL(this);

        verificarVendedor(getBaseContext(), vendedorRegistardo);
    }


    private void iniciar() {

        setContentView(R.layout.login);

        UsuarioDAO usuarioDAO = new UsuarioDAO(getBaseContext(), null);
        final List<Usuario> lista = usuarioDAO.getAll(false);
        String[] usuarios = new String[lista.size()];

        Parametro p = null;
        p = new ParametroDAO(getBaseContext(), null).get();
        int padrao = 0;

        for (int i = 0; i < lista.size(); i++) {
            Usuario u = lista.get(i);
            usuarios[i] = u.getNome();

            if (p != null && u.getIDVendedor() == p.getIdVendedor())
                padrao = i;
        }

        ArrayAdapter<String> ad = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, usuarios);

        Spinner spnUsuario = (Spinner) findViewById(R.idLogin.spnUsuario);
        spnUsuario.setAdapter(ad);
        spnUsuario.setSelection(padrao);

        spnUsuario.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                return;
            }

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                IDUsuario = lista.get(position).getIDUsuario();

            }
        });
        Button btnLogin = (Button) findViewById(R.idLogin.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.iniciar();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 3:
                if (resultCode == Activity.RESULT_OK)
                    finish();
                break;

            case 5: // Senha para alterar a data de �ltimo uso
                if (resultCode == Activity.RESULT_OK) {
                    Parametro parametro = null;
                    ParametroDAO parametroDAO = new ParametroDAO(getBaseContext(), null);
                    parametro = parametroDAO.get();

                    String senhaDigitada = data.getStringExtra("SenhaDigitada");
                    if ((!senhaDigitada.equals(Utils.senhaAlterarData((short) parametro.getIdVendedor())) && (!new File(Environment.getExternalStorageDirectory().getPath() + "/sav/cerpro.txt").exists()))) {
                        Toast.makeText(getBaseContext(), R.string.a_senha_digitada_nao_e_para_o_dia_selecionado_favor_verifique, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        parametroDAO.atualizaDataUltimoAcesso(new Date());
                    }
                } else {
                    finish();
                }
                break;

            case TRANSMISSAO:
                if (resultCode == CadastroProvedorActivity.ABRIR_TRANSMISSAO) {
                    Intent it;
                    it = new Intent(getBaseContext(), TransmissaoActivity.class);
                    startActivity(it);
                } else if (resultCode == CadastroProvedorActivity.USUARIO_POP_SENHA_ERRADOS) {
                    dialogNumeroParaCadastroProvedor();
                }
                break;

            case TRANSMISSAO_SENHA:
                Parametro parametro = new ParametroDAO(getBaseContext(), null).get();

                if (parametro != null && ControleAcesso.quantidadeDiasPassouUltimaTransmissao(this) >= 1) {
                    showDialogSenhaTrabalharSemConexao(parametro);
                }

                break;
        }
    }

    public boolean verificarUltimoAcesso() {
        boolean retorno = false;
        final ParametroDAO parametroDAO = new ParametroDAO(getBaseContext(), new DBHelper(getBaseContext()).getWritableDatabase());
        Parametro parametro = parametroDAO.get();
        final Date dataAtual = new Date();
        Date dataUltimoAcesso = new Date();

        if (parametro != null) {
            long qtdeDiasPassou = 0;

            if (parametro.getDataUltimoAcesso() != null) {

                qtdeDiasPassou = Utils.qtdeDiasPassou(parametro.getDataUltimoAcesso());
                dataUltimoAcesso = parametro.getDataUltimoAcesso();


                if (qtdeDiasPassou >= 2 && qtdeDiasPassou <= 30) { //Fazem dois ou mais dias que o sistema n�o � acessado.
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.atencao);
                    builder.setMessage(String.format(getString(R.string.fazem_dias_que_voce_nao_utiliza_o_sav), qtdeDiasPassou, Utils.sdfDataPtBr.format(parametro.getDataUltimoAcesso()), Utils.sdfDataPtBr.format(dataAtual)))
                            .setCancelable(false)
                            .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    parametroDAO.atualizaDataUltimoAcesso(dataAtual);
                                }
                            })
                            .setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                    builder.show();
                    retorno = true;

                } else if (dataAtual.before(dataUltimoAcesso) || qtdeDiasPassou > 30) { //DataUltimaCompra do aparelho � inferior a data do �ltimo acesso
                    msgAlertaData(dataAtual, dataUltimoAcesso);
                    retorno = true;

//					AlertDialog.Builder builder = new AlertDialog.Builder(this);
//					builder.setTitle(R.string.atencao);
//					builder.setMessage(String.format(getString(R.string.a_data_do_dispositivo_foi_alterada_para_uma_data_inferior_a_data_do_ultimo_uso), Utils.sdfDataPtBr.format(parametro.getDataUltimoAcesso()), Utils.sdfDataPtBr.format(dataAtual)))
//					.setCancelable(false)
//					.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							abrirConfiguracoeesDataHora();
//							finish();
//						}
//					})
//					.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialog, int which) {
//							finish();
//						}
//					});
//					builder.show();
                } else {
                    parametroDAO.atualizaDataUltimoAcesso(dataAtual);
                }
            } else {
                parametroDAO.atualizaDataUltimoAcesso(dataAtual);
            }
        }
        parametroDAO.fecharDB();
        return retorno;

    }

    public void msgAlertaData(final Date dataAtual, final Date dataUltimoAcesso) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setTitle(R.string.atencao);
        builder.setTitle(String.format(getString(R.string.data_do_dispositivo)));

        String[] opcoes = {"" +
                "1 - Se a data do dispositivo estive alterada, retorne para data original!",
                "2 - Se está a Varios dias sem acesso ao Dispositivo, Digite a senha para desbloquear!",
                "3 - sair"};
        builder.setItems(opcoes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        abrirConfiguracoeesDataHora();
                        finish();
                        break;
                    case 1:
                        new GerarSenhaDialog(getApplicationContext(), Principal.this)
                                .showDialogSenhaDesbloquearUltimoAcesso(dataAtual, dataUltimoAcesso, new AlterarObjeto() {
                                    @Override
                                    public void alterar(Pedido pedido, boolean alterado) {
                                        if (alterado) {
                                            new ParametroDAO(getBaseContext(), null).atualizaDataUltimoAcesso(dataAtual);
                                            dialog.dismiss();
                                        }
                                    }
                                });
                        break;
                    case 2:
                        finish();
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void abrirConfiguracoeesDataHora() {
        startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
    }

    private void copiarBanco() {
        String arquivoBanco = "/data/data/br.net.sav.atacadomaringa/databases/Atacado Maringa.db";
        String destFile = "/mnt/sdcard/sav/Atacado Maringa.db";

        String arquivoBancoUtils = "/data/data/br.net.sav.atacadomaringa/databases/Utils.db";
        String destFileUtils = "/mnt/sdcard/sav/Utils_atacadomaringa.db";

        Utils.copiarArquivo(arquivoBanco, destFile);
        Utils.copiarArquivo(arquivoBancoUtils, destFileUtils);
    }

    @Override
    public void onClick(View v) {
        Parametro parametro = new ParametroDAO(getBaseContext(), null).get();

/*		if(parametro != null && ControleAcesso.quantidadeDiasPassouUltimaTransmissao(this) >= 1) {
			Intent it;
			it = new Intent(getBaseContext(), TransmissaoActivity.class);
			startActivityForResult(it, TRANSMISSAO_SENHA);
		} else {*/
        UsuarioDAO usuarioDAO = new UsuarioDAO(getBaseContext(), null);

        Usuario usuario = usuarioDAO.get(IDUsuario);

        TextView txtSenha = (TextView) findViewById(R.idLogin.txtSenha);
        if (txtSenha.getText().toString().trim().length() == 0 && !validarSenha(usuario.getSenha(), txtSenha.getText().toString())) {
            txtSenha.setError("Digite sua senha");
            txtSenha.requestFocus();
        } else if (validarSenha(usuario.getSenha(), txtSenha.getText().toString())) {
            if (verificarUltimoAcesso()) {
                ToastUtils.mostrarMensagem(getBaseContext(), "verificando acesso");
            } else {
                iniciarMenuPrincipal();
            }
        } else {
            txtSenha.setError(getString(R.string.senha_invalida_digite_novamente));
            txtSenha.requestFocus();
        }

        //TODO comentar provedor para test com api
//        if (existeProvedor()) {
//            if (txtSenha.getText().toString().trim().length() == 0 && !validarSenha(usuario.getSenha(), txtSenha.getText().toString())) {
//                txtSenha.setError("Digite sua senha");
//                txtSenha.requestFocus();
//            } else if (validarSenha(usuario.getSenha(), txtSenha.getText().toString())) {
//                if (verificarUltimoAcesso()) {
//                    ToastUtils.mostrarMensagem(getBaseContext(), "verificando acesso");
//                } else {
//                    iniciarMenuPrincipal();
//                }
//            } else {
//                txtSenha.setError(getString(R.string.senha_invalida_digite_novamente));
//                txtSenha.requestFocus();
//            }
//        }
//        else {
////				Intent it = new Intent("Provedor");
////				it.putExtra(CadastroProvedorActivity.KEY_ISMOBILE, true);
////				it.putExtra("ProvedorCadastrado",  provedor);
////				it.addCategory(CATEGORIA);
////				startActivityForResult(it, TRANSMISSAO);
//
//
//            dialogNumeroParaCadastroProvedor();
//        }
    }


    private void dialogNumeroParaCadastroProvedor() {
        final EditText edtNumero = new EditText(getBaseContext());
        edtNumero.setInputType(InputType.TYPE_CLASS_NUMBER);


        edtNumero.setHint("informe o numero");

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.atencao);
        dialog.setMessage("Digite o numero da sua caixa de email, para cadastro no SAV");
        dialog.setView(edtNumero);
        dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //provedor = new ConfigProvedor(getBaseContext()).setProvedorTeste();

//                if (!provedor.getUsuarioPop().equals("")) {
//                    cadastroProvedor();
//                    dialog.dismiss();
//                }else {
                if (StringUtils.editTextIsEmpty(edtNumero)) {
                    //edtNumero.setError("Informe o numero");
                    cadastrarProvedor();
                    return;
                }
                provedor = new ConfigProvedor(getBaseContext()).setProvedor(Integer.parseInt(edtNumero.getText().toString().trim()));
                cadastroProvedor();
                dialog.dismiss();

                // }

            }
        });
        dialog.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void cadastroProvedor() {
        Intent it = new Intent("Provedor");
        it.putExtra(CadastroProvedorActivity.KEY_ISMOBILE, true);
        it.putExtra("ProvedorCadastrado", provedor);
        it.addCategory(CATEGORIA);
        startActivityForResult(it, TRANSMISSAO);
    }

    private void cadastrarProvedor() {
        Intent it = new Intent("Provedor");
        it.putExtra(CadastroProvedorActivity.KEY_ISMOBILE, true);
        it.addCategory(CATEGORIA);
        startActivityForResult(it, TRANSMISSAO);
    }

    private void iniciarMenuPrincipal() {
        startActivityForResult(new Intent(getBaseContext(), MenuPrincipal.class), 3);
    }


    private void showDialogSenhaTrabalharSemConexao(final Parametro parametro) {
        final EditText edtSenha = new EditText(this);
        edtSenha.setInputType(InputType.TYPE_CLASS_NUMBER);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setView(edtSenha)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.trabalhar_sem_conexao)
                .setMessage(String.format(getString(R.string.faz_um_dia_que_voce_nao_recebe_dados), parametro.getIdVendedor()))
                .setCancelable(false)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            if (Utils.Senha.senhaDigitadaEstaCorreta(Utils.Senha.TRABALHAR_SEM_CONEXAO, parametro.getIdVendedor(), 0, Utils.getText(edtSenha))) {
                                iniciarMenuPrincipal();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(getBaseContext(), R.string.senha_invalida, Toast.LENGTH_LONG).show();
                                showDialogSenhaTrabalharSemConexao(parametro);
                            }
                        } catch (Exception e) {
                            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private Boolean validarSenha(String senhaUsuario, String senhaDigitada) {
        String senhaAux = "";

        byte[] bytes = senhaDigitada.getBytes();
        for (byte b : bytes) {
            senhaAux += String.format("%03d", (int) b);
        }
        return senhaUsuario.equals(senhaAux);
    }

    public void criarUsuarioPadrao() {
        usuario = new Usuario();

        usuario.setIDUsuario((short) 1);
        usuario.setIDVendedor((short) 0);
        usuario.setNome("CERPRO");

        String senhaAux = "";
        byte[] bytes = "123".getBytes();

        for (byte b : bytes)
            senhaAux += String.format("%03d", (int) b);

        usuario.setSenha(senhaAux);

        new UsuarioDAO(getBaseContext(), null).insert(usuario);
    }

    public void _startActivity(String intent, String category) {
        Intent it = new Intent(intent);
        it.addCategory(category);
        startActivity(it);
    }

    private boolean existeProvedor() {
        return new ProvedorDAO(getBaseContext()).existeProvedor();
    }

    public void installShortCut() {
        SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean isAppInstalled = appPreferences.getBoolean("isAppInstalled", false);

        if (isAppInstalled == false) {

            Intent shortcutIntent = new Intent(getApplicationContext(), Principal.class);
            shortcutIntent.setAction(Intent.ACTION_MAIN);
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Atacado Maringa");
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.icon));
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

            getApplicationContext().sendBroadcast(intent);
            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }
    }
}
