package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.adapter.AddressAdapter;
import br.net.sav.service.EnderecoApiService;
import br.net.sav.util.CepUtil;
import br.net.sav.util.modelo.Address;

public class ProcuraCepActivity extends Activity implements AdapterView.OnItemClickListener {

    private Spinner spinnerStates;
    private ListView listViewAddress;
    private List<Address> addresses;
    private CepUtil util;
    protected static String ENDERECO_INTEIRO = "endereco";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procura_cep);

        addresses = new ArrayList<>();
        listViewAddress = (ListView) findViewById(R.id.lv_activity_procura_cep_enderecos);
        AddressAdapter adapter = new AddressAdapter(this, addresses);
        listViewAddress.setAdapter(adapter);
        listViewAddress.setOnItemClickListener(this);

        spinnerStates = (Spinner) findViewById(R.id.spn_activity_procura_cep);
        spinnerStates.setAdapter(ArrayAdapter.createFromResource(this, R.array.states, android.R.layout.simple_spinner_item));
        spinnerStates.setSelection(16);

        util = new CepUtil(this,
                R.id.edt_activity_procura_cep_rua,
                R.id.edt_activity_procura_cep_cidade,
                R.id.spn_activity_procura_cep);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Address address = addresses.get(i);

        Intent intent = new Intent();
        intent.putExtra(ENDERECO_INTEIRO, address);

        setResult(RESULT_OK, intent);
        finish();
    }

    private String getState() {
        String[] stateArray = ((String) spinnerStates.getSelectedItem()).split("\\(");

        if (stateArray.length == 2) {
            stateArray = stateArray[1].split("\\)");
            return stateArray[0];
        }
        return "";
    }

    private String getCity() {
        return ((EditText) findViewById(R.id.edt_activity_procura_cep_cidade)).getText().toString();
    }

    private String getStreet() {
        return ((EditText) findViewById(R.id.edt_activity_procura_cep_rua)).getText().toString();
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void updateListView() {
        ((AddressAdapter) listViewAddress.getAdapter()).notifyDataSetChanged();
    }

    public void searchAddress(View view) {
        util.lockFields(true);
        new EnderecoApiService().getEnderecosPorCidadeEstadoRua(getCity(), getState(), getStreet(), new EnderecoApiService.EnderecoApiCallback<List<Address>>() {
            @Override
            public void quandoSucesso(List<Address> enderecos) {
                addresses.clear();
                addresses.addAll(enderecos);
                updateListView();
                util.lockFields(false);
            }

            @Override
            public void quandoErro(String mensagemErro) {
                Toast.makeText(getBaseContext(), mensagemErro, Toast.LENGTH_LONG).show();
                util.lockFields(false);
            }
        });
    }

}
