package br.net.sav.atacadomaringa;

import java.text.ParseException;
import java.util.List;

import android.app.Dialog;
import android.app.ListActivity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.modelo.ProdutoSemVenda;

public class ProdutoSemVendaActivity extends ListActivity {
	public static final String ACTION = "ProdutoSemVenda" ;
	private ProdutoSemVendaAdapter adapter ;
	private EditText edtQtdeDias;
	private TextView txvDialog ;
	private Button btnPesquisar ;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		showCustomDialog();				
	}
	
	@Override
	protected void onResume() {	
		super.onResume();
		
		ProdutoDAO produtoDAO = new ProdutoDAO(getBaseContext(), null);
		List<ProdutoSemVenda> listaSemVenda = null ;		
		
		try {
			if(edtQtdeDias.getText().toString().trim().length() !=0)
				listaSemVenda = produtoDAO.listaSemVenda(Integer.parseInt(edtQtdeDias.getText().toString().trim()));
			else 
				listaSemVenda = produtoDAO.listaSemVenda(0);
		} catch (ParseException e) {
			Log.d("ProdutoSemVendaActivity",e.getMessage());
		}
		
		if(listaSemVenda !=null) {
			if(adapter == null) {
				setContentView(R.layout.produtosemvenda);
				adapter = new ProdutoSemVendaAdapter(getBaseContext(), listaSemVenda);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);			
			} else {
				adapter.changeData(listaSemVenda);				
			}
		}
		
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if(title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			setTitle("Total de produtos sem venda: " + listaSemVenda.size());
		}					
	}
	
	public void showCustomDialog() {
		final Dialog dialog = new Dialog(this);			
		
		dialog.setContentView(R.layout.popupprodutosemvenda);
		dialog.setTitle("Digite a quantidade de dias");
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		txvDialog   = (TextView)dialog.findViewById(R.idpopupProdutoSemVenda.txvSemVenda);	
		edtQtdeDias = (EditText)dialog.findViewById(R.idpopupProdutoSemVenda.txtSemVenda);
		btnPesquisar = (Button)dialog.findViewById(R.idpopupProdutoSemVenda.btnPesquisar);		
		txvDialog.setText("Sem venda a:") ;
		dialog.show();
		btnPesquisar.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {								
				onResume();				
				dialog.dismiss();				
			}
		});								
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.produtosemvenda_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {	
		switch (item.getItemId()) {
		case R.idMenuProdutoSemVenda.filtro:
			showCustomDialog() ;
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}