package br.net.sav.atacadomaringa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.modelo.ProdutoSemVenda;

public class ProdutoSemVendaAdapter extends BaseAdapter {
	private List<ProdutoSemVenda> listaProdutos ;
	private Context ctx ;
		
	List<String> myElements = null ;
	HashMap<String, Integer> alphaIndexer ;
	String[] sections ;
	
	public ProdutoSemVendaAdapter(Context ctx, List<ProdutoSemVenda> lista) {
		this.listaProdutos = lista ;
		this.ctx = ctx ;
		
		myElements = new ArrayList<String>(lista.size());
		
		for(int i=0; i< lista.size() ; i++ ) {
			myElements.add(lista.get(i).getDescricao()); 
		}
		
		alphaIndexer = new HashMap<String, Integer>();
		
		int size = lista.size() ;
		
		for(int i = size - 1; i>= 0 ; i-- ) {
			String element = myElements.get(i);
			alphaIndexer.put(element.substring(0,1), i);
		}
		
		Set<String> keys = alphaIndexer.keySet();
		
		Iterator<String> it = keys.iterator() ;
		ArrayList<String> keyList =  new ArrayList<String>();
		
		while(it.hasNext()) {
			String key = it.next();
			keyList.add(key);
		}
		
		Collections.sort(keyList);
		
		sections = new String[keyList.size()];
		
		keyList.toArray(sections);		
	}
	
	public void changeData(List<ProdutoSemVenda> lista) {
		this.listaProdutos = lista ;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return listaProdutos.size();
	}

	@Override
	public Object getItem(int position) {
		return listaProdutos.get(position);
	}

	@Override
	public long getItemId(int position) {		
		return position ;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.produtosemvenda, null) ;
		}
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		ProdutoSemVenda produto = listaProdutos.get(position) ;
		TextView txtDescricao = (TextView) view.findViewById(R.idProdutoSemVenda.txtDescricao);
		txtDescricao.setText(produto.getDescricao());
		txtDescricao.setTextColor(Color.GREEN);			
		
		TextView txtEmbalagem = (TextView) view.findViewById(R.idProdutoSemVenda.txtEmbalagem);
		txtEmbalagem.setText("Embalagem: "+produto.getDescricaoEmbalagem());
		
		TextView txtNDias = (TextView) view.findViewById(R.idProdutoSemVenda.txtNDias);
		TextView txtUltimaVenda = (TextView) view.findViewById(R.idProdutoSemVenda.txtUltimaVenda);
	
		if(produto.getQtdeDias() != -1) {
			txtNDias.setText(String.format("Quantidade de Dias: %d", produto.getQtdeDias()));
			txtUltimaVenda.setText(ctx.getString(R.string.ultima_venda) +produto.getUltimaVenda());
		} else {
			txtNDias.setText("Quantidade de dias: 0");
			txtUltimaVenda.setText(R.string.ultima_venda_nunca);
		}													
		return view;
	}
}
