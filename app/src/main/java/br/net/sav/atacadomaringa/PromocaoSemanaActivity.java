package br.net.sav.atacadomaringa;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import br.net.sav.dao.SituacaoEspecialDAO;
import br.net.sav.modelo.PromocaoSemana;

public class PromocaoSemanaActivity extends ListActivity {
	public static final String ACTION = "PromocaoSemana" ;
	private PromocaoSemanaAdapter adapter ;
	private List<PromocaoSemana> listaPromocao ;
	private List<PromocaoSemana> listaFornecedor ;
	
	private String[] fornecedor ;
	private long[]    idFornecedor ;
	private Spinner spnFornecedor ;
	private int idLista = 0 ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listaFornecedor = new SituacaoEspecialDAO(getBaseContext(), null).listaFornecedores();
		
		fornecedor   = new String [listaFornecedor.size()];
		idFornecedor = new long   [listaFornecedor.size()];
		
		if(listaFornecedor.size() > 0) {
			for(int i = 0; i < listaFornecedor.size() ; i++) {			
				fornecedor[i]   = listaFornecedor.get(i).getDescricao();
				idFornecedor[i] = listaFornecedor.get(i).getIDFornecedor();			
			}
		}	
		
		SituacaoEspecialDAO situacaoDAO = new SituacaoEspecialDAO(getBaseContext(), null);
		listaPromocao = situacaoDAO.promocaoSemana();
		
		if(listaPromocao.size() > 0 )
			showCustomDialog() ;
	}	
	
	@Override
	protected void onResume() {	
		super.onResume();				
		
		if(idLista > 0) {
			listaPromocao = new SituacaoEspecialDAO(getBaseContext(), null).promocaoSemana(idFornecedor[idLista]);
		} 
		
		if(listaPromocao != null) {
			if(adapter == null) {
				setContentView(R.layout.promocaosemana);
				
				adapter = new PromocaoSemanaAdapter(getBaseContext(), listaPromocao);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(listaPromocao);
			}
		}
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if(title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			setTitle(getString(R.string.total_de_promocoes_da_semana) + listaPromocao.size());
		}
				
	}
	
	public void showCustomDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Selecione o fornecedor:");
		builder.setItems(fornecedor, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {		    	
		    	idLista = item ;
		    	dialog.dismiss();
		    	onResume();
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public void loadSpinner() {
		String[] promocao = new String[listaPromocao.size()];
		
		for(int i = 0 ; i< listaPromocao.size() ; i++) {
			PromocaoSemana p = listaPromocao.get(i);
			promocao[i] = p.getDescricao();			
		}		
		ArrayAdapter<String> adapterTipo = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line,promocao);				
		if(listaPromocao.size() > 0)
			spnFornecedor.setAdapter(adapterTipo);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {	
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.promocaosemana_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuPromocaoSemana.filtro:
			if(listaPromocao.size() > 0)
				showCustomDialog();
			else
				Toast.makeText(getBaseContext(), R.string.nao_existem_forncedores_para_realizar_a_busca, Toast.LENGTH_LONG).show();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}