package br.net.sav.atacadomaringa;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.modelo.PromocaoSemana;

public class PromocaoSemanaAdapter extends BaseAdapter {
	private List<PromocaoSemana> listaPromocao ;
	private Context ctx ;
		
	public PromocaoSemanaAdapter(Context ctx, List<PromocaoSemana> lista) {
		this.ctx = ctx ;
		this.listaPromocao = lista ;
	}
	
	public void changeData(List<PromocaoSemana> lista) {
		this.listaPromocao = lista ;
		notifyDataSetChanged();	
	}
	
	@Override
	public int getCount() {	
		return listaPromocao.size();
	}

	@Override
	public Object getItem(int position) {
		return listaPromocao.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position ;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.promocaosemana, null);
		}
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		PromocaoSemana promocao = listaPromocao.get(position);
		TextView txtDescricao = (TextView)view.findViewById(R.idPromocaoSemana.txtDescricao);
		txtDescricao.setText(promocao.getIDProduto() + " - " + promocao.getDescricao()) ;
		
		TextView txtMultiplo = (TextView)view.findViewById(R.idPromocaoSemana.txtMultiplo);		
		txtMultiplo.setText(String.format(String.valueOf(R.string.multiplo), promocao.getMultiplo())) ;
		
		TextView txtPercDesconto = (TextView)view.findViewById(R.idPromocaoSemana.txtPercDesconto);
		txtPercDesconto.setText(String.format("Percentual de Desconto: ", promocao.getDesconto()));
		return view;
		
	}	
}
