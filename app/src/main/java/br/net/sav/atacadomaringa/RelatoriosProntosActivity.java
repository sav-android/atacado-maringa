package br.net.sav.atacadomaringa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class RelatoriosProntosActivity extends Activity {
	public static final String ACTION = "RelatoriosProntos" ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.relatorio);
		
		TextView txvTexto = (TextView) findViewById(R.idRelatorio.txvConteudo);
		File file = new File(getIntent().getStringExtra("caminhoArquivo"));
		StringBuilder text = new StringBuilder() ;
		
		try {
			BufferedReader bf = new BufferedReader(new FileReader(file));
			String line ;
			
			while((line = bf.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
		} catch (Exception e) {
			Log.d(getString(R.string.relatoriosprontosactivity), e.getMessage());
		}
		txvTexto.setText(text);
	}
}
