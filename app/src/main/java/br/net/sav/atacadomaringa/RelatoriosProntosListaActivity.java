package br.net.sav.atacadomaringa;

import java.io.File;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RelatoriosProntosListaActivity extends ListActivity {
	public static final String ACTION = "RelatoriosProntosLista" ;
	@Override
	protected void onResume() {			
		super.onResume();
		
		final String caminhoArquivos = Environment.getExternalStorageDirectory().getPath() + "/sav/resumo" ;
		File dir = new File(caminhoArquivos);
		if(!dir.exists())
			return ;
		
		setListAdapter(new ArrayAdapter<String>(getBaseContext(),R.layout.listarelatorio, dir.list()));
		ListView lv = getListView();
		lv.setOnItemClickListener(new OnItemClickListener() {			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				showDialogOpcoes(caminhoArquivos + "/" + ((TextView) view).getText()) ;
			}			
		});					
	}
	
	public void showDialogOpcoes(String caminhoArquivo) {
		final CharSequence[] itens = {getString(R.string.visualizar_relatorio), getString(R.string.apagar_relatorio)} ;
		final String caminho = caminhoArquivo ;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(getString(R.string.selecione_opcao_desejada));
		builder.setItems(itens, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0 :
					Intent intent = new Intent(RelatoriosProntosActivity.ACTION);
					intent.putExtra("caminhoArquivo", caminho);
					intent.addCategory(Principal.CATEGORIA);
					startActivity(intent);					
					break;

				case 1 :
					apagarArquivo(caminho);
					break ;
					
				default:
					break;
				}
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();		
	}
	
	public void apagarArquivo(String caminhoArquivo) {
		final String caminho = caminhoArquivo ;
		AlertDialog.Builder msg = new AlertDialog.Builder(this);
		
		msg.setMessage(R.string.deseja_realmente_apagar_o_relatorio_selecionado);
		msg.setTitle(getString(R.string.atencao));
		msg.setIcon(android.R.drawable.ic_dialog_alert);
		msg.setPositiveButton("Sim", new DialogInterface.OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int id) {
				File arquivo = new File(caminho);
				if(arquivo.delete())
					onResume() ;
				else 
					Toast.makeText(getBaseContext(), R.string.nao_foi_possivel_apagar_o_arquivo_selecionado, Toast.LENGTH_LONG).show();
			}
		});
		msg.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = msg.create();
		alert.show();
	}

}
