package br.net.sav.atacadomaringa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import br.net.sav.modelo.Pedido;
import br.net.sav.presenter.TransmissaoDadosPresente;
import br.net.sav.utils.PermissionUtils;

import static br.net.sav.service.VerificaAtualizacao.REQUEST_CODE_WRITE_SETTINGS;
import static br.net.sav.service.VerificaAtualizacao.getRequestIniciarAtualizacao;

@SuppressLint("SimpleDateFormat")
public class TransmissaoActivity extends Activity implements Runnable, TransmissaoDadosPresente.IAoTransmitirDadosPresente {
    public static final int GERANDO_DADOS = 1;
    public static final int COMPACTANDO = 2;
    public static final int ENVIANDO = 3;
    public static final int RECEBENDO = 4;
    public static final int DESCOMPACTANDO = 5;
    public static final int IMPORTANDO = 6;
    public static final int MENSAGEM = 7;
    public static final int ATUALIZANDO_VERSAO = 8;
    public static final int ENVIANDO_EMAILS_TEXTUAIS = 9;
    public static final int VERIFICANDO_FOTOS_PRODUTO = 10;
    public static final int ENVIANDO_DADOS_PEDIDOS_GWEB = 11;
    public static final int ATUALIZA_COMBOS_CM = 12;
    public static final int IMPORTACAO = 2;
    public static final int ATUALIZAR_LISTA_STATUS = 13;
    public static final int ENVIANDO_DADOS_CLIENTES_GWEB = 14;
    public static boolean verificaAtualizacaoDados;

    private TextView mTxvStatus = null;
    private TextView mTxvListaTarefas = null;
    private ProgressDialog mProgressDialog;
    private StringBuilder listaDeStatus;
    private Button btnTransmitir;
    private TransmissaoDadosPresente presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transmissao);

        presenter = new TransmissaoDadosPresente(this,this);

        presenter.getPopularDados();
        PermissionUtils.permissionSLL(this);

        inicializarComponentes();

        if (getValidarConexao()) return;

        presenter.getVerificaAtualizacaoDeVersao();

        getBtnOnClickTransmitir();
    }



    private void getBtnOnClickTransmitir() {
        btnTransmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnTransmitir.setEnabled(false);

                getListaDeArquivosLog();

                if (presenter.isParametroDiferenteNull()) {
                    presenter.getVerificarVendedorAPI();
                }

                if (presenter.isExisteVersao()) {
                    presenter.getAtualizarVersao();
                } else {
                    if (presenter.getValidarVendedorDesativado()) return;

                    getExistePedidoNaoEnviados();
                }

            }
        });
    }




    private void getExistePedidoNaoEnviados() {
        List<Pedido> pedidos = presenter.getListaPedidosNaoEnviadoEAbertos();
        if (!pedidos.isEmpty()) {
            presenter.getmDadosDeTransmissao().setQuantidadeDePedidosParaSerEnviado(pedidos.size());
            startActivityForResult(new Intent(TransmissaoActivity.this, PedidoFechadoListaActivity.class), 3);
        } else {
            transmitir();

        }
    }


    private void getListaDeArquivosLog() {
        String rootPath = Environment.getExternalStorageDirectory().getPath();
        String retorno = rootPath + "/sav/log/log.txt";
        File file = new File(retorno);
        listaDeStatus = new StringBuilder();
        if (listaDeStatus.length() < 0){
            mTxvListaTarefas.setVisibility(View.GONE);
        } else {
            mTxvListaTarefas.setVisibility(View.VISIBLE);
        }
        mTxvListaTarefas.setText(listaDeStatus);
    }


    private void inicializarComponentes() {
        mTxvStatus = (TextView) findViewById(R.idTransmissao.txvStatus);
        mTxvListaTarefas = (TextView) findViewById(R.idTransmissao.txvListaTarefas);
        btnTransmitir = (Button) findViewById(R.idTransmissao.btnTransmitir);
        if (getIntent().getExtras()!= null){
            Boolean isSincronizar = getIntent().getExtras().getBoolean("SINCRONIZACAO");
            if (isSincronizar){
                btnTransmitir.setText(R.string.sincronizacao);
            }
        }
    }



    private boolean getValidarConexao() {
        ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi != null && wifi.isConnectedOrConnecting()) {
            mTxvStatus.setText("Status: Conectado via Wifi");
        } else if (mobile != null && mobile.isConnectedOrConnecting()) {
            mTxvStatus.setText("Status: Conectado via 3G");
        } else {
            mTxvStatus.setText("Status: Desconectado");
            Toast.makeText(getBaseContext(), R.string.nao_existe_conexao_com_a_internet_disponivel, Toast.LENGTH_LONG).show();
            btnTransmitir.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_arredondado_close));
            return true;
        }
        return false;
    }

    private void transmitir() {
        presenter.getIniciarDados();

//        if (presenter.getmDadosDeTransmissao().isProvedorNull()) {
//            Toast.makeText(getBaseContext(), R.string.nao_existe_provedor_disponivel_para_realizar_a_transmissao, Toast.LENGTH_LONG).show();
//            return;
//        }

        try {
           // presenter.getmDadosDeTransmissao().setQtdeEmailRecebidos();
            mProgressDialog = new ProgressDialog(TransmissaoActivity.this, (Build.VERSION.SDK_INT >= 28) ? R.style.DialogStyle : 0);
            mProgressDialog.setTitle(getString(R.string.transmissao_de_dados));
            mProgressDialog.setIcon(getResources().getDrawable(R.drawable.icon));
            mProgressDialog.setMessage("Por favor aguarde enquanto o sistema sincroniza os dados com o servidor.");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            new Thread(TransmissaoActivity.this).start();

        } catch (Exception e) {
            mTxvStatus.setText("Falha no processo.");
            Log.d("Transmissao", e.getMessage());
        }
    }



    @Override
    public void run() {
        presenter.atualizaListaStatus("Iniciando Processo de Transmissao");
        try {
            Looper.prepare();

            presenter.getStartTransmissao();
            //if (Utils.existeArquivosParaImportar())
            startActivity(new Intent(getBaseContext(), ImportacaoActivity.class).addCategory(Principal.CATEGORIA));
            presenter.atualizaListaStatus("Processo de Transmissao Finalizado");
        } catch (Exception e) {
            presenter.setSucessoFalso();
            this.finish();
            e.printStackTrace();
            presenter.atualizaListaStatus("Processo de Transmissao Erro: " + e.getMessage());
            return;
        } finally {
            mProgressDialog.dismiss();
            handler.sendEmptyMessage(0);
            TransmissaoActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnTransmitir.setEnabled(true);
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        //startActivity(new Intent(getBaseContext(), ImportacaoActivity.class).addCategory(Principal.CATEGORIA));
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//            handler.sendEmptyMessage(0);
//        }
    }


    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @SuppressLint("StringFormatMatches")
        @Override
        public void handleMessage(android.os.Message msg) {
            if (mProgressDialog != null) {
                switch (msg.what) {
                    case 0:
                        presenter.getStatusFalhaOuSucessoView();

                        break;
                    case GERANDO_DADOS:
                        mProgressDialog.setMessage("Gerando dados para enviar");


                        break;
                    case COMPACTANDO:
                        mProgressDialog.setMessage("Compactando dados para enviar");

                        break;
                    case ENVIANDO:
                        mProgressDialog.setMessage("Enviando dados para o servidor");

                        break;
                    case RECEBENDO:
                        mProgressDialog.setMessage("Recebendo dados do servidor");

                        break;
                    case DESCOMPACTANDO:
                        mProgressDialog.setMessage("Descompactando dados recebidos para importar");

                        break;
                    case IMPORTANDO:
                        mProgressDialog.setMessage("Importando arquivos recebidos, aguarde...");

                        break;
                    case MENSAGEM:
                        Bundle b = msg.getData();
                        mProgressDialog.setMessage(b.getString("mensagem"));

                        break;
                    case ATUALIZANDO_VERSAO:
                        mProgressDialog.setMessage(getString(R.string.atualizando_versao_do_sav_aguarde));

                        break;
                    case ENVIANDO_EMAILS_TEXTUAIS:
                        mProgressDialog.setMessage("Enviando mensagens salvas");

                        break;
                    case VERIFICANDO_FOTOS_PRODUTO:
                        mProgressDialog.setMessage("Atualizando Fotos Produtos");

                        break;
                    case ENVIANDO_DADOS_PEDIDOS_GWEB:
                        mProgressDialog.setMessage("Enviando Pedidos para API Central Manager");

                        break;
                    case ATUALIZA_COMBOS_CM:
                        mProgressDialog.setMessage("Atualizando Combo Central Manager");

                        break;
                    case ATUALIZAR_LISTA_STATUS:
                        Bundle bundle = msg.getData();
                        listaDeStatus.append(bundle.getString("mensagem"));
                        mTxvListaTarefas.setText(listaDeStatus);

                        break;
                    case ENVIANDO_DADOS_CLIENTES_GWEB:
                        mProgressDialog.setMessage("Enviando clientes para API Central Manager");

                        break;
                }
            }
        }
    };


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == IMPORTACAO && resultCode == RESULT_OK) {
//             presenter.getmDadosDeTransmissao().descompactarDados();
//        }
        if (requestCode == 3 && resultCode == RESULT_OK)
            transmitir();
        if (resultCode == 10) {
            presenter.updateVersion();
        }
        if (requestCode == REQUEST_CODE_WRITE_SETTINGS) {
            getRequestIniciarAtualizacao(TransmissaoActivity.this);
        }

    }


    @Override
    public Handler getHandler() {
        return handler;
    }

    @Override
    public ProgressDialog getProgresDialog() {
        return mProgressDialog;
    }

    @Override
    public TextView getTextViewStatus() {
        return mTxvStatus;
    }

    @Override
    public void atualizarProgresso(String mensagem) { }
}
