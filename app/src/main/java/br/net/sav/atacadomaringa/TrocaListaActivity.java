package br.net.sav.atacadomaringa;

import java.text.NumberFormat;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TrocaDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Troca;

public class TrocaListaActivity extends ListActivity {
	public static final String ACTION = "TrocaLista" ;
	private final CharSequence[] opcoesFiltrar = {"Todas", getString(R.string.apenas_nao_enviadas), "Apenas Enviadas"};
	private ArrayList<Troca> listaTrocas;
	private Parametro parametro;
	private Intent it;
	private int ultimaOpcao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		parametro = new ParametroDAO(getBaseContext(), null).get();
		
		it = getIntent() ;
		filtrarPedidos(ultimaOpcao);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		Troca troca = listaTrocas.get(info.position);
		
		menu.setHeaderTitle(R.string.escolha_uma_opcao);
		menu.add(0, 0, 0, "Ver Itens da Troca");
		menu.add(1, 1, 1, "Alterar Troca");
		menu.add(2, 2, 2, "Excluir Troca");
		
		menu.setGroupVisible(1, !troca.isEnviado());
		menu.setGroupVisible(2, !troca.isEnviado());
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		registerForContextMenu(l);
		v.showContextMenu();
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		final Troca troca = listaTrocas.get(info.position);
		Intent it;
		
		switch (item.getItemId()) {
		case 0: //Ver Itens da Troca
			it = new Intent("ListaItensTroca");
			it.addCategory("SUPREMACIA");
			it.putExtra("IdTroca", troca.getIdTroca());
			startActivity(it);
			
			break;
		case 1: //Alterar Troca
			it = new Intent(getBaseContext(), TrocaTabHostActivity.class);
			it.putExtra("IdCliente", troca.getIdCliente());
			it.putExtra("IdTroca", troca.getIdTroca());
			startActivity(it);

			break;
		case 2: //Excluir Troca
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle(R.string.atencao);
			dialog.setIcon(android.R.drawable.ic_dialog_alert);
			dialog.setMessage(getString(R.string.deseja_realmente_excluir_a_troca_nr) + troca.getIdTroca() + "?");
			dialog.setButton("Sim", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					if (new TrocaDAO(getBaseContext()).excluirTroca(troca)) {
						Toast.makeText(getBaseContext(), R.string.troca_excluida_com_sucesso, Toast.LENGTH_SHORT).show();
					
						try {
							filtrarPedidos(ultimaOpcao);
						} catch (Exception e) {
							Toast.makeText(getBaseContext(), "Erro:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
						}
					}
					else
						Toast.makeText(getBaseContext(), "Erro ao tentar excluir a troca!", Toast.LENGTH_LONG).show();
				}
			});
			dialog.setButton2(getString(R.string.nao), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					return;
				}
			});
			dialog.setCancelable(true);
			dialog.show();
			
			break;
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.consultatroca_menu, menu);		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.idMenuConsultaTroca.quais_mostrar:
			showCustomDialogQuaisMostrar();
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void showCustomDialogQuaisMostrar() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.escolha_uma_opcao);
		builder.setItems(opcoesFiltrar, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	filtrarPedidos(item);
		    	dialog.dismiss();
		    }
		});
		AlertDialog alert = builder.create();
		alert.show();
	}	
	
	private void filtrarPedidos(int opcao) {
		TrocaDAO trocaDAO = new TrocaDAO(getBaseContext());
		ultimaOpcao = opcao;
		long idCliente = it.getLongExtra("IdCliente", 0);
		
		try {
			switch (opcao) {
			case 0: //TODAS
				listaTrocas = trocaDAO.listaTrocas(trocaDAO.TODAS, idCliente);
				break;
			case 1: //APENAS N�O ENVIADAS
				listaTrocas = trocaDAO.listaTrocas(trocaDAO.NAO_ENVIADAS, idCliente);
				break;
			case 2: //APENAS ENVIADAS
				listaTrocas = trocaDAO.listaTrocas(trocaDAO.ENVIADAS, idCliente);
				break;
			}
			
			setarTitulo();
		}
		catch (Exception e) {
			Toast.makeText(getBaseContext(), "Erro ao filtrar trocas:\n" + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	private void setarTitulo() {
		//TOTAL GERAL
		double dblTotalBruto = 0.0, dblTotalLiquido = 0.0;
		for (Troca t : listaTrocas) {
			dblTotalBruto += t.getValorMercadoriaBruto();
			dblTotalLiquido += t.getValorLiquidoTroca();
		}
		
		// SALDO FLEX
		String saldoMostrar = "";
		
		saldoMostrar = NumberFormat.getCurrencyInstance().format(parametro.getSaldoVerba());
				
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		setListAdapter(new TrocaListaAdapter(getBaseContext(), listaTrocas));
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		title.setSingleLine(false);
		title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
		title.setTextSize(16);
		setTitle(String.format(getString(R.string.nr_de_trocas_total_bruto_total_liquido_saldo_verba), listaTrocas.size(), nf.format(dblTotalBruto), nf.format(dblTotalLiquido), saldoMostrar));
	}
}
