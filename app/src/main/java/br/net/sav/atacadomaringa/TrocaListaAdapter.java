package br.net.sav.atacadomaringa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import br.net.sav.dao.ClienteDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Troca;

public class TrocaListaAdapter extends BaseAdapter {
	private Context ctx;
	private ArrayList<Troca> lista;
	private ClienteDAO clienteDAO;
	private SimpleDateFormat sdfData, sdfDataHora;
	private NumberFormat nf;
	
	public TrocaListaAdapter(Context ctx, ArrayList<Troca> lista) {
		this.ctx = ctx;
		this.lista = lista;
		this.sdfData = new SimpleDateFormat("dd/MM/yyyy");
		this.sdfDataHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.nf = NumberFormat.getCurrencyInstance();
		this.clienteDAO = new ClienteDAO(ctx, null);
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if (view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.consultatroca, null);
		}
		
		if (position % 2 == 0){
		    view.setBackgroundResource(R.drawable.alterselector1);
		} else {
		    view.setBackgroundResource(R.drawable.alterselector2);
		}
		
		Troca troca = lista.get(position);
		Cliente cliente = null;
		try {
			cliente = clienteDAO.get(troca.getIdCliente());
		} catch (Exception e) {
			Log.d("PedidoListaAdapter",e.getMessage());
		}
		
		TextView txvPedido = (TextView) view.findViewById(R.idConsultaTroca.txvTroca);
		txvPedido.setText(ctx.getString(R.string.troca_nr) + String.valueOf(troca.getIdTroca()));
		txvPedido.setTextColor(Color.GREEN);
		txvPedido.setTypeface(Typeface.DEFAULT_BOLD);
		txvPedido.setTextSize(18);
		
		TextView txvCliente = (TextView) view.findViewById(R.idConsultaTroca.txvCliente);
		if (cliente == null)
			txvCliente.setText(R.string.cliente_nao_encontrado);
		else
			txvCliente.setText(String.format("%d - %s", cliente.getIdCliente(), cliente.getRazao()));
		txvCliente.setTextSize(18);
		
		TextView txvDataPedido = (TextView) view.findViewById(R.idConsultaTroca.txvData);
		txvDataPedido.setText("DataUltimaCompra: " + sdfData.format(troca.getData()));
		txvDataPedido.setTextSize(18);
		
		TextView txvValorBruto = (TextView) view.findViewById(R.idConsultaTroca.txvNfOrigem);
		txvValorBruto.setText(ctx.getString(R.string.nr_nf) + troca.getNfOrigem());
		txvValorBruto.setTextSize(18);
		
		TextView txvValorLiquido = (TextView) view.findViewById(R.idConsultaTroca.txvValorLiquido);
		txvValorLiquido.setText(R.string.valor_liq + nf.format(troca.getValorLiquidoTroca()));
		txvValorLiquido.setTextSize(18);
		
		TextView txvDataEnvio = (TextView) view.findViewById(R.idConsultaTroca.txvDataEnvio);
		if (troca.isEnviado())
			txvDataEnvio.setText("DataUltimaCompra Envio: " + sdfDataHora.format(troca.getDataEnvio()));
		else
			txvDataEnvio.setText(R.string.data_envio_nao_enviado);
		txvDataEnvio.setTextSize(18);
		
		return view;
	}

}
