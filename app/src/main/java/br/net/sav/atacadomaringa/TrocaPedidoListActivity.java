package br.net.sav.atacadomaringa;

import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.Pedido;

public class TrocaPedidoListActivity extends ListActivity {
	public static final String ACTION =  "TrocaPedidoLista" ;
	private TrocaPedidoListAdapter adapter ;
	private List<Pedido> lista ;
	
	private long idCliente ;	
	private long idPedido ;
	
	private Pedido pedido ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		
		Intent it = getIntent();
		idCliente = it.getLongExtra("IdCliente", 0);
	}
	@Override
	protected void onResume() {	
		super.onResume();
		
		PedidoDAO pedidoDAO = new PedidoDAO(getBaseContext(), null);
		
		lista = pedidoDAO.listaPedidos(pedidoDAO.NAO_ENVIADOS_E_ABERTOS, idCliente);
		
		if(lista != null) {
			if(adapter == null) {
				setContentView(R.layout.listatroca);
				adapter = new TrocaPedidoListAdapter(getBaseContext(), lista);
				setListAdapter(adapter) ;
				getListView().setFastScrollEnabled(true);
			} else {
				adapter.changeData(lista);
			}
		}		
		TextView title = (TextView) getWindow().findViewById(android.R.id.title);
		if(title != null) {
			title.setSingleLine(false);
			title.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
			title.setTextSize(14);
			setTitle(getString(R.string.pedidos_nao_transmitidos_do_cliente) + idCliente);
		}	
	}
	@Override
	protected void onPause() {
		super.onPause();		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);							
		pedido = lista.get(position);
		idPedido = pedido.getIDPedido();
		Intent it = new Intent() ;
		it.putExtra("IdPedido", idPedido);
		setResult(Activity.RESULT_OK, it);
		finish() ;
	}		
}