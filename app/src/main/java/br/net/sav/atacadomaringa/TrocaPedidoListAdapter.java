package br.net.sav.atacadomaringa;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.TrocaDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Pedido;

public class TrocaPedidoListAdapter extends BaseAdapter {
	private Context ctx ;
	private List<Pedido> lista ;
	private ClienteDAO clienteDAO;
	private TrocaDAO trocaDAO;
	
	public TrocaPedidoListAdapter(Context ctx, List<Pedido> lista) {
		this.lista = lista ;
		this.ctx = ctx ;
		this.clienteDAO = new ClienteDAO(ctx, null);
		this.trocaDAO = new TrocaDAO(ctx);
	}
	
	public void changeData(List<Pedido> lista) {
		this.lista = lista;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
			view = layout.inflate(R.layout.listatroca,null);
		}
		
		TextView txtIdPedido = (TextView)view.findViewById(R.idListaTroca.txtPedido);
		TextView txtCliente  = (TextView)view.findViewById(R.idListaTroca.txtCliente);
		TextView txtTemTroca = (TextView)view.findViewById(R.idListaTroca.txtTemTroca);

		Pedido pedido = lista.get(position);
		Cliente cliente = null;
		try {
			cliente = clienteDAO.get(pedido.getIDCliente());
		} catch (Exception e) {
			Log.d("TrocaPedidoListAdapter",e.getMessage());
		}
		
		txtIdPedido.setText(String.valueOf(R.string.pedido_nr + pedido.getIDPedido()));
		txtIdPedido.setTextSize(20);
		txtIdPedido.setTextColor(Color.GREEN);
		txtIdPedido.setTypeface(Typeface.DEFAULT_BOLD);
		
		txtCliente.setText(String.format("Cliente: %s", cliente.getRazao()));
		txtCliente.setTextSize(18);
		
		if(trocaDAO.existeByPedido(pedido.getIDPedido())) {
			txtTemTroca.setText(R.string.status_troca_ja_possui_troca);
			txtTemTroca.setTextColor(Color.RED);
			txtTemTroca.setTextSize(18);
		} else {
			txtTemTroca.setText(R.string.status_troca_nao_possui_troca);
			txtTemTroca.setTextColor(Color.GREEN);
			txtTemTroca.setTextSize(18);
		}
		
		return view;
	}
}