package br.net.sav.atacadomaringa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoTrocaDAO;
import br.net.sav.dao.TrocaDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TipoTroca;
import br.net.sav.modelo.Troca;

public class TrocaTabActivity extends Activity {
	public static final String ACTION = "TrocaTabActivity" ;
	private final String TROCA_SERIALIZAR = "troca.srl";
	
	private EditText edtObservacao;
	private EditText edtNrPedido;
	private EditText edtNrNf;
	private EditText edtCliente;
	private EditText edtValorMercadorias;
	private EditText edtValorLiquido;
	private EditText edtPercIndenizacao;
	private EditText edtData;
	private EditText edtNrTroca;
	private Spinner spnTipoTroca;
	private Spinner spnTabelaPreco;
	private Spinner spnCondicaoPagamento;
	private Button btnDadosCliente;
	private Button btnCancelar;
	
	private List<TipoTroca> lstTipoTroca;	
	private List<Tabela> lstTabelaPreco;
	private List<Condicao> lstCondicaoPagamento;
	private Troca troca;
	private Cliente cliente;
	private Parametro parametro;
	
	private long idTroca;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.troca);
		
		Intent intent = getIntent();
		
		try {
			cliente = new ClienteDAO(getBaseContext(), null).get(intent.getLongExtra("IdCliente", 0));
			if (cliente.getIdCliente() == 0) {
				Toast.makeText(getBaseContext(), String.format(getString(R.string.o_cliente_com_o_codigo_nao_foi_encontrado), intent.getLongExtra("IdCliente", 0)), Toast.LENGTH_LONG).show();
				finish();
			}
			
			parametro = new ParametroDAO(getBaseContext(), null).get();
			if (parametro == null) {
				Toast.makeText(getBaseContext(), R.string.os_parametros_de_funcionamento_nao_foram_encontrados, Toast.LENGTH_LONG).show();
				finish();
			}
			
			idTroca = intent.getLongExtra("IdTroca", 0);
			if (idTroca == 0) {
				troca = new Troca();
				troca.setIdTroca(new TrocaDAO(getBaseContext()).getLastId() + 1);
				troca.setIdCliente(cliente.getIdCliente());
				troca.setIndenizacao(70);
				troca.setNrPedidoVenda(0);
				troca.setObservacao("");
			} else {
				Object obj = Utils.recuperarObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR);
				if (obj != null) {
					troca = (Troca) obj;
				} else {
					troca = new TrocaDAO(getBaseContext()).get(idTroca);
					if (troca == null) {
						Toast.makeText(getBaseContext(), String.format(getString(R.string.a_troca_nr_nao_foi_encontrada_no_banco_de_dados_para_permitir_alterala), idTroca), Toast.LENGTH_LONG).show();
						finish();
					}
				}
			}
			
		} catch (Exception e) {
			Log.d(getString(R.string.trocatabactivity_oncreate), e.getMessage());
		}
		
		edtObservacao = (EditText) findViewById(R.idTroca.edtObservacao);
		edtNrPedido = (EditText) findViewById(R.idTroca.edtNrPedido);
		edtNrNf = (EditText) findViewById(R.idTroca.edtNrNota);
		edtCliente = (EditText) findViewById(R.idTroca.edtCliente);
		edtNrTroca = (EditText) findViewById(R.idTroca.edtNumeroTroca);
		edtValorMercadorias = (EditText) findViewById(R.idTroca.edtValorMercadorias);
		edtPercIndenizacao = (EditText) findViewById(R.idTroca.edtPercIndenizacao);
		edtValorLiquido = (EditText) findViewById(R.idTroca.edtValorIndenizar);
		edtData = (EditText) findViewById(R.idTroca.edtData);
		spnTipoTroca = (Spinner) findViewById(R.idTroca.spnTipoTroca);
		spnTabelaPreco = (Spinner) findViewById(R.idTroca.spnTabelaPreco);
		spnCondicaoPagamento = (Spinner) findViewById(R.idTroca.spnCondicaoPgto);
		btnDadosCliente = (Button) findViewById(R.idTroca.btnDadosClientes);
		btnCancelar = (Button) findViewById(R.idTroca.btnCancelar);
		
		edtCliente.setText(String.format("%d - %s", cliente.getIdCliente(), cliente.getRazao()));
		edtNrTroca.setText(String.valueOf(troca.getIdTroca()));
		
		btnDadosCliente.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				Intent it ;
				it = new Intent(getBaseContext(), ClienteTabHostActivity.class);  
				it.putExtra("IdCliente", cliente.getIdCliente());
				startActivity(it);
			}
		});
		
		btnCancelar.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				confirmaSaida();				
			}
		});
		
		edtNrPedido.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {								
				Intent it = new Intent("TrocaPedidoList");
				it.addCategory("SUPREMACIA");
				it.putExtra("IdCliente", cliente.getIdCliente());
				startActivityForResult(it, 10);
			}
		});		
		
		int padrao = 0;
		
		//Tipo de Troca
		lstTipoTroca = new TipoTrocaDAO(getBaseContext(), null).getAll();
		if (lstTipoTroca.size() == 0) {
			Toast.makeText(getBaseContext(), R.string.nao_existe_nenhum_tipo_de_troca_para_permitir_digitar_uma_troca, Toast.LENGTH_LONG).show();
			finish();
		}
		List<String> lstTipoTrocaAdapter = new ArrayList<String>(lstTipoTroca.size());
		for (TipoTroca tp : lstTipoTroca) {
			lstTipoTrocaAdapter.add(tp.getDescricao());
			if ((idTroca > 0 || troca.getTipoTroca() > 0) && tp.getIdTipoTroca() == troca.getTipoTroca()) {
				padrao = lstTipoTroca.indexOf(tp);
			}
		}
		spnTipoTroca.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstTipoTrocaAdapter));
		if (lstTipoTroca.size() > 0) {
			spnTipoTroca.setSelection(padrao);
			troca.setTipoTroca(lstTipoTroca.get(padrao).getIdTipoTroca());
		}
		spnTipoTroca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
				troca.setTipoTroca(lstTipoTroca.get(position).getIdTipoTroca());
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		
		padrao = 0;
		
		//Tabela de Pre�o
		lstTabelaPreco = new TabelaDAO(getBaseContext(), null).getListaCliente(cliente.getIdCliente());
		if (lstTabelaPreco.size() == 0) {
			Toast.makeText(getBaseContext(), R.string.nao_existe_nenhuma_tabela_de_preco_para_permitir_digitar_uma_troca, Toast.LENGTH_LONG).show();
			finish();
		}
		List<String> lstTabelaPrecoAdapter = new ArrayList<String>(lstTabelaPreco.size());
		for (Tabela tab : lstTabelaPreco) {
			lstTabelaPrecoAdapter.add(tab.getDescricao());
			if ((idTroca > 0 || troca.getIdTabela() > 0) && tab.getIdTabela() == troca.getIdTabela()) {
				padrao = lstTabelaPreco.indexOf(tab);
			}
		}
		spnTabelaPreco.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstTabelaPrecoAdapter));
		if (lstTabelaPreco.size() > 0) {
			spnTabelaPreco.setSelection(padrao);
			troca.setIdTabela(lstTabelaPreco.get(padrao).getIdTabela());
		}
		spnTabelaPreco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
				boolean bAux = true;
				if (troca.getNumeroItens() > 0 && troca.getIdTabela() != lstTabelaPreco.get(position).getIdTabela()) {
					bAux = false;
					final int positionAux = position;
					
					AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
					builder.setIcon(android.R.drawable.ic_dialog_alert)
						.setTitle(R.string.atencao)
						.setMessage(R.string.a_alteracao_de_tabela_de_preco_podera_alterar_os_precos)
						.setCancelable(false)
						.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								troca.setIdTabela(lstTabelaPreco.get(positionAux).getIdTabela());
								preencherSpnCondicao(troca.getIdTabela());

								dialog.dismiss();
							}
						})
						.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								int aux = 0;
								for (int i = 0; i < lstTabelaPreco.size(); i++) {
									if (lstTabelaPreco.get(i).getIdTabela() == troca.getIdTabela()) {
										aux = i;
										break;
									}
								}
								spnTabelaPreco.setSelection(aux);
								dialog.cancel();
							}
						});
					builder.show();
				}
				
				if (bAux) {
					troca.setIdTabela(lstTabelaPreco.get(position).getIdTabela());
					preencherSpnCondicao(troca.getIdTabela());
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		
		//Condi��o de Pagamento
		preencherSpnCondicao(troca.getIdTabela());
		
		spnCondicaoPagamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view,int position, long id) {
				troca.setIdCondicao(lstCondicaoPagamento.get(position).getIdCondicao());
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Object obj = Utils.recuperarObjetoSerializado(getBaseContext(), TROCA_SERIALIZAR);
		if (obj != null)
			troca = (Troca) obj;
		
		NumberFormat nfMoeda = NumberFormat.getCurrencyInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if (idTroca == 0)
			troca.setData(new Date());
		
		edtData.setText(sdf.format(troca.getData()));
		edtValorMercadorias.setText(nfMoeda.format(troca.getValorMercadoriaBruto()));
		edtPercIndenizacao.setText(String.format("%.2f", troca.getIndenizacao()).replace(",", "."));
		edtValorLiquido.setText(nfMoeda.format(troca.getValorLiquidoTroca()));
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		if (troca != null) {
			troca.setObservacao(edtObservacao.getText().toString());
			if (edtNrNf.getText().toString().trim().equals(""))
				troca.setNfOrigem(0);
			else
				troca.setNfOrigem(Long.parseLong(edtNrNf.getText().toString().trim()));
			
			String sPedidoVenda = edtNrPedido.getText().toString();
			if (sPedidoVenda.equals(""))
				troca.setNrPedidoVenda(0);
			else
				troca.setNrPedidoVenda(Long.parseLong(sPedidoVenda));
			
			Utils.serializarObjeto(getBaseContext(), TROCA_SERIALIZAR, troca);
		}
	}
	
	@Override
	public void onBackPressed() {
		confirmaSaida();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent it) {
		super.onActivityResult(requestCode, resultCode, it);
		
		if (requestCode == 10) {
			if (resultCode == Activity.RESULT_OK) {
				troca.setNrPedidoVenda(it.getLongExtra("IdPedido", 0));
				edtNrPedido.setText(String.valueOf(troca.getNrPedidoVenda()));
			}
		}
	}
	
	private void preencherSpnCondicao(short idTabelaPreco) {
		int padrao = -1;
		lstCondicaoPagamento = new CondicaoDAO(getBaseContext(), null).getByTabelaPreco(idTabelaPreco);
		List<String> lstCondicaoPagamentoAdapter = new ArrayList<String>(lstCondicaoPagamento.size());
		for (Condicao cpg : lstCondicaoPagamento) {
			lstCondicaoPagamentoAdapter.add(cpg.getDescricao());
			if (troca.getIdCondicao() == cpg.getIdCondicao())
				padrao = lstCondicaoPagamento.indexOf(cpg);
		}
		spnCondicaoPagamento.setAdapter(new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_dropdown_item_1line, lstCondicaoPagamentoAdapter));
		if (padrao > -1) {
			spnCondicaoPagamento.setSelection(padrao);
			troca.setIdCondicao(lstCondicaoPagamento.get(padrao).getIdCondicao());
		} else {
			if (lstCondicaoPagamento.size() > 0) {
				troca.setIdCondicao(lstCondicaoPagamento.get(0).getIdCondicao());
				spnCondicaoPagamento.setSelection(0);
			} else {
				troca.setIdCondicao((short)0);
			}
		}
	}
	
	public boolean confirmaSaida() {
		AlertDialog.Builder msg = new AlertDialog.Builder(this);
		
		msg.setMessage("Deseja realmente cancelar a troca?");
		msg.setTitle(R.string.atencao);
		msg.setIcon(android.R.drawable.ic_dialog_alert);
		msg.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Utils.apagarObjetoSerializado(getBaseContext());
				troca = null;
				
				setResult(Activity.RESULT_OK);
				finish();
			}
		});		
		msg.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		
		AlertDialog alert = msg.create();  
		alert.show(); 		
		return false;
	}
}
