package br.net.sav.atacadomaringa;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class TrocaTabHostActivity extends TabActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trocatabhost);
		
		Intent it = getIntent();
		long idCliente = it.getLongExtra("IdCliente", 0);
		long idTroca = it.getLongExtra("IdTroca", 0);
		
		Resources res = getResources();
		TabHost tabHost = getTabHost();
		TabSpec spec ;
		Intent intent ;
		
		intent = new Intent(TrocaTabActivity.ACTION);
		intent.addCategory(Principal.CATEGORIA);
		intent.putExtra("IdCliente",idCliente);
		intent.putExtra("IdTroca", idTroca);
		spec = tabHost.newTabSpec("Pedido").setIndicator("Capa da Troca", res.getDrawable(R.drawable.pedido)).setContent(intent);
		tabHost.addTab(spec);
		
		intent = new Intent(this, ItemTrocaTabActivity.class);
		spec = tabHost.newTabSpec("Item").setIndicator("Itens da Troca", res.getDrawable(R.drawable.itens_pedido)).setContent(intent);
		tabHost.addTab(spec);
	}
}
