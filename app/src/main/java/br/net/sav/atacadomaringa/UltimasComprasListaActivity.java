package br.net.sav.atacadomaringa;

import java.text.SimpleDateFormat;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.net.sav.dao.UltimaDAO;
import br.net.sav.modelo.Ultima;

public class UltimasComprasListaActivity extends ListActivity {
	public static final String INTENT = "ListaUltimasCompras";
	private List<Ultima> lista;
	Adaptador adaptador;

	private long idPedido;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		lista = new UltimaDAO(getApplicationContext(), null).getLista(getIntent().getLongExtra("IdCliente", -1));
	}

	@Override
	protected void onResume() {
		super.onResume();
		atualizarAdapter();
	}

	public void atualizarAdapter() {
		if (adaptador == null) {
			setContentView(R.layout.lista_ultimas_compras);
			adaptador = new Adaptador();
			setListAdapter(adaptador);
			getListView().setFastScrollEnabled(true);
		} else {
			adaptador.changeData(lista);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		idPedido = lista.get(position).getIDPedido();
		Long idPalmtop = lista.get(position).getIDPedidoPalmtop();
		startActivity(new Intent("ListaUltimasItens").addCategory(Principal.CATEGORIA)
				.putExtra("IdPedido", idPedido)
				.putExtra("IdPedidoPalmtop", idPalmtop));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		menu.setHeaderTitle(getString(R.string.selecione_opcao_desejada));
		menu.add(1, 1, 1, "Ver Itens do Pedido");

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case 1: // VER ITENS DO PEDIDO
			startActivity(new Intent("ListaUltimasItens").addCategory("JULIANDO").putExtra("IdPedido", idPedido));
			break;
		}

		return super.onContextItemSelected(item);
	}

	// ------------------ADAPTER---------------------
	public class Adaptador extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void changeData(List<Ultima> lista) {
			UltimasComprasListaActivity.this.lista = lista;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			if (view == null) {
				LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = inflater.inflate(R.layout.lista_ultimas_compras, null);
			}

			if (position % 2 == 0) {
				view.setBackgroundResource(R.drawable.alterselector1);
			} else {
				view.setBackgroundResource(R.drawable.alterselector2);
			}

			Ultima u = lista.get(position);

			TextView pedido = (TextView) view.findViewById(R.idListaUltimasCompras.pedido);
			pedido.setText(String.format("Pedido: %d", u.getIDPedido()));
			pedido.setTextColor(Color.GREEN);

			TextView pedidoPalm = (TextView) view.findViewById(R.idListaUltimasCompras.pedidoPalm);
			pedidoPalm.setText(String.format("Pedido Palm: %d ", u.getIDPedidoPalmtop()));

			TextView data = (TextView) view.findViewById(R.idListaUltimasCompras.DatePedido);
			data.setText(String.format("Data Ultima Compra: %s ", sdf.format(u.getDataUltimaCompra())));

			TextView valor = (TextView) view.findViewById(R.idListaUltimasCompras.valor);
			valor.setText(String.format("Valor Faturado: %.4f", u.getTotal()));

			TextView valorUtimaCompra = (TextView) view.findViewById(R.id.ListaUltimasCompras_ValoUtimaCompra);
			valorUtimaCompra.setText(String.format("Valor Utima Compra: %.4f", u.getTotalUltimaCompra()));

			return view;
		}
	}
}
