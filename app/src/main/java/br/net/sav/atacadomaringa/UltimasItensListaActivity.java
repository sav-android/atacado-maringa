package br.net.sav.atacadomaringa;

import android.app.ListActivity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.UltimaItemDAO;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.UltimaItem;

public class UltimasItensListaActivity extends ListActivity {
	private List<UltimaItem> listaAux;
	private List<UltimaItem> lista;
	TextView txvTitulo;
	Adaptador adaptador;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista_ultimas_itens);
		 txvTitulo = (TextView) findViewById(R.id.txtTitle);
		 lista = new ArrayList<>();
		long idPedido = getIntent().getLongExtra("IdPedido", -1);
		long idPedidoPalmtop = getIntent().getLongExtra("IdPedidoPalmtop", 0);


		ArrayList<Item> itens = new ItemDAO(getBaseContext(), null).getByPedido(idPedidoPalmtop);
		listaAux = new UltimaItemDAO(getApplicationContext(), null).getLista(idPedido);

		if (!itens.isEmpty() && !listaAux.isEmpty()) {
			for (UltimaItem item : listaAux) {
				for (Item itemPedido : itens) {
					String idProdutoAux = String.valueOf(itemPedido.getIDProduto());
					String idProdutoUtimaCompra = String.valueOf(item.getIDProduto());

					if (idProdutoAux.contains(idProdutoUtimaCompra)) {
						item.setIDProduto(itemPedido.getIDProduto());
						item.setDescricao(itemPedido.getDescricao());
						lista.add(item);
					}

				}
			}
		}
		if (lista.isEmpty()){
			lista = listaAux;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		atualizar();
	}

	public void atualizar() {
		if (adaptador == null) {
			adaptador = new Adaptador();
			setListAdapter(adaptador);
			getListView().setFastScrollEnabled(true);
		} else {
			adaptador.changeData(lista);
		}
		barraTitulo();
	}

	public void barraTitulo() {
		txvTitulo.setVisibility(View.VISIBLE);
		txvTitulo.setText(String.format(getString(R.string.itens_do_pedido_nr2), getIntent().getLongExtra("IdPedido", 0)));
	}

	// -------------------ADAPTER------------------
	public class Adaptador extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void changeData(List<UltimaItem> lista) {
			UltimasItensListaActivity.this.lista = lista;
			notifyDataSetChanged();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.lista_ultimas_itens, null);
			}

			if (position % 2 == 0)
				convertView.setBackgroundColor(Color.DKGRAY);
			else
				convertView.setBackgroundColor(Color.BLACK);

			UltimaItem ui = lista.get(position);

			TextView codigo = (TextView) convertView.findViewById(R.idListaUltimasItens.codigo);
			TextView descricaoEmb = (TextView) convertView.findViewById(R.idListaUltimasItens.descricaoEmb);
			TextView qtd = (TextView) convertView.findViewById(R.idListaUltimasItens.qtd);
			TextView qtdCortada = (TextView) convertView.findViewById(R.idListaUltimasItens.qtdCortada);
			TextView valor = (TextView) convertView.findViewById(R.idListaUltimasItens.valorLiquido);
			TextView valorFaturado = (TextView) convertView.findViewById(R.id.txvListaUltimasItens_valorFaturado);
			TextView desconto = (TextView) convertView.findViewById(R.id.txvListaUltimasItens_desconto);
			TextView descontofaturado = (TextView) convertView.findViewById(R.id.txvListaUltimasItens_descontoFaturado);

			codigo.setTextColor(Color.GREEN);
			codigo.setText(String.format("%d - %s", ui.getIDProduto(), ui.getDescricao() == null ? getString(R.string.nao_encontrado) : ui.getDescricao()));
			codigo.setTextColor(Color.GREEN);
			descricaoEmb.setText(ui.getDescricaoEmbalagem());
			qtd.setText(String.format("Qtde : %.2f", ui.getQtde()));
			qtdCortada.setText(String.format("Qtde Cortada : %.2f", ui.getQtdeCortada()));
			valor.setText(String.format("Valor : R$ %.4f", ui.getTotalItem()));
			valorFaturado.setText(String.format("Valor Faturado : R$ %.4f", ui.getValorTotalItemFaturado()));
			desconto.setText(String.format("Desconto : R$ %.2f", ui.getDescontoItem()));
			descontofaturado.setText(String.format("Desconto faturado : R$ %.2f", ui.getDescontoItemFaturado()));

			return convertView;
		}
	}
}
