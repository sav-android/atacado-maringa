package br.net.sav.atacadomaringa;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import br.net.sav.dao.VendaVendedorDAO;
import br.net.sav.modelo.VendaVendedor;

public class VendaPorPeriodoListActivity extends ListActivity {
	public static final String ACTION = "VendaPorPeriodo" ;
	private VendaPorPeriodoListAdapter adapter ;
	private List<VendaVendedor> lista ;				
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);		
	}
	
	@Override
	protected void onResume() {	
		super.onResume();				
		
		VendaVendedorDAO vendaDAO = new VendaVendedorDAO(getBaseContext(), null);
		
		lista = vendaDAO.getAll() ;		
		
		if(lista !=null) {
			if(adapter == null){
				setContentView(R.layout.listavendaporperiodo);
				adapter = new VendaPorPeriodoListAdapter(getBaseContext(), lista);
				setListAdapter(adapter);
				getListView().setFastScrollEnabled(true);				
			} else {
				adapter.changeData(lista);
			}
		}		
	}						
}