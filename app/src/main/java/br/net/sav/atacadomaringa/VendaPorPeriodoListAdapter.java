package br.net.sav.atacadomaringa;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.net.sav.Utils;
import br.net.sav.modelo.VendaVendedor;

public class VendaPorPeriodoListAdapter extends BaseAdapter {
	private List<VendaVendedor> lista ;
	private Context ctx ;
	
	public VendaPorPeriodoListAdapter(Context ctx, List<VendaVendedor> lista) {
		this.ctx = ctx ;
		this.lista = lista ;
	}

	public void changeData(List<VendaVendedor> lista) {
		this.lista = lista ;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position ;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewG) {
		if(view == null) {
			LayoutInflater layout = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layout.inflate(R.layout.listavendaporperiodo, null);
		}
		
		VendaVendedor venda = lista.get(position);
		//calcula o valor total da tonelagem e das vendas
		double auxTotalVendasGeral = 0, auxTotalTonelagemGeral = 0;
		
		for(int i = 0; i < lista.size(); i++){
			VendaVendedor v = lista.get(i);
			auxTotalTonelagemGeral += v.getTotalTonelagem() ;
			auxTotalVendasGeral += v.getTotalVendas() ;
		}
						
		TextView txtPeriodo = (TextView)view.findViewById(R.idVendaPeriodo.txtPeriodo);
		txtPeriodo.setText(venda.getPeriodo());
		txtPeriodo.setTextColor(Color.GREEN);
		
		TextView txtTotalVendas = (TextView)view.findViewById(R.idVendaPeriodo.txtTotalVendas);
		if(venda.getTotalVendas() != 0)
			txtTotalVendas.setText(String.format("Total Vendas: %.2f", venda.getTotalVendas()));
		
		TextView txtPercTotalVendas = (TextView)view.findViewById(R.idVendaPeriodo.txtPercTotalVendas);
		if(venda.getTotalVendas() != 0)
			txtPercTotalVendas.setText(String.format("Percentual Total Vendas: %.2f", Utils.arredondar((venda.getTotalVendas() / auxTotalVendasGeral) * 100,2,2)));
		
		TextView txtTonelagem = (TextView)view.findViewById(R.idVendaPeriodo.txtTonelagem);			
		if(venda.getTotalTonelagem() != 0)	
			txtTonelagem.setText(String.format("Total tonelagem: %.2f",venda.getTotalTonelagem()));
		
		TextView txtPercTonelagem = (TextView)view.findViewById(R.idVendaPeriodo.txtPercTonelagem);		
		if(venda.getTotalTonelagem() != 0)
			txtPercTonelagem.setText(String.format("Percentual Tonelagem: %.2f", Utils.arredondar((venda.getTotalTonelagem() / auxTotalTonelagemGeral) *100,2,2)));					
		
		return view;
	}
}
