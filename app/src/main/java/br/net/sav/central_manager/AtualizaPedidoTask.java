package br.net.sav.central_manager;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import br.net.sav.IntegradorWeb.dto.RetornoPedido;
import br.net.sav.dao.PedidoDAO;

public class AtualizaPedidoTask extends AsyncTask<Void,Void, Void> {


    private Context mContext;
    private List<RetornoPedido> resultado;

    public AtualizaPedidoTask(Context mContext, List<RetornoPedido> resultado) {
        this.mContext = mContext;
        this.resultado = resultado;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        new PedidoDAO(mContext, null)
                .atualizaPedidosGweb(resultado);
        return null;
    }
}
