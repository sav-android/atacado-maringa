package br.net.sav.central_manager;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.Cliente;
import br.net.sav.IntegradorWeb.dto.ClienteGWebConversor;
import br.net.sav.IntegradorWeb.dto.Endereco;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.EmpresaFilialDAO;
import br.net.sav.modelo.EmpresaFilial;

public class ClienteConverter implements ClienteGWebConversor {

    private Context mContext;
    List<br.net.sav.modelo.Cliente> clientesParaConverter = null;

    public ClienteConverter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public List<Cliente> getClientesGWebConversor() {
        clientesParaConverter = new ClienteDAO(mContext, null).listaClientesNaoEnviado();
        EmpresaFilial filial = new EmpresaFilialDAO(mContext, null).getFilial();

        List<Cliente> listaClientesJaConvertidos = new ArrayList<>();
        for(br.net.sav.modelo.Cliente cliente:clientesParaConverter){
            listaClientesJaConvertidos.add(getClienteConvertido(cliente, filial));
        }

        return listaClientesJaConvertidos;
    }

    @Override
    public int contarClientesParaEnviar() {
        return clientesParaConverter == null? 0: clientesParaConverter.size();
    }

    private Cliente getClienteConvertido(br.net.sav.modelo.Cliente cliente, EmpresaFilial filial) {
        Cliente clienteConvertido = new Cliente();
        clienteConvertido.setAtivo(true);
        clienteConvertido.setCnpj_cpf(cliente.getCnpjCpf());
        clienteConvertido.setEndereco(getEndereco(cliente));
        clienteConvertido.setCodigo(cliente.getIdCliente());
        clienteConvertido.setInscricao_rg(cliente.getIeRg());
        clienteConvertido.setNome_fantasia(cliente.getFantasia());
        clienteConvertido.setId_filial(filial == null? 0: filial.getIdFilial());
        clienteConvertido.setRazao_social(cliente.getRazao());
        clienteConvertido.setLinha_texto(cliente.toLinhaTexto());

        return clienteConvertido;
    }

    private Endereco getEndereco(br.net.sav.modelo.Cliente cliente) {

        Endereco endereco = new Endereco();
        endereco.setBairro(cliente.getBairro());
        endereco.setCEP(cliente.getCEP());
        endereco.setCidade(cliente.getCidade());
        endereco.setEstado(cliente.getUF());
        endereco.setUf(cliente.getUF());
        endereco.setComplemento(cliente.getComplemento());
        endereco.setLogradouro(cliente.getEndereco());
        endereco.setNumero(String.valueOf(cliente.getNumero()));

        return endereco;

    }

}
