package br.net.sav.central_manager;

import android.content.Context;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


import br.net.sav.IntegradorWeb.dto.Endereco;
import br.net.sav.IntegradorWeb.dto.MapeamentoDto;
import br.net.sav.adapter.MapeamentoCMConversor;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.util.dao.MapeamentoDAO;
import br.net.sav.util.modelo.Mapeamento;

public class MapeamentoConversor implements MapeamentoCMConversor {
    private MapeamentoDAO mapeamentoDAO;
    private final ClienteDAO clienteDAO;

    public MapeamentoConversor(Context mcontext) {

        mapeamentoDAO = new MapeamentoDAO(mcontext);

        clienteDAO = new ClienteDAO(mcontext, null);
    }

    public List<MapeamentoDto> getMapeamentosConvertidos(){
        final ArrayList<MapeamentoDto> retorno = new ArrayList<>();
        List<Mapeamento> mapeamentos = mapeamentoDAO.listaEnviarCentralManager();
        for(Mapeamento mapeamento: mapeamentos){
            MapeamentoDto mapeamentoDto = new MapeamentoDto();
            mapeamentoDto.setId_cliente(mapeamento.getIdCliente());
            mapeamentoDto.setData_hora(mapeamento.getDataHora());
            mapeamentoDto.setId_pedido(mapeamento.getIdPedido());
            mapeamentoDto.setMotivo_nao_venda(mapeamento.getMotivoNaoVenda());
            mapeamentoDto.setPosicao(mapeamento.getPosicao());
            mapeamentoDto.setCliente(getClienteConvertido(mapeamento.getIdCliente()));

            retorno.add(mapeamentoDto);
        }

        return retorno;
    }

    public Integer getContagemDeMapeamentoParaEnvio(){
        return mapeamentoDAO.getContagemParaEnviarParaoCentralManager();
    }

    private br.net.sav.IntegradorWeb.dto.Cliente getClienteConvertido(Long idCliente) {
        Cliente cliente = null;
        try {
            cliente = clienteDAO.get(idCliente);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        br.net.sav.IntegradorWeb.dto.Cliente clienteConvertido = new br.net.sav.IntegradorWeb.dto.Cliente();

        if(cliente==null){
            clienteConvertido.setAtivo(true);
            clienteConvertido.setCnpj_cpf("000000000");
            clienteConvertido.setEndereco(getEndereco(cliente));
            clienteConvertido.setId(idCliente);
            clienteConvertido.setInscricao_rg("");
            clienteConvertido.setNome_fantasia("Cliente não encontrado");
            clienteConvertido.setRazao_social("Cliente não encontrado");
        }else{
            clienteConvertido.setAtivo(true);
            clienteConvertido.setCnpj_cpf(cliente.getCnpjCpf());
            clienteConvertido.setEndereco(getEndereco(cliente));
            clienteConvertido.setId(cliente.getIdCliente());
            clienteConvertido.setInscricao_rg(cliente.getIeRg());
            clienteConvertido.setNome_fantasia(cliente.getFantasia());
            clienteConvertido.setRazao_social(cliente.getRazao());
        }



        return clienteConvertido;
    }

    private Endereco getEndereco(Cliente cliente) {
        Endereco endereco = new Endereco();
        if(cliente!=null){
            endereco.setBairro(cliente.getBairro());
            endereco.setCEP(cliente.getCEP());
            endereco.setCidade(cliente.getCidade());
            endereco.setEstado(cliente.getUF());
            endereco.setUf(cliente.getUF());
            endereco.setComplemento(cliente.getComplemento());
            endereco.setLogradouro(cliente.getEndereco());
            endereco.setNumero("0");
        }else{
            endereco.setBairro("");
            endereco.setCEP("");
            endereco.setCidade("");
            endereco.setEstado("");
            endereco.setUf("");
            endereco.setComplemento("");
            endereco.setLogradouro("");
            endereco.setNumero("0");
        }


        return endereco;
    }
}
