package br.net.sav.central_manager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.Cliente;
import br.net.sav.IntegradorWeb.dto.CondicaoPagamento;
import br.net.sav.IntegradorWeb.dto.Endereco;
import br.net.sav.IntegradorWeb.dto.Familia;
import br.net.sav.IntegradorWeb.dto.FormaPagamento;
import br.net.sav.IntegradorWeb.dto.Fornecedor;
import br.net.sav.IntegradorWeb.dto.ItemPedido;
import br.net.sav.IntegradorWeb.dto.Operacao;
import br.net.sav.IntegradorWeb.dto.Pedido;
import br.net.sav.IntegradorWeb.dto.PedidoGWebConversor;
import br.net.sav.IntegradorWeb.dto.Produto;
import br.net.sav.IntegradorWeb.dto.TabelaPreco;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.FornecedorDAO;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.TipoPedido;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.util.VersoesSav;

import static br.net.sav.util.modelo.Email.ENVIADO;

public class PedidoConverter implements PedidoGWebConversor {
    private Context mContext;

    public PedidoConverter(Context mContext){
        this.mContext = mContext;
    }

    @Override
    public List<Pedido> getPedidoGWebConversor() {
        PedidoDAO dao = new PedidoDAO(mContext, null);

        ArrayList<br.net.sav.modelo.Pedido> pedidosParaConverter =dao
                .listaPedidosNaoEnviadosGWEB();

        String versao = "";

        long idVendedor = new ParametroDAO(mContext, null).get().getIdVendedor();

        try {
            versao = VersoesSav.currentVersionName(mContext);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String modelo = Build.MODEL;
        int versaoSDK =android.os.Build.VERSION.SDK_INT;

        ArrayList<Pedido> pedidosRetornados = new ArrayList<Pedido>();
        for(br.net.sav.modelo.Pedido pedido:pedidosParaConverter){
            Pedido pedidoRetorno = new Pedido();
            if (verificaExisteCliente(pedido)) continue;

            pedidoRetorno.setId(pedido.getIDPedido());
            pedidoRetorno.setId_vinculado(pedido.getIDPedido());
            pedidoRetorno.setData(pedido.getDataPedido());
            pedidoRetorno.setSituacao("Fechado");
            pedidoRetorno.setTotal_bruto(pedido.getTotalGeral());
            pedidoRetorno.setTotal_desconto(pedido.getTotalGeral()-pedido.getTotalLiquido());
            pedidoRetorno.setTotal_liquido(pedido.getTotalLiquido());

            pedidoRetorno.setCliente(getClienteConvertido(pedido.getIDCliente()));

            pedidoRetorno.setItens(getItensConvertidos(pedido, idVendedor));

            pedidoRetorno.setOperacao(getOperacaoConvertido(pedido.getIDTPedido()));

            pedidoRetorno.setForma_pagamento(getFormaPagamentoConvertido(pedido.getIDFormaPagamento()));

            pedidoRetorno.setCondicao_pagamento(getCondicaoDePagamentoConvertido(pedido.getIDCondicao()));

            pedidoRetorno.setTabela_preco(getTabelaPreco(pedido.getIDTabela()));

            pedidoRetorno.setVersaosav(versao);

            pedidoRetorno.setEquipamento(modelo);

            pedidoRetorno.setVersaosdk(String.valueOf(versaoSDK));


            pedidoRetorno.setLinha_texto(pedido.toLinhaTexto(idVendedor));
            pedidosRetornados.add(pedidoRetorno);

        }

        return pedidosRetornados;


    }

    private boolean verificaExisteCliente(br.net.sav.modelo.Pedido pedido) {
        br.net.sav.modelo.Cliente cliente = null;
        try {
            cliente = new ClienteDAO(mContext, null).get(pedido.getIDCliente());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (cliente == null)
            return true;

        return false;
    }

    @Override
    public int contarPedidosParaEnviar() {
        return new PedidoDAO(mContext,null)
                .contarPedidosEnviarCentralManager();
    }

    private TabelaPreco getTabelaPreco(int idTabela) {
        br.net.sav.modelo.Tabela tabelaPreco = new TabelaDAO(mContext, null).get((short) idTabela);
        TabelaPreco tabelaPrecoConvertido = new TabelaPreco();
        tabelaPrecoConvertido.setAtivo(true);
        tabelaPrecoConvertido.setDescricao(tabelaPreco.getDescricao());
        tabelaPrecoConvertido.setId((long) tabelaPreco.getIdTabela());
        return tabelaPrecoConvertido;
    }

    private CondicaoPagamento getCondicaoDePagamentoConvertido(short idCondicao) {
        Condicao condicao = new CondicaoDAO(mContext, null).get(idCondicao);
        CondicaoPagamento condicaoPagamentoConvertido = new CondicaoPagamento();
        condicaoPagamentoConvertido.setDescricao(condicao.getDescricao());
        condicaoPagamentoConvertido.setId((long) condicao.getIdCondicao());
        return condicaoPagamentoConvertido;
    }

    private FormaPagamento getFormaPagamentoConvertido(long idForma) {
        br.net.sav.modelo.FormaPagamento formaPagamento = new FormaPagamentoDAO(mContext, null).get(idForma);
        FormaPagamento formaPagamentoConvertido = new FormaPagamento();
        formaPagamentoConvertido.setId(formaPagamento.getIdFormaPagamento());
        formaPagamentoConvertido.setDescricao(formaPagamento.getDescricao());
        return formaPagamentoConvertido;
    }

    private Operacao getOperacaoConvertido(short idTipoPedido) {
        TipoPedido tipoPedido = new TipoPedidoDAO(mContext, null).get(idTipoPedido);
        Operacao operacaoConvertido = new Operacao();
        operacaoConvertido.setAtivo(true);
        operacaoConvertido.setDescricao(tipoPedido.getDescricao());
        operacaoConvertido.setId((long) tipoPedido.getIdTipoPedido());
        return operacaoConvertido;
    }

    private List<ItemPedido> getItensConvertidos(br.net.sav.modelo.Pedido pedido, long idVendedor) {
        ItemDAO itemDAO = new ItemDAO(mContext, null);
        ArrayList<ItemPedido> itemPedidosConvertidos = new ArrayList<>();

        ArrayList<Item> itensDoPedido = itemDAO.getByPedido(pedido.getIDPedido());
        for(Item item: itensDoPedido){
            ItemPedido itemPedidoConvertido = new ItemPedido();
            itemPedidoConvertido.setProduto(getProduto(item.getIDProduto()));
            itemPedidoConvertido.setId_pedido(pedido.getIDPedido());
            itemPedidoConvertido.setPercentual_desconto(item.getDesconto());
            itemPedidoConvertido.setQuantidade((int) item.getQuantidade());
            itemPedidoConvertido.setValor_desconto(item.getValorDesconto());
            itemPedidoConvertido.setValor_total_bruto(item.getTotalGeral());
            itemPedidoConvertido.setValor_total_liquido(item.getTotalLiquido());
            itemPedidoConvertido.setValor_unitario(item.getValorUnitPraticado());
            itemPedidoConvertido.setLinha_texto(item.toLinhaTexto(pedido,idVendedor));

            itemPedidosConvertidos.add(itemPedidoConvertido);
        }


        return itemPedidosConvertidos;

    }

    private Produto getProduto(long idProduto) {
        br.net.sav.modelo.Produto produto = new ProdutoDAO(mContext, null).get(idProduto);
        Produto produtoConvertido = new Produto();
        produtoConvertido.setDescricao(produto.getDescricao() == null? "Sem descrição" : produto.getDescricao());
        produtoConvertido.setNome(produto.getDescricao() == null? "Sem descrição" : produto.getDescricao());
        produtoConvertido.setFamilia(getFamiliaConvertida(produto.getIdGrupo()));
        produtoConvertido.setFornecedor(getFornecedorConvertido(produto.getIdFornecedor()));
        produtoConvertido.setId(produto.getIdProduto());
        return produtoConvertido;
    }

    private Fornecedor getFornecedorConvertido(long idFornecedor) {
        br.net.sav.modelo.Fornecedor fornecedor = new FornecedorDAO(mContext, null).getById(idFornecedor);

        Fornecedor fornecedorConvertido = new Fornecedor();
        fornecedorConvertido.setAtivo(true);
        fornecedorConvertido.setId(fornecedor.getIdFornecedor()== 0 ?
                idFornecedor:
                fornecedor.getIdFornecedor());
        fornecedorConvertido.setNome(fornecedor.getDescricao() == null?
                "Fornecedor Sem Descrição":
                fornecedor.getDescricao());
        return fornecedorConvertido;
    }

    private Familia getFamiliaConvertida(int idGrupo) {

        Familia familia = new Familia();
        familia.setDescricao("Família Sem Descricao");
        familia.setNome("Família sem Nome");
        familia.setId((long) idGrupo);
        return familia;
    }

    private Cliente getClienteConvertido(long idCliente) {
        br.net.sav.modelo.Cliente cliente = null;
        try {
            cliente = new ClienteDAO(mContext, null).get(idCliente);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Cliente clienteConvertido = new Cliente();
        clienteConvertido.setAtivo(true);
        clienteConvertido.setCnpj_cpf(cliente.getCnpjCpf());
        clienteConvertido.setEndereco(getEndereco(cliente));
        clienteConvertido.setId(cliente.getIdCliente());
        clienteConvertido.setInscricao_rg(cliente.getIeRg());
        clienteConvertido.setNome_fantasia(cliente.getFantasia());
        clienteConvertido.setRazao_social(cliente.getRazao());

        return clienteConvertido;
    }

    private Endereco getEndereco(br.net.sav.modelo.Cliente cliente) {

        Endereco endereco = new Endereco();
        endereco.setBairro(cliente.getBairro());
        endereco.setCEP(cliente.getCEP());
        endereco.setCidade(cliente.getCidade());
        endereco.setEstado(cliente.getUF());
        endereco.setUf(cliente.getUF());
        endereco.setComplemento(cliente.getComplemento());
        endereco.setLogradouro(cliente.getEndereco());
        endereco.setNumero("0");

        return endereco;

    }

    public List<br.net.sav.modelo.Pedido> converteParaPedidoDoSistema(List<Pedido> resultado) {
        ArrayList<br.net.sav.modelo.Pedido> pedidosParaRetornar = new ArrayList<>();
        int qtd = 1;
        for(Pedido pedido:resultado){
            Log.i("PEDIDO HISTORICO", "id: " + pedido.getId_vinculado()+ " - qtd: " + qtd++);
            br.net.sav.modelo.Pedido ped = new br.net.sav.modelo.Pedido();
            ped.setCodPedidoGWeb(pedido.getId());
            ped.setIDPedido(pedido.getId_vinculado());
            ped.setEnviado(true);
            ped.setStatus(true);
            ped.setHistoricoPedidoCM(true);
            ped.setRetornoStatusPedido("Confirmado");
            ped.setRetornoNumeroNotaFiscal("0");
            ped.setStatusPedidoGWeb(StatusCentralManager.OK.getCodigo());
            ped.setTotalGeral(pedido.getTotal_bruto());
            ped.setTotalLiquido(pedido.getTotal_liquido());
            ped.setIDFilial(Short.valueOf(String.valueOf(pedido.getId_filial())));
            ped.setIDCliente(pedido.getCliente().getCodigo());
            ped.setIDFormaPagamento(pedido.getForma_pagamento().getCodigo());
            ped.setIDCondicao(Short.valueOf(String.valueOf(pedido.getCondicao_pagamento().getCodigo())));
            ped.setTipoPedido(pedido.getOperacao().getDescricao());
            ped.setIDTPedido(Short.valueOf(String.valueOf(pedido.getOperacao().getCodigo())));
            ped.setIDTabela(Short.valueOf(String.valueOf((pedido.getTabela_preco().getCodigo()))));
            ped.setDataPedido(pedido.getData());
            ped.setDataFaturamento(pedido.getData());
            ped.setDataEnvio(ped.getDataPedido());
            ped.setItensVendidos(getItensConvertidosParaItensDoSistema(pedido.getItem_pedido(), ped));
            ped.setNrItens((short)pedido.getItem_pedido().size());
            pedidosParaRetornar.add(ped);
        }
        return pedidosParaRetornar;
    }

    private ArrayList<Item> getItensConvertidosParaItensDoSistema(List<ItemPedido> itens, br.net.sav.modelo.Pedido pedido) {
        ArrayList<Item> itensParaRetornar = new ArrayList<>();
        short numeroItem = 1;

        for(ItemPedido item: itens){
            br.net.sav.modelo.Produto produto = new ProdutoDAO(mContext, null).get(item.getProduto().getCodigo());

            Item itemDigitacao = new Item();
            itemDigitacao.setIDPedido(pedido.getIDPedido());
            itemDigitacao.setNrItem(numeroItem++);
            itemDigitacao.setQuantidade((short) item.getQuantidade());
            itemDigitacao.setCodigoBarra(produto.getIdProduto()== 0? "":produto.getCodigoBarra());
            itemDigitacao.setDescricao(item.getProduto().getDescricao());
            itemDigitacao.setDesconto(item.getPercentual_desconto());
            itemDigitacao.setValorUnitOriginal(item.getValor_unitario());
            itemDigitacao.setValorUnitPraticado(item.getValor_unitario());
            itemDigitacao.setTotalGeral(item.getValor_total_bruto());
            itemDigitacao.setTotalLiquido(item.getValor_total_liquido());
            itemDigitacao.setIDProduto(produto.getIdProduto()== 0 ? item.getProduto().getCodigo(): produto.getIdProduto());
            itemDigitacao.setValorDesconto(item.getValor_desconto());
            itemDigitacao.setDataPedido(item.getData_criacao());
            itemDigitacao.setCodigoNCM("0");


            itensParaRetornar.add(itemDigitacao);
        }

        return itensParaRetornar;
    }

}
