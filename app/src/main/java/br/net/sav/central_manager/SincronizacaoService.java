package br.net.sav.central_manager;

import android.util.Log;

import br.net.sav.IntegradorWeb.repository.MapeamentoRepository;
import br.net.sav.IntegradorWeb.repository.PedidoRepository;
import br.net.sav.IntegradorWeb.retrofit.service.AoFinalizarRequisicaoListener;
import br.net.sav.IntegradorWeb.services.CentralManagerService;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.util.dao.MapeamentoDAO;
import okhttp3.ResponseBody;

public class SincronizacaoService extends CentralManagerService {


    @Override
    protected void quandoReqSucesso(String resultado) {
        //new AtualizaPedidoTask(this, resultado).execute();
        Log.d("SincronizacaoService", resultado);
    }

    @Override
    protected void quandoReqErro(String mensagemDeErro) {
        Log.d("SincronizacaoService", mensagemDeErro);
    }

    @Override
    protected String gettoken() {
        return new ParametroDAO(this,null).get()==null?"":new ParametroDAO(this,null).get().getTokenApi();
    }

    @Override
    protected void getEnviarRequisicoes() {
        new PedidoRepository().salvarPedidoSincrono(gettoken(), new br.net.sav.central_manager.PedidoConverter(this), new AoFinalizarRequisicaoListener<String>() {
            @Override
            public void quandoSucesso(String resultado) {
                quandoReqSucesso(resultado);
                setSucessoAPI();
            }

            @Override
            public void quandoErro(String erro) {
                quandoReqErro(erro);
                setSucessoFalsoAPI();
            }
        });

        final MapeamentoDAO mapeamentoDAO = new MapeamentoDAO(getApplication());
        MapeamentoConversor mapeamentoConversor = new MapeamentoConversor(this);

        if (!mapeamentoConversor.getMapeamentosConvertidos().isEmpty())
            new MapeamentoRepository().salvar(gettoken(),  mapeamentoConversor, new MapeamentoRepository.AoFinalizarRequisicaoListener<ResponseBody>() {
                @Override
                public void quandoSucesso(ResponseBody resultado) {
                    // mapeamentoDAO.atualizarMapeamento(resultado);

                    Log.d("Mapeamento CM", "SUCESSO!");
                }

                @Override
                public void quandoErro(String erro) {

                    Log.d("Mapeamento CM", "quandoErro: "+erro);
                }
            });
    }




}
