package br.net.sav.central_manager;

import android.util.Log;

import br.net.sav.IntegradorWeb.repository.ClienteRepository;
import br.net.sav.IntegradorWeb.retrofit.service.AoFinalizarRequisicaoListener;
import br.net.sav.IntegradorWeb.services.CentralManagerService;
import br.net.sav.dao.ParametroDAO;

public class SincronizacaoServiceCliente extends CentralManagerService {
    @Override
    protected void quandoReqSucesso(String resultado) {
        Log.d("SincronizacaoService", resultado);

    }

    @Override
    protected void quandoReqErro(String mensagemDeErro) {
        Log.d("SincronizacaoService", mensagemDeErro);

    }

    @Override
    protected String gettoken() {
        return new ParametroDAO(this,null).get()==null?"":new ParametroDAO(this,null).get().getTokenApi();
    }

    @Override
    protected void getEnviarRequisicoes() {

        new ClienteRepository().salvarClienteAsync(gettoken(), new br.net.sav.central_manager.ClienteConverter(this),
                new AoFinalizarRequisicaoListener<String>() {
            @Override
            public void quandoSucesso(String resultado) {
                setSucessoAPI();
            }

            @Override
            public void quandoErro(String erro) {
                setSucessoFalsoAPI();
            }
        });

    }
}
