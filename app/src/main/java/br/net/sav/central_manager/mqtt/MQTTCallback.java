package br.net.sav.central_manager.mqtt;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.net.sav.central_manager.mqtt.mqttfactory.TratadorMensagemFactory;


public class MQTTCallback {

    private static final String TAG = "MQTT";
    private static final String CONEXAO_PERDIDA = "Conexao Perdida";

    public static MqttCallback getCallBack(final Context context){
        return new MqttCallback() {

            @Override
            public void connectionLost(Throwable cause) {
                Log.d(TAG, CONEXAO_PERDIDA);
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d(TAG,message.toString());


                new TratadorMensagemFactory()
                        .getTratadorPor(topic)
                        .tratarMensagem(context,message);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                try {
                    Log.d(TAG, String.valueOf(token.getMessage().getId()));
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        };

    }




}
