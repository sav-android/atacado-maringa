package br.net.sav.central_manager.mqtt;



import java.util.ArrayList;

import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.modelo.Usuario;

public class MQTTCanais {

    public static ArrayList<String> getCanaisMQTT(Vendedor mParametro, Usuario usuario) {

        ArrayList<String> canais = new ArrayList<>();

        //canal para receber status do pedido
        canais.add(mParametro.getId()+":pedido");
        canais.add(mParametro.getId()+":mapeamento");
        canais.add(mParametro.getId()+":cliente");

        //canais.add(mParametro.getIdFilial()+":"+usuario.getIdUsuario()+":mensagem");


        return canais;
    }
}
