package br.net.sav.central_manager.mqtt;

import android.content.Context;

import java.util.ArrayList;

import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.modelo.Usuario;
import br.net.sav.mqtt.MQTTHelper;

public class MQTTConfig {

//    public static final String URL_MQTT = "tcp://api.solumobi.com.br:1883";
    public static final String URL_MQTT = "tcp://192.168.100.50:1883";
    public static final String SAV = "sav";
    public static final String SENHA = "s0lum0b!*";

    public static void configurar(Context context, Vendedor mParametro, Usuario usuario){
        if(mParametro==null)
            return;

        ArrayList<String> canais = MQTTCanais.getCanaisMQTT(mParametro, usuario);

        MQTTHelper.conectar(
                context,
                URL_MQTT,
                mParametro.getIdUnico(),
                canais,
                MQTTCallback.getCallBack(context), SAV, SENHA);
    }


}
