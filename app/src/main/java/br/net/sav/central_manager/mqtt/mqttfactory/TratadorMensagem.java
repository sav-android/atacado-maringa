package br.net.sav.central_manager.mqtt.mqttfactory;

import android.content.Context;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface TratadorMensagem {

    void tratarMensagem(Context context, MqttMessage message);
}
