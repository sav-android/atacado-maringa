package br.net.sav.central_manager.mqtt.mqttfactory;


import br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento.TratadorMensagemCliente;
import br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento.TratadorMensagemMapeamento;
import br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento.TratadorMensagemPedido;
import br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento.TratadorMensagemSAV;

public class TratadorMensagemFactory {

    public TratadorMensagem getTratadorPor(String topico){
        if(topico.contains("pedido"))
            return new TratadorMensagemPedido();

        if(topico.contains("mapeamento"))
            return new TratadorMensagemMapeamento();

        if(topico.contains("cliente"))
            return new TratadorMensagemCliente();

        if(topico.contains("mensagem"))
            return new TratadorMensagemSAV();

        return null;

    }
}
