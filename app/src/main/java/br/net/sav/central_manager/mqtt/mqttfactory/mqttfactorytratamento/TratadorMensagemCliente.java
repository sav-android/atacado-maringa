package br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento;

import android.content.Context;

import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.net.sav.UIHelper;
import br.net.sav.central_manager.mqtt.mqttfactory.TratadorMensagem;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.mqtt.mqttdto.MensagemClienteMQTTDTO;

import static br.net.sav.mqtt.StatusCentralManager.getStatusBy;

public class TratadorMensagemCliente implements TratadorMensagem {
    @Override
    public void tratarMensagem(Context context, MqttMessage message) {
        MensagemClienteMQTTDTO mensagemDTO = new Gson().fromJson(message.toString(), MensagemClienteMQTTDTO.class);

        try {

            StatusCentralManager status = getStatusBy(mensagemDTO.status);
            switch (status){
                case OK:
                case DUPLICADO:
                    tratarMensagemQuandoSucesso(context, mensagemDTO);
                    break;
                case ERROR:
                    tratarMensagemPedidoErro(context, mensagemDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void tratarMensagemPedidoErro(Context context, MensagemClienteMQTTDTO mensagemDTO) {
        if(mensagemDTO.id_cliente != null) {
            try {
                new ClienteDAO(context, null).atualizarRetornoClienteCM(mensagemDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.id_cliente = "";
        }

        UIHelper.showNotificationIdVariado(null, context, "Cliente foi não Recebido",
                "Erro ao receber Cliente"+mensagemDTO.id_cliente+" no Central Manager: "+mensagemDTO.motivo);
    }

    private void tratarMensagemQuandoSucesso(Context context, MensagemClienteMQTTDTO mensagemDTO) {
        if(mensagemDTO.id_cliente != null) {
            try {
                new ClienteDAO(context, null).atualizarRetornoClienteCM(mensagemDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.id_cliente = "";
        }

        UIHelper.showNotificationIdVariado(null, context, " Cliente Recebido",
                "Cliente "+mensagemDTO.id_cliente+" foi salvo com sucesso no Central Manager.");
    }
}
