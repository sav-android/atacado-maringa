package br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento;

import android.content.Context;

import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.net.sav.UIHelper;
import br.net.sav.central_manager.mqtt.mqttfactory.TratadorMensagem;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.mqtt.mqttdto.MensagemMapeamentoMQTTDTO;
import br.net.sav.util.dao.MapeamentoDAO;

import static br.net.sav.mqtt.StatusCentralManager.getStatusBy;

public class TratadorMensagemMapeamento implements TratadorMensagem {
    @Override
    public void tratarMensagem(Context context, MqttMessage message) {
        MensagemMapeamentoMQTTDTO mensagemDTO = new Gson().fromJson(message.toString(), MensagemMapeamentoMQTTDTO.class);

        try {

            StatusCentralManager status = getStatusBy(mensagemDTO.status);
            switch (status){
                case OK:
                case DUPLICADO:
                    tratarMensagemQuandoSucesso(context, mensagemDTO);
                    break;
                case ERROR:
                    tratarMensagemPedidoErro(context, mensagemDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tratarMensagemPedidoErro(Context context, MensagemMapeamentoMQTTDTO mensagemDTO) {
        if(mensagemDTO.codigo_pedido != null) {
            try {
                new MapeamentoDAO(context).atualizarMapeamentoCM(mensagemDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.codigo_pedido = "";
        }

        UIHelper.showNotificationIdVariado(null, context, "Mapeamento não Atualizado",
                "Erro ao receber Mapeamneto"+mensagemDTO.codigo_pedido+" no Central Manager: "+mensagemDTO.motivo);
    }

    private void tratarMensagemQuandoSucesso(Context context, MensagemMapeamentoMQTTDTO mensagemDTO) {
        if(mensagemDTO.codigo_pedido != null) {
            try {
                new MapeamentoDAO(context).atualizarMapeamentoCM(mensagemDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.codigo_pedido = "";
        }

        UIHelper.showNotificationIdVariado(null, context, "mapeamento Atualizado",
                "Mapeamento "+mensagemDTO.codigo_pedido+" recebido com sucesso no Central Manager.");
    }
}
