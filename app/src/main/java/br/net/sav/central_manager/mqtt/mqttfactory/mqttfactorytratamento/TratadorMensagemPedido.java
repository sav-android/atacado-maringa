package br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento;

import android.content.Context;

import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.net.sav.UIHelper;
import br.net.sav.atacadomaringa.R;
import br.net.sav.mqtt.mqttdto.MensagemPedidoMQTTDTO;
import br.net.sav.central_manager.mqtt.mqttfactory.TratadorMensagem;
import br.net.sav.modelo.Pedido;
import br.net.sav.mqtt.StatusCentralManager;

import static br.net.sav.mqtt.StatusCentralManager.ERROR;
import static br.net.sav.mqtt.StatusCentralManager.getStatusBy;

public class TratadorMensagemPedido implements TratadorMensagem {
    @Override
    public void tratarMensagem(Context context, MqttMessage message) {
        MensagemPedidoMQTTDTO mensagemDTO = new Gson().fromJson(message.toString(), MensagemPedidoMQTTDTO.class);

        try {

            StatusCentralManager status = getStatusBy(mensagemDTO.status);
            switch (status){
                case PENDENTE:
                case ENVIADO:
                case OK:
                case DUPLICADO:
                    tratarMensagemQuandoSucesso(context, mensagemDTO);
                    break;
                case ERROR:
                    tratarMensagemPedidoErro(context, mensagemDTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tratarMensagemPedidoErro(Context context, MensagemPedidoMQTTDTO mensagemDTO) {
        if(mensagemDTO.codigo_pedido != null) {
            try {
                Pedido.atualizarStatusPedidoCM(context, mensagemDTO.id_central_manager, ERROR.getCodigo(), Long.valueOf(mensagemDTO.codigo_pedido));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.codigo_pedido = "";
        }

        UIHelper.showNotificationIdVariado(null, context, context.getString(R.string.Atualizacao_pedido_cm),
                "Erro ao receber pedido"+mensagemDTO.codigo_pedido+" no Central Manager: "+mensagemDTO.motivo);
    }

    private void tratarMensagemQuandoSucesso(Context context, MensagemPedidoMQTTDTO mensagemDTO) {
        if(mensagemDTO.codigo_pedido != null) {
            try {
                Pedido.atualizarStatusPedidoCM(context, mensagemDTO.id_central_manager, getStatusBy(mensagemDTO.status).getCodigo(), Long.valueOf(mensagemDTO.codigo_pedido));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            mensagemDTO.codigo_pedido = "";
        }

        UIHelper.showNotificationIdVariado(null, context, context.getString(R.string.Atualizacao_pedido_cm),
        "Pedido "+mensagemDTO.codigo_pedido+" recebido com sucesso no Central Manager.");
    }

    private void gravarNotificaoPedido(Context context, String titulo, String mensagem) {
       /* NotificacaoSAV notificacaoSAV = new NotificacaoSAV(titulo, mensagem, TipoNotificacao.PEDIDO, new Date());
        notificacaoSAV.inserir(notificacaoSAV.getDao(context,null));*/
    }
}
