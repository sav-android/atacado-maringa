package br.net.sav.central_manager.mqtt.mqttfactory.mqttfactorytratamento;

import android.content.Context;


import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import br.net.sav.UIHelper;
import br.net.sav.mqtt.mqttdto.MensagemDTO;
import br.net.sav.central_manager.mqtt.mqttfactory.TratadorMensagem;
import br.net.sav.modelo.Mensagem;
import br.net.sav.modelo.Usuario;

public class TratadorMensagemSAV implements TratadorMensagem {
    @Override
    public void tratarMensagem(Context context, MqttMessage message) {
        MensagemDTO dto = new Gson().fromJson(message.toString(), MensagemDTO.class);
        Mensagem mensagem = new Mensagem();

        mensagem.setIdMensagem(dto.id);
        mensagem.setAssunto(dto.assunto);
        mensagem.setData(dto.data);
        mensagem.setDataEnvio(dto.data_envio);
        mensagem.setMensagem(dto.mensagem);
        mensagem.setEnviado(dto.enviada);
        mensagem.setLida(dto.lida);

        Usuario usuarioOrigem = new Usuario();
        usuarioOrigem.setIDUsuario((short) dto.usuario_origem);
        mensagem.setUsuarioOrigem(usuarioOrigem);

//        mensagem.setTipoMensagem(Mensagem.TipoMensagem.RECEBIDA);
//
//        mensagem.inserir(mensagem.getDao(context,null));
//
//        gravarNotificacao(context, mensagem.getAssunto(), mensagem.getMensagem());
//
//        MostrarNotificacao(context, mensagem.getAssunto(), mensagem.getMensagem(), mensagem.getIdMensagem());

    }

    private void MostrarNotificacao(Context context, String assunto, String mensagem, int idMensagem) {

        UIHelper.showNotification(null, context, "Nova Mensagem - Assunto: "+assunto, mensagem);
    }

    private void gravarNotificacao(Context context, String assunto, String mensagem) {
//        NotificacaoSAV notificacaoSAV = new NotificacaoSAV("Nova Mensagem", "assunto: "+assunto+"\n\n"+mensagem, TipoNotificacao.PEDIDO, new Date());
//        notificacaoSAV.inserir(notificacaoSAV.getDao(context,null));
    }
}
