package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Adicionais;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class AdicionaisDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	public static final String TABELA="Adicionais";
	
	public AdicionaisDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}		
		
	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			if (cargaCompleta)
				deleteAll();
			getImportacaoDadosAdicionais(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("AdicionaisDAO", e.getMessage());
		}
	}

	private void getImportacaoDadosAdicionais(List<String> linhas, boolean cargaCompleta) {
		try {
			db.beginTransaction();
			for (String linha : linhas) {
				if (getDadosAdicionaisLinha(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} finally {
			db.endTransaction();
		}

	}

	private boolean getDadosAdicionaisLinha(Boolean cargaCompleta, String linha) throws ParseException {
		Adicionais a = new Adicionais();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		if (linha.substring(0, 8).trim().matches(MATCHES)
				&& !linha.substring(0, 8).trim().equals(""))
			a.setIdCliente(Short.parseShort(linha.substring(0, 8)
					.trim()));
		else {
			Utils.gravarLog("\nADI: " + linha);
			return true;
		}
		a.setFornecedor1(linha.substring(8, 33).trim());
		a.setDDD1(linha.substring(33, 35).trim());
		a.setFone1(linha.substring(35, 43).trim());
		a.setFornecedor2(linha.substring(44, 68).trim());
		a.setDDD2(linha.substring(68, 70).trim());
		a.setFone2(linha.substring(70, 78).trim());
		a.setFornecedor3(linha.substring(79, 103).trim());
		a.setDDD3(linha.substring(103, 105).trim());
		a.setFone3(linha.substring(106, 113).trim());
		a.setBanco(linha.substring(113, 123).trim());
		a.setAgencia(linha.substring(123, 133).trim());
		a.setConta(linha.substring(133, 143).trim());
		a.setDataConstituicaoEmpresa(sdf.parse(linha.substring(143,
				151).trim()));

		if (linha.substring(151, 152).trim().equals(""))
			a.setPredioProprio((short) 0);
		else if (linha.substring(151, 152).trim().matches(MATCHES))
			a.setPredioProprio(Short.parseShort(linha.substring(
					151, 152).trim()));
		else {
			Utils.gravarLog("\nADI: " + linha);
			return true;
		}
		a.setResponsavelCompras(linha.substring(152, 177).trim());
		a.setResponsavelPagamentos(linha.substring(177, 202).trim());
		a.setEnviado(true);

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(202))) {
			case ' ':
			case 'A':
				if (exists(a))
					update(a);
				else
					insert(a);
				break;
			case 'E':
				delete(a);
				break;
			default:
				Utils.gravarLog(ctx.getString(R.string.adi_flag_invalida) + linha);
				break;
			}
		} else {
			insert(a);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		try {
			if (getDadosAdicionaisLinha(true, linha)) return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_dados_adicionais);
	}
}
