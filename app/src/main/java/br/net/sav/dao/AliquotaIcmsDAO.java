package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.AliquotaICMS;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class AliquotaIcmsDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA ="AliquotaICMS" ;
	
	public AliquotaIcmsDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}

	public void importar(List<String> linhasTexto) {
		try {					
			deleteAll() ;

			getImportarAliquotaICMS(linhasTexto);
		} catch (Exception e){
			Log.d("AliquotaIcmsDAO", e.getMessage());
		}
	}

	private void getImportarAliquotaICMS(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for(String linha:linhasTexto){
				if (getLinhaAliquotaICMS(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaAliquotaICMS(String linha) {
		AliquotaICMS a = new AliquotaICMS() ;

		a.setUfDestino(linha.substring(0,2).trim());
		if(linha.substring(2, 4).trim().equals(""))
			a.setCodigoICMS((short) 0);
		else if(linha.substring(2, 4).trim().matches(MATCHES))
			a.setCodigoICMS(Short.parseShort(linha.substring(2,4).trim()));
		else {
			Utils.gravarLog("\nICM: " + linha);
			return true;
		}
		if(linha.substring(4, 5).trim().equals(""))
			a.setTipoPessoa((short) 0);
		else if(linha.substring(4, 5).trim().matches(MATCHES))
			a.setTipoPessoa(Short.parseShort(linha.substring(4,5)));
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}
		if(linha.substring(5, 10).trim().equals(""))
			a.setAliquota(0);
		else if(linha.substring(5, 10).trim().matches(MATCHES))
			a.setAliquota(Double.parseDouble(linha.substring(5,10).trim()) / 100);
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}
		if(exists(a))
			update(a);
		else
			insert(a) ;
		return false;
	}


	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaAliquotaICMS(linha)) return true;
		return false;
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}

	@Override
	public String getMensagemTabela() { return ctx.getString(R.string.importando_tabela_tributacao_icms); }
}
