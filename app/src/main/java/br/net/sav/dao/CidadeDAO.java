package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Cidade;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class CidadeDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	public static final String TABELA="Cidade";
	private Context ctx;

	public CidadeDAO(Context ctx,SQLiteDatabase db) {
		super(ctx, TABELA,db);
		this.ctx = ctx;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		getlinhaTexto(linha);
		return false;
	}


	@Override
	public String getMensagemTabela() { return ctx.getString(R.string.importacao_tabela_cidade); }

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}


	public void importar(List<String> linhas){
		getImportacaoCidade(linhas);
	}

	private void getImportacaoCidade(List<String> linhas) {
		try{
			deleteAll();
			try{
				db.beginTransaction();
				for(String linha:linhas){
					getlinhaTexto(linha);
				}
				db.setTransactionSuccessful();
			}catch (Exception e) {
				Log.d("CidadesDAO.Importar", e.getMessage());
			}
		}catch (Exception e) {
			Log.d("CidadesDAO.Importar", e.getMessage());
		}finally{
			db.endTransaction();
		}
	}

	private void getlinhaTexto(String linha) {
		Cidade c=new Cidade();
		c.setCidade(linha.substring(0,30).trim());

		if(!exists(c))
			insert(c);
	}


	public ArrayList<String> getCidades(){
		SQLiteDatabase db1 = new DBHelper(ctx).getWritableDatabase();
		ArrayList<String> cidades = new ArrayList<String>();;
		String sql = "SELECT * FROM cidade";
		Cursor c = null;
		try {

			c = db1.rawQuery(sql,null);

			while (c.moveToNext()){
				String cidade = "";

				cidade = c.getString(c.getColumnIndex("Cidade"));

				cidades.add(cidade);
			}

		}catch (Exception e){
			Log.d("CidadeDao", "getCidades: ");
		}finally {
			db1.close();
		}

		return cidades;
	}

}
