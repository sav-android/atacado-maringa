package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.ClienteListaActivity.OpcaoPesquisa;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.ClienteSemCompra;
import br.net.sav.modelo.ClienteSemVisita;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.mqtt.mqttdto.MensagemClienteMQTTDTO;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ClienteDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Cliente";
	public final short TODOS = 0;
	public final short NAO_ENVIADOS = 1;
	public final short ENVIADOS = 2;
	public static final int VALOR_ID_CLIENTE_NOVO = 1000000;

	public ClienteDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
	}

	@Override
	public Boolean insert(Object objeto) {
		return super.insert(objeto);
	}

	@Override
	public Boolean insert(Object objeto, SQLiteDatabase db) {
		return super.insert(objeto, db);
	}

	@Override
	public Boolean delete(Object objeto) {
		return super.delete(objeto);
	}

	@Override
	public Boolean update(Object objeto) {
		return super.update(objeto);
	}

	public void marcarEnviado(Date dataEnvio) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = String
				.format("UPDATE Cliente SET Enviado = 1, DataEnvio = \'%s\' WHERE Enviado = 0",
						Utils.sdfDataHoraDb.format(dataEnvio));
		db.execSQL(sql);
		//db.close(); singleton();
	}

	public boolean clienteEnviado(Long idCliente) {
		boolean retorno = true;
		SQLiteDatabase dbLocal = null;

		if (db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		Cursor c = null;
		try {
			c = dbLocal
					.rawQuery(
							"SELECT * FROM Cliente WHERE IdCliente = ? and Enviado = 1",
							new String[] { String.valueOf(idCliente) });
			retorno = c.getCount() == 1;

		} catch (Exception e) {
			Log.e("Cliente:StatusEnviado.", e.getMessage());
		} finally {
			c.close();
			if (db == null) {
				//db.close(); singletone();
			}
		}
		return retorno;
	}

	public void gerarTexto(long idVendedor) {
		List<Cliente> clientesExportar = listaClientesNaoEnviado();

		if (clientesExportar.size() == 0) {
			return;
		}

		try {
			Date data = new Date();
			String campo;

			String nomeArquivoCliente = String.format("cli%09d%s.txt",
					idVendedor, Utils.sdfNomeArquivoExportacao.format(data));

			FileOutputStream outCli = new FileOutputStream(new File(Environment
					.getExternalStorageDirectory().getPath() + "/sav/envia",
					nomeArquivoCliente));

			for (Cliente cliente : clientesExportar) {
				outCli.write(cliente.toLinhaTexto().getBytes());
			}
			outCli.flush();
			outCli.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Cliente> listaClientesNaoEnviado() {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<Cliente> listaClientes = new ArrayList<Cliente>();
		Cliente cliente = new Cliente();
		Cursor c = dbLocal.rawQuery(
				"select * from Cliente where Enviado = 1 and statusCm = 0 and IdCliente >= 1000000 or " +
						"Enviado = 0 and statusCm = 0 or " +
						"Enviado is null and statusCM is null " +
						"or Enviado = 1 and  statusCm = 3",
				null);

		try {
			while (c.moveToNext())
				listaClientes.add(preencherObjeto(c));
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("ClienteDAO:lista.", e.getMessage());
		} finally {
			c.close();

			//if (db == null)
				//dbLocal.close(); singletone();
		}

		return listaClientes;
	}

	public void apagarAposPrazo(short quantidadeDiasApagar) {

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		// database.execSQL("insert into exectable (first_column) values (?);",
		// new String[]{});

		String sql = " select IdCliente from Cliente "
				+ " where IdCliente in ( select p.IdCliente  "
				+ "					    from Pedido p  "
				+ "   				    where p.DataPedido <= ?   "
				+ "  					and p.Enviado = 1  ) and  Enviado = 1 and IdCliente >= 1000000 ";

		Calendar cal = Calendar.getInstance();

		cal.add(Calendar.DAY_OF_MONTH, quantidadeDiasApagar * -1);

		Date dataApagar = cal.getTime();

		Cursor c = db
				.rawQuery(sql,
						new String[] { Utils.sdfDataDb.format(dataApagar)
								+ " 00:00:00" });

		try {
			while (c.moveToNext()) {
				String[] param = new String[] { String.valueOf(c.getLong(0)) };

				this.removerContato(db, param);

				db.delete(TABELA, "IdCliente = ? ", param);
			}
		} catch (Exception es) {
			Log.e("Cliente:apagarAposPrazo", es.getMessage().toString());
		}

	}



	private void removerContato(SQLiteDatabase db, String[] param)
			throws Exception {

		db.delete("Contato", "IdCliente = ?", param);

	}

	public boolean clientePossuiPedido(long idCliente) {
		boolean retorno = true;
		SQLiteDatabase dbLocal = null;

		if (db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		Cursor c = null;
		try {
			c = dbLocal.rawQuery("SELECT * FROM Pedido WHERE IdCliente = ?",
					new String[] { String.valueOf(idCliente) });
			retorno = c.getCount() >= 1;

		} catch (Exception e) {
			Log.e("Cliente:statusEnviado.", e.getMessage());
		} finally {
			c.close();
			if (db == null) {
				//db.close(); singletone();
			}
		}
		return retorno;
	}

	public List<Cliente> filtroCliente(String texto, String cidade,
			OpcaoPesquisa param, boolean listaCompleta) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		List<Cliente> lista = new ArrayList<Cliente>();
		try {
			String searc = "";
			String[] arrayPesquisa = null;
			String[] arrayTemp = null;

			if (!listaCompleta) {
				if (!texto.trim().equals("")) {
					arrayTemp = texto.split(",");
					for (int i = 0; i < arrayTemp.length; i++) {
						arrayTemp[i] = "%%%" + arrayTemp[i] + "%%%";
						if (param == OpcaoPesquisa.FANTASIA)
							searc += String.format("c.Fantasia like ? ");
						else if (param == OpcaoPesquisa.RAZAO)
							searc += String.format("c.Razao like ? ");
						else if (param == OpcaoPesquisa.ENDERECO)
							searc += String.format("c.Endereco like ? ");

						if ((i + 1) < arrayTemp.length)
							searc += "and ";
					}

					if (cidade.trim().length() > 0) {
						arrayPesquisa = new String[arrayTemp.length + 1];
						for (int i = 0; i < arrayTemp.length; i++)
							arrayPesquisa[i] = arrayTemp[i];

						arrayPesquisa[arrayPesquisa.length - 1] = cidade;
					} else {
						arrayPesquisa = new String[arrayTemp.length];
						for (int i = 0; i < arrayTemp.length; i++)
							arrayPesquisa[i] = arrayTemp[i];
					}
				}
			}

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT c.IdCliente, c.Razao, c.Fantasia, c.CnpjCpf,c.Endereco, ");
			sql.append("(select count(p.IdPedido) from Pedido p WHERE p.IdCliente = c.IdCliente and p.DataPedido between strftime('%Y-%m-%d 00:00:00','now','localtime') and strftime('%Y-%m-%d 23:59:59','now','localtime')) as PedidoPendente ");
			sql.append("FROM Cliente c ");
			if (param == OpcaoPesquisa.CODIGO && !texto.equals("")) {
				sql.append(String.format("where c.IDCliente =?"));
				arrayPesquisa = new String[] { texto, "" };
			} else if ((param == OpcaoPesquisa.FANTASIA
					|| param == OpcaoPesquisa.RAZAO || param == OpcaoPesquisa.ENDERECO)
					&& !texto.equals("")) {
				sql.append("where " + searc);
			}

			if (cidade.toString().trim().length() > 0) {
				if (sql.toString().contains("where") && arrayPesquisa != null) {
					sql.append(" and c.Cidade =?");
					arrayPesquisa[arrayPesquisa.length - 1] = cidade;
				} else {
					arrayPesquisa = new String[] { cidade };
					sql.append(" where c.Cidade =?");
				}
			} else {
				if (param == OpcaoPesquisa.CODIGO && texto.trim().length() > 0)
					arrayPesquisa = new String[] { texto };
			}

			sql.append(" ORDER BY c.Razao");

			Cursor c = null;

			if (arrayPesquisa != null)
				c = db.rawQuery(sql.toString(), arrayPesquisa);
			else
				c = db.rawQuery(sql.toString(), null);

			try {
				while (c.moveToNext()) {
					Cliente cliente = new Cliente();
					cliente.setIdCliente(c.getLong(c
							.getColumnIndex("IdCliente")));
					cliente.setRazao(c.getString(c.getColumnIndex("Razao")));
					cliente.setFantasia(c.getString(c
							.getColumnIndex("Fantasia")));
					cliente.setCnpjCpf(c.getString(c.getColumnIndex("CnpjCpf")));
					cliente.setEndereco(c.getString(c
							.getColumnIndex("Endereco")));
					cliente.setPedidoPendente(c
							.getColumnIndex("PedidoPendente") > -1 ? c.getInt(c
							.getColumnIndex("PedidoPendente")) > 0 : false);

					lista.add(cliente);
				}
			} catch (Exception e) {
				Log.d("ClienteDAO.filtro", e.getMessage());
			} finally {
				if (c != null) {
					c.close();
				}
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			//db.close(); singleton();
		}
		return lista;
	}

	public String[] arrayCidades() {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

		Cursor c = db.rawQuery(
				"select distinct Cidade from Cliente ORDER BY Cidade", null);

		String[] Cidades = new String[c.getCount() + 1];

		Cidades[0] = "Todas as Cidades";

		int i = 1;

		while (c.moveToNext()) {
			Cidades[i] = c.getString(0);
			i++;
		}

		//db.close(); singleton();
		c.close();
		return Cidades;
	}

	public Cliente getIdRazao(long idCliente) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		Cursor c = null;
		Cliente cliente = null;
		try {

			c = dbLocal.rawQuery(
					"SELECT idCliente, Razao FROM Cliente WHERE IdCliente=?",
					new String[] { String.valueOf(idCliente) });

			if (c.moveToFirst()) {
				cliente = new Cliente();
				cliente.setIdCliente(c.getLong(0));
				cliente.setRazao(c.getString(1));
			}

		} catch (Exception e) {
			Log.e("ClienteDAO:getRazao.", e.getMessage());
		} finally {
			c.close();
			//if (db == null)
				//dbLocal.close(); singletone();
		}

		return cliente;
	}

	public Cliente get(long idCliente) throws ParseException {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db;

		Cliente cliente = null;

		Cursor c = dbLocal.query(TABELA, null, "IdCliente=?",
				new String[] { String.valueOf(idCliente) }, null, null, null);
		try {
			if (c.moveToFirst()) {
				cliente = preencherObjeto(c);
			}
		} finally {
			c.close();
			//if (db == null)
				//dbLocal.close(); singletone();
		}

		return cliente;
	}

	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			//db.beginTransaction();
			if (cargaCompleta)
				deleteAll();
			getImportacaoCliente(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("ClienteDAO", e.getMessage());
		}
	}

	private void getImportacaoCliente(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			db.beginTransaction();
			for(String linha:linhasTexto){
				if (getLinhaClienteImportar(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
//			} catch (Exception e) {
//				Log.d("ClienteDAO", e.getMessage());
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaClienteImportar(Boolean cargaCompleta, String linha) {
		Cliente c = new Cliente();
		//Obs: colocar no campo, quantas casas irá aumentar, exemplo campo NUMERO aumentou 1 casa.
		int aumentar = 0;
		if (linha.length() > 384){
			aumentar = 1;
		}

		// Tratamento para arquivos em formato diferente, e gera��o
		// de Logs, p/ os mesmos .

		if (linha.substring(0, 8).trim().matches((MATCHES))
				&& !linha.substring(0, 8).trim().equals(""))
			c.setIdCliente(Long.parseLong(linha.substring(0, 8)
					.trim()));
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}
		c.setRazao(linha.substring(8, 58).trim());
		c.setFantasia(linha.substring(58, 88).trim());

		if (linha.substring(88, 92).trim().equals(""))
			c.setRamo((short) 0);
		else if (linha.substring(88, 92).trim().matches(MATCHES))
			c.setRamo(Short.parseShort(linha.substring(88, 92)
					.trim()));
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}
		c.setTipoEndereco(linha.substring(92, 112).trim());
		c.setEndereco(linha.substring(112, 172).trim());
		c.setComplemento(linha.substring(172, 232).trim());
		c.setBairro(linha.substring(232, 252).trim());
		c.setCidade(linha.substring(252, 282).trim());
		c.setUF(linha.substring(282, 284).trim());

		if (linha.substring(284, 292).trim().equals(""))
			c.setCEP("");
		else if (linha.substring(284, 292).trim().matches(MATCHES))
			c.setCEP(linha.substring(284, 292).trim());
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}
		if (linha.substring(292, 294).trim().equals(""))
			c.setDDD((short) 0);
		else if (linha.substring(292, 294).trim().matches(MATCHES))
			c.setDDD(Short.parseShort(linha.substring(292, 294)
					.trim()));
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}

		if (linha.substring(294, 302 + aumentar).trim().equals(""))
			c.setTelefone(0);
		else if (linha.substring(294, 302+ aumentar).trim().matches(MATCHES))
			c.setTelefone(Long.parseLong(linha.substring(294, (302 + aumentar))
					.trim()));
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}
		if (linha.substring(302 + aumentar, 303 + aumentar).trim().equals(""))
			c.setTipoPessoa((short) 0);
		else if (linha.substring(302 + aumentar, 303 + aumentar).trim().matches(MATCHES))
			c.setTipoPessoa(Short.parseShort(linha.substring(302 + aumentar,
					303 + aumentar).trim()));
		else {
			Utils.gravarLog("\nCLI: " + linha);
			return true;
		}
		c.setCnpjCpf((linha.substring(303 + aumentar, 317 + aumentar).trim()));
		c.setIeRg(linha.substring(317 + aumentar, 333 + aumentar).trim());
		c.setStatusBloqueio(linha.substring(333 + aumentar, 334 + aumentar));
		c.setZona(linha.substring(334 + aumentar, 337 + aumentar).trim());
		c.setSetor(linha.substring(337 + aumentar, 340 + aumentar).trim());
		c.setRoteiro(linha.substring(340 + aumentar, 343 + aumentar).trim());
		c.setEmail(linha.substring(343 + aumentar, 383 + aumentar).trim());
		c.setDataEnvio(new Date());
		c.setEnviado(true);
		c.setStatusCm(1);
		c.setCodigoContribuinte(Integer.parseInt(linha.substring(383 + aumentar, 384 + aumentar).trim()));

		if (!cargaCompleta) {
			switch (c.getStatusBloqueio().toUpperCase().charAt(0)) {
			case ' ':
			case 'A':
				if (exists(c))
					update(c);
				else
					insert(c);
				break;

			case 'E':
				delete(c);
				break;

			default:
				if (!exists(c))
					insert(c);
				else
					update(c);
				break;
			}
		} else {
			if (!exists(c))
				insert(c);
			else
				update(c);
		}
		return false;
	}

	public List<Long> listaIdClientes() {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<Long> lista = new ArrayList<Long>();

		Cursor c = dbLocal.rawQuery("SELECT (IdCliente) FROM Cliente", null);

		try {
			while (c.moveToNext())
				lista.add(c.getLong(0));
		} catch (Exception e) {
			Log.e("ClienteDAO:l.", e.getMessage());
		} finally {
			c.close();

			//if (db == null)
				//dbLocal.close(); singletone();
		}

		return lista;
	}

	public List<ClienteSemCompra> listaSemCompra(int qtdeDias)
			throws ParseException {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		List<ClienteSemCompra> listaSemCompra = new ArrayList<ClienteSemCompra>();

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.set(Calendar.DATE, gc.get(Calendar.DATE) - qtdeDias);
		Date dataApurar = gc.getTime();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT cli.idCliente, cli.Razao, ( \n");
		sql.append("  SELECT ped.DataPedido FROM Pedido ped \n");
		sql.append("  WHERE ped.IdCliente = cli.IdCliente \n");
		sql.append("  ORDER BY ped.DataPedido desc limit 1 \n");
		sql.append(") DataUltimo, ( \n");
		sql.append("  SELECT Ult.Data FROM Ultima Ult \n");
		sql.append("  WHERE Ult.IdCliente = cli.IdCliente \n");
		sql.append("  ORDER BY Ult.Data desc limit 1 \n");
		sql.append(") DataUltimaCompra \n");
		sql.append(" FROM cliente cli \n");
		sql.append(" WHERE cli.idcliente not in \n");
		sql.append("( \n");
		sql.append(" SELECT ped1.idcliente FROM pedido ped1 \n");
		sql.append(" WHERE ped1.idcliente = cli.idcliente and \n");
		sql.append(String.format("   ped1.datapedido >= \'%s 00:00:00\' \n",
				Utils.sdfDataDb.format(dataApurar)));
		sql.append(") \n");
		sql.append(" AND \n");
		sql.append("cli.idcliente not in \n");
		sql.append("( \n");
		sql.append("SELECT ult1.IdCliente FROM ultima ult1 \n");
		sql.append("WHERE ult1.IdCliente = cli.IdCliente AND \n");
		sql.append(String.format("  ult1.data >= \'%s 00:00:00\' \n",
				Utils.sdfDataDb.format(dataApurar)));
		sql.append(") ORDER BY cli.razao \n");

		Cursor c = db.rawQuery(sql.toString(), null);

		ClienteSemCompra clienteSemCompra = null;

		try {
			while (c.moveToNext()) {

				Date dataPedido, dataUltima;
				dataPedido = new Date();
				dataUltima = new Date();
				long qtdeDiasPedido = -1, qtdeDiasUltima = -1;

				if (c.getString(c.getColumnIndex("DataUltimo")) != null) {
					dataPedido = Utils.sdfDataDb.parse(c.getString(c
							.getColumnIndex("DataUltimo")));
					qtdeDiasPedido = Utils.qtdeDiasPassou(dataPedido);
				}
				if (c.getString(c.getColumnIndex("DataUltimaCompra")) != null) {
					dataUltima = Utils.sdfDataDb.parse(c.getString(c
							.getColumnIndex("DataUltimaCompra")));
					qtdeDiasUltima = Utils.qtdeDiasPassou(dataUltima);
				}

				clienteSemCompra = new ClienteSemCompra();

				clienteSemCompra.setIdCliente(c.getLong(c
						.getColumnIndex("IdCliente")));
				clienteSemCompra
						.setRazao(c.getString(c.getColumnIndex("Razao")));

				if (((qtdeDiasPedido == -1) && (qtdeDiasUltima == -1))
						|| ((qtdeDiasPedido != -1) && (qtdeDiasUltima != -1))) {
					clienteSemCompra.setUltimaCompra(dataPedido);
					clienteSemCompra.setQtdeDias((int) qtdeDiasPedido);

				} else if ((qtdeDiasPedido == -1) && (qtdeDiasUltima != -1)) {
					clienteSemCompra.setUltimaCompra(dataUltima);
					clienteSemCompra.setQtdeDias((int) qtdeDiasUltima);

				} else if ((qtdeDiasPedido != -1) && (qtdeDiasUltima != -1)) {
					if (qtdeDiasPedido < qtdeDiasUltima)
						clienteSemCompra.setUltimaCompra(dataPedido);
					else
						clienteSemCompra.setUltimaCompra(dataUltima);

					if (qtdeDiasPedido < qtdeDiasUltima)
						clienteSemCompra.setQtdeDias((int) qtdeDiasPedido);
					else
						clienteSemCompra.setQtdeDias((int) qtdeDiasUltima);
				}
				listaSemCompra.add(clienteSemCompra);
			}
		} finally {
			c.close();
			//db.close(); singleton();
		}

		return listaSemCompra;
	}

	public List<ClienteSemVisita> listaSemVisita(int qtdeDias)
			throws ParseException {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		List<ClienteSemVisita> listaSemVisita = new ArrayList<ClienteSemVisita>();

		try {
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(new Date());
			gc.set(Calendar.DATE, gc.get(Calendar.DATE) - qtdeDias);
			Date dataApurar = gc.getTime();

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT cli.idcliente, cli.Razao, ( \n");
			sql.append(" SELECT ped.DataPedido FROM Pedido ped \n");
			sql.append(" WHERE ped.IdCliente = cli.IdCliente \n");
			sql.append(" ORDER BY ped.DataPedido desc limit 1 \n ");
			sql.append(") DataUltimo, ( \n");
			sql.append(" SELECT Ult.Data FROM Ultima Ult \n");
			sql.append(" WHERE Ult.IdCliente = cli.IdCliente \n");
			sql.append(" ORDER BY Ult.Data desc limit 1 \n");
			sql.append(") DataUltimaCompra, ( \n");
			sql.append(" SELECT Nao.DataHora FROM NaoVendaMovimento Nao \n");
			sql.append(" WHERE Nao.IdCLiente = cli.IdCliente \n");
			sql.append(" ORDER BY Nao.DataHora desc limit 1 \n");
			sql.append(") DataUltimaNaoVenda \n");
			sql.append("FROM cliente cli \n");
			sql.append(" WHERE cli.idcliente not in \n");
			sql.append("( \n");
			sql.append(" SELECT ped1.idcliente FROM Pedido ped1 \n");
			sql.append(" WHERE ped1.idcliente = cli.idcliente AND \n");
			sql.append(String.format("  ped1.datapedido >= \'%s 00:00:00\' \n",
					Utils.sdfDataDb.format(dataApurar)));
			sql.append(") \n");
			sql.append("AND \n");
			sql.append("cli.idcliente not in \n");
			sql.append("( \n");
			sql.append("SELECT ult1.idcliente FROM ultima ult1 \n");
			sql.append(" WHERE ult1.idcliente = cli.idcliente AND \n");
			sql.append(String.format("  ult1.data >= \'%s 00:00:00\' \n",
					Utils.sdfDataDb.format(dataApurar)));
			sql.append(") \n");
			sql.append("AND \n");
			sql.append("cli.idcliente not in \n");
			sql.append("( \n");
			sql.append("SELECT Nao1.idCliente FROM NaoVendaMovimento Nao1 \n");
			sql.append("WHERE Nao1.idcliente = cli.idcliente and \n");
			sql.append(String.format("   Nao1.datahora >= \'%s 00:00:00\' \n",
					Utils.sdfDataDb.format(dataApurar)));
			sql.append(") \n");
			sql.append("ORDER BY cli.razao \n");

			Cursor c = db.rawQuery(sql.toString(), null);

			ClienteSemVisita clienteSemVisita = null;

			try {
				while (c.moveToNext()) {

					Date dataPedido, dataUltima, dataNao;
					dataPedido = new Date();
					dataUltima = new Date();
					dataNao = new Date();

					long qtdeDiasPedido = -1;
					long qtdeDiasUltima = -1;
					long qtdeDiasNao = -1;

					if (c.getString(c.getColumnIndex("DataUltimo")) != null) {
						dataPedido = Utils.sdfDataDb.parse(c.getString(c
								.getColumnIndex("DataUltimo")));
						qtdeDiasPedido = Utils.qtdeDiasPassou(dataPedido);
					}
					if (c.getString(c.getColumnIndex("DataUltimaCompra")) != null) {
						dataUltima = Utils.sdfDataDb.parse(c.getString(c
								.getColumnIndex("DataUltimaCompra")));
						qtdeDiasUltima = Utils.qtdeDiasPassou(dataUltima);
					}
					if (c.getString(c.getColumnIndex("DataUltimaCompra")) != null) {
						dataNao = Utils.sdfDataDb.parse(c.getString(c
								.getColumnIndex("DataUltimaCompra")));
						qtdeDiasUltima = Utils.qtdeDiasPassou(dataUltima);
					}

					clienteSemVisita = new ClienteSemVisita();
					clienteSemVisita.setIdCliente(c.getLong(c
							.getColumnIndex("IdCliente")));
					clienteSemVisita.setRazao(c.getString(c
							.getColumnIndex("Razao")));

					if (((qtdeDiasPedido == -1) && (qtdeDiasUltima == -1) && (qtdeDiasNao == -1))
							|| ((qtdeDiasPedido != -1)
									&& (qtdeDiasUltima == -1) && qtdeDiasNao == -1)) {
						clienteSemVisita.setUltimaVisita(dataPedido);
						clienteSemVisita.setQtdeDias((int) qtdeDiasPedido);
					} else if ((qtdeDiasPedido == -1) && (qtdeDiasUltima != -1)
							&& (qtdeDiasNao == -1)) {
						clienteSemVisita.setUltimaVisita(dataUltima);
						clienteSemVisita.setQtdeDias((int) qtdeDiasUltima);
					} else if ((qtdeDiasPedido == -1) && (qtdeDiasUltima == -1)
							&& (qtdeDiasNao == -1)) {
						clienteSemVisita.setUltimaVisita(dataNao);
						clienteSemVisita.setQtdeDias((int) qtdeDiasNao);
					} else if ((qtdeDiasPedido != -1) && (qtdeDiasUltima != -1)
							&& (qtdeDiasNao != -1)) {
						if ((qtdeDiasPedido <= qtdeDiasUltima)
								&& (qtdeDiasPedido <= qtdeDiasNao)) {
							clienteSemVisita.setUltimaVisita(dataPedido);
							clienteSemVisita.setQtdeDias((int) qtdeDiasPedido);
						} else if ((qtdeDiasUltima <= qtdeDiasPedido)
								&& (qtdeDiasUltima <= qtdeDiasNao)) {
							clienteSemVisita.setUltimaVisita(dataUltima);
							clienteSemVisita.setQtdeDias((int) qtdeDiasUltima);
						} else if ((qtdeDiasNao <= qtdeDiasPedido)
								&& (qtdeDiasNao <= qtdeDiasUltima)) {
							clienteSemVisita.setUltimaVisita(dataNao);
							clienteSemVisita.setQtdeDias((int) qtdeDiasNao);
						}
					}
					listaSemVisita.add(clienteSemVisita);
				}
			} finally {
				c.close();
			}
		} finally {
			//db.close(); singleton();
		}
		return listaSemVisita;
	}

	public int gerarIdCliente() {
		SQLiteDatabase dbLocal;

		int idCliente = 0;
		// String selectMaiorIdCliente =
		// " SELECT IdCliente FROM Cliente WHERE IdCliente = (SELECT MAX(IdCliente) FROM Cliente)";
		String selectMaiorIdCliente = " select ifnull( max(IdCliente) , 1000000  ) as IdCliente from cliente    ";

		if (db == null) {
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		} else {
			dbLocal = db;
		}

		Cursor cursor = dbLocal.rawQuery(selectMaiorIdCliente, null);
		cursor.moveToFirst();

		idCliente = cursor.getInt(cursor.getColumnIndex("IdCliente"));

		cursor.close();

		if (idCliente != 0) {
			if (idCliente < VALOR_ID_CLIENTE_NOVO) {
				idCliente = VALOR_ID_CLIENTE_NOVO;
			} else {
				idCliente++;
			}
		} else {
			idCliente = 0;
		}

		return idCliente;
	}

	public String getSaldoEstoque() {
		SQLiteDatabase dbLocal;

		String saldoEstoque = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		// Agora alterado o campo SaldoEstoque para string, n�o faremos mais o
		// calculo.
		// Cursor c =
		// dbLocal.rawQuery("SELECT sum(pro.SaldoEstoque*pre.PrecoNormal) from Produto pro inner join Preco pre on (pro.idProduto=pre.idProduto) ",
		// null);
		Cursor c = dbLocal
				.rawQuery(
						"SELECT pro.SaldoEstoque from Produto pro inner join Preco pre on (pro.idProduto=pre.idProduto) ",
						null);

		saldoEstoque += c;

		c.close();

		return saldoEstoque;
	}

	public boolean isCPFCNPJExistente(String cpfCNPJ) {

		SQLiteDatabase dbLocal = null;

		String sql = "select * from Cliente where CnpjCpf = ? ";

		if (db == null) {
			dbLocal = new DBHelper(ctx).getReadableDatabase();

		} else {
			dbLocal = db;
		}

		Cursor c = null;

		try {

			c = dbLocal.rawQuery(sql, new String[] { cpfCNPJ });

			return c.getCount() > 0;

		} catch (Exception e) {

			Log.e("Cliente:StatusEnviado.", e.getMessage());

			return true;
		} finally {

			c.close();
			//if (db == null) {
				//dbLocal.close(); singletone();
	//		}
		}

	}

	private Cliente preencherObjeto(Cursor c) throws ParseException {
		Cliente cliente = new Cliente();
		cliente.setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
		cliente.setRazao(c.getString(c.getColumnIndex("Razao")));
		cliente.setFantasia(c.getString(c.getColumnIndex("Fantasia")));
		cliente.setRamo(c.getShort(c.getColumnIndex("Ramo")));
		cliente.setTipoEndereco(c.getString(c.getColumnIndex("TipoEndereco")));
		cliente.setEndereco(c.getString(c.getColumnIndex("Endereco")));
		cliente.setComplemento(c.getString(c.getColumnIndex("Complemento")));
		cliente.setBairro(c.getString(c.getColumnIndex("Bairro")));
		cliente.setCidade(c.getString(c.getColumnIndex("Cidade")));
		cliente.setUF(c.getString(c.getColumnIndex("UF")));
		cliente.setCEP(c.getString(c.getColumnIndex("CEP")));
		cliente.setDDD(c.getShort(c.getColumnIndex("DDD")));
		cliente.setTelefone(c.getLong(c.getColumnIndex("Telefone")));
		cliente.setTipoPessoa(c.getShort(c.getColumnIndex("TipoPessoa")));
		cliente.setCnpjCpf(c.getString(c.getColumnIndex("CnpjCpf")));
		cliente.setIeRg(c.getString(c.getColumnIndex("IeRg")));
		cliente.setZona(c.getString(c.getColumnIndex("Zona")));
		cliente.setSetor(c.getString(c.getColumnIndex("Setor")));
		cliente.setRoteiro(c.getString(c.getColumnIndex("Roteiro")));
		cliente.setEmail(c.getString(c.getColumnIndex("Email")));
		cliente.setPedidoPendente(c.getInt(c.getColumnIndex("PedidoPendente")) > 0);
		cliente.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
		cliente.setDataEnvio(Utils.sdfDataHoraDb.parse(c.getString(c
				.getColumnIndex("DataEnvio"))));
		cliente.setVisivel(c.getInt(c.getColumnIndex("Visivel")) > 0);
		cliente.setStatusBloqueio(c.getString(c
				.getColumnIndex("StatusBloqueio")));
		cliente.setCodRepresentante(c.getLong(c
				.getColumnIndex("CodRepresentante")));

		cliente.setCodigoContribuinte(c.getInt(c.getColumnIndex("CodigoContribuinte")));
		cliente.setNumero(c.getInt(c.getColumnIndex("Numero")));
		cliente.setStatusCm(c.getInt(c.getColumnIndex("StatusCm")));

		return cliente;
	}

	public boolean atualizarIdRepresentante ( String IdCliente , String IdVendedor ) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = "UPDATE Cliente SET CodRepresentante = " + IdVendedor + " where IdCliente =  " + IdCliente ;
		db.execSQL(sql);
		//db.close(); singleton();
		return true ; 
	}

	public void atualizarRetornoClienteCM(MensagemClienteMQTTDTO resultado) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		int statusGWeb = StatusCentralManager.ERROR.getCodigo();

		int id_cliente = 0;
		if (resultado != null){
			id_cliente = Integer.valueOf(resultado.id_cliente);
			try {
				statusGWeb = StatusCentralManager.getStatusBy(resultado.status).getCodigo();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		String sql = String.format("UPDATE Cliente set StatusCm = %d where IdCliente = %d", statusGWeb,  id_cliente);
		try {
			db.execSQL(sql);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}

		db.close();
	}

	@Override
	public String getTabela() { return TABELA; }

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaClienteImportar(true, linha)) return true;
		
		return false;
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}

	@Override
	public String getMensagemTabela() { return ctx.getString(R.string.importacao_tabela_clientes); }
}
