package br.net.sav.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.Pedido;

import static br.net.sav.modelo.ComboXCliente.ID_CLIENTE;

public class ComboDAO {
    private Context context;


    public ComboDAO(Context context) {
        this.context = context;
    }

    private void fecharBancoLocal(SQLiteDatabase db, SQLiteDatabase dbLocal) {
        if(db==null){
            dbLocal.close();
        }
    }

    private SQLiteDatabase instanciarBancoLocal(SQLiteDatabase db) {
        SQLiteDatabase dbLocal;
        if(db==null){
            dbLocal = new DBHelper(context).getWritableDatabase();
        }else{
            dbLocal = db;
        }
        return dbLocal;
    }

    public List<Combo> buscarCombosPorPedido(Pedido pedido, SQLiteDatabase db){
        SQLiteDatabase dbLocal = null;
        dbLocal = instanciarBancoLocal(db);

        String sql = "select * from combo left join comboxpedido on combo.IdCombo = comboxPedido.IdCombo where idpedido=? AND "+ID_CLIENTE+"=?";
        Cursor cursor = dbLocal.rawQuery(sql, new String[]{String.valueOf(pedido.getIDPedido()), String.valueOf(pedido.getIDCliente())});
        ArrayList<Combo> combos = new ArrayList<Combo>();
        while (cursor.moveToNext()){
            Combo combo = new Combo(cursor);
            combos.add(combo);
        }
        //fecharBancoLocal(db, dbLocal);
        return combos;
    }

    public List<Combo> buscarCombosValidos(SQLiteDatabase db){
        SQLiteDatabase dbLocal = null;
        dbLocal = instanciarBancoLocal(db);

        Date date = new Date();
        String dataAtual = Utils.sdfDataDb.format(date);
        Cursor cursor = dbLocal.query(Combo.TABELA, Combo.campos, "? between "+Combo.PERIODO_INICIAL+" and "+Combo.PERIODO_FINAL, new String[]{dataAtual}, null, null, null);
        ArrayList<Combo> combos = new ArrayList<Combo>();
        while (cursor.moveToNext()){
            Combo combo = new Combo(cursor);
            if (combo.getPeriodoInicial().before(date))
                combos.add(combo);
        }
        //fecharBancoLocal(db, dbLocal);
        return combos;
    }

    public boolean insert(SQLiteDatabase db, Combo combo){
        SQLiteDatabase dbLocal = null;
        dbLocal = instanciarBancoLocal(db);
        ContentValues contentValues = combo.toContentValues();
        long insert = dbLocal.insert(Combo.TABELA, null, contentValues);
       // fecharBancoLocal(db,dbLocal);
        return insert>0;
    }


    private void deleteAll() {
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        db.delete(Combo.TABELA,null,null);
       // db.close();
    }

    public boolean existeCombo(Combo combo) {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        boolean retorno = false;
        String idCombo = String.valueOf(combo.getIdCombo());

        Cursor c = db.rawQuery("Select Count(*) from Combo where IdCombo=?", new String[]{idCombo});
        if (c.moveToFirst())
            retorno = c.getInt(0) > 0;

        c.close();
        //db.close(); singleton();

        return retorno;
    }

    public String existeDataUtimaAtualizacaoCombo() {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        String retorno = null;

        Cursor c = db.rawQuery("Select distinct max(DataAtualizacao) from Combo", null);
        if (c.moveToFirst())
            retorno = c.getString(0);

        c.close();
        //db.close(); singleton();

        return retorno;
    }

    public boolean update(Combo combo, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(context).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.update(Combo.TABELA, combo.toContentValues(),"IdCombo=?", new String[]{String.valueOf(combo.getIdCombo())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }
    public boolean delete(Combo combo, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(context).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.delete(Combo.TABELA, "IdCombo=?", new String[]{String.valueOf(combo.getIdCombo())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }
}
