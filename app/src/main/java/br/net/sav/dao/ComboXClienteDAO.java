package br.net.sav.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.modelo.ComboXCliente;
import static br.net.sav.modelo.ComboXCliente.ID_CLIENTE;
import static br.net.sav.modelo.ComboXCliente.ID_COMBO;
import static br.net.sav.modelo.ComboXCliente.TABELA;
import static br.net.sav.modelo.ComboXCliente.colunas;

public class ComboXClienteDAO {
    private Context context;


    public ComboXClienteDAO(Context context) {
        this.context = context;
    }

    public ComboXCliente buscarComboXclientePorId(long idCliente, long idCombo){
        return buscarComboPor(ID_CLIENTE + "=? and "+ ID_COMBO+"=?", new String[]{String.valueOf(idCliente),String.valueOf(idCombo)});
    }

    public List<ComboXCliente> buscarPorIdClienteCombo(long idCliente, long idCombo) {
        return buscarPor(ID_CLIENTE + "=? and "+ ID_COMBO+"=?", new String[]{String.valueOf(idCliente),String.valueOf(idCombo)});
    }

    public ComboXCliente buscarPorIdCombo(long idCombo){
        return buscarComboPor(ID_COMBO + "=?", new String[]{String.valueOf(idCombo)});
    }

    private List<ComboXCliente> buscarPor(String selection, String[] arguments) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        Cursor cursor = database.query(TABELA, colunas, selection, arguments, null, null, null, null);
        List<ComboXCliente> listaComboXPedido = gerarListaComboXClientePor(cursor);
        database.close();
        return listaComboXPedido;
    }

    private ComboXCliente buscarComboPor(String selection, String[] arguments) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        Cursor cursor = database.query(TABELA, colunas, selection, arguments, null, null, null, null);
        ComboXCliente comboXPedido = gerarComboXClientePor(cursor);
        database.close();
        return comboXPedido;
    }

    private List<ComboXCliente> gerarListaComboXClientePor(Cursor cursor) {
        List<ComboXCliente> comboXClientes = new ArrayList<ComboXCliente>();
        while (cursor.moveToNext()) {
            ComboXCliente comboXCliente = new ComboXCliente(cursor);
            comboXClientes.add(comboXCliente);
        }
        return comboXClientes;
    }

    private ComboXCliente gerarComboXClientePor(Cursor cursor) {
        ComboXCliente comboXCliente = null;
        if (cursor.moveToNext()) {
            comboXCliente = new ComboXCliente(cursor);
        }
        return comboXCliente;
    }

    public boolean inserir(ComboXCliente comboXCliente){
        return insert(null,comboXCliente);
    }

    public boolean insert(SQLiteDatabase db, ComboXCliente comboXCliente){
        SQLiteDatabase localdb = null;
        localdb = instanciarBancoLocal(db);
        ContentValues contentValues = comboXCliente.toContentValues();
        long retorno = localdb.insert(ComboXCliente.TABELA, null, contentValues);
        fecharBancoLocal(db,localdb);
        return retorno > 0;
    }

    private void fecharBancoLocal(SQLiteDatabase db, SQLiteDatabase dbLocal) {
        if(db==null){
            dbLocal.close();
        }
    }
    private SQLiteDatabase instanciarBancoLocal(SQLiteDatabase db) {
        SQLiteDatabase dbLocal;
        if(db==null){
            dbLocal = new DBHelper(context).getWritableDatabase();
        }else{
            dbLocal = db;
        }
        return dbLocal;
    }
    private void deleteAll() {
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        db.delete(ComboXCliente.TABELA,null,null);
        db.close();
    }
}
