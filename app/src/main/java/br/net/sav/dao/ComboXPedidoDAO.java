package br.net.sav.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static br.net.sav.Utils.sdfDataDb;
import static br.net.sav.comboControllerSav.ModeloDTO.ComboXPedido.DATA_PEDIDO;
import static br.net.sav.modelo.ComboXPedido.ID_CLIENTE;
import static br.net.sav.modelo.ComboXPedido.ID_COMBO;
import static br.net.sav.modelo.ComboXPedido.ID_PEDIDO;
import static br.net.sav.modelo.ComboXPedido.TABELA;
import static br.net.sav.modelo.ComboXPedido.colunas;

import br.net.sav.Utils;
import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ComboXPedido;
import br.net.sav.modelo.Pedido;

public class ComboXPedidoDAO {
    private Context context;


    public ComboXPedidoDAO(Context context) {
        this.context = context;
    }


    public List<ComboXPedido> buscarPorIdCliente(long idCliente){
        return buscarPor(ID_CLIENTE + "=?", new String[]{String.valueOf(idCliente)});
    }

    public List<ComboXPedido> buscarTodosCombos(){
        return buscarPor(null,null);
    }

    private List<ComboXPedido> buscarTodosNaoEnviado() {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        String sql = "SELECT * FROM "+TABELA+" WHERE "+ID_PEDIDO+" " +
                "IN (SELECT P.IDPedido FROM PEDIDO P WHERE p.Enviado = 0 and p.aberto = 0) ";
        Cursor cursor = database.rawQuery(sql,null);
        List<ComboXPedido> listaComboXPedido = gerarListaComboXPedidoPor(cursor);
        database.close();
        return listaComboXPedido;
    }

    private List<ComboXPedido> gerarListaComboXPedidoPor(Cursor cursor) {
        List<ComboXPedido> listaComboXPedido = new ArrayList<>();
        while (cursor.moveToNext()) {
            ComboXPedido comboXPedido = new ComboXPedido(cursor);
            listaComboXPedido.add(comboXPedido);
        }
        return listaComboXPedido;
    }

    private List<ComboXPedido> buscarPor(String selection, String[] arguments) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();
        Cursor cursor = database.query(TABELA, colunas, selection, arguments, null, null, null, null);
        List<ComboXPedido> listaComboXPedido = gerarListaComboXPedidoPor(cursor);
      //  database.close();
        return listaComboXPedido;
    }


    public boolean insert(ComboXPedido comboXPedido){
        SQLiteDatabase database = new DBHelper(context).getWritableDatabase();
        ContentValues contentValues = comboXPedido.toContentValues();
        long retorno = database.insert(TABELA, null, contentValues);
      //  database.close();
        return retorno > 0;
    }

    public boolean existeComboXPedido(ComboXPedido comboXPedido) {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        boolean retorno = false;

        Cursor c = db.query(TABELA,colunas, ID_COMBO+"=? AND "+ID_PEDIDO+"=? AND "+ID_CLIENTE+"=? AND "+DATA_PEDIDO+"=?", new String[]{String.valueOf(comboXPedido.getIdCombo()), String.valueOf(comboXPedido.getIdPedido()),String.valueOf(comboXPedido.getIdCliente()), Utils.sdfDataDb.format(comboXPedido.getDataPedido())}, null,null, null);
        if (c.moveToFirst())
            retorno = c.getInt(0) > 0;

        c.close();
        //db.close(); singleton();

        return retorno;
    }

    public boolean deletePorIdPedido(long idCombo, long idPedido, long idCliente, Date dataPedido){
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        long retorno = db.delete(TABELA,ID_COMBO+"=? AND "+ID_PEDIDO+"=? AND "+ID_CLIENTE+"=? AND "+DATA_PEDIDO+"=?", new String[]{String.valueOf(idCombo), String.valueOf(idPedido),String.valueOf(idCliente), Utils.sdfDataDb.format(dataPedido)});
  //      db.close();
        return retorno > 0;
    }

    public boolean deletePorIds(Pedido pedido){
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        long retorno = db.delete(TABELA,ID_PEDIDO+"=? AND "+ID_CLIENTE+"=? AND "+DATA_PEDIDO+"=?", new String[]{ String.valueOf(pedido.getIDPedido()),String.valueOf(pedido.getIDCliente()), Utils.sdfDataDb.format(pedido.getDataPedido())});
        //      db.close();
        return retorno > 0;
    }

    public boolean update(ComboXPedido combo, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(context).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.update(TABELA, combo.toContentValues(),ID_COMBO+"=? AND "+ID_PEDIDO+"=? AND "+ID_CLIENTE+"=?AND "+DATA_PEDIDO+"=?",
                new String[]{String.valueOf(combo.getIdCombo()), String.valueOf(combo.getIdPedido()), String.valueOf(combo.getIdCliente()), Utils.sdfDataDb.format(combo.getDataPedido())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public void apagarAposPrazo(short quantidadeDiasGuardarPedido) {
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, quantidadeDiasGuardarPedido * -1);
            Date dataApagar = cal.getTime();

            SimpleDateFormat sdfDataHora = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            String sql = String.format("DELETE FROM %s WHERE IdPedido IN (SELECT IdPedido FROM %s WHERE Enviado = 1 and DataPedido <= \'%s\')", TABELA, "Pedido", sdfDataHora.format(dataApagar));
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName() + ".apagarAposPrazo()", e.getMessage());
        } finally {
   //         db.close();
        }
    }
    public List<ComboXPedido> buscarPorIdClienteComboNoMes(long idCliente, long idCombo) {
        SQLiteDatabase database = new DBHelper(context).getReadableDatabase();

        Calendar primeiroDia = Calendar.getInstance();
        primeiroDia.set(Calendar.DAY_OF_MONTH,1);


        Calendar diaAtual = Calendar.getInstance();
        diaAtual.set(Calendar.DAY_OF_MONTH, diaAtual.get(Calendar.DAY_OF_MONTH)+1);


        String sql = "SELECT * FROM "+TABELA+
                " left join PEDIDO " +
                " on pedido.IdPedidoVinculado = ComboXPedido.IdPedido "+
                "where Pedido.IdCliente=? " +
                "and "+ID_COMBO+"=? " +
                "and Pedido.DataPedido between ? and ? and Pedido.Combo=1";

        String[] ars = {String.valueOf(idCliente),
                String.valueOf(idCombo),
                sdfDataDb.format(primeiroDia.getTime()),
                sdfDataDb.format(diaAtual.getTime())};

        Cursor cursor = database.rawQuery(sql,ars );
        List<ComboXPedido> listaComboXPedido = gerarListaComboXPedidoPor(cursor);
   //     database.close();
        return listaComboXPedido;
    }
}
