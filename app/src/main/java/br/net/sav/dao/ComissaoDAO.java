package br.net.sav.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Comissao;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ComissaoDAO implements AcaoParaImportacaoDados {

    private static final String MATCHES = "^[0-9]*";

    private Context ctx;

    private static String TABELA = "Comissao";

    private static String[] colunas = new String[]{"Tipo", "Periodo", "TotalVendas", "TotalComissao"};

    public ComissaoDAO(Context ctx) {
        this.ctx = ctx;
    }

    public Boolean insert(Comissao comissao, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        ContentValues ctv = new ContentValues();

        ctv.put("Tipo", comissao.getTipo());
        ctv.put("Periodo", comissao.getPeriodo());
        ctv.put("TotalVendas", comissao.getTotalVendas());
        ctv.put("TotalComissao", comissao.getTotalComissao());

        boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0;

        //if(db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public Comissao getTipo (short Tipo) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        String[] param = new String[] {String.valueOf(Tipo)} ;

        Comissao comissao = new Comissao();

        Cursor c = db.query(TABELA, colunas, "Tipo=? ", param, null, null, null);
        try {
            if(c.moveToFirst()) {
                comissao.setTipo(c.getShort(c.getColumnIndex("Tipo")));
                comissao.setPeriodo(c.getString(c.getColumnIndex("Periodo")));
                comissao.setTotalVendas(c.getDouble(c.getColumnIndex("TotalVendas")));
                comissao.setTotalComissao(c.getDouble(c.getColumnIndex("TotalComissao")));
            }
        } catch (Exception e) {
            Log.e("ComissaoDAO:get(tipo,Periodo).",e.getMessage());
        } finally {
            c.close();
            //db.close(); singleton();
        }

        return comissao ;
    }

    public Boolean delete(Comissao comissao, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        String[] param = new String[]{String.valueOf(comissao.getTipo()), comissao.getPeriodo()};

        boolean retorno = dbLocal.delete(TABELA, "Tipo=?,Periodo=?", param) > 0;

        return retorno;
    }

    public Boolean deleteAll() {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        boolean retorno = db.delete(TABELA, null, null) > 0;

        //db.close(); singleton() ;

        return retorno;
    }

    public Boolean update(Comissao comissao, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        ContentValues ctv = new ContentValues();

        ctv.put("TotalVendas", comissao.getTotalVendas());
        ctv.put("TotalComissao", comissao.getTotalComissao());

        String[] param = new String[]{String.valueOf(comissao.getTipo()), comissao.getPeriodo()};

        boolean retorno = dbLocal.update(TABELA, ctv, "Tipo=?, Periodo=?", param) > 0;

        //if(db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public Comissao get(short Tipo, String Periodo) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        String[] param = new String[]{String.valueOf(Tipo), Periodo};

        Comissao comissao = new Comissao();

        Cursor c = db.query(TABELA, colunas, "Tipo=?, Periodo=?", param, null, null, null);
        try {
            if (c.moveToFirst()) {
                comissao.setTipo(c.getShort(c.getColumnIndex("Tipo")));
                comissao.setPeriodo(c.getString(c.getColumnIndex("Periodo")));
                comissao.setTotalVendas(c.getDouble(c.getColumnIndex("TotalVendas")));
                comissao.setTotalComissao(c.getDouble(c.getColumnIndex("TotalComissao")));
            }
        } catch (Exception e) {
            Log.e("ComissaoDAO:get(tipo,Periodo).", e.getMessage());
        } finally {
            c.close();
            //db.close(); singleton();
        }

        return comissao;
    }

    public List<Comissao> getAll() {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        List<Comissao> lista = new ArrayList<Comissao>();

        Cursor c = db.query(TABELA, colunas, null, null, null, null, null);

        try {
            while (c.moveToNext()) {
                Comissao comissao = new Comissao();

                comissao.setTipo(c.getShort(c.getColumnIndex("Tipo")));
                comissao.setPeriodo(c.getString(c.getColumnIndex("Periodo")));
                comissao.setTotalVendas(c.getDouble(c.getColumnIndex("TotalVendas")));
                comissao.setTotalComissao(c.getDouble(c.getColumnIndex("TotalComissao")));
                lista.add(comissao);
            }
        } catch (Exception e) {
            Log.e("ComissaoDAO:getAll.", e.getMessage());
        } finally {
            //db.close(); singleton();
            c.close();
        }

        return lista;
    }

    public List<Comissao> listaComissao(long idTipo) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        List<Comissao> lista = new ArrayList<Comissao>();

        String[] param = new String[]{String.valueOf(idTipo)};

        Cursor c = db.query(TABELA, colunas, "Tipo=?", param, null, null, null);

        try {
            while (c.moveToNext()) {
                Comissao comissao = new Comissao();

                comissao.setTipo(c.getShort(c.getColumnIndex("Tipo")));
                comissao.setPeriodo(c.getString(c.getColumnIndex("Periodo")));
                comissao.setTotalVendas(c.getDouble(c.getColumnIndex("TotalVendas")));
                comissao.setTotalComissao(c.getDouble(c.getColumnIndex("TotalComissao")));
                lista.add(comissao);
            }
        } catch (Exception e) {
            Log.e("ComissaoDAO:listaComissao(idTipo).", e.getMessage());
        } finally {
            c.close();
            //db.close(); singleton();
        }

        return lista;
    }

    public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
        try {
            deleteAll();
            getimportacaoComissao(linhasTexto, new DBHelper(ctx).getWritableDatabase());
        } catch (Exception e) {
            Log.d("ComissaoDAO", e.getMessage());
        }
    }

    private void getimportacaoComissao(List<String> linhasTexto, SQLiteDatabase db) {
        try {
            db.beginTransaction();

            getZeraIncrementar();
            for(String linha:linhasTexto){
                if (getLinhaComissaoImportar(db, linha)) continue;
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            //db.close(); singleton();
        }
    }

    private boolean getLinhaComissaoImportar(SQLiteDatabase db, String linha) {
        Comissao c = new Comissao();

        if (linha.substring(0, 2).trim().equals(""))
            c.setTipo((short) 0);
        else if (linha.substring(0, 2).trim().matches(MATCHES))
            c.setTipo(Short.parseShort(linha.substring(0, 2).trim()));
        else {
            Utils.gravarLog("\nCOM: " + linha);
            return true;
        }
        c.setPeriodo(linha.substring(2, 42).trim());

        if (linha.substring(42, 52).trim().equals(""))
            c.setTotalVendas(0);
        else if (linha.substring(42, 52).trim().matches(MATCHES))
            c.setTotalVendas(Double.parseDouble(linha.substring(42, 52).trim()) / 100);
        else {
            Utils.gravarLog("\nCOM: " + linha);
            return true;
        }
        if (linha.substring(52, 62).trim().equals(""))
            c.setTotalComissao(0);
        else if (linha.substring(52, 62).trim().matches(MATCHES))
            c.setTotalComissao(Double.parseDouble(linha.substring(52, 62).trim()) / 100);
        else {
            Utils.gravarLog("\nCOM: " + linha);
            return true;
        }
        insert(c, db);
        return false;
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db ) {
        if (getLinhaComissaoImportar(db, linha)) return true;

        return false;
    }

    @Override
    public String getMensagemTabela() { return ctx.getString(R.string.importando_tabela_comissao); }

    @Override
    public SQLiteDatabase getDB() {
            return new DBHelper(ctx).getWritableDatabase();
    }
}
