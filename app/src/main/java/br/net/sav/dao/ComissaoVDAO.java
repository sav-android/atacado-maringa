package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.ComissaoV;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ComissaoVDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	public static final String TABELA="ComissaoV";
	
	public ComissaoVDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
		
	}
	
	public void importar(List<String> linhasTexto) {
		try {					
			deleteAll() ;

			getImportacaoComissaoVendedor(linhasTexto);
		} catch (Exception e){
			Log.d("AliquotaIcmsDAO", e.getMessage());
		}
	}

	private void getImportacaoComissaoVendedor(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for(String linha:linhasTexto){
				if (getlinhaComissaoVendedor(linha)) continue;

			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaComissaoVendedor(String linha) {
		ComissaoV cov = new ComissaoV();

		if (linha.substring(0, 4).trim().equals(""))
			cov.setIdTabelaPreco((short)0);
		else if (linha.substring(0, 4).matches(MATCHES))
			cov.setIdTabelaPreco(Short.parseShort(linha.substring(0,4)));
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}

		if (linha.substring(4, 13).trim().equals(""))
			cov.setIdProduto(0);
		else if (linha.substring(4, 13).matches(MATCHES))
			cov.setIdProduto(Long.parseLong(linha.substring(4,13)));
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}

		if (linha.substring(13, 23).trim().equals(""))
			cov.setDescontoInicial(0);
		else if (linha.substring(13, 23).matches(MATCHES))
			cov.setDescontoInicial(Double.parseDouble(linha.substring(13, 23)) / 100);
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}

		if (linha.substring(23, 33).trim().equals(""))
			cov.setDescontoFinal(0);
		else if (linha.substring(23, 33).matches(MATCHES))
			cov.setDescontoFinal(Double.parseDouble(linha.substring(23, 33)) / 100);
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}

		if (linha.substring(33, 43).trim().equals(""))
			cov.setComissao(0);
		else if (linha.substring(33, 43).matches(MATCHES))
			cov.setComissao(Double.parseDouble(linha.substring(33,43)) / 100);
		else {
			Utils.gravarLog("\nICM: " +linha);
			return true;
		}

		insert(cov);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaComissaoVendedor(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_comissao_v);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
