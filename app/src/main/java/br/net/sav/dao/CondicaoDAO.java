package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Condicao;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class CondicaoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Condicao";

	public CondicaoDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
	}

	public List<Condicao> listaCondicaoPedido(short idEmpresa, short idFilial, short idTabela) {
		List<Condicao> lista = new ArrayList<Condicao>();

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		Cursor c = null;

		String[] param = new String[] { String.valueOf(idEmpresa), String.valueOf(idFilial), String.valueOf(idTabela) };

		try {

			c = db.rawQuery("SELECT * FROM Condicao WHERE IdEmpresa=? AND IdFilial=? AND (IdTabela=? OR IdTabela=0)", param);

			while (c.moveToNext())
				lista.add((Condicao) preencherObjeto(c, new Condicao()));

		} catch (Exception e) {
			Log.e("CondicaoDAO:", "listaCondicaoProduto." + e.getMessage());
		} finally {
			if (c != null) c.close();
			//db.close(); singleton();
		}

		return lista;
	}

	public List<Condicao> listaCondicaoPedidoPorQntdDiasPrazo(short idEmpresa, short idFilial, short idTabela, short qntdDiasPrazo ) {
		List<Condicao> lista = new ArrayList<Condicao>();

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		Cursor c = null;

		String[] param = new String[] { String.valueOf(idEmpresa), String.valueOf(idFilial), String.valueOf(idTabela) };



		try {
			if (qntdDiasPrazo > 0) {
				c = db.rawQuery("SELECT * FROM Condicao WHERE IdEmpresa=? AND IdFilial=? AND QtdeDiasPrazo >= 0 AND IdCondicao NOT IN(1) AND (IdTabela=? OR IdTabela=0)", param);
			}else {
				c = db.rawQuery("SELECT * FROM Condicao WHERE IdEmpresa=? AND IdFilial=? AND QtdeDiasPrazo == 0 AND (IdTabela=? OR IdTabela=0)", param);
			}

			while (c.moveToNext())
				lista.add((Condicao) preencherObjeto(c, new Condicao()));

		} catch (Exception e) {
			Log.e("CondicaoDAO:", "listaCondicaoProduto." + e.getMessage());
		} finally {
			if (c != null) c.close();
			//db.close(); singleton();
		}

		return lista;
	}

	public Condicao get(short idCondicao) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db;

		Condicao condicao = null;

		Cursor c = null;

		try {

			c = dbLocal.query(TABELA, colunas, "IdCondicao=?", new String[] { String.valueOf(idCondicao) }, null, null, null);

			if (c.moveToFirst()) {
				condicao = getObjetoPreenchido(c);
			}
		} catch (Exception e) {
			Log.e("CondicaoDAO:get.", e.getMessage());
		} finally {
			c.close();
			//if (db == null) //dbLocal.close(); singletone();
		}
		return condicao;
	}

	public List<Condicao> getAll() {
		new CondicaoDAO(ctx, db);

		SQLiteDatabase dbLocal = null;

		if (db == null) dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<Condicao> lista = new ArrayList<Condicao>();

		Cursor c = null;

		try {
			c = dbLocal.query(TABELA, colunas, null, null, null, null, null);

			while (c.moveToNext()) {
				lista.add(getObjetoPreenchido(c));
			}
		} catch (Exception e) {
			Log.e("CondicaoDAO:getAll.", e.getMessage());
		} finally {
			c.close();
			//if (db == null) //dbLocal.close(); singletone();
		}

		return lista;
	}

	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			if (cargaCompleta) deleteAll();
			getimportacaoCondicao(linhasTexto, cargaCompleta, null, null);
		} catch (Exception e) {
			Log.d("CondicaoDAO", e.getMessage());
		}
	}

	private void getimportacaoCondicao(List<String> linhasTexto, Boolean cargaCompleta, Activity ctx, IAtualizaMensagemProgress msgView){
		try {
			db.beginTransaction();

			for(String linha:linhasTexto){
				if (getlinhaCondicaoImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaCondicaoImportar(Boolean cargaCompleta, String linha) {
		Condicao c = new Condicao();

		if (linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals("")) c.setIdCondicao(Short.parseShort(linha.substring(0, 4).trim()));
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}
		c.setDescricao(linha.substring(4, 34).trim());

		if (linha.substring(34, 41).trim().equals("")) c.setTaxaFinanceira(0);
		else if (linha.substring(34, 41).trim().matches(MATCHES)) c.setTaxaFinanceira(Double.parseDouble(linha.substring(34, 41).trim()) / 1000000);
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}
		if (linha.substring(41, 50).trim().equals("")) c.setLimiteMinimoPedido(0);
		else if (linha.substring(41, 50).trim().matches(MATCHES)) c.setLimiteMinimoPedido(Double.parseDouble(linha.substring(41, 50).trim()) / 100);
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}

		if (linha.substring(50, 54).trim().equals("")) {
			c.setIdTabela((short) 0);
		} else if (linha.substring(50, 54).trim().matches(MATCHES)) {
			c.setIdTabela(Short.parseShort(linha.substring(50, 54).trim()));
		} else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}

		if (linha.substring(54, 58).trim().matches(MATCHES)) c.setIdEmpresa(Short.parseShort(linha.substring(54, 58)));

		if (linha.substring(58, 62).trim().matches(MATCHES)) c.setIdFilial(Short.parseShort(linha.substring(58, 62)));

		c.setTipoDocumento(linha.substring(62, 64).trim());

		if (linha.substring(64, 66).trim().equals("")) c.setQtdeDiasPrazo((short) 0);
		else if (linha.substring(64, 66).trim().matches(MATCHES)) c.setQtdeDiasPrazo(Short.parseShort(linha.substring(64, 66).trim()));
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}
		if (linha.substring(66, 70).trim().equals("")) c.setPercDescontoMaximo(0);
		else if (linha.substring(66, 70).trim().matches(MATCHES)) c.setPercDescontoMaximo(Double.parseDouble(linha.substring(66, 70).trim()) / 100);
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}

		if (linha.substring(71, 73).trim().equals("")) c.setQtdeParcelas((short) 0);
		else if (linha.substring(71, 73).trim().matches(MATCHES)) c.setQtdeParcelas(Short.parseShort(linha.substring(71, 73).trim()));
		else {
			Utils.gravarLog("\nCPG: " + linha);
			return true;
		}
		c.setCondicaoEspecial(Boolean.parseBoolean(linha.substring(73, 74)));

		if (!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(70))) {
				case ' ':
				case 'A':
					if (exists(c)) update(c);
					else
						insert(c);
					break;
				case 'E':
					delete(c);
					break;
				default:
					Utils.gravarLog("\nCPG(Flag Inv�lida): " + linha);
					break;
			}
		} else {
			if (!exists(c)) insert(c);
		}
		return false;
	}

	public List<Condicao> getByTabelaPreco(short idTabela) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

		Cursor c = db.query(TABELA, colunas, "IdTabela=? or IdTabela=0", new String[] { String.valueOf(idTabela) }, null, null, "IdCondicao");

		List<Condicao> lstCondicao = new ArrayList<Condicao>(c.getCount());

		try {
			while (c.moveToNext()) {
				lstCondicao.add(getObjetoPreenchido(c));
			}
		} catch (Exception e) {
			Log.e("CondicaoDAO:", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}

		return lstCondicao;
	}

	public Condicao get(short IdCondicao, short IdTabela) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		String[] param = new String[] { String.valueOf(IdCondicao), String.valueOf(IdTabela) };

		Condicao condicao = null;

		Cursor c = db.query(TABELA, colunas, "IdCondicao=? and (IdTabela=? or IdTabela=0)", param, null, null, null);

		try {
			if (c.moveToFirst()) {
				condicao = getObjetoPreenchido(c);
			}
		} catch (Exception e) {
			Log.e("CondicaoDAO:", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}
		return condicao;
	}
	
	public Condicao getUltimaCondicaoCliente(long clienteId) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		Cursor c = null;
		Condicao retorno = null;
		try {
			String sql = "";
			sql += "SELECT c.* FROM Condicao c \n";
			sql += "LEFT JOIN Pedido p ON (p.IdCondicao = c.IdCondicao) \n";
			sql += "WHERE p.IdPedido = (SELECT COALESCE(MAX(IdPedido),0) FROM Pedido WHERE IdCliente = ?)";
			
			c = db.rawQuery(sql, new String[] { String.valueOf(clienteId) });
			if (c.moveToFirst()) {
				retorno = getObjetoPreenchido(c);
			}
			
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			if (c != null) {
				c.close();
			}
			//db.close(); singleton();
		}
		return retorno;
	}
	
	private Condicao getObjetoPreenchido(Cursor c) {
		Condicao retorno = new Condicao();
		try {
			retorno.setIdCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
			retorno.setIdFilial(c.getShort(c.getColumnIndex("IdFilial")));
			retorno.setIdEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
			retorno.setDescricao(c.getString(c.getColumnIndex("Descricao")));
			retorno.setTaxaFinanceira(c.getDouble(c.getColumnIndex("TaxaFinanceira")));
			retorno.setLimiteMinimoPedido(c.getDouble(c.getColumnIndex("LimiteMinimoPedido")));
			retorno.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
			retorno.setTipoDocumento(c.getString(c.getColumnIndex("TipoDocumento")));
			retorno.setQtdeDiasPrazo(c.getShort(c.getColumnIndex("QtdeDiasPrazo")));
			retorno.setPercDescontoMaximo(c.getDouble(c.getColumnIndex("PercDescontoMaximo")));
			retorno.setQtdeParcelas(c.getShort(c.getColumnIndex("QtdeParcelas")));
			retorno.setCondicaoEspecial(c.getInt(c.getColumnIndex("CondicaoEspecial")) > 0);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return retorno;
	}

	@Override
	public String getTabela() { return TABELA; }

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaCondicaoImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() { return ctx.getString(R.string.importando_tabela_condicoes_pagamento); }

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
