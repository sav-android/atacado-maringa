package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.CondicaoPagamentoCliente;
import br.net.sav.modelo.CondicaoPagamentoCliente.OpcoesTransacao;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class CondicaoPagamentoClienteDAO implements AcaoParaImportacaoDados {
	private Context context;

	private static final String TABELA = "CondicaoPagamentoCliente";
	private static final String[] colunas = new String[] { "IdCliente", "IdCondicao" };

	public CondicaoPagamentoClienteDAO(Context ctx) {
		this.context = ctx;
	}

	public List<Condicao> getListaCliente(long idCliente, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(context).getWritableDatabase();
		else
			dbLocal = db;

		Cursor c = null;
		List<Condicao> listaRetorno = null;
		List<Short> listaCondicoesCliente = null;

		try {
			c = dbLocal.rawQuery("SELECT IdCondicao FROM CondicaoPagamentoCliente WHERE IdCliente=?", new String[] { String.valueOf(idCliente) });
			listaCondicoesCliente = new ArrayList<Short>(c.getCount());
			while (c.moveToNext())
				listaCondicoesCliente.add(c.getShort(0));

			CondicaoDAO condicaoDAO = new CondicaoDAO(context, dbLocal);
			if (listaCondicoesCliente.size() > 0) {
				listaRetorno = new ArrayList<Condicao>(c.getCount());

				for (Short idCondicao : listaCondicoesCliente) {
					Condicao cAux = condicaoDAO.get(idCondicao);
					if (cAux != null)
						listaRetorno.add(cAux);
				}
			}
		} catch (SQLException se) {
			Log.e("PagamentoCliente", "getListaCliente - " + se.getMessage());
		} finally {
			if (c != null)
				c.close();
		//	if (db == null)
				//dbLocal.close(); singletone();
		}
		return listaRetorno;
	}

	public boolean realizarTransacaoBanco(OpcoesTransacao opcao, CondicaoPagamentoCliente cpc, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(context).getWritableDatabase();
		else
			dbLocal = db;

		ContentValues ctv = null;
		Cursor c = null;

		String clausulaSql = null;
		String[] valoresClausula = null;

		if (cpc != null) {
			ctv = preencherContentValues(cpc);
			clausulaSql = "IdCliente=? AND IdCondicao=?";
			valoresClausula = new String[] { String.valueOf(cpc.getIdCliente()), String.valueOf(cpc.getIdCondicao()) };
		}

		boolean retorno = false;

		try {
			switch (opcao) {
			case INSERIR:
				retorno = dbLocal.insert(TABELA, null, ctv) > 0;
				break;

			case ALTERAR:
				retorno = dbLocal.update(TABELA, ctv, clausulaSql, valoresClausula) > 0;
				break;

			case DELETAR:
				retorno = dbLocal.delete(TABELA, clausulaSql, valoresClausula) > 0;
				break;

			case DELETAR_TUDO:
				retorno = dbLocal.delete(TABELA, null, null) > 0;
				break;

			case EXISTE:
				c = dbLocal.query(TABELA, null, clausulaSql, valoresClausula, null, null, null);
				retorno = c.moveToFirst() && c.getLong(0) > 0;
				break;

			default:
				retorno = false;
				break;
			}
		} catch (SQLException e) {
			retorno = false;
		} finally {
			if (c != null)
				c.close();
		}

		return retorno;
	}

	public void importar(List<String> linhasTexto, Boolean cargaCompleta, SQLiteDatabase dbParam) {
		SQLiteDatabase db = dbParam == null ? new DBHelper(context).getWritableDatabase() : dbParam;

		if (cargaCompleta)
			realizarTransacaoBanco(OpcoesTransacao.DELETAR_TUDO, null, db);

		getImportacaoCondicaoCliente(linhasTexto, cargaCompleta, db);
	}

	private void getImportacaoCondicaoCliente(List<String> linhasTexto, Boolean cargaCompleta, SQLiteDatabase db) {
		db.beginTransaction();
		try {
			for(String linha:linhasTexto){
				if (getlinhaCondicaoCliente(linha,cargaCompleta, db)) {
					continue; }
			}

			db.setTransactionSuccessful();
		} catch (Exception e) {
			Log.e("CondicaoPagamento", "importar - " + e.getMessage());
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaCondicaoCliente(String linha, boolean cargaCompleta, SQLiteDatabase db) {
		CondicaoPagamentoCliente cpc = new CondicaoPagamentoCliente();

		if (linha.substring(0, 8).trim().matches(Utils.MATCHES) && !linha.substring(0, 8).trim().equals(""))
			cpc.setIdCliente(Long.parseLong(linha.substring(0, 8)));
		else {
			Utils.gravarLog("\nCCL: " + linha);
			return true;
		}

		if (linha.substring(8, 12).trim().matches(Utils.MATCHES) && !linha.substring(8, 12).trim().equals(""))
			cpc.setIdCondicao(Short.parseShort(linha.substring(8, 12).trim()));
		else {
			Utils.gravarLog("\nCCL: " + linha);
			return true;
		}

		switch (Character.toUpperCase(linha.charAt(12))) {
			case ' ':
			case 'A':
				if (!realizarTransacaoBanco(OpcoesTransacao.EXISTE, cpc, db))
					realizarTransacaoBanco(OpcoesTransacao.INSERIR, cpc, db);
				else
					realizarTransacaoBanco(OpcoesTransacao.ALTERAR, cpc, db);
				break;

			case 'E':
				realizarTransacaoBanco(OpcoesTransacao.DELETAR, cpc, db);
				break;

			default:
				if (cargaCompleta) {
					if (realizarTransacaoBanco(OpcoesTransacao.EXISTE, cpc, db))
						realizarTransacaoBanco(OpcoesTransacao.INSERIR, cpc, db);
					else
						realizarTransacaoBanco(OpcoesTransacao.ALTERAR, cpc, db);
				}
				break;
		}
		return false;
	}

	private ContentValues preencherContentValues(CondicaoPagamentoCliente cpc) {
		ContentValues ctv = new ContentValues();
		ctv.put(colunas[0], cpc.getIdCliente());
		ctv.put(colunas[1], cpc.getIdCondicao());

		return ctv;
	}

	private CondicaoPagamentoCliente preencherObjeto(Cursor c) {
		CondicaoPagamentoCliente cpc = new CondicaoPagamentoCliente();
		cpc.setIdCliente(c.getLong(c.getColumnIndex(colunas[0])));
		cpc.setIdCondicao(c.getShort(c.getColumnIndex(colunas[1])));

		return cpc;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaCondicaoCliente(linha,true, db)) {
			return true; }

		return false;
	}


	@Override
	public String getMensagemTabela() { return context.getString(R.string.importando_tabela_condicao_pagamento_cliente); }

	@Override
	public SQLiteDatabase getDB() {
		return new DBHelper(context).getReadableDatabase();
	}
}