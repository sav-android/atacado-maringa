package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.CondicaoProduto;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;


public class CondicaoProdutoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "CondicaoProduto";

    public CondicaoProdutoDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public void importar(List<String> linhasTexto) {
        try {
            deleteAll();

            getImportacaoCondicaoProduto(linhasTexto);
        } catch (Exception e) {
            Log.d("CondicaoProdutoDAO", e.getMessage());
        }
    }

    private void getImportacaoCondicaoProduto(List<String> linhasTexto) {
        try {
            db.beginTransaction();

            for (String linha : linhasTexto) {
                if (getLinhaCondicaoProduto(linha)) continue;
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private boolean getLinhaCondicaoProduto(String linha) {
        CondicaoProduto c = new CondicaoProduto();

        if (linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
            c.setIdCondicao(Short.parseShort(linha.substring(0, 4).trim()));
        else {
            Utils.gravarLog("\nCPP: " + linha);
            return true;
        }
        if (linha.substring(4, 13).trim().matches(MATCHES) && !linha.substring(4, 13).trim().equals(""))
            c.setIdProduto(Long.parseLong(linha.substring(4, 13).trim()));
        else {
            Utils.gravarLog("\nCPP: " + linha);
            return true;
        }

        if (!exists(c))
            insert(c);
        return false;
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        if (getLinhaCondicaoProduto(linha)) return true;
        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_condicao_produto);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
