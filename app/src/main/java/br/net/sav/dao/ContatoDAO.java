	package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Contato;
import br.net.sav.modelo.Parametro;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

	/**
 * @author maq4
 *
 */
public class ContatoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Contato";
	private SimpleDateFormat sdfData = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sdfDataHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public ContatoDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	

	public ContatoDAO(Context ctx ){
		super(ctx, TABELA , new DBHelper(ctx).getReadableDatabase());
	}
	



	@Override
	public Boolean insert(Object objeto) {
		return super.insert(objeto);
	}



	@Override
	public Boolean insert(Object objeto, SQLiteDatabase db) {
		return super.insert(objeto, db);
	}



	@Override
	public Boolean delete(Object objeto) {
		return super.delete(objeto);
	}

	


	@Override
	public Boolean update(Object objeto) {
		return super.update(objeto);
	}

	public void gerarTexto(long idVendedor) {
		
		List<Contato> contatosExportar = getContatoNaoEnviado();
		
		if (contatosExportar.size() == 0){
			return;   
		}
		
		try{
			Date data = new Date();
			
			String campo;

			String nomeArquivoContato = String.format("CON%09d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));
			
			FileOutputStream outContato = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/envia", nomeArquivoContato));
			
			 
			
			for (Contato contatos: contatosExportar) {
				
				
				campo = String.format("%09d", contatos.getIdCliente());
				outContato.write(campo.getBytes());
				
				campo = String.format("%-20s", contatos.getNome()); 
				outContato.write(campo.getBytes());
				
				campo = String.format("%-15s", contatos.getHobby()); 
				outContato.write(campo.getBytes());
				
				campo = String.format("%-15s", contatos.getTime()); 
				outContato.write(campo.getBytes());
				
				campo = String.format("%-4s", contatos.getDDD());
				outContato.write(campo.getBytes());
				
				campo = String.format("%-9s", contatos.getTelefone().replace("-", ""));
				outContato.write(campo.getBytes());
				
				campo = String.format("%-8s", new SimpleDateFormat("yyyyMMdd").format(contatos.getAniversario()));
				outContato.write(campo.getBytes());
				
				campo = String.format("%-15s", contatos.getCargo());
				outContato.write(campo.getBytes());
				
				campo = String.format("%-40s", contatos.getEmail());
				outContato.write(campo.getBytes());
				
				
				campo = String.format("%08d", contatos.getCodRepresentante());
				outContato.write(campo.getBytes());
				
				outContato.write("\r\n".getBytes());
			}
			
			outContato.flush();
			outContato.close();
			                                                                                      
			
		}catch(Exception e){
			e.printStackTrace();
			Log.e("Exportando contato",e.getMessage().toString());
		}
	}
	
	
	public List<Contato> listaAniversariantes(Date dataInicial, Date dataFinal) {
		
		List<Contato> listaAniversariante = 
					new ArrayList<Contato>();
		
		String sql = " select *	" +
					 " from Contato  " +
					 "  where 	cast(strftime('%m', aniversario) as integer )  " +
					 "  between cast(strftime('%m' , ? ) as integer ) " +
					 "  and cast(strftime('%m' , ? ) as integer )  Order by IdCliente , Nome  ";
		try
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
			
			String[] datas = new String[]{ format.format(dataInicial) , format.format(dataFinal) };
			
			Cursor c = this.findQuery(sql, datas);
			
			while ( c.moveToNext() )
			{
				Contato contato = this.preencherContato(c);
				
				listaAniversariante.add(contato);
			}
			
		}
		catch (Exception es )
		{
			Log.e("ContatoDAO:listaAniversariantes.", es.getMessage());
		}
		
		
		return listaAniversariante ;
		
	}
	
	
	public void importar(List<String> linhasTexto) {
		try {
			deleteAll();
			getImportacaoContato(linhasTexto);
		} catch (Exception e) {
			Log.d("ContatoDAO", e.getMessage());
		}
	}

		private void getImportacaoContato(List<String> linhasTexto) throws ParseException {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			try {
				db.beginTransaction();

				for(String linha:linhasTexto){
					if (getLinhaContato(sdf, linha)) continue;
				}

				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
				//db.close(); singleton();
			}
		}

		private boolean getLinhaContato(SimpleDateFormat sdf, String linha) throws ParseException {
			Contato c = new Contato();
			//Obs: colocar no campo, quantas casas ir� aumentar, exemplo campo NUMERO aumentou 1 casa.
			int aumentar = 0;
			if (linha.length() > 133){
				aumentar = 1;
			}

			if (linha.substring(0, 8).trim().matches(MATCHES)
					&& !linha.substring(0, 8).trim().equals(""))
				c.setIdCliente(Long.parseLong(linha.substring(0, 8)
						.trim()));
			else {
				Utils.gravarLog("\nCON: " + linha);
				return true;
			}
			c.setNome(linha.substring(8, 28).trim().equals("")? "":linha.substring(8, 28).trim());
			c.setHobby(linha.substring(28, 43).trim().equals("")? "":linha.substring(28, 43).trim());
			c.setTime(linha.substring(43, 58).trim().equals("")? "":linha.substring(43, 58).trim());
			c.setDDD(linha.substring(58, 62).trim().equals("")? "":linha.substring(58, 62).trim());
			c.setTelefone(linha.substring(62, 70 + aumentar).trim().equals("")? "":linha.substring(62, 70 + aumentar).trim());
			c.setAniversario(linha.substring(70 + aumentar, 78 + aumentar).trim().equals("") ? null:sdf.parse(linha.substring(70 + aumentar, 78 + aumentar).trim()) );
			c.setCargo(linha.substring(78 + aumentar, 93 + aumentar).trim().equals("")? "":linha.substring(78 + aumentar, 93 + aumentar).trim());
			c.setEmail(linha.substring(93 + aumentar, 133 + aumentar).trim().equals("")? "":linha.substring(93 + aumentar, 133 + aumentar).trim());
			c.setEnviado(true);
			Parametro parametro = new ParametroDAO(ctx, null).get();
			c.setCodRepresentante(parametro!= null? parametro.getIdVendedor(): 0);

			if(exists(c))
				update(c);
			else
				insert(c);
			return false;
		}

		public List<Contato> getAllContatoCliente(long idCliente){
		
		List<Contato> listaContatoClientes = new ArrayList<Contato>();
		
		String sql = " select * from Contato where IdCliente = ? "; 
		
		
		try {
			
			Cursor c = this.findQuery(sql, new String[]{String.valueOf(idCliente)}); 
			
			while (c.moveToNext()) {
		
				Contato contato = this.preencherContato(c); 
				
				listaContatoClientes.add(contato);
			}			
		} catch (Exception e) {
			Log.d("ContatoClienteDAO:listaContatoClientes", e.getMessage());
		} 
		return listaContatoClientes;
	}
	
	public List<Contato> getContatoNaoEnviado(){
		
		List<Contato> listaContatoCliente = new ArrayList<Contato>();
		
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		String sql = " select * from Contato where Enviado = 0  order by IdCliente asc ";
		

		Cursor c = db.rawQuery(sql, null ) ;
		
		try 
		{
			
			while ( c.moveToNext() )
		
			{
				
				Contato contato = this.preencherContato(c);
				
				listaContatoCliente.add(contato);
				
			}
			
		}
		catch ( Exception es )
		{
			c.close();

		}
		
		return listaContatoCliente;
				
	}
	
	
	private Cursor findQuery ( String query , String...param ) 
		throws Exception 
	{
		try 
		{
			SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
			Cursor cursor = null ;
			
			if ( param.length == 0 )
			
			{
				cursor = db.rawQuery(query, null );
				
			}
			else 
			{
				
				cursor = db.rawQuery(query, param);
				
			}
			
			return cursor;
		}
		catch ( Exception es ){
			throw es ;
		}
		
		
		
	}
	
	
	public List<Contato> getAllContatos(){
		
		List<Contato> listaContatoClientes = new ArrayList<Contato>();
	
		try {
		
			Cursor c = this.findQuery("select * from Contato order by IdCliente , Nome " ); 
			
			while (c.moveToNext()) {
				
				Contato contato = this.preencherContato(c);
				
				listaContatoClientes.add(contato);
				
			}			
		} catch (Exception e) {
			Log.d("ContatoClienteDAO:listaContatoClientes", e.getMessage());
		} 
		return listaContatoClientes;
	}
	
	private Contato preencherContato ( Cursor c ) 
		throws Exception 
	{
		Contato contato = new Contato();
		contato.setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
		contato.setNome(c.getString(c.getColumnIndex("Nome")));
		contato.setCargo(c.getString(c.getColumnIndex("Cargo")));
		contato.setDDD(c.getString(c.getColumnIndex("DDD")));
		contato.setTelefone(c.getString(c.getColumnIndex("Telefone")));
		contato.setAniversario(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("Aniversario"))));
		contato.setHobby(c.getString(c.getColumnIndex("Hobby")));
		contato.setTime(c.getString(c.getColumnIndex("Time")));
		contato.setEmail(c.getString(c.getColumnIndex("Email")));
		contato.setCodRepresentante(c.getLong(c.getColumnIndex("CodRepresentante")));
		contato.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		contato.setDataEnvio(sdf.parse(c.getString(c.getColumnIndex("DataEnvio"))));
		contato.setCodRepresentante(c.getLong(c.getColumnIndex("CodRepresentante")));
		return contato ;
	}
	
	public List<Contato> listaContatoClientes(long iCliente, boolean naoEnviados) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		List<Contato> listaContatoClientes = new ArrayList<Contato>();

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM Contato");

		if (iCliente > 0)
			sql.append(" WHERE IdCliente = " + iCliente);

		if (naoEnviados) {
			if (sql.toString().contains("WHERE"))
				sql.append(" AND Enviado = 0");
			else
				sql.append(" WHERE IdCliente > 1000000");
		}

		sql.append(" ORDER BY IdCliente desc");

		
		try {
			
			Cursor c = this.findQuery(sql.toString());
			
			while (c.moveToNext()) {
			
				Contato contato = this.preencherContato(c);
				
				listaContatoClientes.add(contato);
		
			}			
		} catch (Exception e) {
			Log.d("ContatoClienteDAO:listaContatoClientes", e.getMessage());
		} 
		return listaContatoClientes;
	}

	public void marcarEnviado(Date dataEnvio) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = String.format("UPDATE Contato SET Enviado = 1, DataEnvio = \'%s\' WHERE Enviado = 0", Utils.sdfDataHoraDb.format(dataEnvio));
		db.execSQL(sql);
		//db.close(); singleton();
	} 

	
	public void excluirContatosSemCliente(){

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = " delete from Contato  " +
				" where IdCliente not in ( select IdCliente from Cliente cl  )  ";
		Log.i("excluir contato:","sql");
		db.execSQL(sql);
		//db.close(); singleton();
	}
	
	
	public boolean atualizarIdRepresentante ( String IdCliente  , String nome , String IdVendedor )
	{
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = "UPDATE Contato SET CodRepresentante = " + IdVendedor + " " +
				" where IdCliente =  " + IdCliente + " and Nome = '"+nome+"'    " ;
		db.execSQL(sql);
		//db.close(); singleton();
		return true ; 
	}

		@Override
		public String getTabela() { return TABELA; }

		@Override
		public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			try {
				if (getLinhaContato(sdf, linha)) return true;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return false;
		}

		@Override
		public String getMensagemTabela() { return ctx.getString(R.string.importacao_tabela_contato); }

		@Override
		public SQLiteDatabase getDB() {
			if (db == null){
				return new DBHelper(ctx).getReadableDatabase();
			}else {
				return db;
			}
		}
	}
