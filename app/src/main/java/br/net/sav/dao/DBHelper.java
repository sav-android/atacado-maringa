package br.net.sav.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    public static final String BANCO = "Atacado Maringa.db";
    public static final int VERSION_DB = 26;
    public Context ctx;
    private static SQLiteDatabase db;

    public synchronized SQLiteDatabase getInstance() {
        if(db==null)
            db = super.getWritableDatabase();

        if(!db.isOpen())
            db = super.getWritableDatabase();

        return db;
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return getInstance();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return getInstance();
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.enableWriteAheadLogging();
    }

    private static final String sqlCombo = "CREATE TABLE IF NOT EXISTS "
            + "Combo (IdCombo Integer, CodigoCombo Integer, PeriodoInicial Date, "
            + "PeriodoFinal Date, DataAtualizacao Date, Descricao nvarchar(200), Ativo boolean)";
    private static final String sqlRegraCombo = "CREATE TABLE IF NOT EXISTS"
            + " RegraCombo (IdRegraCombo Integer, IdCombo Integer,TipoRegra nvarchar(1),"
            + " Regra nvarchar(200), DataAtualizacao Date, Ativo boolean)";
    private static final String sqlComboXPedido = "CREATE TABLE IF NOT EXISTS "
            + "ComboXPedido (IdCombo Integer, IdCliente Integer, IdPedido Integer, DataPedido Date)";
    private static final String sqlComboXCliente = "CREATE TABLE IF NOT EXISTS "
            + "ComboXCliente (IdCombo Integer, IdCliente Integer)";
    private static final String sqlRegraDesconto = "CREATE TABLE IF NOT EXISTS "
            + "RegraDesconto (IdDesconto Integer,Sequencia Integer, TipoRegra nvarchar(1),"
            + " Regra nvarchar(200))";
    private static final String sqlAdicionais = "CREATE TABLE IF NOT EXISTS "
            + " Adicionais(IdCliente integer NOT NULL PRIMARY KEY,"
            + " Fornecedor1 nvarchar(25), DDD1 nvarchar(2), Fone1 "
            + " nvarchar(8), Fornecedor2 nvarchar(25), DDD2 nvarchar(2),"
            + " Fone3 nvarchar(8), Banco nvarchar(10),Agencia nvarchar(10),"
            + " Conta nvarchar(10), DataConstituicaoEmpresa Date, PredioProprio"
            + " smallint,ResponsavelCompras nvarchar(25), ResponsavelPagamentos"
            + " nvarchar(25), Enviado boolean, DataEnvio Timestamp);";

    private static final String sqlAliquotaICMSDAO = "CREATE TABLE IF NOT EXISTS"
            + " AliquotaICMS (UfDestino nvarchar(2) NOT NULL,CodigoICMS smallint "
            + " NOT NULL, TipoPessoa smallint NOT NULL, Aliquota double, PRIMARY KEY (UfDestino,"
            + " CodigoICMS, TipoPessoa));";

    private static final String sqlCidades = "CREATE TABLE IF NOT EXISTS Cidade(Cidade nvarchar(30) Not Null Primary Key);";

    private static final String sqlCliente = "CREATE TABLE IF NOT EXISTS Cliente("
            + " IdCliente integer Primary Key NOT NULL, Razao nvarchar(50),"
            + " Fantasia nvarchar(30),Ramo smallint,TipoEndereco nvarchar(20),"
            + " Endereco nvarchar(60),Complemento nvarchar(60),Bairro nvarchar(20),"
            + " Cidade nvarchar(30),UF nvarchar(2),CEP integer,DDD smallint,Telefone"
            + " integer,TipoPessoa smallint,CnpjCpf nvarchar(14),IeRg nvarchar(16),"
            + " Zona nvarchar (3),Setor nvarchar (3),Roteiro nvarchar (3),Email "
            + " nvarchar (40),PedidoPendente boolean,"
            + " Enviado boolean,DataEnvio timestamp,Visivel boolean, StatusBloqueio nvarchar(1),"
            + " Flag nvarchar(1), CodRepresentante integer, CodigoContribuinte integer, Numero integer default 0, " +
            "StatusCm Integer Default 0);";

    private static final String sqlComissao = "CREATE TABLE IF NOT EXISTS Comissao("
            + " Tipo smallint NOT NULL,Periodo nvarchar(40) NOT NULL,"
            + " TotalVendas double,TotalComissao double,Primary Key(Tipo, Periodo));";

    private static final String sqlCondicao = "CREATE TABLE IF NOT EXISTS Condicao("
            + " IdCondicao smallint NOT NULL,IdFilial smallint,IdEmpresa smallint,"
            + " Descricao nvarchar(30),TaxaFinanceira double,"
            + " LimiteMinimoPedido double,IdTabela smallint NOT NULL, TipoDocumento nvarchar(2),"
            + " QtdeDiasPrazo smallint,PercDescontoMaximo double,QtdeParcelas smallint,"
            + " CondicaoEspecial boolean,Primary Key(IdCondicao, IdTabela));";

    private static final String sqlCondicaoPagamentoCliente = "CREATE TABLE IF NOT EXISTS CondicaoPagamentoCliente(IdCliente INTEGER NOT NULL, IdCondicao SMALLINT NOT NULL, PRIMARY KEY(IdCliente, IdCondicao));";

    private static final String sqlCondicaoProduto = "CREATE TABLE IF NOT EXISTS"
            + " CondicaoProduto (IdCondicao smallint NOT NULL, IdProduto integer NOT NULL,"
            + " PRIMARY KEY(IdCondicao, IdProduto));";

    private static final String sqlContato = "CREATE TABLE IF NOT EXISTS Contato("
            + " IdCliente integer NOT NULL, Nome nvarchar(20) NOT NULL,Hobby nvarchar(15),"
            + " Time nvarchar(15), DDD nvarchar(4), Telefone nvarchar(8), Aniversario Date,"
            + " Cargo nvarchar(15),Email nvarchar(40), Enviado boolean, DataEnvio timestamp, CodRepresentante integer , "
            + " Primary Key(IdCliente, Nome));";

    private static final String sqlComissaoV = "CREATE TABLE IF NOT EXISTS ComissaoV" +
            "(IdTabelaPreco smallint not null,IdProduto integernot null," +
            " DescontoFinal double, DescontoInicial double,Comissao double);";

    private static final String sqlDescontoF = "CREATE TABLE IF NOT EXISTS DescontoF"
            + " (IdFornecedor smallint NOT NULL, IdCliente integer NOT NULL,"
            + " Desconto double, PRIMARY KEY (IdFornecedor, IdCliente));";

    private static final String sqlDescontoS = "CREATE TABLE IF NOT EXISTS DescontoS"
            + " (IdCliente integer NOT NULL, Linha smallint NOT NULL, Secao smallint NOT NULL,"
            + " Grupo smallint NOT NULL, SubGrupo smallint NOT NULL, Desconto double,"
            + " Primary Key (IdCliente, Linha, Secao, Grupo, SubGrupo));";

    private static final String sqlEmpresaFilial = "CREATE TABLE IF NOT EXISTS EmpresaFilial ("
            + " IdEmpresa smallint NOT NULL, IdFilial smallint NOT NULL,"
            + " Descricao nvarchar(25),Primary Key (IdEmpresa, IdFilial))";

    private static final String sqlEnderecoCobranca = "CREATE TABLE IF NOT EXISTS "
            + " EnderecoCobranca(IdCliente integer NOT NULL Primary Key,"
            + " TipoEndereco nvarchar(20), Endereco nvarchar(60), "
            + " Complemento nvarchar(60), Bairro nvarchar(20), Cidade nvarchar(30),"
            + " Uf nvarchar(2),Cep integer, Enviado boolean, DataEnvio timestamp);";

    private static final String sqlFormaPagamento = "CREATE TABLE IF NOT EXISTS FormaPagamento("
            + " IdFormaPagamento integer NOT NULL Primary Key,"
            + " Descricao nvarchar(50),"
            + " LimiteMinimo double,"
            + "AmarraCondicao INTEGER DEFAULT 0);";

    private static final String sqlFormaPagamentoCliente = "CREATE TABLE IF NOT EXISTS FormaPagamentoCliente (IdCliente INTEGER NOT NULL, IdFormaPagamento INTEGER NOT NULL, PRIMARY KEY (IdCliente, IdFormaPagamento));";

    private static final String sqlFornecedor = "CREATE TABLE IF NOT EXISTS Fornecedor("
            + " IdFornecedor integer Primary Key NOT NULL, Descricao nvarchar(25));";

    private static final String sqlGorduraF = "CREATE TABLE IF NOT EXISTS GorduraF "
            + " (IdFornecedor smallint NOT NULL PRIMARY KEY, DescontoGordura double,"
            + " MaximoDesconto double);";

    private static final String sqlGorduraS = "CREATE TABLE IF NOT EXISTS GorduraS(IdFornecedor "
            + " smallint NOT NULL, Secao smallint NOT NULL, Grupo smallint NOT NULL,"
            + " SubGrupo smallint NOT NULL,DescontoGordura double, MaximoDesconto "
            + " double, Primary Key (IdFornecedor, Secao, Grupo, SubGrupo));";

    private static final String sqlGrupoProduto = "CREATE TABLE IF NOT EXISTS GrupoProduto (IdGrupo INTEGER NOT NULL PRIMARY KEY, Descricao VARCHAR(30), "
            + "CorRgbHex VARCHAR(6))";

    private static final String sqlItem = "CREATE TABLE IF NOT EXISTS Item(IdPedido"
            + " integer Not Null,IdProduto integer Not Null,NrItem smallint,Quantidade"
            + " double,Desconto double,ValorDesconto double,ValorUtilizouFlex double,"
            + " ValorGeraraFlex double,ValorGerandoFlex double,ValorUnitOriginal double,"
            + " ValorUnitPraticado double,TotalGeral double,TotalLiquido double, Rentabilidade double,"
            + " DataPedido TimeStamp, Descricao nvarchar(80), CodigoBarra nvarchar(30), CodigoNCM nvarchar(20),"
            + " PercentualDescontoCombo double DEFAULT 0, QuantidadeItemLimiteCombo double DEFAULT 0,"
            + " ItemComboAtivado integer DEFAULT 0, qntdCortada integer DEFAULT 0, totalFaturadoItem double DEFAULT 0, descontoFaturado double DEFAULT 0, "
            + " status nvarchar(50), Primary Key(IdPedido, IdProduto));";


    private static final String sqlLimite = "CREATE TABLE IF NOT EXISTS LIMITE(IdCliente integer "
            + " Primary Key NOT NULL, LimiteTotal double,LimiteDisponivel double);";

    //LASTRO--
    private static final String sqlMarca = "CREATE TABLE IF NOT EXISTS Marca("
            + " IdMarca integer Primary Key NOT NULL,Descricao nvarchar(25))";

    private static final String sqlNaoVenda = "CREATE TABLE IF NOT EXISTS NaoVenda(IdNaoVenda "
            + " smallint NOT NULL,Descricao nvarchar(60));";

    private static final String sqlNaoVendaMovimento = "CREATE TABLE IF NOT EXISTS NaoVendaMovimento("
            + " IdCliente integer NOT NULL,Motivo nvarchar(60), DataHora TimeStamp NOT NULL, Enviado "
            + " boolean, DataEnvio timestamp, Primary Key(IdCliente, DataHora));";

    private static final String sqlObjetivo = "CREATE TABLE IF NOT EXISTS Objetivo(Tipo smallint NOT NULL,"
            + " Descricao nvarchar(40) NOT NULL,Objetivo double,Realizado double);";

    private static final String sqlObservacaoCliente = "CREATE TABLE IF NOT EXISTS ObservacaoCliente"
            + "(IdCliente integer Not Null,Observacao nvarchar(180));";

    private static final String sqlParametro = "CREATE TABLE IF NOT EXISTS Parametro(IdVendedor "
            + " smallint Primary Key NOT NULL,Nome nvarchar(50),DDD nvarchar(2),Telefone nvarchar(8),"
            + " IdEmpresaTabPadrao smallint, IdEmpresaClienteNovo smallint,IdFilialTabPadrao smallint,IdFilialClienteNovo smallint,"
            + " LimiteMinimoPedido double,QtdeDiasGuardarPedido smallint,QtdeMaximaItens smallint,"
            + " QtdeDiasBloquearInadimplente smallint,OrdemListaProdutos smallint,"
            + " PermiteIncluirCliente boolean,QtdeBonifAbateVerba boolean,PosicaoCodClienteConsulta"
            + " smallint,TabelaPrecoClienteNovo smallint,UtilizaImpressora boolean,DescontoExtra "
            + " smallint,DataFaturamento boolean,ControlaLimiteCredito boolean,PercMaximoDescontoFlex"
            + " double,ValorMaximoExcederVerba double,SaldoVerba double,ControlaTabelaPrecoCliente "
            + " boolean,MotivoNaoVenda smallint,PermiteUnitarioSugestaoVenda boolean,"
            + " PercDescontoProdutoGeraVerba boolean,GeraVerbaPalmtop smallint,DadosAdicionais smallint,"
            + " UtilizaTroca smallint,PermiteAlterarPreco boolean,ExcessoPrecoGeraFlex boolean,"
            + " IncluiEmbalagem smallint,DataUltimaRecepcao TimeStamp, DataUltimoAcesso TimeStamp, DescontoProdutoVendedor double,"
            + "VisualizarRentabilidade boolean, PercentualRentabilidadeMinimaDoPedido double, PercRentabilidadeMediaDoPedido double,"
            + " Supervisor boolean Default 0, PercentualBonificacao double, DataExpiraLiberacao TimeStamp, "
            + "SolicitarSenhaRentabilidade boolean Default 0, Ativo boolean, idVendedorCM smallint, "
            + "DataUltimaVerificacaoCM TimeStamp, TokenApi nvarchar(150), VisualizarBotaoContato boolean Default 0, "
            + "QuantidadeMinimaItensRentabilidade smallint, IdSequencialPedido smallint Default 0);";

    private static final String sqlPedido = "CREATE TABLE IF NOT EXISTS Pedido(IdCliente integer NOT NULL,"
            + " IdPedido integer Primary Key Not Null,PedidoCliente nvarchar(15),IdTPedido smallint,"
            + " IdTabela smallint,IdCondicao smallint,NrItens smallint,IdFormaPagamento integer, DescontoExtra double,"
            + " ValorUtilizouFlex double,ValorGeraraFlex double,TotalGeral double,TotalLiquido double,"
            + " Observacao nvarchar(240),DataPedido TimeStamp,DataFaturamento Date,SugestaoVenda boolean,"
            + " DescontoCliente double,DesconsiderarFlex boolean,Enviado boolean,DataEnvio timestamp, "
            + "IdEmpresa smallint , IdFilial smallint, Aberto boolean, status boolean, Rentabilidade double, PercRentabilidadeLiberado double, "
            + "VinculoPedido integer, VinculoBonificacao integer, SaldoBonificar double, StatusPedidoGWeb integer, "
            + "CodPedidoGWeb integer,Combo INTEGER DEFAULT 0, RetornoStatusPedido nvarchar(50), RetornoNumeroNotaFiscal nvarchar(50), "
            + "HistoricoPedidoCM boolean Default 0, TotalFaturado double Default 0, statusFaturado nvarchar(50));";

    private static final String sqlPendencia = "CREATE TABLE IF NOT EXISTS Pendencia(IdCliente integer NOT NULL,"
            + " NrTitulo integer not null,TipoDocumento nvarchar(2),TaxaCorrecaoDiaria double,Vencimento Date,"
            + " Portador nvarchar(20),Valor double,Saldo double, Primary Key(IdCliente, NrTitulo, Vencimento));";

    private static final String sqlPlanoVisita = "CREATE TABLE IF NOT EXISTS PlanoVisita(QuantidadeDias integer,"
            + " Descricao nvarchar(20),Flag nvarchar(1));";

    private static final String sqlPreco = "CREATE TABLE IF NOT EXISTS Preco(IdProduto integer NOT NULL,"
            + "IdEmpresa smallint,IdFilial smallint,"
            + " IdTabela smallint NOT NULL,CodigoEmbalagem smallint,PrecoNormal double,PrecoMinimo double,"
            + " Primary Key(IdProduto, IdTabela));";

    private static final String sqlProduto = "CREATE TABLE IF NOT EXISTS Produto(IdProduto integer Primary Key NOT NULL,"
            + " IdFornecedor smallint,Descricao nvarchar(80),CodigoEmbalagem smallint,DescricaoEmbalagem nvarchar(10),"
            + " QuantidadeEmbalagem integer,IdSecao smallint,IdICMS smallint,CodigoBarra nvarchar(30),IdSubgrupo smallint,"
            + " ControleGordura nvarchar(1),DescontoGordura double,Multiplo smallint,IdGrupo smallint,IdLinha smallint,"
            + " EstoqueBaixo smallint,QuantidadeCaixa smallint,SaldoEstoque nvarchar(6),PermiteVender boolean,Peso double,"
            + " Status nvarchar(1),PermiteMeiaCaixa boolean,CodigoReferencia nvarchar(20),PermiteBonificacao boolean,"
            + " MaximoDesconto double, Unitario smallint, DivisaoPreco integer, IdMarca integer, PossuiFoto boolean,"
            + "PercentualRentabilidadeMinima double, PercentualRentabilidadeMedia double, PrecoCustoProduto double, CodigoNCM nvarchar(20) Default 0);";

    private static final String sqlRamo = "CREATE TABLE IF NOT EXISTS Ramo(IdRamo smallint NOT NULL PRIMARY KEY, Descricao nvarchar(31));";

    private static final String sqlSituacaoEspecial = "CREATE TABLE IF NOT EXISTS SitEspecial(IdProduto integer NOT NULL,"
            + " IdFornecedor integer not null,Tipo varchar(1) not null,Quantidade1 integer,DescontoMaximo1 double,"
            + " Quantidade2 integer,DescontoMaximo2 double,Quantidade3 integer,DescontoMaximo3 double,"
            + " Quantidade4 integer,DescontoMaximo4 double,Quantidade5 integer,DescontoMaximo5 double,"
            + " Quantidade6 integer,DescontoMaximo6 double,Quantidade7 integer,DescontoMaximo7 double,"
            + " Primary Key(IdProduto, IdFornecedor, Tipo));";

    private static final String sqlTabelaCliente = "CREATE TABLE IF NOT EXISTS TabelaCliente(IdCliente "
            + " integer not null,IdTabela smallint NOT NULL, Primary Key(IdCliente, IdTabela));";

    private static final String sqlTabelaClienteNovo = "CREATE TABLE IF NOT EXISTS TabelaClienteNovo("
            + " IdTabela smallint);";

    private static final String sqlTabela = "CREATE TABLE IF NOT EXISTS Tabela("
            + " IdTabela smallint NOT NULL, IdEmpresa smallint NOT NULL,IdFilial smallint NOT NULL, "
            + " Descricao nvarchar(25),DataInicial date,DataFinal date,PermiteDescontoPorTabela boolean,"
            +" ultilizaRentabilidade boolean, Primary Key (IdTabela, IdEmpresa, IdFilial))";

    private static final String sqlTipoDocumentoCliente = "CREATE TABLE IF NOT EXISTS TipoDocumentoCliente(IdCliente "
            + " integer NOT NULL,IdTipoDocumento nvarchar(3) NOT NULL);";

    private static final String sqlTipoDocumento = "CREATE TABLE IF NOT EXISTS TipoDocumento(IdTipoDocumento "
            + " nvarchar(3) NOT NULL,Descricao nvarchar(40),OndeMostrar smallint);";

    private static final String sqlTipoPedido = "CREATE TABLE IF NOT EXISTS TipoPedido(IdTipoPedido smallint "
            + " Primary Key NOT NULL, Descricao nvarchar(20), AbaterSaldoVerba smallint, ValorMinimoPedido double, TipoOperacao nvarchar(1));";

    private static final String sqlTipoTroca = "CREATE TABLE IF NOT EXISTS TipoTroca (IdTipoTroca smallint "
            + " NOT NULL PRIMARY KEY, Descricao nvarchar(20));";

    private static final String sqlTroca = "CREATE TABLE IF NOT EXISTS Troca(IdTroca integer NOT NULL PRIMARY KEY, IdCliente"
            + " integer, Data timestamp, IdTabela smallint, IdCondicao smallint, TipoTroca smallint, NFOrigem "
            + " integer, ValorMercadoriaBruto double, ValorMercadoriaLiquido double, Indenizacao double,"
            + " ValorLiquidoTroca double, NumeroItens smallint, NrPedidoVenda integer, Observacao nvarchar(240),"
            + " Enviado boolean, DataEnvio timestamp);";

    private static final String sqlUltima = "CREATE TABLE IF NOT EXISTS Ultima(IdPedido integer NOT NULL, IdPedidoPalmtop "
            + " integer,IdCliente integer,Data Date NOT NULL, DataFaturamento Date, TotalUltimaCompra double,Total double, StatusPedido nvarchar(50),"
            + " NumeroNotaFiscal nvarchar(50),Primary Key(IdPedido, Data));";

    private static final String sqlUltimaItem = "CREATE TABLE IF NOT EXISTS UltimaItem(IdPedido integer NOT NULL,"
            + " IdProduto integer NOT NULL,Qtde integer,QtdeCortada integer,EstoqueDia integer,TotalItem double,"
            + " Data date, IdPedidoPalmtop integer, DescontoItem double, ValorTotalItemFaturado double, DescontoItemFaturado double,"
            + " StatusItem nvarchar(50),Primary Key(IdPedido, IdProduto));";

    private static final String sqlUsuario = "CREATE TABLE IF NOT EXISTS Usuario(IdUsuario smallint Not Null Primary Key,Nome nvarchar(100),"
            + " IdVendedor smallint,Senha nvarchar(100))";

    private static final String sqlValidadeTabela = "CREATE TABLE IF NOT EXISTS ValidadeTabela (DataInicial Date, DataFinal Date);";

    private static final String sqlVendaMesFornecedor = "CREATE TABLE IF NOT EXISTS VendaMesFornecedor(Fornecedor nvarchar(20) "
            + " NOT NULL Primary Key,Vendas double,Tonelagem double);";

    private static final String sqlVendaVendedor = "CREATE TABLE IF NOT EXISTS VendaVendedor(Periodo nvarchar(20) NOT NULL "
            + " Primary Key,TotalVendas double,TotalTonelagem double);";

    private static final String sqlTabelaPrecoPorOperacao = " CREATE TABLE IF NOT EXISTS TabelaPrecoPorOperacao(IdTabela INTEGER NOT NULL, IdTipoPedido INTEGER NOT NULL, "
            + " PRIMARY KEY(IdTabela, IdTipoPedido)); ";

    private static final String sqlTipoPedidoTipoContribuinte = " CREATE TABLE IF NOT EXISTS TipoPedidoTipoContribuinte(CodTipoPedido INTEGER NOT NULL, CodTipoContribuinte INTEGER NOT NULL, "
            + " PRIMARY KEY(CodTipoPedido, CodTipoContribuinte)); ";

    private static final String sqlTabelaRestricaoTCT = "CREATE TABLE IF NOT EXISTS RestricaoTCT (IdTipoPedido integer NOT NULL, IdCondicaoPagamento integer NOT NULL, IdFormaPagamento integer NOT NULL)";

    private static final String sqlNotificacao = "CREATE TABLE IF NOT EXISTS Notificacao(IdNotificacao integer primary key ,DataInicial Date, DataFinal Date, "
            + "FrequanciaNotificacao INTEGER, IdPedido INTEGER, Lido boolean, DescricaoNotificacao nvarchar(120));";

    public DBHelper(Context context) {
        super(context, BANCO, null, VERSION_DB);
        ctx = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlAdicionais);
        db.execSQL(sqlAliquotaICMSDAO);
        db.execSQL(sqlCidades);
        db.execSQL(sqlCliente);
        db.execSQL(sqlComissao);
        db.execSQL(sqlComissaoV);
        db.execSQL(sqlCondicao);
        db.execSQL(sqlCondicaoProduto);
        db.execSQL(sqlCondicaoPagamentoCliente);
        db.execSQL(sqlContato);
        db.execSQL(sqlDescontoF);
        db.execSQL(sqlDescontoS);
        db.execSQL(sqlEnderecoCobranca);
        db.execSQL(sqlEmpresaFilial);
        db.execSQL(sqlFormaPagamento);
        db.execSQL(sqlFormaPagamentoCliente);
        db.execSQL(sqlFornecedor);
        db.execSQL(sqlGorduraF);
        db.execSQL(sqlGorduraS);
        db.execSQL(sqlGrupoProduto);
        db.execSQL(sqlItem);
        db.execSQL(sqlLimite);
        db.execSQL(sqlMarca);
        db.execSQL(sqlNaoVenda);
        db.execSQL(sqlNaoVendaMovimento);
        db.execSQL(sqlObjetivo);
        db.execSQL(sqlObservacaoCliente);
        db.execSQL(sqlParametro);
        db.execSQL(sqlPedido);
        db.execSQL(sqlPendencia);
        db.execSQL(sqlPlanoVisita);
        db.execSQL(sqlPreco);
        db.execSQL(sqlProduto);
        db.execSQL(sqlRamo);
        db.execSQL(sqlSituacaoEspecial);
        db.execSQL(sqlTabela);
        db.execSQL(sqlTabelaCliente);
        db.execSQL(sqlTabelaClienteNovo);
        db.execSQL(sqlTabelaPrecoPorOperacao);
        db.execSQL(sqlTipoDocumento);
        db.execSQL(sqlTipoDocumentoCliente);
        db.execSQL(sqlTipoPedido);
        db.execSQL(sqlTipoTroca);
        db.execSQL(sqlTroca);
        db.execSQL(sqlUltima);
        db.execSQL(sqlUltimaItem);
        db.execSQL(sqlUsuario);
        db.execSQL(sqlValidadeTabela);
        db.execSQL(sqlVendaMesFornecedor);
        db.execSQL(sqlVendaVendedor);
        db.execSQL(sqlTipoPedidoTipoContribuinte);
        db.execSQL(sqlTabelaRestricaoTCT);
        db.execSQL(sqlCombo);
        db.execSQL(sqlRegraCombo);
        db.execSQL(sqlComboXPedido);
        db.execSQL(sqlComboXCliente);
        db.execSQL(sqlRegraDesconto);
        db.execSQL(sqlNotificacao);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                db.execSQL("DELETE FROM RAMO;");
                db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS idx_idramo ON Ramo (IdRamo)");
            case 2:
                try {
                    db.execSQL("ALTER TABLE Produto RENAME TO Produto_Temp;");
                    db.execSQL(sqlProduto);
                    db.execSQL("INSERT INTO Produto SELECT * FROM Produto_TEMP;");
                    db.execSQL("DROP TABLE Produto_Temp;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 1 - > 2", e.getMessage());
                }
            case 3: {
                db.execSQL("ALTER TABLE Cliente ADD COLUMN Flag nvarchar(1);");
                db.execSQL("ALTER TABLE Cliente ADD COLUMN CodRepresentante integer;");
                db.execSQL("ALTER TABLE Contato ADD COLUMN CodRepresentante integer;");
            }
            case 4:
                try {
                    db.execSQL("ALTER TABLE Produto RENAME TO Produto_Temp;");
                    db.execSQL(sqlProduto);
                    db.execSQL("INSERT INTO Produto SELECT * FROM Produto_TEMP;");
                    db.execSQL("DROP TABLE Produto_Temp;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 1 - > 2", e.getMessage());
                }
            case 5:
                try {
                    db.execSQL("ALTER TABLE TipoPedido ADD COLUMN ValorMinimoPedido double;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 1 - > 2", e.getMessage());
                }
            case 6:
                try {
                    db.execSQL(sqlTipoPedidoTipoContribuinte);
                    db.execSQL("ALTER TABLE Cliente ADD COLUMN CodigoContribuinte integer;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN DescontoProdutoVendedor double;");
                    db.execSQL(sqlTabelaRestricaoTCT);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 1 - > 2", e.getMessage());
                }
            case 7:
                try {
                    db.execSQL("ALTER TABLE FormaPagamento ADD COLUMN LimiteMinimo double;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 7 - > 8", e.getMessage());
                }

            case 8:
                try {
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN Aberto boolean;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 8 - > 9", e.getMessage());
                }
            case 9:
                try {
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN status boolean;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 9 - > 10", e.getMessage());
                }
            case 10:
                try {
                    db.execSQL("ALTER TABLE Produto RENAME TO Produto_Temp;");
                    db.execSQL(sqlProduto);
                    db.execSQL("INSERT INTO Produto SELECT * FROM Produto_TEMP;");
                    db.execSQL("DROP TABLE Produto_Temp;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 10 - > 11", e.getMessage());
                }
            case 11:
                try {
                    db.execSQL("ALTER TABLE Produto ADD COLUMN PossuiFoto boolean;");
                    db.execSQL("ALTER TABLE Produto RENAME TO Produto_Temp;");
                    db.execSQL(sqlProduto);
                    db.execSQL("INSERT INTO Produto SELECT * FROM Produto_TEMP;");
                    db.execSQL("DROP TABLE Produto_Temp;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 11 - > 12", e.getMessage());
                }
            case 12:
                try {
                    db.execSQL("ALTER TABLE Tabela ADD COLUMN PermiteDescontoPorTabela boolean Default 0;");
                    db.execSQL("ALTER TABLE Produto ADD COLUMN PercentualRentabilidadeMinima double;");
                    db.execSQL("ALTER TABLE Produto ADD COLUMN PercentualRentabilidadeMedia double;");
                    db.execSQL("ALTER TABLE Produto ADD COLUMN PrecoCustoProduto double;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN PercentualRentabilidadeMinimaDoPedido double;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN PercRentabilidadeMediaDoPedido double;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN VisualizarRentabilidade boolean Default 0;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN PercRentabilidadeLiberado double;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN Rentabilidade double;");
                    db.execSQL("ALTER TABLE Item ADD COLUMN Rentabilidade double;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN Supervisor boolean Default 0;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN PercentualBonificacao double;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN VinculoPedido integer;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN VinculoBonificacao integer;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN SaldoBonificar double;");
                    db.execSQL("ALTER TABLE TipoPedido ADD COLUMN TipoOperacao nvarchar(1);");

                    db.execSQL("ALTER TABLE Produto RENAME TO Produto_Temp;");
                    db.execSQL(sqlProduto);
                    db.execSQL("INSERT INTO Produto SELECT * FROM Produto_TEMP;");
                    db.execSQL("DROP TABLE Produto_Temp;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 12 - > 13", e.getMessage());
                }
            case 13:
                try {
                    db.execSQL("ALTER TABLE Tabela ADD COLUMN ultilizaRentabilidade boolean Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 13 - > 14", e.getMessage());
                }

            case 14:
                try {
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN DataExpiraLiberacao TimeStamp;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN SolicitarSenhaRentabilidade boolean Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 14 - > 15", e.getMessage());
                }
            case 15:
                try {
                    db.execSQL("Update Parametro set SolicitarSenhaRentabilidade = 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 15 - > 16", e.getMessage());
                }
            case 16:
                try {
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN StatusPedidoGWeb;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN CodPedidoGWeb;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN TokenApi nvarchar(150);");
                    db.execSQL("ALTER TABLE Cliente ADD COLUMN Numero integer default 0;");
                }catch (Exception e){
                    Log.e(getClass().getSimpleName() + ": atualizar banco 16 - > 17", e.getMessage());
                }
            case 17:
                try {
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN VisualizarBotaoContato boolean Default 0;");

                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 17 - > 18", e.getMessage());
                }
            case 18:
                try {
                    db.execSQL("ALTER TABLE Produto ADD COLUMN CodigoNCM nvarchar(20) Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 18 - > 19", e.getMessage());
                }
            case 19:
                try {
                    db.execSQL("ALTER TABLE Item ADD COLUMN CodigoBarra nvarchar(30) Default 0;");
                    db.execSQL("ALTER TABLE Item ADD COLUMN Descricao nvarchar(80)");
                    db.execSQL("ALTER TABLE Item ADD COLUMN CodigoNCM nvarchar(20) Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 18 - > 19", e.getMessage());
                }
            case 20:
                try {

                    db.execSQL("ALTER TABLE Parametro ADD COLUMN QuantidadeMinimaItensRentabilidade smallint;");

                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizar banco 19 - > 20", e.getMessage());
                }
            case 21:
                try {
                    Log.i("Combo", "Combo: " + sqlCombo);
                    db.execSQL(sqlCombo);
                    db.execSQL(sqlRegraCombo);
                    Log.i("RegraCombo", "Combo: " + sqlRegraCombo);
                    db.execSQL(sqlComboXPedido);
                    db.execSQL(sqlComboXCliente);
                    db.execSQL(sqlRegraDesconto);
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN Combo INTEGER DEFAULT 0");
                    Log.i("Pedido", "Combo: ");
                    db.execSQL("ALTER TABLE Item ADD COLUMN PercentualDescontoCombo double DEFAULT 0");
                    Log.i("Item", "PercentualDescontoCombo: ");
                    db.execSQL("ALTER TABLE Item ADD COLUMN QuantidadeItemLimiteCombo double DEFAULT 0");
                    Log.i("Item", "QuantidadeItemLimiteCombo: ");
                    db.execSQL("ALTER TABLE Item ADD COLUMN ItemComboAtivado integer DEFAULT 0");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN Ativo boolean;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN idVendedorCM smallint;");
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN DataUltimaVerificacaoCM TimeStamp;");
                    db.execSQL("ALTER TABLE FormaPagamento ADD COLUMN AmarraCondicao INTEGER DEFAULT 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 20 - > 21", e.getMessage());
                }

            case 22:
                try {
                    db.execSQL(sqlNotificacao);
                    db.execSQL("ALTER TABLE Ultima ADD COLUMN StatusPedido nvarchar(50);");
                    db.execSQL("ALTER TABLE Ultima ADD COLUMN NumeroNotaFiscal nvarchar(50);");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN RetornoStatusPedido nvarchar(50);");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN RetornoNumeroNotaFiscal nvarchar(50);");

                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 21 - > 22", e.getMessage());
                }
            case 23:
                try {
                    db.execSQL("ALTER TABLE Parametro ADD COLUMN IdSequencialPedido smallint Default 0;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN HistoricoPedidoCM boolean Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 22 - > 23", e.getMessage());
                }
            case 24:
                try {
                    db.execSQL("ALTER TABLE Ultima ADD COLUMN TotalUltimaCompra double;");
                    db.execSQL("ALTER TABLE Ultima ADD COLUMN DataFaturamento Date;");
                    db.execSQL("ALTER TABLE UltimaItem ADD COLUMN IdPedidoPalmtop integer;");
                    db.execSQL("ALTER TABLE UltimaItem ADD COLUMN DescontoItem double;");
                    db.execSQL("ALTER TABLE UltimaItem ADD COLUMN ValorTotalItemFaturado double;");
                    db.execSQL("ALTER TABLE UltimaItem ADD COLUMN DescontoItemFaturado double;");
                    db.execSQL("ALTER TABLE UltimaItem ADD COLUMN StatusItem nvarchar(50);");

                    db.execSQL("ALTER TABLE Item ADD COLUMN qntdCortada integer;");
                    db.execSQL("ALTER TABLE Item ADD COLUMN totalFaturadoItem double;");
                    db.execSQL("ALTER TABLE Item ADD COLUMN descontoFaturado double;");
                    db.execSQL("ALTER TABLE Item ADD COLUMN status nvarchar(50);");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN TotalFaturado double;");
                    db.execSQL("ALTER TABLE Pedido ADD COLUMN statusFaturado nvarchar(50);");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 23 - > 24", e.getMessage());
                }
            case 25:
                try {
                    db.execSQL("ALTER TABLE Cliente ADD COLUMN StatusCm Integer Default 0;");
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName() + ": atualizando banco 22 - > 23", e.getMessage());
                }

            default:
                break;
        }


    }

}