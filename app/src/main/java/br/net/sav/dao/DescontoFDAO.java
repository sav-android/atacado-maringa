package br.net.sav.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.DescontoF;

public class DescontoFDAO extends PadraoDAO{
	private static final String TABELA = "DescontoF";
	
	public DescontoFDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public void importar(List<String> linhasTexto) {
		try{
			deleteAll();			
			try {
				db.beginTransaction();
				
				for(String linha: linhasTexto) {
					DescontoF d = new DescontoF();
					
					if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
						d.setIdFornecedor(Short.parseShort(linha.substring(0, 8).trim()));
					else {
						Utils.gravarLog("\nDEF: " + linha);
						continue;
					}
					
					if(linha.substring(8, 16).trim().matches(MATCHES) && !linha.substring(8, 16).trim().equals(""))
						d.setIdCliente(Long.parseLong(linha.substring(8, 16).trim()));
					else {
						Utils.gravarLog("\nDEF: " +linha);
						continue;
					}											
					if(linha.substring(16, 22).trim().equals(""))
						d.setDesconto(0);
					else if(linha.substring(16, 22).trim().matches(MATCHES))
						d.setDesconto(Double.parseDouble(linha.substring(16, 22).trim()) / 10000);
					else {
						Utils.gravarLog("\nDEF: " +linha);
						continue;
					}					
					if(!exists(d))
						insert(d);
				}
				
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
			
		} catch (Exception e) {
			Log.d("DescontoFDAO", e.getMessage());
		}
	}

}
