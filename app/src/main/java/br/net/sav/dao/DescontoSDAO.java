package br.net.sav.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.DescontoS;

public class DescontoSDAO extends PadraoDAO{
	private static final String TABELA ="DescontoS";
	
	public DescontoSDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}

	public void importar(List<String> linhasTexto) {
		try{
			deleteAll();
			try {
				db.beginTransaction();
				
				for(String linha : linhasTexto) {
					DescontoS d = new DescontoS();
					
					if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
						d.setIdCliente(Long.parseLong(linha.substring(0, 8).trim()));
					else {
						Utils.gravarLog("\nDES :" + linha);
						continue;
					}
					if(linha.substring(8, 12).trim().equals(""))
						d.setLinha((short) 0);
					else if(linha.substring(8, 12).trim().matches(MATCHES))
						d.setLinha(Short.parseShort(linha.substring(8, 12).trim()));
					else {
						Utils.gravarLog("\nDES :" +linha);
						continue;
					}
					if(linha.substring(12, 16).trim().equals(""))
						d.setSecao((short) 0);
					else if(linha.substring(12, 16).trim().matches(MATCHES))
						d.setSecao(Short.parseShort(linha.substring(12, 16).trim()));
					else {
						Utils.gravarLog("\nDES :" + linha);
						continue;
					}
					if(linha.substring(16, 20).trim().equals(""))
						d.setGrupo((short) 0);
					else if(linha.substring(16, 20).trim().matches(MATCHES))
						d.setGrupo(Short.parseShort(linha.substring(16, 20).trim()));
					else {
						Utils.gravarLog("\nDES :" +linha);
						continue;
					}
					if(linha.substring(20, 24).trim().equals(""))
						d.setGrupo((short) 0);
					else if(linha.substring(20, 24).trim().matches(MATCHES))
						d.setSubGrupo(Short.parseShort(linha.substring(20, 24).trim()));
					else {
						Utils.gravarLog("\nDES :" +linha);
						continue;
					}
					if(linha.substring(24, 30).trim().equals(""))
						d.setDesconto(0);
					else if(linha.substring(24, 30).trim().matches(MATCHES))
						d.setDesconto(Double.parseDouble(linha.substring(24, 30).trim()) / 10000);
					else {
						Utils.gravarLog("\nDES :" +linha);
						continue;
					}
					if(exists(d))
						update(d);
					else
						insert(d);					
				}
				
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		} catch (Exception e) {
			Log.d("DescontOSDAO", e.getMessage());
		}
	}
}
