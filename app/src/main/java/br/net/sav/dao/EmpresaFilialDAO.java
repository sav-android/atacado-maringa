package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.EmpresaFilial;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class EmpresaFilialDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA="EmpresaFilial";
	
	public EmpresaFilialDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public List<EmpresaFilial> getAll() {
		new EmpresaFilialDAO(ctx, db);
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		List<EmpresaFilial> lista = new ArrayList<EmpresaFilial>();
		
		EmpresaFilial eFilial = null ;
		
		Cursor c = null ;
		
		try {
			c = dbLocal.query(TABELA, colunas, null, null, null, null, null) ;
			
			while(c.moveToNext()) {
				eFilial = new EmpresaFilial();
				eFilial.setIdEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
				eFilial.setIdFilial(c.getShort(c.getColumnIndex("IdFilial")));
				eFilial.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				lista.add(eFilial);
			}
		} catch (Exception e) {
			Log.e("EmpresaFiliais.", e.getMessage());
		} finally {
			c.close();
		//	if(db == null)
				//db.close(); singletone();
		}
		return lista ;
	}

	public EmpresaFilial getFilial() {
		new EmpresaFilialDAO(ctx, db);
		SQLiteDatabase dbLocal = null ;

		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;


		EmpresaFilial eFilial = null ;

		Cursor c = null ;

		try {
			c = dbLocal.query(TABELA, colunas, null, null, null, null, null) ;

			if (c.moveToFirst()) {
				eFilial = new EmpresaFilial();
				eFilial.setIdEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
				eFilial.setIdFilial(c.getShort(c.getColumnIndex("IdFilial")));
				eFilial.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				return eFilial;
			}
		} catch (Exception e) {
			Log.e("EmpresaFilial", e.getMessage());
		} finally {
			c.close();
			//	if(db == null)
			//db.close(); singletone();
		}
		return null ;
	}
	
	public void importar(List<String> linhasTexto) {
		try{
			deleteAll();
			getimportacaoEmpresaFilial(linhasTexto);
		} catch (Exception e) {
			Log.d("EmpresaFilialDAO", e.getMessage());
		}
	}

	private void getimportacaoEmpresaFilial(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaEmpresaFilialImportar(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaEmpresaFilialImportar(String linha) {
		EmpresaFilial e = new EmpresaFilial() ;

		if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
			e.setIdEmpresa(Short.parseShort(linha.substring(0, 4)));
		else {
			Utils.gravarLog("\nEMP: " +linha);
			return true;
		}
		if(linha.substring(4, 8).trim().matches(MATCHES) && !linha.substring(4, 8).trim().equals(""))
			e.setIdFilial(Short.parseShort(linha.substring(4, 8).trim()));
		else {
			Utils.gravarLog("\nEMP: " +linha);
			return true;
		}

		e.setDescricao(linha.substring(8,58));

		if (!exists(e))
			insert(e);
		else
			update(e);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaEmpresaFilialImportar(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_tabela_empresa);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
