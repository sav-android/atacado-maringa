package br.net.sav.dao;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.EnderecoCobranca;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class EnderecoCobrancaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "EnderecoCobranca";
	public EnderecoCobrancaDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}
	
	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		Date dataAtual = new Date();
		try {
			if (cargaCompleta)
				deleteAll();
			getImportacaoEnderecoCobranca(linhasTexto, cargaCompleta, dataAtual);
		} catch (Exception e) {
			Log.d("EnderecoCobrancaDAO", e.getMessage());
		}
	}

	private void getImportacaoEnderecoCobranca(List<String> linhasTexto, Boolean cargaCompleta, Date dataAtual) {
		try {
			db.beginTransaction();
			for (String linha : linhasTexto) {
				if (getLinhaEnderecoCobrancaImportar(linha, cargaCompleta, dataAtual)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaEnderecoCobrancaImportar(String linha, boolean cargaCompleta, Date dataAtual) {
		EnderecoCobranca e = new EnderecoCobranca();

		if (linha.substring(0, 8).trim().matches(MATCHES)&& !linha.substring(0, 8).trim().equals(""))
			e.setIdCliente(Long.parseLong(linha.substring(0, 8).trim()));
		else {
			Utils.gravarLog("\nEND: " + linha);
			return true;
		}
		e.setTipoEndereco(linha.substring(8, 28).trim());
		e.setEndereco(linha.substring(28, 88).trim());
		e.setComplemento(linha.substring(88, 148).trim());
		e.setBairro(linha.substring(148, 168).trim());
		e.setCidade(linha.substring(168, 198).trim());
		e.setUf(linha.substring(198, 200).trim());
		if (linha.substring(200, 208).trim().equals(""))
			e.setCep(0);
		else if (linha.substring(200, 208).trim().matches(MATCHES))
			e.setCep(Long.parseLong(linha.substring(200, 208)
					.trim()));
		else {
			Utils.gravarLog("\nEND: " + linha);
			return true;
		}
		e.setEnviado(true);
		e.setDataEnvio(dataAtual);

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(208))) {
				case ' ':
				case 'A':
					if (exists(e))
						update(e);
					else
						insert(e);
					break;
				case 'E':
					delete(e);
					break;
				default:
					Utils.gravarLog("\nEND(Flag Inv�lida): " + linha);
					break;
			}
		} else {
			insert(e);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaEnderecoCobrancaImportar(linha, true, new Date())) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_endereco_cobranca);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
