package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.CondicaoPagamentoCliente.OpcoesTransacao;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.FormaPagamentoCliente;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class FormaPagamentoClienteDAO implements AcaoParaImportacaoDados {
	private Context ctx ;
	private static final String TABELA = "FormaPagamentoCliente";
	private static final String[] colunas = new String[] {"IdCliente", "IdFormaPagamento"} ;
	
	public FormaPagamentoClienteDAO(Context ctx) {
		this.ctx = ctx ;
	}
	
	public List<FormaPagamento> getListaCliente(long idCliente, SQLiteDatabase db) throws SQLException {
		SQLiteDatabase dbLocal = null;
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		Cursor c = null ;
		List<FormaPagamento> listaRetorno = null;
		List<Short> listaFormasPagamentoCliente = null ;
		
		try {
			c = dbLocal.rawQuery("SELECT IdFormaPagamento FROM FormaPagamentoCliente WHERE IdCliente=?", new String[] {String.valueOf(idCliente)}) ;
			listaFormasPagamentoCliente = new ArrayList<Short>(c.getCount());
			while(c.moveToNext())
				listaFormasPagamentoCliente.add(c.getShort(0));
			
			FormaPagamentoDAO fPagamentoDAO = new FormaPagamentoDAO(ctx, dbLocal);
			if(listaFormasPagamentoCliente.size() > 0) {
				listaRetorno = new ArrayList<FormaPagamento>(c.getCount());

				for(Short idCondicao : listaFormasPagamentoCliente) {
					FormaPagamento fAux = fPagamentoDAO.get(idCondicao);
					if(fAux != null)
						listaRetorno.add(fAux);
				}
			}
		} catch (SQLException se) {
			Log.e("CondicaoPagamentoCliente", "getListaCliente - " + se.getMessage());
			throw se ;
		} finally {
			if(c != null)
				c.close();
			//if(db == null)
				//dbLocal.close(); singletone();
		}
		return listaRetorno ;
	}

	public boolean realizarTransacaoBanco(OpcoesTransacao opcao,FormaPagamentoCliente fpc, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null ;
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		Cursor c = null ;
		ContentValues ctv = null;
		
		String clausulaSql = null ;
        String[] valoresClausula = null ;
		
		if(fpc != null) {
			ctv = preencherContentValues(fpc);
			clausulaSql = "IdCliente=? AND IdFormaPagamento=?" ;
			valoresClausula = new String[] {String.valueOf(fpc.getIdCliente()), String.valueOf(fpc.getIdFormaPagamento())} ;
		}	
				
		boolean retorno = false ;
		
		try {
    		switch (opcao) {
    			case INSERIR:
    				retorno = dbLocal.insert(TABELA, null, ctv) > 0 ;
    				break;
    				
    			case ALTERAR:
    				retorno = dbLocal.update(TABELA, ctv,clausulaSql, valoresClausula) > 0 ;
    				break;
    				
    			case DELETAR :
    				retorno = dbLocal.delete(TABELA, clausulaSql, valoresClausula) > 0 ;
    				break ;
    				
    			case DELETAR_TUDO:
    				retorno = dbLocal.delete(TABELA, null, null) > 0 ;
    				break;
    				
    			case EXISTE :
    				c = dbLocal.query(TABELA, null, clausulaSql, valoresClausula, null, null, null);
    				retorno = c.moveToFirst() && c.getLong(0) > 0 ;
    				break;	
    		}
		} catch (SQLException e) {
		    retorno = false ;		    
		} finally {
		    if(c != null)
                c.close();
		}
		
		return retorno ;
	}
	
	private FormaPagamentoCliente preencherObjeto(Cursor c) {
		FormaPagamentoCliente fpc = new FormaPagamentoCliente();
		fpc.setIdCliente(c.getLong(c.getColumnIndex(colunas[0])));
		fpc.setIdFormaPagamento(c.getLong(c.getColumnIndex(colunas[1])));
		
		return fpc ;
	}
	
	private ContentValues preencherContentValues(FormaPagamentoCliente fpc) {
		ContentValues ctv = new ContentValues();
		ctv.put(colunas[0], fpc.getIdCliente());
		ctv.put(colunas[1], fpc.getIdFormaPagamento());
		
		return ctv ;
	}
	
	public void importar(List<String> linhasTexto, Boolean cargaCompleta, SQLiteDatabase dbParam) {
		SQLiteDatabase db = dbParam == null ? new DBHelper(ctx).getWritableDatabase() : dbParam;
		
		if(cargaCompleta)
			realizarTransacaoBanco(OpcoesTransacao.DELETAR_TUDO, null, db);

		getImportacaoFormaPag(linhasTexto, cargaCompleta, db);
	}

	private void getImportacaoFormaPag(List<String> linhasTexto, Boolean cargaCompleta, SQLiteDatabase db) {
		db.beginTransaction();
		try {
			for (String linha : linhasTexto) {
				getLinhaFormaPgImportar(linha, db, cargaCompleta);
			}

			db.setTransactionSuccessful();
		} catch (Exception e) {
			Log.e("FormaPagamentoClienteDAO", "importar - " + e.getMessage());
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaFormaPgImportar(String linha, SQLiteDatabase db , boolean cargaCompleta) {
		FormaPagamentoCliente fpc = new FormaPagamentoCliente();

		if(linha.substring(0, 8).trim().matches(Utils.MATCHES) && !linha.substring(0, 8).trim().equals(""))
			fpc.setIdCliente(Long.parseLong(linha.substring(0, 8)));
		else {
			Utils.gravarLog("\nFCL: " + linha);
			return true;
		}

		if(linha.substring(8, 14).trim().matches(Utils.MATCHES) && !linha.substring(8, 14).trim().equals(""))
			fpc.setIdFormaPagamento(Long.parseLong(linha.substring(8, 14).trim()));
		else {
			Utils.gravarLog("\nFCL: " + linha);
			return true;
		}

		switch (Character.toUpperCase(linha.charAt(14))) {
			case ' ' :
			case 'A' :
				if(!realizarTransacaoBanco(OpcoesTransacao.EXISTE, fpc, db))
					realizarTransacaoBanco(OpcoesTransacao.INSERIR, fpc, db);
				else
					realizarTransacaoBanco(OpcoesTransacao.ALTERAR, fpc, db);
				break ;

			case 'E' :
				realizarTransacaoBanco(OpcoesTransacao.DELETAR, fpc, db);
				break;

			default :
				if(cargaCompleta) {
					if(realizarTransacaoBanco(OpcoesTransacao.EXISTE, fpc, db))
						realizarTransacaoBanco(OpcoesTransacao.INSERIR, fpc, db);
					else
						realizarTransacaoBanco(OpcoesTransacao.ALTERAR, fpc, db);
				}
				break;
		}

		return false;
	}

	@Override
	public String getTabela() { return TABELA; }

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		getLinhaFormaPgImportar(linha, db, true);

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_tabela_f_p_cliente);
	}

	@Override
	public SQLiteDatabase getDB() {
		return new DBHelper(ctx).getReadableDatabase();
	}
}