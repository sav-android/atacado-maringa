package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.FormaPagamento;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class FormaPagamentoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA="FormaPagamento";
	private static final String[] colunas = new String[] {"IdFormaPagamento", "Descricao", "LimiteMinimo", "AmarraCondicao"} ;
	
	public FormaPagamentoDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	
	public FormaPagamento get (long idFormaPagamento) {
		SQLiteDatabase dbLocal = null ;
		if(db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db ;
		
		Cursor c = null ;
		FormaPagamento fpg = null ;
		try {	
			c = dbLocal.query(TABELA, null, "IdFormaPagamento=?", new String[] {String.valueOf(idFormaPagamento)}, null, null, "Descricao") ;
			if(c.moveToFirst())
				fpg = preencherObjeto(c);
				
		} catch (Exception e) {
			Log.e("FormaPagamentoDAO:get", "get - " + e.getMessage());
		} finally {
			if(c != null)
				c.close();
			//if(db == null)
				//dbLocal.close(); singletone();
		}
		return fpg ;
	}
	
	public List<FormaPagamento> getAll() {
		new FormaPagamentoDAO(ctx, db);
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else 
			dbLocal = db ;
		
		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();
		
		Cursor c = null ;
		FormaPagamento fPgto = null ;
		
		try {
			c = dbLocal.query(TABELA, colunas, null, null, null, null, "Descricao");

			while(c.moveToNext()) {
				fPgto = preencherObjeto(c);
				lista.add(fPgto);
			}				
			
		} catch (Exception e) {
			Log.e("FormaPagamentoDAO:getAll.", e.getMessage());
		} finally {
			c.close();
			//if(db == null)
				//dbLocal.close(); singletone();
		}
		
		return lista ;
	}

	public List<FormaPagamento> getFormaPagamentoPorCondicaoSelecionada(int formaAmarracao) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		List<FormaPagamento> lista = new ArrayList<FormaPagamento>();

		Cursor c = null ;
		FormaPagamento fPgto = null ;

		try {
			c = db.rawQuery("SELECT * FROM FormaPagamento WHERE AmarraCondicao NOT IN(?) Order by Descricao", new String[] {String.valueOf(formaAmarracao)});
			while(c.moveToNext()) {
				fPgto = preencherObjeto(c);
				lista.add(fPgto);
			}

		} catch (Exception e) {
			Log.e("FormaPagamentoDAO:getAll.", e.getMessage());
		} finally {
			c.close();
			//if(db == null)
			//dbLocal.close(); singletone();
		}

		return lista ;
	}
	
	private FormaPagamento preencherObjeto(Cursor c) {
		FormaPagamento fpg = new FormaPagamento();
		fpg.setIdFormaPagamento(c.getLong(c.getColumnIndex(colunas[0])));
		fpg.setDescricao(c.getString(c.getColumnIndex(colunas[1])));
		fpg.setLimiteMinimo(c.getDouble(c.getColumnIndex(colunas[2])));
		fpg.setAmarraCondicao(c.getLong(c.getColumnIndex(colunas[3])));

		return fpg ;
	}
	
	private ContentValues preencherContentValues(FormaPagamento fpg) {
		ContentValues ctv = new ContentValues();
		ctv.put(colunas[0], fpg.getIdFormaPagamento());
		ctv.put(colunas[1], fpg.getDescricao());
		ctv.put(colunas[2], fpg.getLimiteMinimo());
		ctv.put(colunas[3], fpg.getAmarraCondicao());

		return ctv ;		
	}
	
	public void importar(List<String> linhasTexto) {
		try{
			deleteAll();
			getImportacaoFormaPG(linhasTexto);
		} catch (Exception e) {
			Log.d("FormaPagamentoDAO", e.getMessage());
		}
	}

	private void getImportacaoFormaPG(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaFormaPG(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaFormaPG(String linha) {
		FormaPagamento f = new FormaPagamento() ;

		if(linha.substring(0, 6).trim().matches(MATCHES) && !linha.substring(0, 6).trim().equals(""))
			f.setIdFormaPagamento(Short.parseShort(linha.substring(0,6).trim()));
		else {
			Utils.gravarLog("\nFPG: " +linha);
			return true;
		}

		if(linha.substring(6, 56).trim().equals(""))
			f.setDescricao("");
		else if(!linha.substring(6 , 56).trim().equals(""))
			f.setDescricao(linha.substring(6, 56).trim());
		else  {
			Utils.gravarLog("\nFPG: " +linha);
			return true;
		}

		if (linha.length() > 56) {
			if(linha.substring(56, 68).trim().equals(""))
				f.setLimiteMinimo(0);
			else if(!linha.substring(56 , 68).trim().equals(""))
				f.setLimiteMinimo(Double.parseDouble(linha.substring(56, 68).trim()));
			else  {
				Utils.gravarLog("\nFPG: " +linha);
				return true;
			}
		} else {
			f.setLimiteMinimo(0);
		}

		if (linha.length() > 68) {
			if(linha.substring(68, 69).trim().equals(""))
				f.setAmarraCondicao(0);
			else if(!linha.substring(68 , 69).trim().equals(""))
				f.setAmarraCondicao(Long.valueOf(linha.substring(68, 69).trim()));
			else  {
				Utils.gravarLog("\nFPG: " +linha);
				return true;
			}
		} else {
			f.setAmarraCondicao(0);
		}

		//Se não for gerado no arquivo texto ou se vier 0, vamos incluir a regra para o Atacado Maringá (ticket 27983)
		if (f.getDescricao().toUpperCase().contains("BOLETO") && f.getLimiteMinimo() == 0) {
			f.setLimiteMinimo(150);
		}

		if (f.getDescricao().toUpperCase().contains("BOLETO") && f.getAmarraCondicao() == 0) {
			f.setAmarraCondicao(2);
		}else if (f.getDescricao().toUpperCase().contains("DINHEIRO") && f.getAmarraCondicao() == 0) {
			f.setAmarraCondicao(1);
		}

		if(!exists(f))
			insert(f);
		else
			update(f);

		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaFormaPG(linha)) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_forma_pg);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
