package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Fornecedor;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class FornecedorDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Fornecedor" ;

	public FornecedorDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public List<String> getListaNomes() {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		Cursor c = db.query(TABELA, new String[] {"Descricao"}, null, null, null, null, "Descricao");
		List<String> retorno = new ArrayList<String>(c.getCount() + 1);
		int aux = 0;
		retorno.add(aux++, "TODOS");
				
		try {
			while (c.moveToNext()) 
				retorno.add(aux++, c.getString(0));
		} catch (Exception e) {
			Log.e("FornecedorDAO.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}				
		
		return retorno;
	}
	
	public long getIdByName(String nomeProcurado) {
		long retorno = 0;
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		Cursor c = db.query(TABELA, new String[] {"IdFornecedor"}, "Descricao=?", new String[] {nomeProcurado}, null, null, null, "1");
		
		try {
			if (c.moveToFirst())
				retorno = c.getLong(0);
		} catch (Exception e) {
			Log.e("FornecedorDAO:", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}
					
		return retorno;
	}
	public Fornecedor getById(long IdFornecedor) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

		String[] param = new String[]{String.valueOf(IdFornecedor)};

		Fornecedor fornecedor = new Fornecedor();

		Cursor c = db.query(TABELA, colunas, "IdFornecedor=?", param, null, null, null);

		try {
			if (c.moveToFirst())
				fornecedor = (Fornecedor) preencherObjeto(c, fornecedor);
		} catch (Exception e) {
			Log.e("ProdutoDAO:get.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}

		return fornecedor;
	}

	public void importar (List<String> linhasTexto, Boolean cargaCompleta) {
		try{
			if(cargaCompleta)
				deleteAll();
			getImportacaoFornecedor(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("FornecedorDAO", e.getMessage()) ;
		}
	}

	private void getImportacaoFornecedor(List<String> linhasTexto, Boolean cargaCompleta) {
		try{
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaFornecedorImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaFornecedorImportar(Boolean cargaCompleta, String linha) {
		Fornecedor f = new Fornecedor();

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
			f.setIdFornecedor(Long.parseLong(linha.substring(0, 6).trim()));
		else {
			Utils.gravarLog("\nFOR: " +linha);
			return true;
		}
		f.setDescricao(linha.substring(8, 33).trim());

		if(!cargaCompleta) {
			switch(Character.toUpperCase(linha.charAt(33))) {
			case ' ' :
			case 'A' :
				if(exists(f))
					update(f);
				else
					insert(f);
				break ;
			case 'E' :
				delete(f);
			default:
				Utils.gravarLog("\nFOR(Flag Inválida): "+linha);
				break;
			}
		} else {
			if(!exists(f))
				insert(f);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaFornecedorImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_fornecedor);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
