package br.net.sav.dao;

import java.io.FileNotFoundException;
import java.util.Date;

import android.content.Context;
import android.util.Log;

import br.net.sav.modelo.Parametro;
import br.net.sav.util.dao.MapeamentoDAO;

public class GeracaoTextoDAO {
    private Context ctx;
    private Parametro mParametro;
    private int quantidadePedidosEnviados;

    public GeracaoTextoDAO(Context ctx) {
        this.ctx = ctx;
        this.mParametro = new ParametroDAO(ctx, null).get();
    }

    public void gerarTextos() {
        if (mParametro != null) {
             new ClienteDAO(ctx, null).gerarTexto(mParametro.getIdVendedor());
           // new ContatoDAO(ctx, null).gerarTexto(mParametro.getIdVendedor());
            quantidadePedidosEnviados =new PedidoDAO(ctx, null).gerarTexto(mParametro.getIdVendedor());
            new TrocaDAO(ctx).gerarTexto(mParametro.getIdVendedor());
            new NaoVendaMovimentoDAO(ctx, null).gerarTextos(mParametro.getIdVendedor());
            try {
                new MapeamentoDAO(ctx).gerarTextos((short) mParametro.getIdVendedor());
            } catch (FileNotFoundException e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
            }
        }
    }

    public void marcarEnviados(Date dataEnvio) {
        new ClienteDAO(ctx, null).marcarEnviado(dataEnvio);
        new PedidoDAO(ctx, null).marcarEnviado(dataEnvio);
        new MapeamentoDAO(ctx).marcarEnviado(dataEnvio);
        new NaoVendaMovimentoDAO(ctx, null).marcarEnviado(dataEnvio);
        new ContatoDAO(ctx, null).marcarEnviado(dataEnvio);

    }

    public void apagarAposPrazo() {
        if (mParametro != null) {
            new PedidoDAO(ctx, null).verificaDataAlterada();
            new ClienteDAO(ctx, null).apagarAposPrazo(mParametro.getQtdeDiasGuardarPedido());
            new PedidoDAO(ctx, null).apagarAposPrazo(mParametro.getQtdeDiasGuardarPedido());
            new MapeamentoDAO(ctx).apagarAposPrazo(mParametro.getQtdeDiasGuardarPedido());
            new NaoVendaMovimentoDAO(ctx, null).apagarAposPrazo(mParametro.getQtdeDiasGuardarPedido());
        }
    }

    public int getQuantidadePedidosEnviados() {
        return this.quantidadePedidosEnviados;
    }
}
