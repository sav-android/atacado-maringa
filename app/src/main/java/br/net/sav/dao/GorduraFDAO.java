package br.net.sav.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.GorduraF;

public class GorduraFDAO extends PadraoDAO {
	private static final String TABELA="GorduraF";
	
	public GorduraFDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}

	public void importar(List<String> linhasTexto) {
		try {
			deleteAll();
			try {
				db.beginTransaction();
				
				for(String linha: linhasTexto) {
					GorduraF g = new GorduraF() ;
					
					if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
						g.setIdFornecedor(Short.parseShort(linha.substring(0, 8).trim()));
					else {
						Utils.gravarLog("\nGOF: " +linha);
						continue;
					}
					if(linha.substring(8, 14).trim().equals(""))
						g.setDescontoGordura(0);
					else if(linha.substring(8, 14).trim().matches(MATCHES))
						g.setDescontoGordura(Double.parseDouble(linha.substring(8, 14).trim()) / 10000);
					else {
						Utils.gravarLog("\nGOF: " +linha);
						continue;
					}
					if(linha.substring(14, 18).trim().equals(""))
						g.setMaximoDesconto(0);
					else if(linha.substring(14, 18).trim().matches(MATCHES))
						g.setMaximoDesconto(Double.parseDouble(linha.substring(14, 18).trim()) / 100);
					else {
						Utils.gravarLog("\nGOF: " +linha);
						continue;
					}
					
					insert(g);
				}
				
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
			
		} catch (Exception e) {
			Log.d("GorduraFDAO", e.getMessage());
		}
	}
}
