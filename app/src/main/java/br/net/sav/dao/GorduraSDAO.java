package br.net.sav.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.GorduraS;

public class GorduraSDAO extends PadraoDAO {
	private static final String TABELA = "GorduraS" ;
	
	public GorduraSDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public void importar (List<String> linhasTexto) {
		try {
			deleteAll();
			try{
				db.beginTransaction();
				
				for(String linha: linhasTexto) {
					GorduraS g = new GorduraS() ;
					
					if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
						g.setIdFornecedor(Short.parseShort(linha.substring(0, 8).trim()));
					else {
						Utils.gravarLog("\nGOS: " + linha);
						continue;
					}
					if(linha.substring(8, 12).trim().equals(""))
							g.setSecao((short) 0);
					else if(linha.substring(8, 12).trim().matches(MATCHES))
						g.setSecao(Short.parseShort(linha.substring(8, 12).trim()));
					else {
						Utils.gravarLog("\nGOS: " +linha);
						continue;
					}
					if(linha.substring(12, 16).trim().equals(""))
						g.setGrupo((short) 0);
					else if(linha.substring(12, 16).trim().matches(MATCHES))
						g.setGrupo(Short.parseShort(linha.substring(12,16).trim()));
					else {
						Utils.gravarLog("\nGOS: " +linha);
						continue;
					}
					if(linha.substring(16, 20).trim().equals(""))
						g.setSubGrupo((short) 0);
					else if(linha.substring(16, 20).trim().matches(MATCHES))
						g.setSubGrupo(Short.parseShort(linha.substring(16, 20).trim()));
					else {
						Utils.gravarLog("\nGOS: " +linha);
						continue;
					}
					if(linha.substring(20, 26).trim().equals(""))
						g.setDescontoGordura(0);
					else if(linha.substring(20, 26).trim().matches(MATCHES))
						g.setDescontoGordura(Double.parseDouble(linha.substring(20, 26).trim()) / 10000);
					else {
						Utils.gravarLog("\nGOS: " +linha);
						continue;
					}
					if(linha.substring(26, 30).trim().equals(""))
						g.setMaximoDesconto(0);
					else if(linha.substring(26, 30).trim().matches(MATCHES))
						g.setMaximoDesconto(Double.parseDouble(linha.substring(26, 30).trim()) / 100);
					else {
						Utils.gravarLog("\nGOS: " +linha);
						continue;
					}			
					
					if(!exists(g))
						insert(g);
					else
						update(g);
					
				}
				
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		} catch (Exception e) {
			Log.d("GordouraSDAO", e.getMessage());
		}		
	}

}
