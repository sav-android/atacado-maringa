package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.GrupoProduto;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class GrupoProdutoDAO extends br.net.sav.util.dao.PadraoDAO implements AcaoParaImportacaoDados {
    public static final String TABELA = "GrupoProduto";
    private Context ctx;

    public GrupoProdutoDAO(Context ctx) {
        super(ctx, TABELA);
        this.ctx = ctx;
    }

    private ContentValues getContentValues(GrupoProduto g) {
        ContentValues ctv = new ContentValues();

        ctv.put(Colunas.ID_GRUPO, g.getIdGrupo());
        ctv.put(Colunas.DESCRICAO, g.getDescricao());
        ctv.put(Colunas.COR_RGB_HEX, g.getCorRgbHex());

        return ctv;
    }

    public void importar(List<String> linhasTexto) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();


        try {
            super.deleteAll(db);

            getImportacaoGProduto(linhasTexto, db);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        } finally {
            if (db.inTransaction())
                db.endTransaction();
            //db.close(); singleton();
        }
    }

    private void getImportacaoGProduto(List<String> linhasTexto, SQLiteDatabase db) {
        db.beginTransaction();

        for (String linha : linhasTexto) {
            try {
                if (getLinhaGrupoProdutoImportar(db, linha)) continue;

            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
            }
        }

        db.setTransactionSuccessful();
    }

    private boolean getLinhaGrupoProdutoImportar(SQLiteDatabase db, String linha) {
        GrupoProduto g = new GrupoProduto();

        if (!linha.substring(0, 4).trim().equals("") && linha.substring(0, 4).trim().matches(MATCHES))
            g.setIdGrupo(Integer.parseInt(linha.substring(0, 4).trim()));
        else
            return true;

        g.setDescricao(linha.substring(4, 34));

        if (linha.substring(34, 40).trim().length() < 6)
            g.setCorRgbHex(null);
        else
            g.setCorRgbHex(linha.substring(34, 40));

        insert(getContentValues(g), db);
        return false;
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        if (getLinhaGrupoProdutoImportar(db, linha)) return true;

        return false;
    }

    @Override
    public String getMensagemTabela() {
        return mContext.getString(R.string.importacao_grupo_produto);
    }

    @Override
    public SQLiteDatabase getDB() {
         return new DBHelper(ctx).getReadableDatabase();
    }

    private class Colunas {
        private static final String ID_GRUPO = "IdGrupo";
        private static final String DESCRICAO = "Descricao";
        private static final String COR_RGB_HEX = "CorRgbHex";
    }
}
