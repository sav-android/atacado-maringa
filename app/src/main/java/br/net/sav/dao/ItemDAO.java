package br.net.sav.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.Pedido;

public class ItemDAO extends PadraoDAO {
	private static final String TABELA = "Item" ;

	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public ItemDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	
	public boolean insert(Item item, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null ;
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else 
			dbLocal = db ;
		
		ContentValues ctv = new ContentValues();
		ctv.put("IdPedido", item.getIDPedido());
		ctv.put("IdProduto", item.getIDProduto());
		ctv.put("NrItem", item.getNrItem());
		ctv.put("Quantidade", item.getQuantidade());
		ctv.put("Desconto", item.getDesconto());
		ctv.put("ValorDesconto", item.getValorDesconto());
		ctv.put("ValorUtilizouFlex", item.getValorUtilizouFlex());
		ctv.put("ValorGeraraFlex", item.getValorGeraraFlex());
		ctv.put("ValorGerandoFlex", item.getValorGerandoFlex());
		ctv.put("ValorUnitOriginal", item.getValorUnitOriginal());
		ctv.put("ValorUnitPraticado", item.getValorUnitPraticado());
		ctv.put("TotalGeral", item.getTotalGeral());
		ctv.put("TotalLiquido", item.getTotalLiquido());
		ctv.put("DataPedido", sdf1.format(item.getDataPedido()));
		ctv.put("Rentabilidade", item.getRentabilidade());
		ctv.put("CodigoBarra", item.getCodigoBarra());
		ctv.put("Descricao", item.getDescricao());
		ctv.put("CodigoNCM", item.getCodigoNCM());
		ctv.put("PercentualDescontoCombo", item.getPercentualDescontoCombo());
		ctv.put("QuantidadeItemLimiteCombo", item.getQuantidadeItemLimiteCombo());
		ctv.put("ItemComboAtivado", item.getItemComboAtivado());
		
		boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0;
		
		//if(db == null)
			//dbLocal.close(); singletone();
		
		return retorno ;
	}
	
	public Boolean insert(List<Item> itens, SQLiteDatabase db) {
		boolean retorno = true;
		for (Item item : itens) {
			if (!(retorno = insert(item, db)))
				break;
		}
		return retorno;
	}

	public Boolean insertCM(List<Item> itens, Pedido pedido, SQLiteDatabase db) {
		boolean retorno = true;
		for (Item item : itens) {
			item.setIDPedido(pedido.getIDPedido());
			if (!(retorno = insert(item, db)))
				break;
		}
		return retorno;
	}

	public List<Item> listaByPedido(long idPedido) {
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		List<Item> lista = new ArrayList<Item>();
		
		Cursor c = null ;
		
		Item i = null ;				
		
		StringBuilder sql = new StringBuilder();
		sql.append("select i.*, pro.Descricao as DescricaoProduto, pro.DescricaoEmbalagem from Item i ");
		sql.append("left join Produto pro on (i.IdProduto = pro.IdProduto) ");
		sql.append(String.format("where i.IdPedido = %d order by DescricaoProduto", idPedido));
		
		try {
			
			c = dbLocal.rawQuery(sql.toString(), null);
			
			while(c.moveToNext()) {
				i = new Item();
				i.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
				i.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
				i.setNrItem(c.getShort(c.getColumnIndex("NrItem")));
				i.setQuantidade(c.getDouble(c.getColumnIndex("Quantidade")));
				i.setDesconto(c.getDouble(c.getColumnIndex("Desconto")));
				i.setValorDesconto(c.getDouble(c.getColumnIndex("ValorDesconto")));
				i.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
				i.setValorGeraraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
				i.setValorGerandoFlex(c.getDouble(c.getColumnIndex("ValorGerandoFlex")));
				i.setValorUnitOriginal(c.getDouble(c.getColumnIndex("ValorUnitOriginal")));
				i.setValorUnitPraticado(c.getDouble(c.getColumnIndex("ValorUnitPraticado")));
				i.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
				i.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
				i.setDataPedido(sdf1.parse(c.getString(c.getColumnIndex("DataPedido"))));
				i.setDescricao(c.getString(c.getColumnIndex("DescricaoProduto")));
				i.setDescricaoEmbalagem(c.getString(c.getColumnIndex("DescricaoEmbalagem")));
				i.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
				i.setPercentualDescontoCombo(c.getDouble(c.getColumnIndex("PercentualDescontoCombo")));
				i.setQuantidadeItemLimiteCombo(c.getDouble(c.getColumnIndex("QuantidadeItemLimiteCombo")));
				i.setItemComboAtivado(c.getInt(c.getColumnIndex("ItemComboAtivado")));
				i.setQntdCortada(c.getInt(c.getColumnIndex("qntdCortada")));
				i.setDescontoFaturado(c.getDouble(c.getColumnIndex("descontoFaturado")));
				i.setTotalFaturadoItem(c.getDouble(c.getColumnIndex("totalFaturadoItem")));
				i.setStatus(c.getString(c.getColumnIndex("status")));
				lista.add(i);
			}
			
		} catch (Exception e) {
			Log.e("ItemDAO:listaByPedido", e.getMessage());
		} finally {
			c.close();
		//	if(db == null)
				//db.close(); singletone();
		}
		
		return lista ;
	}
	
	public Boolean delete(long idPedido, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null ;
		if(db == null)
			dbLocal  = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		boolean retorno = dbLocal.delete(TABELA, "IdPedido=?", new String[] {String.valueOf(idPedido)}) > 0; 
		
		//if(db == null)
			//dbLocal.close(); singletone();
		
		return retorno ;
	}

	public List<Item> listaPedidoVendaProduto() {
		SQLiteDatabase dbLocal;
		if(db == null)
			dbLocal= new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal=db;
		
		List<Item> lista = new ArrayList<Item>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select ite.idproduto, prod.Descricao, sum(TotalGeral) as ValorBruto, sum(TotalLiquido) as ValorLiquido,");
		sql.append(" sum(Quantidade) as QtdeVendida,  sum(ite.quantidade * prod.peso) as Peso,");		
		sql.append("(select distinct count(ped1.idpedido) from pedido ped1 inner join item ite1 on (ite1.idpedido = ped1.idpedido)");
		sql.append(" where ite1.idproduto = ite.idproduto and ped1.enviado = 0) as QtdePedidos");
		sql.append(" from item ite inner join produto prod on (prod.idproduto = ite.idproduto) where QtdePedidos > 0 group by ite.idproduto");
		
		Cursor c  = dbLocal.rawQuery(sql.toString(), null);		
		
		try {
			while(c.moveToNext()) {
				Item i = new Item();
				
				i.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
				i.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				i.setTotalGeral(c.getDouble(c.getColumnIndex("ValorBruto")));
				i.setTotalLiquido(c.getDouble(c.getColumnIndex("ValorLiquido")));
				i.setQuantidade(c.getDouble(c.getColumnIndex("QtdeVendida")));	
				i.setPeso(c.getDouble(c.getColumnIndex("Peso")));				
				
				lista.add(i);
			}
		} catch (Exception e) {	
			Log.e("ItemDAO:listaPedidoVendaProduto.", e.getMessage());
		} finally {
			c.close();
	//		if(db==null)
				//dbLocal.close(); singletone();
		}				
		
		return lista ;		
	}
	
	public ArrayList<Item> getByPedido(long IdPedido) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select i.*, pro.Descricao as DescricaoProduto, pro.DescricaoEmbalagem from Item i ");
		sql.append("left join Produto pro on (i.IdProduto = pro.IdProduto) ");
		sql.append(String.format("where i.IdPedido = %d order by i.NrItem", IdPedido));
		
		Cursor c = db.rawQuery(sql.toString(), null);
		
		ArrayList<Item> lista = new ArrayList<Item>(c.getCount());
		
		try {
			while(c.moveToNext()) {
				Item item = new Item() ;
				item.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
				item.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
				item.setNrItem(c.getShort(c.getColumnIndex("NrItem")));
				item.setQuantidade(c.getDouble(c.getColumnIndex("Quantidade")));
				item.setDesconto(c.getDouble(c.getColumnIndex("Desconto")));
				item.setValorDesconto(c.getDouble(c.getColumnIndex("ValorDesconto")));
				item.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
				item.setValorGeraraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
				item.setValorGerandoFlex(c.getDouble(c.getColumnIndex("ValorGerandoFlex")));
				item.setValorUnitOriginal(c.getDouble(c.getColumnIndex("ValorUnitOriginal")));
				item.setValorUnitPraticado(c.getDouble(c.getColumnIndex("ValorUnitPraticado")));
				item.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
				item.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
				item.setDataPedido(sdf1.parse(c.getString(c.getColumnIndex("DataPedido"))));
				item.setDescricao(c.getString(c.getColumnIndex("DescricaoProduto")));
				item.setDescricaoEmbalagem(c.getString(c.getColumnIndex("DescricaoEmbalagem")));
				item.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
				item.setCodigoNCM(c.getString(c.getColumnIndex("CodigoNCM")));
				item.setCodigoBarra(c.getString(c.getColumnIndex("CodigoBarra")));
				item.setPercentualDescontoCombo(c.getDouble(c.getColumnIndex("PercentualDescontoCombo")));
				item.setQuantidadeItemLimiteCombo(c.getDouble(c.getColumnIndex("QuantidadeItemLimiteCombo")));
				item.setItemComboAtivado(c.getInt(c.getColumnIndex("ItemComboAtivado")));
				item.setDescricao(item.getDescricao() != null? item.getDescricao(): c.getString(c.getColumnIndex("Descricao")) );
				item.setQntdCortada(c.getInt(c.getColumnIndex("qntdCortada")));
				item.setDescontoFaturado(c.getDouble(c.getColumnIndex("descontoFaturado")));
				item.setTotalFaturadoItem(c.getDouble(c.getColumnIndex("totalFaturadoItem")));
				item.setStatus(c.getString(c.getColumnIndex("status")));
				lista.add(item);
			}
		} catch (Exception e) {	
			Log.e("ItemDAO:getByPedido.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();	
		}		
		
		return lista ;		
	}

}
