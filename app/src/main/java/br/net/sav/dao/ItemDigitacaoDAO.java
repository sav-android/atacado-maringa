package br.net.sav.dao;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Produto;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TipoPedido;
import br.net.sav.utils.MathUtils;

public class ItemDigitacaoDAO extends PadraoDAO {
	public ItemDigitacaoDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, null, db);
	}

	@SuppressLint("LongLogTag")
	public List<ItemDigitacao> getEstruturaDigitacaoPedido(TipoPedido tipoPedido, Tabela tabela, Condicao condicao) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<ItemDigitacao> itensDigitacao = new ArrayList< ItemDigitacao>();

		Parametro parametro = new ParametroDAO(ctx, db).get();

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pro.*, pre.PrecoNormal as PrecoProduto, m.Descricao as DescricaoMarca, g.CorRgbHex as CorProduto");
		sql.append(" FROM Produto pro JOIN Preco pre ON ");
		sql.append(String.format(" (pro.Idproduto = pre.idProduto AND pre.IdTabela = %d AND pre.IdEmpresa = %d AND pre.IdFilial = %d)", tabela.getIdTabela(), tabela.getIdEmpresa(), tabela.getIdFilial()));
		sql.append("JOIN Marca m ON (m.IdMarca = pro.IdMarca) ");
		sql.append("LEFT JOIN GrupoProduto g ON (pro.IdGrupo = g.IdGrupo) ");
		sql.append("WHERE pre.PrecoNormal <> 0 AND pro.SaldoEstoque <> '0' ORDER BY Descricao");

		Cursor c = null;


		try {
			try {
				c = dbLocal.rawQuery(sql.toString(), null);

				double precoProduto;

				while (c.moveToNext()) {
					ItemDigitacao i = new ItemDigitacao();

					i.setProduto(new Produto());
					i.getProduto().setIdProduto(c.getLong(c.getColumnIndex("IdProduto")));
					i.getProduto().setIdFornecedor(c.getShort(c.getColumnIndex("IdFornecedor")));
					i.getProduto().setDescricao(c.getString(c.getColumnIndex("Descricao")));
					i.getProduto().setCodigoEmbalagem(c.getShort(c.getColumnIndex("CodigoEmbalagem")));
					i.getProduto().setDescricaoEmbalagem(c.getString(c.getColumnIndex("DescricaoEmbalagem")));
					i.getProduto().setQuantidadeEmbalagem(c.getLong(c.getColumnIndex("QuantidadeEmbalagem")));
					i.getProduto().setIdSecao(c.getShort(c.getColumnIndex("IdSecao")));
					i.getProduto().setIdICMS(c.getShort(c.getColumnIndex("IdICMS")));
					i.getProduto().setCodigoBarra(c.getString(c.getColumnIndex("CodigoBarra")));
					i.getProduto().setIdSubGrupo(c.getShort(c.getColumnIndex("IdSubgrupo")));
					i.getProduto().setControleGordura(c.getString(c.getColumnIndex("ControleGordura"))); // UNNUSED
					i.getProduto().setDescontoGordura(c.getDouble(c.getColumnIndex("DescontoGordura"))); // UNNUSED
					i.getProduto().setMultiplo(c.getShort(c.getColumnIndex("Multiplo")));
					i.getProduto().setIdGrupo(c.getShort(c.getColumnIndex("IdGrupo")));
					i.getProduto().setIdLinha(c.getShort(c.getColumnIndex("IdLinha")));
					i.getProduto().setIdMarca(c.getLong(c.getColumnIndex("IdMarca")));
					i.getProduto().setEstoqueBaixo(c.getShort(c.getColumnIndex("EstoqueBaixo")));
					i.getProduto().setQuantidadeCaixa(c.getShort(c.getColumnIndex("QuantidadeCaixa")));
					i.getProduto().setSaldoEstoque(c.getString(c.getColumnIndex("SaldoEstoque")));
					i.getProduto().setPermiteVender(c.getInt(c.getColumnIndex("PermiteVender")) > 0);
					i.getProduto().setPeso(c.getDouble(c.getColumnIndex("Peso")));
					i.getProduto().setStatus(c.getString(c.getColumnIndex("Status")));
					i.getProduto().setPermiteMeiaCaixa(c.getInt(c.getColumnIndex("PermiteMeiaCaixa")) > 0);
					i.getProduto().setCodigoReferencia(c.getString(c.getColumnIndex("CodigoReferencia")));
					i.getProduto().setPermiteBonificacao(c.getInt(c.getColumnIndex("PermiteBonificacao")) > 0);
					i.getProduto().setMaximoDesconto(c.getDouble(c.getColumnIndex("MaximoDesconto")));
					i.getProduto().setUnitario(c.getShort(c.getColumnIndex("Unitario")));
					i.getProduto().setDivisaoPreco(c.getLong(c.getColumnIndex("DivisaoPreco")));
					i.getProduto().setDescricaoMarca(c.getString(c.getColumnIndex("DescricaoMarca")));
					i.setDescricaoMarca(c.getString(c.getColumnIndex("DescricaoMarca")));
					i.setCorProduto(c.getString(c.getColumnIndex("CorProduto")));
					i.setPossuiFoto(c.getInt(c.getColumnIndex("PossuiFoto")) > 0);
					i.setPercentualRentabilidadeMedia(c.getDouble(c.getColumnIndex("PercentualRentabilidadeMedia")));
					i.setPercentualRentabilidadeMinima(c.getDouble(c.getColumnIndex("PercentualRentabilidadeMinima")));
					i.setPrecoCustoProduto(c.getDouble(c.getColumnIndex("PrecoCustoProduto")));
					i.getProduto().setCodigoNCM(c.getString(c.getColumnIndex("CodigoNCM")));

					//Aplicar o desconto do vendedor como desconto do produto
					if(!tabela.isUltilizaRentabilidade()) {
						if (tabela.isPermiteDescontoPorTabela())
							i.getProduto().setMaximoDesconto(parametro.getDescontoProdutoVendedor());
						else
							i.getProduto().setMaximoDesconto(0);
					}


					precoProduto = c.getDouble(c.getColumnIndex("PrecoProduto"));
					double precoCustoProduto = i.getPrecoCustoProduto();
                    double resultado = precoCustoProduto * i.getProduto().getQuantidadeEmbalagem();

                    i.setValorUnitOriginal(Utils.arredondar(precoProduto / i.getProduto().getDivisaoPreco(), 2));

					i.setPrecoCustoProduto(Utils.arredondar(resultado,2));

					if (condicao.getTaxaFinanceira() != 0)
						i.setValorUnitOriginal(i.getValorUnitOriginal() * condicao.getTaxaFinanceira());

					i.setValorUnit(i.getValorUnitOriginal());

					if (i.getPrecoCustoProduto() > 0) {
						i.setRentabilidade(MathUtils.arredondar((i.getValorUnitOriginal() - i.getPrecoCustoProduto()) / i.getPrecoCustoProduto() * 100, 2));
					}

					itensDigitacao.add(i);


				}
				//setQuantidadeFotosNaPasta(ctx,quantidadeFotos());

			} catch (Exception e) {
				Log.e("ItemDigitacaoDAO:getEstruturaDigitacao", e.getMessage());
			}
		} catch (Exception e) {
			Log.e("ItemDigitacaoDAO:getEstruturaDigitacao", e.getMessage());
		} finally {
			c.close();
			//if (db == null)
				//dbLocal.close(); singletone();
		}

		return itensDigitacao;
	}


}