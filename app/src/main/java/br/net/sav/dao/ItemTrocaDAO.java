package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.modelo.ItemTroca;

public class ItemTrocaDAO {
	
	private Context ctx ;
	
	private static String TABELA = "ItemTroca";
	
	private static String[] colunas = new String[] {"IdTroca", "IdProduto", "NumeroItem", "Unitario", "Quantidade", "ValorUnitario",
		"ValorMercadoriaBruto", "Desconto", "ValorDesconto","ValorMercadoriaLiquido"} ;
	
	public ItemTrocaDAO(Context ctx) {
		this.ctx = ctx ;
	}
	
	public Boolean insert ( ItemTroca itemTroca, SQLiteDatabase db ) {
		SQLiteDatabase dbLocal = null ;
		
		if ( db == null )
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else 
			dbLocal = db ;
		
		ContentValues ctv = new ContentValues();		
		
		ctv.put("IdTroca", itemTroca.getIdTroca());
		ctv.put("IdProduto", itemTroca.getIdProduto());
		ctv.put("NumeroItem", itemTroca.getNumeroItem());
		ctv.put("Unitario", itemTroca.isUnitario());
		ctv.put("Quantidade", itemTroca.getQuantidade());
		ctv.put("ValorUnitario", itemTroca.getValorUnitario());
		ctv.put("ValorMercadoriaBruto", itemTroca.getValorMercadoriaBruto());
		ctv.put("Desconto", itemTroca.getDesconto());
		ctv.put("ValorDesconto", itemTroca.getValorDesconto());
		ctv.put("ValorMercadoriaLiquido", itemTroca.getValorMercadoriaLiquido());
		
		boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0 ;
		
		//if( db == null )
			//dbLocal.close(); singletone();
		
		return retorno ;			
	}
	
	public Boolean insert(List<ItemTroca> itens, SQLiteDatabase db) {
		boolean retorno = true;
		for (ItemTroca item : itens) {
			if (!(retorno = insert(item, db)))
				break;
		}
		return retorno;
	}
	
	public Boolean delete(long idTroca, SQLiteDatabase db) {
		return db.delete(TABELA, "IdTroca=?", new String[] {String.valueOf(idTroca)}) > 0;
	}
	
	public Boolean deleteAll() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		boolean retorno = db.delete(TABELA, null, null) > 0 ;
		
		//db.close(); singleton();
		
		return retorno ;
	}
	
	public ItemTroca get (long IdTroca, long IdProduto) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		ItemTroca itemTroca = new ItemTroca() ;
		
		String[] param = new String[] {String.valueOf(IdTroca), String.valueOf(IdProduto)} ;
		
		Cursor c = db.query(TABELA, colunas, "IdTroca=?, IdProduto=?", param, null, null, null);
		
		try {
			if(c.moveToFirst()) {
				itemTroca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
				itemTroca.setIdProduto(c.getLong(c.getColumnIndex("IdProduto")));
				itemTroca.setNumeroItem(c.getShort(c.getColumnIndex("NumeroItem")));
				itemTroca.setUnitario(c.getInt(c.getColumnIndex("Unitario")) >0);
				itemTroca.setQuantidade(c.getShort(c.getColumnIndex("Quantidade")));
				itemTroca.setValorUnitario(c.getInt(c.getColumnIndex("ValorUnitario")));
				itemTroca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
				itemTroca.setDesconto(c.getDouble(c.getColumnIndex("Desconto")));
				itemTroca.setValorDesconto(c.getDouble(c.getColumnIndex("ValorDesconto")));
				itemTroca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
			}
		} catch (Exception e) {
			Log.e("Get(idTroca,idProduto).", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();	
		}
				
		return itemTroca ;
	}
	
	public List<ItemTroca> getAll() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		List<ItemTroca> lista = new ArrayList<ItemTroca>();
		
		Cursor c = db.query(TABELA, colunas, null, null, null, null, null);
		
		try {
			while(c.moveToNext()) {
				ItemTroca itemTroca = new ItemTroca() ;
				
				itemTroca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
				itemTroca.setIdProduto(c.getLong(c.getColumnIndex("IdProduto")));
				itemTroca.setNumeroItem(c.getShort(c.getColumnIndex("NumeroItem")));
				itemTroca.setUnitario(c.getInt(c.getColumnIndex("Unitario")) >0);
				itemTroca.setQuantidade(c.getShort(c.getColumnIndex("Quantidade")));
				itemTroca.setValorUnitario(c.getInt(c.getColumnIndex("ValorUnitario")));
				itemTroca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
				itemTroca.setDesconto(c.getDouble(c.getColumnIndex("Desconto")));
				itemTroca.setValorDesconto(c.getDouble(c.getColumnIndex("ValorDesconto")));
				itemTroca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
				lista.add(itemTroca);
			}
		} catch (Exception e) {
			Log.e("ItemTrocaDAO:getAll.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}
		
		
		return lista ;		
	}
	
	public ArrayList<ItemTroca> getByTroca(long idTroca) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select i.*, pro.Descricao as DescricaoProduto, pro.DescricaoEmbalagem from ItemTroca i ");
		sql.append("left join Produto pro on (i.IdProduto = pro.IdProduto) ");
		sql.append(String.format("where i.IdTroca = %d order by i.NumeroItem", idTroca));
		
		Cursor c = db.rawQuery(sql.toString(), null);
		
		ArrayList<ItemTroca> lista = new ArrayList<ItemTroca>(c.getCount());
		
		try {
			while (c.moveToNext()) {
				ItemTroca item = preencherObjeto(c);
				item.setDescricaoProduto(c.getString(c.getColumnIndex("DescricaoProduto")));
				item.setDescricaoEmbalagem(c.getString(c.getColumnIndex("DescricaoEmbalagem")));
				
				lista.add(item);
			}
		} catch (Exception e) {	
			Log.e("ItemTrocaDAO:getByTroca.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}		
		
		return lista;
	}
	
	private ItemTroca preencherObjeto(Cursor c) {
		ItemTroca itemTroca = new ItemTroca();
		
		itemTroca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
		itemTroca.setIdProduto(c.getLong(c.getColumnIndex("IdProduto")));
		itemTroca.setNumeroItem(c.getShort(c.getColumnIndex("NumeroItem")));
		itemTroca.setUnitario(c.getInt(c.getColumnIndex("Unitario")) >0);
		itemTroca.setQuantidade(c.getShort(c.getColumnIndex("Quantidade")));
		itemTroca.setValorUnitario(c.getDouble(c.getColumnIndex("ValorUnitario")));
		itemTroca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
		itemTroca.setDesconto(c.getDouble(c.getColumnIndex("Desconto")));
		itemTroca.setValorDesconto(c.getDouble(c.getColumnIndex("ValorDesconto")));
		itemTroca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
		
		return itemTroca ;
	}
}
