package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Limite;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class LimiteDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Limite" ;
	
	public LimiteDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	@Override
	public Boolean insert(Object objeto) {
		return super.insert(objeto);
	}

	@Override
	public Boolean delete(Object objeto) {
		return super.delete(objeto);
	}

	@Override
	public Boolean update(Object objeto) {
		return super.update(objeto);
	}

	public Limite get(long idCliente) {
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		Cursor c = dbLocal.query(TABELA, colunas, "IdCliente=?", new String[] {String.valueOf(idCliente)}, null, null, null) ;
		
		Limite limite = null ;
		
		try {
		
			if(c.moveToFirst()) {
				limite = new Limite();
				limite = (Limite) preencherObjeto(c, limite);
			}
		} finally {	
			c.close();			
			//if(db == null)
				//dbLocal.close(); singletone();
	
		}
						
		return limite ;			
	}
	
	public void importar(List<String> linhasTexto, Boolean cargaCompleta){
		try{
			if(cargaCompleta)
				deleteAll();
			getImportacaoLimite(linhasTexto, cargaCompleta);

		} catch (Exception e ) {
			Log.d("LimiteDAO", e.getMessage());
		}
	}

	private void getImportacaoLimite(List<String> linhasTexto, Boolean cargaCompleta){
		try{
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaLimiteImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaLimiteImportar(Boolean cargaCompleta, String linha) {
		Limite l = new Limite() ;

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
			l.setIdCliente(Long.parseLong(linha.substring(0,8).trim()));
		else {
			Utils.gravarLog("\nLIM: "+linha);
			return true;
		}
		if(linha.substring(8, 18).trim().equals(""))
			l.setLimiteTotal(0);
		else if(linha.substring(8, 18).trim().matches(MATCHES))
			l.setLimiteTotal(Double.parseDouble(linha.substring( 8, 18).trim()) / 100);
		else {
			Utils.gravarLog("\nLIM: " +linha);
			return true;
		}
		if(linha.substring(18, 28).trim().equals(""))
			l.setLimiteDisponivel(0);
		else if(linha.substring(18, 28).trim().matches(MATCHES) || linha.substring(18, 28).trim().toString().contains("-"))
			l.setLimiteDisponivel(Double.parseDouble(Utils.tirarZerosEsquerda(linha.substring(18, 28).trim())) / 100);
		else {
			Utils.gravarLog("\nLIM: " +linha);
			return true;
		}

		if(!cargaCompleta) {
			switch(Character.toUpperCase(linha.charAt(28))) {
			case ' ':
			case 'A':
				if(exists(l))
					update(l);
				else
					insert(l);
				break;
			case 'E':
				delete(l);
			default :
				Utils.gravarLog("\nLIM(Flag Inv�lida): " + linha);
				break;
			}
		} else {
			if(!exists(l))
				insert(l);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaLimiteImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_limite_credito);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
