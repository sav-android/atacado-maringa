package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Marca;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class MarcaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA="Marca";
	
	public MarcaDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public long getIdByName(String nomePesquisa) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		Cursor c = null ;
		long id = 0 ;
		try {
			c = db.query(TABELA, new String[] {"IdMarca"}, "Descricao=?", new String[] {nomePesquisa}, null, null, null, "1");
			
			if(c.moveToFirst())
				id = c.getLong(0) ;
			
		} catch (Exception e) {
			Log.e("MarcaDAO:getIdByName", "getIdByName. " + e.getMessage());
		} finally {
			if(c != null)
				c.close();
			//db.close(); singleton();
		}
		
		return id ;
	}
	
	public List<String> listaNomes() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		Cursor c = null ;
		List<String> lista = null ;
		
		try {
			c = db.rawQuery("SELECT DISTINCT m.Descricao FROM Marca m INNER JOIN Produto p ON (p.IdMarca = m.IdMarca) ORDER BY m.DESCRICAO", null);
						
			lista = new ArrayList<String>(c.getCount() + 1); 
			lista.add("TODOS");
			
			while(c.moveToNext())
				lista.add(c.getString(0));
			
		} catch (Exception e) {
			Log.e("MarcaDAO:listaNomes()", "listaNomes. " + e.getMessage()) ;
		} finally {
			if(c != null)
				c.close();
			//db.close(); singleton();
		}
		
		return lista ;
	}

	public String getDescricaoMarcaByIdProduto(long idProduto){
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		String result = null;

		String sql = "SELECT mar.Descricao FROM Produto pro INNER JOIN Marca mar on mar.IdMarca = pro.IdMarca WHERE pro.IdProduto = "+idProduto;
		Cursor cursor = db.rawQuery(sql, null);
		try{
			if(cursor.moveToFirst())
				result = cursor.getString(0) ;

		} catch (Exception e) {
			Log.e("MarcaDAO:", "getDescricaoMarcaByIdProduto. " + e.getMessage());
		} finally {
			if(cursor != null)
				cursor.close();
			//db.close(); singleton();
		}

		return result ;
	}
	
	private Marca preencherObjeto(Cursor c) {
		Marca m = new Marca();
		m.setIdMarca(c.getLong(c.getColumnIndex("IdMarca")));
		m.setDescricao(c.getString(c.getColumnIndex("Descricao")));
		
		return m ;
	}

	public void importar(List<String> linhasTexto, boolean cargaCompleta) {
		try{
			if(cargaCompleta)
				deleteAll();
			getImportacaoMarca(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("MarcaDAO", "Importar: " + e.getMessage());
		}
	}

	private void getImportacaoMarca(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaMarcaImportar(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaMarcaImportar(boolean cargaCompleta, String linha) {
		Marca m = new Marca() ;

		if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
			m.setIdMarca(Short.parseShort(linha.substring(0,4).trim()));
		else {
			Utils.gravarLog("\nMAR: " +linha);
			return true;
		}
		m.setDescricao(linha.substring(4, 29).trim());

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(29))) {
				case ' ':
				case 'A':
					if (exists(m))
						update(m);
					else
						insert(m);
					break;

				case 'E':
					delete(m);
					break;

				default:
					Utils.gravarLog("\nMAR(Flag Inv�lida): " + linha);
					break;
			}
		} else {
			insert(m);
		}

		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaMarcaImportar(true, linha)) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_marca);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
