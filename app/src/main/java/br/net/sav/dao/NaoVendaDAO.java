package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.NaoVenda;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class NaoVendaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "NaoVenda";
	
	public NaoVendaDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}

	public void importar(List<String> linhas) {
		try {
			deleteAll();
			getImportacaoNaoVenda(linhas);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
	}

	private void getImportacaoNaoVenda(List<String> linhas)  {

		for (String linha : linhas) {
			try {
				getLinhaNaoVendaImportar(linha);

			} catch (Exception e) {
				Utils.gravarLog("\nNVE: " + linha);
			}
		}
	}

	private void getLinhaNaoVendaImportar(String linha) {
		NaoVenda n = new NaoVenda();
		n.setIdNaoVenda(Short.parseShort(linha.substring(0, 4).trim()));
		n.setDescricao(linha.substring(4, 64));

		insert(n);
	}

	public List<NaoVenda> getAll() {
		List<NaoVenda> retorno = new ArrayList<NaoVenda>();
		SQLiteDatabase dbLocal = (db == null) ? new DBHelper(ctx).getReadableDatabase() : db;
		Cursor c = null;
		try {
			c = dbLocal.query(TABELA, null, null, null, null, null, "Descricao");
			while (c.moveToNext()) {
				NaoVenda naoVenda = new NaoVenda();
				preencherObjeto(c, naoVenda);
				retorno.add(naoVenda);
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			if (c != null) {
				c.close();
			}
			//if (db == null) {
				//dbLocal.close(); singletone();
			//}
		}
		return retorno;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		getLinhaNaoVendaImportar(linha);

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_motivo_nao_venda);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
