package br.net.sav.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.NaoVendaMovimento;
import br.net.sav.util.StringUtils;

public class NaoVendaMovimentoDAO extends PadraoDAO {
	private static final String TABELA = "NaoVendaMovimento";
	
	public NaoVendaMovimentoDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	
	public void gerarTextos(Long idVendedor) {
		List<NaoVendaMovimento> listaNaoVenda = listaNaoVendaMovimentoEnviar();
		
		if (listaNaoVenda.isEmpty()) {
			return ;
		}
		try {
			Date data = new Date();
			String campo;
			String nomeArquivoNaoVenda = String.format("nvd%08d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));
			FileOutputStream outNve = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() +"/sav/envia", nomeArquivoNaoVenda));
			
			for (NaoVendaMovimento naoVenda : listaNaoVenda) {
				
				campo = String.format("%08d", idVendedor);
				outNve.write(campo.getBytes());
				
				campo = String.format("%06d", naoVenda.getCliente().getIdCliente() >= 1000000 ? 0 : naoVenda.getCliente().getIdCliente());
				outNve.write(campo.getBytes());
				
				campo = String.format("%-50.50s", naoVenda.getCliente().getRazao());
				outNve.write(campo.getBytes());
				
				campo = String.format("%4.4s", naoVenda.getMotivo());
				outNve.write(campo.getBytes());
				
				campo = String.format("%s", Utils.sdfDataHoraExport.format(naoVenda.getDataHora()));
				outNve.write(campo.getBytes());
				
				campo = String.format("%-90.90s", naoVenda.getCliente().getEndereco());
				outNve.write(campo.getBytes());
								
				outNve.write(StringUtils.QUEBRA_LINHA.getBytes());
				
				outNve.flush();
			}			
			outNve.close();
			
		} catch (Exception e) {
			Log.e(getClass().getName() +"exportar - ", e.getMessage());
		}
	}
	
	public void marcarEnviado(Date dataEnvio) {
		SQLiteDatabase dbLocal = (db == null) ? new DBHelper(ctx).getWritableDatabase() : db;
	
		String sql = String.format("UPDATE "+TABELA+" SET Enviado = 1, "
				+"DataEnvio = \'%s\' WHERE Enviado = 0", Utils.sdfDataHoraDb.format(dataEnvio));
		dbLocal.execSQL(sql);
		if (db == null) {
			//dbLocal.close(); singletone();
		}
	}
	
	public void apagarAposPrazo(short quantidadeDiasApagar) {
		SQLiteDatabase dbLocal = (db == null) ? new DBHelper(ctx).getWritableDatabase() : db;
		List<NaoVendaMovimento> listaNaoVendaApagar = new ArrayList<NaoVendaMovimento>();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, quantidadeDiasApagar * -1);
		Date dataApagar = cal.getTime();
		
		Cursor c = dbLocal.query(TABELA, null, "Enviado = 1 AND DataEnvio <= ?", 
				new String[] { Utils.sdfDataDb.format(dataApagar) + " 00:00:00" }, null, null, null);

		try {
			while (c.moveToNext()) {
				listaNaoVendaApagar.add(getObjetoPreenchido(c));
			}
			if (db == null) {
				db = dbLocal;
			}
			for (NaoVendaMovimento naoVenda: listaNaoVendaApagar) {
				delete(naoVenda);
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}
	}
	
	public List<NaoVendaMovimento> listaNaoVendaMovimentoEnviar() {
		List<NaoVendaMovimento> naoVendaMovimento = new ArrayList<NaoVendaMovimento>();
		SQLiteDatabase dataBaseLocal = (db == null) ? new DBHelper(ctx).getWritableDatabase() : db;
		
		String sql = "SELECT n.*, COALESCE(c.Razao,'') as NomeCliente, COALESCE(c.Endereco,'') as EnderecoCliente \n" +
				"FROM NaoVendaMovimento n \n" +
				"LEFT JOIN Cliente c ON (n.IdCliente = c.IdCliente) \n" +
				"WHERE n.Enviado = 0";
		
		Cursor cursor = dataBaseLocal.rawQuery(sql, null);
		
		try {
			while (cursor.moveToNext()) {
				naoVendaMovimento.add(getObjetoPreenchido(cursor));
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			cursor.close();
			if (db == null) {
				//dataBaseLocal.close();
			}
		}
		return naoVendaMovimento;
	}
	
	@Override
	protected NaoVendaMovimento preencherObjeto(Cursor c, Object objeto) {
		return getObjetoPreenchido(c);
	}
	
	public NaoVendaMovimento getObjetoPreenchido(Cursor c) {
		NaoVendaMovimento n = new NaoVendaMovimento();
		try {
			n.getCliente().setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
			n.setDataHora(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataHora"))));
			n.setMotivo(c.getString(c.getColumnIndex("Motivo")));
			if (c.getColumnIndex("NomeCliente") > -1) {
				n.getCliente().setRazao(c.getString(c.getColumnIndex("NomeCliente")));
			}
			if (c.getColumnIndex("EnderecoCliente") > -1) {
				n.getCliente().setEndereco(c.getString(c.getColumnIndex("EnderecoCliente")));
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return n;
	}
	
	@Override
	protected ContentValues preencherContentValues(Object objeto) {
		NaoVendaMovimento n = (NaoVendaMovimento) objeto;
		ContentValues ctv = new ContentValues();
		try {
			ctv.put("IdCliente", n.getCliente().getIdCliente());
			ctv.put("Motivo", n.getMotivo());
			ctv.put("DataHora", Utils.sdfDataHoraDb.format(n.getDataHora()));
			ctv.put("Enviado", n.isEnviado());
			if (n.getDataEnvio() != null)
				ctv.put("DataEnvio", Utils.sdfDataHoraDb.format(n.getDataEnvio()));
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return ctv;
	}
}
