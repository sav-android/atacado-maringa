package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Notificacao;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class NotificacaoDao extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "Notificacao";


    public NotificacaoDao(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public void importar(List<String> linhas, boolean cargaCompleta) {
        getImportacaoNotificacao(linhas, cargaCompleta);
    }

    private void getImportacaoNotificacao(List<String> linhas, boolean cargaCompleta) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        try {
            db.beginTransaction();
            for (String linha : linhas) {
                getLinhaNotificacaoImportar(cargaCompleta, sdf, linha);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());

        } finally {
            db.endTransaction();
        }
    }

    private void getLinhaNotificacaoImportar(boolean cargaCompleta, SimpleDateFormat sdf, String linha) {
        try {
            Notificacao n = new Notificacao();
            n.setIdNotificacao(Integer.valueOf(linha.substring(0, 14)));
            n.setDescricaoNotificacao(linha.substring(14, 214).trim());
            n.setFrequanciaNotificacao(Integer.valueOf(linha.substring(214, 216)));
            n.setDataInicial(sdf.parse(linha.substring(216, 224).trim()));
            n.setDataFinal(sdf.parse(linha.substring(224, 232).trim()));

            if (!linha.substring(232, 253).trim().equals(""))
                n.setIdPedido(Long.valueOf(linha.substring(232, 253 - 4).trim()));
            n.setLido(false);

            if (!cargaCompleta) {
                switch (Character.toUpperCase(linha.charAt(257))) {
                    case ' ':
                    case 'A':
                        if (!exists(n))
                            insert(n);
                        break;
                    case 'E':
                        delete(n);
                        break;
                    default:
                        Utils.gravarLog("\nNOT(Flag Inv�lida): " + linha);
                        break;
                }
            } else {
                if (!exists(n))
                    insert(n);
            }

        } catch (Exception e) {
            Utils.gravarLog("\nNOT: " + linha);
        }
    }

    public void removerNotificacoesVencidas() {
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd");

        SQLiteDatabase dbLocal = new DBHelper(ctx).getReadableDatabase();
        Date dataAtual = new Date();

        // dbLocal.execSQL("delete from "+TABELA+" where DataFinal < '"+dataFormat.format(dataAtual)+"'");

    }

    public List<Notificacao> buscarNotficacoesValidas() {
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd");

        SQLiteDatabase dbLocal = new DBHelper(ctx).getReadableDatabase();
        Date dataAtual = new Date();
        Cursor cursor = dbLocal.rawQuery("SELECT * FROM " + TABELA + " WHERE DataInicial <= '" + dataFormat.format(dataAtual) + " 23:59' and DataFinal >= '" + dataFormat.format(dataAtual) + "'", null);
        ArrayList<Notificacao> notificacoesEncontrados = new ArrayList<>();
        while (cursor.moveToNext()) {
            notificacoesEncontrados.add((Notificacao) preencherObjeto(cursor, new Notificacao()));
        }

        return notificacoesEncontrados;
    }

    public void atualizar(Notificacao notificacao) {
        update(notificacao);
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        getLinhaNotificacaoImportar(true, sdf, linha);
        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_Notificacao);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
