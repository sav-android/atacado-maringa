package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.ObjetivoMeta;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ObjetivoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Objetivo";

	public ObjetivoDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
	}

	public Boolean insert(ObjetivoMeta objetivoMeta, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		ContentValues ctv = new ContentValues();

		ctv.put("Tipo", objetivoMeta.getTipo());
		ctv.put("Descricao", objetivoMeta.getDescricao());
		ctv.put("Objetivo", objetivoMeta.getObjetivo());
		ctv.put("Realizado", objetivoMeta.getRealizado());

		boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public Boolean deleteAll() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		boolean retorno = db.delete(TABELA, null, null) > 0;

		//db.close(); singleton();

		return retorno;
	}

	public List<ObjetivoMeta> getObjetivosPorTipo(int tipo) {
		List<ObjetivoMeta> lista = new ArrayList<ObjetivoMeta>();
		String[] param = new String[] { String.valueOf(tipo) };

		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		Cursor c = null;

		try {
			long diasUteisMes = Utils.diasUteisMes(false);
			long diasPassouMes = Utils.diasPassouMes(false);

			if (tipo > -1)
				c = db.query(TABELA, colunas, "Tipo=?", param, null, null, null);
			else
				c = db.query(TABELA, colunas, null, null, null, null, null);

			while (c.moveToNext()) {
				ObjetivoMeta objetivo = new ObjetivoMeta();

				objetivo.setTipo(c.getShort(c.getColumnIndex("Tipo")));
				objetivo.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				objetivo.setObjetivo(c.getDouble(c.getColumnIndex("Objetivo")));
				objetivo.setRealizado(c.getDouble(c.getColumnIndex("Realizado")));

				objetivo.setPercRealizado(Utils.arredondar(objetivo.getRealizado() / objetivo.getObjetivo() * 100, 2));
				objetivo.setTendencia(Utils.arredondar(objetivo.getRealizado() / diasUteisMes, 2));
				objetivo.setPercTendencia(Utils.arredondar(objetivo.getRealizado() / diasPassouMes * diasUteisMes / objetivo.getObjetivo(), 2));
				objetivo.setSugestaoDia(Utils.arredondar((objetivo.getObjetivo() - objetivo.getRealizado()) / (diasUteisMes - diasPassouMes), 2));

				lista.add(objetivo);
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			if (c != null)
				c.close();
			//db.close(); singleton();
		}

		return lista;
	}

	public void importar(List<String> linhasTexto) {
		try {
			deleteAll();
			getImportacaoObjMeta(linhasTexto);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
	}

	private void getImportacaoObjMeta(List<String> linhasTexto)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaObjetivoMetaImportar(linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaObjetivoMetaImportar(String linha) {
		try {
			ObjetivoMeta o = new ObjetivoMeta();

			if (linha.substring(0, 2).trim().equals(""))
				o.setTipo((short) 0);
			else if (linha.substring(0, 2).trim().matches(MATCHES))
				o.setTipo(Short.parseShort(linha.substring(0, 2).trim()));
			else {
				Utils.gravarLog("\nOBJ: " + linha);
				return true;
			}
			o.setDescricao(linha.substring(2, 42).trim());
			if (linha.substring(42, 52).trim().equals(""))
				o.setObjetivo(0);
			else if (linha.substring(42, 52).trim().matches(MATCHES))
				o.setObjetivo(Double.parseDouble(linha.substring(42, 52).trim()) / 100);
			else {
				Utils.gravarLog("\nOBJ: " + linha);
				return true;
			}
			if (linha.substring(52, 62).trim().equals(""))
				o.setRealizado(0);
			else if (linha.substring(52, 62).trim().matches(MATCHES))
				o.setRealizado(Double.parseDouble(linha.substring(52, 62).trim()) / 100);
			else {
				Utils.gravarLog("\nOBJ: " + linha);
				return true;
			}
			insert(o);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
			Utils.gravarLog("\nOBJ: " + linha);
		}
		return false;
	}

	public boolean existemRegistrosBanco(boolean apenasTiposValidos) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		boolean retorno = false;
		Cursor c = null;
		StringBuilder sql = new StringBuilder();

		sql.append(String.format("SELECT COUNT(*) FROM %s ", TABELA));
		if (apenasTiposValidos)
			sql.append("WHERE Tipo IN (1,2,3,4)");

		try {
			c = db.rawQuery(sql.toString(), null);
			if (c.moveToFirst()) {
				retorno = c.getInt(0) > 0;
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		} finally {
			if (c != null)
				c.close();
			//db.close(); singleton();
		}

		return retorno;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaObjetivoMetaImportar(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_tabela_obj_meta);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
