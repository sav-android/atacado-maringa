package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.ObservacaoCliente;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ObservacaoClienteDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	public static final String TABELA="ObservacaoCliente";
	
	public ObservacaoClienteDAO(Context ctx,SQLiteDatabase db){
		super(ctx,TABELA,db);
	}
	
	public void importar(List<String> linhasTexto,boolean cargaCompleta) {
		try{
			if(cargaCompleta)
				deleteAll();
			getImportacaoObsCliente(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("CondicaoProdutoDAO", e.getMessage());
		}
	}

	private void getImportacaoObsCliente(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaObsCliente(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaObsCliente(boolean cargaCompleta, String linha) {
		ObservacaoCliente ob = new ObservacaoCliente() ;

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
			ob.setIdCliente(Long.parseLong(linha.substring(0,8).trim()));
		else {
			Utils.gravarLog("\nOBS: " +linha);
			return true;
		}
		if(linha.substring(8, 188).trim().matches(MATCHES) && !linha.substring(8, 188).trim().equals(""))
			ob.setObservacao(linha.substring(8, 188).trim());
		else {
			Utils.gravarLog("\nOBS: " +linha);
			return true;
		}

		if(!cargaCompleta){
			switch(Character.toUpperCase(linha.charAt(23))){
			case' ':
			case'A':
				if (!exists(ob))
					insert(ob);
				else
					update(ob);
				break;
			case'E':
				delete(ob);
				break;
			default:
				Utils.gravarLog("\nOBS: " +linha);
				break;
			}
		} else {
			insert(ob);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaObsCliente(true, linha)) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_observacoes_cliente_bloqueado);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
