package br.net.sav.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

public class PadraoDAO {
	protected static final String MATCHES = "^[0-9]*";
	protected Context ctx;
	protected String TABELA;
	protected String[] colunas;
	protected String[] primaryKeys;
	protected SQLiteDatabase db;
	private boolean instanciado = false;

	public PadraoDAO(Context ctx, String tabela, SQLiteDatabase db) {
		this.ctx = ctx;

		TABELA = tabela;
		this.db = db;

		if (!instanciado) {
			if (tabela != null) {
				colunas = getCamposTabela(db, ctx, TABELA);
				primaryKeys = getPrimaryKeys(db, ctx, TABELA);
				instanciado = true;
			}
		}
	}

	public Boolean insert(Object objeto) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;
		long insert = dbLocal.insert(TABELA, null, preencherContentValues(objeto));
		boolean retorno =  insert > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public Boolean insert(Object objeto, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		boolean retorno = dbLocal.insert(TABELA, null, preencherContentValues(objeto)) > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public Boolean delete(Object objeto) {
		SQLiteDatabase dbLocal = null;
		boolean retorno = false;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;
		try {
			retorno = dbLocal.delete(TABELA, getPksWhereClauses(primaryKeys), getParams(objeto)) > 0;
		} catch (Exception e) {
			Log.e("PadraoDao", "Delete " + e.getMessage());
		}

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public Boolean deleteAll() {
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		boolean retorno = dbLocal.delete(TABELA, null, null) > 0;

//		if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public Boolean update(Object objeto) {
		SQLiteDatabase dbLocal = null;
		boolean retorno = false;
		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		try {
			retorno = dbLocal.update(TABELA, preencherContentValues(objeto), getPksWhereClauses(primaryKeys), getParams(objeto)) > 0;
		} catch (Exception e) {
			Log.e("PadraoDao", "update " + e.getMessage());
		}

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	public boolean exists(Object objeto) {
		SQLiteDatabase dbLocal = null;
		boolean retorno = false;
		Cursor c = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		try {
			c = dbLocal.rawQuery("Select Count(*) from " + TABELA + " where " + getPksWhereClauses(primaryKeys), getParams(objeto));
			if (c.moveToFirst())
				retorno = c.getInt(0) > 0;
		} catch (Exception e) {
			Log.e("PadraoDAO", "exists " + TABELA + e.getMessage());
		} finally {
			if (c != null)
				c.close();
		}

//		if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	@SuppressWarnings("rawtypes")
	@SuppressLint("UseValueOf")
	protected Object preencherObjeto(Cursor c, Object objeto) {
		SimpleDateFormat sdfT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		for (int i = 0; i < colunas.length; i++) {
			try {
				Class campo = objeto.getClass().getField(colunas[i]).getType();
				if (c.getColumnIndex(colunas[i]) != -1) {

					if (campo.equals(new String().getClass())) {
						if (c.getString(c.getColumnIndex(colunas[i])) != null)
							objeto.getClass().getField(colunas[i]).set(objeto, c.getString(c.getColumnIndex(colunas[i])));
						else
							objeto.getClass().getField(colunas[i]).set(objeto, "");
					}

					else if (campo.equals(new Long(0).getClass()) || campo.equals(long.class))
						objeto.getClass().getField(colunas[i]).setLong(objeto, c.getLong(c.getColumnIndex(colunas[i])));

					else if (campo.equals(new Double(0).getClass()) || campo.equals(double.class))
						objeto.getClass().getField(colunas[i]).setDouble(objeto, c.getDouble(c.getColumnIndex(colunas[i])));

					else if (campo.equals(int.class))
						objeto.getClass().getField(colunas[i]).setInt(objeto, c.getInt(c.getColumnIndex(colunas[i])));

					else if (campo.equals(new Date().getClass())) {
						if (c.getString(c.getColumnIndex(colunas[i])).length() == 10)
							objeto.getClass().getField(colunas[i]).set(objeto, sdf.parse(c.getString(c.getColumnIndex(colunas[i]))));
						else
							objeto.getClass().getField(colunas[i]).set(objeto, sdfT.parse(c.getString(c.getColumnIndex(colunas[i]))));
					}

					else if (campo.equals(new Short((short) 0).getClass()) || campo.equals(short.class))
						objeto.getClass().getField(colunas[i]).setShort(objeto, c.getShort(c.getColumnIndex(colunas[i])));

					else if (campo.equals(new Boolean(true).getClass()) || campo.equals(boolean.class))
						objeto.getClass().getField(colunas[i]).setBoolean(objeto, c.getInt(c.getColumnIndex(colunas[i])) > 0);
				}

			} catch (Exception e) {
				Log.e("PadraoDAO", "PreencherObjeto: " + e.getMessage());
			}
		}

		return objeto;
	}

	@SuppressLint("UseValueOf")
	@SuppressWarnings("rawtypes")
	protected ContentValues preencherContentValues(Object objeto) {

		ContentValues ctv = new ContentValues();
		SimpleDateFormat sdfT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			for (int i = 0; i < colunas.length; i++) {
				Object campo = objeto.getClass().getField(colunas[i]).get(objeto);
				Class tipo = objeto.getClass().getField(colunas[i]).getType();

				if (tipo.equals(new String().getClass())) {
					if (campo != null)
						ctv.put(colunas[i], campo.toString());
					else
						ctv.put(colunas[i], "");
				}

				else if (tipo.equals(new Long(0).getClass()) || tipo.equals(long.class))
					ctv.put(colunas[i], Long.parseLong(campo.toString()));

				else if (tipo.equals(new Double(0).getClass()) || tipo.equals(double.class))
					ctv.put(colunas[i], Double.parseDouble(campo.toString()));

				else if (tipo.equals(new Integer(0).getClass()) || tipo.equals(int.class))
					ctv.put(colunas[i], Integer.parseInt(campo.toString()));

				else if (tipo.equals(new Date().getClass())) {
					if (campo != null) {
						if (campo.toString().length() == 10)
							ctv.put(colunas[i], sdf.format(campo));
						else
							ctv.put(colunas[i], sdfT.format(campo));
					} else
						ctv.put(colunas[i], "0000-00-00");
				}

				else if (tipo.equals(new Short((short) 0).getClass()) || tipo.equals(short.class))
					ctv.put(colunas[i], Short.parseShort(campo.toString()));

				else if (tipo.equals(new Boolean(true).getClass()) || tipo.equals(boolean.class))
					ctv.put(colunas[i], Boolean.parseBoolean(campo.toString()));
			}
		} catch (Exception e) {
			Log.e("PadraoDAO", e.getMessage());
		}

		return ctv;
	}

	private String getPksWhereClauses(String[] primaryKeys) {
		String pks = "";
		for (int i = 0; i < primaryKeys.length; i++) {
			if (i + 1 >= primaryKeys.length)
				pks += primaryKeys[i] + "=?";
			else
				pks += primaryKeys[i] + "=? and  ";
		}
		return pks;
	}

	private String[] getParams(Object objeto) {
		String[] params = new String[primaryKeys.length];

		for (int i = 0; i < primaryKeys.length; i++) {
			try {
				params[i] = objeto.getClass().getField(primaryKeys[i]).get(objeto).toString();
			} catch (Exception e) {
				Log.d("PadraoDAO.getParams", e.getMessage());
			}
		}
		return params;
	}

	public static String[] getCamposTabela(SQLiteDatabase db, Context ctx, String tabela) {
		SQLiteDatabase dbLocal = null;

		int i = 0;

		if (db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db;

		Cursor c = null;
		String[] campos = null;
		try {
			c = dbLocal.rawQuery(String.format("pragma table_info(%s)", tabela), null);
			campos = new String[c.getCount()];

			while (c.moveToNext()) {
				campos[i] = c.getString(c.getColumnIndex("name"));
				i++;
			}
		} catch (Exception e) {
			Log.e("PadraoDAO:getCamposTabela", e.getMessage());
		} finally {
			c.close();
//			if (db == null)
				//dbLocal.close(); singletone();
		}

		return campos;
	}

	public static String[] getPrimaryKeys(SQLiteDatabase db, Context ctx, String tabela) {
		SQLiteDatabase dbLocal;
		boolean pk = false;
		int i = 0;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		Cursor c = dbLocal.rawQuery(String.format("pragma table_info(%s)", tabela), null);
		List<String> lista = new ArrayList<String>();

		while (c.moveToNext()) {
			if (c.getInt(c.getColumnIndex("pk")) >= 1) {
				lista.add(c.getString(c.getColumnIndex("name")));
				pk = true;
			} else if (i == c.getCount() - 1 && !pk) {
				lista.add(c.getString(c.getColumnIndex("name")));
			}
			i++;
		}
		i = 0;
		//if (db == null)
			//dbLocal.close(); singletone();

		String[] pks = new String[lista.size()];
		for (@SuppressWarnings("unused")
		String s : lista) {
			pks[i] = lista.get(i);
			i++;
		}

		c.close();
		return pks;

	}

	public static void gerarModelo(Context ctx, SQLiteDatabase db) {
		try {
			File arquivo = new File(Environment.getExternalStorageDirectory() + "/sav/Modelos.txt");
			FileOutputStream fs = new FileOutputStream(arquivo);
			List<String> tabelas = getTabelas(db);
			String texto = "";
			String metodos;

			for (int i = 0; i < tabelas.size(); i++) {
				texto = String.format("\n ###############################################\n\npublic Class %s{\n", tabelas.get(i));
				Cursor c = db.rawQuery(String.format("pragma table_info(%s)", tabelas.get(i)), null);
				metodos = "";
				while (c.moveToNext()) {
					texto += "    public  ";
					String campo = c.getString(c.getColumnIndex("name"));

					if (c.getString(c.getColumnIndex("type")).toLowerCase().substring(0, 3).equals("nva")) {
						texto += "String  ";
						metodos += String.format("    public void set%s(String %s){\n        this.%s=%s; \n    } \n\n", campo, campo, campo, campo);
						metodos += String.format("    public String get%s(){\n    	return %s; \n    } \n\n", campo, campo);
					} else if (c.getString(c.getColumnIndex("type")).toLowerCase().equals("double")) {
						texto += "double  ";
						metodos += String.format("    public void set%s(double %s){\n        this.%s=%s; \n    } \n\n", campo, campo, campo, campo);
						metodos += String.format("    public double get%s(){\n            return %s; \n    } \n\n", campo, campo);
					} else if (c.getString(c.getColumnIndex("type")).toLowerCase().equals("integer")) {
						texto += "long    ";
						metodos += String.format("    public void set%s(long %s){\n        this.%s=%s; \n    } \n\n", campo, campo, campo, campo);
						metodos += String.format("    public long get%s(){\n        return %s; \n    } \n\n", campo, campo);
					} else if (c.getString(c.getColumnIndex("type")).toLowerCase().equals("smallint")) {
						texto += "short   ";
						metodos += String.format("    public void set%s(short %s){\n        this.%s=%s; \n    }  \n\n", campo, campo, campo, campo);
						metodos += String.format("    public short get%s(){\n        return %s; \n    } \n\n", campo, campo);
					} else if (c.getString(c.getColumnIndex("type")).toLowerCase().equals("timestamp") || c.getString(c.getColumnIndex("type")).toLowerCase().equals("date")) {
						texto += "Date    ";
						metodos += String.format("    public void set%s(Date %s){\n        this.%s=%s; \n    }  \n\n", campo, campo, campo, campo);
						metodos += String.format("    public Date get%s(){\n        return %s; \n    } \n\n", campo, campo);
					} else if (c.getString(c.getColumnIndex("type")).toLowerCase().equals("boolean")) {
						texto += "boolean ";
						metodos += String.format("    public void set%s(boolean %s){\n        this.%s=%s; \n    } \n\n", campo, campo, campo, campo);
						metodos += String.format("    public boolean get%s(){\n        return %s; \n    } \n\n", campo, campo);
					}

					texto += campo + ";\n";

				}
				texto += "\n" + metodos;
				fs.write(texto.getBytes());
				c.close();
				fs.flush();
			}
			fs.close();
		} catch (Exception e) {
			Log.e("PadraoDAO.gerarModelo", e.getMessage());
		}
		// //db.close(); singleton();
	}
	
	public void fecharDB() {
		if (db.isOpen()) {
			//db.close(); singleton();
		}
	}

	public static List<String> getTabelas(SQLiteDatabase db) {
		List<String> lista = new ArrayList<String>();
		Cursor c = db.rawQuery("SELECT name as coluna FROM sqlite_master WHERE type=\'table\' ORDER BY name;", null);
		while (c.moveToNext())
			lista.add(c.getString(c.getColumnIndex("coluna")));

		c.close();
		return lista;
	}

	public void importarGen() throws FileNotFoundException, UnsupportedEncodingException {
		File f = new File(Environment.getExternalStorageDirectory().getPath() + "/sav/recebe/");
		FileInputStream fin = new FileInputStream(f);
		BufferedReader br;
		HashMap<String, String> keys = new HashMap<String, String>();
		@SuppressWarnings("unused")
		HashMap<String, Layout> layout = new HashMap<String, Layout>();

		File[] arquivos = f.listFiles();

		for (File fi : arquivos) {
			String iniciais = fi.getName().toString();
			br = new BufferedReader(new InputStreamReader(fin, "iso-8859-1"));

			String linha;
			String aux = "";

			short ini = 0, mei = 0, fim = 0, i = 0;
			boolean linha1 = true;
			if (iniciais.equals("KEY")) {
				try {
					while ((linha = br.readLine()) != null) {
						keys.put(linha.substring(0, 3), linha.substring(3));
					}
					br.close();
				} catch (IOException e) {
					Log.e("PadraoDAO.ImportaGen", e.getMessage());
				}
			} else if (iniciais.equals("SIP")) {

				try {
					while ((linha = br.readLine()) != null) {
						if (linha1) {
							ini = Short.parseShort(linha.substring(0, 3));
							mei = Short.parseShort(linha.substring(3, 6));
							fim = Short.parseShort(linha.substring(6, 9));
							linha1 = false;
						} else {
							Layout l = new Layout();

							l.campos[i] = linha.substring(ini, mei).trim();
							l.iniciais[i] = Short.parseShort(linha.substring(mei, fim).trim());
							l.finais[i] = Short.parseShort(linha.substring(fim).trim());

							if (aux != linha.substring(0, ini)) {
								l.tabela = aux = linha.substring(0, ini).trim();
							}

						}
					}
					br.close();
				} catch (IOException e) {
					Log.e("PadraoDAO.ImportaGen", e.getMessage());
				}
			}
		}

	}

	// ------------------------Layout----------------------
	private static class Layout {
		@SuppressWarnings("unused")
		protected String tabela;
		protected String[] campos;
		protected short[] iniciais;
		protected short[] finais;

	}

}
