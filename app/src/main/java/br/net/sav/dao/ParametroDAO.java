package br.net.sav.dao;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Parametro;
import br.net.sav.util.Logs;
import br.net.sav.util.dao.VendedorParametroDAO;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ParametroDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Parametro";

	public ParametroDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}
	
	public void atualizaSaldoVerba() {
		
	}

	public Parametro get () {
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db ;
		
		Parametro parametro = null ;
		
		Cursor c = dbLocal.rawQuery(" select * from Parametro ", null);
		
		if(c.moveToFirst()) {
			try {
				parametro = (Parametro) preencherParametro(c);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		c.close();
		
	//	if(db == null)
			//dbLocal.close(); singleton();
		
		return parametro ;
	}

	public void alterarDataExpiraLiberacaoAtendimento(Parametro parametro, Date dataAtual) {
		dataAtual = Utils.alterarData(dataAtual, 24);

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = String.format("UPDATE Parametro SET DataExpiraLiberacao = \'%s\' WHERE IDVendedor = %d", Utils.sdfDataHoraDb.format(dataAtual), parametro.getIdVendedor());
		db.execSQL(sql);
		//db.close(); singleton();
	}
	
	public void atualizaDataUltimoAcesso(Date data) {
		SQLiteDatabase dbLocal = new DBHelper(ctx).getWritableDatabase();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		ContentValues ctv = new ContentValues();
		ctv.put("DataUltimoAcesso", sdf.format(data));
		
		dbLocal.update(TABELA, ctv, null, null);
		//db.close(); singletone();
	}		
	
	public Parametro preencherParametro(Cursor c) throws ParseException {
		Parametro parametro = new Parametro();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		
		parametro.setControlaLimiteCredito(c.getInt(c.getColumnIndex("ControlaLimiteCredito")) > 0);
		parametro.setControlaTabelaPrecoCliente(c.getInt(c.getColumnIndex("ControlaTabelaPrecoCliente")) > 0);
		parametro.setDadosAdicionais(c.getShort(c.getColumnIndex("DadosAdicionais")));
		parametro.setDataFaturamento(c.getShort(c.getColumnIndex("DataFaturamento")) > 0);
		parametro.setDataUltimaRecepcao(sdf.parse(c.getString(c.getColumnIndex("DataUltimaRecepcao"))));
		parametro.setDataUltimoAcesso(sdf.parse(c.getString(c.getColumnIndex("DataUltimoAcesso"))));
		parametro.setDDD(c.getString(c.getColumnIndex("DDD")));
		parametro.setDescontoExtra(c.getShort(c.getColumnIndex("DescontoExtra")));
		//parametro.setDiasValidadeOrcamento(c.getShort(c.getColumnIndex("")));
		parametro.setExcessoPrecoGeraFlex(c.getShort(c.getColumnIndex("ExcessoPrecoGeraFlex")) > 0);
		parametro.setGeraVerbaPalmtop(c.getShort(c.getColumnIndex("GeraVerbaPalmtop")));
		parametro.setIDEmpresaClienteNovo(c.getShort(c.getColumnIndex("IdEmpresaClienteNovo")));
		parametro.setIDEmpresaTabPadrao(c.getShort(c.getColumnIndex("IdEmpresaTabPadrao")));
		parametro.setIDFilialClienteNovo(c.getShort(c.getColumnIndex("IdFilialClienteNovo")));
		parametro.setIDFilialTabPadrao(c.getShort(c.getColumnIndex("IdFilialTabPadrao")));
		parametro.setIdVendedor(c.getLong(c.getColumnIndex("IdVendedor")));
		parametro.setIncluiEmbalagem(c.getShort(c.getColumnIndex("IncluiEmbalagem")));
		parametro.setLimiteMinimoPedido(c.getDouble(c.getColumnIndex("LimiteMinimoPedido")));
		parametro.setMotivoNaoVenda(c.getShort(c.getColumnIndex("MotivoNaoVenda")));
		parametro.setNome(c.getString(c.getColumnIndex("Nome")));
		parametro.setOrdemListaProdutos(c.getShort(c.getColumnIndex("OrdemListaProdutos")));
		parametro.setPercDescontoProdutoGeraVerba(c.getShort(c.getColumnIndex("PercDescontoProdutoGeraVerba")) > 0);
		parametro.setPercMaximoDescontoFlex(c.getDouble(c.getColumnIndex("PercMaximoDescontoFlex")));
		parametro.setPermiteAlterarPreco(c.getShort(c.getColumnIndex("PermiteAlterarPreco")) > 0);
		parametro.setPermiteIncluirCliente(c.getShort(c.getColumnIndex("PermiteIncluirCliente")) > 0);
		parametro.setPermiteUnitarioSugestaoVenda(c.getShort(c.getColumnIndex("PermiteUnitarioSugestaoVenda")) > 0);
		parametro.setPosicaoCodClienteConsulta(c.getShort(c.getColumnIndex("PosicaoCodClienteConsulta")));
		parametro.setQtdeBonifAbateVerba(c.getShort(c.getColumnIndex("QtdeBonifAbateVerba")) > 0);
		parametro.setQtdeDiasBloquearInadimplente(c.getShort(c.getColumnIndex("QtdeDiasBloquearInadimplente")));
		parametro.setQtdeDiasGuardarPedido(c.getShort(c.getColumnIndex("QtdeDiasGuardarPedido")));
		parametro.setQtdeMaximaItens(c.getShort(c.getColumnIndex("QtdeMaximaItens")));
		parametro.setSaldoVerba(c.getDouble(c.getColumnIndex("SaldoVerba")));
		//parametro.setTabelaPadraoItens(c.getShort(c.getColumnIndex("")));
		parametro.setTabelaPrecoClienteNovo(c.getShort(c.getColumnIndex("TabelaPrecoClienteNovo")));
		parametro.setTelefone(c.getString(c.getColumnIndex("Telefone")));
		parametro.setUtilizaImpressora(c.getShort(c.getColumnIndex("UtilizaImpressora")) > 0);
		parametro.setUtilizaTroca(c.getShort(c.getColumnIndex("UtilizaTroca")));
		parametro.setValorMaximoExcederVerba(c.getDouble(c.getColumnIndex("ValorMaximoExcederVerba")));
		parametro.setDescontoProdutoVendedor(c.getDouble(c.getColumnIndex("DescontoProdutoVendedor")));
		parametro.setPercentualRentabilidadeMinimaDoPedido(c.getDouble(c.getColumnIndex("PercentualRentabilidadeMinimaDoPedido")));
		parametro.setPercRentabilidadeMediaDoPedido(c.getDouble(c.getColumnIndex("PercRentabilidadeMediaDoPedido")));
		parametro.setVisualizarRentabilidade(c.getInt(c.getColumnIndex("VisualizarRentabilidade"))> 0);
		parametro.setSupervisor(c.getInt(c.getColumnIndex("Supervisor")) > 0);
		parametro.setPercentualBonificacao(c.getDouble(c.getColumnIndex("PercentualBonificacao")));
		parametro.setSolicitarSenhaRentabilidade(c.getInt(c.getColumnIndex("SolicitarSenhaRentabilidade")) > 0);
		parametro.setTokenApi(c.getString(c.getColumnIndex("TokenApi")));
		parametro.setVisualizarBotaoContato(c.getInt(c.getColumnIndex("VisualizarBotaoContato")) > 0);
		parametro.setAtivo(c.getInt(c.getColumnIndex("Ativo"))==1);
		parametro.setIdVendedorCM(c.getLong(c.getColumnIndex("idVendedorCM")));
		parametro.setDataUltimaVerificacaoCM(c.getString(c.getColumnIndex("DataUltimaVerificacaoCM"))!=null?sdf.parse(c.getString(c.getColumnIndex("DataUltimaVerificacaoCM"))):null);
		parametro.setQuantidadeMinimaItensRentabilidade(c.getShort(c.getColumnIndex("QuantidadeMinimaItensRentabilidade")));
		parametro.setIdSequencialPedido(c.getLong(c.getColumnIndex("IdSequencialPedido")));
		return parametro;
	}
	
	public void importar (List<String> linhasTexto) {
		try {
			Parametro parametroAtual = null;
			try {
				parametroAtual = get();
			} catch (Exception e) {
				Log.e(getClass().getSimpleName(), e.getMessage());
			}
			deleteAll();
			getImportacaoParametro(linhasTexto, parametroAtual);
		} catch (Exception e) {
			Log.d("ParametroDAO", e.getMessage());
		}
	}

	private void getImportacaoParametro(List<String> linhasTexto, Parametro parametroAtual)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaParametro(parametroAtual, linha)) continue;
			}

			db.setTransactionSuccessful();
		}  catch (Exception e) {
			Log.d("ParametroDAO", e.getMessage());
		}finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaParametro(Parametro parametroAtual, String linha) {
		Parametro p = new Parametro();

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
			p.setIdVendedor(Short.parseShort(linha.substring(0, 8).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setNome(linha.substring(8, 58).trim());
		if(linha.substring(58, 60).trim().equals(""))
			p.setDDD("0");
		else if(linha.substring(58, 60).trim().matches(MATCHES))
			p.setDDD(linha.substring(58, 60).trim());
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setTelefone(linha.substring(60, 68).trim());
		if(linha.substring(68, 78).trim().equals(""))
			p.setLimiteMinimoPedido(0);
		else if(linha.substring(68, 78).trim().matches(MATCHES))
			p.setLimiteMinimoPedido(Double.parseDouble(linha.substring(68, 78).trim()) / 100);
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(78,80).trim().equals(""))
			p.setQtdeDiasGuardarPedido((short) 0);
		else if(linha.substring(78, 80).trim().matches(MATCHES))
			p.setQtdeDiasGuardarPedido(Short.parseShort(linha.substring(78,80).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(80,83).trim().equals(""))
			p.setQtdeMaximaItens((short) 0);
		else if(linha.substring(80,83).trim().matches(MATCHES))
			p.setQtdeMaximaItens(Short.parseShort(linha.substring(80, 83).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(83, 85).trim().equals(""))
			p.setQtdeDiasBloquearInadimplente((short) 0);
		else if(linha.substring(83, 85).trim().matches(MATCHES))
			p.setQtdeDiasBloquearInadimplente(Short.parseShort(linha.substring(83, 85).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(85, 86).trim().equals(""))
			p.setOrdemListaProdutos((short) 0);
		else if(linha.substring(85, 86).trim().matches(MATCHES))
			p.setOrdemListaProdutos(Short.parseShort(linha.substring(85, 86).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setDiasValidadeOrcamento(Short.parseShort(linha.substring(87,89)));
		p.setPermiteIncluirCliente(linha.substring(89, 90).equals("1"));
		p.setQtdeBonifAbateVerba(linha.substring(91, 92).equals("0"));
		if(linha.substring(91, 92).trim().equals(""))
			p.setPosicaoCodClienteConsulta((short) 0);
		else if(linha.substring(92, 93).trim().matches(MATCHES))
			p.setPosicaoCodClienteConsulta(Short.parseShort(linha.substring(92, 93).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if (linha.substring(93,95).trim().equals(""))
			p.setQuantidadeMinimaItensRentabilidade((short) 0);
		else if(linha.substring(93,95).trim().matches(MATCHES))
			p.setQuantidadeMinimaItensRentabilidade(Short.parseShort(linha.substring(93,95)));
		if(linha.substring(95,99).equals(""))
			p.setTabelaPadraoItens((short)0);
		else if(linha.substring(95,99).trim().matches(MATCHES))
			p.setTabelaPadraoItens(Short.parseShort(linha.substring(95, 96)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(99,103).equals(""))
			p.setIDEmpresaTabPadrao((short)0);
		else if(linha.substring(99,103).trim().matches(MATCHES))
			p.setIDEmpresaTabPadrao(Short.parseShort(linha.substring(99, 103)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}

		if(linha.substring(103,107).equals(""))
			p.setIDFilialTabPadrao((short)0);
		else if(linha.substring(103,107).trim().matches(MATCHES))
			p.setIDFilialTabPadrao(Short.parseShort(linha.substring(103,107)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}

		if(linha.substring(107,111).trim().equals(""))
			p.setTabelaPrecoClienteNovo((short) 0);
		else if(linha.substring(107,111).trim().matches(MATCHES))
			p.setTabelaPrecoClienteNovo(Short.parseShort(linha.substring(107,111).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}

		if(linha.substring(111,115).equals(""))
			p.setIDEmpresaClienteNovo((short)0);
		else if(linha.substring(111,115).trim().matches(MATCHES))
			p.setIDFilialClienteNovo(Short.parseShort(linha.substring(111, 115)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}

		if(linha.substring(115,119).equals(""))
			p.setTabelaPadraoItens((short)0);
		else if(linha.substring(115,119).trim().matches(MATCHES))
			p.setTabelaPadraoItens(Short.parseShort(linha.substring(115,119)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}

		p.setUtilizaImpressora(linha.substring(123, 124).equals("1"));
		if(linha.substring(124, 125).trim().equals(""))
			p.setDescontoExtra((short) 0);
		else if(linha.substring(124, 125).trim().matches(MATCHES))
			p.setDescontoExtra(Short.parseShort(linha.substring(124, 125).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setDataFaturamento(linha.substring(125, 126).equals("1"));
		p.setControlaLimiteCredito(linha.substring(180, 181).equals("1"));
		if(linha.substring(187, 191).trim().equals(""))
			p.setPercMaximoDescontoFlex(0);
		else if(linha.substring(187, 191).trim().matches(MATCHES))
			p.setPercMaximoDescontoFlex(Double.parseDouble(linha.substring(187,191).trim()) / 100);
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(191, 197).trim().equals(""))
			p.setValorMaximoExcederVerba(0);
		else if(linha.substring(191, 197).trim().matches(MATCHES))
			p.setValorMaximoExcederVerba(Double.parseDouble(linha.substring(191, 197).trim()) / 100);
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(197, 207).trim().equals(""))
			p.setSaldoVerba(0);
		else if(linha.substring(197, 207).trim().matches(MATCHES))
			p.setSaldoVerba(Double.parseDouble(linha.substring(197, 207).trim()) / 100);
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setControlaTabelaPrecoCliente(linha.substring(207,208).equals("1"));
		if(linha.substring(208, 209).trim().equals(""))
			p.setMotivoNaoVenda((short) 0);
		else if(linha.substring(208, 209).trim().matches(MATCHES))
			p.setMotivoNaoVenda(Short.parseShort(linha.substring(208, 209).trim()));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setPermiteUnitarioSugestaoVenda(linha.substring(209, 210).equals("1"));
		p.setPercDescontoProdutoGeraVerba(linha.substring(210, 211).equals("1"));
		if(linha.substring(211, 212).trim().equals(""))
			p.setGeraVerbaPalmtop((short) 0);
		else if(linha.substring(211, 212).trim().matches(MATCHES))
			p.setGeraVerbaPalmtop(Short.parseShort(linha.substring(211, 212)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(212, 213).trim().equals(""))
			p.setDadosAdicionais((short) 0);
		else if(linha.substring(212, 213).trim().matches(MATCHES))
			p.setDadosAdicionais(Short.parseShort(linha.substring(212, 213)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(213, 214).trim().equals(""))
			p.setUtilizaTroca((short) 0);
		else if(linha.substring(213, 214).trim().matches(MATCHES))
			p.setUtilizaTroca(Short.parseShort(linha.substring(213, 214)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		p.setPermiteAlterarPreco(linha.substring(214, 215).equals("1"));
		p.setExcessoPrecoGeraFlex(linha.substring(215, 216).equals("1"));
		if(linha.substring(216, 217).trim().equals(""))
			p.setIncluiEmbalagem((short) 0);
		else if(linha.substring(216, 217).trim().matches(MATCHES))
			p.setIncluiEmbalagem(Short.parseShort(linha.substring(216, 217)));
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if(linha.substring(217, 225).trim().equals(""))
			p.setDescontoProdutoVendedor(0.0);
		else if(linha.substring(217, 225).trim().matches(MATCHES))
			p.setDescontoProdutoVendedor(Double.parseDouble(linha.substring(217, 225))/100);
		else {
			Utils.gravarLog("\nPAR: " +linha);
			return true;
		}
		if (linha.length() > 225) {
			p.setVisualizarRentabilidade(linha.substring(225, 226).equals("1"));
		}

		if (linha.length() > 226) {
			if (linha.substring(226, 231).trim().equals(""))
				p.setPercentualRentabilidadeMinimaDoPedido(0.0);
			else if (linha.substring(226, 231).trim().matches(MATCHES))
				p.setPercentualRentabilidadeMinimaDoPedido(Double.parseDouble(linha.substring(226, 231)) / 100);
			else {
				Utils.gravarLog("\nPAR: " + linha);
				return true;
			}
		}

		if (linha.length() > 231) {
			if (linha.substring(231, 236).trim().equals(""))
				p.setPercRentabilidadeMediaDoPedido(0.0);
			else if (linha.substring(231, 236).trim().matches(MATCHES))
				p.setPercRentabilidadeMediaDoPedido(Double.parseDouble(linha.substring(231, 236)) / 100);
			else {
				Utils.gravarLog("\nPAR: " + linha);
				return true;
			}
		}
		if (linha.length() > 236) {
			p.setSupervisor(linha.substring(236, 237).equals("1"));
		}

		if (linha.length() > 237) {
			if (linha.substring(237, 242).trim().equals(""))
				p.setPercentualBonificacao(0.0);
			else if (linha.substring(237, 242).trim().matches(MATCHES))
				p.setPercentualBonificacao(Double.parseDouble(linha.substring(237, 242)) / 100);
			else {
				Utils.gravarLog("\nPAR: " + linha);
				return true;
			}
		}
		if (linha.length() > 242) {
			p.setSolicitarSenhaRentabilidade(linha.substring(242, 243).equals("1"));
		}else{
			p.setSolicitarSenhaRentabilidade(Boolean.FALSE);
		}

		if (linha.length() > 243) {
			p.setVisualizarBotaoContato(linha.substring(243, 244).equals("1"));
		}else{
			p.setVisualizarBotaoContato(Boolean.FALSE);
		}

		p.setDataUltimaRecepcao(new Date());
		p.setDataExpiraLiberacao(new Date());

		p.setDataUltimoAcesso(new Date());

        getAtualizarTokenParamentro(parametroAtual, p);

        if(exists(p))
			update(p);
		else
			insert(p);
		return false;
	}

    private void getAtualizarTokenParamentro(Parametro parametroAtual, Parametro p) {
        Vendedor vendedor = new VendedorParametroDAO(ctx, null).getVendedorRegistrado();
	    if (parametroAtual == null && vendedor != null){
            p.setTokenApi(vendedor.getToken());
            p.setIdVendedorCM(vendedor.getId());
            p.setDataUltimaVerificacaoCM(vendedor.getDataRegistro());
            p.setAtivo(vendedor.isAtivo());
            p.setIdSequencialPedido(Long.parseLong(vendedor.getSequencial()));
        }else {
            p.setTokenApi(parametroAtual == null ? "" : parametroAtual.getToken());
            p.setIdVendedorCM(parametroAtual == null ? 0 : parametroAtual.getIdVendedorCM());
            p.setDataUltimaVerificacaoCM(parametroAtual == null ? null : parametroAtual.getDataUltimaVerificacaoCM());
            p.setAtivo(parametroAtual == null || parametroAtual.isAtivo());
            p.setIdSequencialPedido(parametroAtual == null ? 0 : parametroAtual.getIdSequencialPedido());
        }
    }

    public void importarToken(List<String> linhas) {


		try {

			for(String linha: linhas) {
				if (!linha.substring(8, linha.length()).trim().equals("")) {
					String token = linha.substring(8, linha.length());
					Parametro parametro = get();
					parametro.setTokenApi(token);
					if(exists(parametro))
						update(parametro);
					else
						insert(parametro);

				} else {
					Logs.gravarLog("\nPAR: " + linha);
					continue;
				}

			}}catch (Exception e){
			Logs.gravarLog("\nPAR: " + e.getMessage());
		}
	}

	public static Parametro atualizaRegistroVendedor(Context ctx, Vendedor vendedor) {
		ParametroDAO paDao = new ParametroDAO(ctx, null);
		if (vendedor != null) {
			Parametro v = paDao.get();
			v.setTokenApi(vendedor.getToken());

			v.setDataUltimaVerificacaoCM(new Date());
			v.setAtivo(vendedor.isAtivo());
			v.setIdVendedorCM(vendedor.getId());

			if (vendedor.getSequencial() != null && !vendedor.getSequencial().matches("[A-Z]*")) {
				v.setIdVendedorCM(Long.valueOf(vendedor.getSequencial()));
			} else {
				v.setIdVendedorCM(0L);
			}
			if (paDao.update(v)) {
				return v;
			}
		}
		return null;
	}


	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		Parametro parametroAtual = null;
		try {
			parametroAtual = get();
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		if (getLinhaParametro(parametroAtual, linha)) {
			return true;
		}

		return false;
	}


	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_arquivo_de_parametros);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
