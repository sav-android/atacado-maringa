package br.net.sav.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.RetornoPedido;
import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.repository.PedidoRepository;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.Principal;
import br.net.sav.atacadomaringa.R;
import br.net.sav.central_manager.PedidoConverter;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Condicao;
import br.net.sav.modelo.EmpresaFilial;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Produto;
import br.net.sav.modelo.TipoPedido;
import br.net.sav.mqtt.StatusCentralManager;
import br.net.sav.service.TaskGeneric;
import br.net.sav.util.dao.VendedorParametroDAO;
import br.net.sav.util.dao.HistoricoRetornoPedidoDAO;
import br.net.sav.util.modelo.DataParameterDTO;
import br.net.sav.util.modelo.StatusPedidoGWeb;
import br.net.sav.util.pdf.GeracaoPdfPedido;

import static br.net.sav.modelo.EmpresaFilial.getFilialDiferente;
import static br.net.sav.modelo.Parametro.getlistener;
import static br.net.sav.utils.IntentUtils._startActivitySincronizacao;
import static br.net.sav.utils.ToastUtils.mostrarMensagem;
import static java.lang.String.format;

public class PedidoDAO extends PadraoDAO {
    private static final String TABELA = "Pedido";
    public final short TODOS = 0;
    public static final short NAO_ENVIADOS = 1;
    public static final short ENVIADOS = 2;
    public final short NAO_ENVIADOS_E_ABERTOS = 3;
    public final short ENVIADOS_E_FECHADOS = 4;
    public final short NAO_ENVIADOS_E_FECHADOS = 5;
    public static boolean emUso = false;
    public SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public PedidoDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }


    public boolean update(Pedido pedido, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.update(TABELA, preencherContentValues(pedido), "IdPedido=?", new String[]{String.valueOf(pedido.getIDPedido())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public boolean delete(Pedido pedido, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;
        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.delete(TABELA, "IdPedido=?", new String[]{String.valueOf(pedido.getIDPedido())}) > 0;

        //if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public boolean salvarPedido(Pedido pedido, List<Item> itens, boolean atualizarParametro) {

        boolean retorno = false;
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        dbLocal.beginTransaction();
        try {
            Pedido pedAux = get(pedido.getIDPedido(), dbLocal);

            if (pedAux == null) {
                if (insert(pedido, dbLocal)) {
                    if (new ItemDAO(ctx, dbLocal).insert(itens, dbLocal)) {
                        dbLocal.setTransactionSuccessful();
                        retorno = true;
                    }

                    if (atualizarParametro)
                        atualizarSequencialParametro(pedido, dbLocal);

                }
            } else if (pedido.isHistoricoPedidoCM() && pedAux != null) {

                pedido.setIDPedido(gerarId());
                if (insert(pedido, dbLocal)) {
                    if (new ItemDAO(ctx, dbLocal).insertCM(itens, pedido, dbLocal)) {
                        dbLocal.setTransactionSuccessful();
                        retorno = true;
                    }
                    atualizarSequencialParametro(pedido, dbLocal);

                }


            } else {
                ItemDAO itemDAO = new ItemDAO(ctx, dbLocal);

                if (itemDAO.delete(pedido.getIDPedido(), dbLocal)) {
                    if (update(pedido, dbLocal)) {
                        if (itemDAO.insert(itens, dbLocal)) {
                            dbLocal.setTransactionSuccessful();
                            retorno = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e("PedidoDAO:salvarPedido", e.getMessage());
        } finally {
            dbLocal.endTransaction();
            //if (db == null)
            //dbLocal.close(); singletone();
        }

        return retorno;
    }

    private void atualizarSequencialParametro(Pedido pedido, SQLiteDatabase dbLocal) {
        Parametro parametro = new ParametroDAO(ctx, dbLocal).get();
        parametro.setIdSequencialPedido(pedido.getIDPedido());
        new ParametroDAO(ctx, dbLocal).update(parametro);
    }

    public int gerarTexto(long idVendedor) {
        int retorno = 0;
        List<Pedido> pedidosExportar = listaPedidos(NAO_ENVIADOS_E_FECHADOS);
        if (pedidosExportar.size() == 0)
            return retorno;

        retorno = pedidosExportar.size();

        try {
            ItemDAO itemDAO = new ItemDAO(ctx, null);
            Date data = new Date();

            String nomeArquivoPedido = format("ped%08d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));
            String nomeArquivoItens = format("ite%08d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));

            FileOutputStream outPed = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/envia", nomeArquivoPedido));
            FileOutputStream outIte = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/envia", nomeArquivoItens));

            for (Pedido pedido : pedidosExportar) {

                gerarRetornoPedido(idVendedor, pedido);
                outPed.write(pedido.toLinhaTexto(idVendedor).getBytes());

                ArrayList<Item> itensPedido = itemDAO.getByPedido(pedido.getIDPedido());

                for (Item item : itensPedido) {
                    outIte.write(item.toLinhaTexto(pedido,idVendedor).getBytes());
                }
            }
            outPed.flush();
            outPed.close();

            outIte.flush();
            outIte.close();

        } catch (Exception e) {
            Log.e("GeracaoTextoDAO", e.getMessage());
        }

        return retorno;
    }


    private void gerarRetornoPedido(long idVendedor, Pedido pedido) {
        Cliente cliente = null;
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        try {
            cliente = new ClienteDAO(ctx, db).get(pedido.getIDCliente());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            HistoricoRetornoPedidoDAO.gerarRetornoParaGerenciador(pedido.getIDPedido(), idVendedor, cliente.getCnpjCpf());
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    public void apagarAposPrazo(short quantidadeDiasApagar) {
        SimpleDateFormat sdfDataDb = new SimpleDateFormat(
                "yyyy-MM-dd");
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, quantidadeDiasApagar * -1);
        Date dataApagar = cal.getTime();

        List<Long> listaPedidosApagar = new ArrayList<Long>();

        Cursor c = db.query(TABELA, new String[]{"IdPedido"}, "Enviado=1 and DataPedido<=?", new String[]{sdfDataDb.format(dataApagar) + " 00:00:00"}, null, null, null);

        try {
            while (c.moveToNext())
                listaPedidosApagar.add(c.getLong(0));

            for (Long idPedidoApagar : listaPedidosApagar) {
                if (listaPedidosApagar.size() == 0)
                    break;

                String[] param = new String[]{String.valueOf(idPedidoApagar)};
                if (db.delete(TABELA, "IdPedido=?", param) > 0)
                    db.delete("Item", "IdPedido=?", param);
            }
        } catch (Exception e) {
            Log.e("PedidoDAO:apagarAposPrazo.", e.getMessage());
        } finally {
            //db.close(); singleton();
            c.close();
        }
    }

    public void marcarEnviado(Date dataEnvio) {
        SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        String sql = format("UPDATE Pedido SET Enviado = 1, DataEnvio = \'%s\' WHERE Enviado = 0 and Aberto = 1", sdfDataHoraDb.format(dataEnvio));
        db.execSQL(sql);
        //db.close(); singleton();
    }

    public List<Pedido> listaPedidos(short quais) {
        return listaPedidos(quais, (long) 0);
    }

    public Boolean insert(Pedido pedido, SQLiteDatabase db) {
        SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        ContentValues ctv = new ContentValues();
        ctv.put("IdCliente", pedido.getIDCliente());
        ctv.put("IdPedido", pedido.getIDPedido());
        ctv.put("PedidoCliente", pedido.getPedidoCliente());
        ctv.put("IdTPedido", pedido.getIDTPedido());
        ctv.put("IdTabela", pedido.getIDTabela());
        ctv.put("IdCondicao", pedido.getIDCondicao());
        ctv.put("NrItens", pedido.getNrItens());
        ctv.put("DescontoExtra", pedido.getDescontoExtra());
        ctv.put("ValorUtilizouFlex", pedido.getValorUtilizouFlex());
        ctv.put("ValorGeraraFlex", pedido.getValorGeraFlex());
        ctv.put("TotalGeral", pedido.getTotalGeral());
        ctv.put("TotalLiquido", pedido.getTotalLiquido());
        ctv.put("Observacao", pedido.getObservacao());
        ctv.put("DataPedido", sdfDataHoraDb.format(pedido.getDataPedido()));
        ctv.put("DataFaturamento", sdfDataHoraDb.format(pedido.getDataFaturamento()));
        ctv.put("SugestaoVenda", pedido.isSugestaoVenda());
        ctv.put("DescontoCliente", pedido.getDescontoCliente());
        ctv.put("Enviado", pedido.isEnviado());
        ctv.put("DataEnvio", sdfDataHoraDb.format(pedido.getDataEnvio()));
        ctv.put("IdEmpresa", pedido.getIDEmpresa());
        ctv.put("IdFilial", pedido.getIDFilial());
        ctv.put("IdFormaPagamento", pedido.getIDFormaPagamento());
        ctv.put("Aberto", pedido.isAberto());
        ctv.put("status", pedido.isStatus());
        ctv.put("HistoricoPedidoCM", pedido.isHistoricoPedidoCM());
        ctv.put("Rentabilidade", pedido.getRentabilidade());
        ctv.put("PercRentabilidadeLiberado", pedido.getPercRentabilidadeLiberado());
        ctv.put("SaldoBonificar", pedido.getSaldoBonificar());
        ctv.put("VinculoBonificacao", pedido.getVinculoBonificacao());
        ctv.put("VinculoPedido", pedido.getVinculoPedido());
        ctv.put("StatusPedidoGWeb", pedido.getStatusPedidoGWeb());
        ctv.put("RetornoStatusPedido", pedido.getRetornoStatusPedido());
        ctv.put("RetornoNumeroNotaFiscal", pedido.getRetornoNumeroNotaFiscal());
        ctv.put("CodPedidoGWeb", pedido.getCodPedidoGWeb());
        ctv.put("statusFaturado", pedido.getStatusFaturado());
        ctv.put("DesconsiderarFlex", pedido.isDesconsiderarFlex());


        boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0;

        //if (db == null)
        //dbLocal.close(); singletone();


        return retorno;
    }

    public void confirmarPedido(List<String> linhas) {
        SimpleDateFormat sdfDataDb = new SimpleDateFormat(
                "yyyy-MM-dd");
        for (String linha : linhas) {
            long idPedido = Long.parseLong(linha.substring(0, 6));
            String data = sdfDataDb.format(linha.substring(6, 14));

            StringBuilder sql = new StringBuilder();
            sql.append("update Pedido set DesconsiderarFlex=1");
            sql.append(format(" where IdPedido=%d and DataPedido between \'%s 00:00:00\'  and \'%s 23:59:59 \' ", idPedido, data, data));
            db.execSQL(sql.toString(), null);
        }
    }

    public void atualizaCampo(String campoTabela, long idPedido, boolean status) {
        SQLiteDatabase dbLocal;
        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        int valor = status ? 1 : 0;

        try {
            String[] param = new String[]{String.valueOf(idPedido)};
            StringBuilder sql = new StringBuilder();

            sql.append(format("UPDATE Pedido SET " + campoTabela + "= " + valor + " where IdPedido = ? "));
            dbLocal.execSQL(sql.toString(), param);
            //db.close(); singleton();
        } catch (Exception e) {
            Log.d("PedidoDAO", "atualizaCampo(): " + campoTabela);
        } finally {
        }

    }

    public void atualizaCampoPedidoMQTT(String id_central_manager, long idPedido, boolean statusSav, int statusCM) {
        SQLiteDatabase dbLocal;
        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        int idCM = Integer.parseInt(id_central_manager);
        int valor = statusSav ? 1 : 0;

        try {
            String[] param = new String[]{String.valueOf(idPedido)};
            StringBuilder sql = new StringBuilder();

            sql.append(String.format("UPDATE Pedido SET StatusPedidoGWeb = %d, status = %d, CodPedidoGWeb = %d where IdPedido = ? ", statusCM, valor, idCM));
            dbLocal.execSQL(sql.toString(), param);
            //db.close(); singleton();
        } catch (Exception e) {
            Log.d("PedidoDAO", "atualizaCampo(): " + idPedido);
        } finally {
        }

    }


    public void atualizaCampoData(String campoTabela, long idPedido, Date data) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        try {
            String[] param = new String[]{String.valueOf(idPedido)};
            StringBuilder sql = new StringBuilder();

            sql.append(format("UPDATE Pedido SET " + campoTabela + "=\'%s\' where IdPedido = ? ", sdf1.format(data)));
            db.execSQL(sql.toString(), param);
            //db.close(); singleton();
        } catch (Exception e) {
            Log.d("PedidoDAO", "atualizaCampoData(): " + campoTabela);
        } finally {
        }

    }

    public void atualizaPedidoFilialEmpresa(long idPedido, EmpresaFilial filial){
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        try {
            String[] param = new String[]{String.valueOf(idPedido)};
            StringBuilder sql = new StringBuilder();

            sql.append(format("update Pedido set IdEmpresa = %d , IdFilial = %d WHERE IdPedido = ?",filial.getIdEmpresa(),filial.getIdFilial()));
            db.execSQL(sql.toString(), param);
        } catch (Exception e) {
            Log.d("PedidoDAO", "atualizapedidofilial: " + idPedido);
        } finally {
        }

    }

    private Item buscarDataDoItem(long idPedido, SQLiteDatabase db) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Item item = null;
        Cursor c = null;
        SQLiteDatabase dbLocal;

        StringBuilder sql = new StringBuilder();
        String[] param = new String[]{String.valueOf(idPedido)};
        sql.append("SELECT DISTINCT DataPedido FROM Item WHERE IDPedido = ?");

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(sql.toString(), param);
            while (c.moveToNext()) {
                item = new Item();
                item.setDataPedido(sdf1.parse(c.getString(c.getColumnIndex("DataPedido"))));
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //dbLocal.close(); singleton();
        }

        return item;

    }

    public int buscarSequecial(SQLiteDatabase db) {
        Cursor c = null;
        SQLiteDatabase dbLocal;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT max(idPedido) as idPedido from pedido;");

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                return c.getInt(c.getColumnIndex("idPedido"));
            }
        } catch (Exception e) {
            Log.d("buscaSequencial", e.getMessage());
        } finally {
            if (c != null)
                c.close();
        }

        return 0;

    }

    private void atualizaDatasAlteradas() {
        try {
            List<Pedido> pedidos = pedidosAlterados(null);
            if (!pedidos.isEmpty()) {
                for (Pedido pedido : pedidos) {
                    Item item = buscarDataDoItem(pedido.getIDPedido(), null);
                    if (item != null)
                        if (item.getDataPedido() != pedido.getDataPedido()) {
                            atualizaCampoData("DataPedido", pedido.getIDPedido(), item.getDataPedido());
                        }
                }
            }

        } catch (Exception e) {
            Log.d("PedidoDAO", "atualizaDatas()");
        } finally {
            //db.close(); singleton();
        }

    }

    private boolean existeDataAlterada(SQLiteDatabase db) {
        boolean retorno = false;
        SQLiteDatabase dbLocal = null;

        if (db == null) {
            dbLocal = new DBHelper(ctx).getReadableDatabase();

        } else {
            dbLocal = db;
        }

        try {
            StringBuilder sql = new StringBuilder();

            sql.append(format("select DISTINCT  p.DataPedido from Pedido p inner JOIN Item i on p.IdPedido = i.IdPedido  where p.DataPedido <> i.DataPedido and Enviado=1 and HistoricoPedidoCM =0 ;"));

            Cursor c = dbLocal.rawQuery(sql.toString(), null);

            if (c.moveToFirst()) {
                retorno = c.getInt(0) > 0;
            }
            c.close();
        } catch (Exception e) {
            Log.d("PedidoDAO", "existeDataAlterada()");
        }

        //if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public void verificaDataAlterada() {
        if (existeDataAlterada(null)) {
            atualizaDatasAlteradas();
        }
    }

    private List<Pedido> pedidosAlterados(SQLiteDatabase db) {
        Pedido pedido = null;
        Cursor c = null;
        SQLiteDatabase dbLocal;
        List<Pedido> pedidos = new ArrayList<>();

        StringBuilder sql = new StringBuilder();
        sql.append("select DISTINCT  p.* from Pedido p inner JOIN Item i on p.IDPedido = i.IDPedido  where p.DataPedido <> i.DataPedido and Enviado=1 and HistoricoPedidoCM =0 ;");

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                pedido = new Pedido();
                objetoPedido(c, pedido);
                pedidos.add(pedido);
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //dbLocal.close(); singleton();
        }

        return pedidos;

    }

    private List<Pedido> pedidosComFiliasDiferentes(SQLiteDatabase db) {
        Pedido pedido = null;
        Cursor c = null;
        SQLiteDatabase dbLocal;
        List<Pedido> pedidos = new ArrayList<>();

        StringBuilder sql = new StringBuilder();
        sql.append("select * from Pedido p, EmpresaFilial e where e.IdFilial <> p.IdFilial ");

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                pedido = new Pedido();
                objetoPedido(c, pedido);
                pedidos.add(pedido);
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //dbLocal.close(); singleton();
        }

        return pedidos;

    }

    public long gerarId() {
        SQLiteDatabase dbLocal = null;


        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        long id = 0;

        Cursor c = null;

        try {
            Parametro parametro = new ParametroDAO(ctx, dbLocal).get();

             c = dbLocal.rawQuery("SELECT MAX(IdPedido) as IdPedido FROM Pedido", null);


            if (c.moveToFirst()) {
                id = c.getLong(0);
                if (parametro.getIdSequencialPedido() > id) {
                    id = parametro.getIdSequencialPedido();
                }
                id = c.getLong(0) + 1;
            }else {
                id = 1;
            }
        } catch (Exception e) {
            Log.e("PedidoDAO:gerarId.", e.getMessage());
        } finally {
            c.close();
            //if (db == null)
            //dbLocal.close(); singletone();
        }

        return id;
    }

    public Pedido get(long idPedido, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        Pedido pedido = null;

        Cursor c = null;

        try {
            c = dbLocal.query(TABELA, colunas, "IdPedido=?", new String[]{String.valueOf(idPedido)}, null, null, null);

            if (c.moveToFirst()) {
                pedido = preencherObjeto(c);
            }

        } catch (Exception e) {
            Log.e("PedidoDAO:get(idPedido).", e.getMessage());
        } finally {
            c.close();
            //if (db == null)
            //dbLocal.close(); singletone();
        }

        return pedido;
    }

    public boolean excluirPedido(Pedido pedido) {
        boolean retorno = false;
        SQLiteDatabase dbLocal;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        dbLocal.beginTransaction();

        try {
            if (delete(pedido, dbLocal)) {
                if (new ItemDAO(ctx, dbLocal).delete(pedido.getIDPedido(), dbLocal)) {
                    dbLocal.setTransactionSuccessful();
                    retorno = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbLocal.endTransaction();
        }

        //  if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public long quantidadePedidos() {
        SQLiteDatabase dbLocal;
        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        Cursor c = dbLocal.rawQuery("select sum(IdPedido) as QtdePedidos from Pedido where Enviado = 0", null);
        long qtd = 0;

        if (c.moveToFirst())
            qtd = c.getLong(c.getColumnIndex("QtdePedidos"));

        //if (db == null)
        //dbLocal.close(); singleton();

        c.close();
        return qtd;
    }

    public ArrayList<Pedido> listaPedidosSemBonificacao(short quais, Pedido pedidoCliente) {
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();

        StringBuilder sql = new StringBuilder();
        sql.append("Select p.*, c.Razao, tpe.Descricao as TipoPedido, tpe.TipoOperacao as Tipo from Pedido p JOIN Cliente c, TipoPedido tpe ON (c.IdCliente = p.IdCliente AND p.IdTPedido = tpe.IdTipoPedido) ");
        if (quais == ENVIADOS)
            sql.append("where p.Enviado = 1 AND p.VinculoBonificacao = 0 ");
        else if (quais == NAO_ENVIADOS)
            sql.append("where p.Enviado = 0 AND p.VinculoBonificacao = 0 ");
        if (pedidoCliente.getIDPedido() != 0) {
            if (sql.toString().contains("where"))
                sql.append(format("AND p.IDCliente= %d ", pedidoCliente.getIDCliente()));
            else
                sql.append(format("WHERE p.IDCliente= %d ", pedidoCliente.getIDCliente()));
        }
        sql.append("Order By p.DataPedido desc");

        Cursor c = null;

        try {
            c = db.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                Pedido pedido = new Pedido();
                objetoPedido(c, pedido);
                if (pedido.getVinculoPedido() == 0 && pedido.getIDPedido() != pedidoCliente.getIDPedido())
                    pedidos.add(pedido);
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //db.close(); singleton();
        }

        return pedidos;
    }

    public ArrayList<Pedido> listaPedidosPorData(short quais, long idCliente, Date pDataInicial, Date pDataFinal) {
        SimpleDateFormat sdfDataDb = new SimpleDateFormat(
                "yyyy-MM-dd");
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();

        StringBuilder sql = new StringBuilder();
        sql.append("Select p.*, c.Razao, tpe.Descricao as TipoPedido, tpe.TipoOperacao as Tipo from Pedido p JOIN Cliente c, TipoPedido tpe ON (c.IdCliente = p.IdCliente AND p.IdTPedido = tpe.IdTipoPedido) ");
        if (quais == ENVIADOS)
            sql.append("where p.Enviado = 1 ");
        else if (quais == NAO_ENVIADOS)
            sql.append("where p.Enviado = 0 ");
        else if (quais == NAO_ENVIADOS_E_ABERTOS)
            sql.append("where p.Enviado = 0 and p.Aberto = 0 ");
        else if (quais == NAO_ENVIADOS_E_FECHADOS)
            sql.append("where p.Enviado = 0 and p.Aberto = 1 ");
        else if (quais == ENVIADOS_E_FECHADOS)
            sql.append("where p.Enviado = 1 and p.Aberto = 1 ");
        if (idCliente != 0) {
            if (sql.toString().contains("where"))
                sql.append(format("AND p.IDCliente= %d ", idCliente));
            else
                sql.append(format("WHERE p.IDCliente= %d ", idCliente));
        }
        sql.append(format(" AND (p.DataPedido BETWEEN '%s 00:00:00' AND '%s 23:59:59')", sdfDataDb.format(pDataInicial), sdfDataDb.format(pDataFinal)));
        sql.append("Order By p.IdPedido desc");

        Cursor c = null;

        try {
            c = db.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                Pedido pedido = new Pedido();
                setObjetoPedido(c, pedido);
                pedidos.add(pedido);
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //db.close(); singleton();
        }

        return pedidos;
    }

    public ArrayList<Pedido> listaPedidos(short quais, long idCliente) {
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();

        StringBuilder sql = new StringBuilder();
        sql.append("Select p.*, c.Razao, tpe.Descricao as TipoPedido, tpe.TipoOperacao as Tipo from Pedido p JOIN Cliente c, TipoPedido tpe ON (c.IdCliente = p.IdCliente AND p.IdTPedido = tpe.IdTipoPedido) ");
        if (quais == ENVIADOS)
            sql.append("where p.Enviado = 1 ");
        else if (quais == NAO_ENVIADOS)
            sql.append("where p.Enviado = 0 ");
        else if (quais == NAO_ENVIADOS_E_ABERTOS)
            sql.append("where p.Enviado = 0 and p.Aberto = 0 ");
        else if (quais == NAO_ENVIADOS_E_FECHADOS)
            sql.append("where p.Enviado = 0 and p.Aberto = 1 ");
        else if (quais == ENVIADOS_E_FECHADOS)
            sql.append("where p.Enviado = 1 and p.Aberto = 1 ");
        if (idCliente != 0) {
            if (sql.toString().contains("where"))
                sql.append(format("AND p.IDCliente= %d ", idCliente));
            else
                sql.append(format("WHERE p.IDCliente= %d ", idCliente));
        }
        sql.append("Order By p.DataPedido desc");

        Cursor c = null;

        try {
            c = db.rawQuery(sql.toString(), null);
            while (c.moveToNext()) {
                Pedido pedido = new Pedido();
                setObjetoPedido(c, pedido);
                pedidos.add(pedido);
            }
        } catch (Exception e) {
            Log.d("listaPedidos", e.getMessage());
        } finally {
            if (c != null)
                c.close();
            //db.close(); singleton();
        }

        return pedidos;
    }

    private void setObjetoPedido(Cursor c, Pedido pedido) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        pedido.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
        pedido.setIDCliente(c.getLong(c.getColumnIndex("IdCliente")));
        pedido.setPedidoCliente(c.getString(c.getColumnIndex("PedidoCliente")));
        pedido.setIDTPedido(c.getShort(c.getColumnIndex("IdTPedido")));
        pedido.setIDTabela(c.getShort(c.getColumnIndex("IdTabela")));
        pedido.setIDCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
        pedido.setNrItens(c.getShort(c.getColumnIndex("NrItens")));
        pedido.setDescontoExtra(c.getDouble(c.getColumnIndex("DescontoExtra")));
        pedido.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
        pedido.setValorGeraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
        pedido.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
        pedido.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
        pedido.setObservacao(c.getString(c.getColumnIndex("Observacao")));
        pedido.setDataPedido(sdf1.parse(c.getString(c.getColumnIndex("DataPedido"))));
        pedido.setDataFaturamento(sdf1.parse(c.getString(c.getColumnIndex("DataFaturamento"))));
        pedido.setSugestaoVenda(c.getInt(c.getColumnIndex("SugestaoVenda")) > 0);
        pedido.setDescontoCliente(c.getDouble(c.getColumnIndex("DescontoCliente")));
        pedido.setDesconsiderarFlex(c.getInt(c.getColumnIndex("DesconsiderarFlex")) > 0);
        pedido.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
        pedido.setDataEnvio(sdf1.parse(c.getString(c.getColumnIndex("DataEnvio"))));
        pedido.setNomeCliente(c.getString(c.getColumnIndex("Razao")));
        pedido.setTipoPedido(c.getString(c.getColumnIndex("TipoPedido")));
        pedido.setIDEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
        pedido.setIDFilial(c.getShort(c.getColumnIndex("IdFilial")));
        pedido.setIDFormaPagamento(c.getLong(c.getColumnIndex("IdFormaPagamento")));
        pedido.setAberto(c.getInt(c.getColumnIndex("Aberto")) > 0);
        pedido.setStatus(c.getInt(c.getColumnIndex("status")) > 0);
        pedido.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
        pedido.setPercRentabilidadeLiberado(c.getDouble(c.getColumnIndex("PercRentabilidadeLiberado")));
        pedido.setSaldoBonificar(c.getDouble(c.getColumnIndex("SaldoBonificar")));
        pedido.setVinculoBonificacao(c.getInt(c.getColumnIndex("VinculoBonificacao")));
        pedido.setVinculoPedido(c.getInt(c.getColumnIndex("VinculoPedido")));
        pedido.setRetornoStatusPedido(c.getString(c.getColumnIndex("RetornoStatusPedido")));
        pedido.setRetornoNumeroNotaFiscal(c.getString(c.getColumnIndex("RetornoNumeroNotaFiscal")));
        pedido.setHistoricoPedidoCM(c.getInt(c.getColumnIndex("HistoricoPedidoCM")) > 0);
        pedido.setTotalFaturado(c.getDouble(c.getColumnIndex("TotalFaturado")));
        pedido.setStatusFaturado(c.getString(c.getColumnIndex("statusFaturado")));

    }

    private void objetoPedido(Cursor c, Pedido pedido) throws ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        pedido.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
        pedido.setIDCliente(c.getLong(c.getColumnIndex("IdCliente")));
        pedido.setPedidoCliente(c.getString(c.getColumnIndex("PedidoCliente")));
        pedido.setIDTPedido(c.getShort(c.getColumnIndex("IdTPedido")));
        pedido.setIDTabela(c.getShort(c.getColumnIndex("IdTabela")));
        pedido.setIDCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
        pedido.setNrItens(c.getShort(c.getColumnIndex("NrItens")));
        pedido.setDescontoExtra(c.getDouble(c.getColumnIndex("DescontoExtra")));
        pedido.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
        pedido.setValorGeraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
        pedido.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
        pedido.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
        pedido.setObservacao(c.getString(c.getColumnIndex("Observacao")));
        pedido.setDataPedido(sdf1.parse(c.getString(c.getColumnIndex("DataPedido"))));
        pedido.setDataFaturamento(sdf1.parse(c.getString(c.getColumnIndex("DataFaturamento"))));
        pedido.setSugestaoVenda(c.getInt(c.getColumnIndex("SugestaoVenda")) > 0);
        pedido.setDescontoCliente(c.getDouble(c.getColumnIndex("DescontoCliente")));
        pedido.setDesconsiderarFlex(c.getInt(c.getColumnIndex("DesconsiderarFlex")) > 0);
        pedido.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
        pedido.setDataEnvio(sdf1.parse(c.getString(c.getColumnIndex("DataEnvio"))));
        pedido.setIDEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
        pedido.setIDFilial(c.getShort(c.getColumnIndex("IdFilial")));
        pedido.setIDFormaPagamento(c.getLong(c.getColumnIndex("IdFormaPagamento")));
        pedido.setAberto(c.getInt(c.getColumnIndex("Aberto")) > 0);
        pedido.setStatus(c.getInt(c.getColumnIndex("status")) > 0);
        pedido.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
        pedido.setPercRentabilidadeLiberado(c.getDouble(c.getColumnIndex("PercRentabilidadeLiberado")));
        pedido.setSaldoBonificar(c.getDouble(c.getColumnIndex("SaldoBonificar")));
        pedido.setVinculoBonificacao(c.getInt(c.getColumnIndex("VinculoBonificacao")));
        pedido.setVinculoPedido(c.getInt(c.getColumnIndex("VinculoPedido")));
        pedido.setRetornoStatusPedido(c.getString(c.getColumnIndex("RetornoStatusPedido")));
        pedido.setRetornoNumeroNotaFiscal(c.getString(c.getColumnIndex("RetornoNumeroNotaFiscal")));
        pedido.setHistoricoPedidoCM(c.getInt(c.getColumnIndex("HistoricoPedidoCM")) > 0);
        pedido.setTotalFaturado(c.getDouble(c.getColumnIndex("TotalFaturado")));
        pedido.setStatusFaturado(c.getString(c.getColumnIndex("statusFaturado")));
    }

    public List<String> listaDatasEnvio() {
        List<String> retorno = null;
        try {
            SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
            Cursor c = db.query(true, TABELA, new String[]{"DataEnvio"}, "Enviado=?", new String[]{String.valueOf(1)}, null, null, "DataEnvio desc", null);
            retorno = new ArrayList<String>(c.getCount());

            while (c.moveToNext()) {
                retorno.add(c.getString(0));
            }
            c.close();
            //db.close(); singleton();

        } catch (Exception e) {
            Log.d("listaDatasEnvio", e.getMessage());
        }

        return retorno;
    }

    public void marcarRetransmitir(List<Date> listaDatas) {
        SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

        ContentValues ctv = new ContentValues();
        ctv.put("Enviado", false);
        ctv.put("Aberto", false);

        for (Date date : listaDatas)
            db.update(TABELA, ctv, "DataEnvio=?", new String[]{sdfDataHoraDb.format(date)});

        //db.close(); singleton();
    }

    public Pedido preencherObjeto(Cursor c) throws ParseException {
        SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Pedido pedido = new Pedido();
        pedido.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
        pedido.setIDCliente(c.getLong(c.getColumnIndex("IdCliente")));
        pedido.setPedidoCliente(c.getString(c.getColumnIndex("PedidoCliente")));
        pedido.setIDTPedido(c.getShort(c.getColumnIndex("IdTPedido")));
        pedido.setIDTabela(c.getShort(c.getColumnIndex("IdTabela")));
        pedido.setIDCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
        pedido.setNrItens(c.getShort(c.getColumnIndex("NrItens")));
        pedido.setDescontoExtra(c.getDouble(c.getColumnIndex("DescontoExtra")));
        pedido.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
        pedido.setValorGeraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
        pedido.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
        pedido.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
        pedido.setObservacao(c.getString(c.getColumnIndex("Observacao")));
        pedido.setDataPedido(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataPedido"))));
        pedido.setDataFaturamento(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataFaturamento"))));
        pedido.setSugestaoVenda(c.getInt(c.getColumnIndex("SugestaoVenda")) > 0);
        pedido.setDescontoCliente(c.getDouble(c.getColumnIndex("DescontoCliente")));
        pedido.setDesconsiderarFlex(c.getInt(c.getColumnIndex("DesconsiderarFlex")) > 0);
        pedido.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
        pedido.setDataEnvio(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataEnvio"))));
        pedido.setIDEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
        pedido.setIDFilial(c.getShort(c.getColumnIndex("IdFilial")));
        pedido.setIDFormaPagamento(c.getLong(c.getColumnIndex("IdFormaPagamento")));
        pedido.setAberto(c.getInt(c.getColumnIndex("Aberto")) > 0);
        pedido.setStatus(c.getInt(c.getColumnIndex("status")) > 0);
        pedido.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
        pedido.setPercRentabilidadeLiberado(c.getDouble(c.getColumnIndex("PercRentabilidadeLiberado")));
        pedido.setSaldoBonificar(c.getDouble(c.getColumnIndex("SaldoBonificar")));
        pedido.setVinculoBonificacao(c.getInt(c.getColumnIndex("VinculoBonificacao")));
        pedido.setVinculoPedido(c.getInt(c.getColumnIndex("VinculoPedido")));
        pedido.setRetornoStatusPedido(c.getString(c.getColumnIndex("RetornoStatusPedido")));
        pedido.setRetornoNumeroNotaFiscal(c.getString(c.getColumnIndex("RetornoNumeroNotaFiscal")));
        pedido.setHistoricoPedidoCM(c.getInt(c.getColumnIndex("HistoricoPedidoCM")) > 0);
        pedido.setTotalFaturado(c.getDouble(c.getColumnIndex("TotalFaturado")));
        pedido.setStatusFaturado(c.getString(c.getColumnIndex("statusFaturado")));

        return pedido;
    }

    public Pedido preencherObjetoGweb(Cursor c) throws ParseException {
        SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Pedido pedido = new Pedido();
        pedido.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
        pedido.setIDCliente(c.getLong(c.getColumnIndex("IdCliente")));
        pedido.setPedidoCliente(c.getString(c.getColumnIndex("PedidoCliente")));
        pedido.setIDTPedido(c.getShort(c.getColumnIndex("IdTPedido")));
        pedido.setIDTabela(c.getShort(c.getColumnIndex("IdTabela")));
        pedido.setIDCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
        pedido.setNrItens(c.getShort(c.getColumnIndex("NrItens")));
        pedido.setDescontoExtra(c.getDouble(c.getColumnIndex("DescontoExtra")));
        pedido.setValorUtilizouFlex(c.getDouble(c.getColumnIndex("ValorUtilizouFlex")));
        pedido.setValorGeraFlex(c.getDouble(c.getColumnIndex("ValorGeraraFlex")));
        pedido.setTotalGeral(c.getDouble(c.getColumnIndex("TotalGeral")));
        pedido.setTotalLiquido(c.getDouble(c.getColumnIndex("TotalLiquido")));
        pedido.setObservacao(c.getString(c.getColumnIndex("Observacao")));
        pedido.setDataPedido(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataPedido"))));
        pedido.setDataFaturamento(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataFaturamento"))));
        pedido.setSugestaoVenda(c.getInt(c.getColumnIndex("SugestaoVenda")) > 0);
        pedido.setDescontoCliente(c.getDouble(c.getColumnIndex("DescontoCliente")));
        pedido.setDesconsiderarFlex(c.getInt(c.getColumnIndex("DesconsiderarFlex")) > 0);
        pedido.setEnviado(c.getInt(c.getColumnIndex("Enviado")) > 0);
        pedido.setDataEnvio(sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataEnvio"))));
        pedido.setIDEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
        pedido.setIDFilial(c.getShort(c.getColumnIndex("IdFilial")));
        pedido.setIDFormaPagamento(c.getLong(c.getColumnIndex("IdFormaPagamento")));
        pedido.setAberto(c.getInt(c.getColumnIndex("Aberto")) > 0);
        pedido.setStatus(c.getInt(c.getColumnIndex("status")) > 0);
        pedido.setRentabilidade(c.getDouble(c.getColumnIndex("Rentabilidade")));
        pedido.setPercRentabilidadeLiberado(c.getDouble(c.getColumnIndex("PercRentabilidadeLiberado")));
        pedido.setSaldoBonificar(c.getDouble(c.getColumnIndex("SaldoBonificar")));
        pedido.setVinculoBonificacao(c.getInt(c.getColumnIndex("VinculoBonificacao")));
        pedido.setVinculoPedido(c.getInt(c.getColumnIndex("VinculoPedido")));
        pedido.setCodPedidoGWeb(c.getInt(c.getColumnIndex("CodPedidoGWeb")));
        pedido.setStatusPedidoGWeb(c.getInt(c.getColumnIndex("StatusPedidoGWeb")));
        pedido.setRetornoStatusPedido(c.getString(c.getColumnIndex("RetornoStatusPedido")));
        pedido.setRetornoNumeroNotaFiscal(c.getString(c.getColumnIndex("RetornoNumeroNotaFiscal")));
        pedido.setHistoricoPedidoCM(c.getInt(c.getColumnIndex("HistoricoPedidoCM")) > 0);
        pedido.setTotalFaturado(c.getDouble(c.getColumnIndex("TotalFaturado")));
        pedido.setStatusFaturado(c.getString(c.getColumnIndex("statusFaturado")));

        return pedido;
    }

    public ContentValues preencherContentValues(Pedido pedido) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ContentValues ctv = new ContentValues();
        ctv.put("IdPedido", pedido.getIDPedido());
        ctv.put("IdCliente", pedido.getIDCliente());
        ctv.put("PedidoCliente", pedido.getPedidoCliente());
        ctv.put("IdTPedido", pedido.getIDTPedido());
        ctv.put("IdTabela", pedido.getIDTabela());
        ctv.put("IdCondicao", pedido.getIDCondicao());
        ctv.put("NrItens", pedido.getNrItens());
        ctv.put("DescontoExtra", pedido.getDescontoExtra());
        ctv.put("ValorUtilizouFlex", pedido.getValorUtilizouFlex());
        ctv.put("ValorGeraraFlex", pedido.getValorGeraFlex());
        ctv.put("TotalGeral", pedido.getTotalGeral());
        ctv.put("TotalLiquido", pedido.getTotalLiquido());
        ctv.put("Observacao", pedido.getObservacao());
        ctv.put("DataPedido", sdf1.format(pedido.getDataPedido()));
        ctv.put("DataFaturamento", sdf1.format(pedido.getDataFaturamento()));
        ctv.put("SugestaoVenda", pedido.isSugestaoVenda());
        ctv.put("DescontoCliente", pedido.getDescontoCliente());
        ctv.put("DesconsiderarFlex", pedido.isDesconsiderarFlex());
        ctv.put("Enviado", pedido.isEnviado());
        ctv.put("DataEnvio", sdf1.format(pedido.getDataEnvio()));
        ctv.put("IdEmpresa", pedido.getIDEmpresa());
        ctv.put("IdFilial", pedido.getIDFilial());
        ctv.put("IdFormaPagamento", pedido.getIDFormaPagamento());
        ctv.put("Aberto", pedido.isAberto());
        ctv.put("status", pedido.isStatus());
        ctv.put("SaldoBonificar", pedido.getSaldoBonificar());
        ctv.put("VinculoBonificacao", pedido.getVinculoBonificacao());
        ctv.put("VinculoPedido", pedido.getVinculoPedido());
        ctv.put("Rentabilidade", pedido.getRentabilidade());
        ctv.put("StatusPedidoGWeb", pedido.getStatusPedidoGWeb());
        ctv.put("CodPedidoGWeb", pedido.getCodPedidoGWeb());
        ctv.put("HistoricoPedidoCM", pedido.isHistoricoPedidoCM());
        ctv.put("statusFaturado", pedido.getStatusFaturado());
        ctv.put("DesconsiderarFlex", pedido.isDesconsiderarFlex());


        return ctv;
    }

    public void alterarPercDeRentabilidadeLiberadoDoPedido(Pedido pedido, String pecRetLiberado) {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        String sql = format("UPDATE Pedido SET PercRentabilidadeLiberado = \'%s\' WHERE IDPedido = %d", pecRetLiberado, pedido.getIDPedido());
        db.execSQL(sql);
        //db.close(); singleton();
    }

    public Pedido ultimoPedido() {
        Pedido pedido = null;
        Cursor c = null;
        SQLiteDatabase dbLocal;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT max(DataPedido) as UltimoPedido, IDPedido as ID from Pedido;");

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            if (!listaPedidos(NAO_ENVIADOS).isEmpty() || !listaPedidos(ENVIADOS).isEmpty()) {

                c = dbLocal.rawQuery(sql.toString(), null);

                if (c.moveToFirst()) {
                    pedido = new Pedido();
                    pedido.setDataPedido(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("UltimoPedido"))));
                    pedido.setIDPedido(c.getLong(c.getColumnIndex("ID")));
                }
            }

        } catch (Exception e) {
            Log.e(getClass().getSimpleName() + "get()", e.getMessage());
        } finally {
            // if (db == null)
            //dbLocal.close(); singletone();
        }
        return pedido;

    }

    public Pedido getPedidoVinculado(Pedido obj) {
        Pedido pedido = new Pedido();
        Cursor c = null;
        SQLiteDatabase dbLocal;

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(format("SELECT * FROM %s p WHERE p.%s = ?", TABELA, "VinculoPedido"),
                    new String[]{String.valueOf(obj.getIDPedido())});

            if (c.moveToFirst()) {
                pedido = preencherObjeto(c);
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName() + "get()", e.getMessage());
        } finally {
            //if (db == null)
            //dbLocal.close(); singletone();
        }
        return pedido;
    }

    public Pedido getBuscarVendaDaBonificacao(Pedido obj) {
        Pedido pedido = new Pedido();
        Cursor c = null;
        SQLiteDatabase dbLocal;

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        try {
            c = dbLocal.rawQuery(format("SELECT * FROM %s p WHERE p.%s = ? ", TABELA, "IdPedido"),
                    new String[]{String.valueOf(obj.getVinculoPedido())});

            if (c.moveToFirst()) {
                pedido = preencherObjeto(c);
            }
        } catch (Exception e) {
            Log.e(getClass().getSimpleName() + "get()", e.getMessage());
        } finally {
            //if (db == null)
            //dbLocal.close(); singletone();
        }
        return pedido;
    }

    public ArrayList<String> getLinhasPdfPedido(Pedido pedido, List<Item> itens) {
        ArrayList<String> linhas = new ArrayList<String>();
        SQLiteDatabase dbLocal = (db == null) ? new DBHelper(ctx).getReadableDatabase() : db;
        try {
            Double valorTotal = 0d;

            // Buscar parametro
            Parametro parametro = new ParametroDAO(ctx, dbLocal).get();
            // Buscar cliente
            Cliente cliente = new ClienteDAO(ctx, dbLocal).get(pedido.getIDCliente());
            // Buscar tipo de pedido
            TipoPedido t = new TipoPedidoDAO(ctx, dbLocal).get(pedido.getIDTPedido());
            // Buscar condi��o
            Condicao c = new CondicaoDAO(ctx, dbLocal).get(pedido.getIDCondicao());
            // Buscar forma de pagamento
            FormaPagamento f = new FormaPagamentoDAO(ctx, dbLocal).get(pedido.getIDFormaPagamento());

            // Primeira linha � o t�tulo do pdf
            linhas.add("Pedido " + pedido.getIDPedido());

            linhas.add(format("\nCLIENTE.......: %d - %s", cliente.getIdCliente(), cliente.getRazao()));
            linhas.add(ctx.getString(R.string.ENDERECOS) + ": " + cliente.getEndereco() + ", " + cliente.getBairro());
            linhas.add(format("CIDADE........: %1$s - %2$-20s CEP:%3$s ", cliente.getCidade(), cliente.getUF(), Utils.formataCep(cliente.getCEP())));
            linhas.add("DOCUMENTO " + Utils.formataCnpjCpf(cliente.getCnpjCpf(), true));
            linhas.add(linhaGrid(127));
            linhas.add(format("%-8s %-62s %14s %10s %8s  %10s %9s", "CODIGO", ctx.getString(R.string.descricao), "CODIGO BARRA", " COD. NCM", "QTD", "VALOR UNT.", "TOTAL"));
            linhas.add(linhaGrid(127));
            for (Item item : itens) {
                String descricaoMarcaTMP = new MarcaDAO(ctx, db).getDescricaoMarcaByIdProduto(item.getIDProduto());
                Produto produto = new ProdutoDAO(ctx, db).get(item.getIDProduto());
                validarItem(item);

                String idProduto = String.valueOf(item.getIDProduto()).length() > 8 ? String.valueOf(item.getIDProduto()).substring(0, 8) : String.valueOf(item.getIDProduto());
                String descricaoProduto = produto.getDescricao() != null ? produto.getDescricao().length() > 62 ? produto.getDescricao().substring(0, 62) : produto.getDescricao() : item.getDescricao();
                //  String descricaoMarca = descricaoMarcaTMP != null? descricaoMarcaTMP.length() > 12 ? descricaoMarcaTMP.substring(0, 12) : descricaoMarcaTMP: "Sem Marca";

                StringBuilder sb = new StringBuilder();
                sb.append(format("%-8s|", idProduto));
                sb.append(format("%-62s|", descricaoProduto));
                sb.append(format("%14s|", produto.getCodigoBarra() != null ? produto.getCodigoBarra() : item.getCodigoBarra()));
                sb.append(format("%10s|", produto.getCodigoNCM() != null ? produto.getCodigoNCM() : item.getCodigoNCM()));
                sb.append(format("%8s|", String.valueOf(item.getQuantidade())));
                sb.append(format("%10s|", format("%.2f", item.getValorUnitPraticado())));
                sb.append(format("%10s", format("%.2f", item.getTotalLiquido())));

                linhas.add(sb.toString());

                valorTotal += item.getTotalLiquido();
            }
            linhas.add(linhaGrid(127));

            linhas.add(format("%128s", "VALOR TOTAL: " + format("%.2f", valorTotal)));

            linhas.add(5, "DATA PEDIDO...: " + Utils.sdfDataPtBr.format(pedido.getDataPedido()) + "\n" +
                    ctx.getString(R.string.condicao_pag) + ": " + c + "\n" +
                    "FORMA DE PGTO.: " + f + "\n" +
                    "TIPO DE PEDIDO: " + t + "\n" +
                    "VENDEDOR......: " + parametro);

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        } finally {
            if (db == null) {
                //db.close(); singletone();
            }
        }
        return linhas;
    }

    private void validarItem(Item item) {
        if (item.getDescricao() == null)
            item.setDescricao(ctx.getString(R.string.descrisao_nao_encontrada));
        if (item.getCodigoBarra() == null)
            item.setCodigoBarra("0");
        if (item.getCodigoNCM() == null)
            item.setCodigoNCM("0");
    }

    public String linhaGrid(int cont) {
        String valorOriginal = "-";
        for (int i = 0; i < cont; i++) {
            valorOriginal += "-";
        }
        return valorOriginal;
    }

    public String gerarPdf(ArrayList<String> linhas, long idProduto) {
        String nomeArquivo = "";
        Font.FontFamily fontfamily = Font.FontFamily.COURIER;
        Document documento = new Document(PageSize.A4.rotate(), 35, 35, 35, 35);
        try {
            String titulo = linhas.get(0);
            nomeArquivo = new GeracaoPdfPedido().gerar(linhas, idProduto);

            FileOutputStream saida = new FileOutputStream(nomeArquivo);
            PdfWriter.getInstance(documento, saida);
            documento.open();

            Bitmap bitmap2 = BitmapFactory.decodeResource(ctx.getResources(),
                    R.drawable.logo);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Bitmap bitmapBg = Bitmap.createBitmap(bitmap2.getWidth() * 2, bitmap2.getHeight() * 2, bitmap2.getConfig());
            Canvas canvas = new Canvas(bitmapBg);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap2, 0, 0, null);

            Image image = Image.getInstance(stream.toByteArray());
            image.scaleAbsolute(125, 80);
            documento.add(image);

            Font font = new Font(fontfamily);
            font.setStyle(Font.BOLD);
            font.setSize(16);
            titulo += "\n";
            Paragraph p = new Paragraph(titulo, font);
            documento.add(p);

            Font font2 = new Font(fontfamily);

            font2.setSize(10);
            for (String linha : linhas) {
                linha += "\n";
                if (linha.trim().startsWith("VALOR") || linha.startsWith("COD")) {
                    font2.setStyle(Font.BOLD);
                } else {
                    font2.setStyle(Font.NORMAL);
                }
                p = new Paragraph(linha, font2);
                documento.add(p);

            }

            documento.close();
            try {
                saida.close();
            } catch (IOException e) {
                Log.e("erro pdfRodape", e.getMessage());
            }

        } catch (FileNotFoundException e) {
            Log.e("GeradorPdf", e.getMessage());
        } catch (DocumentException e) {
            Log.e("GeradorPdf", e.getMessage());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nomeArquivo;

    }


    public void confirmarPedidoRetornado(List<String> linhas) {

        for (String linha : linhas) {
            long idPedido = Long.parseLong(linha.substring(0, 8));
//            String data = Utils.sdfDataDb.format(linha.substring(6, 14));

            try {
                Pedido pedido = get(idPedido, null);

                if (pedido != null) {
                    atualizaCampo("status", pedido.IDPedido, true);
                }
            } catch (Exception e) {
                Log.d("Retorno G:", "Retornado pedido gerenciador: " + e.getStackTrace());
            }
        }
    }

    public ArrayList<Pedido> listaPedidosNaoEnviadosGWEB() {
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
        StringBuilder sql = new StringBuilder();
        int quantdade = 0;

        sql.append("SELECT * FROM " + TABELA + " ");
        sql.append(" WHERE StatusPedidoGWeb = " + StatusCentralManager.PENDENTE.getCodigo());
        sql.append(" AND aberto = 1 OR StatusPedidoGWeb = " + StatusCentralManager.ERROR.getCodigo()+" AND aberto = 1");
        sql.append(" AND aberto = 1 OR StatusPedidoGWeb is NULL AND aberto = 1 ");
        sql.append(" ORDER BY DataPedido DESC ");

        try {
            Cursor c = db.rawQuery(sql.toString(), null);

            while (c.moveToNext()) {
                pedidos.add(preencherObjetoGweb(c));
            }

            c.close();
        } catch (Exception e) {
            Log.e(getClass().getName() + ".listaPedidosNaoEnviadosGWEB()", e.getMessage());
        } finally {
            //db.close(); singleton();
        }

        return pedidos;
    }

    public void atualizaPedidosGweb(List<RetornoPedido> resultado) {
        Pedido pedido;

        for (RetornoPedido retorno : resultado) {
            try {
                pedido = this.get(Long.valueOf(retorno.getId()), null);

                pedido.setStatusPedidoGWeb(StatusPedidoGWeb.getById(retorno.getStatus_code()));
                pedido.setCodPedidoGWeb(retorno.getId_vinculado());
                pedido.setStatus(true);


                Log.d("PEDIDO_RETORNO: ", retorno.toString());

                if (this.update(pedido, null)) {
                    Log.d("Status pedido n ", String.valueOf(pedido.getIDPedido()));
                }
            } catch (Exception e) {
                Log.d("Status pedido n ", e.getMessage());
            }
        }
    }


    public int contarPedidosEnviarCentralManager() {

        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT count(*) as total FROM " + TABELA + " ");
        sql.append(" WHERE StatusPedidoGWeb = " + StatusPedidoGWeb.NAOENVIADO.getId());
        sql.append(" AND aberto = 1 OR StatusPedidoGWeb is NULL AND aberto = 1 ");
        sql.append(" ORDER BY DataPedido DESC");

        try {
            Cursor c = db.rawQuery(sql.toString(), null);

            if (c.moveToFirst()) {
                return c.getInt(c.getColumnIndex("total"));
            }

            c.close();
        } catch (Exception e) {
            Log.e(getClass().getName() + ".listaPedidosNaoEnviadosGWEB()", e.getMessage());
        } finally {
            //db.close(); singleton();
        }

        return 0;

    }

    public void insereHistoricoDePedidos(List<br.net.sav.IntegradorWeb.dto.Pedido> resultado) {
        //   usarBanco();
        List<Pedido> pedidos = new PedidoConverter(ctx).converteParaPedidoDoSistema(resultado);
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

        Collections.sort(pedidos, new Comparator<Pedido>() {
            @Override
            public int compare(Pedido p1, Pedido p2) {
                return p1.getDataPedido().compareTo(p2.getDataPedido());
            }
        });
        EmpresaFilial filial = new EmpresaFilialDAO(ctx, null).getFilial();

        for (Pedido pedido : pedidos) {

            getFilialDiferente(filial, pedido);
            setTratarDadosNulos(ctx, pedido);


            db.beginTransaction();
            try {
                if (!new PedidoDAO(ctx,db).exists(pedido))
                salvarPedido(pedido, pedido.getItensVendidos(), false);
                db.setTransactionSuccessful();
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Erro ao inserir pedido: " + e.getMessage());
            } finally {
                db.endTransaction();
            }
        }
        //     db.close();
        //     liberarBanco();
    }
    public void getAtualizarPedidosFilialEmpresaOrigem(){
        List<Pedido> pedidos = pedidosComFiliasDiferentes(null);
        EmpresaFilial filial = new EmpresaFilialDAO(ctx, null).getFilial();

        if (!pedidos.isEmpty() && filial != null){
            for (Pedido pedidoAtualizar : pedidos){
                atualizaPedidoFilialEmpresa(pedidoAtualizar.getIDPedido(), filial);
            }
        }

    }
    public static void buscarHistoricos(final Context ctx, final Vendedor parametro) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, /*parametro.getQtdeDiasGuardarPedido()*/ 32 * -1);
        Date dataInicial = cal.getTime();
        Date dataFinal = new Date();

        final DataParameterDTO dado = new DataParameterDTO(parametro.getToken(), dataInicial, dataFinal);

        new TaskGeneric(ctx, ctx.getString(br.net.sav.R.string.baixar_historico_de_pedidos), new TaskGeneric.IObjectIntance() {
            @Override
            public Object addObject() {
                return null;
            }

            @Override
            public String execute(Object object) {
                new PedidoRepository().buscarTodosPedidos(dado, getlistener(ctx));
                return null;
            }

            @Override
            public void posExecute(String s) {
                getAtualizarSequencialCasoForZero(ctx, parametro);
                getAtualizarFilialEmpresaHistorico(ctx);

            }
        }).execute();
    }


    private static void getAtualizarFilialEmpresaHistorico(Context ctx) {
        new PedidoDAO(ctx, null).getAtualizarPedidosFilialEmpresaOrigem();
    }

    private static void getAtualizarSequencialCasoForZero(Context ctx,Vendedor parametro) {
        if (parametro.getSequencial().equals("0")) {
            parametro.setSequencial(String.valueOf(new PedidoDAO(ctx, null)
                    .buscarSequecial(null)));
            new VendedorParametroDAO(ctx, null).update(parametro);
        }
    }

    public interface IAposBaixarHistoricoPedido{
        void aposHistorico();
    }

    private static void setTratarDadosNulos(Context ctx,Pedido pedido) {
        pedido.setPedidoCliente("");
        pedido.setObservacao("");
        pedido.setStatusFaturado("");
        pedido.setDesconsiderarFlex(false);
    }

}
