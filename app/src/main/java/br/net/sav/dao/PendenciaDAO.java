package br.net.sav.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Pendencia;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class PendenciaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Pendencia" ;
	private static final String TABELA_JOIN = "Pendencia p JOIN Cliente c ON (c.IdCliente = p.IdCliente)" ;
	private static final String[] colunas_join = {"p.IdCliente","p.NrTitulo","p.TipoDocumento","p.TaxaCorrecaoDiaria","p.Vencimento","p.Portador","p.Valor","p.Saldo","c.Razao"} ;
	
	private int qtde_vencidos;
	private double vencidos ;
	
	private int qtde_avencer ;
	private double avencer ;
	
	private int qtde_total ;
	private double total_geral ;
	
	public PendenciaDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	
	public boolean existePendencia(long idCliente) {
		SQLiteDatabase dbLocal = null; 
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		boolean retorno = false ;
		Cursor c = null ;
		try {
			c =  dbLocal.rawQuery("SELECT IdCliente FROM Pendencia WHERE IdCliente=?", new String[] {String.valueOf(idCliente)});
			
			retorno = c.moveToFirst() ;
			
		} catch (Exception e) {
			Log.e("PendenciaDAO", e.getMessage());
		} finally {
			c.close();
			//if(db == null)
				//dbLocal.close(); singletone();
		}
				
		return retorno ;
	}

	public List<Pendencia> getLista() {
		SQLiteDatabase dbLocal;
		List<Pendencia> lista=new ArrayList<Pendencia>();
		if(db==null)
			dbLocal=new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal=db;
		
		Cursor c = dbLocal.rawQuery("Select p.IdCliente,p.NrTitulo,p.TipoDocumento,p.Vencimento,p.Valor,p.Portador,p.Saldo,c.Razao from Pendencia p " +
				"left join Cliente c on (p.IdCliente = c.IdCliente) ORDER BY Vencimento",null);
		
		try {
			while(c.moveToNext()) {
				Pendencia p=new Pendencia();
				p = (Pendencia) preencherObjeto(c, p);
				p.setRazao(c.getString(c.getColumnIndex("Razao"))) ;
				lista.add(p);
			}									
		} finally {
			c.close();
			
	//		if(db==null)
				//dbLocal.close(); singletone();
		}		
		return lista;
	}
	
	public List<Pendencia> listaPendencia (long idCliente, short iFiltro ) throws ParseException {	
		SQLiteDatabase dbLocal;
		if(db==null)
			dbLocal=new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal=db;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		String[] param ;
		
		Date dataAtual = new Date(); 		
		dataAtual.setHours(0);
		dataAtual.setMinutes(0);
		dataAtual.setSeconds(0);
		
		if(idCliente != 0&& iFiltro == 0)
			param = new String[] {String.valueOf(idCliente)} ;
		else if (idCliente != 0 && iFiltro > 0)
			param = new String[] {String.valueOf(idCliente), String.valueOf(sdf.format(dataAtual).toString())} ;
		else 
			param = new String[] {String.valueOf(sdf.format(dataAtual))};
		
		List<Pendencia> listaPendencia = new ArrayList<Pendencia>();
		
		Cursor c = null ;
		
		if(idCliente == 0 && iFiltro == 0)
			c = dbLocal.query(TABELA_JOIN, colunas_join, null, null, null, null, "Vencimento");
		else if (idCliente != 0 && iFiltro == 0)
			c = dbLocal.query(TABELA_JOIN, colunas_join, "p.IdCliente=?", param, null, null, "p.Vencimento");
		else if (idCliente == 0 && iFiltro == 1)
			c = dbLocal.query(TABELA_JOIN, colunas_join, "p.Vencimento<=?", param, null, null, "p.Vencimento");
		else if (idCliente != 0 && iFiltro == 1)
			c = dbLocal.query(TABELA_JOIN, colunas_join, "p.IdCliente=? AND p.Vencimento <=?", param, null, null, "p.Vencimento");
		else if (idCliente == 0&& iFiltro == 2)
			c = dbLocal.query(TABELA_JOIN, colunas_join, "p.Vencimento> ?", param, null, null, "p.Vencimento");
		else if (idCliente != 0&& iFiltro == 2)
			c = dbLocal.query(TABELA_JOIN, colunas_join, "p.IdCliente=? AND p.Vencimento> ?", param, null, null, "p.Vencimento");
		
		int iqtde_vencidos = 0 ;
		int iqtde_avencer = 0 ;
		
		try {		
			if(c != null) {
				while(c.moveToNext()) {
					Pendencia pendencia = new Pendencia();
					
					pendencia.setIdCliente(c.getLong(0));
					pendencia.setNrTitulo(c.getLong(1));
					pendencia.setTipoDocumento(c.getString(2));
					pendencia.setTaxaCorrecaoDiaria(c.getDouble(3));
					pendencia.setVencimento(sdf.parse(c.getString(4)));
					pendencia.setPortador(c.getString(5));
					pendencia.setValor(c.getDouble(6));
					pendencia.setSaldo(c.getDouble(7));
					pendencia.setRazao(c.getString(8)); 
					
					//	Verifica��o t�tulos atrasados
					if (Utils.qtdeDiasPassou(pendencia.getVencimento()) > 0) {
						iqtde_vencidos += 1 ;
						
						setQtde_vencidos(iqtde_avencer);
						setAvencer(getAvencer() + (pendencia.getValor()));
					} else {
						iqtde_avencer += 1 ;
						
						setQtde_avencer(iqtde_avencer);
						setAvencer(getAvencer() + (pendencia.getValor()));
					}
					
					setQtde_total(iqtde_vencidos + iqtde_avencer);
					setTotal_geral(getVencidos() + getAvencer());
					
					listaPendencia.add(pendencia);
				}			
			}
		} catch (Exception e) {
			Log.e("PendenciaDAO:listaPendencia.", e.getMessage());
		} finally {
			c.close();			
		}		
		
	//	if(db==null)
			//dbLocal.close(); singletone();
		return listaPendencia ;
	}
	
	public boolean possuiPendencia(long idCliente){
		SQLiteDatabase db=new DBHelper(ctx).getWritableDatabase();
		boolean retorno=false;
		
		Cursor c=db.query(TABELA, new String[]{"NrTitulo"}, "IdCliente=?",new String[] {String.valueOf(idCliente)}, null,null, null);
		
		if(c.moveToFirst())
			retorno=true;
		//db.close(); singleton();
		c.close();
		
		return retorno;
	}
	
	public int getMaiorVencimentocliente(long idCliente) throws ParseException {
		SQLiteDatabase db=new DBHelper(ctx).getReadableDatabase();
		int qtdDias;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String data = null;
		Cursor c=db.rawQuery("select vencimento from pendencia where(idcliente=?) order by vencimento desc limit 1", new String[]{String.valueOf(idCliente)});
		if(c.moveToFirst())
			data=c.getString(0);
		Date dataVencimento=sdf.parse(data);
		qtdDias= (int) ((new Date().getTime() - dataVencimento.getTime()) / (1000*60*60*24));
		
		//db.close(); singleton();
		c.close();
		return qtdDias;
	}
				
	public void importar (List<String> linhasTexto,boolean cargaCompleta) {
		try{			
			if(cargaCompleta)
				deleteAll();
			getimportacaoPendencia(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("PendenciaDAO", e.getMessage());
		}
	}

	private void getimportacaoPendencia(List<String> linhasTexto, boolean cargaCompleta) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaPendenciaImportar(cargaCompleta, sdf, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaPendenciaImportar(boolean cargaCompleta, SimpleDateFormat sdf, String linha) throws ParseException {
		Pendencia p = new Pendencia() ;

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
			p.setIdCliente(Long.parseLong(linha.substring(0,8).trim()));
		else {
			Utils.gravarLog("\nPEN: "+linha);
			return true;
		}

		if(linha.substring(8, 14).trim().equals(""))
			p.setNrTitulo(0);
		else if(linha.substring(8, 14).trim().matches(MATCHES))
			p.setNrTitulo(Long.parseLong(linha.substring(8, 14).trim()));
		else {
			Utils.gravarLog("\nPEN: " +linha);
			return true;
		}
		p.setTipoDocumento(linha.substring(14, 16).trim().length()==0 ? "":linha.substring(14, 16).trim());

		if(linha.substring(16, 23).trim().equals(""))
			p.setTaxaCorrecaoDiaria(0);
		else if(linha.substring(16, 23).trim().matches(MATCHES))
			p.setTaxaCorrecaoDiaria(Double.parseDouble(linha.substring(16, 23).trim()) / 1000000);
		else {
			Utils.gravarLog("\nPEN: " +linha);
			return true;
		}
		p.setVencimento(sdf.parse(linha.substring(23,31).trim()));

		if(linha.substring(31,41).trim().length()==0)
			p.setPortador("");
		else
			p.setPortador(linha.substring(31, 41).trim());

		if(linha.substring(41, 51).trim().equals(""))
			p.setValor(0);
		else if(linha.substring(41, 51).trim().matches(MATCHES))
			p.setValor(Double.parseDouble(linha.substring(41, 51).trim()) / 100);
		else {
			Utils.gravarLog("\nPEN: " +linha);
			return true;
		}
		if(linha.substring(52, 62).trim().equals(""))
			p.setSaldo(0);
		else if(linha.substring(52, 62).trim().matches(MATCHES))
			p.setSaldo(Double.parseDouble(linha.substring(52, 62).trim()) / 100);
		else {
			Utils.gravarLog("\nPEN: " +linha);
			return true;
		}

		if(!cargaCompleta) {
			switch(Character.toUpperCase(linha.charAt(34))) {
			case ' ' :
			case 'A' :
				if(exists(p))
					update(p);
				else
					insert(p);
				break ;
			case 'E' :
				delete(p);
				break;
			default:
				Utils.gravarLog("\nPEN(Flag Inv�lida): "+linha);
				break;
			}
		} else {
			insert(p);
		}
		return false;
	}

	public int getQtde_vencidos() {
		return qtde_vencidos;
	}

	public void setQtde_vencidos(int qtde_vencidos) {
		this.qtde_vencidos = qtde_vencidos;
	}

	public double getVencidos() {
		return vencidos;
	}

	public void setVencidos(double vencidos) {
		this.vencidos = vencidos;
	}

	public int getQtde_avencer() {
		return qtde_avencer;
	}

	public void setQtde_avencer(int qtde_avencer) {
		this.qtde_avencer = qtde_avencer;
	}

	public double getAvencer() {
		return avencer;
	}

	public void setAvencer(double avencer) {
		this.avencer = avencer;
	}

	public int getQtde_total() {
		return qtde_total;
	}

	public void setQtde_total(int qtde_total) {
		this.qtde_total = qtde_total;
	}

	public double getTotal_geral() {
		return total_geral;
	}

	public void setTotal_geral(double total_geral) {
		this.total_geral = total_geral;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			if (getLinhaPendenciaImportar(true, sdf, linha)) return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_pendencias_financeiras);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}