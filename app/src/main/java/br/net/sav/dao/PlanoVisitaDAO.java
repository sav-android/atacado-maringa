package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.PlanoVisita;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class PlanoVisitaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	public static final String TABELA="PlanoVisita";
	
	public PlanoVisitaDAO(Context ctx,SQLiteDatabase db){
		super(ctx,TABELA,db);
	}
	
	public void importar(List<String> linhasTexto, boolean cargaCompleta) {
		try{
			if(cargaCompleta)
				deleteAll();
			getImportacaoPlanoVis(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("CondicaoProdutoDAO", e.getMessage());
		}
	}

	private void getImportacaoPlanoVis(List<String> linhasTexto, boolean cargaCompleta)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaPlanoVisitaImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaPlanoVisitaImportar(boolean cargaCompleta, String linha) {
		PlanoVisita c = new PlanoVisita() ;

		if(linha.substring(0, 3).trim().matches(MATCHES) && !linha.substring(0, 3).trim().equals(""))
			c.setQuantidadeDias(Long.parseLong(linha.substring(0,3).trim()));
		else {
			Utils.gravarLog("\nQDI: " +linha);
			return true;
		}
		if(linha.substring(3, 23).trim().matches(MATCHES) && !linha.substring(3, 23).trim().equals(""))
			c.setDescricao(linha.substring(3, 23).trim());
		else {
			Utils.gravarLog("\nQDI: " +linha);
			return true;
		}

		if(!cargaCompleta) {
			switch(Character.toUpperCase(linha.charAt(24))){
				case ' ':
				case 'A':
					if (!exists(c))
						insert(c);
					else
						update(c);
					break;
				case'E':
					delete(c);
					break;
				default:
					Utils.gravarLog("\nQDI: " +linha);
					break;
			}
		} else {
			insert(c);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaPlanoVisitaImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_plano_visita);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
