package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Preco;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class PrecoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Preco" ;
	
	public PrecoDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}

	public void importar (List<String> linhasTexto, Boolean cargaCompleta ) {
		try {
			if (cargaCompleta)
				deleteAll();
			Parametro parametro = new ParametroDAO(ctx,db).get();
			new PrecoDAO(ctx,db);
			getImportacaoPreco(linhasTexto, parametro);
		} catch (Exception e) {
			Log.d("PrecoDAO", e.getMessage());
		}
	}

	private void getImportacaoPreco(List<String> linhasTexto, Parametro parametro) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaPrecoImportar(parametro, linha)) continue;
			}

		} finally {
			db.setTransactionSuccessful();
			db.endTransaction();
		}
	}

	private boolean getLinhaPrecoImportar(Parametro parametro, String linha) {
		Preco p = new Preco();

		//Embalagem 1
		if(linha.substring(0, 9).trim().matches(MATCHES) && !linha.substring(0, 9).trim().equals(""))
			p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}
		if(linha.substring(9, 13).trim().matches(MATCHES) && !linha.substring(9, 13).trim().equals(""))
			p.setIdTabela(Short.parseShort(linha.substring(9, 13).trim()));
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}
		if(linha.substring(13, 17).trim().equals(""))
			p.setIdEmpresa((short) 0);
		else if(linha.substring(13, 17).trim().matches(MATCHES))
			p.setIdEmpresa(Short.parseShort(linha.substring(13, 17).trim()));
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}
		if(linha.substring(17, 21).trim().equals(""))
			p.setIdFilial((short) 0);
		else if(linha.substring(17, 21).trim().matches(MATCHES))
			p.setIdFilial(Short.parseShort(linha.substring(17, 21).trim()));
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}
		if(linha.substring(21, 23).trim().equals(""))
			p.setCodigoEmbalagem((short) 0);
		else if(linha.substring(21, 23).trim().matches(MATCHES))
			p.setCodigoEmbalagem(Short.parseShort(linha.substring(21, 23).trim()));
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}
		if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
			p.setIdProduto(p.getIdProduto() * 10);
		else if (parametro.getIncluiEmbalagem() == 2)
			p.setIdProduto(p.getIdProduto() * 100);

		if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 2)
			p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
		else if (parametro.getIncluiEmbalagem() == 3)
			p.setIdProduto(p.getIdProduto() + 1);

		if(linha.substring(23, 33).trim().equals(""))
			p.setPrecoNormal(0);
		else if(linha.substring(23, 33).trim().matches(MATCHES))
			p.setPrecoNormal(Double.parseDouble(linha.substring(23, 33).trim()) / 100);
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}

		if(linha.substring(35, 45).trim().equals(""))
			p.setPrecoMinimo(0);
		else if(linha.substring(35, 45).trim().matches(MATCHES))
			p.setPrecoMinimo(Double.parseDouble(linha.substring(35,45).trim()) / 100);
		else {
			Utils.gravarLog("\nPRE: " +linha);
			return true;
		}


		if(!exists(p))
			insert(p);
		else
			update(p);

		//Embalagem 2
		if (parametro.getIncluiEmbalagem()!=0) {
			p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
			if (linha.substring(33, 35).trim().equals(""))
				p.setCodigoEmbalagem((short) 0);
			else
				p.setCodigoEmbalagem(Short.parseShort(linha.substring(33, 35).trim()));

			if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
				p.setIdProduto(p.getIdProduto() * 10);
			else if (parametro.getIncluiEmbalagem() == 2)
				p.setIdProduto(p.getIdProduto() * 100);

			if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 2)
				p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
			else if (parametro.getIncluiEmbalagem() == 3)
				p.setIdProduto(p.getIdProduto() + 2);
			if(exists(p))
				update(p);
			else
				insert(p);
		}
		return false;
	}

	public Preco getPreco( SQLiteDatabase db, long idProduto, long idTabela) {
		Preco retorno = new Preco();
		SQLiteDatabase dbLocal = null;

		if (db == null){
			dbLocal = new DBHelper(ctx).getReadableDatabase();

		} else{
			dbLocal = db;
		}

		Cursor c = dbLocal.rawQuery("SELECT * FROM Preco WHERE IdProduto ="+ idProduto+" and IdTabela ="+ idTabela, null);

		if (c.moveToFirst()) {
			retorno = (Preco) preencherObjeto(c, retorno);;
		}
		c.close();

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaPrecoImportar(new ParametroDAO(ctx,null).get(), linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_precos_dos_produtos);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
