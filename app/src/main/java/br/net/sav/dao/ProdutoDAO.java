package br.net.sav.dao;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import br.net.sav.IntegradorWeb.dto.ImportacaoDTO;
import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.IntegradorWeb.repository.ImportacaoRepository;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Produto;
import br.net.sav.modelo.ProdutoSemVenda;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ProdutoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "Produto";

    public ProdutoDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public List<Long> listaIdProdutos() {
        Cursor c = null;

        List<Long> lista = new ArrayList<Long>();

        try {
            c = db.query(TABELA, new String[]{"IdProduto"}, null, null, null, null, null);

            while (c.moveToNext())
                lista.add(c.getLong(0));

        } catch (Exception e) {
            Log.e("ProdutoDAO:", "listaIdProdutos" + e.getMessage());
        } finally {
            if (c != null)
                c.close();
        }
        return lista;
    }

    public List<Produto> listaProdutos() {
        SQLiteDatabase dbLocal = null;
        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        List<Produto> lista = new ArrayList<Produto>();

        Cursor c = null;

        Produto p = null;

        try {

            c = dbLocal.query(TABELA, colunas, null, null, null, null, null);

            while (c.moveToNext()) {
                p = (Produto) preencherObjeto(c, p);
                lista.add(p);
            }

        } catch (Exception e) {
            Log.e("ProdutoDAO:listaProdutos.", e.getMessage());
        } finally {
            c.close();
            //if (db == null)
                //dbLocal.close(); singletone();
        }

        return lista;
    }

    public List<Produto> getListaEstoque() {
        SQLiteDatabase dbLocal;
        List<Produto> lista = new ArrayList<Produto>();

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        Cursor c = dbLocal.rawQuery("SELECT IdProduto,Descricao,SaldoEstoque from Produto ORDER BY Descricao", null);

        try {
            while (c.moveToNext()) {
                Produto p = new Produto();
                p.setIdProduto(c.getLong(c.getColumnIndex("IdProduto")));
                p.setDescricao(c.getString(c.getColumnIndex("Descricao")));
                p.setSaldoEstoque(c.getString(c.getColumnIndex("SaldoEstoque")));
                lista.add(p);
            }
        } finally {
            c.close();
        }

        //if (db == null)
            //dbLocal.close(); singletone();

        return lista;
    }

    public String getSaldoEstoque() {
        SQLiteDatabase dbLocal;

        String saldoEstoque = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

//Agora alterado o campo SaldoEstoque para string, n?o faremos mais o calculo.
//		Cursor c = dbLocal.rawQuery("SELECT sum(pro.SaldoEstoque*pre.PrecoNormal) from Produto pro inner join Preco pre on (pro.idProduto=pre.idProduto) ", null);
        Cursor c = dbLocal.rawQuery("SELECT pro.SaldoEstoque from Produto pro inner join Preco pre on (pro.idProduto=pre.idProduto) ", null);

        saldoEstoque += c;

        c.close();

        return saldoEstoque;
    }


    public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
        try {
            db.beginTransactionNonExclusive();
            if (cargaCompleta)
                deleteAll();

            Parametro parametro = new ParametroDAO(ctx, db).get();
            try {
               // db.beginTransactionNonExclusive();

                for (String linha : linhasTexto) {

                    if (linha.length() > 193) {
                        //Importa��o Nova Produto com 80 caracteres
                        importacaoNova(linha, db, parametro, cargaCompleta);
                        Log.d("Importacao", "Nova");
                    }else{
                        importacaoAntiga(linha, db, parametro, cargaCompleta);
                        Log.d("Importacao", "Antiga");
                    }
                }

                db.setTransactionSuccessful();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                Log.d("ProdutoDAO", e.getMessage());
            }catch (Exception e) {
                Log.d("ProdutoDAO", e.getMessage());
            } finally {
                db.endTransaction();
            }
        } catch (Exception e) {
            Log.d("ProdutoDAO", e.getMessage());
        }
    }

    private void importacaoAntiga(String linha, SQLiteDatabase db, Parametro parametro, Boolean cargaCompleta) {

        //Importa��o Antiga com Produto com 40 caracteres

        Produto p = new Produto();

        // Embalagem 1
        if (linha.substring(0, 9).trim().matches(MATCHES) && !linha.substring(0, 9).trim().equals(""))
            p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        if (linha.substring(9, 17).trim().matches(MATCHES) && !linha.substring(9, 17).trim().equals(""))
            p.setIdFornecedor(Short.parseShort(linha.substring(9, 17).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setDescricao(linha.substring(17, 57).trim());
        if (linha.substring(57, 59).trim().equals(""))
            p.setCodigoEmbalagem((short) 0);
        else if (linha.substring(57, 59).trim().matches(MATCHES))
            p.setCodigoEmbalagem(Short.parseShort(linha.substring(57, 59).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setDescricaoEmbalagem(linha.substring(59, 69).trim());

        if (linha.substring(69, 75).trim().equals(""))
            p.setQuantidadeEmbalagem(0);
        else if (linha.substring(69, 75).trim().matches(MATCHES))
            p.setQuantidadeEmbalagem(Long.parseLong(linha.substring(69, 75).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (p.getQuantidadeEmbalagem() == 0)
            p.setQuantidadeEmbalagem(1);

        if (parametro.getIncluiEmbalagem() >= 1) {
            if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
                p.setIdProduto(p.getIdProduto() * 10);
            else if (parametro.getIncluiEmbalagem() == 2)
                p.setIdProduto(p.getIdProduto() * 100);

            if (parametro.getIncluiEmbalagem() != 3)
                p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
            else
                p.setIdProduto(p.getIdProduto() + 1);
        }

        if (linha.substring(93, 97).trim().matches(MATCHES) && !linha.substring(93, 97).trim().equals(""))
            p.setIdSecao(Short.parseShort(linha.substring(93, 97).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(97, 101).trim().matches(MATCHES) && !linha.substring(97, 101).trim().equals(""))
            p.setIdICMS(Short.parseShort(linha.substring(97, 101).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setCodigoBarra(linha.substring(102, 115).trim());

        if (linha.substring(115, 119).trim().matches(MATCHES) && !linha.substring(115, 119).trim().equals(""))
            p.setIdSubGrupo(Short.parseShort(linha.substring(115, 119).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setControleGordura(linha.substring(119, 120));

        if (linha.substring(120, 126).trim().equals(""))
            p.setDescontoGordura(0);
        else if (linha.substring(120, 126).trim().matches(MATCHES))
            p.setDescontoGordura(Double.parseDouble(linha.substring(120, 126).trim()) / 10000);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(126, 129).trim().equals(""))
            p.setMultiplo((short) 0);
        else if (linha.substring(126, 129).trim().matches(MATCHES))
            p.setMultiplo((Short.parseShort(linha.substring(126, 129))));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

					/*if (p.getMultiplo() == (short) 0)
						p.setMultiplo((short) 1);*/

        if (linha.substring(129, 133).trim().matches(MATCHES) && !linha.substring(129, 133).trim().equals(""))
            p.setIdGrupo(Short.parseShort(linha.substring(129, 133).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        if (linha.substring(133, 137).trim().matches(MATCHES) && !linha.substring(133, 137).trim().equals(""))
            p.setIdLinha(Short.parseShort(linha.substring(133, 137).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(137, 138).trim().equals(""))
            p.setEstoqueBaixo((short) 0);
        else if (linha.substring(137, 138).trim().matches(MATCHES))
            p.setEstoqueBaixo(Short.parseShort(linha.substring(137, 138).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(138, 141).trim().equals(""))
            p.setQuantidadeCaixa((short) 0);
        else if (linha.substring(138, 141).trim().matches(MATCHES))
            p.setQuantidadeCaixa(Short.parseShort(linha.substring(138, 141).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.length() >= 147) {
            p.setSaldoEstoque(linha.substring(141, 147).trim());
        } else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(147, 148).trim().matches(MATCHES) && !linha.substring(147, 148).trim().equals(""))
            p.setPermiteVender(Integer.parseInt(linha.substring(147, 148)) == 0);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(150, 162).trim().equals(""))
            p.setPeso(0);
        else if (linha.substring(150, 162).trim().matches(MATCHES))
            p.setPeso(Double.parseDouble(linha.substring(150, 162).trim()) / 100);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        p.setStatus(linha.substring(162, 163));

        if (linha.substring(163, 164).trim().matches(MATCHES) && !linha.substring(163, 164).trim().equals(""))
            p.setPermiteMeiaCaixa(Boolean.parseBoolean(linha.substring(163, 164)));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        p.setCodigoReferencia(linha.substring(164, 184).trim());

        if (linha.substring(184, 185).trim().equals("")) {
            p.setPermiteBonificacao(false);
        } else if (linha.substring(184, 185).trim().matches(MATCHES)) {
            p.setPermiteBonificacao(Boolean.parseBoolean(linha.substring(184, 185)));
        } else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(185, 189).trim().equals(""))
            p.setMaximoDesconto(0);
        else if (linha.substring(185, 189).trim().matches(MATCHES))
            p.setMaximoDesconto(Double.parseDouble(linha.substring(185, 189).trim()) / 100);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(189, 193).trim().equals(""))
            p.setIdMarca(0);
        else if (linha.substring(189, 193).trim().matches(MATCHES))
            p.setIdMarca(Long.parseLong(linha.substring(189, 193).trim()));
        else {
            Utils.gravarLog("\nPRO (189, 193): " + linha);
            return;
        }

        p.setDivisaoPreco(1);

        if (!linha.substring(75, 77).trim().equals("")) {
            if (Short.parseShort(linha.substring(75, 77).trim()) > 0)
                p.setUnitario((short) 0);
            else
                p.setUnitario((short) 1);
        } else {
            p.setUnitario((short) 1);
        }

        new ProdutoDAO(ctx, db);
        switch (Character.toUpperCase(linha.charAt(101))) {
            case ' ':
            case 'A':
                if (exists(p))
                    update(p);
                else
                    insert(p);
                break;
            case 'E':
                delete(p);
                break;
            default:
                Utils.gravarLog("\nPRO(Flag Inv�lida): " + linha);
                break;
        }

        // Embalagem 2
        if (parametro.getIncluiEmbalagem() >= 1) {
            if (!linha.substring(75, 77).trim().equals("") && Short.parseShort(linha.substring(75, 77).trim()) > 0) {
                if (!linha.substring(87, 93).trim().equals("") && Integer.parseInt(linha.substring(87, 93).trim()) > 0) {

                    if (linha.substring(0, 9).trim().matches(MATCHES) && !linha.substring(0, 9).trim().equals(""))
                        p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    if (linha.substring(75, 77).trim().matches(MATCHES))
                        p.setCodigoEmbalagem(Short.parseShort(linha.substring(75, 77).trim()));
                    else if (linha.substring(75, 77).trim().equals(""))
                        p.setCodigoEmbalagem((short) 0);
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    p.setDescricaoEmbalagem(linha.substring(77, 87).trim());

                    if (linha.substring(87, 93).trim().matches(MATCHES))
                        p.setQuantidadeEmbalagem(Long.parseLong(linha.substring(87, 93).trim()));
                    else if (linha.substring(87, 93).trim().equals(""))
                        p.setQuantidadeEmbalagem(0);
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    if (linha.substring(126, 129).trim().equals(""))
                        p.setMultiplo((short) 1);
                    else if (linha.substring(126, 129).trim().matches(MATCHES))
                        p.setMultiplo(Short.parseShort(linha.substring(126, 129).trim()));
                    else {
                        Utils.gravarLog("\nPRO (126, 129): " + linha);
                        return;
                    }

                    if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
                        p.setIdProduto(p.getIdProduto() * 10);
                    else if (parametro.getIncluiEmbalagem() == 2)
                        p.setIdProduto(p.getIdProduto() * 100);

                    if (parametro.getIncluiEmbalagem() != 3)
                        p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
                    else
                        p.setIdProduto(p.getIdProduto() + 2);

                    if (linha.substring(69, 75).trim().matches(MATCHES) && !linha.substring(69, 75).trim().equals(""))
                        p.setDivisaoPreco(Long.parseLong(linha.substring(69, 75).trim()));
                    else {
                        Utils.gravarLog("\nPRO (69, 75): " + linha);
                        return;
                    }
                    p.setUnitario((short) 2);

                    if (!cargaCompleta) {
                        switch (Character.toUpperCase(linha.charAt(101))) {
                            case ' ':
                            case 'A':
                                if (exists(p))
                                    update(p);
                                else
                                    insert(p);
                                break;
                            case 'E':
                                delete(p);
                                break;
                            default:
                                Utils.gravarLog("\nPRO(Flag Inv?lida): " + linha);
                                break;
                        }
                    } else {
                        insert(p);
                    }
                }
            }
        }
    }

    private void importacaoNova(String linha, SQLiteDatabase db, Parametro parametro, Boolean cargaCompleta) {
        Produto p = new Produto();

        int valor = linha.length() > 253 ? 7 : 0;

        // Embalagem 1
        if (linha.substring(0, 9).trim().matches(MATCHES) && !linha.substring(0, 9).trim().equals(""))
            p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        if (linha.substring(9, 17).trim().matches(MATCHES) && !linha.substring(9, 17).trim().equals(""))
            p.setIdFornecedor(Short.parseShort(linha.substring(9, 17).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setDescricao(linha.substring(17, 97).trim());
        if (linha.substring(97, 99).trim().equals(""))
            p.setCodigoEmbalagem((short) 0);
        else if (linha.substring(97, 99).trim().matches(MATCHES))
            p.setCodigoEmbalagem(Short.parseShort(linha.substring(97, 99).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setDescricaoEmbalagem(linha.substring(99, 109).trim());

        if (linha.substring(109, 115).trim().equals(""))
            p.setQuantidadeEmbalagem(0);
        else if (linha.substring(109, 115).trim().matches(MATCHES))
            p.setQuantidadeEmbalagem(Long.parseLong(linha.substring(109, 115).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (p.getQuantidadeEmbalagem() == 0)
            p.setQuantidadeEmbalagem(1);

        if (parametro.getIncluiEmbalagem() >= 1) {
            if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
                p.setIdProduto(p.getIdProduto() * 10);
            else if (parametro.getIncluiEmbalagem() == 2)
                p.setIdProduto(p.getIdProduto() * 100);

            if (parametro.getIncluiEmbalagem() != 3)
                p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
            else
                p.setIdProduto(p.getIdProduto() + 1);
        }

        if (linha.substring(137, 141).trim().matches(MATCHES) && !linha.substring(137, 141).trim().equals(""))
            p.setIdICMS(Short.parseShort(linha.substring(137, 141).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        p.setCodigoBarra(linha.substring(142, 155 + valor).trim());

        if (linha.substring(155 + valor, 159 + valor).trim().matches(MATCHES) && !linha.substring(155 + valor, 159 + valor).trim().equals(""))
            p.setIdSubGrupo(Short.parseShort(linha.substring(155 + valor, 159 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        p.setControleGordura(linha.substring(159 + valor, 160 + valor));

        if (linha.substring(173 + valor, 177 + valor).trim().matches(MATCHES) && !linha.substring(173 + valor, 177 + valor).trim().equals(""))
            p.setIdSecao(Short.parseShort(linha.substring(173 + valor, 177 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(160 + valor, 166 + valor).trim().equals(""))
            p.setDescontoGordura(0);
        else if (linha.substring(160 + valor, 166 + valor).trim().matches(MATCHES))
            p.setDescontoGordura(Double.parseDouble(linha.substring(160 + valor, 166 + valor).trim()) / 10000);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(166 + valor, 169 + valor).trim().equals(""))
            p.setMultiplo((short) 0);
        else if (linha.substring(166 + valor, 169 + valor).trim().matches(MATCHES))
            p.setMultiplo((Short.parseShort(linha.substring(166 + valor, 169 + valor))));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

					/*if (p.getMultiplo() == (short) 0)
                        p.setMultiplo((short) 1);*/

        if (linha.substring(169 + valor, 173 + valor).trim().matches(MATCHES) && !linha.substring(169 + valor, 173 + valor).trim().equals(""))
            p.setIdGrupo(Short.parseShort(linha.substring(169 + valor, 173 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }
        if (linha.substring(173 + valor, 177 + valor).trim().matches(MATCHES) && !linha.substring(173 + valor, 177 + valor).trim().equals(""))
            p.setIdLinha(Short.parseShort(linha.substring(173 + valor, 177 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(177 + valor, 178 + valor).trim().equals(""))
            p.setEstoqueBaixo((short) 0);
        else if (linha.substring(177 + valor, 178 + valor).trim().matches(MATCHES))
            p.setEstoqueBaixo(Short.parseShort(linha.substring(177 + valor, 178 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(178 + valor, 181 + valor).trim().equals(""))
            p.setQuantidadeCaixa((short) 0);
        else if (linha.substring(178 + valor, 181 + valor).trim().matches(MATCHES))
            p.setQuantidadeCaixa(Short.parseShort(linha.substring(178 + valor, 181 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.length() >= 181 + valor) {
            p.setSaldoEstoque(linha.substring(181 + valor, 187 + valor).trim());
        } else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(187 + valor, 188 + valor).trim().matches(MATCHES) && !linha.substring(187 + valor, 188 + valor).trim().equals(""))
            p.setPermiteVender(Integer.parseInt(linha.substring(187 + valor, 188 + valor)) == 0);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(190 + valor, 202 + valor).trim().equals(""))
            p.setPeso(0);
        else if (linha.substring(190 + valor, 202 + valor).trim().matches(MATCHES))
            p.setPeso(Double.parseDouble(linha.substring(190 + valor, 202 + valor).trim()) / 100);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        p.setStatus(linha.substring(202 + valor, 203 + valor));

        if (linha.substring(203 + valor, 204 + valor).trim().matches(MATCHES) && !linha.substring(203 + valor, 204 + valor).trim().equals(""))
            p.setPermiteMeiaCaixa(Boolean.parseBoolean(linha.substring(203 + valor, 204 + valor)));
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        p.setCodigoReferencia(linha.substring(204 + valor, 224 + valor).trim());

        if (linha.substring(224 + valor, 225 + valor).trim().equals("")) {
            p.setPermiteBonificacao(false);
        } else if (linha.substring(224 + valor, 225 + valor).trim().matches(MATCHES)) {
            p.setPermiteBonificacao(Boolean.parseBoolean(linha.substring(224 + valor, 225 + valor)));
        } else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(225 + valor, 229 + valor).trim().equals(""))
            p.setMaximoDesconto(0);
        else if (linha.substring(225 + valor, 229 + valor).trim().matches(MATCHES))
            p.setMaximoDesconto(Double.parseDouble(linha.substring(225 + valor, 229 + valor).trim()) / 100);
        else {
            Utils.gravarLog("\nPRO: " + linha);
            return;
        }

        if (linha.substring(229 + valor, 233 + valor).trim().equals(""))
            p.setIdMarca(0);
        else if (linha.substring(229 + valor, 233 + valor).trim().matches(MATCHES))
            p.setIdMarca(Long.parseLong(linha.substring(229 + valor, 233 + valor).trim()));
        else {
            Utils.gravarLog("\nPRO (229, 233): " + linha);
            return;
        }
        String path = caminhoFoto(p);
        File file = new File(path);
        if (file.exists()){
            p.setPossuiFoto(Boolean.TRUE);
        }else
             p.setPossuiFoto(Boolean.FALSE);

        if(linha.length() > 233 + valor) {
            if (linha.substring(233 + valor, 243 + valor).trim().equals(""))
                p.setPrecoCustoProduto(0);
            else if (linha.substring(233 + valor, 243 + valor).trim().matches(MATCHES))
                p.setPrecoCustoProduto(Double.parseDouble(linha.substring(233 + valor, 243 + valor).trim())/ 100);
            else {
                Utils.gravarLog("\nPRO (233, 243): " + linha);
                return;
            }
        }else
            p.setPrecoCustoProduto(0);

        if(linha.length() > 243 + valor) {
            if (linha.substring(243 + valor, 248 + valor).trim().equals(""))
                p.setPercentualRentabilidadeMinima(0);
            else if (linha.substring(243 + valor, 248 + valor).trim().matches(MATCHES))
                p.setPercentualRentabilidadeMinima(Double.parseDouble(linha.substring(243 + valor, 248 + valor).trim()) / 100);
            else {
                Utils.gravarLog("\nPRO (243, 248): " + linha);
                return;
            }
        }else
            p.setPercentualRentabilidadeMinima(0);


        if(linha.length() > 248 + valor) {
            if (linha.substring(248 + valor, 253 + valor).trim().equals(""))
                p.setPercentualRentabilidadeMedia(0);
            else if (linha.substring(248 + valor, 253 + valor).trim().matches(MATCHES))
                p.setPercentualRentabilidadeMedia(Double.parseDouble(linha.substring(248 + valor, 253 + valor).trim()) / 100);
            else {
                Utils.gravarLog("\nPRO (248, 253): " + linha);
                return;
            }
        }
        else
            p.setPercentualRentabilidadeMedia(0);

        if (linha.length() > 253 + valor){
            if (linha.substring(253 + valor, 263 + valor).trim().equals(""))
                p.setCodigoNCM("0");
            else if (linha.substring(253 + valor, 263 + valor).trim().matches(MATCHES))
                p.setCodigoNCM(linha.substring(253 + valor, 263 + valor).trim());
            else {
                Utils.gravarLog("\nPRO (253, 263): " + linha);
                return;
            }
        }else {
            p.setCodigoNCM("0");
        }

        p.setDivisaoPreco(1);

        if (!linha.substring(115, 117).trim().equals("")) {
            if (Short.parseShort(linha.substring(115, 117).trim()) > 0)
                p.setUnitario((short) 0);
            else
                p.setUnitario((short) 1);
        } else {
            p.setUnitario((short) 1);
        }

        new ProdutoDAO(ctx, db);
        switch (Character.toUpperCase(linha.charAt(141))) {
            case ' ':
            case 'A':
                if (exists(p))
                    update(p);
                else
                    insert(p);
                break;
            case 'E':
                delete(p);
                break;
            default:
                Utils.gravarLog("\nPRO(Flag Inv?lida): " + linha);
                break;
        }

        // Embalagem 2
        if (parametro.getIncluiEmbalagem() >= 1) {
            if (!linha.substring(115, 117).trim().equals("") && Short.parseShort(linha.substring(115, 117).trim()) > 0) {
                if (!linha.substring(127, 133).trim().equals("") && Integer.parseInt(linha.substring(127, 133).trim()) > 0) {

                    if (linha.substring(0, 9).trim().matches(MATCHES) && !linha.substring(0, 9).trim().equals(""))
                        p.setIdProduto(Long.parseLong(linha.substring(0, 9).trim()));
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    if (linha.substring(115, 117).trim().matches(MATCHES))
                        p.setCodigoEmbalagem(Short.parseShort(linha.substring(115, 117).trim()));
                    else if (linha.substring(115, 117).trim().equals(""))
                        p.setCodigoEmbalagem((short) 0);
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    p.setDescricaoEmbalagem(linha.substring(117, 127).trim());

                    if (linha.substring(127, 133).trim().matches(MATCHES))
                        p.setQuantidadeEmbalagem(Long.parseLong(linha.substring(127, 133).trim()));
                    else if (linha.substring(127, 133).trim().equals(""))
                        p.setQuantidadeEmbalagem(0);
                    else {
                        Utils.gravarLog("\nPRO: " + linha);
                        return;
                    }

                    if (linha.substring(166 + valor, 169 + valor).trim().equals(""))
                        p.setMultiplo((short) 1);
                    else if (linha.substring(166 + valor, 169 + valor).trim().matches(MATCHES))
                        p.setMultiplo(Short.parseShort(linha.substring(166 + valor, 169  + valor).trim()));
                    else {
                        Utils.gravarLog("\nPRO (166, 169): " + linha);
                        return;
                    }

                    if (parametro.getIncluiEmbalagem() == 1 || parametro.getIncluiEmbalagem() == 3)
                        p.setIdProduto(p.getIdProduto() * 10);
                    else if (parametro.getIncluiEmbalagem() == 2)
                        p.setIdProduto(p.getIdProduto() * 100);

                    if (parametro.getIncluiEmbalagem() != 3)
                        p.setIdProduto(p.getIdProduto() + p.getCodigoEmbalagem());
                    else
                        p.setIdProduto(p.getIdProduto() + 2);

                    if (linha.substring(109, 115).trim().matches(MATCHES) && !linha.substring(109, 115).trim().equals(""))
                        p.setDivisaoPreco(Long.parseLong(linha.substring(109, 115).trim()));
                    else {
                        Utils.gravarLog("\nPRO (109, 115): " + linha);
                        return;
                    }

                    String path2 = caminhoFoto(p);
                    File file2 = new File(path2);
                    if (file2.exists()){
                        p.setPossuiFoto(Boolean.TRUE);
                    }else
                        p.setPossuiFoto(Boolean.FALSE);
                    p.setUnitario((short) 2);

                    if (!cargaCompleta) {
                        switch (Character.toUpperCase(linha.charAt(141))) {
                            case ' ':
                            case 'A':
                                if (exists(p))
                                    update(p);
                                else
                                    insert(p);
                                break;
                            case 'E':
                                delete(p);
                                break;
                            default:
                                Utils.gravarLog("\nPRO(Flag Inv?lida): " + linha);
                                break;
                        }
                    } else {
                        if (!exists(p))
                            insert(p);
                        else
                            update(p);
                    }
                }
            }
        }
    }

    private String caminhoFoto(Produto p) {
        //TODO em teste
        String path = Environment.getExternalStorageDirectory() + "/sav/fotos/"+ p.getIdProduto() +".jpg";
        File file = new File(path);
        if(!file.exists()){
            path = Environment.getExternalStorageDirectory() + "/sav/fotos/"+ p.getIdProduto() +".jpeg";
        }

        File file2 = new File(path);
        if(!file2.exists()){
            path = Environment.getExternalStorageDirectory() + "/sav/fotos/"+ p.getIdProduto() +".png";
        }
        return path;
    }

    public Produto get(long IdProduto) {
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

        String[] param = new String[]{String.valueOf(IdProduto)};

        Produto produto = new Produto();

        Cursor c = db.query(TABELA, colunas, "IdProduto=?", param, null, null, null);

        try {
            if (c.moveToFirst())
                produto = (Produto) preencherObjeto(c, produto);
        } catch (Exception e) {
            Log.e("ProdutoDAO:get.", e.getMessage());
        } finally {
            //db.close(); singleton();
            c.close();
        }

        return produto;
    }

    public List<ProdutoSemVenda> listaSemVenda(int qtdeDias) throws ParseException {
        SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
        List<ProdutoSemVenda> listaSemVenda = new ArrayList<ProdutoSemVenda>();

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new Date());
        gc.set(Calendar.DATE, gc.get(Calendar.DATE) - qtdeDias);
        Date dataApurar = gc.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT pro.Descricao, pro.DescricaoEmbalagem, ( \n");
        sql.append("  SELECT ite.DataPedido FROM Item ite \n");
        sql.append("  WHERE ite.IdProduto = pro.IdProduto \n");
        sql.append("  ORDER BY ite.DataPedido desc limit 1 \n");
        sql.append(") DataUltimaVenda, ( \n");
        sql.append(" SELECT ultite.Data FROM UltimaItem ultite \n");
        sql.append(" WHERE ultite.IdProduto = pro.IdProduto \n");
        sql.append(" ORDER BY Ultite.Data desc limit 1 \n");
        sql.append(") DataUltima \n");
        sql.append(" FROM Produto pro \n");
        sql.append(" WHERE pro.IdProduto not in \n");
        sql.append("( \n");
        sql.append(" SELECT ite.IdProduto FROM Item ite \n");
        sql.append(" WHERE ite.IdProduto = pro.IdProduto AND \n");
        sql.append(String.format("  ite.datapedido >= \'%s\' \n", sdf.format(dataApurar)));
        sql.append(") AND pro.IdProduto not in \n");
        sql.append("( \n");
        sql.append(" SELECT ULTITE.IdProduto FROM UltimaItem ultite \n");
        sql.append(" WHERE ultite.IdProduto = pro.IdProduto AND \n");
        sql.append(String.format("   ultite.data >= \'%s\' \n", sdf.format(dataApurar)));
        sql.append(") \n");
        sql.append("ORDER BY pro.Descricao \n");

        Cursor c = db.rawQuery(sql.toString(), null);

        ProdutoSemVenda produtoSemVenda = null;

        try {
            while (c.moveToNext()) {
                Date dataPedido, dataUltima;
                dataPedido = new Date();
                dataUltima = new Date();

                long qtdeDiasPedido = -1, qtdeDiasUltima = -1;

                if (c.getString(c.getColumnIndex("DataUltimaVenda")) != null) {
                    dataPedido = sdf.parse(c.getString(c.getColumnIndex("DataUltimaVenda")));
                    qtdeDiasPedido = Utils.qtdeDiasPassou(dataPedido);
                }
                if (c.getString(c.getColumnIndex("DataUltima")) != null) {
                    dataUltima = sdf.parse(c.getString(c.getColumnIndex("DataUltima")));
                    qtdeDias = (int) Utils.qtdeDiasPassou(dataUltima);
                }

                produtoSemVenda = new ProdutoSemVenda();
                produtoSemVenda.setDescricao(c.getString(c.getColumnIndex("Descricao")));
                produtoSemVenda.setDescricaoEmbalagem(c.getString(c.getColumnIndex("DescricaoEmbalagem")));

                if (((qtdeDiasPedido == -1) && (qtdeDiasUltima == -1)) || ((qtdeDiasPedido != -1) && (qtdeDiasUltima == -1))) {
                    produtoSemVenda.setUltimaVenda(dataPedido);
                    produtoSemVenda.setQtdeDias((int) qtdeDiasPedido);
                } else if ((qtdeDiasPedido == -1) && (qtdeDiasUltima != -1)) {
                    produtoSemVenda.setUltimaVenda(dataUltima);
                    produtoSemVenda.setQtdeDias((int) qtdeDiasUltima);
                } else if ((qtdeDiasPedido != -1) && (qtdeDiasUltima != -1)) {
                    if (qtdeDiasPedido < qtdeDiasUltima) {
                        produtoSemVenda.setUltimaVenda(dataPedido);
                        produtoSemVenda.setQtdeDias((int) qtdeDiasPedido);
                    } else {
                        produtoSemVenda.setUltimaVenda(dataUltima);
                        produtoSemVenda.setQtdeDias((int) qtdeDiasUltima);
                    }
                }
                listaSemVenda.add(produtoSemVenda);
            }
        } catch (Exception e) {
            Log.e("ProdutoDAO: semVenda", e.getMessage());
        } finally {
            //db.close(); singleton();
            c.close();
        }

        return listaSemVenda;
    }

    public boolean existeFotos( SQLiteDatabase db) {
        boolean retorno = false;
        SQLiteDatabase dbLocal = null;

        if (db == null){
            dbLocal = new DBHelper(ctx).getReadableDatabase();

        } else{
            dbLocal = db;
        }

        Cursor c = dbLocal.rawQuery("SELECT COUNT (idProduto) FROM Produto WHERE PossuiFoto = 1", null);

        if (c.moveToFirst()) {
            retorno = c.getInt(0) > 0;
        }
        c.close();

        //if (db == null)
            //dbLocal.close(); singletone();

        return retorno;
    }

    public boolean existeProduto( SQLiteDatabase db, long id) {
        boolean retorno = false;
        SQLiteDatabase dbLocal = null;

        if (db == null){
            dbLocal = new DBHelper(ctx).getReadableDatabase();

        } else{
            dbLocal = db;
        }

        Cursor c = dbLocal.rawQuery("SELECT COUNT (*) FROM Produto WHERE IdProduto ="+ id, null);

        if (c.moveToFirst()) {
            retorno = c.getInt(0) > 0;
        }
        c.close();

        //if (db == null)
            //dbLocal.close(); singletone();

        return retorno;
    }

    public void updateProduto(SQLiteDatabase db, long idProduto) {
        SQLiteDatabase dbLocal = null;
        String sql = "UPDATE Produto SET PossuiFoto = 1 WHERE IdProduto =" + idProduto;
        boolean retorno = false;
        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        try {
            dbLocal.execSQL(sql) ;
        } catch (Exception e) {
            Log.e("PadraoDao", "update " + e.getMessage());
        }

        //if (db == null)
            //dbLocal.close(); singletone();
    }

    public void updatesProduto(SQLiteDatabase db, List<String> idProdutos) {
        SQLiteDatabase dbLocal = null;
        int size = idProdutos.size();
        int quantidade = 0;
        String sql ="";

        if (size > 0) {
            StringBuffer sqls = new StringBuffer();
            for (String idProduto : idProdutos) {
                quantidade++;

                if(idProduto.isEmpty())
                    continue;

                if (quantidade == size) {
                    sqls.append(""+idProduto+"");
                    break;
                } else
                    sqls.append(""+idProduto+", ");
            }

            sql = "UPDATE Produto SET PossuiFoto = 1 WHERE IdProduto in ( " + sqls + ")";


            if (db == null)
                dbLocal = new DBHelper(ctx).getWritableDatabase();
            else
                dbLocal = db;

            try {
                dbLocal.execSQL(sql);
            } catch (Exception e) {
                Log.e("PadraoDao", "update " + e.getMessage());
            }

            //if (db == null)
                //dbLocal.close(); singletone();
        }
    }

    public int contarProdutosComFoto() {

        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

        int quantidade = 0;

        Cursor c = db.rawQuery("select count(*) as quantidade from produto where possuifoto = 1", null);

        try {
            if (c.moveToFirst())
                quantidade = c.getInt(c.getColumnIndex("quantidade"));
        } catch (Exception e) {
            Log.e("ProdutoDAO:get.", e.getMessage());
        } finally {
            //db.close(); singleton();
            c.close();
        }

        return quantidade;

    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        importacaoNova(linha, db, new ParametroDAO(ctx, db).get(), true);
        return false;
    }


    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importacao_tabela_produto);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
