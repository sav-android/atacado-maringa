package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.modelo.Provedor;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class ProvedorDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Provedor" ;
	
	public ProvedorDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}

	public void importar (List<String> linhasTexto,boolean cargaCompleta) {
		try {
			if(cargaCompleta)
				deleteAll();

			getImportacaoProvedor(linhasTexto);
		} catch (Exception e ) {
			Log.d("ProvedoresDAO", e.getMessage()) ;
		}
	}

	private void getImportacaoProvedor(List<String> linhasTexto) {
		try {
			db.beginTransaction() ;

			for (String linha : linhasTexto) {
				if (getLinhaProvedorImportar(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction() ;
		}
	}

	private boolean getLinhaProvedorImportar(String linha) {
		Provedor p = new Provedor() ;

		if(linha.substring(0, 3).trim().matches(MATCHES) && !linha.substring(0, 3).trim().equals(""))
			p.setIdProvedor(Short.parseShort(linha.substring(0, 3).trim()));
		else {
			Utils.gravarLog("\nPRV: "+linha);
			return true;
		}
		p.setDescricao(linha.substring(3, 33).trim()) ;

		p.setUsuarioLogin(linha.substring(33, 83).trim());

		p.setSenhaLogin(linha.substring(83, 133).trim());

		p.setServidorPop(linha.substring(133, 183).trim()) ;

		p.setServidorSmtp(linha.substring(183, 233).trim()) ;

		if(linha.substring(233, 234).trim().matches(MATCHES) && !linha.substring(233, 234).trim().equals(""))
			p.setSmtpReqAutenticacao(Short.parseShort(linha.substring(233, 234).trim())> 0) ;
		else {
			Utils.gravarLog("\nPRV: "+linha);
			return true;
		}

		p.setEmailOrigem(linha.substring(234, 284).trim()) ;

		p.setUsuarioPop(linha.substring(284, 334).trim());

		p.setSenhaPop(linha.substring(334, 384).trim()) ;

		p.setEmailDestino(linha.substring(384, 434).trim());

		p.setNrConexao(linha.substring(434, 484).trim()) ;

		p.setParametrosIniciaisModem(linha.substring(484, 534).trim());

		if(exists(p))
			update(p);
		else
			insert(p);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaProvedorImportar(linha)) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return "Importando dados Provedor";
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
