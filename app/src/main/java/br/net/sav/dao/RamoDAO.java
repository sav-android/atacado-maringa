package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Ramo;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class RamoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Ramo" ;
	
	public RamoDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
		
	}
	
	
	public void importar (List<String> linhasTexto, Boolean cargaCompleta ) {
		try{		
			if(cargaCompleta)
				deleteAll();
			getImportacaoRamo(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("RamoDAO", e.getMessage());
		}
	}

	private void getImportacaoRamo(List<String> linhasTexto, Boolean cargaCompleta){
		try{
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaRamoImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaRamoImportar(Boolean cargaCompleta, String linha) {
		try {
			Ramo r = new Ramo() ;

			if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
				r.setIdRamo(Short.parseShort(linha.substring(0, 4).trim()));
			else {
				Utils.gravarLog("\nRAM: " +linha);
				return true;
			}

			r.setDescricao(linha.substring(4, 35).trim());

			if(!cargaCompleta) {
				switch(Character.toUpperCase(linha.charAt(35))) {
				case ' ' :
				case 'A' :
					if (exists(r))
						update(r);
					else
						insert(r);
					break;
				case 'E' :
					delete(r);
				default:
					Utils.gravarLog("\nRAM(Flag Inv�lida): " +linha);
					break;
				}
			} else {
				if (exists(r))
					update(r);
				else
					insert(r);

			}
		} catch (Exception e) {
			Log.e("RamoDAO", e.getMessage());
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaRamoImportar(true, linha)) return true;
		return false;
	}
	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_ramo_atividade);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
