package br.net.sav.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.modelo.Combo;
import br.net.sav.modelo.ComboXProduto;
import br.net.sav.modelo.RegraCombo;
import br.net.sav.modelo.RegraCompra;


public class RegraComboDAO {
    private final Context context;
    private static final String GANHE = "G";
    private static final String COMPRA = "C";

    public RegraComboDAO(Context context) {
        this.context = context;
    }

    public List<RegraCombo> buscarRegrasPor(long idCombo, SQLiteDatabase db){
        SQLiteDatabase dbLocal = instanciarBancoLocal(db);
        Cursor cursor = buscarRegraComboPor(idCombo, dbLocal);
        List<RegraCombo> regraCombosRetornados = instanciarArrayRegraCombosPor(cursor);
        //fecharBancoLocal(db,dbLocal);
        return regraCombosRetornados;
    }

    private List<RegraCombo> instanciarArrayRegraCombosPor(Cursor cursor) {
        List<RegraCombo> regraCombos = new ArrayList<>();
        while (cursor.moveToNext()){
            RegraCombo regraCombo = new RegraCombo(cursor);
            regraCombos.add(regraCombo);
        }
        return regraCombos;
    }

    private Cursor buscarRegraComboPor(long idCombo, SQLiteDatabase dbLocal) {

        return dbLocal.query(RegraCombo.TABELA, RegraCombo.campos, "IdCombo = ?", new String[]{String.valueOf(idCombo)}, null, null, null);
    }
    public String buscarRegraComboPorCaracter(long idCombo, String caracterRegra) {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Regra, TipoRegra from RegraCombo WHERE IdCombo = ? ", new String[]{String.valueOf(idCombo)});

        while (cursor.moveToNext()){
           String regra = cursor.getString(cursor.getColumnIndex("Regra"));
           String tipoRegra = cursor.getString(cursor.getColumnIndex("TipoRegra"));
            if (tipoRegra.equalsIgnoreCase("G") && caracterRegra.equals(GANHE)){
                return regra;
            }
            if (tipoRegra.equalsIgnoreCase("C") && caracterRegra.equals(COMPRA)){
                return regra;
            }
        }

        return "";
    }

    public List<RegraCombo> buscarTodasRegraDeCombo() {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
        Cursor cursor= null;
        List<RegraCombo> regrasCombo = new ArrayList<>();
        try {
             cursor = db.rawQuery("SELECT * from RegraCombo ", null);

            while (cursor.moveToNext()) {
                RegraCombo regra = new RegraCombo(cursor);
                regrasCombo.add(regra);
            }
        }finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return regrasCombo;
    }


    public boolean insert(RegraCombo regraCombo, SQLiteDatabase db) {
        SQLiteDatabase bancoLocal = instanciarBancoLocal(db);
        long insert = bancoLocal.insert(RegraCombo.TABELA, null, regraCombo.toContentValues());
       // fecharBancoLocal(db,bancoLocal);
        return insert>0;
    }
    public boolean existeRegra(RegraCombo regraCombo) {
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();

        boolean retorno = false;
        String idRegraCombo = String.valueOf(regraCombo.getId());

        Cursor c = db.rawQuery(String.format("Select * from RegraCombo where IdRegraCombo = ?"), new String[]{idRegraCombo});
        if (c.moveToFirst())
            retorno = c.getInt(0) > 0;

        c.close();
        //db.close(); singleton();

        return retorno;
    }

    public boolean update(RegraCombo regra, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(context).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.update(RegraCombo.TABELA, regra.toContentValues(),"IdRegraCombo=?", new String[]{String.valueOf(regra.getId())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    public boolean delete(RegraCombo regra, SQLiteDatabase db) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(context).getWritableDatabase();
        else
            dbLocal = db;

        boolean retorno = dbLocal.delete(RegraCombo.TABELA,"IdRegraCombo=?", new String[]{String.valueOf(regra.getId())}) > 0;

        // if (db == null)
        //dbLocal.close(); singletone();

        return retorno;
    }

    private void deleteAll() {
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        db.delete(RegraCombo.TABELA,null,null);
     //   db.close();
    }

    private SQLiteDatabase instanciarBancoLocal(SQLiteDatabase db) {
        return db==null?
                new DBHelper(context).getWritableDatabase()
                :db;
    }

    private void fecharBancoLocal(SQLiteDatabase db, SQLiteDatabase dbLocal) {
        if(db==null)
            dbLocal.close();
    }
}
