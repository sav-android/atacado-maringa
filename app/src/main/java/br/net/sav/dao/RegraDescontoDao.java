package br.net.sav.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import static br.net.sav.modelo.RegraDesconto.ID_DESCONTO;
import static br.net.sav.modelo.RegraDesconto.TABELA;
import static br.net.sav.modelo.RegraDesconto.campos;

import br.net.sav.modelo.RegraDesconto;

public class RegraDescontoDao {
    private final Context context;

    public RegraDescontoDao(Context context) {
        this.context = context;
    }

    public List<RegraDesconto> buscarRegrasPor(long idDesconto, SQLiteDatabase db){
        SQLiteDatabase dbLocal = instanciarBancoLocal(db);
        Cursor cursor = buscarRegraDescontoPor(idDesconto, dbLocal);
        List<RegraDesconto> regraDescontoRetornados = instanciarArrayRegraCombosPor(cursor);
        fecharBancoLocal(db,dbLocal);
        return regraDescontoRetornados;
    }

    private List<RegraDesconto> instanciarArrayRegraCombosPor(Cursor cursor) {
        List<RegraDesconto> regraDescontos = new ArrayList<>();
        while (cursor.moveToNext()){
            RegraDesconto regraDesconto = new RegraDesconto(cursor);
            regraDescontos.add(regraDesconto);
        }
        return regraDescontos;
    }

    private Cursor buscarRegraDescontoPor(long idDesconto, SQLiteDatabase dbLocal) {
        return dbLocal.query(TABELA, campos, ID_DESCONTO +" = ?", new String[]{String.valueOf(idDesconto)}, null, null, null);
    }

    private void apagarTodosSeCargaCompleta(Boolean cargaCompleta) {
        if (cargaCompleta)
            deleteAll();
    }

    private boolean insert(RegraDesconto regraDesconto, SQLiteDatabase db) {
        SQLiteDatabase bancoLocal = instanciarBancoLocal(db);
        long insert = bancoLocal.insert(TABELA, null, regraDesconto.toContentValues());
        fecharBancoLocal(db,bancoLocal);
        return insert>0;
    }

    private void deleteAll() {
        SQLiteDatabase db = new DBHelper(context).getWritableDatabase();
        db.delete(TABELA,null,null);
        db.close();
    }

    private SQLiteDatabase instanciarBancoLocal(SQLiteDatabase db) {
        return db==null?
                new DBHelper(context).getWritableDatabase()
                :db;
    }

    private void fecharBancoLocal(SQLiteDatabase db, SQLiteDatabase dbLocal) {
        if(db==null)
            dbLocal.close();
    }
}
