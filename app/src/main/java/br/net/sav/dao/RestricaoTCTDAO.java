package br.net.sav.dao;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.RestricaoTCT;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class RestricaoTCTDAO implements AcaoParaImportacaoDados {
    public static final String TABELA = "RestricaoTCT";
    protected static final String MATCHES = "^[0-9]*";
    private SQLiteDatabase db;
    private Context mContext;

    public RestricaoTCTDAO(Context mContext, SQLiteDatabase db) {
        this.mContext = mContext;
        this.db = db;
    }

    public void importar(List<String> linhasTexto) {
        try{
            deleteAll();

            getImportacaoRestricaoTCT(linhasTexto);
        } catch (Exception e) {
            Log.d("TabelaPrecoOperacaoDAO", e.getMessage());
        }
    }

    private void getImportacaoRestricaoTCT(List<String> linhasTexto) {
        try {
            db.beginTransaction();

            for (String linha : linhasTexto) {

                if (getLinhaRestricaoTCTImportar(linha)) continue;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private boolean getLinhaRestricaoTCTImportar(String linha) {
        RestricaoTCT restricaoTCT = new RestricaoTCT();

        if(linha.substring(0, 6).trim().matches(MATCHES) && !linha.substring(0, 6).trim().equals(""))
            restricaoTCT.setCodigoTipoPedido(Long.parseLong(linha.substring(0,6).trim()));
        else {
            Utils.gravarLog("\nRestricaoTCT: " +linha);
            return true;
        }
        if(linha.substring(6, 10).trim().matches(MATCHES) && !linha.substring(6, 10).trim().equals(""))
            restricaoTCT.setCodigoCondicaoPagamento(Long.parseLong(linha.substring(6, 10).trim()));
        else {
            Utils.gravarLog("\nRestricaoTCT: " +linha);
            return true;
        }

        if(linha.substring(10, 16).trim().matches(MATCHES) && !linha.substring(10, 16).trim().equals(""))
            restricaoTCT.setCodigoFormaPagamento(Long.parseLong(linha.substring(10,16).trim()));
        else {
            Utils.gravarLog("\nRestricaoTCT: " +linha);
            return true;
        }

        if (!exists(restricaoTCT))
            insert(restricaoTCT);
        return false;
    }

    public Boolean insert(RestricaoTCT tct) {
        SQLiteDatabase dbLocal = null;

        if (db == null) {
            dbLocal = new DBHelper(mContext).getWritableDatabase();
        } else {
            dbLocal = db;
        }

        boolean retorno = dbLocal.insert(TABELA, null, preencherContentValues(tct)) > 0;

        //if (db == null)
            //dbLocal.close(); singletone();

        return retorno;
    }

    private ContentValues preencherContentValues(RestricaoTCT tct) {
        ContentValues cv = new ContentValues();

        cv.put("IdTipoPedido", tct.getCodigoTipoPedido());
        cv.put("IdCondicaoPagamento", tct.getCodigoCondicaoPagamento());
        cv.put("IdFormaPagamento", tct.getCodigoFormaPagamento());

        return cv;
    }

    public boolean exists(RestricaoTCT tct) {
        SQLiteDatabase dbLocal = null;
        boolean retorno = false;
        Cursor c = null;
        String[] argumentos = new String[]{String.valueOf(tct.getCodigoTipoPedido()), String.valueOf(String.valueOf(tct.getCodigoCondicaoPagamento())), String.valueOf(tct.getCodigoFormaPagamento())};

        if (db == null) {
            dbLocal = new DBHelper(mContext).getWritableDatabase();
        } else {
            dbLocal = db;
        }

        try {
            c = dbLocal.rawQuery("SELECT COUNT(*) FROM RestricaoTCT WHERE IdTipoPedido = ? AND IdCondicaoPagamento = ? AND IdFormaPagamento = ?", argumentos);
            if (c.moveToFirst())
                retorno = c.getInt(0) > 0;
        } catch (Exception e) {
            Log.e("RestricaoTCT", ".exists(): " + TABELA + e.getMessage());
        } finally {
            if (c != null)
                c.close();
        }

        //if (db == null)
            //dbLocal.close(); singletone();

        return retorno;
    }

    public Boolean deleteAll() {
        SQLiteDatabase dbLocal = null;
        if (db == null) {
            dbLocal = new DBHelper(mContext).getWritableDatabase();
        } else {
            dbLocal = db;
        }

        boolean retorno = dbLocal.delete(TABELA, null, null) > 0;

        //if (db == null)
            //dbLocal.close(); singletone();

        return retorno;
    }

    public List<RestricaoTCT> getAll() {
        Cursor c = null;
        SQLiteDatabase dbLocal = null;
        List<RestricaoTCT> lista = new ArrayList<RestricaoTCT>();

        if(db == null) {
            dbLocal = new DBHelper(mContext).getWritableDatabase();
        } else {
            dbLocal = db;
        }

        try {
            c = dbLocal.query(TABELA, null,	null, null, null, null, null);
            while(c.moveToNext()) {
                lista.add(preencherObjeto(c));
            }
        } catch(Exception e) {
            Log.d("RestricaoTCTDAO", e.getMessage());
        } finally {
            c.close();
            //if(db == null) {
                //dbLocal.close(); singletone();
            //}
        }
        return lista;
    }

    private RestricaoTCT preencherObjeto(Cursor c) {
        RestricaoTCT tct = new RestricaoTCT();

        tct.setCodigoTipoPedido(c.getLong(c.getColumnIndex("IdTipoPedido")));
        tct.setCodigoCondicaoPagamento(c.getLong(c.getColumnIndex("IdCondicaoPagamento")));
        tct.setCodigoFormaPagamento(c.getLong(c.getColumnIndex("IdFormaPagamento")));

        return tct;
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        if (getLinhaRestricaoTCTImportar(linha)) return true;
        return false;
    }

    @Override
    public String getMensagemTabela() {
        return mContext.getString(R.string.importando_tabela_restricao_tipo_pedido_condicao_pagamento_forma_pagamento);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(mContext).getReadableDatabase();
        }else {
            return db;
        }
    }
}
