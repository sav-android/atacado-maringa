package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.PromocaoSemana;
import br.net.sav.modelo.SituacaoEspecial;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class SituacaoEspecialDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "SitEspecial" ;
	
	public SituacaoEspecialDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);	
	}
	
	public SituacaoEspecial get (long idProduto) {
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else 
			dbLocal = db ;
		
		SituacaoEspecial esp = null ;
		Cursor c = null ;
		
		try {
			c = dbLocal.query(TABELA, colunas, "IdProduto=?", new String[] {String.valueOf(idProduto)} , null, null, null);
			
			if(c.moveToFirst())
				esp = (SituacaoEspecial) preencherObjeto(c, esp);
			
		} catch (Exception e) {
			Log.e("SituacaoEspecialDAO:get(long idProduto)", e.getMessage());
		} finally {
			c.close();
			//if(db == null)
				//dbLocal.close(); singletone();
		}
				
		return esp ;
	}
	
	public List<PromocaoSemana> promocaoSemana() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		List<PromocaoSemana> listaPromocao = new ArrayList<PromocaoSemana>();
		
		StringBuilder sql = new StringBuilder() ;
		
		sql.append("select Sit.*, Pro.Descricao, Pro.Multiplo from SitEspecial Sit \n");
        sql.append("join Produto Pro on (Pro.IDProduto = Sit.IDProduto) \n");
        sql.append("where Sit.Tipo = 'P' \n");
        sql.append("order by Pro.Descricao\n");
        
        long quantidade = 999999 ;
		
		Cursor c = db.rawQuery(sql.toString(), null);
		PromocaoSemana promocaoSemana = null ;
		SituacaoEspecial situacaoEspecial = new SituacaoEspecial() ;
				
		try {
			while(c.moveToNext()) {
				situacaoEspecial.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
				situacaoEspecial.setIDFornecedor(c.getLong(c.getColumnIndex("IdFornecedor")));
				
				promocaoSemana = new PromocaoSemana() ;
				promocaoSemana.setIDProduto(situacaoEspecial.getIDFornecedor());
				promocaoSemana.setIDFornecedor(situacaoEspecial.getIDFornecedor());					
				
				situacaoEspecial.setQuantidade1(c.getLong(c.getColumnIndex("Quantidade1")));
				situacaoEspecial.setDescontoMaximo1(c.getDouble(c.getColumnIndex("DescontoMaximo1")));
				situacaoEspecial.setQuantidade2(c.getLong(c.getColumnIndex("Quantidade2")));
				situacaoEspecial.setDescontoMaximo2(c.getDouble(c.getColumnIndex("DescontoMaximo2")));
				situacaoEspecial.setQuantidade3(c.getLong(c.getColumnIndex("Quantidade3")));
				situacaoEspecial.setDescontoMaximo3(c.getDouble(c.getColumnIndex("DescontoMaximo3")));
				situacaoEspecial.setQuantidade4(c.getLong(c.getColumnIndex("Quantidade4")));
				situacaoEspecial.setDescontoMaximo4(c.getDouble(c.getColumnIndex("DescontoMaximo4")));
				situacaoEspecial.setQuantidade5(c.getLong(c.getColumnIndex("Quantidade5")));
				situacaoEspecial.setDescontoMaximo5(c.getDouble(c.getColumnIndex("DescontoMaximo5")));
				situacaoEspecial.setQuantidade6(c.getLong(c.getColumnIndex("Quantidade6")));
				situacaoEspecial.setDescontoMaximo6(c.getDouble(c.getColumnIndex("DescontoMaximo6")));
				situacaoEspecial.setQuantidade7(c.getLong(c.getColumnIndex("Quantidade7")));
				situacaoEspecial.setDescontoMaximo7(c.getDouble(c.getColumnIndex("DescontoMaximo7")));
				
				if((quantidade >= situacaoEspecial.getQuantidade7()) && (situacaoEspecial.getQuantidade7()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo7()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade6()) && (situacaoEspecial.getQuantidade6()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo6()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade5()) && (situacaoEspecial.getQuantidade5()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo5()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade4()) && (situacaoEspecial.getQuantidade4()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo4()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade3()) && (situacaoEspecial.getQuantidade3()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo3()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade2()) && (situacaoEspecial.getQuantidade2()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo2()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade1()) && (situacaoEspecial.getQuantidade1()>= 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo7()) ;
				
				promocaoSemana.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				promocaoSemana.setMultiplo(c.getShort(c.getColumnIndex("Multiplo")));
				
				listaPromocao.add(promocaoSemana) ;				
			}
		} catch (Exception e) {	
			Log.e("SituacaoEspecialDAO:promocaoSemana.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();	
		}
				
		return listaPromocao ;				
	}
	
	public List<PromocaoSemana> promocaoSemana(long idFornecedor) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		List<PromocaoSemana> listaPromocao = new ArrayList<PromocaoSemana>();
		
		StringBuilder sql = new StringBuilder() ;
		
		sql.append(" select Sit.*, Pro.Descricao, Pro.Multiplo from SitEspecial Sit \n");
        sql.append(" join Produto Pro on (Pro.IDProduto = Sit.IDProduto) \n");
        sql.append(" where Sit.Tipo = 'P' AND IdFornecedor= \n");
        sql.append(idFornecedor);
        sql.append(" order by Pro.Descricao\n");
        
        long quantidade = 999999 ;
		
		Cursor c = db.rawQuery(sql.toString(), null);
		PromocaoSemana promocaoSemana = null ;
		SituacaoEspecial situacaoEspecial = new SituacaoEspecial() ;
		
		try {
			while(c.moveToNext()) {
				situacaoEspecial.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
				situacaoEspecial.setIDFornecedor(c.getLong(c.getColumnIndex("IdFornecedor")));
				
				promocaoSemana = new PromocaoSemana() ;
				promocaoSemana.setIDProduto(situacaoEspecial.getIDFornecedor());
				promocaoSemana.setIDFornecedor(situacaoEspecial.getIDFornecedor());					
				
				situacaoEspecial.setQuantidade1(c.getLong(c.getColumnIndex("Quantidade1")));
				situacaoEspecial.setDescontoMaximo1(c.getDouble(c.getColumnIndex("DescontoMaximo1")));
				situacaoEspecial.setQuantidade2(c.getLong(c.getColumnIndex("Quantidade2")));
				situacaoEspecial.setDescontoMaximo2(c.getDouble(c.getColumnIndex("DescontoMaximo2")));
				situacaoEspecial.setQuantidade3(c.getLong(c.getColumnIndex("Quantidade3")));
				situacaoEspecial.setDescontoMaximo3(c.getDouble(c.getColumnIndex("DescontoMaximo3")));
				situacaoEspecial.setQuantidade4(c.getLong(c.getColumnIndex("Quantidade4")));
				situacaoEspecial.setDescontoMaximo4(c.getDouble(c.getColumnIndex("DescontoMaximo4")));
				situacaoEspecial.setQuantidade5(c.getLong(c.getColumnIndex("Quantidade5")));
				situacaoEspecial.setDescontoMaximo5(c.getDouble(c.getColumnIndex("DescontoMaximo5")));
				situacaoEspecial.setQuantidade6(c.getLong(c.getColumnIndex("Quantidade6")));
				situacaoEspecial.setDescontoMaximo6(c.getDouble(c.getColumnIndex("DescontoMaximo6")));
				situacaoEspecial.setQuantidade7(c.getLong(c.getColumnIndex("Quantidade7")));
				situacaoEspecial.setDescontoMaximo7(c.getDouble(c.getColumnIndex("DescontoMaximo7")));
				
				if((quantidade >= situacaoEspecial.getQuantidade7()) && (situacaoEspecial.getQuantidade7()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo7()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade6()) && (situacaoEspecial.getQuantidade6()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo6()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade5()) && (situacaoEspecial.getQuantidade5()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo5()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade4()) && (situacaoEspecial.getQuantidade4()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo4()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade3()) && (situacaoEspecial.getQuantidade3()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo3()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade2()) && (situacaoEspecial.getQuantidade2()> 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo2()) ;
				else if((quantidade >= situacaoEspecial.getQuantidade1()) && (situacaoEspecial.getQuantidade1()>= 0))
					promocaoSemana.setDesconto(situacaoEspecial.getDescontoMaximo7()) ;
				
				promocaoSemana.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				promocaoSemana.setMultiplo(c.getShort(c.getColumnIndex("Multiplo")));
				
				listaPromocao.add(promocaoSemana) ;				
			}
		} catch (Exception e) {
			Log.e("SituacaoEspecialDAO:promocaoSemana.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}				
		
		return listaPromocao ;				
	}
	
	public List<PromocaoSemana> listaFornecedores() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		List<PromocaoSemana> lista = new ArrayList<PromocaoSemana>();
		
		Cursor c = db.rawQuery("SELECT s.IdFornecedor,f.Descricao FROM SitEspecial s LEFT JOIN Fornecedor f ON (s.IdFornecedor = f.IdFornecedor)", null);
		
		try {
			while(c.moveToNext()) {							
				PromocaoSemana pro = new PromocaoSemana();
							
				pro.setIDFornecedor(c.getLong(c.getColumnIndex("IdFornecedor")));
				pro.setDescricao(c.getString(c.getColumnIndex("Descricao")));			
				
				lista.add(pro);
			}
		} catch (Exception e) {
			Log.e("SituacaoEspecialDAO:listaFornecedor", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}		
		
		return lista ;
	}
	
	public void importar (List<String> linhasTexto) {
		try {
			deleteAll();
			getImportacaoSituacaoEsp(linhasTexto);
		} catch (Exception e) {
			Log.e("SituacaoEspecialr.", e.getMessage());
		}
	}

	private void getImportacaoSituacaoEsp(List<String> linhasTexto)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaSituacaoEspImportar(linha)) continue;
			}
			db.setTransactionSuccessful();

		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaSituacaoEspImportar(String linha) {
		SituacaoEspecial esp = new SituacaoEspecial();

		if(linha.substring(0, 11).trim().matches(MATCHES) && !linha.substring(0, 11).trim().equals("")) {
			esp.setIDProduto(Long.parseLong(linha.substring(0, 11).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(11, 19).trim().matches(MATCHES) && !linha.substring(11, 19).trim().equals("")) {
			esp.setIDFornecedor(Long.parseLong(linha.substring(11, 19).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		esp.setTipo(linha.substring(19, 20).trim()) ;

		if(linha.substring(20, 26).trim().matches(MATCHES) && !linha.substring(20, 26).trim().equals("")){
			esp.setQuantidade1(Long.parseLong(linha.substring(20, 26).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(26, 31).trim().matches(MATCHES) && !linha.substring(26, 31).trim().equals("")){
			esp.setDescontoMaximo1(Double.parseDouble(linha.substring(26, 31).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(31, 37).trim().matches(MATCHES) && !linha.substring(31, 37).trim().equals("")){
			esp.setQuantidade2(Long.parseLong(linha.substring(31, 37).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(37, 42).trim().matches(MATCHES) && !linha.substring(37, 42).trim().equals("")){
			esp.setDescontoMaximo2(Double.parseDouble(linha.substring(37, 42).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(42, 48).trim().matches(MATCHES) && !linha.substring(42, 48).trim().equals("")){
			esp.setQuantidade3(Long.parseLong(linha.substring(42, 48).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(48, 53).trim().matches(MATCHES) && !linha.substring(48, 53).trim().equals("")){
			esp.setDescontoMaximo3(Double.parseDouble(linha.substring(48, 53).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(53, 59).trim().matches(MATCHES) && !linha.substring(53, 59).trim().equals("")){
			esp.setQuantidade4(Long.parseLong(linha.substring(53, 59).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(59,64).trim().matches(MATCHES) && !linha.substring(59,64).trim().equals("")){
			esp.setDescontoMaximo4(Double.parseDouble(linha.substring(59, 64).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(64,70).trim().matches(MATCHES) && !linha.substring(64,70).trim().equals("")){
			esp.setQuantidade5(Long.parseLong(linha.substring(64, 70).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(70,75).trim().matches(MATCHES) && !linha.substring(70,75).trim().equals("")){
			esp.setDescontoMaximo5(Double.parseDouble(linha.substring(70, 75).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(75,81).trim().matches(MATCHES) && !linha.substring(75,81).trim().equals("")){
			esp.setQuantidade6(Long.parseLong(linha.substring(75, 81).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(81,86).trim().matches(MATCHES) && !linha.substring(81,86).trim().equals("")){
			esp.setDescontoMaximo6(Double.parseDouble(linha.substring(81, 86).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(86,92).trim().matches(MATCHES) && !linha.substring(86,92).trim().equals("")){
			esp.setQuantidade7(Long.parseLong(linha.substring(86, 92).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(linha.substring(92,97).trim().matches(MATCHES) && !linha.substring(92,97).trim().equals("")){
			esp.setDescontoMaximo7(Double.parseDouble(linha.substring(92, 97).trim()));
		} else {
			Utils.gravarLog("\nESP: " + linha);
			return true;
		}

		if(!exists(esp))
			insert(esp);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaSituacaoEspImportar(linha)) return true;
		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_situacao_especial);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
