package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TabelaCliente;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TabelaClienteDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "TabelaCliente" ;
	
	public TabelaClienteDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}

	public void importar(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			if(cargaCompleta)
				deleteAll();

			getimportacaoTabelaCliente(linhasTexto, cargaCompleta);

		} catch (Exception e) {
			Log.e("TabelaClienteDAO:", e.getMessage()) ;
		}
	}

	private void getimportacaoTabelaCliente(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaTabelaClienteImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaTabelaClienteImportar(boolean cargaCompleta, String linha) {
		TabelaCliente tbc = new TabelaCliente();

		if(linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals("")) {
			tbc.setIdCliente(Long.parseLong(linha.substring(0, 8).trim()));
		} else {
			Utils.gravarLog("\nTBC: " + linha);
			return true;
		}

		if(linha.substring(8, 12).trim().matches(MATCHES) && !linha.substring(8, 12).trim().equals(""))	 {
			tbc.setIdTabela(Short.parseShort(linha.substring(8, 12).trim()));
		} else {
			Utils.gravarLog("\nTBC: " + linha);
			return true;
		}

		if(linha.substring(12, 16).trim().matches(MATCHES) && !linha.substring(12, 16).trim().equals("")) {
			tbc.setIdEmpresa(Short.parseShort(linha.substring(12, 16).trim()));
		} else {
			Utils.gravarLog("\nTBC: " + linha);
			return true;
		}

		if(linha.substring(16, 20).trim().matches(MATCHES) && !linha.substring(16, 20).trim().equals("")) {
			tbc.setIdFilial(Short.parseShort(linha.substring(16, 20).trim()));
		} else {
			Utils.gravarLog("\nTBC: " + linha);
			return true;
		}

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(20))) {
			case ' ':
			case 'A':
				if(exists(tbc)) {
					update(tbc);
				} else {
					insert(tbc);
				}
				break;

			case 'E':
				delete(tbc);

			default:
				if(!exists(tbc))
					insert(tbc);
				break;
			}
		} else {
			insert(tbc);
		}
		return false;
	}

	public TabelaCliente get(long IdCliente) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		String[] param = new String[]{String.valueOf(IdCliente)};

		TabelaCliente tbCliente = new TabelaCliente();

		Cursor c = db.query(TABELA, colunas, "IdCliente=?", param, null, null, null);

		try {
			if (c.moveToFirst())
				tbCliente = (TabelaCliente) preencherObjeto(c, tbCliente);
		} catch (Exception e) {
			Log.e("TabelaClienteDAO:get.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}

		return tbCliente;
	}

	public List<TabelaCliente> getAll(long idCliente) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		String[] param = new String[]{String.valueOf(idCliente)};

		StringBuilder sql = new StringBuilder();
		sql.append("select * from TabelaCliente where IdCliente =? ");

		Cursor c = db.rawQuery(sql.toString(), idCliente == 0 ? null: param);

		List<TabelaCliente> lstTabela = new ArrayList<TabelaCliente>();

		try {
			while(c.moveToNext()) {
				TabelaCliente t = new TabelaCliente();
//				t.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
//				t.setIdCliente(c.getShort(c.getColumnIndex("IdCliente")));

				t = (TabelaCliente) preencherObjeto(c, t);

				lstTabela.add(t);
			}
		} catch (Exception e) {
			Log.e("TabelaPrecoCliDAO.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}

		return lstTabela;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaTabelaClienteImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_precos_por_cliente);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
