package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TabelaClienteNovo;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TabelaClienteNovoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA="TabelaClienteNovoDAO";
	
	public TabelaClienteNovoDAO(Context ctx,SQLiteDatabase db){
		super(ctx,TABELA,db);	
	}
	
	public void importar(List<String> linhasTexto) {
		try {
			deleteAll();
			
			SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
			getImportacaoTabCliNovo(linhasTexto, db);

		} catch (Exception e) {
			Log.e("TabelaClienteNovoDAO:", e.getMessage());
		}
	}

	private void getImportacaoTabCliNovo(List<String> linhasTexto, SQLiteDatabase db)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaTabelaCliNovo(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaTabelaCliNovo(String linha) {
		TabelaClienteNovo tbn = new TabelaClienteNovo();

		if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals("")) {
			tbn.setIdTabela(Short.parseShort(linha.substring(0, 4).trim())) ;
		} else {
			Utils.gravarLog("\nTBN: "+ linha);
			return true;
		}

		if(linha.substring(4, 8).trim().matches(MATCHES) && !linha.substring(4, 8).trim().equals("")) {
			tbn.setIdEmpresa(Short.parseShort(linha.substring(4, 8).trim()));
		} else {
			Utils.gravarLog("\nTBN: "+ linha);
			return true;
		}

		if(linha.substring(8, 12).trim().matches(MATCHES) && !linha.substring(8, 12).trim().equals(""))	 {
			tbn.setIdFilial(Short.parseShort(linha.substring(8, 12).trim()));
		} else {
			Utils.gravarLog("\nTBN: "+ linha);
			return true;
		}

		if(!exists(tbn))
			insert(tbn);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaTabelaCliNovo(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_precos_para_clientes_novos);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
