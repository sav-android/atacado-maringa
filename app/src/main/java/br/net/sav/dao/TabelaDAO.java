package br.net.sav.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TabelaPrecoOperacaoTroca;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TabelaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "Tabela";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat sdfData = new SimpleDateFormat("yyyy-MM-dd");

    public TabelaDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public List<Tabela> getListaCliente(long idCliente) {
        SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

        StringBuilder sql = new StringBuilder();
        sql.append("select tab.*, ");
        sql.append(String.format("(select tabcli.IdCliente from TabelaCliente tabcli where IdCliente = %s limit 1) as Cliente ", idCliente));
        sql.append("from Tabela tab ");
        sql.append("where (tab.idTabela in ");
        sql.append(String.format("(select tabcli.IdTabela from TabelaCliente tabcli where tabcli.IdTabela = tab.IdTabela and tabcli.IdCliente = %d) or Cliente is null) ", idCliente));
        sql.append("order by tab.idTabela");

        Cursor c = db.rawQuery(sql.toString(), null);

        List<Tabela> lstTabela = new ArrayList<Tabela>(c.getCount());

        try {
            while (c.moveToNext()) {
                Tabela t = new Tabela();
                t.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
                t.setDescricao(c.getString(c.getColumnIndex("Descricao")));

                lstTabela.add(t);
            }
        } catch (Exception e) {
            Log.e("TabelaPrecoDAO:getListaCliente.", e.getMessage());
        } finally {
            c.close();
            //db.close(); singleton();
        }

        return lstTabela;
    }

    public Tabela get(short idTabela) {
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getReadableDatabase();
        else
            dbLocal = db;

        Tabela tab = null;
        Cursor c = null;

        try {

            c = dbLocal.query(TABELA, colunas, "IdTabela=?", new String[]{String.valueOf(idTabela)}, null, null, null);

            if (c.moveToFirst()) {
                tab = new Tabela();
                tab.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
                tab.setIdEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
                tab.setIdFilial(c.getShort(c.getColumnIndex("IdFilial")));
                tab.setDescricao(c.getString(c.getColumnIndex("Descricao")));
                tab.setDataInicial(sdfData.parse(c.getString(c.getColumnIndex("DataInicial"))));
                tab.setDataFinal(sdfData.parse(c.getString(c.getColumnIndex("DataFinal"))));
                tab.setPermiteDescontoPorTabela(c.getInt(c.getColumnIndex("PermiteDescontoPorTabela")) > 0);
                tab.setUltilizaRentabilidade(c.getInt(c.getColumnIndex("ultilizaRentabilidade")) > 0);
            }
        } catch (Exception e) {
            Log.e("TabelaDAO:get.", e.getMessage());
        } finally {
            c.close();
            //if(db == null)
            //db.close(); singletone();
        }

        return tab;
    }

    public List<ItemDigitacao> getListaItemDigitacao() {
        List<ItemDigitacao> listaDigitacaos = new ArrayList<ItemDigitacao>();
        SQLiteDatabase dbLocal = new DBHelper(ctx).getWritableDatabase();
        ItemDigitacao ig = new ItemDigitacao();

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * from produto  ");

        Cursor c = dbLocal.rawQuery(sql.toString(), null);

        while (c.moveToNext()) {
            ig = (ItemDigitacao) preencherObjeto(c, ig);
            listaDigitacaos.add(ig);
        }
        return listaDigitacaos;
    }

    public List<Tabela> getAll(short idTabelaPrecoOperacaoTroca, List<TabelaPrecoOperacaoTroca> listaTabelaPrecoOperacaoTroca) {
        new TabelaDAO(ctx, db);
        SQLiteDatabase dbLocal = null;
        List<Tabela> lista = new ArrayList<Tabela>();
        Cursor c = null;
        Tabela tabela = null;
        StringBuilder sql = new StringBuilder();
        String[] param = new String[]{String.valueOf(idTabelaPrecoOperacaoTroca)};

        if (db == null) {
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        } else {
            dbLocal = db;
        }

        try {
            if (idTabelaPrecoOperacaoTroca > 0) {
                sql.append(" SELECT tab.IdTabela, tab.IdEmpresa, tab.IdFilial, tab.Descricao, tab.DataInicial, tab.DataFinal, tab.PermiteDescontoPorTabela, tab.ultilizaRentabilidade FROM Tabela tab ");
                sql.append(" WHERE tab.IdTabela = ? ");
                sql.append(" ORDER BY Descricao ");
            } else if (listaTabelaPrecoOperacaoTroca != null && !listaTabelaPrecoOperacaoTroca.isEmpty()) {
                sql.append(" SELECT tab.IdTabela, tab.IdEmpresa, tab.IdFilial, tab.Descricao, tab.DataInicial, tab.DataFinal, tab.PermiteDescontoPorTabela, tab.ultilizaRentabilidade ");
                sql.append(" FROM Tabela tab ");
                sql.append(" JOIN TabelaPrecoPorOperacao tpo ON (tpo.IdTabela <> tab.IdTabela) ");
                sql.append(" ORDER BY Descricao ");
            } else {
                sql.append(" SELECT tab.IdTabela, tab.IdEmpresa, tab.IdFilial, tab.Descricao, tab.DataInicial, tab.DataFinal, tab.PermiteDescontoPorTabela, tab.ultilizaRentabilidade ");
                sql.append(" FROM Tabela tab ");
                sql.append(" ORDER BY Descricao ");
            }

            c = dbLocal.rawQuery(sql.toString(), idTabelaPrecoOperacaoTroca == 0 ? null : param);

            while (c.moveToNext()) {
                tabela = new Tabela();
                tabela.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
                tabela.setIdEmpresa(c.getShort(c.getColumnIndex("IdEmpresa")));
                tabela.setIdFilial(c.getShort(c.getColumnIndex("IdFilial")));
                tabela.setDescricao(c.getString(c.getColumnIndex("Descricao")));
                tabela.setDataInicial(sdfData.parse(c.getString(c.getColumnIndex("DataInicial"))));
                tabela.setDataFinal(sdfData.parse(c.getString(c.getColumnIndex("DataFinal"))));
                tabela.setPermiteDescontoPorTabela(c.getInt(c.getColumnIndex("PermiteDescontoPorTabela")) > 0);
                tabela.setUltilizaRentabilidade(c.getInt(c.getColumnIndex("ultilizaRentabilidade")) > 0);
                lista.add(tabela);
            }

        } catch (Exception e) {
            Log.e("TabelaDAO:getAll.", e.getMessage());
        } finally {
            c.close();
            //	if(db == null)
            //db.close(); singletone();
        }

        return lista;
    }

    public void importar(List<String> linhasTexto, boolean cargaCompleta) {
        try {
            if (cargaCompleta)
                deleteAll();
            getImportacaoTabelaPreco(linhasTexto);

        } catch (Exception e) {
            Log.d("TabelaDAO:importar.", e.getMessage());
        }
    }

    private void getImportacaoTabelaPreco(List<String> linhasTexto)  {
        try {
            db.beginTransaction();


            for (String linha : linhasTexto) {
                if (getLinhaTabelaPrecoImportar(linha)) continue;
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("TabelaDAO:importar.", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    private boolean getLinhaTabelaPrecoImportar(String linha) {
        Tabela tab = new Tabela();

        if (linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals("")) {
            tab.setIdTabela(Short.parseShort(linha.substring(0, 4).trim()));
        } else {
            Utils.gravarLog("\nTAB: " + linha);
            return true;
        }

        if (linha.substring(4, 8).trim().matches(MATCHES) && !linha.substring(4, 8).trim().equals("")) {
            tab.setIdEmpresa(Short.parseShort(linha.substring(4, 8).trim()));
        } else {
            Utils.gravarLog("\nTAB: " + linha);
            return true;
        }

        if (linha.substring(8, 12).trim().matches(MATCHES) && !linha.substring(8, 12).trim().equals("")) {
            tab.setIdFilial(Short.parseShort(linha.substring(8, 12).trim()));
        } else {
            Utils.gravarLog("\nTAB: " + linha);
            return true;
        }

        tab.setDataInicial(new Date());
        tab.setDataFinal(new Date());

        tab.setDescricao(linha.substring(12, 37).trim());

        if (linha.length() > 38) {
            if (linha.substring(38, 39).trim().equals("")) {
                tab.setPermiteDescontoPorTabela(linha.substring(38, 39).trim().equals("0"));
            } else if (linha.substring(38, 39).trim().equals("0") || linha.substring(38, 39).trim().equals("1")) {
                tab.setPermiteDescontoPorTabela(linha.substring(38, 39).equals("1"));
            } else {
                Utils.gravarLog("\nPRO: " + linha);
                return true;
            }
        }
        if (linha.length() > 39) {
            if (linha.substring(39, 40).trim().equals("")) {
                tab.setUltilizaRentabilidade(linha.substring(39, 40).trim().equals("0"));
            } else if (linha.substring(39, 40).trim().equals("0") || linha.substring(39, 40).trim().equals("1")) {
                tab.setUltilizaRentabilidade(linha.substring(39, 40).equals("1"));
            } else {
                Utils.gravarLog("\nPRO: " + linha);
                return true;
            }
        }


        switch (Character.toUpperCase(linha.charAt(37))) {
            case ' ':
            case 'A':
                if (exists(tab))
                    update(tab);
                else
                    insert(tab);
                break;

            case 'E':
                delete(tab);
                break;

            default:
                if (!exists(tab))
                    insert(tab);
                break;
        }

        return false;
    }

    public boolean exisValues(SQLiteDatabase db) {
        SQLiteDatabase dbLocal;
        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;
        return dbLocal.rawQuery("select IdTabela from Tabela ", null).moveToFirst();
    }

    public void importarVAL(List<String> linhasTexto) {
        try {
            try {
                db.beginTransaction();

                String linha = linhasTexto.get(0);
                Tabela t = new Tabela();

                if (linha.substring(0, 8).trim().matches(MATCHES) && !linha.substring(0, 8).trim().equals(""))
                    t.setDataInicial(sdf.parse(linha.substring(0, 8).trim()));
                else {
                    Utils.gravarLog("\nVAL: " + linha);
                    return;
                }
                if (linha.substring(8, 16).trim().matches(MATCHES) && !linha.substring(8, 16).trim().equals(""))
                    t.setDataFinal(sdf.parse(linha.substring(8, 16).trim()));
                else {
                    Utils.gravarLog("\nVAL: " + linha);
                    return;
                }

                String sql = String.format("update Tabela set DataInicial=\'%s\' , DataFinal=\'%s\' ;", sdfData.format(t.getDataInicial()), sdfData.format(t.getDataFinal()));
                Log.d("SQL", sql);
                db.execSQL(sql);

                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
                //db.close(); singleton() ;
            }
        } catch (Exception e) {
            Log.d("tabela PrecoDao:", e.getMessage());
        }
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        if (getLinhaTabelaPrecoImportar(linha)) return true;

        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_de_precos);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
