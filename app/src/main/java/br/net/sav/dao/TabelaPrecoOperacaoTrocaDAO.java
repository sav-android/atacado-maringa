package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TabelaPrecoOperacaoTroca;
import br.net.sav.modelo.TipoPedido;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TabelaPrecoOperacaoTrocaDAO implements AcaoParaImportacaoDados {
	public static final String TABELA = "TabelaPrecoPorOperacao";
	protected static final String MATCHES = "^[0-9]*";
	private Context ctx;
	private SQLiteDatabase db;
	
	public TabelaPrecoOperacaoTrocaDAO(Context ctx, SQLiteDatabase db) {
		this.ctx = ctx;
		this.db = db;
	}

	public Boolean insert(TabelaPrecoOperacaoTroca tpo) {
		SQLiteDatabase dbLocal = null;

		if (db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		
		boolean retorno = dbLocal.insert(TABELA, null, preencherContentValues(tpo)) > 0;

	//	if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}
	
	public boolean exists(TabelaPrecoOperacaoTroca tpo) {
		SQLiteDatabase dbLocal = null;
		boolean retorno = false;
		Cursor c = null;
		String[] argumentos = new String[]{String.valueOf(tpo.getTabela().getIdTabela()), String.valueOf(String.valueOf(tpo.getTipoPedido().getIdTipoPedido()))};

		if (db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		
		try {
			c = dbLocal.rawQuery("SELECT COUNT(*) FROM TabelaPrecoPorOperacao WHERE IdTabela = ? AND IdTipoPedido = ?", argumentos);
			if (c.moveToFirst())
				retorno = c.getInt(0) > 0;
		} catch (Exception e) {
			Log.e("TabelaPrecoOperacaoDAO", ".exists(): " + TABELA + e.getMessage());
		} finally {
			if (c != null)
				c.close();
		}

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}
	
	public Boolean deleteAll() {
		SQLiteDatabase dbLocal = null;
		if (db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		
		boolean retorno = dbLocal.delete(TABELA, null, null) > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}
	
	public boolean isOperacaoTroca(TipoPedido tp) {
		SQLiteDatabase dbLocal = null;
		Cursor c = null;
		String[] param = new String[]{String.valueOf(tp.getIdTipoPedido())};
		
		if(db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		
		try {
			c = dbLocal.rawQuery(" SELECT * FROM TabelaPrecoPorOperacao tpo WHERE tpo.IdTipoPedido = ? ", param);
			if(c.moveToFirst()) {
				return c.getInt(0) > 0;
			}
		} catch(Exception e) {
			Log.d("TabelaPrecoOperacaoDAO" + ".isOperacaoTroca(): ", e.getMessage());
		} finally {
			c.close();
			//if(db == null) {
				//db.close(); singletone();
			//}
		}
		return false;
	}
	
	private ContentValues preencherContentValues(TabelaPrecoOperacaoTroca tabelaPrecoOperacao) {
		ContentValues cv = new ContentValues();
		
		cv.put("IdTabela", tabelaPrecoOperacao.getTabela().getIdTabela());
		cv.put("IdTipoPedido", tabelaPrecoOperacao.getTipoPedido().getIdTipoPedido());
		
		return cv;
	}
	
	public void importar(List<String> linhasTexto) {
		try{
			deleteAll();

			getimportacaoTabelaOpeTroca(linhasTexto);
		} catch (Exception e) {
			Log.d("TabelaPrecoOperacaoDAO", e.getMessage());
		}
	}

	private void getimportacaoTabelaOpeTroca(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getTabelaOpeTrocaImportar(linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getTabelaOpeTrocaImportar(String linha) {
		TabelaPrecoOperacaoTroca tpo = new TabelaPrecoOperacaoTroca() ;

		if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
			tpo.getTabela().setIdTabela(Short.parseShort(linha.substring(0,4).trim()));
		else {
			Utils.gravarLog("\nTPO: " +linha);
			return true;
		}
		if(linha.substring(4, 10).trim().matches(MATCHES) && !linha.substring(4, 10).trim().equals(""))
			tpo.getTipoPedido().setIdTipoPedido(Short.parseShort(linha.substring(4, 10).trim()));
		else {
			Utils.gravarLog("\nTPO: " +linha);
			return true;
		}

		if (!exists(tpo))
			insert(tpo);
		return false;
	}

	public List<TabelaPrecoOperacaoTroca> getAll() {
		Cursor c = null;
		SQLiteDatabase dbLocal = null;
		List<TabelaPrecoOperacaoTroca> lista = new ArrayList<TabelaPrecoOperacaoTroca>();
		
		if(db == null) {
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		} else {
			dbLocal = db;
		}
		
		try {
			c = dbLocal.query(TABELA, null,	null, null, null, null, null);
			while(c.moveToNext()) {
				lista.add(preencherObjeto(c));
			}
		} catch(Exception e) {
			Log.d("TabelaPrecoOperacaoDAO", e.getMessage());
		} finally {
			c.close();
			//if(db == null) {
				//dbLocal.close(); singletone();
			//}
		}
		return lista;
	}
	
	private TabelaPrecoOperacaoTroca preencherObjeto(Cursor c) {
		TabelaPrecoOperacaoTroca tpo = new TabelaPrecoOperacaoTroca();
		
		tpo.getTabela().setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
		tpo.getTipoPedido().setIdTipoPedido(c.getShort(c.getColumnIndex("IdTipoPedido")));
		
		return tpo;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getTabelaOpeTrocaImportar(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_preco_operacao);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
