package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TipoPedido;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TipoPedidoDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "TipoPedido";

	public TipoPedidoDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
	}

	public List<TipoPedido> getAll() {
		new TipoPedidoDAO(ctx, db);
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<TipoPedido> lista = new ArrayList<TipoPedido>();

		Cursor c = null;

		TipoPedido tipoPedido = null;

		try {

			c = dbLocal.query(TABELA, colunas, null, null, null, null, "Descricao");

			while (c.moveToNext()) {
				tipoPedido = new TipoPedido();
				tipoPedido.setIdTipoPedido(c.getShort(c.getColumnIndex("IdTipoPedido")));
				tipoPedido.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				tipoPedido.setAbaterSaldoVerba(c.getShort(c.getColumnIndex("AbaterSaldoVerba")));
				tipoPedido.setValorMinimoPedido(c.getDouble(c.getColumnIndex("ValorMinimoPedido")));

				if (c.getString(c.getColumnIndex("TipoOperacao"))!= null)
					tipoPedido.setTipoOperacao(c.getString(c.getColumnIndex("TipoOperacao")));
				else
					tipoPedido.setTipoOperacao(br.net.sav.enumerador.TipoOperacao.VENDA.toString());

				lista.add(tipoPedido);
			}

		} catch (Exception e) {
			Log.e("TipoPedidoDAO:getAll.", e.getMessage());
		} finally {
			c.close();
	//		if (db == null)
				//dbLocal.close(); singletone();
		}

		return lista;
	}

	public void importar(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			if (cargaCompleta)
				deleteAll();
			getImportacaoTipoPedido(linhasTexto, cargaCompleta);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("TipoPedidoDAO:importar.", e.getMessage());
		}
	}

	private void getImportacaoTipoPedido(List<String> linhasTexto, boolean cargaCompleta) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaTipoPedidoImportar(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaTipoPedidoImportar(boolean cargaCompleta, String linha) {
		TipoPedido tpe = new TipoPedido();

		if (linha.substring(0, 6).trim().matches(MATCHES) && !linha.substring(0, 6).trim().equals("")) {
			tpe.setIdTipoPedido(Short.parseShort(linha.substring(0, 6).trim()));
		} else {
			Utils.gravarLog("\nTPE: " + linha);
			return true;
		}

		tpe.setDescricao(linha.substring(6, 26).trim());

		if (linha.substring(26, 27).trim().matches(MATCHES) && !linha.substring(26, 27).trim().equals("")) {
			tpe.setAbaterSaldoVerba(Short.parseShort(linha.substring(26, 27).trim()));
		} else {
			Utils.gravarLog("\nTPE: " + linha);
			return true;
		}

		if (linha.substring(28, 40).trim().matches(MATCHES) && !linha.substring(28, 40).trim().equals("")) {
			tpe.setValorMinimoPedido(Double.parseDouble(linha.substring(28, 40).trim())/100);
		} else {
			Utils.gravarLog("\nTPE: " + linha);
			return true;
		}
		int length = linha.length();

		if (linha.length() > 40) {
			if (linha.substring(40, 41).trim().length() > 0)
				tpe.setTipoOperacao(linha.substring(40, 41).trim());
			else {
				Utils.gravarLog("\nPAR: " + linha);
				return true;
			}
		}

		if (!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(27))) {
			case ' ':
			case 'A':
				if (exists(tpe))
					update(tpe);
				else
					insert(tpe);
				break;

			case 'E':
				delete(tpe);
				break;

			default:
				if (!exists(tpe))
					insert(tpe);
				break;
			}
		} else {

			insert(tpe);
		}
		return false;
	}

	public List<TipoPedido> BuscarTodasBonificacoes(String  tipoOperacao) {
		ArrayList<TipoPedido> lista = new ArrayList<TipoPedido>();

		SQLiteDatabase	dbLocal = new DBHelper(ctx).getReadableDatabase();
		Cursor cursor = null;

		try {
			cursor = dbLocal.query(TABELA, null, "TipoOperacao=?", new String[]{tipoOperacao}, null, null, null);

			TipoPedido tipoPedido = null;

			while (cursor.moveToNext()) {
				tipoPedido = new TipoPedido();
				lista.add((TipoPedido) preencherObjeto(cursor, tipoPedido));
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName() + ":listarTodos()", e.getMessage());
		} finally {
			cursor.close();
			//db.close(); singletone();
		}

		return lista;
	}

	public TipoPedido get(short IdTipoPedido) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();

		String[] param = new String[] { String.valueOf(IdTipoPedido) };

		TipoPedido tipo = null;

		Cursor c = db.query(TABELA, colunas, "IdTipoPedido=?", param, null, null, "IdTipoPedido");

		try {
			if (c.moveToFirst()) {
				tipo = new TipoPedido();
				tipo.setIdTipoPedido(c.getShort(c.getColumnIndex("IdTipoPedido")));
				tipo.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				tipo.setAbaterSaldoVerba(c.getShort(c.getColumnIndex("AbaterSaldoVerba")));
				tipo.setValorMinimoPedido(c.getDouble(c.getColumnIndex("ValorMinimoPedido")));
				if (c.getString(c.getColumnIndex("TipoOperacao"))!= null)
					tipo.setTipoOperacao(c.getString(c.getColumnIndex("TipoOperacao")));
				else
					tipo.setTipoOperacao(br.net.sav.enumerador.TipoOperacao.VENDA.toString());
			}
		} catch (Exception e) {
			Log.d("TipoPedidoDAO:get.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}

		return tipo;
	}

	public List<TipoPedido> getByCliente(Long idCliente) {
		new TipoPedidoDAO(ctx, db);
		SQLiteDatabase dbLocal = null;

		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		List<TipoPedido> lista = new ArrayList<TipoPedido>();

		Cursor c = null;

		TipoPedido tipoPedido = null;

		try {
			c = dbLocal.rawQuery(
					"SELECT tp.*" +
							"  FROM TipoPedido tp" +
							"       LEFT JOIN" +
							"       TipoPedidoTipoContribuinte tc ON tp.IdTipoPedido = tc.CodTipoPedido AND " +
							"                                        tc.CodTipoContribuinte = (" +
							"                                                                     SELECT cl.CodigoContribuinte" +
							"                                                                       FROM Cliente cl" +
							"                                                                      WHERE cl.IdCliente = "+idCliente +
							"                                                                 )" +
							" WHERE tc.CodTipoPedido IS NULL;",
					null);

			while (c.moveToNext()) {
				tipoPedido = new TipoPedido();
				tipoPedido.setIdTipoPedido(c.getShort(c.getColumnIndex("IdTipoPedido")));
				tipoPedido.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				tipoPedido.setAbaterSaldoVerba(c.getShort(c.getColumnIndex("AbaterSaldoVerba")));
				tipoPedido.setValorMinimoPedido(c.getDouble(c.getColumnIndex("ValorMinimoPedido")));

				if (c.getString(c.getColumnIndex("TipoOperacao"))!= null)
					tipoPedido.setTipoOperacao(c.getString(c.getColumnIndex("TipoOperacao")));
				else
					tipoPedido.setTipoOperacao(br.net.sav.enumerador.TipoOperacao.VENDA.toString());

				lista.add(tipoPedido);
			}

		} catch (Exception e) {
			Log.e("TipoPedidoDAO:getAll.", e.getMessage());
		} finally {
			c.close();
	//		if (db == null)
				//dbLocal.close(); singletone();
		}

		return lista;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaTipoPedidoImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importacao_tipo_pedido);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
