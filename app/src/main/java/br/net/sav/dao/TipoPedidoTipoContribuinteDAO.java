package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TipoPedidoTipoContribuinte;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TipoPedidoTipoContribuinteDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "TipoPedidoTipoContribuinte";

    public TipoPedidoTipoContribuinteDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public List<TipoPedidoTipoContribuinte> getAll() {
        new TipoPedidoTipoContribuinteDAO(ctx, db);
        SQLiteDatabase dbLocal = null;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        List<TipoPedidoTipoContribuinte> lista = new ArrayList<TipoPedidoTipoContribuinte>();

        Cursor c = null;

        TipoPedidoTipoContribuinte tipoPedido = null;

        try {

            c = dbLocal.query(TABELA, colunas, null, null, null, null, null);

            while (c.moveToNext()) {
                tipoPedido = new TipoPedidoTipoContribuinte();
                tipoPedido.setCodTipoContribuinte(c.getInt(c.getColumnIndex("CodTipoContribuinte")));
                tipoPedido.setCodTipoPedido(c.getInt(c.getColumnIndex("CodTipoPedido")));
                lista.add(tipoPedido);
            }

        } catch (Exception e) {
            Log.e("TipoContribuinteDAO: ", e.getMessage());
        } finally {
            c.close();
         //   if (db == null)
                //dbLocal.close(); singletone();
        }

        return lista;
    }

    public void importar(List<String> linhasTexto) {
        try {
            deleteAll();
            getImportacaoTipoContribuinte(linhasTexto);

        } catch (Exception e) {
            Log.d("ERRO TTC","Erro na importa��o.");
            e.printStackTrace();
            Log.e("ipoContribuinteDAO: ", e.getMessage());
        }
    }

    private void getImportacaoTipoContribuinte(List<String> linhasTexto)  {
        try {
            db.beginTransaction();

            for (String linha : linhasTexto) {
                if (getlinhaTipoContribuinteImportar(linha)) continue;
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private boolean getlinhaTipoContribuinteImportar(String linha) {
        TipoPedidoTipoContribuinte tpe = new TipoPedidoTipoContribuinte();

        if (linha.substring(0, 6).trim().matches(MATCHES) && !linha.substring(0, 6).trim().equals("")) {
            tpe.setCodTipoPedido(Integer.parseInt(linha.substring(0, 6).trim()));
        } else {
            Utils.gravarLog("\nTPE: " + linha);
            return true;
        }

        if (linha.substring(6, 7).trim().matches(MATCHES) && !linha.substring(6, 7).trim().equals("")) {
            tpe.setCodTipoContribuinte(Integer.parseInt(linha.substring(6, 7).trim()));
        } else {
            Utils.gravarLog("\nTPE: " + linha);
            return true;
        }

        insert(tpe);
        return false;
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        if (getlinhaTipoContribuinteImportar(linha)) return true;

        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_restricoes_tipo_pedido_tipo_contribuintes);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
