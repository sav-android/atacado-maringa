package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.TipoTroca;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class TipoTrocaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA ="TipoTroca"; 
	
	public TipoTrocaDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);		
	}
	
	public List<TipoTroca> getAll() {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		Cursor c = db.query(TABELA, colunas, null, null, null, null, null);
		
		List<TipoTroca> lista = new ArrayList<TipoTroca>(c.getCount());
		
		try {
			while(c.moveToNext()) {
				TipoTroca tipo = new TipoTroca();
				tipo.setIdTipoTroca(c.getShort(c.getColumnIndex("IdTipoTroca")));
				tipo.setDescricao(c.getString(c.getColumnIndex("Descricao")));
				lista.add(tipo);
			}
		} catch (Exception e) {
			Log.e("TipoTrocaDAO:getAll.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}		
		
		return lista ;
	}
	
	public void importar (List<String> linhasTexto, boolean cargaCompleta) {
		try {
			if(cargaCompleta)
				deleteAll();
			getImportacaoTipoTroca(linhasTexto, cargaCompleta);

		} catch (Exception e) {
			Log.e("TipoTroca:importar.", e.getMessage());
		}
	}

	private void getImportacaoTipoTroca(List<String> linhasTexto, boolean cargaCompleta)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getlinhaTipoTrocaImportar(cargaCompleta, linha)) continue;
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getlinhaTipoTrocaImportar(boolean cargaCompleta, String linha) {
		TipoTroca ttr = new TipoTroca() ;

		if(linha.substring(0, 2).trim().matches(MATCHES) && !linha.substring(0, 2).trim().equals("")) {
			ttr.setIdTipoTroca(Short.parseShort(linha.substring(0, 2).trim()));
		} else {
			Utils.gravarLog("\nTTR: " + linha);
			return true;
		}

		ttr.setDescricao(linha.substring(2, 22).trim()) ;

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(22))) {
			case ' ':
			case 'A':
				if(exists(ttr))
					update(ttr);
				else
					insert(ttr);
				break;

			case 'E':
				delete(ttr);
				break ;

			default:
				if(!exists(ttr))
					insert(ttr);
				break;
			}
		} else {
			insert(ttr);
		}
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getlinhaTipoTrocaImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tipo_troca);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
