package br.net.sav.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.ItemTroca;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Troca;
import br.net.sav.util.StringUtils;

public class TrocaDAO {
	public final short TODAS = 0;
	public final short NAO_ENVIADAS = 1;
	public final short ENVIADAS = 2;
	
	private Context ctx ;
	
	private static final String TABELA = "Troca" ;
	
	private static final String[] colunas = new String[] {"IdTroca", "IdCliente", "Data", "IdTabela", "IdCondicao", "TipoTroca",
		"NFOrigem", "ValorMercadoriaBruto", "ValorMercadoriaLiquido", "Indenizacao", "ValorLiquidoTroca", "NumeroItens",
		"NrPedidoVenda", "Observacao", "Enviado","DataEnvio"} ;
	
	public TrocaDAO(Context ctx) {
		this.ctx = ctx ;
	}
	
	public Boolean insert ( Troca troca, SQLiteDatabase db ) {			
		SQLiteDatabase dbLocal = null ;
		
		if( db == null )
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else 
			dbLocal = db ;
		
		ContentValues ctv = new ContentValues();
						
		ctv.put("IdTroca", troca.getIdTroca());
		ctv.put("IdCliente", troca.getIdCliente());
		ctv.put("Data", Utils.sdfDataDb.format(troca.getData()));
		ctv.put("IdTabela", troca.getIdTabela());
		ctv.put("IdCondicao", troca.getIdCondicao());
		ctv.put("TipoTroca", troca.getTipoTroca());
		ctv.put("NFOrigem", troca.getNfOrigem());
		ctv.put("ValorMercadoriaBruto", troca.getValorMercadoriaBruto());
		ctv.put("ValorMercadoriaLiquido", troca.getValorMercadoriaLiquido());
		ctv.put("Indenizacao", troca.getIndenizacao());
		ctv.put("ValorLiquidoTroca", troca.getValorLiquidoTroca());
		ctv.put("NumeroItens", troca.getNumeroItens());
		ctv.put("NrPedidoVenda", troca.getNrPedidoVenda());
		ctv.put("Observacao", troca.getObservacao());
		ctv.put("Enviado", troca.isEnviado());
		ctv.put("DataEnvio", Utils.sdfDataHoraDb.format(troca.getDataEnvio()));		
													
		boolean retorno = dbLocal.insert(TABELA, null, ctv) > 0 ;
		
	//	if( db == null )
			//dbLocal.close(); singletone();
		
		return retorno ;
	}
	
	public Boolean delete(long idTroca, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		String[] param = new String[] { String.valueOf(idTroca) };
		
		boolean retorno = dbLocal.delete(TABELA, "IdTroca=?", param) > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}
	
	public Boolean deleteAll() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		boolean retorno = db.delete(TABELA, null, null) > 0 ;
		
		//db.close(); singleton();
		
		return retorno ;
	}
	
	public Boolean update( Troca troca, SQLiteDatabase db) {		
		SQLiteDatabase dbLocal = null;
		
		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		ContentValues ctv = new ContentValues();
		
		ctv.put("IdCliente", troca.getIdCliente());
		ctv.put("Data", Utils.sdfDataDb.format(troca.getData()));
		ctv.put("IdTabela", troca.getIdTabela());
		ctv.put("IdCondicao", troca.getIdCondicao());
		ctv.put("TipoTroca", troca.getTipoTroca());
		ctv.put("NFOrigem", troca.getNfOrigem());
		ctv.put("ValorMercadoriaBruto", troca.getValorMercadoriaBruto());
		ctv.put("ValorMercadoriaLiquido", troca.getValorMercadoriaLiquido());
		ctv.put("Indenizacao", troca.getIndenizacao());
		ctv.put("ValorLiquidoTroca", troca.getValorLiquidoTroca());
		ctv.put("NumeroItens", troca.getNumeroItens());
		ctv.put("NrPedidoVenda", troca.getNrPedidoVenda());
		ctv.put("Observacao", troca.getObservacao());
		ctv.put("Enviado", troca.isEnviado());
		ctv.put("DataEnvio", Utils.sdfDataHoraDb.format(troca.getDataEnvio()));
		
		String[] param = new String[] { String.valueOf(troca.getIdTroca()) };

		boolean retorno = dbLocal.update(TABELA, ctv, "IdTroca=?", param) > 0;

		//if (db == null)
			//dbLocal.close(); singletone();

		return retorno;
	}		
	
	public Troca get(long IdTroca) throws ParseException {		
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		Troca troca = new Troca();
		Cursor c = db.query(TABELA, colunas, "IdTroca=?", new String[] {String.valueOf(IdTroca)}, null, null, null);
		
		try {
			if(c.moveToFirst()) {
				troca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
				troca.setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
				troca.setData(Utils.sdfDataDb.parse(c.getString(c.getColumnIndex("Data"))));
				troca.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
				troca.setIdCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
				troca.setTipoTroca(c.getShort(c.getColumnIndex("TipoTroca")));
				troca.setNfOrigem(c.getLong(c.getColumnIndex("NFOrigem")));
				troca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
				troca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
				troca.setIndenizacao(c.getDouble(c.getColumnIndex("Indenizacao")));
				troca.setValorLiquidoTroca(c.getDouble(c.getColumnIndex("ValorLiquidoTroca")));
				troca.setNumeroItens(c.getShort(c.getColumnIndex("NumeroItens")));
				troca.setNrPedidoVenda(c.getLong(c.getColumnIndex("NrPedidoVenda")));
				troca.setObservacao(c.getString(c.getColumnIndex("Observacao")));
				troca.setEnviado(c.getInt(c.getColumnIndex("Enviado")) >0);
				troca.setDataEnvio(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataEnvio"))));
			}
		} catch (Exception e) {
			Log.e("TrocaDAO:get.", e.getMessage());
		} finally {
			c.close();
			//db.close(); singleton();
		}		
		
		return troca ;
	}
	
	public List<Troca>  getAll() throws ParseException {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		List<Troca> lista = new ArrayList<Troca>();
		
		Cursor c = db.query(TABELA, colunas, null, null, null, null, null);
		
		try {
			while(c.moveToNext()) {
				Troca troca = new Troca();
				troca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
				troca.setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
				troca.setData(Utils.sdfDataDb.parse(c.getString(c.getColumnIndex("Data"))));
				troca.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
				troca.setIdCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
				troca.setTipoTroca(c.getShort(c.getColumnIndex("TipoTroca")));
				troca.setNfOrigem(c.getLong(c.getColumnIndex("NFOrigem")));
				troca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
				troca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
				troca.setIndenizacao(c.getDouble(c.getColumnIndex("Indenizacao")));
				troca.setValorLiquidoTroca(c.getDouble(c.getColumnIndex("ValorLiquidoTroca")));
				troca.setNumeroItens(c.getShort(c.getColumnIndex("NumeroItens")));
				troca.setNrPedidoVenda(c.getLong(c.getColumnIndex("NrPedidoVenda")));
				troca.setObservacao(c.getString(c.getColumnIndex("Observacao")));
				troca.setEnviado(c.getInt(c.getColumnIndex("Enviado")) >0);
				troca.setDataEnvio(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataEnvio"))));
				
				lista.add(troca);
			}
		} catch (Exception e) {
			Log.e("TrocaDAO:getAll.", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}
				
		return lista ;
	}
	
	public Troca getByPedidoVenda(long idPedidoVenda) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		Troca troca = null;
		Cursor c = db.query(TABELA, colunas, "NrPedidoVenda=?", new String[] {String.valueOf(idPedidoVenda)}, null, null, null);
		if (c.moveToFirst()) {
			troca = preencherObjeto(c);
		}
		c.close();
		//db.close(); singleton();
		
		return troca;
	}
	
	public Troca preencherObjeto(Cursor c) {
		Troca troca = new Troca();
		try {			
			troca.setIdTroca(c.getLong(c.getColumnIndex("IdTroca")));
			troca.setIdCliente(c.getLong(c.getColumnIndex("IdCliente")));
			troca.setData(Utils.sdfDataDb.parse(c.getString(c.getColumnIndex("Data"))));
			troca.setIdTabela(c.getShort(c.getColumnIndex("IdTabela")));
			troca.setIdCondicao(c.getShort(c.getColumnIndex("IdCondicao")));
			troca.setTipoTroca(c.getShort(c.getColumnIndex("TipoTroca")));
			troca.setNfOrigem(c.getLong(c.getColumnIndex("NFOrigem")));
			troca.setValorMercadoriaBruto(c.getDouble(c.getColumnIndex("ValorMercadoriaBruto")));
			troca.setValorMercadoriaLiquido(c.getDouble(c.getColumnIndex("ValorMercadoriaLiquido")));
			troca.setIndenizacao(c.getDouble(c.getColumnIndex("Indenizacao")));
			troca.setValorLiquidoTroca(c.getDouble(c.getColumnIndex("ValorLiquidoTroca")));
			troca.setNumeroItens(c.getShort(c.getColumnIndex("NumeroItens")));
			troca.setNrPedidoVenda(c.getLong(c.getColumnIndex("NrPedidoVenda")));
			troca.setObservacao(c.getString(c.getColumnIndex("Observacao")));
			troca.setEnviado(c.getInt(c.getColumnIndex("Enviado")) >0);
			troca.setDataEnvio(Utils.sdfDataHoraDb.parse(c.getString(c.getColumnIndex("DataEnvio"))));
			
		} catch (Exception e) {
			Log.d("TrocaDAO.preencherObjeto", e.getMessage());
		}
		
		return troca ;
	}
	
	public ArrayList<Troca> listaTrocas(short quais, long idCliente) {
		ArrayList<Troca> lista = new ArrayList<Troca>();
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		StringBuilder sql = new StringBuilder();
		sql.append("Select * from Troca ");
		if (quais == NAO_ENVIADAS)
			sql.append("where Enviado=0 ");
		else if (quais == ENVIADAS)
			sql.append("where Enviado=1 ");
		
		if (idCliente > 0) {
			if (sql.toString().contains("where"))
				sql.append(String.format("and IdCliente=%d ", idCliente));
			else
				sql.append(String.format("where IdCliente=%d ", idCliente));
		}
		sql.append("order by Data");
		
		Cursor c = db.rawQuery(sql.toString(), null);
		while (c.moveToNext()) {
			lista.add(preencherObjeto(c));
		}
		c.close();
		//db.close(); singleton();
		
		return lista;
	}
	
	public boolean existeByPedido(long idPedido) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		boolean retorno = false;
		
		Cursor c = db.rawQuery(String.format("Select Count(NrPedidoVenda) from Troca where NrPedidoVenda=%d", idPedido), null);
		if (c.moveToFirst())
			retorno = c.getInt(0) > 0;
			
		c.close();
		//db.close(); singleton();
		
		return retorno;
	}
	
	public void gerarTexto(long idVendedor) {
		List<Troca> listaExportar = listaTrocas(NAO_ENVIADAS, 0) ;
		if (listaExportar.size() == 0)
			return;
		
		try {
			ItemTrocaDAO itemTrocaDAO = new ItemTrocaDAO(ctx);			
			Date data = new Date();
			String campo, valorAux;
			
			String nomeArquivoTroca = String.format("tro%08d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));
			String nomeArquivoItens = String.format("itr%08d%s.txt", idVendedor, Utils.sdfNomeArquivoExportacao.format(data));
			
            FileOutputStream outTroca = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/envia", nomeArquivoTroca), false);
            FileOutputStream outIte = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/envia", nomeArquivoItens), false);
                        
            for (Troca troca : listaExportar) {
            	campo = String.format("%09d", troca.getIdCliente());
				outTroca.write(campo.getBytes());
            	
				campo = String.format("%06d%04d", troca.getIdTroca(), idVendedor);
				outTroca.write(campo.getBytes());								
				
				campo = Utils.sdfDataExport.format(troca.getData());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%02d", troca.getIdTabela());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%02d", troca.getIdCondicao());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%02d", troca.getTipoTroca());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%10d", troca.getNfOrigem());
				outTroca.write(campo.getBytes());
				
				valorAux = String.format("%s", Utils.arredondar(troca.getValorMercadoriaBruto(), 2) * 100);
				valorAux = valorAux.substring(0, valorAux.indexOf("."));
				campo = String.format("%012d", Integer.parseInt(valorAux));
				outTroca.write(campo.getBytes());
				
				valorAux = String.format("%s", Utils.arredondar(troca.getValorMercadoriaLiquido(), 2) * 100);
				valorAux = valorAux.substring(0, valorAux.indexOf("."));
				campo = String.format("%012d", Integer.parseInt(valorAux));
				outTroca.write(campo.getBytes());
				
				valorAux = String.format("%s", Utils.arredondar(troca.getIndenizacao(), 2) * 100);
				valorAux = valorAux.substring(0, valorAux.indexOf("."));
				campo = String.format("%05d", Integer.parseInt(valorAux));
				outTroca.write(campo.getBytes());
				
				valorAux = String.format("%s", Utils.arredondar(troca.getValorLiquidoTroca(), 2) * 100);
				valorAux = valorAux.substring(0, valorAux.indexOf("."));
				campo = String.format("%012d", Integer.parseInt(valorAux));
				outTroca.write(campo.getBytes());
				
				campo = String.format("%03d", troca.getNumeroItens());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%06d", troca.getNrPedidoVenda());
				outTroca.write(campo.getBytes());
				
				campo = String.format("%-240s", troca.getObservacao());
				outTroca.write(campo.getBytes());
				
				campo = StringUtils.QUEBRA_LINHA;
				outTroca.write(campo.getBytes());
				
				ArrayList<ItemTroca> itensTroca = itemTrocaDAO.getByTroca(troca.getIdTroca());
				for (ItemTroca item : itensTroca) {
					campo = String.format("%06d%04d", item.getIdTroca(), idVendedor);
					outIte.write(campo.getBytes());
					
					campo = String.format("%011d", item.getIdProduto());
					outIte.write(campo.getBytes());
					
					campo = String.format("%03d", item.getNumeroItem());
					outIte.write(campo.getBytes());
					
					campo = item.isUnitario() ? "1" : "0";
					outIte.write(campo.getBytes());
					
					campo = String.format("%04d", item.getQuantidade());
					outIte.write(campo.getBytes());
					
					valorAux = String.format("%s", Utils.arredondar(item.getValorUnitario(), 2) * 100);
					valorAux = valorAux.substring(0, valorAux.indexOf("."));
					campo = String.format("%012d", Integer.parseInt(valorAux));
					outIte.write(campo.getBytes());
					
					valorAux = String.format("%s", Utils.arredondar(item.getValorMercadoriaBruto(), 2) * 100);
					valorAux = valorAux.substring(0, valorAux.indexOf("."));
					campo = String.format("%012d", Integer.parseInt(valorAux));
					outIte.write(campo.getBytes());
					
					valorAux = String.format("%s", Utils.arredondar(item.getDesconto(), 2) * 100);
					valorAux = valorAux.substring(0, valorAux.indexOf("."));
					campo = String.format("%05d", Integer.parseInt(valorAux));
					outIte.write(campo.getBytes());
					
					valorAux = String.format("%s", Utils.arredondar(item.getValorDesconto(), 2) * 100);
					valorAux = valorAux.substring(0, valorAux.indexOf("."));
					campo = String.format("%012d", Integer.parseInt(valorAux));
					outIte.write(campo.getBytes());
					
					valorAux = String.format("%s", Utils.arredondar(item.getValorMercadoriaLiquido(), 2) * 100);
					valorAux = valorAux.substring(0, valorAux.indexOf("."));
					campo = String.format("%012d", Integer.parseInt(valorAux));
					outIte.write(campo.getBytes());
					
					campo = StringUtils.QUEBRA_LINHA;
					outIte.write(campo.getBytes());
				}	
            }	
            outTroca.flush();
            outTroca.close();
            
            outIte.flush();
            outIte.close();
		} catch (Exception e) {
			Log.d("TrocaDAO.gerarTextos", e.getMessage());
		} 
	}
	
	public void marcarEnviado(Date dataEnvio) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		String sql = String.format("UPDATE Troca SET Enviado = 1, DataEnvio = \'%s\' WHERE Enviado = 0", Utils.sdfDataHoraDb.format(dataEnvio)); 
		db.execSQL(sql);
		//db.close(); singleton();
	}
	
	public void apagarAposPrazo() {
		try {
			Parametro parametro = new ParametroDAO(ctx, null).get();
			if(parametro == null)
				return ;
			SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, parametro.getQtdeDiasGuardarPedido() * -1);
			Date dataApagar = cal.getTime();
			
			List<Long> listaTrocasApagar = new ArrayList<Long>();
			Cursor c = db.query(TABELA, new String[]{"IdTroca"}, "Enviado=1 and Data<=?", new String[] { Utils.sdfDataDb.format(dataApagar) + " 00:00:00"}, null, null, null);
			while (c.moveToNext()) {
				listaTrocasApagar.add(c.getLong(0));
			}
			c.close();
			
			for (Long idPedidoApagar : listaTrocasApagar) {
				if(listaTrocasApagar.size() == 0)
					break ;
				
				String[] param = new String[] { String.valueOf(idPedidoApagar) };
				if (db.delete(TABELA, "IdTroca=?", param) > 0)
					db.delete("ItemTroca", "IdTroca=?", param);
			}
			
			//db.close(); singleton();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void marcarRetransmitir(List<Date> listaDatas) {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		
		ContentValues ctv = new ContentValues();
		ctv.put("Enviado", false);
		
		for (Date date : listaDatas) {
			db.update(TABELA, ctv, "DataEnvio=?", new String[] { Utils.sdfDataHoraDb.format(date) });
		}
		//db.close(); singleton();
	}
	
	public long getLastId() {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		long retorno = 0;
		Cursor c = db.rawQuery("Select Max(IdTroca) from Troca", null);
		if (c.moveToFirst())
			retorno = c.getLong(0);
		c.close();
		//db.close(); singleton();
		
		return retorno;
	}
	
	public Troca get(long idTroca, SQLiteDatabase db) {
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db;

		Troca troca = null;
		Cursor c = dbLocal.query(TABELA, colunas, "IdTroca=?", new String[] {String.valueOf(idTroca)}, null, null, null);
		if (c.moveToFirst())
			troca = preencherObjeto(c);
		c.close();
		
		//if (db == null)
			//dblocal.close(); singletone();
		
		return troca;
	}
	
	public Boolean existeTrocaParaPedido(long idPedido) {
		boolean retorno = false;
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		Cursor c = db.rawQuery("Select Count(IdTroca) from Troca where NrPedidoVenda=?", new String[] {String.valueOf(idPedido)});
		if (c.moveToFirst())
			retorno = c.getInt(0) > 0;
		c.close();
		//db.close(); singleton();
		
		return retorno;
	}
	
	public boolean salvarTroca(Troca troca, List<ItemTroca> itens) {
		boolean retorno = false;

		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		db.beginTransaction();
		try {
			Troca trocaAux = get(troca.getIdTroca(), db);
			
			if (trocaAux == null) {
				if (insert(troca, db)) {
					if (new ItemTrocaDAO(ctx).insert(itens, db)) {
						db.setTransactionSuccessful();
						retorno = true;
					}
				}
			}
			else {
				ItemTrocaDAO itemTrocaDAO = new ItemTrocaDAO(ctx);

				if (itemTrocaDAO.delete(troca.getIdTroca(), db)) {
					if (update(troca, db)) {
						if (itemTrocaDAO.insert(itens, db)) {
							db.setTransactionSuccessful();
							retorno = true;
						}
					}
				}
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}

		//db.close(); singleton();
		return retorno;
	}
	
	public Boolean excluirTroca(Troca troca) {
		boolean retorno = false;
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();
		db.beginTransaction();
		try {
			if (delete(troca.getIdTroca(), db)) {
				if (new ItemTrocaDAO(ctx).delete(troca.getIdTroca(), db)) {
					db.setTransactionSuccessful();
					retorno = true;
				}
			}		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		
		//db.close(); singleton();
		
		return retorno;
	}
}
