package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Ultima;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class UltimaDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "Ultima";
    private SimpleDateFormat sdfImport = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


    public UltimaDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public List<Ultima> getLista(Long IdCliente) {
        SQLiteDatabase dbLocal;
        List<Ultima> lista = new ArrayList<Ultima>();

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        Cursor c = null;

        try {
            c = dbLocal.rawQuery("SELECT * from Ultima where IdCliente=?", new String[]{String.valueOf(IdCliente)});

            Ultima u = null;

            while (c.moveToNext()) {
                u = new Ultima();
                u.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
                u.setIDPedidoPalmtop(c.getLong(c.getColumnIndex("IdPedidoPalmtop")));
                u.setDataUltimaCompra(sdf.parse(c.getString(c.getColumnIndex("Data"))));
                u.setTotalUltimaCompra(c.getDouble(c.getColumnIndex("TotalUltimaCompra")));
                u.setTotal(c.getDouble(c.getColumnIndex("Total")));
                u.setStatusPedido(c.getString(c.getColumnIndex("StatusPedido")));
                u.setNumeroNotaFiscal(c.getString(c.getColumnIndex("NumeroNotaFiscal")));
                u.setDataFaturamento(sdf.parse(c.getString(c.getColumnIndex("DataFaturamento"))));

                lista.add(u);
            }
        } catch (Exception e) {
            Log.e("UltimaDAO:getLista.", e.getMessage());
        } finally {
            c.close();
        }

        //if(db == null)
        //dbLocal.close(); singletone();

        return lista;
    }

    public void importar(List<String> linhasTexto) {
        try {
            deleteAll();
            new UltimaDAO(ctx, db).deleteAll();
            getImportacaoUltimas(linhasTexto);

        } catch (Exception e) {
            Log.e("UltimaDAO:importar.", e.getMessage());
        }
    }

    private void getImportacaoUltimas(List<String> linhasTexto)  throws ParseException {
        try {
            db.beginTransaction();

            List<Long> listaIdPedidos = new ArrayList<Long>();
            List<Long> listaIdCliente = new ClienteDAO(ctx, db).listaIdClientes();

            for (String linha : linhasTexto) {
                getlinhaUltimaImportar(listaIdCliente, listaIdPedidos, linha);
            }

            db.execSQL("Create Index If Not Exists UltimaItemProdutoData on UltimaItem (IDProduto, Data)");

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void getlinhaUltimaImportar(List<Long> listaIdCliente, List<Long> listaIdPedidos, String linha) throws ParseException {
        String numeroNF = "0";
        double totalFaturado = 0;
        String status = "ABERTO";
        Ultima ult = new Ultima();
        if (!linha.isEmpty()) {
            if (linha.substring(37, 45).trim().matches(MATCHES)
                    && !linha.substring(37, 45).trim().equals(""))
                ult.setIDCliente(Long.parseLong(linha.substring(37, 45).trim()));
            else
                Utils.gravarLog("\nPUL: " + linha);

            if (listaIdCliente.contains(ult.getIDCliente())) {

                if (linha.substring(0, 12).trim().matches(MATCHES)
                        && !linha.substring(0, 12).trim().equals(""))
                    ult.setIDPedido(Long.parseLong(linha.substring(0, 12).trim()));
                else
                    Utils.gravarLog("\nPUL: " + linha);

                if (linha.substring(12, 37).trim().matches(MATCHES)
                        && !linha.substring(12, 37).trim().equals(""))
                    ult.setIDPedidoPalmtop(Long.parseLong(linha.substring(12, 29).trim()));
                else
                    Utils.gravarLog("\nPUL: " + linha);

                if (!linha.substring(45, 53).trim().equals(""))
                    ult.setDataUltimaCompra(sdfImport.parse(linha.substring(45, 53).trim()));
                else {
                    ult.setDataUltimaCompra(sdfImport.parse("00000000"));
                    Utils.gravarLog("\nPUL: " + linha);
                }

                if (!linha.substring(53, 61).trim().equals(""))
                    ult.setDataFaturamento(sdfImport.parse(linha.substring(53, 61).trim()));
                else {
                    ult.setDataFaturamento(sdfImport.parse("00000000"));
                    Utils.gravarLog("\nPUL: " + linha);
                }

                if (linha.substring(61, 71).trim().matches(MATCHES)
                        && !linha.substring(61, 71).trim().equals(""))
                    ult.setTotalUltimaCompra(Double.parseDouble(linha.substring(61, 71).trim()) / 100);
                else {
                    Utils.gravarLog("\nPUL: " + linha);
                }

                if (linha.substring(71, 81).trim().matches(MATCHES)
                        && !linha.substring(71, 81).trim().equals(""))
                    ult.setTotal(Double.parseDouble(linha.substring(71, 81).trim()) / 100);
                else
                    Utils.gravarLog("\nPUL: " + linha);

                if (!linha.substring(81, 131).trim().equals(""))
                    ult.setStatusPedido(linha.substring(81, 131).trim());
                else
                    Utils.gravarLog("\nPUL: " + linha);

                if (linha.length() > 131) {
                    if (!linha.substring(131, 181).trim().equals(""))
                        ult.setNumeroNotaFiscal(linha.substring(131, 181).trim());
                    else
                        Utils.gravarLog("\nPUL: " + linha);
                } else {
                    ult.setNumeroNotaFiscal("0");
                }

                if (ult.getIDPedidoPalmtop() != 0
                        && new PedidoDAO(ctx, null).get(ult.getIDPedidoPalmtop(), db) != null) {
                    SimpleDateFormat sdfDataHoraDb = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss");
                    sdfDataHoraDb.format(ult.getDataFaturamento());

                    if (!ult.getStatusPedido().trim().isEmpty()
                            && !ult.getNumeroNotaFiscal().trim().isEmpty()) {
                        status = ult.getStatusPedido();
                        numeroNF = ult.getNumeroNotaFiscal();
                        totalFaturado = ult.getTotal();

                    }

                    try {
                        db.execSQL("UPDATE Pedido SET " +
                                "RetornoStatusPedido = '" + status + "' " +
                                ", DataFaturamento = '" + sdfDataHoraDb.format(ult.getDataFaturamento()) + "' " +
                                ", TotalFaturado = " + totalFaturado +
                                ", statusFaturado = '" + status + "' " +
                                ", RetornoNumeroNotaFiscal = '" + numeroNF + "' " +
                                "WHERE IdPedido = " + ult.getIDPedidoPalmtop());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (listaIdPedidos.indexOf(ult.getIDPedido()) == -1) {
                    listaIdPedidos.add(ult.getIDPedido());
                    if (this.exists(ult))
                        this.update(ult);
                    else
                        this.insert(ult);
                }
            }
        }
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        List<Long> listaIdPedidos = new ArrayList<Long>();
        List<Long> listaIdCliente = new ClienteDAO(ctx, db).listaIdClientes();

        try {
            getlinhaUltimaImportar(listaIdCliente, listaIdPedidos, linha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_ultimas_compras);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
