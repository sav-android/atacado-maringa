package br.net.sav.dao;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.UltimaItem;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class UltimaItemDAO extends PadraoDAO implements AcaoParaImportacaoDados {
    private static final String TABELA = "UltimaItem";

    public UltimaItemDAO(Context ctx, SQLiteDatabase db) {
        super(ctx, TABELA, db);
    }

    public List<UltimaItem> getLista(long IdPedido) {
        List<UltimaItem> lista = new ArrayList<UltimaItem>();
        SQLiteDatabase dbLocal;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        Cursor c = null;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.*,p.Descricao FROM UltimaItem u");
        sql.append(" LEFT JOIN Produto p on  (p.IdProduto = u.IdProduto) WHERE IdPedido=?");

        try {
            c = dbLocal.rawQuery(sql.toString(), new String[]{String.valueOf(IdPedido)});

            while (c.moveToNext()) {
                UltimaItem ui = new UltimaItem();

                ui.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
                ui.setIdPedidoPalmtop(c.getLong(c.getColumnIndex("IdPedidoPalmtop")));
                ui.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
                ui.setQtde(c.getDouble(c.getColumnIndex("Qtde")));
                ui.setQtdeCortada(c.getDouble(c.getColumnIndex("QtdeCortada")));
                ui.setTotalItem(c.getDouble(c.getColumnIndex("TotalItem")));
                ui.setDescontoItem(c.getDouble(c.getColumnIndex("DescontoItem")));
                ui.setValorTotalItemFaturado(c.getDouble(c.getColumnIndex("ValorTotalItemFaturado")));
                ui.setDescontoItemFaturado(c.getDouble(c.getColumnIndex("DescontoItemFaturado")));
                ui.setStatusItem(c.getString(c.getColumnIndex("StatusItem")));

                ui.setDescricao(c.getString(c.getColumnIndex("Descricao")));
                ui.setEstoqueDia(c.getLong(c.getColumnIndex("EstoqueDia")));

                lista.add(ui);
            }
        } catch (Exception e) {
            Log.e("UltimaItemDAO:getLista.", e.getMessage());
        } finally {
            c.close();
        }

        return lista;
    }

    public List<UltimaItem> getListaItemUtimaCompra(long IdPedido) {
        List<UltimaItem> lista = new ArrayList<UltimaItem>();
        SQLiteDatabase dbLocal;

        if (db == null)
            dbLocal = new DBHelper(ctx).getWritableDatabase();
        else
            dbLocal = db;

        Cursor c = null;

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM UltimaItem WHERE IdPedidoPalmtop LIKE ?");

        try {
            c = dbLocal.rawQuery(sql.toString(), new String[]{String.valueOf(IdPedido)+"%"});

            while (c.moveToNext()) {
                UltimaItem ui = new UltimaItem();

                ui.setIDPedido(c.getLong(c.getColumnIndex("IdPedido")));
                ui.setIdPedidoPalmtop(c.getLong(c.getColumnIndex("IdPedidoPalmtop")));
                ui.setIDProduto(c.getLong(c.getColumnIndex("IdProduto")));
                ui.setQtde(c.getDouble(c.getColumnIndex("Qtde")));
                ui.setQtdeCortada(c.getDouble(c.getColumnIndex("QtdeCortada")));
                ui.setTotalItem(c.getDouble(c.getColumnIndex("TotalItem")));
                ui.setDescontoItem(c.getDouble(c.getColumnIndex("DescontoItem")));
                ui.setValorTotalItemFaturado(c.getDouble(c.getColumnIndex("ValorTotalItemFaturado")));
                ui.setDescontoItemFaturado(c.getDouble(c.getColumnIndex("DescontoItemFaturado")));
                ui.setStatusItem(c.getString(c.getColumnIndex("StatusItem")));
                ui.setEstoqueDia(c.getLong(c.getColumnIndex("EstoqueDia")));

                lista.add(ui);
            }
        } catch (Exception e) {
            Log.e("UltimaItemDAO:getLista.", e.getMessage());
        } finally {
            c.close();
        }

        return lista;
    }

    public void importar(List<String> linhasTexto) {
        try {
            deleteAll();
            new UltimaItemDAO(ctx, db).deleteAll();
            getImportacaoItemUltimas(linhasTexto);

        } catch (Exception e) {
            Log.e("UltimaItemDAO:importar.", e.getMessage());
        }
    }

    private void getImportacaoItemUltimas(List<String> linhasTexto) {
        try {
            db.beginTransactionNonExclusive();

            for (String linha : linhasTexto) {
              getLinhaItemUltimasImportar(linha);
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.e("UltimaDAO Importar", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    private void getLinhaItemUltimasImportar(String linha) {
        String status = "";
        int qntdCortada = 0;
        double descontoFaturado = 0;
        double totalFaturadoItem = 0;
        UltimaItem ult = new UltimaItem();
        if (!linha.isEmpty()) {
            if (linha.substring(0, 12).trim().matches(MATCHES) && !linha.substring(0, 12).trim().equals(""))
                ult.setIDPedido(Long.parseLong(linha.substring(0, 12).trim()));
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(12, 37).trim().matches(MATCHES) && !linha.substring(12, 37).trim().equals(""))
                ult.setIdPedidoPalmtop(Long.parseLong(linha.substring(12, 29).trim()));
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(37, 48).trim().matches(MATCHES) && !linha.substring(37, 48).trim().equals("")) {
                ult.setIDProduto(Long.parseLong(linha.substring(37, 46).trim()));
                String idProduto = linha.substring(37, 46).trim();
                String idEmbalagem = linha.substring(47, 48).trim();
                ult.setIDProduto(Long.parseLong(idProduto+idEmbalagem));

            } else
                Utils.gravarLog("\nULT(Item): " + linha);


            if (linha.substring(48, 54).trim().matches(MATCHES) && !linha.substring(48, 54).trim().equals(""))
                ult.setQtde(Long.parseLong(linha.substring(48, 54).trim()));
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(54, 60).trim().matches(MATCHES) && !linha.substring(54, 60).trim().equals(""))
                ult.setQtdeCortada(Long.parseLong(linha.substring(54, 60).trim()));
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(60, 70).trim().matches(MATCHES) && !linha.substring(60, 70).trim().equals(""))
                ult.setTotalItem(Double.parseDouble(linha.substring(60, 70).trim()) / 100);
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(70, 75).trim().matches(MATCHES) && !linha.substring(70, 75).trim().equals(""))
                ult.setDescontoItem(Double.parseDouble(linha.substring(70, 75).trim()) / 100);
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(75, 85).trim().matches(MATCHES) && !linha.substring(75, 85).trim().equals(""))
                ult.setValorTotalItemFaturado(Double.parseDouble(linha.substring(75, 85).trim()) / 100);
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (linha.substring(85, 90).trim().matches(MATCHES) && !linha.substring(85, 90).trim().equals(""))
                ult.setDescontoItemFaturado(Double.parseDouble(linha.substring(85, 90).trim()) / 100);
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (!linha.substring(90, 140).trim().equals(""))
                ult.setStatusItem(linha.substring(90, 140).trim());
            else
                Utils.gravarLog("\nULT(Item): " + linha);

            if (ult.getIdPedidoPalmtop() != 0
                    && new ItemDAO(ctx, null).listaByPedido(ult.getIdPedidoPalmtop()) != null) {

                if (!ult.getStatusItem().trim().isEmpty()) {
                    status = ult.getStatusItem();
                    qntdCortada = (int) ult.getQtdeCortada();
                    descontoFaturado = ult.getDescontoItemFaturado();
                    totalFaturadoItem = ult.getValorTotalItemFaturado();

                }

                try {
                    db.execSQL("UPDATE Item SET " +
                            "status = '" + status + "' " +
                            ",descontoFaturado = " + descontoFaturado +
                            " ,qntdCortada = " + qntdCortada +
                            " ,totalFaturadoItem = " + totalFaturadoItem +
                            " WHERE IdPedido = " + ult.getIdPedidoPalmtop() +" AND IdProduto = "+ult.getIDProduto());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (!exists(ult))
                insert(ult);
        }
    }

    @Override
    public String getTabela() {
        return TABELA;
    }

    @Override
    public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
        getLinhaItemUltimasImportar(linha);
        return false;
    }

    @Override
    public String getMensagemTabela() {
        return ctx.getString(R.string.importando_tabela_itens_ultimas_compras);
    }

    @Override
    public SQLiteDatabase getDB() {
        if (db == null){
            return new DBHelper(ctx).getReadableDatabase();
        }else {
            return db;
        }
    }
}
