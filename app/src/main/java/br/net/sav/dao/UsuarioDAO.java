package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Usuario;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class UsuarioDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "Usuario" ;	
	
	public UsuarioDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
			
	public Usuario get (short idUsuario) {
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		Usuario usuario = null ;
		
		String[] param = new String[] {String.valueOf(idUsuario)} ;
		
		Cursor c = dbLocal.rawQuery("SELECT * FROM Usuario WHERE IDUsuario=?", param);
		
		if(c.moveToFirst())
			usuario = (Usuario) preencherObjeto(c, new Usuario());
		
		c.close();
		
		//if(db == null)
			//dbLocal.close(); singletone();
		
		return usuario ;
	}
	
	public Usuario getByVendedor(short IDVendedor) {
		SQLiteDatabase db = new DBHelper(ctx).getReadableDatabase();
		
		String[] param = new String[] {String.valueOf(IDVendedor)};
		
		Usuario usuario = null;
		Cursor c = db.query(TABELA, colunas, "IDVendedor=?", param, null, null, null);
		
		if (c.moveToFirst()) {
			usuario = new Usuario();
			usuario.setIDUsuario(c.getShort(c.getColumnIndex("IDUsuario")));
			usuario.setNome(c.getString(c.getColumnIndex("Nome")));
			usuario.setIDVendedor(c.getShort(c.getColumnIndex("IDVendedor")));
			usuario.setSenha(c.getString(c.getColumnIndex("Senha")));
		}
		c.close();
		//db.close(); singleton();
		
		return usuario;
	}			
	
	public List<Usuario> getAll(boolean menosUsuarioDoAparelho) {
		Parametro parametro = null;		
		SQLiteDatabase dbLocal = null ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		parametro = new ParametroDAO(ctx,db).get();
		List<Usuario> lista = new ArrayList<Usuario>();
		Cursor c = null;
		
		if (menosUsuarioDoAparelho) {
			if (parametro != null)
				c = dbLocal.query(TABELA, colunas, "IDVendedor<>?", new String[]{String.valueOf(parametro.getIdVendedor())}, null, null, "Nome");
		} else {
			c = dbLocal.query(TABELA, colunas, null, null, null, null, "Nome");
		}
		
		while (c.moveToNext()){
			Usuario usuario = new Usuario();
			
			usuario.setIDUsuario(c.getShort(c.getColumnIndex("IdUsuario")));
			usuario.setNome(c.getString(c.getColumnIndex("Nome")));
			usuario.setIDVendedor(c.getShort(c.getColumnIndex("IdVendedor")));
			usuario.setSenha(c.getString(c.getColumnIndex("Senha")));
			
			lista.add(usuario);
		}
		c.close();
	//	if(db==null)
			//dblocal.close(); singletone();
		
		return lista;
	}
	
	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			if (cargaCompleta)
				deleteAll();
			getImportacaoUsuario(linhasTexto, cargaCompleta);
		} catch (Exception e) {
			Log.d("UsuarioDAO", e.getMessage());
		}
	}

	private void getImportacaoUsuario(List<String> linhasTexto, Boolean cargaCompleta)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaUsuarioImportar(cargaCompleta, linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaUsuarioImportar(Boolean cargaCompleta, String linha) {
		Usuario usuario = new Usuario();

		if(linha.substring(0, 4).trim().matches(MATCHES) && !linha.substring(0, 4).trim().equals(""))
			usuario.setIDUsuario(Short.parseShort(linha.substring(0, 4).trim()));
		else {
			Utils.gravarLog("\nUSU: "+linha);
			return true;
		}
		usuario.setNome(linha.substring(4, 104).trim());
		if(linha.substring(104, 108).trim().matches(MATCHES) && !linha.substring(104, 108).trim().equals(""))
			usuario.setIDVendedor(Short.parseShort(linha.substring(104, 108).trim()));
		else {
			Utils.gravarLog("\nUSU: "+linha);
			return true;
		}
		usuario.setSenha(linha.substring(108, 208).trim());

		if(!cargaCompleta) {
			switch (Character.toUpperCase(linha.charAt(208))) {
			case ' ':
				insert(usuario);
				break;
			case 'A':
				if (exists(usuario))
					update(usuario);
				else
					insert(usuario);
				break;
			case 'E':
				delete(usuario);
				break;
			}
		} else {
			insert(usuario);
		}
		return false;
	}

	public boolean exist(Usuario usuario) {
		boolean retorno = false;
		SQLiteDatabase dbLocal = null;
		if (db == null)
			dbLocal = new DBHelper(ctx).getReadableDatabase();
		else
			dbLocal = db;
		
		Cursor c = dbLocal.rawQuery("Select Count(IDUsuario) from Usuario where IDUsuario=?", new String[] {String.valueOf(usuario.getIDUsuario())});
		if (c.moveToFirst()) {
			retorno = c.getInt(0) > 0;
		}
		c.close();
		
		//if (db == null)
			//dbLocal.close(); singletone();
		
		return retorno;
	}
	
	public boolean existeUsuario() {
		SQLiteDatabase dbLocal = null ; 
		
		boolean retorno = false ;
		
		if(db == null)
			dbLocal = new DBHelper(ctx).getWritableDatabase();
		else
			dbLocal = db ;
		
		Cursor c = dbLocal.rawQuery("SELECT COUNT (*) FROM Usuario", null);
		
		if(c.moveToFirst())
			retorno = c.getInt(0) > 0 ;
			
		c.close();
		
		//if(db == null)
			//dbLocal.close(); singletone();
		
		return retorno ;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaUsuarioImportar(true, linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_de_usuarios);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}

