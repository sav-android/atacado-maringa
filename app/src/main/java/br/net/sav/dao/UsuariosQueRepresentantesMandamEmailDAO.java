package br.net.sav.dao;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.net.sav.Utils;
import br.net.sav.modelo.UsuariosQueRepresentantesMandamEmail;

public class UsuariosQueRepresentantesMandamEmailDAO extends PadraoDAO{
	public static final String TABELA="UsuariosQueRepresentantesMandamEmail";
	
	public UsuariosQueRepresentantesMandamEmailDAO(Context ctx,SQLiteDatabase db) {
		super(ctx, TABELA,db);
	}
	
	public void importar(List<String> linhasTexto, Boolean cargaCompleta) {
		try {
			if (cargaCompleta)
				deleteAll();
			try {
				db.beginTransaction();
				
				for (String linha : linhasTexto) {
					UsuariosQueRepresentantesMandamEmail usu = new UsuariosQueRepresentantesMandamEmail();
					
					if(linha.substring(0, 4).trim().matches(MATCHES))
						usu.setIDUsuario(Integer.parseInt(linha.substring(0, 4).trim()));
					else {
						Utils.gravarLog("\nUEM: "+linha);
						continue ;
					}
					usu.setNome(linha.substring(4, 34).trim());
					usu.setEmail(linha.substring(34, 94).trim());											
					
					switch (Character.toUpperCase(linha.charAt(94))) {
					case ' ':
					case 'A':
						if(exists(usu))
							update(usu);
						else
							insert(usu);
						break ;
					case 'E' :
						delete(usu);
						break ;

					default:
						Utils.gravarLog("\nUEM: Flag Inv�lida!");
						continue ;						
					}					
				}
				
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		} catch (Exception e) {
			Log.d("UsuarioQueRepresentanteMandaEmailDAO", e.getMessage());
		}
	}

}
