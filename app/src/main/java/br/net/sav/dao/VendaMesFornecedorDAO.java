package br.net.sav.dao;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.VendaMesFornecedor;

import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.getZeraIncrementar;
import static br.net.sav.IntegradorWeb.dto.ImportacaoDTO.msgAtualizacaProgress;

public class VendaMesFornecedorDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "VendaMesFornecedor" ;
	
	public VendaMesFornecedorDAO(Context ctx,SQLiteDatabase db) {
		super(ctx,TABELA,db);
	}
	
	public void importar (List<String> linhasTexto) {
		try {
			
			deleteAll();

			getImportacaoVendaMesFornecedor(linhasTexto);

		} catch (Exception e) {
			Log.e("VendaMesFornecedorDAO:", e.getMessage());
		}
	}

	private void getImportacaoVendaMesFornecedor(List<String> linhasTexto) {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaVendaMesFornecedorImportar(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.setTransactionSuccessful();
		}
	}

	private boolean getLinhaVendaMesFornecedorImportar(String linha) {
		VendaMesFornecedor vdf = new VendaMesFornecedor();

		vdf.setFornecedor(linha.substring(0,20).trim());

		if(linha.substring(20, 30).trim().matches(MATCHES) && !linha.substring(20, 30).trim().equals("")) {
			vdf.setVendas(Double.parseDouble(linha.substring(20, 30).trim()) / 100);
		} else {
			Utils.gravarLog("\nVDF: " + linha);
			return true;
		}

		if(linha.substring(30, 40).trim().matches(MATCHES) && !linha.substring(30, 40).trim().equals("")) {
			vdf.setTonelagem(Double.parseDouble(linha.substring(30, 40).trim()) / 100);
		} else {
			Utils.gravarLog("\nVDF: " + linha);
			return true;
		}

		if(!exists(vdf))
			insert(vdf);

		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaVendaMesFornecedorImportar(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_venda_mes_fornecedor);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
