package br.net.sav.dao;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.VendaVendedor;

public class VendaVendedorDAO extends PadraoDAO implements AcaoParaImportacaoDados {
	private static final String TABELA = "VendaVendedor";

	public VendaVendedorDAO(Context ctx, SQLiteDatabase db) {
		super(ctx, TABELA, db);
	}

	public List<VendaVendedor> getAll() {
		SQLiteDatabase db = new DBHelper(ctx).getWritableDatabase();

		List<VendaVendedor> lista = new ArrayList<VendaVendedor>();

		Cursor c = db.rawQuery("SELECT * FROM VendaVendedor ORDER BY Periodo", null);

		try {
			while (c.moveToNext()) {
				VendaVendedor venda = new VendaVendedor();
				venda.setPeriodo(c.getString(c.getColumnIndex("Periodo")));
				venda.setTotalVendas(c.getDouble(c.getColumnIndex("TotalVendas")));
				venda.setTotalTonelagem(c.getDouble(c.getColumnIndex("TotalTonelagem")));
				lista.add(venda);
			}
		} catch (Exception e) {
			Log.e("VendaVendedorDAO:", e.getMessage());
		} finally {
			//db.close(); singleton();
			c.close();
		}

		return lista;
	}

	public void importar(List<String> linhasTexto) {
		try {
			deleteAll();
			getImportacaoVendaVendedor(linhasTexto, null, null);

		} catch (Exception e) {
			Log.e("VendaVendedorDAO:", e.getMessage());
		}
	}

	private void getImportacaoVendaVendedor(List<String> linhasTexto, Activity ctx, IAtualizaMensagemProgress msgView)  {
		try {
			db.beginTransaction();

			for (String linha : linhasTexto) {
				if (getLinhaVendaVendedorImportar(linha)) continue;
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	private boolean getLinhaVendaVendedorImportar(String linha) {
		VendaVendedor venda = new VendaVendedor();

		venda.setPeriodo(linha.substring(0, 20).trim());

		if (linha.substring(20, 30).trim().matches(MATCHES) && !linha.substring(20, 30).trim().equals("")) {
			venda.setTotalVendas(Double.parseDouble(linha.substring(20, 30).trim()) / 100);
		} else {
			Utils.gravarLog("\nvda: " + linha);
			return true;
		}

		if (linha.substring(30, 40).trim().matches(MATCHES) && !linha.substring(30, 40).trim().equals("")) {
			venda.setTotalTonelagem(Double.parseDouble(linha.substring(30, 40).trim()) / 100);
		} else {
			Utils.gravarLog("\nvda: " + linha);
			return true;
		}

		if (!exists(venda))
			insert(venda);
		return false;
	}

	@Override
	public String getTabela() {
		return TABELA;
	}

	@Override
	public boolean importarlinhaApi(Activity ctx, String linha, SQLiteDatabase db) {
		if (getLinhaVendaVendedorImportar(linha)) return true;

		return false;
	}

	@Override
	public String getMensagemTabela() {
		return ctx.getString(R.string.importando_tabela_vendas_mes);
	}

	@Override
	public SQLiteDatabase getDB() {
		if (db == null){
			return new DBHelper(ctx).getReadableDatabase();
		}else {
			return db;
		}
	}
}
