package br.net.sav.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import br.net.sav.atacadomaringa.R;

public class DialogCustom {

    public static void showCustomDialogMensagem(int layoit, String msg, Activity context, boolean umBotao, final IResultadoDialog resultado) {
        final Dialog dialog = istanciarDialog(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layoit, null, false);
        dialog.setCancelable(false);
        TextView text = (TextView) view.findViewById(R.id.id_txt_msg);
        Button buttonCancelar = (Button) view.findViewById(R.id.bt_cancelar);
        Button btnEncerrar = view.findViewById(R.id.bt_encerrar);
        btnEncerrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                resultado.execultar(dialog);
            }
        });
        if (umBotao) {
            buttonCancelar.setVisibility(View.GONE);
            btnEncerrar.setText("OK");

        } else {
            buttonCancelar.setVisibility(View.VISIBLE);
        }

        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                resultado.cancelar(dialog);
            }
        });
        text.setText(msg);
        gerarDialog(context, dialog, view);
    }

    public static void gerarDialog(Activity context, Dialog dialog, View view) {
        context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        dialog.setContentView(view);
        final Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }

    public static Dialog istanciarDialog(Activity context) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public interface IResultadoDialog {
        void execultar(Dialog dialog);

        void cancelar(Dialog dialog);
    }
}
