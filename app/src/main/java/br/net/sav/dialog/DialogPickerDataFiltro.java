package br.net.sav.dialog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.net.sav.Utils;

public class DialogPickerDataFiltro {


    private final Activity context;
    private final IniciarPesquisaData iniciar;
    private Calendar mDataInicio;
    private Calendar mDataFim;
    private final EditText mEdtDataInicio;
    private final EditText mEdtDataFim;

    public DialogPickerDataFiltro(Activity context, IniciarPesquisaData iniciar, Calendar mDataInicio, Calendar mDataFim, EditText mEdtDataInicio, EditText mEdtDataFim) {
        this.context = context;
        this.iniciar = iniciar;
        this.mDataInicio = mDataInicio;
        this.mDataFim = mDataFim;
        this.mEdtDataInicio = mEdtDataInicio;
        this.mEdtDataFim = mEdtDataFim;
    }

    public void showDatePickerDataFim() {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(Utils.sdfDataPtBr.parse(mEdtDataFim.getText().toString()));
        } catch (ParseException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        DatePickerDialog dialog = new DatePickerDialog(context, mDateFimSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener mDateFimSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mEdtDataFim.setText(Utils.sdfDataPtBr.format(new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime()));

            try {
                mDataInicio.setTime(converteFormatoData(Utils.getText(mEdtDataInicio)));
                mDataFim.setTime(converteFormatoData(Utils.getText(mEdtDataFim)));

                if(verificarConcordanciaDatas(mDataInicio.getTime(), mDataFim.getTime())) {
                    iniciar.execute();
                } else {
                    iniciar.execute();
                    mEdtDataInicio.setText(Utils.sdfDataPtBr.format(buscarDataMesAnterior(new Date())));
                    mEdtDataFim.setText(Utils.sdfDataPtBr.format(new Date()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    public void showDatePickerDataInicio() {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(Utils.sdfDataPtBr.parse(mEdtDataInicio.getText().toString()));
        } catch (ParseException e) {
            Log.e(getClass().getName() +".showDatePickerInicio()", e.getMessage());
        }
        DatePickerDialog dialog = new DatePickerDialog(context, mDateInicioSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener mDateInicioSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mEdtDataInicio.setText(Utils.sdfDataPtBr.format(new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime()));

            try {
                mDataInicio.setTime(converteFormatoData(Utils.getText(mEdtDataInicio)));
                mDataFim.setTime(converteFormatoData(Utils.getText(mEdtDataFim)));

                if(verificarConcordanciaDatas(mDataInicio.getTime(), mDataFim.getTime())) {
                    iniciar.execute();
                } else{
                    iniciar.execute();
                    mEdtDataInicio.setText(Utils.sdfDataPtBr.format(buscarDataMesAnterior(new Date())));
                    mEdtDataFim.setText(Utils.sdfDataPtBr.format(new Date()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    private boolean verificarConcordanciaDatas(Date dataInicial, Date dataFinal) {
        dataInicial = truncarData(dataInicial,false);
        dataFinal = truncarData(dataFinal,false);

        return dataInicial.compareTo(dataFinal) <= 0;
    }

    public static Date truncarData(Date data, boolean horaFinalDia) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.set(Calendar.HOUR_OF_DAY, (horaFinalDia) ? 23 : 0);
        cal.set(Calendar.MINUTE, (horaFinalDia) ? 59 : 0);
        cal.set(Calendar.SECOND, (horaFinalDia) ? 59 : 0);
        cal.set(Calendar.MILLISECOND, (horaFinalDia) ? 999 : 0);
        // Retorna data truncada
        return cal.getTime();
    }

    private Date buscarDataMesAnterior(Date pData) {
        Calendar data = Calendar.getInstance();

        data.setTime(pData);
        data.add(Calendar.MONTH, -1);

        return data.getTime();
    }

    private Date converteFormatoData(String data) throws ParseException {
        StringBuilder strData = new StringBuilder();

        strData.append(data.substring(data.length() - 4, data.length()));
        strData.append("-");
        strData.append(data.substring(data.length() - 7, data.length() - 5));
        strData.append("-");
        strData.append(data.substring(data.length() - 10, data.length() - 8));

        return Utils.sdfDataDb.parse(strData.toString());
    }


    protected void inicializarDatas() {
        mDataFim = Calendar.getInstance();
        mDataInicio = Calendar.getInstance();
        mDataInicio.add(Calendar.DAY_OF_MONTH, - mDataFim.get(Calendar.DAY_OF_MONTH));
        mDataInicio.add(Calendar.DAY_OF_MONTH, 1);

        if (mEdtDataFim != null) {
            mEdtDataFim.setText(Utils.sdfDataPtBr.format(mDataFim.getTime()));
        }
        if (mEdtDataInicio != null) {
            mEdtDataInicio.setText(Utils.sdfDataPtBr.format(mDataInicio.getTime()));
        }
    }


    public interface IniciarPesquisaData{
         void execute();
    }
}
