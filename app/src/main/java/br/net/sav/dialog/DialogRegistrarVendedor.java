package br.net.sav.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.interfaces.IAoRegistrarVendedor;
import br.net.sav.IntegradorWeb.interfaces.IgetToken;
import br.net.sav.IntegradorWeb.repository.VendedorRepository;
import br.net.sav.Utils;

public class DialogRegistrarVendedor {

    private AlertDialog.Builder dialog;
    private EditText edtSenha;
    private EditText edtCodigo;

    public void registra(final Context context, IgetToken igetToken, final IAoRegistrarVendedor aoRegistrarVendedor){

        final String token = igetToken==null?null:igetToken.getToken();

        if(token==null || (token!=null && token.length()==0)){

            View layout = LayoutInflater.from(context).inflate(br.net.sav.R.layout.dialog_registro_central_manager, null);

            edtSenha = (EditText) layout.findViewById(br.net.sav.R.id.registro_cm_senha);
            edtCodigo = (EditText) layout.findViewById(br.net.sav.R.id.registro_cm_codigo);

            dialog = new AlertDialog.Builder(context);
            dialog.setTitle("Registro de vendedor");
            dialog.setMessage("Digite a senha para registro");
            dialog.setView(layout);

            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, int which) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setCancelable(false);
                    final AlertDialog dialogCarregando = builder.setTitle("Registrando aparelho...").create();
                    dialogCarregando.show();
                    Integer currentVersion =0;
                    try {
                        currentVersion = Utils.currentVersionCode(context);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    VendedorRepository vendedorRepository = new VendedorRepository();
                    vendedorRepository.registraVendedor(edtCodigo.getText().toString(), edtSenha.getText().toString(), currentVersion.toString() ,new IAoRegistrarVendedor() {
                        @Override
                        public void aoRegistrar(Vendedor vendedor) {
                            dialogCarregando.setMessage("registrado");
                            dialogCarregando.dismiss();
                            aoRegistrarVendedor.aoRegistrar(vendedor);
                        }

                        @Override
                        public void aoFalhar(String mensagemErro) {
                            dialogCarregando.dismiss();
                            aoRegistrarVendedor.aoFalhar(mensagemErro);
                        }
                    });
                }
            });
            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }

    }
}
