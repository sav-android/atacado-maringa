package br.net.sav.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.Pedido;

public class DialogResumoPedido extends Dialog {
    private Context context;
    private List<Pedido> pedidosResumo;
    private TextView txvFaturado, txvEnviado, txvNaoEnviado, txvCancelado;

    public DialogResumoPedido(@NonNull Context context, List<Pedido> pedidosResumo) {
        super(context);
        this.context = context;
        this.pedidosResumo = pedidosResumo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_resumo_pedido, null);
        setContentView(layout);

        try {
            txvFaturado = (TextView) layout.findViewById(R.id.txvResumoValorFaturado);
            txvEnviado = (TextView) layout.findViewById(R.id.txvResumoValorEnviado);
            txvNaoEnviado = (TextView) layout.findViewById(R.id.txvResumoValorNaoEnviado);
            txvCancelado = (TextView) layout.findViewById(R.id.txvResumoValorCancelado);
            setTitle("Resumo Pedido");

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }

        CalculaResumoTotal calculaResumoTotal = new CalculaResumoTotal().invoke();
        txvFaturado.setText("Total Faturado: R$ "+ formatMoeda(calculaResumoTotal.getTotalFaturado()));
        txvEnviado.setText("Total Enviado: R$ "+ formatMoeda(calculaResumoTotal.getTotalEnviado()));
        txvNaoEnviado.setText("Total N�o Enviado: R$ " + formatMoeda(calculaResumoTotal.getTotalNaoEnviado()));
        txvCancelado.setText("Total Cancelado: R$ " + formatMoeda(calculaResumoTotal.getTotalCancelado()));
    }

    private static String formatMoeda(double valor) {
        DecimalFormat decimal = new DecimalFormat("###,###,###,##0.00");
         return decimal.format(valor);
    }

    private class CalculaResumoTotal {
        private double totalFaturado = 0;
        private double totalEnviado = 0;
        private double totalNaoEnviado = 0;
        private double totalCancelado = 0;


        public double getTotalFaturado() {
            return totalFaturado;
        }

        public double getTotalEnviado() {
            return totalEnviado;
        }

        public double getTotalNaoEnviado() {
            return totalNaoEnviado;
        }

        public double getTotalCancelado() {
            return totalCancelado;
        }

        public CalculaResumoTotal invoke() {
            if (!pedidosResumo.isEmpty()){

                for (Pedido pedido: pedidosResumo){
                    if (pedido.getStatusFaturado() != null) {
                        if (pedido.getStatusFaturado().trim().contains("Faturado")) {
                            totalFaturado = totalFaturado + pedido.getTotalFaturado();
                        }
                    }
                }

                for (Pedido pedido: pedidosResumo){
                    if (pedido.getStatusFaturado() != null) {
                        if (pedido.getStatusFaturado().trim().contains("Cancelado")) {
                            totalCancelado = totalCancelado + pedido.getTotalLiquido();
                        }
                    }
                }

                for (Pedido pedido: pedidosResumo){
                    if (pedido.isEnviado()){
                        totalEnviado = totalEnviado + pedido.getTotalLiquido();
                    }
                }

                for (Pedido pedido: pedidosResumo){
                    if (!pedido.isEnviado()){
                        totalNaoEnviado = totalNaoEnviado + pedido.getTotalLiquido();
                    }
                }
            }
            return this;
        }
    }
}
