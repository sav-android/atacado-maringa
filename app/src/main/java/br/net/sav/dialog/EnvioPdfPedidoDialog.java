package br.net.sav.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.GeradorDePdf;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.Pedido;

public class EnvioPdfPedidoDialog extends DialogFragment {
    public static String TAG = "EnvioPdfPedidoDialog";

    private String[] mDestinatarios;
    private Pedido mPedido;
    private List<Item> mItens;

    public EnvioPdfPedidoDialog(Context context, Pedido pedido, List<Item> itens, String... destinatarios) {
        this.mDestinatarios = destinatarios;
        this.mPedido = pedido;
        this.mItens = itens;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        setCancelable(false);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Resumo do Pedido");
        dialog.setMessage("Deseja enviar um resumo do Pedido para o cliente?");
        dialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface pDialog, int pWhich) {
                try {
                    String nomeArquivo = gerarResumoDoPedido();
                    Uri r = Uri.fromFile(new File(GeradorDePdf.getCaminhoArquivosPadrao() + nomeArquivo + ".pdf"));
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);

                    Log.v("ItemTabActivity", "arquivo resumo existe - " + new File(GeradorDePdf.getCaminhoArquivosPadrao() + nomeArquivo + ".pdf").exists());
                    sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                    sendIntent.putExtra(Intent.EXTRA_EMAIL, mDestinatarios);
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Atacado Maringa - Pedido");
                    sendIntent.setType("application/pdf");
                    sendIntent.putExtra(Intent.EXTRA_STREAM, r);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Resumo do pedido, arquivo " + nomeArquivo);
                    startActivity(sendIntent);

                    dismiss();
                } catch (Exception e) {
                    Log.e(String.format("%s.enviarResumo()", getClass().getSimpleName()), e.getMessage());
                }
            }
        });
        dialog.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface pDialog, int pWhich) {
                dismiss();
            }
        });

        dialog.setCancelable(false);
        return dialog.show();
    }

    private String gerarResumoDoPedido() {
        ArrayList<String> linhas = new PedidoDAO(getActivity(), null).getLinhasPdfPedido(mPedido, mItens);
        return new PedidoDAO(getActivity(), null).gerarPdf(linhas, mPedido.getIDPedido());
    }

    public static void iniciar(FragmentActivity activity, Pedido pedido, List<Item> itens, String... destinatarios) {
        FragmentManager manager = activity.getSupportFragmentManager();

        EnvioPdfPedidoDialog dialog = new EnvioPdfPedidoDialog(activity, pedido, itens, destinatarios);
        dialog.show(manager, TAG);
    }
}
