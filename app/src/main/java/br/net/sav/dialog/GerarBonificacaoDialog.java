package br.net.sav.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.enumerador.TipoOperacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Tabela;
import br.net.sav.modelo.TipoPedido;

public class GerarBonificacaoDialog {

    public interface OnClickListener{
        void onClick();
    }


    public static void gerarDialog(Parametro parametro, Pedido pedido, Context context, final OnClickListener onClickListener){

        if(parametro.getPercentualBonificacao() > 0 && pedido.getIDPedido()!=-1 &&
                ehTipoVenda(pedido, context) && existeTipoBonificacaoParaCliente(pedido, context)){

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.atencao)
                    .setMessage(R.string.deseja_gerar_bonificacao)
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onClickListener.onClick();
                        }
                    })
                    .setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    public static boolean ehTipoVenda(Pedido pedido, Context context) {
        TipoPedido tipoPedido = new TipoPedidoDAO(context, null).get(pedido.getIDTPedido());
        return tipoPedido.getTipoOperacao().equals(TipoOperacao.VENDA.toString());
    }

    public static boolean existeTipoBonificacaoParaCliente(Pedido pedido, Context ctx){
        List<TipoPedido> tiposDePedido = new TipoPedidoDAO(ctx, null).getByCliente(pedido.IDCliente);

        for (TipoPedido tipo: tiposDePedido){
            if (tipo.getTipoOperacao().equals("B") || tipo.getTipoOperacao().equals("b")){
                return true;
            }
        }

        return false;
    }

    public static void GerarBonificacaoSelecionandoVenda(final Pedido pedido, final Parametro parametro, final Context context ){
       final ArrayList<Pedido> pedidos = new PedidoDAO(context, null).listaPedidosSemBonificacao(PedidoDAO.NAO_ENVIADOS, pedido);


        String mensagemBonifca = mensagemParaGerarBonificacao(context, pedidos);

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setIcon(android.R.drawable.ic_dialog_alert);
        CharSequence[] itemCodigo = new CharSequence[pedidos.size()];
        int cont = 0;

        for (Pedido p : pedidos){
            itemCodigo[cont] = "Codigo do Pedido: " + p.getIDPedido();
            cont++;

        }

        if (pedidos.size() > 0) {
            builderSingle.setTitle(mensagemBonifca);
            builderSingle.setItems(itemCodigo, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Pedido pedidoSelecionado = pedidos.get(which);
                    pedidoSelecionado.calcularSaldoBonificar(parametro.getPercentualBonificacao());

                    if (getTabelaPrecoSemRentabilidade(pedidoSelecionado, pedido, context)) {
                        mostrarMensagem(String.format(context.getString(R.string.tabela_de_preco_diferente)), context);
                        return;
                    }

                    if (getPedidoAbaixoDoSaldoBonificado(pedidoSelecionado, pedido)) {
                        mostrarMensagem(String.format("Valor do Pedido ultrapassa o valor do Saldo a Bonificar do Pedido Vinculado: %.2f R$", pedidoSelecionado.getSaldoBonificar()), context);
                        return;
                    } else {
                        pedido.setVinculoPedido(pedidoSelecionado.getIDPedido());
                    }


                    mostrarMensagem("Pedido Vinculado com Sucesso!", context);
                    dialog.dismiss();

                }
            });
        }else {
            builderSingle.setTitle(context.getString(R.string.atencao));
            builderSingle.setMessage(mensagemBonifca);
        }

        builderSingle.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.show();

    }

    private static boolean getPedidoAbaixoDoSaldoBonificado(Pedido pedidoSelecionado, Pedido pedido) {
        return pedidoSelecionado.getSaldoBonificar() < pedido.getTotalLiquido();
    }

    private static boolean getTabelaPrecoSemRentabilidade(Pedido pedidoSelecionado, Pedido pedido, Context ctx) {
        Tabela tabela = new TabelaDAO(ctx, null).get(pedidoSelecionado.getIDTabela());
        Tabela tabelaAux = new TabelaDAO(ctx, null).get(pedido.getIDTabela());

        return tabela.isUltilizaRentabilidade() && !tabelaAux.isUltilizaRentabilidade() ||
                !tabela.isUltilizaRentabilidade() && tabelaAux.isUltilizaRentabilidade();

    }

    private static String mensagemParaGerarBonificacao(Context context, ArrayList<Pedido> pedidos) {
        String mensagemBonifca = "";
        if (pedidos.size() > 0){
            mensagemBonifca = context.getString(R.string.gerar_bonifiacao);
        }else {
            mensagemBonifca = context.getString(R.string.gerar_bonificacao_fazer_venda);
        }
        return mensagemBonifca;
    }

    public static void mostrarMensagem(String string, Context mContext){
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();

    }

}
