package br.net.sav.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.enumerador.TipoSenha;
import br.net.sav.modelo.AlterarObjeto;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.VendaVendedor;
import br.net.sav.util.StringUtils;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class GerarSenhaDialog extends Dialog {

    private final Context mContext;
    private Activity parentActivity;

    private Parametro mParametro;
    private Cliente mCliente;
    private VendaVendedor mVendaVendedor;
    private Pedido mPedido;
    private TipoSenha tipoSenha;

    private TextView txvCampo1, txvCampo2, txvCampo3, txvCampo4;
    private EditText edtCampo1, edtCampo2, edtCampo3, edtCampo4;
    private LinearLayout lnlCampo1, lnlCampo2, lnlCampo3, lnlCampo4, lnlCampo5;
    private Button btnCampo5;
    private String mensagem = "";
    private View dialogView;


    public GerarSenhaDialog(@NonNull Context context, TipoSenha tipoSenha) {
        super(context);

        this.mContext = context;
        this.tipoSenha = tipoSenha;

        // Inicializar objetos
        mParametro = new ParametroDAO(context, null).get();

        if (mParametro == null) {
            Toast.makeText(mContext, R.string.parametros_nao_encontrados, Toast.LENGTH_LONG).show();
            dismiss();
        }
    }

    public GerarSenhaDialog(Context context, Activity parentActivity) {
        super(context);
        this.mContext = context;
        this.parentActivity = parentActivity;
        // Inicializar objetos
        mParametro = new ParametroDAO(context, null).get();

        if (mParametro == null) {
            Toast.makeText(mContext, R.string.parametros_nao_encontrados, Toast.LENGTH_LONG).show();
            dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void viewConfigSenhaParaDesbloqueio() {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dialogView = inflater.inflate(R.layout.layout_gerador_senha, null);

        lnlCampo1 = (LinearLayout) dialogView.findViewById(R.id.dialogSenha_lnlCampo1);
        lnlCampo2 = (LinearLayout) dialogView.findViewById(R.id.dialogSenha_lnlCampo2);
        lnlCampo3 = (LinearLayout) dialogView.findViewById(R.id.dialogSenha_lnlCampo3);
        lnlCampo4 = (LinearLayout) dialogView.findViewById(R.id.dialogSenha_lnlCampo4);
        lnlCampo5 = (LinearLayout) dialogView.findViewById(R.id.dialogSenha_lnlCampo5);

        txvCampo1 = (TextView) dialogView.findViewById(R.id.dialogSenha_txvCampo1);
        txvCampo2 = (TextView) dialogView.findViewById(R.id.dialogSenha_txvCampo2);
        txvCampo3 = (TextView) dialogView.findViewById(R.id.dialogSenha_txvCampo3);
        txvCampo4 = (TextView) dialogView.findViewById(R.id.dialogSenha_txvCampo4);

        edtCampo1 = (EditText) dialogView.findViewById(R.id.dialogSenha_edtCampo1);
        edtCampo2 = (EditText) dialogView.findViewById(R.id.dialogSenha_edtCampo2);
        edtCampo3 = (EditText) dialogView.findViewById(R.id.dialogSenha_edtCampo3);
        edtCampo4 = (EditText) dialogView.findViewById(R.id.dialogSenha_edtCampo4);

        btnCampo5 = (Button) dialogView.findViewById(R.id.dialogSenha_btnCampo5);
        btnCampo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                String pasteData = "";
                if (!(clipboard.hasPrimaryClip())) {
                    btnCampo5.setEnabled(false);
                } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
                    btnCampo5.setEnabled(false);
                } else {
                    btnCampo5.setEnabled(true);
                }

                ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                pasteData = String.valueOf(item.getText());
                String[] strings = pasteData.split(" ");
                strings[3] = strings[3].substring(0,strings[3].indexOf("."));

                if (!pasteData.isEmpty()) {
                    edtCampo1.setText(strings[0]);
                    edtCampo2.setText(strings[1]);
                    edtCampo3.setText(strings[2]);
                    edtCampo4.setText(strings[3]);
                }
            }
        });

        // Desabilitar todos os campos
        lnlCampo1.setVisibility(View.GONE);
        lnlCampo2.setVisibility(View.GONE);
        lnlCampo3.setVisibility(View.GONE);
        lnlCampo4.setVisibility(View.GONE);
        lnlCampo5.setVisibility(View.GONE);
    }

    public void dialogMsgGerarSenha() {
        viewConfigSenhaParaDesbloqueio();
        tratarCamposQueIramAparecer();

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(tipoSenha.toString());
        dialogBuilder.setMessage(mensagem);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(mContext.getString(R.string.gerar_senha), null);
        dialogBuilder.setNegativeButton(mContext.getString(R.string.cancelar), null);

        final AlertDialog dialog = dialogBuilder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!camposValidos()) {
                    return;
                }
                preencherCampos();
                dialog.dismiss();

            }
        });
    }

    private void tratarCamposQueIramAparecer() {

        // Tratar campos que dever�o aparecer conforme tipo de senha
//        if (TipoSenha.AUMENTAR_DESCONTO.equals(tipoSenha)) {
//            lnlCampo1.setVisibility(View.VISIBLE);
//            lnlCampo2.setVisibility(View.VISIBLE);
//            lnlCampo3.setVisibility(View.VISIBLE);
//
//            txvCampo1.setText(mContext.getString(R.string.pedido));
//            txvCampo2.setText(mContext.getString(R.string.cliente));
//            txvCampo3.setText(mContext.getString(R.string.produto));
//
//            edtCampo1.setHint(mContext.getString(R.string.informe_numero_do_pedido));
//            edtCampo2.setHint(mContext.getString(R.string.informe_codigo_do_cliente));
//            edtCampo3.setHint(mContext.getString(R.string.informe_codigo_do_produto));
//
//            mensagem = String.format(mContext.getString(R.string.permitira_conceder_desconto), mParametro.getPercDescontoAutorizadoPorSenha());
//        } else
        if (TipoSenha.RENTABILIDADE_MINIMA.equals(tipoSenha)) {
            lnlCampo1.setVisibility(View.VISIBLE);
            lnlCampo2.setVisibility(View.VISIBLE);
            lnlCampo3.setVisibility(View.VISIBLE);
            lnlCampo4.setVisibility(View.VISIBLE);
            lnlCampo5.setVisibility(View.VISIBLE);

            txvCampo1.setText(mContext.getString(R.string.vendedor));
            txvCampo2.setText(mContext.getString(R.string.cliente));
            txvCampo3.setText(mContext.getString(R.string.pedido));
            txvCampo4.setText(mContext.getString(R.string.rentabilidade));

            edtCampo1.setHint(mContext.getString(R.string.informe_codigo_do_vendedor));
            edtCampo2.setHint(mContext.getString(R.string.informe_codigo_do_cliente));
            edtCampo3.setHint(mContext.getString(R.string.informe_numero_do_pedido));
            edtCampo4.setHint(mContext.getString(R.string.informe_perc_rentabilidade));
            edtCampo4.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});

            mensagem = mContext.getString(R.string.perc_rentabilidade_abaixo_do_minimo_permitido);
        } else if (TipoSenha.CLIENTE_BLOQUEADO.equals(tipoSenha)) {
            lnlCampo1.setVisibility(View.VISIBLE);
            lnlCampo2.setVisibility(View.VISIBLE);

            txvCampo1.setText(mContext.getString(R.string.vendedor));
            txvCampo2.setText(mContext.getString(R.string.cliente));

            edtCampo1.setHint(mContext.getString(R.string.informe_codigo_do_vendedor));
            edtCampo2.setHint(mContext.getString(R.string.informe_codigo_do_cliente));

            mensagem = mContext.getString(R.string.atender_cliente_bloquado);
        } else if (TipoSenha.PEDIDO_BLOQUEADO.equals(tipoSenha)) {
            lnlCampo1.setVisibility(View.VISIBLE);
            lnlCampo2.setVisibility(View.VISIBLE);

            txvCampo1.setText(mContext.getString(R.string.vendedor));
            txvCampo2.setText(mContext.getString(R.string.pedido));

            edtCampo1.setHint(mContext.getString(R.string.informe_codigo_do_vendedor));
            edtCampo2.setHint(mContext.getString(R.string.informe_numero_do_pedido));

            mensagem = mContext.getString(R.string.permitira_desbloquear_pedido);
        } else if (TipoSenha.TRABALHAR_SEM_CONEXAO.equals(tipoSenha)) {
            lnlCampo1.setVisibility(View.VISIBLE);

            txvCampo1.setText(mContext.getString(R.string.vendedor));

            edtCampo1.setHint(mContext.getString(R.string.informe_codigo_do_vendedor));

            mensagem = mContext.getString(R.string.a_senha_gerada_informada);
        } else if (TipoSenha.ACESSO_BLOQUEADO.equals(tipoSenha)) {
            lnlCampo1.setVisibility(View.VISIBLE);

            txvCampo1.setText(mContext.getString(R.string.vendedor));

            edtCampo1.setHint(mContext.getString(R.string.informe_codigo_do_vendedor));

            mensagem = mContext.getString(R.string.a_senha_gerada_informada_ao_vendedor);
        }

    }

    private void preencherCampos() {
        try {

            String conteudo = null;
            String senha = null;

//            if (TipoSenha.AUMENTAR_DESCONTO.equals(tipoSenha)) {
//                long pedido = Long.parseLong(StringUtils.getText(edtCampo1));
//                long cliente = Long.parseLong(StringUtils.getText(edtCampo2));
//                long produto = Long.parseLong(StringUtils.getText(edtCampo3));
//
//                conteudo = String.format("DataUltimaCompra: %s\nPedido: %d\nCliente: %d\nProduto: %d\n\nSenha: %s", Utils.sdfDataPtBr.format(new Date()), pedido, cliente, produto, VerificadorSenha.gerarSenhaAumentarDescontoItem(pedido, cliente, produto));
//            } else
            if (TipoSenha.RENTABILIDADE_MINIMA.equals(tipoSenha)) {
                int vendedor = Integer.parseInt(StringUtils.getText(edtCampo1));
                long cliente = Long.parseLong(StringUtils.getText(edtCampo2));
                long pedido = Long.parseLong(StringUtils.getText(edtCampo3));
                long rentabilidade = Long.parseLong(StringUtils.getText(edtCampo4));

                conteudo = String.format("DataUltimaCompra: %s\nVendedor: %d\nCliente: %d\nPedido: %d\nPerc.Rentabilidade (LIBERADO): %s%%\n\nSenha: %s",
                        Utils.sdfDataPtBr.format(new Date()),
                        vendedor,
                        cliente,
                        pedido,
                        rentabilidade,
                        Utils.gerarSenhaLiberarPedidoBloqueadoPorRentabilidade(vendedor,
                                cliente,
                                pedido,
                                rentabilidade));
            } else if (TipoSenha.CLIENTE_BLOQUEADO.equals(tipoSenha)) {
                int vendedor = Integer.parseInt(StringUtils.getText(edtCampo1));
                int cliente = Integer.parseInt(StringUtils.getText(edtCampo2));

                conteudo = String.format("DataUltimaCompra: %s\nVendedor: %d\nCliente: %d\n\nSenha: %s", Utils.sdfDataPtBr.format(new Date()), vendedor, cliente, Utils.gerarSenhaClienteBloqueado(vendedor, cliente));
            } else if (TipoSenha.PEDIDO_BLOQUEADO.equals(tipoSenha)) {
                long vendedor = Long.parseLong(StringUtils.getText(edtCampo1));
                long pedido = Long.parseLong(StringUtils.getText(edtCampo2));

                conteudo = String.format("DataUltimaCompra: %s\nVendedor: %d\nPedido: %d\n\nSenha: %s", Utils.sdfDataPtBr.format(new Date()), vendedor, pedido, Utils.gerarSenhaPedidoBloqueado(vendedor, pedido));
            } else if (TipoSenha.TRABALHAR_SEM_CONEXAO.equals(tipoSenha)) {
                long vendedor = Long.parseLong(StringUtils.getText(edtCampo1));

                conteudo = String.format("DataUltimaCompra: %s\nVendedor: %d\n\nSenha: %s", Utils.sdfDataPtBr.format(new Date()), vendedor, Utils.gerarSenhaTrabalharSemConexao(vendedor));
            } else if (TipoSenha.ACESSO_BLOQUEADO.equals(tipoSenha)) {
                long vendedor = Long.parseLong(StringUtils.getText(edtCampo1));

                conteudo = String.format("DataUltimaCompra: %s\nVendedor: %d\n\nSenha: %s", Utils.sdfDataPtBr.format(new Date()), vendedor, Utils.gerarSenhaDesbloquearDataAlterada(vendedor, null));
            }

            if (conteudo != null) {
                senha = conteudo.substring(conteudo.lastIndexOf(":") + 2);
                Utils.showDialogSharePassword(mContext, tipoSenha.toString(), tipoSenha.toString(), conteudo, senha);
            }

        } catch (Exception e) {
            mostrarMensagem("ERRO: " + e.getMessage());
        }

        camposValidos();
    }


    private boolean camposValidos() {

        if (TipoSenha.RENTABILIDADE_MINIMA.equals(tipoSenha)) {
            if (StringUtils.editTextIsEmpty(edtCampo1)) {
                edtCampo1.setError(mContext.getString(R.string.informe_codigo_do_vendedor));
                return false;
            }
            if (StringUtils.editTextIsEmpty(edtCampo2)) {
                edtCampo2.setError(mContext.getString(R.string.informe_codigo_do_cliente));
                return false;
            }
            if (StringUtils.editTextIsEmpty(edtCampo3)) {
                edtCampo3.setError(mContext.getString(R.string.informe_numero_do_pedido));
                return false;
            }

            if (StringUtils.editTextIsEmpty(edtCampo4)) {
                edtCampo4.setError(mContext.getString(R.string.informe_perc_rentabilidade));
                return false;
            }
        } else if (TipoSenha.CLIENTE_BLOQUEADO.equals(tipoSenha)) {
            if (StringUtils.editTextIsEmpty(edtCampo1)) {
                edtCampo1.setError(mContext.getString(R.string.informe_codigo_do_vendedor));
                ;
                return false;
            }
            if (StringUtils.editTextIsEmpty(edtCampo2)) {
                edtCampo2.setError(mContext.getString(R.string.informe_codigo_do_cliente));
                return false;
            }
        } else if (TipoSenha.PEDIDO_BLOQUEADO.equals(tipoSenha)) {
            if (StringUtils.editTextIsEmpty(edtCampo1)) {
                edtCampo1.setError(mContext.getString(R.string.informe_codigo_do_vendedor));
                ;
                return false;
            }
            if (StringUtils.editTextIsEmpty(edtCampo2)) {
                edtCampo2.setError(mContext.getString(R.string.informe_numero_do_pedido));
                return false;
            }
        } else if (TipoSenha.TRABALHAR_SEM_CONEXAO.equals(tipoSenha)) {
            if (StringUtils.editTextIsEmpty(edtCampo1)) {
                edtCampo1.setError(mContext.getString(R.string.informe_codigo_do_vendedor));
                ;
                return false;
            }
        }
        return true;
    }


    protected void mostrarMensagem(String pMensagem) {
        Toast.makeText(mContext, pMensagem, Toast.LENGTH_LONG).show();
    }

    public void showDialogSenhaDesbloquearAtendimento(final Pedido pedido, final Date dataAtual, final AlterarObjeto alterarObjeto) {

        String mensagem = String.format(mContext.getString(R.string.atenccao) + " Existe(m) pedido(s) com data alterada, Avise seu superior " +
                "e verifique DataUltimaCompra/Hora do seu dispositivo," +
                " ou digite a senha para fazer um novo atendimento. \n\nDataUltimaCompra do Dispositivo: %s\nVendedor: %d", Utils.sdfDataPtBr.format(pedido.getDataPedido()), mParametro.getIdVendedor());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_inserir_senha, null);

        final EditText edtSenha = (EditText) dialogView.findViewById(R.id.dialogSenha_edtSenha);


        edtSenha.setHint(mContext.getString(R.string.informe_a_senha));

        final AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
        dialog.setTitle(mContext.getString(R.string.senha_desbloquear_pedido));
        dialog.setMessage(mensagem);
        dialog.setView(dialogView);
        dialog.setPositiveButton(mContext.getString(R.string.ok), null);
        dialog.setNegativeButton(mContext.getString(R.string.cancelar), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alterarObjeto.alterar(pedido, false);
            }
        });

        final AlertDialog dialogs = dialog.create();
        dialogs.show();
        dialogs.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.editTextIsEmpty(edtSenha)) {
                    edtSenha.setError(mContext.getString(R.string.informe_a_senha));
                    return;
                }

                if (Utils.isSenhaDesbloquearDataAlterada(StringUtils.getText(edtSenha), mParametro.getIdVendedor(), dataAtual)) {
                    mostrarMensagem(mContext.getString(R.string.pedido_liberado));
                    alterarObjeto.alterar(pedido, true);
                    dialogs.dismiss();
                } else {
                    alterarObjeto.alterar(pedido, false);
                    mostrarMensagem(mContext.getString(R.string.senha_invalida));
                    dialogs.dismiss();
                }

            }

        });

    }

    public void showDialogSenhaDesbloquearUltimoAcesso(final Date dataAtual, final Date dataUltimoAcesso, final AlterarObjeto alterarObjeto) {

        String mensagem = String.format(mContext.getString(R.string.atenccao) + " Passe o codigo do vendedor Para o responsavel gerar a senha para desbloqueio do Acesso \n\nDataUltimaCompra do Atual: %s\nDataUltimaCompra do Ultimo Acesso: %s\nVendedor: %d", Utils.sdfDataPtBr.format(dataAtual), Utils.sdfDataPtBr.format(dataUltimoAcesso), mParametro.getIdVendedor());

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_inserir_senha, null);

        final EditText edtSenha = (EditText) dialogView.findViewById(R.id.dialogSenha_edtSenha);


        edtSenha.setHint(mContext.getString(R.string.informe_a_senha));

        final AlertDialog.Builder dialog = new AlertDialog.Builder(parentActivity);
        dialog.setTitle(mContext.getString(R.string.senha_desbloquear_Utimo_acesso));
        dialog.setMessage(mensagem);
        dialog.setView(dialogView);
        dialog.setPositiveButton(mContext.getString(R.string.ok), null);
        dialog.setNegativeButton(mContext.getString(R.string.cancelar), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //      alterarObjeto.alterar(pedido,false);
            }
        });

        final AlertDialog dialogs = dialog.create();
        dialogs.show();
        dialogs.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.editTextIsEmpty(edtSenha)) {
                    edtSenha.setError(mContext.getString(R.string.informe_a_senha));
                    return;
                }

                if (Utils.isSenhaDesbloquearDataAlterada(StringUtils.getText(edtSenha), mParametro.getIdVendedor(), dataAtual)) {
                    mostrarMensagem(mContext.getString(R.string.acesso_liberado));
                    alterarObjeto.alterar(new Pedido(), true);
                    dialogs.dismiss();
                } else {
                    // alterarObjeto.alterar(pedido,false);
                    mostrarMensagem(mContext.getString(R.string.senha_invalida));
                    dialogs.dismiss();
                }

            }

        });

    }

    public static String retirarPercentualSenha(EditText edtSenha, double rentabilidaBaixa) {
        String senha = StringUtils.getText(edtSenha);
        if (senha.length() > 2 && rentabilidaBaixa >= 10) {
            return senha.substring(senha.length() - 2);
        } else if (senha.length() > 2 && rentabilidaBaixa < 10) {
            return senha.substring(senha.length() - 1);
        }

        return "";
    }

}
