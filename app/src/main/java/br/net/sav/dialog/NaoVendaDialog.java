package br.net.sav.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import br.net.sav.LocationService;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.Principal;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.NaoVendaDAO;
import br.net.sav.dao.NaoVendaMovimentoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.NaoVenda;
import br.net.sav.modelo.NaoVendaMovimento;
import br.net.sav.modelo.Parametro;
import br.net.sav.util.dao.MapeamentoDAO;
import br.net.sav.util.modelo.Mapeamento;

/**
 * @author marlon
 * 
 */
public class NaoVendaDialog extends Dialog {

	private Context mContext;
	
	private TextView mTxvData, mTxvHora;
	private Spinner mSpnMotivo;
	private Button mBtnOk, mBtnCancelar;
	
	private Cliente mCliente;
	private Parametro mParametro;
	private List<NaoVenda> mMotivos;
	

	public NaoVendaDialog(Context context, Cliente cliente) {
		super(context);

		this.mContext = context;
		this.mCliente = cliente;
		
		// Inicializar objetos
		mParametro = new ParametroDAO(context, null).get();
		mMotivos = new NaoVendaDAO(context, null).getAll();
		
		if (mParametro == null) {
			Toast.makeText(mContext, R.string.parametros_nao_encontrados, Toast.LENGTH_LONG).show();
			dismiss();
		}
		
		// Iniciar geolocaliza??o
		iniciarServicoLocalizacao();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		povoarComponentes();
	}

	@SuppressLint("InflateParams")
	private void povoarComponentes() {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_nao_venda, null);
		setContentView(layout);
		
		try {
			mTxvData = (TextView) layout.findViewById(R.idDialogMotivoNaoVenda.txvDataNaoVenda);
			mTxvHora = (TextView) layout.findViewById(R.idDialogMotivoNaoVenda.txvHoraNaoVenda);
			mSpnMotivo = (Spinner) layout.findViewById(R.idDialogMotivoNaoVenda.spnMotivos);
			mBtnOk = (Button) layout.findViewById(R.idDialogMotivoNaoVenda.btOk);
			mBtnCancelar = (Button) layout.findViewById(R.idDialogMotivoNaoVenda.btCancelar);
		
			iniciarValoresDosComponentes();
			verificarAcessibilidadeDosComponentes();
			adicionarAcaoAosComponentes();
			
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
	}
	
	private void iniciarValoresDosComponentes() {
		try {
			Date dataAtual = new Date();
			mTxvData.setText(Utils.sdfDataPtBr.format(dataAtual));
			mTxvHora.setText(Utils.sdfHora.format(dataAtual));
			
			if (mMotivos != null)
				mSpnMotivo.setAdapter(new ArrayAdapter<NaoVenda>(mContext, android.R.layout.simple_dropdown_item_1line, mMotivos));
			
			setTitle(R.string.informe_o_motivo_de_nao_venda);
			
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
	}

	private void verificarAcessibilidadeDosComponentes() {
		boolean isCancelable = (!mParametro.isObrigatorioInformarNaoVenda() || !existemMotivos());
		
		mBtnCancelar.setVisibility(isCancelable ? View.VISIBLE : View.GONE);
		
		setCancelable(isCancelable);
		setCanceledOnTouchOutside(false);
	}

	private void adicionarAcaoAosComponentes() {
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				salvar();
			}
		});
		
		mBtnCancelar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dismiss();
			}
		});
	}
	
	private void salvar() {
		try {
			if (mSpnMotivo.getSelectedItem() == null) {
				Toast.makeText(mContext, "Informe um motivo", Toast.LENGTH_SHORT).show();
				return;
			}
			
			NaoVenda motivo = (NaoVenda) mSpnMotivo.getSelectedItem();
			
			// Salvar n�o venda movimento
			NaoVendaMovimento movimento = new NaoVendaMovimento();
			movimento.setCliente(mCliente);
			movimento.setDataHora(new Date());
			movimento.setMotivo(String.valueOf(motivo.getIdNaoVenda()));
			if (new NaoVendaMovimentoDAO(mContext, null).insert(movimento)) {
				gravarGeoPosicionamento(motivo);
				
				Toast.makeText(mContext, "Motivo salvo com sucesso!", Toast.LENGTH_LONG).show();
				dismiss();
			} else {
				Toast.makeText(mContext, R.string.falha_ao_salvar_motivo_de_nao_venda, Toast.LENGTH_LONG).show();
			}
			
		} catch (Exception e) {
			Toast.makeText(mContext, "Problema ao salvar: " + e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	private boolean existemMotivos() {
		return (mMotivos != null && mMotivos.size() > 0);
	}

	private void gravarGeoPosicionamento(NaoVenda pMotivo) {
		try {
			Mapeamento m = (Mapeamento) Utils.recuperarObjetoSerializado(mContext, LocationService.MAPEAMENTO_SERIALIZADO);
			if (m == null) {
				m = new Mapeamento();
				m.setIdVendedor(mParametro.getIdVendedor());
				m.setEnviado(false);
				m.setDataEnvio(new Date());
				m.setDataHora(new Date());
			}
			m.setIdPedido(0);
			m.setValorPedido(0d);
			m.setIdCliente(mCliente.getIdCliente());
			m.setNomeCliente(mCliente.getRazao());
			m.setMotivoNaoVenda(pMotivo.toString());
			// Inserir
			new MapeamentoDAO(mContext).insert(m, null);
			
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
	}
	
	private void iniciarServicoLocalizacao() {
		LocationService.iniciar(mContext, Principal.CATEGORIA,
				mCliente.getIdCliente(), mCliente.getRazao(),
				(short) mParametro.getIdVendedor());
	}
}
