package br.net.sav.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.AlterarObjeto;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.service.GeraPDFAsync;
import br.net.sav.service.GeraPDFAsync.ExecultaTask;
import br.net.sav.util.StringUtils;
import br.net.sav.utils.GeraPDFRentabilidade;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static br.net.sav.utils.ToastUtils.mostrarMensagem;

public class ValidaSenhaDialog extends Dialog {
    private Context mContext;
    private Activity mActivity;
    private Parametro mParametro;
    private Cliente cliente = null;
    private List<Item> itens = null;


    public ValidaSenhaDialog(Context mContext, Activity mActivity) {
        super(mContext);
        this.mContext = mContext;
        this.mActivity = mActivity;
        // Inicializar objetos
        mParametro = new ParametroDAO(mContext, null).get();

        if (mParametro == null) {
            Toast.makeText(mContext, R.string.parametros_nao_encontrados, Toast.LENGTH_LONG).show();
            dismiss();
        }
    }

    public ValidaSenhaDialog(Context mContext) {
        super(mContext);
        this.mContext = mContext;
        // Inicializar objetos
        mParametro = new ParametroDAO(mContext, null).get();

        if (mParametro == null) {
            Toast.makeText(mContext, R.string.parametros_nao_encontrados, Toast.LENGTH_LONG).show();
            dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showDialogSenhaDesbloquearPedidoRentabilidade(final Pedido pedido, final AlterarObjeto alterarObjeto) {
        buscarDados(pedido);
        String mensagem = String.format("Vendedor: %d\nCliente: %d\nPedido: %d\nPerc. Rentabilidade do Pedido Gerado: %.2f%%",
                mParametro.getIdVendedor(),
                pedido.getIDCliente(),
                pedido.getIDPedido(),
                pedido.getRentabilidade());

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.layout_inserir_senha, null);

        final EditText edtSenha = (EditText) dialogView.findViewById(R.id.dialogSenha_edtSenha);

        final Button btnCopiar = (Button) dialogView.findViewById(R.id.layout_inserir_senha_copiar);
        final Button btnColar = (Button) dialogView.findViewById(R.id.layout_inserir_senha_colar);

        edtSenha.setHint(mContext.getString(R.string.informe_a_senha));
        final AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
        dialog.setTitle(mContext.getString(R.string.senha_desbloquear_pedido_rentabilidade));
        dialog.setMessage(mensagem);
        dialog.setView(dialogView);
        dialog.setPositiveButton(mContext.getString(R.string.ok), null);
        dialog.setNeutralButton("Copiar", null);
        dialog.setNeutralButton("+ Compartilhar", null);
//        dialog.setNeutralButton("Colar", null);
        dialog.setNegativeButton(mContext.getString(R.string.cancelar), null);
        dialog.setCancelable(false);

        final AlertDialog dialogs = dialog.create();
        dialogs.show();
        dialogs.setCancelable(false);

        dialogs.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alterarObjeto.alterar(pedido, false);
                dialogs.dismiss();

            }
        });
        dialogs.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validarText(edtSenha)) {
                    return;
                }
                validar(edtSenha, pedido, alterarObjeto, dialogs);

            }
        });


        btnCopiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validaDados())
                    return;
                copiaDadosParaClipboard(pedido);
            }
        });

        btnColar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                String pasteData;
                assert clipboard != null;
                if (!(clipboard.hasPrimaryClip())) {
                    btnColar.setEnabled(false);
                } else if (!(Objects.requireNonNull(clipboard.getPrimaryClipDescription()).hasMimeType(MIMETYPE_TEXT_PLAIN))) {
                    btnColar.setEnabled(false);
                } else {
                    btnColar.setEnabled(true);
                }

                ClipData.Item item = Objects.requireNonNull(clipboard.getPrimaryClip()).getItemAt(0);

                pasteData = String.valueOf(item.getText());

                if (!pasteData.isEmpty()) {
                    edtSenha.setText(pasteData);
                }
            }
        });


        dialogs.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copiaDadosParaClipboard(pedido);

                new GeraPDFAsync(new ExecultaTask() {
                    @Override
                    public String iniciar() {
                        return gerarResumoDoPedido(pedido);
                    }

                    @Override
                    public void resultado(String nome) {
                        abrirActivityCompartilhar(nome, cliente);
                        alterarObjeto.alterar(pedido, false);
                    }
                }).execute();
            }
        });

    }

    private void copiaDadosParaClipboard(Pedido pedido) {
        String dados = mParametro.getIdVendedor() +
                " " +
                pedido.getIDCliente() +
                " " +
                pedido.getIDPedido() +
                " " +
                pedido.getRentabilidade();
        ClipboardManager clipboard = (ClipboardManager)
                getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        assert clipboard != null;

        ClipData clip = ClipData.newPlainText("Dados", dados);

        clipboard.setPrimaryClip(clip);

        Toast.makeText(getContext(), R.string.valida_senha_dialog_toast_copiado, Toast.LENGTH_LONG).show();
    }

    private boolean validaDados() {
        if (cliente == null) {
            mostrarMensagem(mContext, mContext.getString(R.string.cliente_nao_encontrados));
            return true;
        }
        if (itens == null) {
            mostrarMensagem(mContext, mContext.getString(R.string.itens_nao_encontrado));
            return true;
        }
        return false;
    }

    private void buscarDados(Pedido pedido) {
        itens = new ItemDAO(mContext, null).getByPedido(pedido.getIDPedido());
        try {
            cliente = new ClienteDAO(mContext, null).get(pedido.getIDCliente());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private String gerarResumoDoPedido(Pedido pedido) {
        ArrayList<String> linhas = new GeraPDFRentabilidade(mActivity).getLinhasPdfPedido(pedido, itens);
        return new GeraPDFRentabilidade(mActivity).gerarPdf(linhas, pedido.getIDPedido());
    }

    private void abrirActivityCompartilhar(String nomeArquivo, Cliente cliente) {
        Utils.abrirActivityCompartilhar(mActivity, "Pedido", new String[]{cliente.getEmail()}, nomeArquivo);
    }


    private void validar(EditText edtSenha, Pedido pedido, AlterarObjeto alterarObjeto, AlertDialog dialogs) {
        String percRentabilidade = GerarSenhaDialog.retirarPercentualSenha(edtSenha, pedido.getRentabilidade());

        if (Utils.isSenhaLiberarPedidoBloqueadoPorRentabilidade(StringUtils.getText(edtSenha), mParametro.getIdVendedor(), pedido.getIDCliente(), pedido.getIDPedido(), Long.valueOf(percRentabilidade))) {
            new PedidoDAO(mContext, null).alterarPercDeRentabilidadeLiberadoDoPedido(pedido, percRentabilidade);
            pedido.setPercRentabilidadeLiberado(Double.valueOf(percRentabilidade));
            alterarObjeto.alterar(pedido, true);
            mostrarMensagem(mContext, mContext.getString(R.string.pedido_liberado));
            dialogs.dismiss();
        } else {
            alterarObjeto.alterar(pedido, false);
            mostrarMensagem(mContext, mContext.getString(R.string.senha_invalida));
            dialogs.dismiss();
            return;
        }
    }

    private boolean validarText(EditText edtSenha) {
        if (StringUtils.editTextIsEmpty(edtSenha)) {
            edtSenha.setError(mContext.getString(R.string.informe_a_senha));
            return false;
        }

        if (StringUtils.getText(edtSenha).length() < 5) {
            edtSenha.setError("Senha possui mais de 5 digitos, digite novamente!");
            return false;
        }
        return true;
    }
}
