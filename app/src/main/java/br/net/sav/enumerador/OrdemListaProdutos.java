package br.net.sav.enumerador;

public enum OrdemListaProdutos {

	PRODUTOS_NOVOS(1, "Produtos Novos"), ORDEM_ALFABETICA(2, "Ordem Alfabetica");
	
	private int id;
	private String descricao;

	private OrdemListaProdutos(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
