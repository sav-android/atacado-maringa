package br.net.sav.enumerador;

public enum TipoOperacao {


    VENDA(1, "V"),
    TROCA(2, "T"),
    BONIFICACAO(3, "B");

    private Integer id;


    private String descricao;

    private TipoOperacao(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public static TipoOperacao get(Integer id) {
        switch (id) {
            case 1:
                return VENDA;
            case 2:
                return TROCA;
            case 3:
                return BONIFICACAO;
            default:
                return BONIFICACAO;
        }
    }

    @Override
    public String toString() {
        return descricao;
    }

    public Integer getId() {
        return id;
    }
}
