package br.net.sav.enumerador;

public enum TipoPessoaEnum {
	
    TIPO_PESSOA_FISICA(1, "Física"),
    TIPO_PESSOA_JURIDICA(2, "Jurídica");
    
	private int id;
	private String descricao;
	
	TipoPessoaEnum(int id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
    public static TipoPessoaEnum getByDescricao(String descricao) {
        for (TipoPessoaEnum tipo : values()) {
            if (tipo.descricao.equals(descricao)) {
                return tipo;
            }
        }
        return TIPO_PESSOA_JURIDICA;
    }
    
	public int getId() {
		return id;
	}
	
	public String getDescricao() {
	    return descricao;
	}
    
	@Override
	public String toString() {
		return descricao;
	}
}

