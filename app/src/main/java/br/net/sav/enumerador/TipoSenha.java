package br.net.sav.enumerador;

public enum TipoSenha {

    RENTABILIDADE_MINIMA("Gerar senha para pedido bloqueado por Rentabilidade Minima"),
    CLIENTE_BLOQUEADO("Gerar senha para cliente bloqueado"),
    PEDIDO_BLOQUEADO("Gerar senha para pedido bloqueado"),
    TRABALHAR_SEM_CONEXAO("Gerar senha para trabalhar sem conexao"),
    ACESSO_BLOQUEADO("Gerar senha para desbloquear Acesso ao Sav");

    private String descricao;

    private TipoSenha(String pDescricao) {
        this.descricao = pDescricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

    public static TipoSenha getById(int pId) {
        switch (pId) {
            case 0:
                return RENTABILIDADE_MINIMA;
            case 1:
                return CLIENTE_BLOQUEADO;
            case 2:
                return PEDIDO_BLOQUEADO;
            case 3:
                return TRABALHAR_SEM_CONEXAO;
            case 4:
                return ACESSO_BLOQUEADO;
            default:
                return null;
        }
    }
}

