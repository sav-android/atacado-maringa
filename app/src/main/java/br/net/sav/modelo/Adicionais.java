package br.net.sav.modelo;

import java.util.Date;

public class Adicionais {
	public long IdCliente;
	public String Fornecedor1;
	public String DDD1;
	public String Fone1;
	public String Fornecedor2;
	public String DDD2;
	public String Fone2;
	public String Fornecedor3;
	public String DDD3;
	public String Fone3;
	public String Banco;
	public String Agencia;
	public String Conta;
	public Date DataConstituicaoEmpresa;
	public short PredioProprio;
	public String ResponsavelCompras;
	public String ResponsavelPagamentos;
	public boolean Enviado;
	public Date DataEnvio;
	
	
	public long getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}
	public String getFornecedor1() {
		return Fornecedor1;
	}
	public void setFornecedor1(String fornecedor1) {
		Fornecedor1 = fornecedor1;
	}
	public String getDDD1() {
		return DDD1;
	}
	public void setDDD1(String dDD1) {
		DDD1 = dDD1;
	}
	public String getFone1() {
		return Fone1;
	}
	public void setFone1(String fone1) {
		Fone1 = fone1;
	}
	public String getFornecedor2() {
		return Fornecedor2;
	}
	public void setFornecedor2(String fornecedor2) {
		Fornecedor2 = fornecedor2;
	}
	public String getDDD2() {
		return DDD2;
	}
	public void setDDD2(String dDD2) {
		DDD2 = dDD2;
	}
	public String getFone2() {
		return Fone2;
	}
	public void setFone2(String fone2) {
		Fone2 = fone2;
	}
	public String getFornecedor3() {
		return Fornecedor3;
	}
	public void setFornecedor3(String fornecedor3) {
		Fornecedor3 = fornecedor3;
	}
	public String getDDD3() {
		return DDD3;
	}
	public void setDDD3(String dDD3) {
		DDD3 = dDD3;
	}
	public String getFone3() {
		return Fone3;
	}
	public void setFone3(String fone3) {
		Fone3 = fone3;
	}
	public String getBanco() {
		return Banco;
	}
	public void setBanco(String banco) {
		Banco = banco;
	}
	public String getAgencia() {
		return Agencia;
	}
	public void setAgencia(String agencia) {
		Agencia = agencia;
	}
	public String getConta() {
		return Conta;
	}
	public void setConta(String conta) {
		Conta = conta;
	}
	public Date getDataConstituicaoEmpresa() {
		return DataConstituicaoEmpresa;
	}
	public void setDataConstituicaoEmpresa(Date dataConstituicaoEmpresa) {
		DataConstituicaoEmpresa = dataConstituicaoEmpresa;
	}
	public short getPredioProprio() {
		return PredioProprio;
	}
	public void setPredioProprio(short predioProprio) {
		PredioProprio = predioProprio;
	}
	public String getResponsavelCompras() {
		return ResponsavelCompras;
	}
	public void setResponsavelCompras(String responsavelCompras) {
		ResponsavelCompras = responsavelCompras;
	}
	public String getResponsavelPagamentos() {
		return ResponsavelPagamentos;
	}
	public void setResponsavelPagamentos(String responsavelPagamentos) {
		ResponsavelPagamentos = responsavelPagamentos;
	}
	public boolean isEnviado() {
		return Enviado;
	}
	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}
	public Date getDataEnvio() {
		return DataEnvio;
	}
	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}
	

}
