package br.net.sav.modelo;

public class AliquotaICMS {
	
	public String UfDestino ;
	public short CodigoICMS ;
	public short TipoPessoa ;
	public double Aliquota ;
	public String getUfDestino() {
		return UfDestino;
	}
	public void setUfDestino(String ufDestino) {
		UfDestino = ufDestino;
	}
	public short getCodigoICMS() {
		return CodigoICMS;
	}
	public void setCodigoICMS(short codigoICMS) {
		CodigoICMS = codigoICMS;
	}
	public short getTipoPessoa() {
		return TipoPessoa;
	}
	public void setTipoPessoa(short tipoPessoa) {
		TipoPessoa = tipoPessoa;
	}
	public double getAliquota() {
		return Aliquota;
	}
	public void setAliquota(double aliquota) {
		Aliquota = aliquota;
	}				
}
