package br.net.sav.modelo;

public interface AlterarObjeto {
    void alterar(Pedido pedido, boolean alterado);
}
