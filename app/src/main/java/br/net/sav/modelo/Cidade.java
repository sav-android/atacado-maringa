package br.net.sav.modelo;

public class Cidade {
	public String Cidade;
	public int id;
	public int valor;
	public String uf;
	public String nome;
	
	public int getId(){
		return id;
	}
	
	public String getCidade() {
		return Cidade;
	}

	public void setCidade(String cidade) {
		Cidade = cidade;
	}
}
