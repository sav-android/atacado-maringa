package br.net.sav.modelo;

import java.io.Serializable;
import java.util.Date;

import br.net.sav.Utils;
import br.net.sav.util.StringUtils;

import static java.lang.String.format;

public class Cliente implements Serializable {
	public static final String CONSULTA_CLIENTE_PREFS = "ConsultaClientePrefs", EXTRA_IDCLIENTE = "IdCliente";
	
	public long IdCliente ; 
	public String Razao ;
	public String Fantasia ;   
	public short Ramo ;
	public String TipoEndereco ;
    public String Endereco ;
    public String Complemento ;
    public String Bairro ;
    public String Cidade ;
    public String UF ;
    public String CEP ;
    public short DDD ;
    public long Telefone ;
    public short TipoPessoa ;
    public String CnpjCpf ;
    public String IeRg ;
    public String Zona ;
    public String Setor ;
    public String Roteiro ;
    public String Email ;
    public boolean PedidoPendente ;
    public boolean Enviado ;
    public Date DataEnvio ;
    public boolean Visivel ;
    public String  StatusBloqueio;
    public int Numero;
    public int StatusCm;
    //Campos novos;
    public String Flag; // ""NOVO / "A" Alterado / "E" Excluir;
    public long CodRepresentante;
    public int CodigoContribuinte;

	public int getCodigoContribuinte() {
		return CodigoContribuinte;
	}

	public void setCodigoContribuinte(int codigoContribuinte) {
		CodigoContribuinte = codigoContribuinte;
	}

	public int getStatusCm() {
		return StatusCm;
	}

	public void setStatusCm(int statusCm) {
		StatusCm = statusCm;
	}

	public int getNumero() {
		return Numero;
	}

	public void setNumero(int numero) {
		Numero = numero;
	}

	public long getCodRepresentante() {
		return CodRepresentante;
	}

	public void setCodRepresentante(long codRepresentante) {
		this.CodRepresentante = codRepresentante;
	}

	// Propriedades Auxiliar
    public Date VencimentoTituloMaisAntigo ;
    
	public String getStatusBloqueio() {
		return StatusBloqueio;
	}

	public void setStatusBloqueio(String statusBloqueio) {
		StatusBloqueio = statusBloqueio;
	}

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}

	public String getRazao() {
		return Razao;
	}

	public void setRazao(String razao) {
		Razao = razao;
	}

	public String getFantasia() {
		return Fantasia;
	}

	public void setFantasia(String fantasia) {
		Fantasia = fantasia;
	}

	public short getRamo() {
		return Ramo;
	}

	public void setRamo(short ramo) {
		Ramo = ramo;
	}

	public String getTipoEndereco() {
		return TipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		TipoEndereco = tipoEndereco;
	}

	public String getEndereco() {
		return Endereco;
	}

	public void setEndereco(String endereco) {
		Endereco = endereco;
	}

	public String getComplemento() {
		return Complemento;
	}

	public void setComplemento(String complemento) {
		Complemento = complemento;
	}

	public String getBairro() {
		return Bairro;
	}

	public void setBairro(String bairro) {
		Bairro = bairro;
	}

	public String getCidade() {
		return Cidade;
	}

	public void setCidade(String cidade) {
		Cidade = cidade;
	}

	public String getUF() {
		return (UF != null) ? UF : "";
	}
	

	public void setUF(String uF) {
		UF = uF;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public short getDDD() {
		return DDD;
	}

	public void setDDD(short dDD) {
		DDD = dDD;
	}

	public long getTelefone() {
		return Telefone;
	}

	public void setTelefone(long telefone) {
		Telefone = telefone;
	}

	public short getTipoPessoa() {
		return TipoPessoa;
	}

	public void setTipoPessoa(short tipoPessoa) {
		TipoPessoa = tipoPessoa;
	}

	public String getCnpjCpf() {
		return CnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		CnpjCpf = cnpjCpf;
	}

	public String getIeRg() {
		return IeRg;
	}

	public void setIeRg(String ieRg) {
		IeRg = ieRg;
	}

	public String getZona() {
		return Zona;
	}

	public void setZona(String zona) {
		Zona = zona;
	}

	public String getSetor() {
		return Setor;
	}

	public void setSetor(String setor) {
		Setor = setor;
	}

	public String getRoteiro() {
		return Roteiro;
	}

	public void setRoteiro(String roteiro) {
		Roteiro = roteiro;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public boolean isPedidoPendente() {
		return PedidoPendente;
	}

	public void setPedidoPendente(boolean pedidoPendente) {
		PedidoPendente = pedidoPendente;
	}

	public boolean isEnviado() {
		return Enviado;
	}

	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

	public boolean isVisivel() {
		return Visivel;
	}

	public void setVisivel(boolean visivel) {
		Visivel = visivel;
	}

	public Date getVencimentoTituloMaisAntigo() {
		return VencimentoTituloMaisAntigo;
	}

	public void setVencimentoTituloMaisAntigo(Date vencimentoTituloMaisAntigo) {
		VencimentoTituloMaisAntigo = vencimentoTituloMaisAntigo;
	}

	public String getFlagCliente() {
		return Flag;
	}

	public void setFlagCliente(String flagCliente) {
		this.Flag = flagCliente;
	}

	public String toLinhaTexto() {
		StringBuilder builder = new StringBuilder();
		String campo;// IdCliente 1
		try {
			builder.append(format("%09d", getIdCliente()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Razao Social 2
		try {
			builder.append(format("%-50s", Utils.removerCaracteres(getRazao())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Nome Fantasia 3
		try {
			builder.append(format("%-30s", Utils.removerCaracteres(getFantasia())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Ramo 4
		try {
			builder.append(format("%04d", getRamo()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Tipo Endere�o 5
		try {
			builder.append(format("%-20s", Utils
					.removerCaracteresEspeciais(getTipoEndereco())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Endere�o 6
		try {
			builder.append(format("%-60s", Utils.removerCaracteres(getEndereco())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Complemento 7
		try {
			builder.append(format("%-60s", Utils.removerCaracteres(getComplemento())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Bairro 8
		try {
			builder.append(format("%-20s", Utils.removerCaracteres(getBairro())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Cidade 9
		try {
			builder.append(format("%-30s", Utils.removerCaracteres(getCidade())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// UF 10
		try {
			builder.append(format("%-2s",
					Utils.removerCaracteresEspeciais(getUF())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// CEP 11
			if(!getCEP().contains("-")){
				setCEP(getCEP().substring(0, 5) + "-" + getCEP().substring(5));
			}

			builder.append(format("%-9s", getCEP()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		// DDD 12
		try {
			builder.append(format("%02d", getDDD()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Telefone 13
		try {
			builder.append(format("%09d", getTelefone()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// TipoPessoa 14
		try {
			builder.append(format("%01d", getTipoPessoa()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// CNPJ - CPF 15
		try {
			builder.append(format("%-14s", Utils
					.removerCaracteresEspeciais(getCnpjCpf())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// IE - RG 16
		try {
			builder.append(format("%-16s", Utils
							.removerCaracteresEspeciais(getIeRg())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Flag Cliente 17
		try {
			builder.append(format("%-1s", Utils
					.removerCaracteresEspeciais(getFlagCliente())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Zona 18
		try {
			builder.append(format("%-3s", Utils.removerCaracteresEspeciais(
									getZona())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Setor 19
		try {
			builder.append(format("%-3s", Utils
					.removerCaracteresEspeciais(getSetor())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Roteiro 20
		try {
			builder.append(format("%-3s", Utils
					.removerCaracteresEspeciais(getRoteiro())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Email 20
		try {
			builder.append(format("%-40s", Utils
					.removerCaracteresEspeciais(getEmail())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Codigo Representante 21
		try {
			builder.append(format("%08d", getCodRepresentante()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// numero endereco
		try {
			builder.append(format("%08d", getNumero()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		builder.append(StringUtils.QUEBRA_LINHA);
		return builder.toString();
	}
	
	
	
}
