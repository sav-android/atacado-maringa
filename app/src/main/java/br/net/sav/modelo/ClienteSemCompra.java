package br.net.sav.modelo;

import java.util.Date;

public class ClienteSemCompra {
	public long IdCliente;
	public String Razao;
	public int QtdeDias;
	public Date UltimaCompra;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}
	
	public String getRazao() {
		return Razao;
	}

	public void setRazao(String razao) {
		Razao = razao;
	}

	public int getQtdeDias() {
		return QtdeDias;
	}

	public void setQtdeDias(int qtdeDias) {
		QtdeDias = qtdeDias;
	}

	public Date getUltimaCompra() {
		return UltimaCompra;
	}

	public void setUltimaCompra(Date ultimaCompra) {
		UltimaCompra = ultimaCompra;
	}

}
