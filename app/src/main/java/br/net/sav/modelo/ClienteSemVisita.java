package br.net.sav.modelo;

import java.util.Date;

public class ClienteSemVisita {
	private long IdCliente ;
	private String Razao ;
	private int QtdeDias ;
	private Date UltimaVisita ;
	
	public long getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}
	public String getRazao() {
		return Razao;
	}
	public void setRazao(String razao) {
		Razao = razao;
	}
	public int getQtdeDias() {
		return QtdeDias;
	}
	public void setQtdeDias(int qtdeDias) {
		QtdeDias = qtdeDias;
	}
	public Date getUltimaVisita() {
		return UltimaVisita;
	}
	public void setUltimaVisita(Date ultimaVisita) {
		UltimaVisita = ultimaVisita;
	}
	
	
}
