package br.net.sav.modelo;

import android.content.Context;
import android.database.Cursor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.ComboController.ComboControle;
import br.net.sav.ComboController.factory.RegraComboFactory;
import br.net.sav.IntegradorWeb.dto.ComboDTO;
import br.net.sav.IntegradorWeb.dto.RegraComboDTO;
import br.net.sav.atacadomaringa.R;
import br.net.sav.comboControllerSav.RegraAplicavel;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComboDAO;
import br.net.sav.dao.RegraComboDAO;
import br.net.sav.utils.ToastUtils;


public class Combo extends br.net.sav.comboControllerSav.ModeloDTO.Combo {
    public List<RegraCompra> cbRegraCompras = null;
    public List<RegraGanhe> cbRegraGanhes = null;
    public List<RegraLimite> cbRegraLimites = null;
    public List<RegraExcecao> cbRegraExcecoes = null;
    public static boolean comboParaCheckboxVisivel = false;
    private boolean comboAtivadoMsg;

    public Combo() {
    }

    public Combo(Cursor cursor) {
        super(cursor);
        cbRegraCompras = (List<RegraCompra>) regraCompras;
        cbRegraGanhes = (List<RegraGanhe>) regraGanhes;
        cbRegraLimites = (List<RegraLimite>) regraLimites;
        cbRegraExcecoes = (List<RegraExcecao>) regraExcecoes;
    }

    public Combo(ComboDTO combo) {
        super(combo);
    }

    public boolean isComboAtivadoMsg() {
        return comboAtivadoMsg;
    }

    public void setComboAtivadoMsg(boolean comboAtivadoMsg) {
        this.comboAtivadoMsg = comboAtivadoMsg;
    }

    @Override
    public int contagemRegraLimite(Object pedido, Context context) {
        Pedido pedido1 = (Pedido) pedido;
        Integer contagemAnterior = null;

        for (RegraLimite regraLimite : cbRegraLimites) {
            int contagem = regraLimite.quantidadeLimiteRestante(this, pedido1, context);
            if (contagemAnterior == null) {
                contagemAnterior = contagem;
            } else {
                contagemAnterior = contagemAnterior >= contagem ? contagemAnterior : contagem;
            }
        }

        return contagemAnterior == null ? 0 : contagemAnterior;
    }

    @Override
    public Integer contagemRegraCompraSatisfeita(List<?> itensPedido) {
        List<ItemDigitacao> itens = (List<ItemDigitacao>) itensPedido;
        Integer contagemAnterior = null;

        for (RegraCompra regraCompra : cbRegraCompras) {
            int contagem = regraCompra.QuantasVezesARegraFoiStisfeita(itens, new RegraCompra.Excecao() {
                @Override
                public boolean ehExcecao(Object item) {
                    for (RegraExcecao regraExcecao : cbRegraExcecoes)
                        if (regraExcecao.ehExcecao(item))
                            return true;
                    return false;
                }
            });
            if (regraCompra.regraMultiplicadora()) {
                if (contagemAnterior == null) {
                    contagemAnterior = contagem;
                } else {
                    contagemAnterior = contagemAnterior < contagem ? contagemAnterior : contagem;
                }
            }


        }
        return contagemAnterior;
    }

    @Override
    public void addRegraCompra(Object regraCompra) {
        cbRegraCompras.add((RegraCompra) regraCompra);
    }

    @Override
    public void addRegraGanhe(Object regraGanhe) {
        cbRegraGanhes.add((RegraGanhe) regraGanhe);
    }

    @Override
    public void addRegraLimite(Object regraLimite) {
        cbRegraLimites.add((RegraLimite) regraLimite);
    }

    @Override
    public void addRegraExcecao(Object regraExcecao) {
        cbRegraCompras.add((RegraCompra) regraExcecao);
    }

    @Override
    public boolean satisfazRegraCompraSav(List<?> itensPedido) {
        List<ItemDigitacao> itens = (List<ItemDigitacao>) itensPedido;

        if (cbRegraCompras.isEmpty())
            return false;
        for (RegraCompra regraCompra : cbRegraCompras) {
            if (!regraCompra.satisfazRegra(itens, new RegraCompra.Excecao() {
                @Override
                public boolean ehExcecao(Object item) {
                    for (RegraExcecao regraExcecao : cbRegraExcecoes)
                        if (regraExcecao.ehExcecao(item)) {
                            return true;
                        }
                    return false;
                }
            }))
                return false;
        }
        return true;
    }

    @Override
    public boolean satisfazRegraLimiteSav(Object pedido, Context context) {

        for (RegraLimite regraLimite : cbRegraLimites)
            if (!regraLimite.satisfazRegraLimite(this, pedido, context))
                return false;
        return true;
    }

    @Override
    public List<?> getItensComDesconto(Context ctx, List<?> itens) {
        List<ItemDigitacao> itensPedido = (List<ItemDigitacao>) itens;
        ArrayList<ItemDigitacao> itemDigitacaoRetorno = new ArrayList<ItemDigitacao>();
        for (RegraGanhe regraGanhe : cbRegraGanhes) {
            List<?> itemComDesconto = regraGanhe.RetornarItemComDesconto(ctx, itensPedido);
            itemDigitacaoRetorno.addAll((List<ItemDigitacao>) itemComDesconto);
        }
        return itemDigitacaoRetorno;
    }

    @Override
    public List<?> getItensSemDesconto(Context ctx, List<?> itens) {
        List<ItemDigitacao> itensPedido = (List<ItemDigitacao>) itens;
        ArrayList<ItemDigitacao> itemDigitacaoRetorno = new ArrayList<ItemDigitacao>();

        for (RegraGanhe regraGanhe : cbRegraGanhes) {
            List<?> itemSemDesconto = regraGanhe.RetornarItemSemDesconto(ctx, itensPedido);
            itemDigitacaoRetorno.addAll((List<ItemDigitacao>) itemSemDesconto);
        }
        return itemDigitacaoRetorno;
    }


    public void sincronizaCombo(Context ctx, List<ComboDTO> recebeListaCombo) {
        boolean comboAtualizou = false;
        ComboDAO comboDAO = new ComboDAO(ctx);
        RegraComboDAO regraComboDAO = new RegraComboDAO(ctx);

        for (ComboDTO combo : recebeListaCombo) {

            if (combo.getRegraCombos().isEmpty() && combo.getCod() > 0 && combo.isAtivo()) {
                ToastUtils.mostrarMensagem(ctx, String.format(ctx.getString(R.string.combo_sem_regra), combo.getCod()));
                continue;
            }

            comboAtualizou = atualizaCombo(ctx, comboDAO, combo, comboAtualizou);

            for (RegraComboDTO regra : combo.getRegraCombos()) {

                atualizarRegra(regraComboDAO, regra);
            }
        }
        if (comboAtualizou)
        ToastUtils.mostrarMensagem(ctx, "COMBO Atualizado com sucesso!");


    }

    private boolean atualizaCombo(Context ctx, ComboDAO comboDAO, ComboDTO combo, boolean atualiza) {
        Combo combo1 = new Combo(combo);
        if (comboDAO.existeCombo(combo1)) {
            if (combo.isAtivo()) {
                comboDAO.update(combo1, null);
            } else {
                comboDAO.delete(combo1, null);
            }
        } else {
            if (combo1.isAtivo()) {
                comboDAO.insert(null, combo1);
                atualiza = true;
            }
        }

        return atualiza;
    }

    private void atualizarRegra(RegraComboDAO regraComboDAO, RegraComboDTO regra) {
        RegraCombo regraCombo = new RegraCombo(regra);
        if (regraComboDAO.existeRegra(regraCombo)) {

            if (regra.isAtivo()) {
                regraComboDAO.update(regraCombo, null);
            } else {
                regraComboDAO.delete(regraCombo, null);
            }
        } else {
            if (regra.isAtivo()) {
                regraComboDAO.insert(regraCombo, null);
            }
        }
    }

    public static List<RegraAplicavel> buscarCombos(Context ctx, Pedido pedido) {
        Cliente cliente = null;
        if (pedido != null) {
            try {
                cliente = new ClienteDAO(ctx, null).get(pedido.getIDCliente());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return new ComboControle().getComboValidos(ctx, cliente, new RegraComboFactory());
    }
}
