package br.net.sav.modelo;

import android.content.ContentValues;
import android.database.Cursor;

public class ComboXCliente {
    public static final String TABELA = "ComboXCliente";
    public static final String ID_COMBO = "IdCombo";
    public static final String ID_CLIENTE = "IdCliente";
    public static final String FORMAT_6_POSICOES = "%06d";
    private static final  String MATCHES = "^[0-9]*";
    public static String[] colunas = {ID_COMBO, ID_CLIENTE};
    private long idCombo;
    private long idCliente;

    public ComboXCliente(Cursor cursor) {
        idCombo = cursor.getLong(cursor.getColumnIndex(ID_COMBO));
        idCliente = cursor.getLong(cursor.getColumnIndex(ID_CLIENTE));
    }

    public ComboXCliente(long idCombo, long idCliente) {
        this.idCombo = idCombo;
        this.idCliente = idCliente;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_COMBO, idCombo);
        contentValues.put(ID_CLIENTE, idCliente);
        return contentValues;
    }
}
