package br.net.sav.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.Date;

import br.net.sav.dao.ComboXPedidoDAO;

public class ComboXPedido extends br.net.sav.comboControllerSav.ModeloDTO.ComboXPedido {
    public ComboXPedido(Cursor cursor) {
        super(cursor);
    }
    public ComboXPedido(long idCombo, long idCliente, long idPedido, Date dataPedido) {
        super(idCombo,idCliente,idPedido,dataPedido);
    }

    public static void gravarCombos(Context ctx, Pedido pedido) {
        if (!pedido.getCombos().isEmpty()) {
            for (Combo combo : pedido.getCombos()) {
                ComboXPedido comboXPedido = new ComboXPedido(combo.getIdCombo(), pedido.getIDCliente(), pedido.getIDPedido(), pedido.getDataPedido());
               if(new ComboXPedidoDAO(ctx).existeComboXPedido(comboXPedido)){
                   new ComboXPedidoDAO(ctx).update(comboXPedido,null);
               }else {
                   new ComboXPedidoDAO(ctx).insert(comboXPedido);
               }
            }
        }
    }
}
