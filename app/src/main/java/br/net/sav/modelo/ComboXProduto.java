package br.net.sav.modelo;

import java.io.Serializable;

public class ComboXProduto implements Serializable {

    private long IdCombo;
    private long IdProduto;
    private long IdCondicao;

    public ComboXProduto() { }

    public long getIdCombo() {
        return IdCombo;
    }

    public void setIdCombo(long idCombo) {
        IdCombo = idCombo;
    }

    public long getIdProduto() {
        return IdProduto;
    }

    public void setIdProduto(long idProduto) {
        IdProduto = idProduto;
    }

    public long getIdCondicao() {
        return IdCondicao;
    }

    public void setIdCondicao(long idCondicao) {
        IdCondicao = idCondicao;
    }
}
