package br.net.sav.modelo;

public class Comissao {
	private short Tipo;
	private String Periodo;
	private double TotalVendas;
	private double TotalComissao;

	public short getTipo() {
		return Tipo;
	}

	public void setTipo(short tipo) {
		Tipo = tipo;
	}

	public String getPeriodo() {
		return Periodo;
	}

	public void setPeriodo(String periodo) {
		Periodo = periodo;
	}

	public double getTotalVendas() {
		return TotalVendas;
	}

	public void setTotalVendas(double totalVendas) {
		TotalVendas = totalVendas;
	}

	public double getTotalComissao() {
		return TotalComissao;
	}

	public void setTotalComissao(double totalComissao) {
		TotalComissao = totalComissao;
	}

}
