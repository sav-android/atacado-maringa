package br.net.sav.modelo;

public class ComissaoV {
	public short IdTabelaPreco;
	public long IdProduto;
	public double DescontoFinal;
	public double DescontoInicial;
	public double Comissao;
	
	public short getIdTabelaPreco() {
		return IdTabelaPreco;
	}
	public void setIdTabelaPreco(short idTabelaPreco) {
		IdTabelaPreco = idTabelaPreco;
	}
	public long getIdProduto() {
		return IdProduto;
	}
	public void setIdProduto(long idProduto) {
		IdProduto = idProduto;
	}
	public double getDescontoFinal() {
		return DescontoFinal;
	}
	public void setDescontoFinal(double descontoFinal) {
		DescontoFinal = descontoFinal;
	}
	public double getDescontoInicial() {
		return DescontoInicial;
	}
	public void setDescontoInicial(double descontoInicial) {
		DescontoInicial = descontoInicial;
	}
	public double getComissao() {
		return Comissao;
	}
	public void setComissao(double comissao) {
		Comissao = comissao;
	}
}
