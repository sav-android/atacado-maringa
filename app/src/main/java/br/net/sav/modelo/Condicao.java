package br.net.sav.modelo;

public class Condicao {
	public short IdCondicao;
	public String Descricao;
	public double TaxaFinanceira;
	public double LimiteMinimoPedido;
	public short IdTabela;
	public short IdEmpresa;
	public short IdFilial;
	public String TipoDocumento;
	public short QtdeDiasPrazo;
	public double PercDescontoMaximo;
	public short QtdeParcelas;
	public boolean CondicaoEspecial;

	public short getIdCondicao() {
		return IdCondicao;
	}

	public void setIdCondicao(short idCondicao) {
		IdCondicao = idCondicao;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public double getTaxaFinanceira() {
		return TaxaFinanceira;
	}

	public void setTaxaFinanceira(double taxaFinanceira) {
		TaxaFinanceira = taxaFinanceira;
	}

	public double getLimiteMinimoPedido() {
		return LimiteMinimoPedido;
	}

	public void setLimiteMinimoPedido(double limiteMinimoPedido) {
		LimiteMinimoPedido = limiteMinimoPedido;
	}

	public short getIdTabela() {
		return IdTabela;
	}

	public void setIdTabela(short idTabela) {
		IdTabela = idTabela;
	}

	public short getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(short idEmpresa) {
		IdEmpresa = idEmpresa;
	}

	public short getIdFilial() {
		return IdFilial;
	}

	public void setIdFilial(short idFilial) {
		IdFilial = idFilial;
	}

	public String getTipoDocumento() {
		return TipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		TipoDocumento = tipoDocumento;
	}

	public short getQtdeDiasPrazo() {
		return QtdeDiasPrazo;
	}

	public void setQtdeDiasPrazo(short qtdeDiasPrazo) {
		QtdeDiasPrazo = qtdeDiasPrazo;
	}

	public double getPercDescontoMaximo() {
		return PercDescontoMaximo;
	}

	public void setPercDescontoMaximo(double percDescontoMaximo) {
		PercDescontoMaximo = percDescontoMaximo;
	}

	public short getQtdeParcelas() {
		return QtdeParcelas;
	}

	public void setQtdeParcelas(short qtdeParcelas) {
		QtdeParcelas = qtdeParcelas;
	}

	public boolean isCondicaoEspecial() {
		return CondicaoEspecial;
	}

	public void setCondicaoEspecial(boolean condicaoEspecial) {
		CondicaoEspecial = condicaoEspecial;
	}

	@Override
	public String toString() {
		return String.format("%d - %s", IdCondicao, Descricao);
	}

}
