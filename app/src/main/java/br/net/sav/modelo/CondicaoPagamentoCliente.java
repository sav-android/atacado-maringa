package br.net.sav.modelo;

public class CondicaoPagamentoCliente {
	private long idCliente;
	private short idCondicao;

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public short getIdCondicao() {
		return idCondicao;
	}

	public void setIdCondicao(short idCondicao) {
		this.idCondicao = idCondicao;
	}
	
	public enum OpcoesTransacao {
		INSERIR, ALTERAR, DELETAR, DELETAR_TUDO, EXISTE ;
	}
}
