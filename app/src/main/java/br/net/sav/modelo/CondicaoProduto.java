package br.net.sav.modelo;

public class CondicaoProduto {
	public short IdCondicao;
	public long IdProduto;

	public short getIdCondicao() {
		return IdCondicao;
	}

	public void setIdCondicao(short idCondicao) {
		IdCondicao = idCondicao;
	}

	public long getIdProduto() {
		return IdProduto;
	}

	public void setIdProduto(long idProduto) {
		IdProduto = idProduto;
	}

}
