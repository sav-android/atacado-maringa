package br.net.sav.modelo;

public class ConsultaPedidoVendaProduto {
	public long IdProduto;
	public String DescricaoProduto;
	public int QtdePedidos;
	public double QtdeVendida;
	public double ValorBruto;
	public double ValorLiquido;
	public double Peso;

	public long getIdProduto() {
		return IdProduto;
	}

	public void setIdProduto(long idProduto) {
		IdProduto = idProduto;
	}

	public String getDescricaoProduto() {
		return DescricaoProduto;
	}

	public void setDescricaoProduto(String descricaoProduto) {
		DescricaoProduto = descricaoProduto;
	}

	public int getQtdePedidos() {
		return QtdePedidos;
	}

	public void setQtdePedidos(int qtdePedidos) {
		QtdePedidos = qtdePedidos;
	}

	public double getQtdeVendida() {
		return QtdeVendida;
	}

	public void setQtdeVendida(double qtdeVendida) {
		QtdeVendida = qtdeVendida;
	}

	public double getValorBruto() {
		return ValorBruto;
	}

	public void setValorBruto(double valorBruto) {
		ValorBruto = valorBruto;
	}

	public double getValorLiquido() {
		return ValorLiquido;
	}

	public void setValorLiquido(double valorLiquido) {
		ValorLiquido = valorLiquido;
	}

	public double getPeso() {
		return Peso;
	}

	public void setPeso(double peso) {
		Peso = peso;
	}
}
