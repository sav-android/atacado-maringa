package br.net.sav.modelo;

import java.util.Date;

public class Contato {
	public long IdCliente;
	public String Nome;
	public String Hobby;
	public String Time;
	public String DDD;
	public String Telefone;
	public Date Aniversario;
	public String Cargo;
	public String Email;
	public boolean Enviado;
	public Date DataEnvio;
	public long CodRepresentante;

	// aux
	public String NomeAnterior;
	public short Idade;
	public String Razao;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getHobby() {
		return Hobby;
	}

	public void setHobby(String hobby) {
		Hobby = hobby;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public String getDDD() {
		return DDD;
	}

	public void setDDD(String dDD) {
		DDD = dDD;
	}

	public String getTelefone() {
		return Telefone;
	}

	public void setTelefone(String telefone) {
		Telefone = telefone;
	}

	public Date getAniversario() {
		return Aniversario;
	}

	public void setAniversario(Date aniversario) {
		Aniversario = aniversario;
	}

	public String getCargo() {
		return Cargo;
	}

	public void setCargo(String cargo) {
		Cargo = cargo;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public boolean isEnviado() {
		return Enviado;
	}

	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

	public String getNomeAnterior() {
		return NomeAnterior;
	}

	public void setNomeAnterior(String nomeAnterior) {
		NomeAnterior = nomeAnterior;
	}

	public short getIdade() {
		return Idade;
	}

	public void setIdade(short idade) {
		Idade = idade;
	}

	public String getRazao() {
		return Razao;
	}

	public void setRazao(String razao) {
		Razao = razao;
	}

	public long getCodRepresentante() {
		return CodRepresentante;
	}

	public void setCodRepresentante(long codRepresentante) {
		this.CodRepresentante = codRepresentante;
	}
	
	

}
