package br.net.sav.modelo;

public class DescontoF {
	public short IdFornecedor;
	public long IdCliente;
	public double Desconto;

	public short getIdFornecedor() {
		return IdFornecedor;
	}

	public void setIdFornecedor(short idFornecedor) {
		IdFornecedor = idFornecedor;
	}

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

}
