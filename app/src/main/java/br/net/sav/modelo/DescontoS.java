package br.net.sav.modelo;

public class DescontoS {
	public long IdCliente;
	public short Linha;
	public short Secao;
	public short Grupo;
	public short SubGrupo;
	public double Desconto;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long iDCliente) {
		IdCliente = iDCliente;
	}

	public short getLinha() {
		return Linha;
	}

	public void setLinha(short linha) {
		Linha = linha;
	}

	public short getSecao() {
		return Secao;
	}

	public void setSecao(short secao) {
		Secao = secao;
	}

	public short getGrupo() {
		return Grupo;
	}

	public void setGrupo(short grupo) {
		Grupo = grupo;
	}

	public short getSubGrupo() {
		return SubGrupo;
	}

	public void setSubGrupo(short subGrupo) {
		SubGrupo = subGrupo;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

}
