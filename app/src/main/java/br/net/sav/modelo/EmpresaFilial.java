package br.net.sav.modelo;

public class EmpresaFilial {
	public short IdEmpresa;
	public short IdFilial;
	public String Descricao;

	public short getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(short idEmpresa) {
		IdEmpresa = idEmpresa;
	}

	public short getIdFilial() {
		return IdFilial;
	}

	public void setIdFilial(short idFilial) {
		IdFilial = idFilial;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public static void getFilialDiferente(EmpresaFilial filial, Pedido pedido) {
		if (filial != null){
			if (pedido.getIDFilial() != filial.getIdFilial()){
				pedido.setIDEmpresa(filial.getIdEmpresa());
				pedido.setIDFilial(filial.getIdFilial());
			}
		}
	}

}
