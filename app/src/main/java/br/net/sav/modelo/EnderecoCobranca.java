package br.net.sav.modelo;

import java.util.Date;

public class EnderecoCobranca {
	public long IdCliente;
	public String TipoEndereco;
	public String Endereco;
	public String Complemento;
	public String Bairro;
	public String Cidade;
	public String Uf;
	public long    Cep;
	public boolean Enviado;
	public Date DataEnvio;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long iDCliente) {
		IdCliente = iDCliente;
	}

	public String getTipoEndereco() {
		return TipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		TipoEndereco = tipoEndereco;
	}

	public String getEndereco() {
		return Endereco;
	}

	public void setEndereco(String endereco) {
		Endereco = endereco;
	}

	public String getComplemento() {
		return Complemento;
	}

	public void setComplemento(String complemento) {
		Complemento = complemento;
	}

	public String getBairro() {
		return Bairro;
	}

	public void setBairro(String bairro) {
		Bairro = bairro;
	}

	public String getCidade() {
		return Cidade;
	}

	public void setCidade(String cidade) {
		Cidade = cidade;
	}

	public String getUf() {
		return Uf;
	}

	public void setUf(String uf) {
		Uf = uf;
	}

	public long getCep() {
		return Cep;
	}

	public void setCep(long cep) {
		Cep = cep;
	}

	public boolean isEnviado() {
		return Enviado;
	}

	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

}
