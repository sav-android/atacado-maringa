package br.net.sav.modelo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.LocationService;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.PedidoListaActivity;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.dialog.DialogCustom;
import br.net.sav.util.dao.MapeamentoDAO;
import br.net.sav.util.modelo.Mapeamento;
import br.net.sav.utils.ShearedPreferenceUltils;
import br.net.sav.utils.ToastUtils;

import static android.app.Activity.RESULT_OK;
import static br.net.sav.atacadomaringa.ClienteListaActivity.isSalvo;
import static br.net.sav.atacadomaringa.ComboAdapter.acaoCombo;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR_ALTERAR;
import static br.net.sav.atacadomaringa.ItemTabActivity.itemPedido;
import static br.net.sav.atacadomaringa.ItemTabActivity.mIsInicializandoEstruturas;
import static br.net.sav.atacadomaringa.PedidoTabActivity.PEDIDO_SERIALIZAR;
import static br.net.sav.atacadomaringa.PedidoTabActivity.PEDIDO_SERIALIZAR_ALTERAR;
import static br.net.sav.atacadomaringa.PedidoTabActivity.PEDIDO_SERIALIZAR_OP;
import static br.net.sav.utils.ComponentePedidoUtils.isSemRestricaoTCT;
import static br.net.sav.utils.ShearedPreferenceUltils.isCondicaoPagamentoValido;
import static br.net.sav.utils.ShearedPreferenceUltils.isFormaPagamentoValido;
import static br.net.sav.utils.ShearedPreferenceUltils.isTipoPedidoValido;

public class EstruturaGravarPedido implements Serializable {

    private Pedido pedido;
    private ArrayList<ItemDigitacao> itens;
    private SharedPreferences prefs;
    private Pedido pedidoAlteracao;
    private Parametro parametro;
    private TipoPedido tipoPedido;
    private Condicao condicao;
    private Cliente cliente;
    private Tabela tabela;
    private Activity mActivity;

    public EstruturaGravarPedido(Activity mActivity, SharedPreferences prefs) {
        this.mActivity = mActivity;
        this.prefs = prefs;
    }

    public void salvarPedido() {
        pedido = (Pedido) Utils.recuperarObjetoSerializado(mActivity.getBaseContext(), PEDIDO_SERIALIZAR);
        if (itemPedido == null)
            itemPedido = (ArrayList<ItemDigitacao>) Utils.recuperarObjetoSerializado(mActivity.getBaseContext(), ITENSPEDIDO_SERIALIZAR);

        if (validarPedidoNull()) return;
        if (validarItens()){
            ToastUtils.mostrarMensagem(mActivity,mActivity.getString(R.string.pedido_sem_itens));
            return;
        }
        buscarDados();
        reapurarPreco();
        if (ValidarOperacoes()) return;
        gravarPedido();
        if (isSalvo) {
            Utils.apagarObjetoSerializado(mActivity);
        }

    }

    private boolean validarItens() {
        if (itemPedido == null)
            return true;
        if (itemPedido.isEmpty())
            return true;
        return false;
    }

    private void reapurarPreco() {
        if (!mIsInicializandoEstruturas || acaoCombo) {
            Pedido pedidoOperacoes = pedido.reapurarPrecosPedido(mActivity.getBaseContext(), tipoPedido, tabela, condicao, pedidoAlteracao);

            pedido.setIDCondicao(pedidoOperacoes.getIDCondicao());
            pedido.setIDFormaPagamento(pedidoOperacoes.getIDFormaPagamento());
            pedido.setIDTabela(pedidoOperacoes.getIDTabela());
        }
    }

    private void buscarDados() {
        try {
            cliente = new ClienteDAO(mActivity.getBaseContext(), null).get(pedido.getIDCliente());
            tipoPedido = new TipoPedidoDAO(mActivity.getBaseContext(), null).get(pedido.getIDTPedido());
            condicao = new CondicaoDAO(mActivity.getBaseContext(), null).get(pedido.getIDCondicao());
            tabela = new TabelaDAO(mActivity.getBaseContext(), null).get(pedido.getIDTabela());
            parametro = new ParametroDAO(mActivity.getBaseContext(), null).get();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private boolean ValidarOperacoes() {
        if (!isTipoPedidoValido(prefs, pedido, mActivity.getBaseContext())) {
            Toast.makeText(mActivity.getBaseContext(), R.string.selecione_tipo_pedido, Toast.LENGTH_SHORT).show();
            return true;
        }

        if (!isCondicaoPagamentoValido(prefs, pedido, mActivity.getBaseContext())) {
            Toast.makeText(mActivity.getBaseContext(), R.string.selecione_condicao_pagamento, Toast.LENGTH_SHORT).show();
            return true;
        }

        if (!isFormaPagamentoValido(prefs, pedido, mActivity.getBaseContext())) {
            Toast.makeText(mActivity.getBaseContext(), R.string.selecione_forma_pagamento, Toast.LENGTH_SHORT).show();
            return true;
        }
        if (!isSemRestricaoTCT(pedido, mActivity.getBaseContext())) {
            return true;
        }

        return false;

    }

    private boolean validarPedidoNull() {
        if (pedido == null)
            return true;

        return false;
    }

    private void gravarPedido() {
        if (!pedido.getCombos().isEmpty()) {
            for (Combo combo : pedido.getCombos()) {
                if (!pedido.mesmaCondicaoPagamento(mActivity.getBaseContext(), combo)) {
                    ToastUtils.mostrarMensagem(mActivity.getBaseContext(), mActivity.getString(R.string.salvar_combo_com_mesma_condicao_do_pedido));
                    return;
                }
            }
        }

        if (tipoPedido.getValorMinimoPedido() > pedido.getTotalLiquido()) {
            Toast.makeText(mActivity.getBaseContext(),
                    mActivity.getString(R.string.valor_minimo_permitido_para_tipo_pedido_selecionado) + String.format("%.2f", tipoPedido.getValorMinimoPedido() < 0 ? 0 : tipoPedido.getValorMinimoPedido()),
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (parametro.getQuantidadeMinimaItensRentabilidade() > pedido.getNrItens() && tabela.isUltilizaRentabilidade()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
            dialog.setTitle(mActivity.getString(R.string.atencao));
            dialog.setIcon(android.R.drawable.ic_dialog_alert);
            dialog.setMessage(mActivity.getString(R.string.quantidadeMinimaItens) + " " + parametro.getQuantidadeMinimaItensRentabilidade() + " " + mActivity.getString(R.string.itensDistintos));
            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
            return;
        }

        DialogCustom.showCustomDialogMensagem(R.layout.dialog_salvar, mActivity.getString(R.string.deseja_gravar_o_pedido), mActivity,false,
                new DialogCustom.IResultadoDialog() {
                    @Override
                    public void execultar(Dialog dialog) {
                        if (itemPedido.size() == 0) {
                            Toast.makeText(mActivity.getBaseContext(), R.string.nao_existem_itens_digitado_nesse_pedido, Toast.LENGTH_LONG).show();
                            return;
                        }

                        FormaPagamento forma = new FormaPagamentoDAO(mActivity.getBaseContext(), null).get(pedido.getIDFormaPagamento());
                        if (forma.getLimiteMinimo() > 0) {
                            if ((pedido.getTotalGeral() / condicao.getQtdeParcelas()) < forma.getLimiteMinimo()) {
                                Toast.makeText(mActivity.getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo_da_forma_de_pagamento, Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        if (((pedido.getTotalGeral() < parametro.getLimiteMinimoPedido()) && parametro.getLimiteMinimoPedido() > 0.0 || pedido.getTotalGeral() <= 0.0) || (pedido.getTotalGeral() < condicao.getLimiteMinimoPedido()) && pedido.getTotalGeral() <= 0.0) {
                            Toast.makeText(mActivity.getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo, Toast.LENGTH_LONG).show();
                            return;
                        }

                        ArrayList<Item> itensPedido = estrururaItemDigitacao();

                        if (new PedidoDAO(mActivity.getBaseContext(), null).salvarPedido(pedido, itensPedido, true)) {

                            aposSalvarPedido(itensPedido);
                        } else {
                            Toast.makeText(mActivity.getBaseContext(), R.string.erro_durante_gravacao_do_pedido, Toast.LENGTH_LONG).show();
                        }
                        dialog.cancel();
                    }

                    @Override
                    public void cancelar(Dialog dialog) {
                        dialog.cancel();
                    }

                });

//        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
//        builder.setTitle(mActivity.getString(R.string.atencao));
//        builder.setIcon(android.R.drawable.ic_dialog_alert);
//        builder.setMessage(R.string.deseja_gravar_o_pedido)
//                .setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (itemPedido.size() == 0) {
//                    Toast.makeText(mActivity.getBaseContext(), R.string.nao_existem_itens_digitado_nesse_pedido, Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                FormaPagamento forma = new FormaPagamentoDAO(mActivity.getBaseContext(), null).get(pedido.getIDFormaPagamento());
//                if (forma.getLimiteMinimo() > 0) {
//                    if ((pedido.getTotalGeral() / condicao.getQtdeParcelas()) < forma.getLimiteMinimo()) {
//                        Toast.makeText(mActivity.getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo_da_forma_de_pagamento, Toast.LENGTH_LONG).show();
//                        return;
//                    }
//                }
//
//                if (((pedido.getTotalGeral() < parametro.getLimiteMinimoPedido()) && parametro.getLimiteMinimoPedido() > 0.0 || pedido.getTotalGeral() <= 0.0) || (pedido.getTotalGeral() < condicao.getLimiteMinimoPedido()) && pedido.getTotalGeral() <= 0.0) {
//                    Toast.makeText(mActivity.getBaseContext(), R.string.valor_total_do_pedido_abaixo_do_minimo, Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                ArrayList<Item> itensPedido = estrururaItemDigitacao();
//
//                if (new PedidoDAO(mActivity.getBaseContext(), null).salvarPedido(pedido, itensPedido, true)) {
//
//                    aposSalvarPedido(itensPedido);
//                } else {
//                    Toast.makeText(mActivity.getBaseContext(), R.string.erro_durante_gravacao_do_pedido, Toast.LENGTH_LONG).show();
//                }
//            }
//        }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        builder.show();
    }

    private void aposSalvarPedido(ArrayList<Item> itensPedido) {
        ComboXPedido.gravarCombos(mActivity.getBaseContext(), pedido);
        // Mapeamento
        gravarGeoPosicionamento();
        ShearedPreferenceUltils.resetPreferencesSpinner(mActivity.getBaseContext());
        Toast.makeText(mActivity.getBaseContext(), R.string.pedido_gravado_com_sucesso, Toast.LENGTH_LONG).show();
        Utils.apagarObjetoSerializado(mActivity);
        Utils.apagarObjetoSerializado(mActivity, ITENSPEDIDO_SERIALIZAR_ALTERAR);
        Utils.apagarObjetoSerializado(mActivity, ITENSPEDIDO_SERIALIZAR);
        isSalvo = true;
        if (!pedido.isAlterarPedido())
            mActivity.setResult(RESULT_OK, mActivity.getIntent().putExtra(Pedido.EXTRA_ID, pedido.getIDPedido()));
        else
            mActivity.setResult(PedidoListaActivity.REQUEST_PEDIDO_ALTERADO, mActivity.getIntent().putExtra(Pedido.EXTRA_ID, pedido.getIDPedido()));

        enviarResumo(pedido, itensPedido);
    }

    private ArrayList<Item> estrururaItemDigitacao() {
        ArrayList<Item> itensPedido = new ArrayList<Item>(itemPedido.size());
        short numeroItem = 1;
        for (ItemDigitacao itemDigitacao : itemPedido) {
            Item i = new Item();
            i.setIDPedido(pedido.getIDPedido());
            i.setIDProduto(itemDigitacao.getProduto().getIdProduto());
            i.setDescricao(itemDigitacao.getProduto().getDescricao());
            i.setNrItem(numeroItem++);
            i.setQuantidade(itemDigitacao.getQuantidade());
            i.setDesconto(itemDigitacao.getDesconto());
            i.setValorDesconto(itemDigitacao.getValorDesconto());
            i.setValorUtilizouFlex(itemDigitacao.getValorUtilizaFlex());
            i.setValorGeraraFlex(itemDigitacao.getValorGeraFlex());
            i.setValorUnitOriginal(itemDigitacao.getValorUnitOriginal());
            i.setValorUnitPraticado(itemDigitacao.getValorUnit());
            i.setTotalGeral(itemDigitacao.getValorBruto());
            i.setTotalLiquido(itemDigitacao.getValorLiquido());
            i.setDataPedido(pedido.getDataPedido());
            i.setRentabilidade(itemDigitacao.getRentabilidade());
            i.setCodigoNCM(itemDigitacao.getProduto().getCodigoNCM());
            i.setCodigoBarra(itemDigitacao.getProduto().getCodigoBarra());
            i.setPercentualDescontoCombo(itemDigitacao.getPercentualDescontoCombo());
            i.setQuantidadeItemLimiteCombo(itemDigitacao.getQuantidadeItemLimiteCombo());
            i.setItemComboAtivado(itemDigitacao.getItemComboAtivado());

            itensPedido.add(i);
        }
        return itensPedido;
    }

    private void enviarResumo(final Pedido ped, final ArrayList<Item> itemPedido) {

        DialogInterface.OnClickListener eventoBtnSim = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                String nomeArquivo = gerarResumoDoPedido(ped, itemPedido);
                abrirActivityEmailEnviarPdf(nomeArquivo);
            }
        };
        DialogInterface.OnClickListener eventoBtnNeutro = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                String nomeArquivo = gerarResumoDoPedido(ped, itemPedido);
                abrirActivityCompartilhar(nomeArquivo);
            }
        };
        DialogInterface.OnClickListener eventoBtnNao = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                mActivity.finish();
            }
        };

        showDialogConfirmacaoCompartilhar(mActivity, eventoBtnSim, eventoBtnNeutro, eventoBtnNao, mActivity.getString(R.string.pergunta_enviar_resumo_pedido));
    }

    private String gerarResumoDoPedido(Pedido pedido, List<Item> itens) {
        ArrayList<String> linhas = new PedidoDAO(mActivity.getBaseContext(), null).getLinhasPdfPedido(pedido, itens);
        return new PedidoDAO(mActivity.getBaseContext(), null).gerarPdf(linhas, pedido.getIDPedido());
    }

    private static Dialog showDialogConfirmacaoCompartilhar(Activity activity, DialogInterface.OnClickListener eventoBtnSim, DialogInterface.OnClickListener eventoBtnNeutro, DialogInterface.OnClickListener eventoBtnNao, String mensagem) {
        return Utils.showDialogConfirmacoes(activity, eventoBtnSim, eventoBtnNeutro, eventoBtnNao, mensagem, "E-mail", "+ Compartilhar", activity.getString(br.net.sav.R.string.nao), activity.getString(br.net.sav.R.string.a_t_e_n_c_a_o));
    }

    private void abrirActivityEmailEnviarPdf(String nomeArquivo) {
        Utils.openGmail(mActivity, new String[]{cliente.getEmail()}, mActivity.getString(R.string.assunto_email_pedido_pdf), nomeArquivo);
    }

    private void abrirActivityCompartilhar(String nomeArquivo) {
        Utils.abrirActivityEmail(mActivity, new String[]{cliente.getEmail()}, nomeArquivo);
    }

    protected void gravarGeoPosicionamento() {
        try {
            Mapeamento m = (Mapeamento) Utils.recuperarObjetoSerializado(mActivity.getBaseContext(), LocationService.MAPEAMENTO_SERIALIZADO);
            if (m == null) {
                m = new Mapeamento();
                m.setIdVendedor(parametro.getIdVendedor());
                m.setEnviado(false);
                m.setDataEnvio(new Date());
                m.setDataHora(new Date());
            }
            m.setIdPedido(pedido.getIDPedido());
            m.setValorPedido(pedido.getTotalLiquido());
            m.setIdCliente(cliente.getIdCliente());
            m.setNomeCliente(cliente.getRazao());
            // Inserir
            new MapeamentoDAO(mActivity.getBaseContext()).insert(m, null);

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }
}
