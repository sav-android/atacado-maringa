package br.net.sav.modelo;

public class FormaPagamento {
	public long IdFormaPagamento;
	public String Descricao;
	public double LimiteMinimo;
	public long AmarraCondicao;

	public long getIdFormaPagamento() {
		return IdFormaPagamento;
	}

	public void setIdFormaPagamento(long idFormaPagamento) {
		IdFormaPagamento = idFormaPagamento;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	@Override
	public String toString() {
		return String.format("%d - %s", IdFormaPagamento, Descricao);
	}

	public double getLimiteMinimo() {
		return LimiteMinimo;
	}

	public void setLimiteMinimo(double limiteMinimo) {
		LimiteMinimo = limiteMinimo;
	}

	public long getAmarraCondicao() {
		return AmarraCondicao;
	}

	public void setAmarraCondicao(long amarraCondicao) {
		AmarraCondicao = amarraCondicao;
	}
}
