package br.net.sav.modelo;

public class FormaPagamentoCliente {
	private long idCliente;
	private long idFormaPagamento;

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public long getIdFormaPagamento() {
		return idFormaPagamento;
	}

	public void setIdFormaPagamento(long idFormaPagamento) {
		this.idFormaPagamento = idFormaPagamento;
	}

}
