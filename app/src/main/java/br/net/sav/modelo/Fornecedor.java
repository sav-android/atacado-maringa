package br.net.sav.modelo;

public class Fornecedor {
	public long IdFornecedor;
	public String Descricao;

	public long getIdFornecedor() {
		return IdFornecedor;
	}

	public void setIdFornecedor(long idFornecedor) {
		IdFornecedor = idFornecedor;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}		
}
