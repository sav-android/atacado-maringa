package br.net.sav.modelo;

public class GorduraF {
	public short IdFornecedor;
	public double DescontoGordura;
	public double MaximoDesconto;

	public short getIdFornecedor() {
		return IdFornecedor;
	}

	public void setIdFornecedor(short idFornecedor) {
		IdFornecedor = idFornecedor;
	}

	public double getDescontoGordura() {
		return DescontoGordura;
	}

	public void setDescontoGordura(double descontoGordura) {
		DescontoGordura = descontoGordura;
	}

	public double getMaximoDesconto() {
		return MaximoDesconto;
	}

	public void setMaximoDesconto(double maximoDesconto) {
		MaximoDesconto = maximoDesconto;
	}

}
