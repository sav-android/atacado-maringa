package br.net.sav.modelo;

public class GorduraS {
	public short IdFornecedor;
	public short Secao;
	public short Grupo;
	public short SubGrupo;
	public double DescontoGordura;
	public double MaximoDesconto;

	public short getIdFornecedor() {
		return IdFornecedor;
	}

	public void setIdFornecedor(short iDFornecedor) {
		IdFornecedor = iDFornecedor;
	}

	public short getSecao() {
		return Secao;
	}

	public void setSecao(short secao) {
		Secao = secao;
	}

	public short getGrupo() {
		return Grupo;
	}

	public void setGrupo(short grupo) {
		Grupo = grupo;
	}

	public short getSubGrupo() {
		return SubGrupo;
	}

	public void setSubGrupo(short subGrupo) {
		SubGrupo = subGrupo;
	}

	public double getDescontoGordura() {
		return DescontoGordura;
	}

	public void setDescontoGordura(double descontoGordura) {
		DescontoGordura = descontoGordura;
	}

	public double getMaximoDesconto() {
		return MaximoDesconto;
	}

	public void setMaximoDesconto(double maximoDesconto) {
		MaximoDesconto = maximoDesconto;
	}

}
