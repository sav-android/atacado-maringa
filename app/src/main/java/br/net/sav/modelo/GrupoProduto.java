package br.net.sav.modelo;

public class GrupoProduto {

	private int idGrupo;
	private String descricao;
	private String corRgbHex;

	public int getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(int idGrupo) {
		this.idGrupo = idGrupo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCorRgbHex() {
		return corRgbHex;
	}

	public void setCorRgbHex(String corRgbHex) {
		this.corRgbHex = corRgbHex;
	}
}
