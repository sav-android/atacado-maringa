package br.net.sav.modelo;

import java.util.Date;

public class Inadimplencia {
	public short IDVendedor;
	public long IDCliente;
	public String Nome;
	public double AVencer;
	public double Vencidos;
	public Date VencimentoMaisAntigo;

	public short getIDVendedor() {
		return IDVendedor;
	}

	public void setIDVendedor(short iDVendedor) {
		IDVendedor = iDVendedor;
	}

	public long getIDCliente() {
		return IDCliente;
	}

	public void setIDCliente(long iDCliente) {
		IDCliente = iDCliente;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public double getAVencer() {
		return AVencer;
	}

	public void setAVencer(double aVencer) {
		AVencer = aVencer;
	}

	public double getVencidos() {
		return Vencidos;
	}

	public void setVencidos(double vencidos) {
		Vencidos = vencidos;
	}

	public Date getVencimentoMaisAntigo() {
		return VencimentoMaisAntigo;
	}

	public void setVencimentoMaisAntigo(Date vencimentoMaisAntigo) {
		VencimentoMaisAntigo = vencimentoMaisAntigo;
	}

}
