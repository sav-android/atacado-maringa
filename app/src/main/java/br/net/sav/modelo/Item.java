package br.net.sav.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.net.sav.util.StringUtils;

import static java.lang.String.format;

public class Item implements Serializable {
	private static final long serialVersionUID = 1L;
	public long IDPedido;
	public long IDProduto;
	public short NrItem;
	public double Quantidade;
	public double Desconto;
	public double ValorDesconto;
	public double Flex;
	public double ValorUtilizouFlex;
	public double ValorUnitOriginal;
	public double ValorUnitPraticado;
	public double TotalGeral;
	public double TotalLiquido;
	public double ValorGeraraFlex;
	public Date DataPedido;

	// Auxiliar
	public String Descricao;
	public String CodigoNCM;
	public String CodigoBarra;
	public String DescricaoEmbalagem;
	public double ValorGerandoFlex;
	public String DescricaoMarca;
	public double Peso;
	public double Rentabilidade;
	public double PercentualDescontoCombo;
	public double QuantidadeItemLimiteCombo;
	public int ItemComboAtivado;
	public String status;
	public int qntdCortada;
	public double descontoFaturado;
	public double totalFaturadoItem;


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getQntdCortada() {
		return qntdCortada;
	}

	public void setQntdCortada(int qntdCortada) {
		this.qntdCortada = qntdCortada;
	}

	public double getDescontoFaturado() {
		return descontoFaturado;
	}

	public void setDescontoFaturado(double descontoFaturado) {
		this.descontoFaturado = descontoFaturado;
	}

	public double getTotalFaturadoItem() {
		return totalFaturadoItem;
	}

	public void setTotalFaturadoItem(double totalFaturadoItem) {
		this.totalFaturadoItem = totalFaturadoItem;
	}

	public int getItemComboAtivado() { return ItemComboAtivado; }

	public void setItemComboAtivado(int itemComboAtivado) { ItemComboAtivado = itemComboAtivado; }

	public double getPercentualDescontoCombo() {
		return PercentualDescontoCombo;
	}

	public void setPercentualDescontoCombo(double percentualDescontoCombo) {
		PercentualDescontoCombo = percentualDescontoCombo;
	}

	public double getQuantidadeItemLimiteCombo() {
		return QuantidadeItemLimiteCombo;
	}

	public void setQuantidadeItemLimiteCombo(double quantidadeItemLimiteCombo) {
		QuantidadeItemLimiteCombo = quantidadeItemLimiteCombo;
	}

	public String getCodigoNCM() { return CodigoNCM; }

	public void setCodigoNCM(String codigoNCM) { CodigoNCM = codigoNCM; }

	public String getCodigoBarra() { return CodigoBarra; }

	public void setCodigoBarra(String codigoBarra) { CodigoBarra = codigoBarra; }

	public double getRentabilidade() {
		return Rentabilidade;
	}

	public void setRentabilidade(double rentabilidade) {
		Rentabilidade = rentabilidade;
	}

	public double getPeso() {
		return Peso;
	}

	public void setPeso(double peso) {
		Peso = peso;
	}

	public long getIDPedido() {
		return IDPedido;
	}

	public void setIDPedido(long iDPedido) {
		IDPedido = iDPedido;
	}

	public long getIDProduto() {
		return IDProduto;
	}

	public void setIDProduto(long iDProduto) {
		IDProduto = iDProduto;
	}

	public short getNrItem() {
		return NrItem;
	}

	public void setNrItem(short nrItem) {
		NrItem = nrItem;
	}

	public double getQuantidade() {
		return Quantidade;
	}

	public void setQuantidade(double quantidade) {
		Quantidade = quantidade;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

	public double getValorDesconto() {
		return ValorDesconto;
	}

	public void setValorDesconto(double valorDesconto) {
		ValorDesconto = valorDesconto;
	}

	public double getFlex() {
		return Flex;
	}

	public void setFlex(double flex) {
		Flex = flex;
	}

	public double getValorUtilizouFlex() {
		return ValorUtilizouFlex;
	}

	public void setValorUtilizouFlex(double valorUtilizouFlex) {
		ValorUtilizouFlex = valorUtilizouFlex;
	}

	public double getValorUnitOriginal() {
		return ValorUnitOriginal;
	}

	public void setValorUnitOriginal(double valorUnitOriginal) {
		ValorUnitOriginal = valorUnitOriginal;
	}

	public double getValorUnitPraticado() {
		return ValorUnitPraticado;
	}

	public void setValorUnitPraticado(double valorUnitPraticado) {
		ValorUnitPraticado = valorUnitPraticado;
	}

	public double getTotalGeral() {
		return TotalGeral;
	}

	public void setTotalGeral(double totalGeral) {
		TotalGeral = totalGeral;
	}

	public double getTotalLiquido() {
		return TotalLiquido;
	}

	public void setTotalLiquido(double totalLiquido) {
		TotalLiquido = totalLiquido;
	}

	public double getValorGeraraFlex() {
		return ValorGeraraFlex;
	}

	public void setValorGeraraFlex(double valorGeraraFlex) {
		ValorGeraraFlex = valorGeraraFlex;
	}

	public Date getDataPedido() {
		return DataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		DataPedido = dataPedido;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public String getDescricaoEmbalagem() {
		return DescricaoEmbalagem;
	}

	public void setDescricaoEmbalagem(String descricaoEmbalagem) {
		DescricaoEmbalagem = descricaoEmbalagem;
	}

	public double getValorGerandoFlex() {
		return ValorGerandoFlex;
	}

	public void setValorGerandoFlex(double valorGerandoFlex) {
		ValorGerandoFlex = valorGerandoFlex;
	}

	public String getDescricaoMarca() {
		return DescricaoMarca;
	}

	public void setDescricaoMarca(String descricaoMarca) {
		DescricaoMarca = descricaoMarca;
	}

	public String toLinhaTexto(Pedido pedido, long idVendedor) {

		String valorAux;
		BigDecimal twoDecimalPlaces = new BigDecimal(100);
		BigDecimal fourDecimalPlaces = new BigDecimal(10000);
		BigDecimal big = null;

		StringBuilder builder = new StringBuilder();
		builder.append(format("%04d", pedido.getIDEmpresa()));
		builder.append(format("%04d", pedido.getIDFilial()));
		builder.append(format("%06d%08d", getIDPedido(), idVendedor));
		builder.append(format("%011d", getIDProduto()));
		builder.append(format("%03d", getNrItem()));

		big = new BigDecimal(getQuantidade()).setScale(2, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(twoDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getDesconto() < 0 ? 0 : getDesconto()).setScale(2, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(twoDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%05d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getValorDesconto() < 0 ? 0 : getValorDesconto()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getFlex()).setScale(2, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(twoDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%05d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getValorUtilizouFlex()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getValorUnitOriginal()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getValorUnitPraticado()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getTotalGeral()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getTotalLiquido()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		big = new BigDecimal(getValorGeraraFlex()).setScale(4, BigDecimal.ROUND_HALF_UP);
		valorAux = format("%s", big.multiply(fourDecimalPlaces));
		valorAux = valorAux.substring(0, valorAux.indexOf("."));
		builder.append(format("%012d", Integer.parseInt(valorAux)));

		builder.append(StringUtils.QUEBRA_LINHA);
		return builder.toString();
	}
}
