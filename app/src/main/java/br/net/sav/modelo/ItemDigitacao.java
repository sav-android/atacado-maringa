package br.net.sav.modelo;

import android.graphics.Color;
import android.util.Log;

import java.io.Serializable;
import java.util.Comparator;

import br.net.sav.enumerador.OrdemListaProdutos;

public class ItemDigitacao implements Serializable, Comparable<ItemDigitacao>, Cloneable {
	private static final long serialVersionUID = 1L;
	
	private Produto Produto;
	private double Quantidade;
	private double QuantidadeGravada;
	private double ValorGeraFlex;
	private double ValorFlex;
	private double Desconto;
	private double ValorUtilizaFlex;
	private double ValorUnit;
	private double ValorUnitOriginal;
	private double ValorBruto;
	private double ValorLiquido;
	private double ValorDesconto;
	private boolean PossuiFoto;
	private String corProduto;
	private double Rentabilidade;
	private double PrecoCustoProduto;
	public double PercentualRentabilidadeMinima;
	public double PercentualRentabilidadeMedia;
	public double PercentualDescontoCombo;
	public double QuantidadeItemLimiteCombo;
	public int ItemComboAtivado;

	// Percentual de Desconto que pode gerar e consumir flex
	private double PercGera;
	private double PercConceder;
	private String DescricaoMarca;

	//AUX
	private OrdemListaProdutos ordemListaProdutos;

	public int getItemComboAtivado() { return ItemComboAtivado; }

	public boolean itemComboAtivadoMaiorQueUm(){ return ItemComboAtivado > 1; }

	public boolean itemComboAtivado(){ return ItemComboAtivado == 1; }

	public void setItemComboAtivado(int itemComboAtivado) { ItemComboAtivado = itemComboAtivado; }

	public double getPercentualDescontoCombo() {
		return PercentualDescontoCombo;
	}

	public void setPercentualDescontoCombo(double percentualDescontoCombo) {
		PercentualDescontoCombo = percentualDescontoCombo;
	}

	public double getQuantidadeItemLimiteCombo() {
		return QuantidadeItemLimiteCombo;
	}

	public void setQuantidadeItemLimiteCombo(double quantidadeItemLimiteCombo) {
		QuantidadeItemLimiteCombo = quantidadeItemLimiteCombo;
	}

	public double getPrecoCustoProduto() {
		return PrecoCustoProduto;
	}

	public void setPrecoCustoProduto(double precoCustoProduto) {
		PrecoCustoProduto = precoCustoProduto;
	}

	public double getRentabilidade() {
		return Rentabilidade;
	}

	public void setRentabilidade(double rentabilidade) {
		Rentabilidade = rentabilidade;
	}

	public double getPercentualRentabilidadeMinima() {
		return PercentualRentabilidadeMinima;
	}

	public void setPercentualRentabilidadeMinima(double percentualRentabilidadeMinima) {
		PercentualRentabilidadeMinima = percentualRentabilidadeMinima;
	}

	public double getPercentualRentabilidadeMedia() {
		return PercentualRentabilidadeMedia;
	}

	public void setPercentualRentabilidadeMedia(double percentualRentabilidadeMedia) {
		PercentualRentabilidadeMedia = percentualRentabilidadeMedia;
	}

	public ItemDigitacao() {
		this.ordemListaProdutos = OrdemListaProdutos.PRODUTOS_NOVOS;
	}
	
	public boolean isPossuiFoto() {
		return PossuiFoto;
	}

	public void setPossuiFoto(boolean possuiFoto) {
		PossuiFoto = possuiFoto;
	}

	public Produto getProduto() {
		return Produto;
	}

	public void setProduto(Produto produto) {
		Produto = produto;
	}

	public double getQuantidade() {
		return Quantidade;
	}

	public void setQuantidade(double quantidade) {
		Quantidade = quantidade;
	}

	public double getQuantidadeGravada() {
		return QuantidadeGravada;
	}

	public void setQuantidadeGravada(double quantidadeGravada) {
		QuantidadeGravada = quantidadeGravada;
	}

	public double getValorGeraFlex() {
		return ValorGeraFlex;
	}

	public void setValorGeraFlex(double valorGeraFlex) {
		ValorGeraFlex = valorGeraFlex;
	}

	public double getValorFlex() {
		return ValorFlex;
	}

	public void setValorFlex(double valorFlex) {
		ValorFlex = valorFlex;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

	public double getValorUtilizaFlex() {
		return ValorUtilizaFlex;
	}

	public void setValorUtilizaFlex(double valorUtilizaFlex) {
		ValorUtilizaFlex = valorUtilizaFlex;
	}

	public double getValorUnit() {
		return ValorUnit;
	}

	public void setValorUnit(double valorUnit) {
		ValorUnit = valorUnit;
	}

	public double getValorUnitOriginal() {
		return ValorUnitOriginal;
	}

	public void setValorUnitOriginal(double valorUnitOriginal) {
		ValorUnitOriginal = valorUnitOriginal;
	}

	public double getValorBruto() {
		return ValorBruto;
	}

	public void setValorBruto(double valorBruto) {
		ValorBruto = valorBruto;
	}

	public double getValorLiquido() {
		return ValorLiquido;
	}

	public void setValorLiquido(double valorLiquido) {
		ValorLiquido = valorLiquido;
	}

	public double getValorDesconto() {
		return ValorDesconto;
	}

	public void setValorDesconto(double valorDesconto) {
		ValorDesconto = valorDesconto;
	}

	public double getPercGera() {
		return PercGera;
	}

	public void setPercGera(double percGera) {
		PercGera = percGera;
	}

	public double getPercConceder() {
		return PercConceder;
	}

	public void setPercConceder(double percConceder) {
		PercConceder = percConceder;
	}

	public String getDescricaoMarca() {
		return DescricaoMarca;
	}

	public void setDescricaoMarca(String descricaoMarca) {
		DescricaoMarca = descricaoMarca;
	}

	public void setCorProduto(String corProduto) {
		this.corProduto = corProduto;
	}

	public String getCorProduto() {
		return corProduto == null ? "#00FF00" : "#" + corProduto;
	}

	public long verificaQuantidadeEmbalagem(){
		return String.valueOf(this.getProduto().getQuantidadeEmbalagem()) == null? 1:
				this.getProduto().getQuantidadeEmbalagem()== 0? 1: this.getProduto().getQuantidadeEmbalagem();
	}
	
	public int getBackgroundProduto() {
		int retorno = Color.TRANSPARENT;
		try {
			if("N".equalsIgnoreCase(getProduto().getStatus())) {
				retorno = Color.parseColor("#0ba4c5");
			} else if("R".equalsIgnoreCase(getProduto().getStatus())) {
				retorno = Color.parseColor("#130bf6");
			} else if("P".equalsIgnoreCase(getProduto().getStatus())) {
				retorno = Color.parseColor("#ec66d9");
			}
			if(getProduto().getDescricaoEmbalagem().equalsIgnoreCase("UNIDADE") ||
					getProduto().getDescricaoEmbalagem().equalsIgnoreCase("UN")  ||
					getProduto().getDescricaoEmbalagem().equalsIgnoreCase("UND")){
				retorno = Color.parseColor("#b5af12");
			}
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage());
		}
		return retorno;
	}

	public OrdemListaProdutos getOrdemListaProdutos() {
		return ordemListaProdutos;
	}

	public void setOrdemListaProdutos(OrdemListaProdutos ordemListaProdutos) {
		this.ordemListaProdutos = ordemListaProdutos;
	}

	public int compareTo(ItemDigitacao itemDigitacao) {
		if (itemDigitacao.getProduto().getStatus().equalsIgnoreCase("N")) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public static Comparator<ItemDigitacao> ItemDigitacaoComparatorOrdemAlfabetica = new Comparator<ItemDigitacao>() {

		public int compare(ItemDigitacao itemDigitacao, ItemDigitacao itemDigitacao2) {
			return itemDigitacao.getProduto().getDescricao().compareTo(itemDigitacao2.getProduto().getDescricao());
		}
	};

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
