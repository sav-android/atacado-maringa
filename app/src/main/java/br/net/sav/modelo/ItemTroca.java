package br.net.sav.modelo;

public class ItemTroca {
	private long IdTroca;
	private long IdProduto;
	private short NumeroItem;
	private boolean Unitario;
	private double Quantidade;
	private double ValorUnitario;
	private double ValorMercadoriaBruto;
	private double Desconto;
	private double ValorDesconto;
	private double ValorMercadoriaLiquido;

	// Auxiliares
	private String DescricaoProduto;
	private String DescricaoEmbalagem;

	public double getQuantidade() {
		return Quantidade;
	}

	public void setQuantidade(double quantidade) {
		Quantidade = quantidade;
	}

	public long getIdTroca() {
		return IdTroca;
	}

	public void setIdTroca(long idTroca) {
		IdTroca = idTroca;
	}

	public long getIdProduto() {
		return IdProduto;
	}

	public void setIdProduto(long idProduto) {
		IdProduto = idProduto;
	}

	public short getNumeroItem() {
		return NumeroItem;
	}

	public void setNumeroItem(short numeroItem) {
		NumeroItem = numeroItem;
	}

	public boolean isUnitario() {
		return Unitario;
	}

	public void setUnitario(boolean unitario) {
		Unitario = unitario;
	}

	public double getValorUnitario() {
		return ValorUnitario;
	}

	public void setValorUnitario(double valorUnitario) {
		ValorUnitario = valorUnitario;
	}

	public double getValorMercadoriaBruto() {
		return ValorMercadoriaBruto;
	}

	public void setValorMercadoriaBruto(double valorMercadoriaBruto) {
		ValorMercadoriaBruto = valorMercadoriaBruto;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

	public double getValorDesconto() {
		return ValorDesconto;
	}

	public void setValorDesconto(double valorDesconto) {
		ValorDesconto = valorDesconto;
	}

	public double getValorMercadoriaLiquido() {
		return ValorMercadoriaLiquido;
	}

	public void setValorMercadoriaLiquido(double valorMercadoriaLiquido) {
		ValorMercadoriaLiquido = valorMercadoriaLiquido;
	}

	public String getDescricaoProduto() {
		return DescricaoProduto;
	}

	public void setDescricaoProduto(String descricaoProduto) {
		DescricaoProduto = descricaoProduto;
	}

	public String getDescricaoEmbalagem() {
		return DescricaoEmbalagem;
	}

	public void setDescricaoEmbalagem(String descricaoEmbalagem) {
		DescricaoEmbalagem = descricaoEmbalagem;
	}

}
