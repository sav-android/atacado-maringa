package br.net.sav.modelo;

public class Limite {
	public long IdCliente;
	public double LimiteTotal;
	public double LimiteDisponivel;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}

	public double getLimiteTotal() {
		return LimiteTotal;
	}

	public void setLimiteTotal(double limiteTotal) {
		LimiteTotal = limiteTotal;
	}

	public double getLimiteDisponivel() {
		return LimiteDisponivel;
	}

	public void setLimiteDisponivel(double limiteDisponivel) {
		LimiteDisponivel = limiteDisponivel;
	}

}
