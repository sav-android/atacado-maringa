package br.net.sav.modelo;

public class Marca {
	public long IdMarca;
	public String Descricao;

	public long getIdMarca() {
		return IdMarca;
	}

	public void setIdMarca(long idMarca) {
		IdMarca = idMarca;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

}
