package br.net.sav.modelo;

public class NaoVenda {
	public static final int OBRIGATORIO = 1;
	public static final int OPCIONAL = 2;
	public static final int NAO_SOLICITAR = 3;
	
	public short IdNaoVenda;
	public String Descricao;

	public short getIdNaoVenda() {
		return IdNaoVenda;
	}

	public void setIdNaoVenda(short idNaoVenda) {
		IdNaoVenda = idNaoVenda;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	@Override
	public String toString() {
		return Descricao;
	}
}
