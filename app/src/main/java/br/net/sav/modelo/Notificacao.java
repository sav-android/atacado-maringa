package br.net.sav.modelo;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.net.sav.dao.NotificacaoDao;
import br.net.sav.notificacao.ExibeDetalhesNotificacao;
import br.net.sav.notificacao.ExibeNotificacaoDialog;
import br.net.sav.notificacao.IAcaoBotaoNotificacao;
import br.net.sav.notificacao.TipoFrequencia;
import br.net.sav.notificacao.TipoNotificacao;

public class Notificacao implements Serializable {

    public int IdNotificacao;
    public Date DataInicial;
    public Date DataFinal;
    public int FrequanciaNotificacao;
    public String DescricaoNotificacao;
    public long IdPedido;
    public boolean Lido;

    public static void exibirNotificacoes(Context ctx) {
        final NotificacaoDao notificacaoDao = new NotificacaoDao(ctx,null);

        List<Notificacao> notificacoes = notificacaoDao.buscarNotficacoesValidas();

        for(final br.net.sav.modelo.Notificacao notificacao:notificacoes){


                if(!notificacao.getLido() || TipoFrequencia.SEMPRE.getId()==notificacao.getFrequanciaNotificacao()){
                    if(notificacao.getIdPedido()>0){
                        NotificacaoPedidoAlterado notificacaoPedidoAlterado = notificacao.toNotificacaoPedido();

                        ExibeNotificacaoDialog.show(ctx, notificacaoPedidoAlterado, new ExibeNotificacaoDialog.AOFecharDialogCallback() {
                            @Override
                            public void aoFecharDialogCallback(br.net.sav.notificacao.Notificacao notificacaoLida) {
                                notificacao.setLido(notificacaoLida.isLido());
                                notificacaoDao.atualizar(notificacao);
                            }
                        });
                    }else{

                        ExibeNotificacaoDialog.show(ctx, notificacao.toNotificacao(), new ExibeNotificacaoDialog.AOFecharDialogCallback() {
                            @Override
                            public void aoFecharDialogCallback(br.net.sav.notificacao.Notificacao notificacaoLida) {
                                notificacao.setLido(notificacaoLida.isLido());
                                notificacaoDao.atualizar(notificacao);
                            }
                        });
                    }

                }

        }
    }

    private br.net.sav.notificacao.Notificacao toNotificacao() {
        return new br.net.sav.notificacao.Notificacao(getDescricaoNotificacao(), TipoFrequencia.getFrequenciaByCod(getFrequanciaNotificacao()), TipoNotificacao.PADRAO, null, "", getLido(),
                new IAcaoBotaoNotificacao() {
                    @Override
                    public void onClick(Context context, br.net.sav.notificacao.Notificacao notificacao) {
                        final String caminhoArquivos = Environment.getExternalStorageDirectory().getPath() + "/sav/notificacao_detalhes" ;
                        File file = new File(caminhoArquivos + "/" + getIdNotificacao() + ".not");
                        ExibeDetalhesNotificacao.show(context,notificacao, file);
                    }
                });

    }


    public static void excluirNotificacoesVencidas(Context ctx) {
        new NotificacaoDao(ctx,null).removerNotificacoesVencidas();
    }

    public Date getDataInicial() {
        return DataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        DataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return DataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        DataFinal = dataFinal;
    }

    public Integer getFrequanciaNotificacao() {
        return FrequanciaNotificacao;
    }

    public void setFrequanciaNotificacao(Integer frequanciaNotificacao) {
        FrequanciaNotificacao = frequanciaNotificacao;
    }

    public String getDescricaoNotificacao() {
        return DescricaoNotificacao;
    }

    public void setDescricaoNotificacao(String descricaoNotificacao) {
        DescricaoNotificacao = descricaoNotificacao;
    }

    public Long getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(Long idPedido) {
        IdPedido = idPedido;
    }

    public Boolean getLido() {
        return Lido;
    }

    public void setLido(Boolean lido) {
        Lido = lido;
    }

    public int getIdNotificacao() {
        return IdNotificacao;
    }

    public void setIdNotificacao(int idNotificacao) {
        IdNotificacao = idNotificacao;
    }

    public NotificacaoPedidoAlterado toNotificacaoPedido() {
            return new NotificacaoPedidoAlterado(getDescricaoNotificacao(), getIdPedido(), getIdNotificacao());
    }
}
