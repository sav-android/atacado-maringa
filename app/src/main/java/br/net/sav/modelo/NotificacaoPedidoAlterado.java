package br.net.sav.modelo;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import java.io.File;

import br.net.sav.atacadomaringa.ItemListaActivity;
import br.net.sav.atacadomaringa.Principal;
import br.net.sav.notificacao.ExibeDetalhesNotificacao;
import br.net.sav.notificacao.IAcaoBotaoNotificacao;
import br.net.sav.notificacao.Notificacao;
import br.net.sav.notificacao.TipoFrequencia;
import br.net.sav.notificacao.TipoNotificacao;

public class NotificacaoPedidoAlterado extends Notificacao {


    private final long idPedido;
    private final long idNotificacao;

    public NotificacaoPedidoAlterado(String mensagem, final Long idPedido, final long idNotificacao) {
        super(mensagem, TipoFrequencia.SEMPRE, TipoNotificacao.ATUALIZACAO_PEDIDO, new IAcaoBotaoNotificacao() {
            @Override
            public void onClick(Context context, Notificacao notificacao) {
                Intent it = new Intent(ItemListaActivity.INTENT);
                it.addCategory(Principal.CATEGORIA);
                it.putExtra("IdPedido", idPedido);
                context.startActivity(it);
            }
        }, "Ver Pedido", false, new IAcaoBotaoNotificacao() {
            @Override
            public void onClick(Context context, Notificacao notificacao) {
                final String caminhoArquivos = Environment.getExternalStorageDirectory().getPath() + "/sav/notificacao_detalhes" ;
                File file = new File(caminhoArquivos + "/" + idNotificacao + ".not");
                ExibeDetalhesNotificacao.show(context,notificacao, file);
            }
        });
        this.idPedido = idPedido;
        this.idNotificacao = idNotificacao;
    }


}
