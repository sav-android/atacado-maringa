package br.net.sav.modelo;

public class ObjetivoMeta {
	public short Tipo;
	public String Descricao;
	public double Objetivo;
	public double Realizado;

	// propriedades auxiliares
	public double PercRealizado;
	public double Tendencia;
	public double PercTendencia;
	public double SugestaoDia;

	public short getTipo() {
		return Tipo;
	}

	public void setTipo(short tipo) {
		Tipo = tipo;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public double getObjetivo() {
		return Objetivo;
	}

	public void setObjetivo(double objetivo) {
		Objetivo = objetivo;
	}

	public double getRealizado() {
		return Realizado;
	}

	public void setRealizado(double realizado) {
		Realizado = realizado;
	}

	public double getPercRealizado() {
		return PercRealizado;
	}

	public void setPercRealizado(double percRealizado) {
		PercRealizado = percRealizado;
	}

	public double getTendencia() {
		return Tendencia;
	}

	public void setTendencia(double tendencia) {
		Tendencia = tendencia;
	}

	public double getPercTendencia() {
		return PercTendencia;
	}

	public void setPercTendencia(double percTendencia) {
		PercTendencia = percTendencia;
	}

	public double getSugestaoDia() {
		return SugestaoDia;
	}

	public void setSugestaoDia(double sugestaoDia) {
		SugestaoDia = sugestaoDia;
	}

}
