package br.net.sav.modelo;

public class ObservacaoCliente {
	public Long IdCliente;
	public String Observacao;
	
	public Long getIdCliente() {
		return IdCliente;
	}
	public void setIdCliente(Long idCliente) {
		IdCliente = idCliente;
	}
	public String getObservacao() {
		return Observacao;
	}
	public void setObservacao(String observacao) {
		Observacao = observacao;
	}
}
