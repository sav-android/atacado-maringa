package br.net.sav.modelo;

import android.content.Context;

import java.text.ParseException;

import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoPedidoDAO;

public class OperacoesPedidoDTO {

    private Parametro parametro;
    private TipoPedido tipoPedido;
    private Condicao condicao;
    private Cliente cliente;
    private Tabela tabela;
    private Pedido pedido;

    public OperacoesPedidoDTO(Context ctx, Pedido pedido){
        try {
        this.pedido = pedido;
        cliente = new ClienteDAO(ctx, null).get(pedido.getIDCliente());
        tipoPedido = new TipoPedidoDAO(ctx, null).get(pedido.getIDTPedido());
        condicao = new CondicaoDAO(ctx, null).get(pedido.getIDCondicao());
        tabela = new TabelaDAO(ctx, null).get(pedido.getIDTabela());
        parametro = new ParametroDAO(ctx, null).get();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Parametro getParametro() {
        return parametro;
    }

    public void setParametro(Parametro parametro) {
        this.parametro = parametro;
    }

    public TipoPedido getTipoPedido() {
        return tipoPedido;
    }

    public void setTipoPedido(TipoPedido tipoPedido) {
        this.tipoPedido = tipoPedido;
    }

    public Condicao getCondicao() {
        return condicao;
    }

    public void setCondicao(Condicao condicao) {
        this.condicao = condicao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Tabela getTabela() {
        return tabela;
    }

    public void setTabela(Tabela tabela) {
        this.tabela = tabela;
    }
}
