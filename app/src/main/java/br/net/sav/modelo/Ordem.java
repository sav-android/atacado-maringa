package br.net.sav.modelo;

public class Ordem {
	public long NrPedido;
	public long NrTroca;

	public long getNrPedido() {
		return NrPedido;
	}

	public void setNrPedido(long nrPedido) {
		NrPedido = nrPedido;
	}

	public long getNrTroca() {
		return NrTroca;
	}

	public void setNrTroca(long nrTroca) {
		NrTroca = nrTroca;
	}

}
