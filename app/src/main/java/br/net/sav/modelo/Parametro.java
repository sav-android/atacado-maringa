package br.net.sav.modelo;

import android.content.Context;

import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.Pedido;
import br.net.sav.IntegradorWeb.interfaces.IgetToken;
import br.net.sav.IntegradorWeb.retrofit.service.AoFinalizarRequisicaoListener;
import br.net.sav.dao.PedidoDAO;

public class Parametro implements IgetToken {
	public long IdVendedor;
	public String Nome;
	public String DDD;
	public String Telefone;
	public double LimiteMinimoPedido;
	public short QtdeDiasGuardarPedido;
	public short QtdeMaximaItens;
	public short QtdeDiasBloquearInadimplente;
	public short OrdemListaProdutos;
	public short DiasValidadeOrcamento;
	public boolean PermiteIncluirCliente;
	public boolean QtdeBonifAbateVerba;
	public short PosicaoCodClienteConsulta;
	public short TabelaPadraoItens;
	public short IdEmpresaTabPadrao;
	public short IdFilialTabPadrao;
	public short TabelaPrecoClienteNovo;
	public short IdEmpresaClienteNovo;
	public short IdFilialClienteNovo;
	public boolean UtilizaImpressora;
	public short DescontoExtra;
	public boolean DataFaturamento;
	public boolean ControlaLimiteCredito;
	public double PercMaximoDescontoFlex;
	public double ValorMaximoExcederVerba;
	public double SaldoVerba;
	public boolean ControlaTabelaPrecoCliente;
	public short MotivoNaoVenda;
	public boolean PermiteUnitarioSugestaoVenda;
	public boolean PercDescontoProdutoGeraVerba;
	public short GeraVerbaPalmtop;
	public short DadosAdicionais;
	public short UtilizaTroca;
	public boolean PermiteAlterarPreco;
	public boolean ExcessoPrecoGeraFlex;
	public short IncluiEmbalagem;
	public Date DataUltimaRecepcao;
	public Date DataUltimoAcesso;
	public double DescontoProdutoVendedor;
	public double PercentualRentabilidadeMinimaDoPedido;
	public double PercRentabilidadeMediaDoPedido;
	public boolean VisualizarRentabilidade;
	public boolean Supervisor;
	public double PercentualBonificacao;
	public Date DataExpiraLiberacao;
	public boolean SolicitarSenhaRentabilidade;
	public String TokenApi;
	public boolean Ativo;
	public Date DataUltimaVerificacaoCM;
	public boolean VisualizarBotaoContato;
	public long idVendedorCM;
	public short QuantidadeMinimaItensRentabilidade;
	public long IdSequencialPedido;

	public short getQuantidadeMinimaItensRentabilidade() {
		return QuantidadeMinimaItensRentabilidade;
	}

	public void setQuantidadeMinimaItensRentabilidade(short quantidadeMinimaItensRentabilidade) {
		QuantidadeMinimaItensRentabilidade = quantidadeMinimaItensRentabilidade;
	}

	public long getIdSequencialPedido() {
		return IdSequencialPedido;
	}

	public void setIdSequencialPedido(long idSequencialPedido) {
		IdSequencialPedido = idSequencialPedido;
	}

	public String getTokenApi() {
		return TokenApi;
	}

	public void setTokenApi(String tokenApi) {
		TokenApi = tokenApi;
	}

	public boolean isSolicitarSenhaRentabilidade() {
		return SolicitarSenhaRentabilidade;
	}

	public void setSolicitarSenhaRentabilidade(boolean solicitarSenhaRentabilidade) {
		SolicitarSenhaRentabilidade = solicitarSenhaRentabilidade;
	}

	public Date getDataExpiraLiberacao() {
		return DataExpiraLiberacao;
	}

	public void setDataExpiraLiberacao(Date dataExpiraLiberacao) {
		DataExpiraLiberacao = dataExpiraLiberacao;
	}

	public double getPercentualBonificacao() {
		return PercentualBonificacao;
	}

	public void setPercentualBonificacao(double percentualBonificacao) {
		PercentualBonificacao = percentualBonificacao;
	}

	public boolean isSupervisor() {
		return Supervisor;
	}

	public void setSupervisor(boolean supervisor) {
		Supervisor = supervisor;
	}

	public double getPercRentabilidadeMediaDoPedido() {
		return PercRentabilidadeMediaDoPedido;
	}

	public void setPercRentabilidadeMediaDoPedido(double percRentabilidadeMediaDoPedido) {
		PercRentabilidadeMediaDoPedido = percRentabilidadeMediaDoPedido;
	}

	public double getPercentualRentabilidadeMinimaDoPedido() {
		return PercentualRentabilidadeMinimaDoPedido;
	}

	public void setPercentualRentabilidadeMinimaDoPedido(double percentualRentabilidadeMinimaDoPedido) {
		PercentualRentabilidadeMinimaDoPedido = percentualRentabilidadeMinimaDoPedido;
	}

	public boolean getVisualizarRentabilidade() {
		return VisualizarRentabilidade;
	}

	public void setVisualizarRentabilidade(boolean visualizarRentabilidade) {
		VisualizarRentabilidade = visualizarRentabilidade;
	}

	public double getDescontoProdutoVendedor() {
		return DescontoProdutoVendedor;
	}

	public void setDescontoProdutoVendedor(double descontoProdutoVendedor) {
		DescontoProdutoVendedor = descontoProdutoVendedor;
	}

	public Date getDataUltimoAcesso() {
		return DataUltimoAcesso;
	}

	public void setDataUltimoAcesso(Date dataUltimaAcesso) {
		DataUltimoAcesso = dataUltimaAcesso;
	}

	public long getIdVendedor() {
		return IdVendedor;
	}

	public void setIdVendedor(long iDVendedor) {
		IdVendedor = iDVendedor;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getDDD() {
		return DDD;
	}

	public void setDDD(String dDD) {
		DDD = dDD;
	}

	public String getTelefone() {
		return Telefone;
	}

	public void setTelefone(String telefone) {
		Telefone = telefone;
	}

	public double getLimiteMinimoPedido() {
		return LimiteMinimoPedido;
	}

	public void setLimiteMinimoPedido(double limiteMinimoPedido) {
		LimiteMinimoPedido = limiteMinimoPedido;
	}

	public short getQtdeDiasGuardarPedido() {
		return QtdeDiasGuardarPedido;
	}

	public void setQtdeDiasGuardarPedido(short qtdeDiasGuardarPedido) {
		QtdeDiasGuardarPedido = qtdeDiasGuardarPedido;
	}

	public short getQtdeMaximaItens() {
		return QtdeMaximaItens;
	}

	public void setQtdeMaximaItens(short qtdeMaximaItens) {
		QtdeMaximaItens = qtdeMaximaItens;
	}

	public short getQtdeDiasBloquearInadimplente() {
		return QtdeDiasBloquearInadimplente;
	}

	public void setQtdeDiasBloquearInadimplente(
			short qtdeDiasBloquearInadimplente) {
		QtdeDiasBloquearInadimplente = qtdeDiasBloquearInadimplente;
	}

	public short getOrdemListaProdutos() {
		return OrdemListaProdutos;
	}

	public void setOrdemListaProdutos(short ordemListaProdutos) {
		OrdemListaProdutos = ordemListaProdutos;
	}

	public short getDiasValidadeOrcamento() {
		return DiasValidadeOrcamento;
	}

	public void setDiasValidadeOrcamento(short diasValidadeOrcamento) {
		DiasValidadeOrcamento = diasValidadeOrcamento;
	}

	public boolean isPermiteIncluirCliente() {
		return PermiteIncluirCliente;
	}

	public void setPermiteIncluirCliente(boolean permiteIncluirCliente) {
		PermiteIncluirCliente = permiteIncluirCliente;
	}

	public boolean isQtdeBonifAbateVerba() {
		return QtdeBonifAbateVerba;
	}

	public void setQtdeBonifAbateVerba(boolean qtdeBonifAbateVerba) {
		QtdeBonifAbateVerba = qtdeBonifAbateVerba;
	}

	public short getPosicaoCodClienteConsulta() {
		return PosicaoCodClienteConsulta;
	}

	public void setPosicaoCodClienteConsulta(short posicaoCodClienteConsulta) {
		PosicaoCodClienteConsulta = posicaoCodClienteConsulta;
	}

	public short getTabelaPadraoItens() {
		return TabelaPadraoItens;
	}

	public void setTabelaPadraoItens(short tabelaPadraoItens) {
		TabelaPadraoItens = tabelaPadraoItens;
	}

	public short getIDEmpresaTabPadrao() {
		return IdEmpresaTabPadrao;
	}

	public void setIDEmpresaTabPadrao(short iDEmpresaTabPadrao) {
		IdEmpresaTabPadrao = iDEmpresaTabPadrao;
	}

	public short getIDFilialTabPadrao() {
		return IdFilialTabPadrao;
	}

	public void setIDFilialTabPadrao(short iDFilialTabPadrao) {
		IdFilialTabPadrao = iDFilialTabPadrao;
	}

	public short getTabelaPrecoClienteNovo() {
		return TabelaPrecoClienteNovo;
	}

	public void setTabelaPrecoClienteNovo(short tabelaPrecoClienteNovo) {
		TabelaPrecoClienteNovo = tabelaPrecoClienteNovo;
	}

	public short getIDEmpresaClienteNovo() {
		return IdEmpresaClienteNovo;
	}

	public void setIDEmpresaClienteNovo(short iDEmpresaClienteNovo) {
		IdEmpresaClienteNovo = iDEmpresaClienteNovo;
	}

	public short getIDFilialClienteNovo() {
		return IdFilialClienteNovo;
	}

	public void setIDFilialClienteNovo(short iDFilialClienteNovo) {
		IdFilialClienteNovo = iDFilialClienteNovo;
	}

	public boolean isUtilizaImpressora() {
		return UtilizaImpressora;
	}

	public void setUtilizaImpressora(boolean utilizaImpressora) {
		UtilizaImpressora = utilizaImpressora;
	}

	public short isDescontoExtra() {
		return DescontoExtra;
	}

	public void setDescontoExtra(short descontoExtra) {
		DescontoExtra = descontoExtra;
	}

	public boolean isDataFaturamento() {
		return DataFaturamento;
	}

	public void setDataFaturamento(boolean dataFaturamento) {
		DataFaturamento = dataFaturamento;
	}

	public boolean isControlaLimiteCredito() {
		return ControlaLimiteCredito;
	}

	public void setControlaLimiteCredito(boolean controlaLimiteCredito) {
		ControlaLimiteCredito = controlaLimiteCredito;
	}

	public double getPercMaximoDescontoFlex() {
		return PercMaximoDescontoFlex;
	}

	public void setPercMaximoDescontoFlex(double percMaximoDescontoFlex) {
		PercMaximoDescontoFlex = percMaximoDescontoFlex;
	}

	public double getValorMaximoExcederVerba() {
		return ValorMaximoExcederVerba;
	}

	public void setValorMaximoExcederVerba(double valorMaximoExcederVerba) {
		ValorMaximoExcederVerba = valorMaximoExcederVerba;
	}

	public double getSaldoVerba() {
		return SaldoVerba;
	}

	public void setSaldoVerba(double saldoVerba) {
		SaldoVerba = saldoVerba;
	}

	public boolean isControlaTabelaPrecoCliente() {
		return ControlaTabelaPrecoCliente;
	}

	public void setControlaTabelaPrecoCliente(boolean controlaTabelaPrecoCliente) {
		ControlaTabelaPrecoCliente = controlaTabelaPrecoCliente;
	}

	public short getMotivoNaoVenda() {
		return MotivoNaoVenda;
	}

	public void setMotivoNaoVenda(short motivoNaoVenda) {
		MotivoNaoVenda = motivoNaoVenda;
	}

	public boolean isPermiteUnitarioSugestaoVenda() {
		return PermiteUnitarioSugestaoVenda;
	}

	public void setPermiteUnitarioSugestaoVenda(
			boolean permiteUnitarioSugestaoVenda) {
		PermiteUnitarioSugestaoVenda = permiteUnitarioSugestaoVenda;
	}

	public boolean isPercDescontoProdutoGeraVerba() {
		return PercDescontoProdutoGeraVerba;
	}

	public void setPercDescontoProdutoGeraVerba(
			boolean percDescontoProdutoGeraVerba) {
		PercDescontoProdutoGeraVerba = percDescontoProdutoGeraVerba;
	}

	public short isGeraVerbaPalmtop() {
		return GeraVerbaPalmtop;
	}

	public void setGeraVerbaPalmtop(short geraVerbaPalmtop) {
		GeraVerbaPalmtop = geraVerbaPalmtop;
	}

	public short getDadosAdicionais() {
		return DadosAdicionais;
	}

	public void setDadosAdicionais(short dadosAdicionais) {
		DadosAdicionais = dadosAdicionais;
	}

	public short getUtilizaTroca() {
		return UtilizaTroca;
	}

	public void setUtilizaTroca(short utilizaTroca) {
		UtilizaTroca = utilizaTroca;
	}

	public boolean isPermiteAlterarPreco() {
		return PermiteAlterarPreco;
	}

	public void setPermiteAlterarPreco(boolean permiteAlterarPreco) {
		PermiteAlterarPreco = permiteAlterarPreco;
	}

	public boolean isExcessoPrecoGeraFlex() {
		return ExcessoPrecoGeraFlex;
	}

	public void setExcessoPrecoGeraFlex(boolean excessoPrecoGeraFlex) {
		ExcessoPrecoGeraFlex = excessoPrecoGeraFlex;
	}

	public short getIncluiEmbalagem() {
		return IncluiEmbalagem;
	}

	public void setIncluiEmbalagem(short incluiEmbalagem) {
		IncluiEmbalagem = incluiEmbalagem;
	}

	public Date getDataUltimaRecepcao() {
		return DataUltimaRecepcao;
	}

	public void setDataUltimaRecepcao(Date dataUltimaRecepcao) {
		DataUltimaRecepcao = dataUltimaRecepcao;
	}
	
	public boolean isObrigatorioInformarNaoVenda() {
		return MotivoNaoVenda == NaoVenda.OBRIGATORIO;
	}
	
	public boolean isPermitidoInformarNaoVenda() {
		return (MotivoNaoVenda == NaoVenda.OBRIGATORIO || MotivoNaoVenda == NaoVenda.OPCIONAL);
	}

	@Override
	public String toString() {
		return String.format("%d - %s", IdVendedor, Nome);
	}

	public boolean isVisualizarBotaoContato() {
		return VisualizarBotaoContato;
	}

	public void setVisualizarBotaoContato(boolean visualizarBotaoContato) {
		VisualizarBotaoContato = visualizarBotaoContato;
	}

	public boolean isAtivo() {
		return Ativo;
	}

	public void setAtivo(boolean ativo) {
		this.Ativo = ativo;
	}

	public Date getDataUltimaVerificacaoCM() {
		return DataUltimaVerificacaoCM;
	}

	public void setDataUltimaVerificacaoCM(Date dataUltimaVerificacaoCM) {
		DataUltimaVerificacaoCM = dataUltimaVerificacaoCM;
	}

	public Long getIdVendedorCM() {
		return idVendedorCM;
	}

	public void setIdVendedorCM(Long idVendedorCM) {
		this.idVendedorCM = idVendedorCM;
	}

	@Override
	public String getToken() {
		return getTokenApi();
	}

	public static AoFinalizarRequisicaoListener<List<Pedido>> getlistener(final Context ctx) {
		return new AoFinalizarRequisicaoListener<List<br.net.sav.IntegradorWeb.dto.Pedido>>() {
			@Override
			public void quandoSucesso(List<Pedido> resultado) {
				new PedidoDAO(ctx, null).insereHistoricoDePedidos(resultado);
			}

			@Override
			public void quandoErro(String erro) {
			}
		};
	}
}
