package br.net.sav.modelo;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.PedidoTabActivity;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.ItemDAO;
import br.net.sav.dao.ItemDigitacaoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.enumerador.TipoOperacao;
import br.net.sav.service.TaskGeneric;
import br.net.sav.util.StringUtils;
import br.net.sav.utils.MathUtils;
import br.net.sav.utils.ToastUtils;

import static br.net.sav.atacadomaringa.ComboAdapter.acaoCombo;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR;
import static br.net.sav.atacadomaringa.ItemTabActivity.ITENSPEDIDO_SERIALIZAR_ALTERAR;
import static br.net.sav.atacadomaringa.ItemTabActivity.itemCompleto;
import static br.net.sav.atacadomaringa.ItemTabActivity.itemPedido;
import static br.net.sav.atacadomaringa.ItemTabActivity.itemFiltrado;
import static br.net.sav.atacadomaringa.ItemTabActivity.itemAux;
import static java.lang.String.format;

@SuppressWarnings("serial")
public class Pedido implements Serializable {
	public static final String EXTRA_ID = "IdPedido";
	
	public long IDCliente;
	public long IDPedido;
	public String PedidoCliente;
	public short IDEmpresa;
	public short IDFilial;
	public short IDTPedido;
	public short IDTabela;
	public short IDCondicao;
	public long IDFormaPagamento;
	public short NrItens;
	public double DescontoExtra;
	public double ValorUtilizouFlex;
	public double ValorGeraFlex;
	public double TotalGeral;
	public double TotalLiquido;
	public String Observacao;
	public Date DataPedido;
	public Date DataFaturamento;
	public boolean SugestaoVenda;
	public boolean Enviado;
	public Date DataEnvio;
	public double DescontoCliente;
	public boolean DesconsiderarFlex;
	public boolean aberto;
	private boolean status;
	private double Rentabilidade;
	public double PercentualRentabilidadeMinima;
	public double PercentualRentabilidadeMedia;
	public double PercRentabilidadeLiberado;
	public long VinculoPedido;
	public long VinculoBonificacao;
	public double SaldoBonificar;
	public boolean HistoricoPedidoCM;
	public double TotalFaturado;
	private String statusFaturado;


	// Propriedades Auxiliares
	public double TaxaCondicao;
	public boolean TemTroca;
	public String NomeCliente;
	public String DescTipoPedido;
	public String NomeEmpresa;
	public String TipoPedido;
	public boolean AlterarPedido;

	public int StatusPedidoGWeb;
	public long CodPedidoGWeb;
	public List<Combo> combos = new ArrayList<Combo>();
	public Integer Combo;
	public String RetornoStatusPedido;
	public String RetornoNumeroNotaFiscal;
    public List<Item> itensVendidos = new ArrayList<Item>();


    public boolean isHistoricoPedidoCM() {
		return HistoricoPedidoCM;
	}

	public void setHistoricoPedidoCM(boolean historicoPedidoCM) {
		HistoricoPedidoCM = historicoPedidoCM;
	}

	public double getTotalFaturado() {
		return TotalFaturado;
	}

	public void setTotalFaturado(double totalFaturado) {
		TotalFaturado = totalFaturado;
	}

	public String getStatusFaturado() {
		return statusFaturado;
	}

	public void setStatusFaturado(String statusFaturado) {
		this.statusFaturado = statusFaturado;
	}

	public void setCombos(List<Combo> combos) {
		this.combos = combos;
	}

	public boolean comboAtivado(Combo combo) {
		return combos.contains(combo);
	}

	public void removeCombo(Combo combo){
		combos.remove(combo);
	}

	public void addCombo(Combo combo){
		combos.add(combo);
	}

	public List<Combo> getCombos() {
		return combos;
	}

	public int getStatusPedidoGWeb() {
		return StatusPedidoGWeb;
	}

	public void setStatusPedidoGWeb(int statusPedidoGWeb) {
		this.StatusPedidoGWeb = statusPedidoGWeb;
	}

	public long getCodPedidoGWeb() {
		return CodPedidoGWeb;
	}

	public void setCodPedidoGWeb(long codPedidoGWeb) {
		this.CodPedidoGWeb = codPedidoGWeb;
	}

	public boolean isAlterarPedido() {
		return AlterarPedido;
	}

	public void setAlterarPedido(boolean alterarPedido) {
		AlterarPedido = alterarPedido;
	}

	public double getSaldoBonificar() {
		return SaldoBonificar;
	}

	public void setSaldoBonificar(double saldoBonificar) {
		SaldoBonificar = saldoBonificar;
	}

	public void calcularSaldoBonificar(double percentualBonificacao){
		this.SaldoBonificar = MathUtils.arredondar((this.TotalLiquido * percentualBonificacao)/100, 2);
	}

	public long getVinculoPedido() {
		return VinculoPedido;
	}

	public void setVinculoPedido(long vinculoPedido) {
		VinculoPedido = vinculoPedido;
	}

	public long getVinculoBonificacao() {
		return VinculoBonificacao;
	}

	public void setVinculoBonificacao(long vinculoBonificacao) {
		VinculoBonificacao = vinculoBonificacao;
	}

	public double getPercRentabilidadeLiberado() {
		return PercRentabilidadeLiberado;
	}

	public void setPercRentabilidadeLiberado(double percRentabilidadeLiberado) {
		PercRentabilidadeLiberado = percRentabilidadeLiberado;
	}

	public double getRentabilidade() {
		return Rentabilidade;
	}

	public void setRentabilidade(double rentabilidade) {
		Rentabilidade = rentabilidade;
	}

	public double getPercentualRentabilidadeMinima() {
		return PercentualRentabilidadeMinima;
	}

	public void setPercentualRentabilidadeMinima(double percentualRentabilidadeMinima) {
		PercentualRentabilidadeMinima = percentualRentabilidadeMinima;
	}

	public double getPercentualRentabilidadeMedia() {
		return PercentualRentabilidadeMedia;
	}

	public void setPercentualRentabilidadeMedia(double percentualRentabilidadeMedia) {
		PercentualRentabilidadeMedia = percentualRentabilidadeMedia;
	}

	public boolean isStatus() { return status; }

	public void setStatus(boolean status) { this.status = status; }

	public boolean isAberto() { return aberto; }

	public void setAberto(boolean aberto) { this.aberto = aberto; }

	public String getTipoPedido() {
		return TipoPedido;
	}

	public void setTipoPedido(String tipoPedido) {
		TipoPedido = tipoPedido;
	}

	public long getIDCliente() {
		return IDCliente;
	}

	public void setIDCliente(long iDCliente) {
		IDCliente = iDCliente;
	}

	public long getIDPedido() {
		return IDPedido;
	}

	public void setIDPedido(long iDPedido) {
		IDPedido = iDPedido;
	}

	public String getPedidoCliente() {
		return PedidoCliente;
	}

	public void setPedidoCliente(String pedidoCliente) {
		PedidoCliente = pedidoCliente;
	}

	public short getIDEmpresa() {
		return IDEmpresa;
	}

	public void setIDEmpresa(short iDEmpresa) {
		IDEmpresa = iDEmpresa;
	}

	public short getIDFilial() {
		return IDFilial;
	}

	public void setIDFilial(short iDFilial) {
		IDFilial = iDFilial;
	}

	public short getIDTPedido() {
		return IDTPedido;
	}

	public void setIDTPedido(short iDTPedido) {
		IDTPedido = iDTPedido;
	}

	public short getIDTabela() {
		return IDTabela;
	}

	public void setIDTabela(short iDTabela) {
		IDTabela = iDTabela;
	}

	public short getIDCondicao() {
		return IDCondicao;
	}

	public void setIDCondicao(short iDCondicao) {
		IDCondicao = iDCondicao;
	}

	public long getIDFormaPagamento() {
		return IDFormaPagamento;
	}

	public void setIDFormaPagamento(long iDFormaPagamento) {
		IDFormaPagamento = iDFormaPagamento;
	}

	public short getNrItens() {
		return NrItens;
	}

	public void setNrItens(short nrItens) {
		NrItens = nrItens;
	}

	public double getDescontoExtra() {
		return DescontoExtra;
	}

	public void setDescontoExtra(double descontoExtra) {
		DescontoExtra = descontoExtra;
	}

	public double getValorUtilizouFlex() {
		return ValorUtilizouFlex;
	}

	public void setValorUtilizouFlex(double valorUtilizouFlex) {
		ValorUtilizouFlex = valorUtilizouFlex;
	}

	public double getValorGeraFlex() {
		return ValorGeraFlex;
	}

	public void setValorGeraFlex(double valorGeraFlex) {
		ValorGeraFlex = valorGeraFlex;
	}

	public double getTotalGeral() {
		return TotalGeral;
	}

	public void setTotalGeral(double totalGeral) {
		TotalGeral = totalGeral;
	}

	public double getTotalLiquido() {
		return TotalLiquido;
	}

	public void setTotalLiquido(double totalLiquido) {
		TotalLiquido = totalLiquido;
	}

	public String getObservacao() {
		return Observacao;
	}

	public void setObservacao(String observacao) {
		Observacao = observacao;
	}

	public Date getDataPedido() {
		return DataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		DataPedido = dataPedido;
	}

	public Date getDataFaturamento() {
		return DataFaturamento;
	}

	public void setDataFaturamento(Date dataFaturamento) {
		DataFaturamento = dataFaturamento;
	}

	public boolean isSugestaoVenda() {
		return SugestaoVenda;
	}

	public void setSugestaoVenda(boolean sugestaoVenda) {
		SugestaoVenda = sugestaoVenda;
	}

	public boolean isEnviado() {
		return Enviado;
	}

	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

	public double getDescontoCliente() {
		return DescontoCliente;
	}

	public void setDescontoCliente(double descontoCliente) {
		DescontoCliente = descontoCliente;
	}

	public boolean isDesconsiderarFlex() {
		return DesconsiderarFlex;
	}

	public void setDesconsiderarFlex(boolean desconsiderarFlex) {
		DesconsiderarFlex = desconsiderarFlex;
	}

	public double getTaxaCondicao() {
		return TaxaCondicao;
	}

	public void setTaxaCondicao(double taxaCondicao) {
		TaxaCondicao = taxaCondicao;
	}

	public boolean isTemTroca() {
		return TemTroca;
	}

	public void setTemTroca(boolean temTroca) {
		TemTroca = temTroca;
	}

	public String getNomeCliente() {
		return NomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		NomeCliente = nomeCliente;
	}

	public String getDescTipoPedido() {
		return DescTipoPedido;
	}

	public void setDescTipoPedido(String descTipoPedido) {
		DescTipoPedido = descTipoPedido;
	}

	public String getNomeEmpresa() {
		return NomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		NomeEmpresa = nomeEmpresa;
	}

	public String getRetornoStatusPedido() {
		return RetornoStatusPedido;
	}

	public void setRetornoStatusPedido(String retornoStatusPedido) {
		RetornoStatusPedido = retornoStatusPedido;
	}

	public String getRetornoNumeroNotaFiscal() {
		return RetornoNumeroNotaFiscal;
	}

	public void setRetornoNumeroNotaFiscal(String retornoNumeroNotaFiscal) {
		RetornoNumeroNotaFiscal = retornoNumeroNotaFiscal;
	}

	public boolean isAlterado() {
		return DataEnvio.compareTo(DataPedido)!=0;
	}

	public boolean isBonificacao(Context context){
		TipoPedido tipoPedido = null;
		if (String.valueOf(this.IDTPedido) != null) {
			 tipoPedido = new TipoPedidoDAO(context, null).get(this.IDTPedido);
		}
		else {
			return false;
		}
		return tipoPedido.getTipoOperacao().equals(TipoOperacao.BONIFICACAO.toString());
	}

	public static void atualizarStatusPedidoCM(Context context, String idCentralManager, int statusCentralManager, Long idPedido) {
		new PedidoDAO(context, null).atualizaCampoPedidoMQTT(idCentralManager,idPedido,true, statusCentralManager);
	}

	public void retirarProdutoPedido(ItemDigitacao itemDigitacao, List<ItemDigitacao> itemPedido) {
		// Diminuir o n?mero de itens do pedido
		this.setNrItens((short) (this.getNrItens() - 1));

		// Diminuir o valor do pedido
		this.setTotalGeral(this.getTotalGeral() - itemDigitacao.getValorBruto());
		this.setTotalLiquido(this.getTotalLiquido() - itemDigitacao.getValorLiquido());
		this.setValorUtilizouFlex(this.getValorUtilizouFlex() - itemDigitacao.getValorUtilizaFlex());
		this.setValorGeraFlex(this.getValorGeraFlex() - itemDigitacao.getValorGeraFlex());
		this.apurarRentabilidadePedido(itemPedido);
	}

	public void inserirProdutoPedido(ItemDigitacao itemDigitacao, Parametro parametro, TipoPedido tipoPedido, List<ItemDigitacao> itemPedido) {
		itemDigitacao.setValorBruto(itemDigitacao.getValorUnitOriginal() * itemDigitacao.getQuantidade());
		itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade());

		if (!parametro.isPercDescontoProdutoGeraVerba()) {
			if (parametro.isPermiteAlterarPreco()) {
				if (itemDigitacao.getValorUnit() > itemDigitacao.getValorUnitOriginal()) {
					if (parametro.isExcessoPrecoGeraFlex())
						itemDigitacao.setValorGeraFlex(itemDigitacao.getValorUnit() - itemDigitacao.getValorUnitOriginal());
					else
						itemDigitacao.setValorGeraFlex(0.0);
				} else if (itemDigitacao.getValorUnit() < itemDigitacao.getValorUnitOriginal()) {
					itemDigitacao.setValorGeraFlex(0.0);
					itemDigitacao.setValorUtilizaFlex(itemDigitacao.getValorUnitOriginal() - itemDigitacao.getValorUnit());
				} else {
					itemDigitacao.setValorGeraFlex(0.0);
					itemDigitacao.setValorUtilizaFlex(0.0);
				}
			}
			itemDigitacao.setValorGeraFlex(Utils.arredondar(itemDigitacao.getValorGeraFlex() * itemDigitacao.getQuantidade(), 4));
			itemDigitacao.setValorUtilizaFlex(Utils.arredondar(itemDigitacao.getValorUtilizaFlex() * itemDigitacao.getQuantidade(), 4));

			if (!parametro.isPermiteAlterarPreco())
				itemDigitacao.setValorLiquido(Utils.arredondar(itemDigitacao.getValorBruto() - (itemDigitacao.getValorBruto() * (itemDigitacao.getDesconto()+ itemDigitacao.getPercentualDescontoCombo() / 100)) - itemDigitacao.getValorUtilizaFlex(), 4));
			else {
				itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade());
			}
		} else {
			if (tipoPedido.getAbaterSaldoVerba() == 2) {
				itemDigitacao.setValorGeraFlex(Utils.arredondar(itemDigitacao.getValorBruto() * itemDigitacao.getPercGera() / 100, 4));
				itemDigitacao.setValorUtilizaFlex(Utils.arredondar(itemDigitacao.getValorBruto() * itemDigitacao.getDesconto() / 100, 4));
			} else if (tipoPedido.getAbaterSaldoVerba() == 1) {
				itemDigitacao.setValorUtilizaFlex(itemDigitacao.getValorLiquido());
			}
		}
		this.setValorUtilizouFlex(this.getValorUtilizouFlex() + itemDigitacao.getValorUtilizaFlex());

		/*
		 * if (!parametro.isPercDescontoProdutoGeraVerba()) { if
		 * (tipoPedido.getAbaterSaldoVerba() == 2 && !pedido.isSugestaoVenda())
		 * { pedido.setSaldoVerba(pedido.getSaldoVerba() -
		 * itemDigitacao.getValorUtilizaFlex());
		 *
		 * if (parametro.getGeraVerbaPalmtop() == 1)
		 * pedido.setSaldoVerba(pedido.getSaldoVerba() +
		 * itemDigitacao.getValorGeraFlex()); } else if
		 * (tipoPedido.getAbaterSaldoVerba() == 1) {
		 * pedido.setSaldoVerba(pedido.getSaldoVerba() -
		 * itemDigitacao.getValorLiquido()); } } else { if
		 * (tipoPedido.getAbaterSaldoVerba() == 2) {
		 * pedido.setSaldoVerba(pedido.getSaldoVerba() +
		 * itemDigitacao.getValorGeraFlex());
		 * pedido.setSaldoVerba(pedido.getSaldoVerba() -
		 * itemDigitacao.getValorUtilizaFlex()); } else if
		 * (tipoPedido.getAbaterSaldoVerba() == 1) {
		 * pedido.setSaldoVerba(pedido.getSaldoVerba() -
		 * itemDigitacao.getValorUtilizaFlex()); } }
		 */

		this.setTotalGeral(this.getTotalGeral() + itemDigitacao.getValorBruto());
		this.setTotalLiquido(this.getTotalLiquido() + itemDigitacao.getValorLiquido());
		this.apurarRentabilidadePedido(itemPedido);

		if (itemDigitacao.getQuantidade() > 0)
			this.setNrItens((short) (this.getNrItens() + 1));
	}


	public void apurarRentabilidadePedido(List<ItemDigitacao> itemPedido) {
		double precoCustoTotal = 0d;
		double valorAlterado = 0.d;
		double resultado = 0d;


		if (!itemPedido.isEmpty()) {
            for (ItemDigitacao item : itemPedido) {
                double desconto =  item.getDesconto() - item.getPercentualDescontoCombo();


            	if (item.getItemComboAtivado() > 0 && desconto > 0) {
					double valorUntarioAlterado = Utils.arredondar(item.getValorUnitOriginal() * (1 - (desconto / 100)), 2);
					precoCustoTotal = precoCustoTotal + (item.getPrecoCustoProduto() * item.getQuantidade());
					valorAlterado = valorAlterado + (valorUntarioAlterado * item.getQuantidade());
				}else if (item.getItemComboAtivado() > 0 && desconto <=0){
					precoCustoTotal = precoCustoTotal + (item.getPrecoCustoProduto() * item.getQuantidade());
					valorAlterado = valorAlterado + (item.getValorUnitOriginal() * item.getQuantidade());
				}else {
					precoCustoTotal = precoCustoTotal + (item.getPrecoCustoProduto() * item.getQuantidade());
					valorAlterado = valorAlterado + (item.getValorUnit() * item.getQuantidade());
				}
			}

			this.setRentabilidade((valorAlterado - precoCustoTotal)/ precoCustoTotal * 100);
        }else{
            this.setRentabilidade(0.00);

        }

	}


	public boolean mesmaCondicaoPagamento(Context ctx, Combo combo) {
		List<ComboXProduto> comboXProdutos = RegraCompra.buscarProdutosCombo(ctx);

		for (ComboXProduto cxp : comboXProdutos){
			if (cxp.getIdCombo() == combo.getIdCombo()){
				if (cxp.getIdCondicao() == 0){
					return true;
				}
				return this.getIDCondicao() == cxp.getIdCondicao();
			}
		}

		return false;
	}

	public Pedido reapurarPrecosPedido(Context ctx, TipoPedido tipoPedido, Tabela tabela, Condicao condicao, Pedido pedidoAlterado) {
		Pedido pedido = null;
		try {

			pedido = (Pedido) Utils.recuperarObjetoSerializado(ctx, PedidoTabActivity.PEDIDO_SERIALIZAR);
			TipoPedido tipoPedidoAux = new TipoPedidoDAO(ctx, null).get(pedido.getIDTPedido());
			Parametro parametro = new ParametroDAO(ctx, null).get();
			if (tipoPedidoAux != null && tipoPedidoAux.getIdTipoPedido() != tipoPedido.getIdTipoPedido()) {
				tipoPedido = tipoPedidoAux;
				pedido.setIDTPedido(tipoPedido.getIdTipoPedido());
			}

			Tabela tabelaAux = new TabelaDAO(ctx, null).get(pedido.getIDTabela());
			Condicao condicaoAux = new CondicaoDAO(ctx, null).get(pedido.getIDCondicao());

			if (mudouTabelaPrecoOuCondicaoPagamento(tabelaAux, condicaoAux, tabela, condicao) || mudouCondicaoPagamentoPedidoAlteracao(pedidoAlterado, condicao) || acaoCombo) {
				tabela = tabelaAux;
				condicao = condicaoAux;


				for (int i = 0; i < itemPedido.size(); i++) {
					ItemDigitacao itemDigitacao = itemPedido.get(i);

					retirarProdutoPedido(itemDigitacao, itemPedido);

					if (!(itemDigitacao.getItemComboAtivado() > 0)) {
						itemDigitacao.setValorUnitOriginal(getNovoValorOriginal(itemDigitacao));
						itemDigitacao.setValorUnit(Utils.arredondar(itemDigitacao.getValorUnitOriginal() * (1 - (itemDigitacao.getDesconto() / 100)), 2));

						itemDigitacao.setValorBruto(itemDigitacao.getValorUnitOriginal() * itemDigitacao.getQuantidade());
						itemDigitacao.setValorLiquido(itemDigitacao.getValorUnit() * itemDigitacao.getQuantidade());

						itemDigitacao.setValorUtilizaFlex(0.0);
						itemDigitacao.setValorGeraFlex(0.0);
					}

					inserirProdutoPedido(itemDigitacao, parametro, tipoPedido, itemPedido);
					acaoCombo = false;
				}

				if (itemPedido.size() > 0) {
					Utils.serializarObjeto(ctx, PedidoTabActivity.PEDIDO_SERIALIZAR, pedido);
					Utils.serializarObjeto(ctx, ITENSPEDIDO_SERIALIZAR, itemPedido);
				}


			}
		} catch (Exception e) {
			Log.e("ItemTabActivity.reapurarPrecos", e.getMessage());
		}

		return pedido;
	}

	private boolean mudouCondicaoPagamentoPedidoAlteracao(Pedido pedidoAlteracao, Condicao condicao) {
		return pedidoAlteracao != null && pedidoAlteracao.getIDCondicao() != condicao.getIdCondicao();
	}

	private boolean mudouTabelaPrecoOuCondicaoPagamento(Tabela tabelaAux, Condicao condicaoAux, Tabela tabela, Condicao condicao) {
		return tabelaAux.getIdTabela() != tabela.getIdTabela() || condicaoAux.getIdCondicao() != condicao.getIdCondicao();
	}

	private double getNovoValorOriginal(ItemDigitacao item) {
		for (ItemDigitacao itemNovaLista : itemCompleto) {
			if (itemNovaLista.getProduto().getIdProduto() == item.getProduto().getIdProduto()) {
				return itemNovaLista.getValorUnitOriginal();
			}
		}

		return item.getValorUnitOriginal();
	}

	public void executarOperacoesParaDigitacao(final Activity activity, final Pedido pedido){

		if (pedido == null)
			return;

		new TaskGeneric(activity,activity.getString(R.string.por_favor_aguarde_carregando_estruturas_alt),
				new TaskGeneric.IObjectIntance() {
			@Override
			public Object addObject() {
				return new OperacoesPedidoDTO(activity, pedido);
			}

			@Override
			public String execute(Object object) {
				return reapurarItemPedido(activity, object);
			}

			@Override
			public void posExecute(String s) {
				if (s != null) {
					ToastUtils.mostrarMensagem(activity, s);
				}
			}
		}).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}


	public String reapurarItemPedido(Activity mActivity, Object object){
		OperacoesPedidoDTO operacoes = (OperacoesPedidoDTO)object;
		try {
			List<ItemDigitacao> itemCompleto = new ItemDigitacaoDAO(mActivity.getBaseContext(), null)
					.getEstruturaDigitacaoPedido(operacoes.getTipoPedido(), operacoes.getTabela(), operacoes.getCondicao());

			itemFiltrado = new ArrayList<ItemDigitacao>(itemCompleto);
			itemAux = new ArrayList<ItemDigitacao>(itemCompleto);
			itemPedido = new ArrayList<ItemDigitacao>();

			List<ItemDigitacao> itensRecuperados = null;
			List<Item> itensPedidoAlteracao = null;

			Object obj = Utils.recuperarObjetoSerializado(mActivity.getBaseContext(), ITENSPEDIDO_SERIALIZAR);
			if (obj != null) {
				// Caso esteja recuperando um pedido que n?o foi finalizado corretamente
				itensRecuperados = (ArrayList<ItemDigitacao>) obj;
			} else {
				// Busca os itens do pedido caso seja uma altera??o, se for um pedido novo essa lista ficar? vazia
				itensPedidoAlteracao = new ItemDAO(mActivity.getBaseContext(), null).listaByPedido(operacoes.getPedido().getIDPedido());
			}

			if (itensRecuperados != null || (itensPedidoAlteracao != null && itensPedidoAlteracao.size() > 0)) {
				if (itensPedidoAlteracao != null) {
					Item itemAux;
					int aux = 0;
					for (ItemDigitacao itemDigitacao : itemFiltrado) {
						if (aux == itensPedidoAlteracao.size())
							break;

						itemAux = null;

						for (Item itemAlterar : itensPedidoAlteracao) {
							if (itemAlterar.getIDProduto() == itemDigitacao.getProduto().getIdProduto()) {
								itemAux = itemAlterar;
								aux++;
								break;
							}
						}

						if (itemAux != null) {
							itemDigitacao.setQuantidade(itemAux.getQuantidade());
							itemDigitacao.setQuantidadeGravada(itemAux.getQuantidade());
							itemDigitacao.setDesconto(itemAux.getDesconto());
							itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
							// itemDigitacao.setValorUtilizaFlex(itemAux.getValorUtilizouFlex());
							// itemDigitacao.setValorGeraFlex(itemAux.getValorGeraraFlex());
							itemDigitacao.setValorUnit(itemAux.getValorUnitPraticado());
							itemDigitacao.setValorBruto(itemAux.getTotalGeral());
							itemDigitacao.setValorLiquido(itemAux.getTotalLiquido());
							itemDigitacao.setRentabilidade(itemAux.getRentabilidade());
							itemDigitacao.setPercentualDescontoCombo(itemAux.getPercentualDescontoCombo());
							itemDigitacao.setQuantidadeItemLimiteCombo(itemAux.getQuantidadeItemLimiteCombo());
							itemDigitacao.setItemComboAtivado(itemAux.getItemComboAtivado());

							itemPedido.add(itemDigitacao);
						}
					}
					Utils.serializarObjeto(mActivity.getBaseContext(), ITENSPEDIDO_SERIALIZAR_ALTERAR, itemPedido);
				} else if (itensRecuperados != null) {
					int aux = 0;
					ItemDigitacao itemAux = null;

					for (ItemDigitacao itemDigitacao : itemFiltrado) {
						if (aux == itensRecuperados.size())
							break;

						itemAux = null;

						for (ItemDigitacao itemRecuperar : itensRecuperados) {
							if (itemRecuperar.getProduto().getIdProduto() == itemDigitacao.getProduto().getIdProduto()) {
								itemAux = itemRecuperar;
								aux++;
								break;
							}
						}

						if (itemAux != null) {
							double desconto =  itemAux.getDesconto() - itemAux.getPercentualDescontoCombo();

							if (itemAux.getPrecoCustoProduto() > 0 && itemAux.getItemComboAtivado() > 0 && desconto >= 0) {
								double valorUntarioAlterado = Utils.arredondar(itemAux.getValorUnitOriginal() * (1 - (desconto / 100)), 2);
								itemAux.setRentabilidade(MathUtils.arredondar((valorUntarioAlterado - itemAux.getPrecoCustoProduto()) / itemAux.getPrecoCustoProduto() * 100, 2));
							}
							itemDigitacao.setQuantidade(itemAux.getQuantidade());
							itemDigitacao.setDesconto(itemAux.getDesconto());
							itemDigitacao.setValorDesconto(itemAux.getValorDesconto());
							// itemDigitacao.setValorFlex(itemAux.getValorFlex());
							// itemDigitacao.setValorUtilizaFlex(itemAux.getValorUtilizaFlex());
							// itemDigitacao.setValorGeraFlex(itemAux.getValorGeraFlex());
							itemDigitacao.setValorUnit(itemAux.getValorUnit());
							itemDigitacao.setValorBruto(itemAux.getValorBruto());
							itemDigitacao.setValorLiquido(itemAux.getValorLiquido());
							itemDigitacao.setRentabilidade(itemAux.getRentabilidade());
							itemDigitacao.setPercentualDescontoCombo(itemAux.getPercentualDescontoCombo());
							itemDigitacao.setQuantidadeItemLimiteCombo(itemAux.getQuantidadeItemLimiteCombo());
							itemDigitacao.setItemComboAtivado(itemAux.getItemComboAtivado());


							itemPedido.add(itemDigitacao);
						}
					}
				}
			}
		} catch (Exception e) {
			return mActivity.getString(R.string.erro_inicializando_estrutura_digitacao) + (e != null ? e.getMessage() : "Exception null");
		}
		return null;
	}

    public List<Item> getItensVendidos() {
        return itensVendidos;
    }

    public void setItensVendidos(List<Item> itensVendidos) {
        this.itensVendidos = itensVendidos;
    }

    public void zerar() {
        this.setTotalFaturado(0);
        this.setNrItens((short) 0);
        this.setTotalLiquido(0);
        this.setTotalGeral(0);
        this.setRentabilidade(0);
    }

	public String toLinhaTexto(long idVendedor) {

			BigDecimal big;
			String valorAux;
			String campo;
			BigDecimal fourDecimalPlaces = new BigDecimal(10000);
			StringBuilder builder = new StringBuilder();
			builder.append(format("%09d", getIDCliente()));
			builder.append(format("%04d", getIDEmpresa()));
			builder.append(format("%04d", getIDFilial()));
			builder.append(format("%06d%08d", getIDPedido(), idVendedor));
			builder.append(format("%-15s", getPedidoCliente()));
			builder.append(format("%06d", getIDTPedido()));
			builder.append(format("%04d", getIDTabela()));
			builder.append(format("%04d", getIDFormaPagamento()));
			builder.append(format("%04d", getIDCondicao()));
			builder.append(format("%03d", getNrItens()));

			big = new BigDecimal(getDescontoExtra()).setScale(4, BigDecimal.ROUND_HALF_UP);
			valorAux = format("%s", big.multiply(fourDecimalPlaces));
			valorAux = valorAux.substring(0, valorAux.indexOf("."));
			builder.append(format("%012d", Long.parseLong(valorAux)));


			big = new BigDecimal(getValorUtilizouFlex()).setScale(4, BigDecimal.ROUND_HALF_UP);
			valorAux = format("%s", big.multiply(fourDecimalPlaces));
			valorAux = valorAux.substring(0, valorAux.indexOf("."));
			builder.append(format("%012d", Long.parseLong(valorAux)));


			big = new BigDecimal(getValorGeraFlex()).setScale(4, BigDecimal.ROUND_HALF_UP);
			valorAux = format("%s", big.multiply(fourDecimalPlaces));
			valorAux = valorAux.substring(0, valorAux.indexOf("."));
			builder.append(format("%012d", Integer.parseInt(valorAux)));


			big = new BigDecimal(getTotalGeral()).setScale(4, BigDecimal.ROUND_HALF_UP);
			valorAux = format("%s", big.multiply(fourDecimalPlaces));
			valorAux = valorAux.substring(0, valorAux.indexOf("."));
			builder.append(format("%012d", Integer.parseInt(valorAux)));

			big = new BigDecimal(getTotalLiquido()).setScale(4, BigDecimal.ROUND_HALF_UP);
			valorAux = format("%s", big.multiply(fourDecimalPlaces));
			valorAux = valorAux.substring(0, valorAux.indexOf("."));
			builder.append(format("%012d", Integer.parseInt(valorAux)));

			builder.append(format("%-240s", validarCaracterSpecial(getObservacao())));
			builder.append(Utils.sdfDataExport.format(getDataPedido()));
			builder.append(Utils.sdfDataExport.format(getDataFaturamento()));
			builder.append(isSugestaoVenda() ? "1" : "0");
			builder.append(StringUtils.QUEBRA_LINHA);
			campo = builder.toString();
			return campo;

	}

	public String validarCaracterSpecial(String text) {
		return Utils.removerCaracteresEspeciais(text);
	}
}
