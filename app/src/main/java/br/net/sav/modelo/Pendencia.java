package br.net.sav.modelo;

import java.util.Date;

public class Pendencia {
	public long IdCliente;
	public long NrTitulo;
	public String TipoDocumento;
	public double TaxaCorrecaoDiaria;
	public Date Vencimento;
	public String Portador;
	public double Valor;
	public double Saldo;

	// Propriedades Auxiliar
	public double SaldoCorrigido ;
	public String Razao ;

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long iDCliente) {
		IdCliente = iDCliente;
	}

	public long getNrTitulo() {
		return NrTitulo;
	}

	public void setNrTitulo(long nrTitulo) {
		NrTitulo = nrTitulo;
	}

	public String getTipoDocumento() {
		return TipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		TipoDocumento = tipoDocumento;
	}

	public double getTaxaCorrecaoDiaria() {
		return TaxaCorrecaoDiaria;
	}

	public void setTaxaCorrecaoDiaria(double taxaCorrecaoDiaria) {
		TaxaCorrecaoDiaria = taxaCorrecaoDiaria;
	}

	public Date getVencimento() {
		return Vencimento;
	}

	public void setVencimento(Date vencimento) {
		Vencimento = vencimento;
	}

	public String getPortador() {
		return Portador;
	}

	public void setPortador(String portador) {
		Portador = portador;
	}

	public double getValor() {
		return Valor;
	}

	public void setValor(double valor) {
		Valor = valor;
	}

	public double getSaldo() {
		return Saldo;
	}

	public void setSaldo(double saldo) {
		Saldo = saldo;
	}

	public double getSaldoCorrigido() {
		return SaldoCorrigido;
	}

	public void setSaldoCorrigido(double saldoCorrigido) {
		SaldoCorrigido = saldoCorrigido;
	}

	public String getRazao() {
		return Razao;
	}

	public void setRazao(String razao) {
		Razao = razao;
	}	
}
