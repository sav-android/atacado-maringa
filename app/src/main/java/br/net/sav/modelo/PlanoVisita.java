package br.net.sav.modelo;

public class PlanoVisita {
	public long QuantidadeDias;
	public String Descricao;
	
	public long getQuantidadeDias() {
		return QuantidadeDias;
	}
	public void setQuantidadeDias(long quantidadeDias) {
		QuantidadeDias = quantidadeDias;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
}
