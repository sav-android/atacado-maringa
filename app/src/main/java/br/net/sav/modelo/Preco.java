package br.net.sav.modelo;

public class Preco {

	public long IdProduto;
	public short IdTabela;
	public short IdEmpresa;
	public short IdFilial;
	public short CodigoEmbalagem;
	public double PrecoNormal;
	public double PrecoMinimo;
	public double PercComissao;

	public double getPercComissao() {
		return PercComissao;
	}

	public void setPercComissao(double percComissao) {
		PercComissao = percComissao;
	}

	public long getIdProduto() {
		return IdProduto;
	}

	public void setIdProduto(long iDProduto) {
		IdProduto = iDProduto;
	}

	public short getIdTabela() {
		return IdTabela;
	}

	public void setIdTabela(short iDTabela) {
		IdTabela = iDTabela;
	}

	public short getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(short iDEmpresa) {
		IdEmpresa = iDEmpresa;
	}

	public short getIdFilial() {
		return IdFilial;
	}

	public void setIdFilial(short iDFilial) {
		IdFilial = iDFilial;
	}

	public short getCodigoEmbalagem() {
		return CodigoEmbalagem;
	}

	public void setCodigoEmbalagem(short codigoEmbalagem) {
		CodigoEmbalagem = codigoEmbalagem;
	}

	public double getPrecoNormal() {
		return PrecoNormal;
	}

	public void setPrecoNormal(double precoNormal) {
		PrecoNormal = precoNormal;
	}

	public double getPrecoMinimo() {
		return PrecoMinimo;
	}

	public void setPrecoMinimo(double precoMinimo) {
		PrecoMinimo = precoMinimo;
	}

}
