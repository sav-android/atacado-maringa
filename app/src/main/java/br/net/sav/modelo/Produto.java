package br.net.sav.modelo;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.net.sav.dao.ProdutoDAO;
import br.net.sav.utils.ShearedPreferenceUltils;

@SuppressWarnings("serial")
public class Produto implements Serializable {
	public long IdProduto;
	public short IdFornecedor;
	public String Descricao;
	public short CodigoEmbalagem;
	public String DescricaoEmbalagem;
	public long QuantidadeEmbalagem;
	public short IdSecao;
	public short IdICMS;
	public String CodigoBarra;
	public short IdSubgrupo;
	public String ControleGordura;
	public double DescontoGordura;
	public short Multiplo;
	public short IdGrupo;
	public short IdLinha;
	public short EstoqueBaixo;
	public short QuantidadeCaixa;
	public String SaldoEstoque;
	public boolean PermiteVender;
	public double Peso;
	public String Status;
	public boolean PermiteMeiaCaixa;
	public String CodigoReferencia;
	public boolean PermiteBonificacao;
	public double MaximoDesconto;
	public short Unitario;
	public long IdMarca;
	public long DivisaoPreco;
	public String DescricaoMarca;
	public boolean PossuiFoto;
	public double PercentualRentabilidadeMinima;
	public double PercentualRentabilidadeMedia;
	public double PrecoCustoProduto;
	public String CodigoNCM;

	public String getCodigoNCM() {
		return CodigoNCM;
	}

	public void setCodigoNCM(String codigoNCM) {
		CodigoNCM = codigoNCM;
	}

	public double getPercentualRentabilidadeMinima() {
		return PercentualRentabilidadeMinima;
	}

	public void setPercentualRentabilidadeMinima(double percentualRentabilidadeMinima) {
		PercentualRentabilidadeMinima = percentualRentabilidadeMinima;
	}

	public double getPercentualRentabilidadeMedia() {
		return PercentualRentabilidadeMedia;
	}

	public void setPercentualRentabilidadeMedia(double percentualRentabilidadeMedia) {
		PercentualRentabilidadeMedia = percentualRentabilidadeMedia;
	}

	public double getPrecoCustoProduto() {
		return PrecoCustoProduto;
	}

	public void setPrecoCustoProduto(double precoCustoProduto) {
		PrecoCustoProduto = precoCustoProduto;
	}

	public long getIdProduto() {
		return IdProduto;
	}

	public boolean isPossuiFoto() {
		return PossuiFoto;
	}

	public void setPossuiFoto(boolean possuiFoto) {
		PossuiFoto = possuiFoto;
	}

	public void setIdProduto(long iDProduto) {
		IdProduto = iDProduto;
	}

	public short getIdFornecedor() {
		return IdFornecedor;
	}

	public long getDivisaoPreco() {
		return DivisaoPreco;
	}

	public void setDivisaoPreco(long divisaoPreco) {
		DivisaoPreco = divisaoPreco;
	}

	public void setIdFornecedor(short iDFornecedor) {
		IdFornecedor = iDFornecedor;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public short getCodigoEmbalagem() {
		return CodigoEmbalagem;
	}

	public void setCodigoEmbalagem(short codigoEmbalagem) {
		CodigoEmbalagem = codigoEmbalagem;
	}

	public String getDescricaoEmbalagem() {
		return DescricaoEmbalagem;
	}

	public void setDescricaoEmbalagem(String descricaoEmbalagem) {
		DescricaoEmbalagem = descricaoEmbalagem;
	}

	public long getQuantidadeEmbalagem() {
		return QuantidadeEmbalagem;
	}

	public void setQuantidadeEmbalagem(long quantidadeEmbalagem) {
		QuantidadeEmbalagem = quantidadeEmbalagem;
	}

	public short getIdSecao() {
		return IdSecao;
	}

	public void setIdSecao(short iDSecao) {
		IdSecao = iDSecao;
	}

	public String getCodigoBarra() {
		return CodigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		CodigoBarra = codigoBarra;
	}

	public void setIdSubGrupo(short idSubgrupo) {
		IdSubgrupo = idSubgrupo;
	}

	public short getIdICMS() {
		return IdICMS;
	}

	public void setIdICMS(short iDICMS) {
		IdICMS = iDICMS;
	}

	public short getIdSubGrupo() {
		return IdSubgrupo;
	}

	public String getControleGordura() {
		return ControleGordura;
	}

	public void setControleGordura(String controleGordura) {
		ControleGordura = controleGordura;
	}

	public double getDescontoGordura() {
		return DescontoGordura;
	}

	public void setDescontoGordura(double descontoGordura) {
		DescontoGordura = descontoGordura;
	}

	public short getMultiplo() {
		return Multiplo;
	}

	public void setMultiplo(short multiplo) {
		Multiplo = multiplo;
	}

	public short getIdGrupo() {
		return IdGrupo;
	}

	public void setIdGrupo(short iDGrupo) {
		IdGrupo = iDGrupo;
	}

	public short getIdLinha() {
		return IdLinha;
	}

	public void setIdLinha(short iDLinha) {
		IdLinha = iDLinha;
	}

	public short getEstoqueBaixo() {
		return EstoqueBaixo;
	}

	public void setEstoqueBaixo(short estoqueBaixo) {
		EstoqueBaixo = estoqueBaixo;
	}

	public short getQuantidadeCaixa() {
		return QuantidadeCaixa;
	}

	public void setQuantidadeCaixa(short quantidadeCaixa) {
		QuantidadeCaixa = quantidadeCaixa;
	}

	public String getSaldoEstoque() {
		return SaldoEstoque;
	}

	public void setSaldoEstoque(String saldoEstoque) {
		SaldoEstoque = saldoEstoque;
	}

	public boolean isPermiteVender() {
		return PermiteVender;
	}

	public void setPermiteVender(boolean permiteVender) {
		PermiteVender = permiteVender;
	}

	public double getPeso() {
		return Peso;
	}

	public void setPeso(double peso) {
		Peso = peso;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public boolean isPermiteMeiaCaixa() {
		return PermiteMeiaCaixa;
	}

	public void setPermiteMeiaCaixa(boolean permiteMeiaCaixa) {
		PermiteMeiaCaixa = permiteMeiaCaixa;
	}

	public String getCodigoReferencia() {
		return CodigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		CodigoReferencia = codigoReferencia;
	}

	public boolean isPermiteBonificacao() {
		return PermiteBonificacao;
	}

	public void setPermiteBonificacao(boolean permiteBonificacao) {
		PermiteBonificacao = permiteBonificacao;
	}

	public double getMaximoDesconto() {
		return MaximoDesconto;
	}

	public void setMaximoDesconto(double maximoDesconto) {
		MaximoDesconto = maximoDesconto;
	}

	public short getUnitario() {
		return Unitario;
	}

	public void setUnitario(short unitario) {
		Unitario = unitario;
	}

	public long getIdMarca() {
		return IdMarca;
	}

	public void setIdMarca(long iDMarca) {
		IdMarca = iDMarca;
	}

	public String getDescricaoMarca() {
		return DescricaoMarca;
	}

	public void setDescricaoMarca(String descricaoMarca) {
		DescricaoMarca = descricaoMarca;
	}

	@NonNull
	public static List<String> buscarListaDeFotos() {

		File file = new File( Environment.getExternalStorageDirectory()
				.getPath() + "/sav/fotos/");
		return Arrays.asList(file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String filename) {
				return filename.contains("jpeg.jpg")|| filename.contains("(") || filename.contains(",")||
						filename.contains("-")|| filename.contains(";") ? false :
						filename.contains(".png") || filename.contains(".jpg") || filename.contains(".JPG") || filename.contains(".jpeg");
			}
		}));
	}

	public static void atualizaFotos(Context context, List<String> listaDeFotos,int quantidade) {
		ProdutoDAO produtoDAO = new ProdutoDAO(context, null);
		List<String> idRetirado = new ArrayList<>();
		Boolean fotoAtualizada = ShearedPreferenceUltils.getFotoAtualizada(context);

		if ((quantidade != Produto.buscarListaDeFotos().size()) || fotoAtualizada) {
			for (String id: listaDeFotos){
				String idString = id.replaceAll("[^0-9.]", "").replace(".", "");
				idRetirado.add(idString);
			}

			produtoDAO.updatesProduto(null, idRetirado);
			ShearedPreferenceUltils.setQuantidade(context, listaDeFotos.size());
		}
		ShearedPreferenceUltils.setFotoAtualizada(context, false);
	}

	@Override
	public String toString() {
		return String.format("%d %s", IdProduto, Descricao);
	}
}
