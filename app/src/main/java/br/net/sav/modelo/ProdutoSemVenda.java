package br.net.sav.modelo;

import java.util.Date;

public class ProdutoSemVenda {
	public String Descricao;
	public String DescricaoEmbalagem;
	public int QtdeDias;
	public Date UltimaVenda;

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public String getDescricaoEmbalagem() {
		return DescricaoEmbalagem;
	}

	public void setDescricaoEmbalagem(String descricaoEmbalagem) {
		DescricaoEmbalagem = descricaoEmbalagem;
	}

	public int getQtdeDias() {
		return QtdeDias;
	}

	public void setQtdeDias(int qtdeDias) {
		QtdeDias = qtdeDias;
	}

	public Date getUltimaVenda() {
		return UltimaVenda;
	}

	public void setUltimaVenda(Date ultimaVenda) {
		UltimaVenda = ultimaVenda;
	}

}
