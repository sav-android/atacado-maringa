package br.net.sav.modelo;

public class PromocaoSemana {
	public long IDProduto;
	public long IDFornecedor;
	public String Descricao;
	public short Multiplo;
	public double Desconto;

	public long getIDProduto() {
		return IDProduto;
	}

	public void setIDProduto(long iDProduto) {
		IDProduto = iDProduto;
	}

	public long getIDFornecedor() {
		return IDFornecedor;
	}

	public void setIDFornecedor(long iDFornecedor) {
		IDFornecedor = iDFornecedor;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public short getMultiplo() {
		return Multiplo;
	}

	public void setMultiplo(short multiplo) {
		Multiplo = multiplo;
	}

	public double getDesconto() {
		return Desconto;
	}

	public void setDesconto(double desconto) {
		Desconto = desconto;
	}

}
