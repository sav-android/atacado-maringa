package br.net.sav.modelo;

public class Provedor {
	public short  IdProvedor;
	public String Descricao;
	public String UsuarioLogin;
	public String SenhaLogin;
	public String ServidorPOP;
	public String ServidorSmtp;
	public boolean SmtpReqAutenticacao; 
	public String EmailOrigem;
	public String UsuarioPOP;
	public String SenhaPop;
	public String EmailDestino;
	public String NrConexao;
	public String ParametrosIniciaisModem;
	
	

	public boolean isSmtpReqAutenticacao() {
		return SmtpReqAutenticacao;
	}

	public void setSmtpReqAutenticacao(boolean smtpReqAutenticacao) {
		SmtpReqAutenticacao = smtpReqAutenticacao;
	}

	public short getIdProvedor() {
		return IdProvedor;
	}

	public void setIdProvedor(short idProvedor) {
		IdProvedor = idProvedor;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public String getUsuarioLogin() {
		return UsuarioLogin;
	}

	public void setUsuarioLogin(String usuarioLogin) {
		UsuarioLogin = usuarioLogin;
	}

	public String getSenhaLogin() {
		return SenhaLogin;
	}

	public void setSenhaLogin(String senhaLogin) {
		SenhaLogin = senhaLogin;
	}

	public String getServidorPop() {
		return ServidorPOP;
	}

	public void setServidorPop(String servidorPop) {
		ServidorPOP = servidorPop;
	}

	public String getServidorSmtp() {
		return ServidorSmtp;
	}

	public void setServidorSmtp(String servidorSmtp) {
		ServidorSmtp = servidorSmtp;
	}

	public String getEmailOrigem() {
		return EmailOrigem;
	}

	public void setEmailOrigem(String emailOrigem) {
		EmailOrigem = emailOrigem;
	}

	public String getUsuarioPop() {
		return UsuarioPOP;
	}

	public void setUsuarioPop(String usuarioPop) {
		UsuarioPOP = usuarioPop;
	}

	public String getSenhaPop() {
		return SenhaPop;
	}

	public void setSenhaPop(String senhaPop) {
		SenhaPop = senhaPop;
	}

	public String getEmailDestino() {
		return EmailDestino;
	}

	public void setEmailDestino(String emailDestino) {
		EmailDestino = emailDestino;
	}

	public String getNrConexao() {
		return NrConexao;
	}

	public void setNrConexao(String nrConexao) {
		NrConexao = nrConexao;
	}

	public String getParametrosIniciaisModem() {
		return ParametrosIniciaisModem;
	}

	public void setParametrosIniciaisModem(String parametrosIniciaisModem) {
		ParametrosIniciaisModem = parametrosIniciaisModem;
	}
}
