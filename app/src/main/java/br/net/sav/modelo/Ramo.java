package br.net.sav.modelo;

public class Ramo {
	public short IdRamo;
	public String Descricao;

	public short getIdRamo() {
		return IdRamo;
	}

	public void setIdRamo(short idRamo) {
		IdRamo = idRamo;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

}
