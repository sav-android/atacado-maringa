package br.net.sav.modelo;

import android.database.Cursor;

import br.net.sav.IntegradorWeb.dto.RegraComboDTO;

public class RegraCombo extends br.net.sav.comboControllerSav.ModeloDTO.RegraCombo {
    public RegraCombo(Cursor cursor) {
        super(cursor);
    }
    public RegraCombo(RegraComboDTO regra) {
        super(regra);
    }
}
