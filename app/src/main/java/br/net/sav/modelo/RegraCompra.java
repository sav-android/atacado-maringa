package br.net.sav.modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.ComboController.RegraDeNegocio.ProdutoCamposHelper;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.RegraComboDAO;

import static br.net.sav.atacadomaringa.ComboAdapter.acaoCombo;


public class RegraCompra extends br.net.sav.comboControllerSav.ModeloDTO.RegraCompra{
    public static final String COMPRA = "C";

    public RegraCompra(Cursor cursor) {
        super(cursor);
    }


    @Override
    public float getContagem(List<?> itensPedido, String campo, Excecao excecao) {
        List<ItemDigitacao> itens =(List<ItemDigitacao>) itensPedido;
        ProdutoCamposHelper produtoCamposHelper  = new ProdutoCamposHelper();
        float contagem = ZERO;

        for(ItemDigitacao item: itens){
            long idComparacao = getIdParaFazerAComparacao(campo, item.getProduto(), produtoCamposHelper);

            long quantidadeCaixa = item.verificaQuantidadeEmbalagem();

            if(listIdCampo.contains(idComparacao) && !excecao.ehExcecao(item)) {

                if(idFornecedor>ZERO && idFornecedor==item.getProduto().getIdFornecedor()) {
                    if (quantidadeMinima > ZERO)
                        contagem = contagem + (float) item.getQuantidade() * quantidadeCaixa;
                    if (valorMinimo > ZERO)
                        contagem = (float) (contagem + item.getValorLiquido());
                }else if(idFornecedor==ZERO){
                    if (quantidadeMinima > ZERO) {
                        contagem = contagem + (float) item.getQuantidade() * quantidadeCaixa;
                    }
                    if (valorMinimo > ZERO)
                        contagem = (float) (contagem + item.getValorLiquido());
                }

            }
        }

        if(usaQuantidadeDiferente())
            contagem = contagem + contarQuantidadeDiferente(itensPedido, excecao);

        return contagem;
    }

    @Override
    public float contarQuantidadeDiferente(List<?> itensPedido, Excecao excecao) {
        List<ItemDigitacao> itens =(List<ItemDigitacao>) itensPedido;
        ArrayList<Short> registroNaTabela = new ArrayList<Short>();

        for(ItemDigitacao item: itens) {
            if(excecao.ehExcecao(item))
                continue;

            Short idTabela = new ProdutoCamposHelper().buscarIdCampoPorTabela(tabela,item.getProduto());

            if (item.getProduto().getIdFornecedor() == idFornecedor
                    && !registroNaTabela.contains(idTabela)) {
                registroNaTabela.add(idTabela);
            }
        }

        return registroNaTabela.size();
    }

    public static RegraCompra buscaRegraCompra(Context ctx, long IdCombo ){
        Cursor c = null;
        try {

            SQLiteDatabase database = new DBHelper(ctx).getReadableDatabase();
            String regra = new RegraComboDAO(ctx).buscarRegraComboPorCaracter(IdCombo,COMPRA);
             c = database.rawQuery("select " + regra, null);
            if (c.moveToFirst()) {
                return new RegraCompra(c);
            }
        } finally {
            if(c != null)
                c.close();
        }
        return null;
    }



    public static List<ComboXProduto> buscarProdutosCombo(Context context){
        List<RegraCombo> regraCombos = new RegraComboDAO(context).buscarTodasRegraDeCombo();
        SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
        List<ComboXProduto> comboXProdutos = new ArrayList<>();
        for(RegraCombo regraCombo: regraCombos){
            if(regraCombo.getTipoRegra().toUpperCase().equals(RegraCombo.TIPO_COMPRA)) {
                Cursor cursor = db.rawQuery("select " + regraCombo.getRegra(), null);
                if (cursor.moveToFirst()) {
                    RegraCompra regraCompra = new RegraCompra(cursor);

                    List<Long> listIdCampo = regraCompra.listIdCampo;
                    for (Long idProduto: regraCompra.listIdCampo){
                        ComboXProduto comboXProduto = new ComboXProduto();
                        comboXProduto.setIdCombo(regraCombo.getIdCombo());
                        comboXProduto.setIdProduto(idProduto);
                        comboXProduto.setIdCondicao(regraCompra.getIdCondicao());
                        comboXProdutos.add(comboXProduto);
                    }
                }
            }
        }
        return comboXProdutos;
    }

    public static ComboXProduto buscaComboXProdutoPor(Context ctx, Combo combo){
        List<ComboXProduto> comboXProdutos = buscarProdutosCombo(ctx);
        for (ComboXProduto cxp: comboXProdutos){
            if (cxp.getIdCombo() == combo.getIdCombo()){
                return cxp;
            }
        }

        return null;
    }

    public static boolean mesmaCondicaoPagamentoDoComboAtivado(Context ctx, Combo combo, Combo comboAtivado) {
        List<ComboXProduto> comboXProdutos = RegraCompra.buscarProdutosCombo(ctx);
        RegraCompra compra = RegraCompra.buscaRegraCompra(ctx, comboAtivado.getIdCombo());

        for (ComboXProduto cxp : comboXProdutos){
            if (cxp.getIdCombo() == combo.getIdCombo()){
                if (cxp.getIdCondicao() == 0 ||compra.getIdCondicao() == 0){
                    return true;
                }
                return compra.getIdCondicao() == cxp.getIdCondicao() ;
            }
        }

        return false;
    }
}
