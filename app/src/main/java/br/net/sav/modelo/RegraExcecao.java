package br.net.sav.modelo;

import android.database.Cursor;

import java.io.Serializable;

public class RegraExcecao extends br.net.sav.comboControllerSav.ModeloDTO.RegraExcecao {
    public RegraExcecao(Cursor cursor) {
        super(cursor);
    }

    @Override
    public long getIdParaFazerAComparacao(String campo, Object item) {
        ItemDigitacao itemDigitacao = (ItemDigitacao)item;
        long idComparacao = ZERO;

        if(campo.equals(ID_PRODUTO)) {
            idComparacao = itemDigitacao.getProduto().getIdProduto();
        }else if(campo.equals(ID_GRUPO)){
            idComparacao = itemDigitacao.getProduto().getIdGrupo();
        }else if(campo.equals(ID_FORNECEDOR)){
            idComparacao = itemDigitacao.getProduto().getIdFornecedor();
        }else if(campo.equals(ID_SUBGRUPO)){
            idComparacao = itemDigitacao.getProduto().getIdSubGrupo();
        }else if(campo.equals(ID_CATEGORIA)){
            idComparacao = 0; // itemDigitacao.getProduto().getIdCategoria();
        }
        return idComparacao;
    }
}
