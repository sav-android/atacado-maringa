package br.net.sav.modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.ComboController.RegraDeNegocio.ProdutoCamposHelper;
import br.net.sav.ComboController.RegraDeNegocio.RegraGanheFactory;
import br.net.sav.Utils;
import br.net.sav.comboControllerSav.regraDeNegocioSav.RegraGanheSav.regraGanheDescontoItem.RegraDescontoCombo;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.RegraComboDAO;

import static br.net.sav.atacadomaringa.ComboAdapter.IdComboAtivado;

public class RegraGanhe extends br.net.sav.comboControllerSav.ModeloDTO.RegraGanhe {

    public static final String GANHE ="G";
    public RegraGanhe(Cursor cursor) {
        super(cursor);
    }

    @Override
    public Object gerarItensDeBonus(Context context) throws Exception {
        return null;
    }

    @Override
    public void darDescontoNosItens(int quantidadeItens, Object item) {
        ItemDigitacao item1 = (ItemDigitacao) item;
        RegraDescontoCombo regraDescontoRetornado = new RegraGanheFactory().regraDescontoNoItemPor(tipo);

        regraDescontoRetornado.darDesconto(quantidadeItens, item1, this);

    }

    public static int getQuantidadeItensDoCombo(List<ItemDigitacao> lista, FiltroDeObjeto<ItemDigitacao> filtro){
        Integer quantidade = 0;
        for(ItemDigitacao item: lista){
            if(filtro.filtro(item)){
                quantidade++;
            }
        }
        return quantidade;
    }

    public interface FiltroDeObjeto<T> {

        boolean filtro(T t);
    }

    @Override
    public List<?> RetornarItemComDesconto(Context ctx, List<?> itens) {
        List<ItemDigitacao> itensRecebidos = (List<ItemDigitacao>)itens;
        ArrayList<ItemDigitacao> itensDigitacaoRetornado = new ArrayList<ItemDigitacao>();
        final ProdutoCamposHelper produtoCamposHelper  = new ProdutoCamposHelper();
        RegraCompra compra = RegraCompra.buscaRegraCompra(ctx,IdComboAtivado);

        int quantidadeItens = getQuantidadeItensDoCombo(itensRecebidos, new FiltroDeObjeto<ItemDigitacao>() {
            @Override
            public boolean filtro(ItemDigitacao itemDigitacao) {
                long idComparacao = getIdParaFazerAComparacao(campo, itemDigitacao.getProduto(), produtoCamposHelper);
                return listIdCampo.contains(idComparacao);
            }
        });

        try {
            for (ItemDigitacao itemDigitacao : itensRecebidos) {
                long idComparacao = getIdParaFazerAComparacao(campo, itemDigitacao.getProduto(), produtoCamposHelper);

                if (listIdCampo.contains(idComparacao)) {
                    ItemDigitacao itemDigitacaoClonado = (ItemDigitacao) itemDigitacao.clone();
                    alterarQuantidadeItensLimiteCombo(compra, itemDigitacaoClonado);
                    darDescontoNosItens(quantidadeItens, itemDigitacaoClonado);
                    itensDigitacaoRetornado.add(itemDigitacaoClonado);
                }
            }
        } catch (Exception e) {
            Log.i("Ganhe", "Desconto: " + e.getStackTrace());
        }

        return itensDigitacaoRetornado;
    }

    private void alterarQuantidadeItensLimiteCombo(RegraCompra compra, ItemDigitacao itemDigitacaoClonado) {

//        if (itemDigitacaoClonado.itemComboAtivadoMaiorQueUm() &&
//                itemDigitacaoClonado.getQuantidadeItemLimiteCombo() > compra.getQuantidadeMinima() && listIdCampo.size() == 1) {
//            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() + compra.getQuantidadeMinima());
//        } else if (listIdCampo.size()> 1){
//            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(getQuantdadeItem(itemDigitacaoClonado));
//        }else if (itemDigitacaoClonado.itemComboAtivado() && listIdCampo.size()> 1) {
//            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() +
//                    getQuantdadeItem(itemDigitacaoClonado));
//        } else
        if (itemDigitacaoClonado.getItemComboAtivado() > 0) {
            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() +
                    compra.getQuantidadeMinima());
        } else {
            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(compra.getQuantidadeMinima());
        }
    }

    private double getQuantdadeItem(ItemDigitacao item) {
       double qtdCaixa= item.getProduto().getQuantidadeEmbalagem() == 0? 1:
               item.getProduto().getQuantidadeEmbalagem();
        double v = item.getQuantidade() * qtdCaixa;
        return (item.getQuantidade() * qtdCaixa);
    }

    @Override
    public List<?> RetornarItemSemDesconto(Context ctx, List<?> itens) {
        List<ItemDigitacao> itensRecebidos = (List<ItemDigitacao>)itens;
        ArrayList<ItemDigitacao> itensDigitacaoRetornado = new ArrayList<ItemDigitacao>();
        final ProdutoCamposHelper produtoCamposHelper  = new ProdutoCamposHelper();
        RegraCompra compra = RegraCompra.buscaRegraCompra(ctx, IdComboAtivado);

        int quantidadeItens = getQuantidadeItensDoCombo(itensRecebidos, new FiltroDeObjeto<ItemDigitacao>() {
            @Override
            public boolean filtro(ItemDigitacao itemDigitacao) {
                long idComparacao = getIdParaFazerAComparacao(campo, itemDigitacao.getProduto(), produtoCamposHelper);
                return listIdCampo.contains(idComparacao) && itemDigitacao.getItemComboAtivado() > 0;
            }
        });
        try {
            for (ItemDigitacao itemDigitacao : itensRecebidos) {
                long idComparacao = getIdParaFazerAComparacao(campo, itemDigitacao.getProduto(), produtoCamposHelper);
                if (listIdCampo.contains(idComparacao) && itemDigitacao.getItemComboAtivado() > 0) {
                    ItemDigitacao itemDigitacaoClonado = (ItemDigitacao) itemDigitacao.clone();
                    removerQuantdidadeDeItensLimiteCombo(compra, itemDigitacaoClonado);
                    removerDesconto(quantidadeItens, this, itemDigitacaoClonado);
                    itensDigitacaoRetornado.add(itemDigitacaoClonado);
                }
            }
        } catch (Exception e) {

        }

        return itensDigitacaoRetornado;
    }

    private void removerQuantdidadeDeItensLimiteCombo(RegraCompra compra, ItemDigitacao itemDigitacaoClonado) {
//        if (itemDigitacaoClonado.itemComboAtivadoMaiorQueUm() &&
//                itemDigitacaoClonado.getQuantidadeItemLimiteCombo() > compra.getQuantidadeMinima() && listIdCampo.size() == 1) {
//            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() -
//                    compra.getQuantidadeMinima());
//        } else if (itemDigitacaoClonado.itemComboAtivado() &&
//                itemDigitacaoClonado.getQuantidadeItemLimiteCombo() > compra.getQuantidadeMinima() && listIdCampo.size() > 1) {
//            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() -
//                    getQuantdadeItem(itemDigitacaoClonado));
//       } else
            if (itemDigitacaoClonado.getItemComboAtivado() > 0 /*&&
                itemDigitacaoClonado.getQuantidadeItemLimiteCombo() > compra.getQuantidadeMinima() */) {
            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(itemDigitacaoClonado.getQuantidadeItemLimiteCombo() -
                    compra.getQuantidadeMinima());
        } else {
            itemDigitacaoClonado.setQuantidadeItemLimiteCombo(ZERO);
       }
    }

    @Override
    public void removerDesconto(int quantidadeItens, Object regraGanhe, Object item) {
        float valorModificado = ZERO;
        RegraGanhe ganhe = (RegraGanhe) regraGanhe;
        ItemDigitacao itemRecebido =(ItemDigitacao) item;

        if(ganhe.listIdCampo.contains(itemRecebido.getProduto().getIdProduto())) {
            valorModificado = (float) Utils.arredondar((ganhe.getValor()/quantidadeItens), 2);

            if (itemRecebido.getItemComboAtivado() > 0 && getQuantdadeItem(itemRecebido) > itemRecebido.getQuantidadeItemLimiteCombo()) {
                itemRecebido.setItemComboAtivado(itemRecebido.getItemComboAtivado() - 1);
                itemRecebido.setPercentualDescontoCombo(itemRecebido.getPercentualDescontoCombo() - valorModificado);
                itemRecebido.setDesconto(itemRecebido.getDesconto()- valorModificado);
                itemRecebido.setValorUnit(Utils.arredondar(itemRecebido.getValorUnitOriginal() * (1 - (itemRecebido.getDesconto() / 100)),2));
            }else {

                itemRecebido.setValorUnit(itemRecebido.getValorUnitOriginal());
                itemRecebido.setPercentualDescontoCombo(ZERO);
                itemRecebido.setDesconto(ZERO);
                itemRecebido.setItemComboAtivado(ZERO);
                itemRecebido.setItemComboAtivado(ZERO);

           }
            itemRecebido.setValorBruto(itemRecebido.getValorUnit());
            itemRecebido.setValorLiquido(itemRecebido.getValorUnit());
        }

    }

    public static RegraGanhe buscaRegraGanhe(Context ctx, long IdCombo ){
        SQLiteDatabase database = new DBHelper(ctx).getReadableDatabase();
        String regra = new RegraComboDAO(ctx).buscarRegraComboPorCaracter(IdCombo, GANHE);
        Cursor c = database.rawQuery("select " + regra, null);
        if (c.moveToFirst()) {
            return  new RegraGanhe(c);
        }
        return null;
    }
}
