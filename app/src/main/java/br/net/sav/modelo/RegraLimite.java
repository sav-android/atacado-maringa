package br.net.sav.modelo;

import android.content.Context;
import android.database.Cursor;

import java.io.Serializable;
import java.util.List;

import br.net.sav.comboControllerSav.ModeloDTO.Combo;
import br.net.sav.dao.ComboXPedidoDAO;

public class RegraLimite extends br.net.sav.comboControllerSav.ModeloDTO.RegraLimite {

    public RegraLimite(Cursor cursor) {
        super(cursor);
    }

    @Override
    public boolean satisfazRegraLimite(Combo combo, Object pedido, Context context) {
        Pedido ped = (Pedido) pedido;
        List<ComboXPedido> comboXPedidos = new ComboXPedidoDAO(context).buscarPorIdClienteComboNoMes(ped.getIDCliente(), combo.getIdCombo());
        return comboXPedidos.size()>=quantidade? false: true;
    }

    @Override
    public int quantidadeLimiteRestante(Combo combo, Object pedido, Context context) {
        Pedido ped = (Pedido) pedido;
        List<ComboXPedido> comboXPedidos = new ComboXPedidoDAO(context).buscarPorIdClienteComboNoMes(ped.getIDCliente(),combo.getIdCombo());
        return (int) (quantidade - comboXPedidos.size());
    }
}
