package br.net.sav.modelo;

public class RestricaoTCT {
    private long codigoTipoPedido;
    private long codigoCondicaoPagamento;
    private long codigoFormaPagamento;

    public long getCodigoTipoPedido() {
        return codigoTipoPedido;
    }

    public void setCodigoTipoPedido(long codigoTipoPedido) {
        this.codigoTipoPedido = codigoTipoPedido;
    }

    public long getCodigoCondicaoPagamento() {
        return codigoCondicaoPagamento;
    }

    public void setCodigoCondicaoPagamento(long codigoCondicaoPagamento) {
        this.codigoCondicaoPagamento = codigoCondicaoPagamento;
    }

    public long getCodigoFormaPagamento() {
        return codigoFormaPagamento;
    }

    public void setCodigoFormaPagamento(long codigoFormaPagamento) {
        this.codigoFormaPagamento = codigoFormaPagamento;
    }
}
