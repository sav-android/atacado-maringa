package br.net.sav.modelo;

public class SituacaoEspecial {
	public long IDProduto;
	public long IDFornecedor;
	public String Tipo;
	public long Quantidade1;
	public double DescontoMaximo1;
	public long Quantidade2;
	public double DescontoMaximo2;
	public long Quantidade3;
	public double DescontoMaximo3;
	public long Quantidade4;
	public double DescontoMaximo4;
	public long Quantidade5;
	public double DescontoMaximo5;
	public long Quantidade6;
	public double DescontoMaximo6;
	public long Quantidade7;
	public double DescontoMaximo7;

	public long getIDProduto() {
		return IDProduto;
	}

	public void setIDProduto(long iDProduto) {
		IDProduto = iDProduto;
	}

	public long getIDFornecedor() {
		return IDFornecedor;
	}

	public void setIDFornecedor(long iDFornecedor) {
		IDFornecedor = iDFornecedor;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}

	public long getQuantidade1() {
		return Quantidade1;
	}

	public void setQuantidade1(long quantidade1) {
		Quantidade1 = quantidade1;
	}

	public double getDescontoMaximo1() {
		return DescontoMaximo1;
	}

	public void setDescontoMaximo1(double descontoMaximo1) {
		DescontoMaximo1 = descontoMaximo1;
	}

	public long getQuantidade2() {
		return Quantidade2;
	}

	public void setQuantidade2(long quantidade2) {
		Quantidade2 = quantidade2;
	}

	public double getDescontoMaximo2() {
		return DescontoMaximo2;
	}

	public void setDescontoMaximo2(double descontoMaximo2) {
		DescontoMaximo2 = descontoMaximo2;
	}

	public long getQuantidade3() {
		return Quantidade3;
	}

	public void setQuantidade3(long quantidade3) {
		Quantidade3 = quantidade3;
	}

	public double getDescontoMaximo3() {
		return DescontoMaximo3;
	}

	public void setDescontoMaximo3(double descontoMaximo3) {
		DescontoMaximo3 = descontoMaximo3;
	}

	public long getQuantidade4() {
		return Quantidade4;
	}

	public void setQuantidade4(long quantidade4) {
		Quantidade4 = quantidade4;
	}

	public double getDescontoMaximo4() {
		return DescontoMaximo4;
	}

	public void setDescontoMaximo4(double descontoMaximo4) {
		DescontoMaximo4 = descontoMaximo4;
	}

	public long getQuantidade5() {
		return Quantidade5;
	}

	public void setQuantidade5(long quantidade5) {
		Quantidade5 = quantidade5;
	}

	public double getDescontoMaximo5() {
		return DescontoMaximo5;
	}

	public void setDescontoMaximo5(double descontoMaximo5) {
		DescontoMaximo5 = descontoMaximo5;
	}

	public long getQuantidade6() {
		return Quantidade6;
	}

	public void setQuantidade6(long quantidade6) {
		Quantidade6 = quantidade6;
	}

	public double getDescontoMaximo6() {
		return DescontoMaximo6;
	}

	public void setDescontoMaximo6(double descontoMaximo6) {
		DescontoMaximo6 = descontoMaximo6;
	}

	public long getQuantidade7() {
		return Quantidade7;
	}

	public void setQuantidade7(long quantidade7) {
		Quantidade7 = quantidade7;
	}

	public double getDescontoMaximo7() {
		return DescontoMaximo7;
	}

	public void setDescontoMaximo7(double descontoMaximo7) {
		DescontoMaximo7 = descontoMaximo7;
	}

}
