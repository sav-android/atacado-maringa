package br.net.sav.modelo;

import java.util.Date;

public class Tabela {
	public short IdTabela;
	public short IdEmpresa;
	public short IdFilial;
	public String Descricao;
	public Date DataInicial;
	public Date DataFinal;
	public boolean PermiteDescontoPorTabela;
	public boolean ultilizaRentabilidade;

	public boolean isUltilizaRentabilidade() {
		return ultilizaRentabilidade;
	}

	public void setUltilizaRentabilidade(boolean ultilizaRentabilidade) {
		this.ultilizaRentabilidade = ultilizaRentabilidade;
	}

	public boolean isPermiteDescontoPorTabela() {
		return PermiteDescontoPorTabela;
	}

	public void setPermiteDescontoPorTabela(boolean permiteDescontoPorTabela) {
		PermiteDescontoPorTabela = permiteDescontoPorTabela;
	}

	public Date getDataInicial() {
		return DataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		DataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return DataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		DataFinal = dataFinal;
	}

	public short getIdTabela() {
		return IdTabela;
	}

	public void setIdTabela(short idTabela) {
		IdTabela = idTabela;
	}

	public short getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(short idEmpresa) {
		IdEmpresa = idEmpresa;
	}

	public short getIdFilial() {
		return IdFilial;
	}

	public void setIdFilial(short idFilial) {
		IdFilial = idFilial;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

}
