package br.net.sav.modelo;

public class TabelaClienteNovo {
	public short IdTabela;
	public short IdEmpresa;
	public short IdFilial;

	public short getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(short idEmpresa) {
		IdEmpresa = idEmpresa;
	}

	public short getIdFilial() {
		return IdFilial;
	}

	public void setIdFilial(short idFilial) {
		IdFilial = idFilial;
	}

	public short getIdTabela() {
		return IdTabela;
	}

	public void setIdTabela(short idTabela) {
		IdTabela = idTabela;
	}
}
