package br.net.sav.modelo;

public class TabelaPrecoOperacaoTroca {
	private Tabela tabela;
	private TipoPedido tipoPedido;
	
	public TabelaPrecoOperacaoTroca() {
		tabela = new Tabela();
		tipoPedido = new TipoPedido();
	}

	public Tabela getTabela() {
		return tabela;
	}

	public void setTabela(Tabela tabela) {
		this.tabela = tabela;
	}

	public TipoPedido getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(TipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}
}
