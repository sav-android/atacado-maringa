package br.net.sav.modelo;

import br.net.sav.enumerador.TipoOperacao;

public class TipoPedido {
	public short IdTipoPedido;
	public String Descricao;
	public short AbaterSaldoVerba;
	public double ValorMinimoPedido;
	public String TipoOperacao;

	public String getTipoOperacao() {
		return TipoOperacao;
	}

	public void setTipoOperacao(String tipoOperacao) {
		TipoOperacao = tipoOperacao;
	}

	public double getValorMinimoPedido() {
		return ValorMinimoPedido;
	}

	public void setValorMinimoPedido(double valorMinimoPedido) {
		this.ValorMinimoPedido = valorMinimoPedido;
	}

	public short getIdTipoPedido() {
		return IdTipoPedido;
	}

	public void setIdTipoPedido(short idTipoPedido) {
		IdTipoPedido = idTipoPedido;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public short getAbaterSaldoVerba() {
		return AbaterSaldoVerba;
	}

	public void setAbaterSaldoVerba(short abaterSaldoVerba) {
		AbaterSaldoVerba = abaterSaldoVerba;
	}

	@Override
	public String toString() {
		return String.format("%d - %s", IdTipoPedido, Descricao);
	}
}
