package br.net.sav.modelo;

public class TipoPedidoTipoContribuinte {
	public int CodTipoPedido;
	public int CodTipoContribuinte;

	public int getCodTipoPedido() {
		return CodTipoPedido;
	}

	public void setCodTipoPedido(int codTipoPedido) {
		CodTipoPedido = codTipoPedido;
	}

	public int getCodTipoContribuinte() {
		return CodTipoContribuinte;
	}

	public void setCodTipoContribuinte(int codTipoContribuinte) {
		CodTipoContribuinte = codTipoContribuinte;
	}
}
