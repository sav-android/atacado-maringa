package br.net.sav.modelo;

public class TipoTroca {
	public short IdTipoTroca;
	public String Descricao;

	public short getIdTipoTroca() {
		return IdTipoTroca;
	}

	public void setIdTipoTroca(short idTipoTroca) {
		IdTipoTroca = idTipoTroca;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

}
