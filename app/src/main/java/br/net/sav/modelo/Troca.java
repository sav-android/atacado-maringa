package br.net.sav.modelo;

import java.io.Serializable;
import java.util.Date;

public class Troca implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long IdTroca;
	private long IdCliente;
	private Date Data;
	private short IdTabela;
	private short IdCondicao;
	private short TipoTroca;
	private long NfOrigem;
	private double ValorMercadoriaBruto;
	private double ValorMercadoriaLiquido;
	private double Indenizacao;
	private double ValorLiquidoTroca;
	private short NumeroItens;
	private long NrPedidoVenda;
	private String Observacao;
	private boolean Enviado;
	private Date DataEnvio;
	private double DescontoCliente;

	public long getIdTroca() {
		return IdTroca;
	}

	public void setIdTroca(long idTroca) {
		IdTroca = idTroca;
	}

	public long getIdCliente() {
		return IdCliente;
	}

	public void setIdCliente(long idCliente) {
		IdCliente = idCliente;
	}

	public Date getData() {
		return Data;
	}

	public void setData(Date data) {
		Data = data;
	}

	public short getIdTabela() {
		return IdTabela;
	}

	public void setIdTabela(short idTabela) {
		IdTabela = idTabela;
	}

	public short getIdCondicao() {
		return IdCondicao;
	}

	public void setIdCondicao(short idCondicao) {
		IdCondicao = idCondicao;
	}

	public short getTipoTroca() {
		return TipoTroca;
	}

	public void setTipoTroca(short tipoTroca) {
		TipoTroca = tipoTroca;
	}

	public long getNfOrigem() {
		return NfOrigem;
	}

	public void setNfOrigem(long nfOrigem) {
		NfOrigem = nfOrigem;
	}

	public double getValorMercadoriaBruto() {
		return ValorMercadoriaBruto;
	}

	public void setValorMercadoriaBruto(double valorMarcadoriaBruto) {
		ValorMercadoriaBruto = valorMarcadoriaBruto;
	}

	public double getValorMercadoriaLiquido() {
		return ValorMercadoriaLiquido;
	}

	public void setValorMercadoriaLiquido(double valorMercadoriaLiquido) {
		ValorMercadoriaLiquido = valorMercadoriaLiquido;
	}

	public double getIndenizacao() {
		return Indenizacao;
	}

	public void setIndenizacao(double indenizacao) {
		Indenizacao = indenizacao;
	}

	public double getValorLiquidoTroca() {
		return ValorLiquidoTroca;
	}

	public void setValorLiquidoTroca(double valorLiquidoTroca) {
		ValorLiquidoTroca = valorLiquidoTroca;
	}

	public short getNumeroItens() {
		return NumeroItens;
	}

	public void setNumeroItens(short numeroItens) {
		NumeroItens = numeroItens;
	}

	public long getNrPedidoVenda() {
		return NrPedidoVenda;
	}

	public void setNrPedidoVenda(long nrPedidoVenda) {
		NrPedidoVenda = nrPedidoVenda;
	}

	public String getObservacao() {
		return Observacao;
	}

	public void setObservacao(String observacao) {
		Observacao = observacao;
	}

	public boolean isEnviado() {
		return Enviado;
	}

	public void setEnviado(boolean enviado) {
		Enviado = enviado;
	}

	public Date getDataEnvio() {
		return DataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		DataEnvio = dataEnvio;
	}

	public double getDescontoCliente() {
		return DescontoCliente;
	}

	public void setDescontoCliente(double descontoCliente) {
		DescontoCliente = descontoCliente;
	}

}
