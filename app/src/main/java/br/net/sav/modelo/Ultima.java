package br.net.sav.modelo;

import java.util.Date;

public class Ultima {

	public long IdPedido;
	public long IdPedidoPalmtop;
	public long IdCliente;
	public Date Data;
	public Date DataFaturamento;
	public double TotalUltimaCompra;
	public double Total;
	public String StatusPedido;
	public String NumeroNotaFiscal;

	public long Status;

	// Propriedades Auxiliares
	public String NomeCliente;

	public long getIDPedido() {
		return IdPedido;
	}

	public void setIDPedido(long iDPedido) {
		IdPedido = iDPedido;
	}

	public long getIDPedidoPalmtop() {
		return IdPedidoPalmtop;
	}

	public void setIDPedidoPalmtop(long iDPedidoPalmtop) {
		IdPedidoPalmtop = iDPedidoPalmtop;
	}

	public long getIDCliente() {
		return IdCliente;
	}

	public void setIDCliente(long iDCliente) {
		IdCliente = iDCliente;
	}

	public Date getDataUltimaCompra() {
		return Data;
	}

	public void setDataUltimaCompra(Date dataUltimaCompra) {
		Data = dataUltimaCompra;
	}

	public double getTotal() {
		return Total;
	}

	public void setTotal(double total) {
		Total = total;
	}

	public String getStatusPedido() {
		return StatusPedido;
	}

	public void setStatusPedido(String statusPedido) {
		StatusPedido = statusPedido;
	}

	public String getNumeroNotaFiscal() {
		return NumeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		NumeroNotaFiscal = numeroNotaFiscal;
	}

	public String getNomeCliente() {
		return NomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		NomeCliente = nomeCliente;
	}

	public double getTotalUltimaCompra() {
		return TotalUltimaCompra;
	}

	public void setTotalUltimaCompra(double totalUltimaCompra) {
		TotalUltimaCompra = totalUltimaCompra;
	}

    public Date getDataFaturamento() {
        return DataFaturamento;
    }

    public void setDataFaturamento(Date dataFaturamento) {
        DataFaturamento = dataFaturamento;
    }
}
