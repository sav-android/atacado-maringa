package br.net.sav.modelo;

import java.util.Date;

public class UltimaItem {
	
	public long IdPedido;
	public long IdPedidoPalmtop;
	public long IdProduto;
	public double Qtde;
	public double QtdeCortada;
	public double TotalItem;
	public double DescontoItem;
	public double ValorTotalItemFaturado;
	public double DescontoItemFaturado;
	public String StatusItem;

	public Date Data;
	public long EstoqueDia;

	// Propriedades auxiliares
	public String Descricao;
	public String DescricaoEmbalagem;

	public long getIDPedido() {
		return IdPedido;
	}

	public void setIDPedido(long iDPedido) {
		IdPedido = iDPedido;
	}

	public long getIDProduto() {
		return IdProduto;
	}

	public void setIDProduto(long iDProduto) {
		IdProduto = iDProduto;
	}

	public double getQtde() {
		return Qtde;
	}

	public void setQtde(double qtde) {
		Qtde = qtde;
	}

	public double getQtdeCortada() {
		return QtdeCortada;
	}

	public void setQtdeCortada(double qtdeCortada) {
		QtdeCortada = qtdeCortada;
	}

	public long getEstoqueDia() {
		return EstoqueDia;
	}

	public void setEstoqueDia(long estoqueDia) {
		EstoqueDia = estoqueDia;
	}

	public double getTotalItem() {
		return TotalItem;
	}

	public void setTotalItem(double totalItem) {
		TotalItem = totalItem;
	}

	public Date getData() {
		return Data;
	}

	public void setData(Date data) {
		Data = data;
	}

	public String getDescricao() {
		return Descricao;
	}

	public void setDescricao(String descricao) {
		Descricao = descricao;
	}

	public String getDescricaoEmbalagem() {
		return DescricaoEmbalagem;
	}

	public void setDescricaoEmbalagem(String descricaoEmbalagem) {
		DescricaoEmbalagem = descricaoEmbalagem;
	}

	public long getIdPedidoPalmtop() {
		return IdPedidoPalmtop;
	}

	public void setIdPedidoPalmtop(long idPedidoPalmtop) {
		IdPedidoPalmtop = idPedidoPalmtop;
	}

	public double getDescontoItem() {
		return DescontoItem;
	}

	public void setDescontoItem(double descontoItem) {
		DescontoItem = descontoItem;
	}

	public double getValorTotalItemFaturado() {
		return ValorTotalItemFaturado;
	}

	public void setValorTotalItemFaturado(double valorTotalItemFaturado) {
		ValorTotalItemFaturado = valorTotalItemFaturado;
	}

	public double getDescontoItemFaturado() {
		return DescontoItemFaturado;
	}

	public void setDescontoItemFaturado(double descontoItemFaturado) {
		DescontoItemFaturado = descontoItemFaturado;
	}

	public String getStatusItem() {
		return StatusItem;
	}

	public void setStatusItem(String statusItem) {
		StatusItem = statusItem;
	}
}
