package br.net.sav.modelo;

public class Usuario {

	public short IdUsuario;
	public String Nome;
	public short IdVendedor;
	public String Senha;

	public short getIDUsuario() {
		return IdUsuario;
	}

	public void setIDUsuario(short iDUsuario) {
		IdUsuario = iDUsuario;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public short getIDVendedor() {
		return IdVendedor;
	}

	public void setIDVendedor(short iDVendedor) {
		IdVendedor = iDVendedor;
	}

	public String getSenha() {
		return Senha;
	}

	public void setSenha(String senha) {
		Senha = senha;
	}
}
