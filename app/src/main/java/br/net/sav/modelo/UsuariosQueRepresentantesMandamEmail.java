package br.net.sav.modelo;

import java.io.Serializable;

public class UsuariosQueRepresentantesMandamEmail implements Serializable{
	public static final long serialVersionUID = 1L;
	
    public int IdUsuario;
    public String Nome;
    public String Email;    
    
	public int getIDUsuario() {
		return IdUsuario;
	}
	public void setIDUsuario(int iDUsuario) {
		IdUsuario = iDUsuario;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String Nome) {
		this.Nome = Nome;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}	
}
