package br.net.sav.modelo;

public class VendaMesFornecedor {
	public String Fornecedor;
	public double Vendas;
	public double Tonelagem;

	public double PercVendas;
	public double PercTonelagem;

	public String getFornecedor() {
		return Fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		Fornecedor = fornecedor;
	}

	public double getVendas() {
		return Vendas;
	}

	public void setVendas(double vendas) {
		Vendas = vendas;
	}

	public double getTonelagem() {
		return Tonelagem;
	}

	public void setTonelagem(double tonelagem) {
		Tonelagem = tonelagem;
	}

	public double getPercVendas() {
		return PercVendas;
	}

	public void setPercVendas(double percVendas) {
		PercVendas = percVendas;
	}

	public double getPercTonelagem() {
		return PercTonelagem;
	}

	public void setPercTonelagem(double percTonelagem) {
		PercTonelagem = percTonelagem;
	}

}
