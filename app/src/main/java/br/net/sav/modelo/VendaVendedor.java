package br.net.sav.modelo;

public class VendaVendedor {
	public String Periodo;
	public double TotalVendas;
	public double PercentualTotalVendas;
	public double TotalTonelagem;
	public double PercentualTotalTonelagem;

	public String getPeriodo() {
		return Periodo;
	}

	public void setPeriodo(String periodo) {
		Periodo = periodo;
	}

	public double getTotalVendas() {
		return TotalVendas;
	}

	public void setTotalVendas(double totalVendas) {
		TotalVendas = totalVendas;
	}

	public double getPercentualTotalVendas() {
		return PercentualTotalVendas;
	}

	public void setPercentualTotalVendas(double percentualTotalVendas) {
		PercentualTotalVendas = percentualTotalVendas;
	}

	public double getTotalTonelagem() {
		return TotalTonelagem;
	}

	public void setTotalTonelagem(double totalTonelagem) {
		TotalTonelagem = totalTonelagem;
	}

	public double getPercentualTotalTonelagem() {
		return PercentualTotalTonelagem;
	}

	public void setPercentualTotalTonelagem(double percentualTotalTonelagem) {
		PercentualTotalTonelagem = percentualTotalTonelagem;
	}

}
