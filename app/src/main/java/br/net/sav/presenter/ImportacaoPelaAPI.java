package br.net.sav.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.IntegradorWeb.importacao.RecebimentoDadosApi;
import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.dao.AdicionaisDAO;
import br.net.sav.dao.AliquotaIcmsDAO;
import br.net.sav.dao.CidadeDAO;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComissaoDAO;
import br.net.sav.dao.ComissaoVDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.CondicaoPagamentoClienteDAO;
import br.net.sav.dao.CondicaoProdutoDAO;
import br.net.sav.dao.ContatoDAO;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.EmpresaFilialDAO;
import br.net.sav.dao.EnderecoCobrancaDAO;
import br.net.sav.dao.FormaPagamentoClienteDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.FornecedorDAO;
import br.net.sav.dao.GrupoProdutoDAO;
import br.net.sav.dao.LimiteDAO;
import br.net.sav.dao.MarcaDAO;
import br.net.sav.dao.NaoVendaDAO;
import br.net.sav.dao.NotificacaoDao;
import br.net.sav.dao.ObservacaoClienteDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PendenciaDAO;
import br.net.sav.dao.PlanoVisitaDAO;
import br.net.sav.dao.PrecoDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.dao.RamoDAO;
import br.net.sav.dao.RestricaoTCTDAO;
import br.net.sav.dao.SituacaoEspecialDAO;
import br.net.sav.dao.TabelaClienteDAO;
import br.net.sav.dao.TabelaClienteNovoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TabelaPrecoOperacaoTrocaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.dao.TipoPedidoTipoContribuinteDAO;
import br.net.sav.dao.TipoTrocaDAO;
import br.net.sav.dao.UltimaDAO;
import br.net.sav.dao.UltimaItemDAO;
import br.net.sav.dao.UsuarioDAO;
import br.net.sav.dao.VendaMesFornecedorDAO;
import br.net.sav.dao.VendaVendedorDAO;
import br.net.sav.utils.VerificaFotosUtils;

import static br.net.sav.AtualizarActivity.MENSAGEM;
import static br.net.sav.IntegradorWeb.importacao.RecebimentoDadosApi.getRunOnUpdateView;
import static java.lang.Thread.sleep;

public class ImportacaoPelaAPI {

    private ImportacaoPorLinhaTexto.IAoImportaDadoPresenter presenterImporta;
    private Activity context;
    private SQLiteDatabase db;
    private PowerManager.WakeLock watever;

    public ImportacaoPelaAPI(Activity context, ImportacaoPorLinhaTexto.IAoImportaDadoPresenter presenterImporta) {
        this.presenterImporta = presenterImporta;
        this.context = context;
        db = new DBHelper(context).getWritableDatabase();
    }


    public void getIniciarImportacao() {
        new RecebimentoDadosApi(context, getListaAcoesParaImportacao(), getIntanceAtualizaMsgView())
               .executar();
    }


    public void getVerificarFotosFtp() {
        new VerificaFotosUtils(context, getIntanceAtualizaMsgView()).verificacaoFotos();
    }


    private List<AcaoParaImportacaoDados> getListaAcoesParaImportacao() {

        List<AcaoParaImportacaoDados> acoes = new ArrayList<>();
        acoes.add(new ParametroDAO(context, db));
        acoes.add(new EmpresaFilialDAO(context, db));
        acoes.add(new ClienteDAO(context, db));
        acoes.add(new TabelaDAO(context, db));
        acoes.add(new ProdutoDAO(context, db));
        acoes.add(new PrecoDAO(context, db));
        acoes.add(new CondicaoDAO(context, db));
        acoes.add(new ContatoDAO(context, db));
        acoes.add(new CidadeDAO(context, db));
        acoes.add(new PendenciaDAO(context, db));
        acoes.add(new TabelaClienteDAO(context, db));
        acoes.add(new TabelaClienteNovoDAO(context, db));
        acoes.add(new CondicaoPagamentoClienteDAO(context));
        acoes.add(new TipoPedidoDAO(context, db));
        acoes.add(new FormaPagamentoDAO(context, db));
        acoes.add(new FormaPagamentoClienteDAO(context));
        acoes.add(new AdicionaisDAO(context, db));
        acoes.add(new EnderecoCobrancaDAO(context, db));
        acoes.add(new FornecedorDAO(context, db));
        acoes.add(new MarcaDAO(context, db));
        acoes.add(new GrupoProdutoDAO(context));
        acoes.add(new ObservacaoClienteDAO(context, db));
        acoes.add(new NotificacaoDao(context, db));
        acoes.add(new RamoDAO(context, db));
        acoes.add(new AliquotaIcmsDAO(context, db));
        acoes.add(new UsuarioDAO(context, db));
        acoes.add(new LimiteDAO(context, db));
        acoes.add(new SituacaoEspecialDAO(context, db));
        acoes.add(new TipoTrocaDAO(context, db));
        acoes.add(new PlanoVisitaDAO(context, db));
        acoes.add(new VendaVendedorDAO(context, db));
        acoes.add(new VendaMesFornecedorDAO(context, db));
        acoes.add(new CondicaoProdutoDAO(context, db));
        acoes.add(new ComissaoDAO(context));
        acoes.add(new ComissaoVDAO(context, db));
        acoes.add(new TabelaPrecoOperacaoTrocaDAO(context, db));
        acoes.add(new NaoVendaDAO(context, db));
        acoes.add(new TipoPedidoTipoContribuinteDAO(context, db));
        acoes.add(new RestricaoTCTDAO(context, db));
        acoes.add(new UltimaDAO(context, db));
        acoes.add(new UltimaItemDAO(context, db));

        return acoes;
    }

    private void atualizarMensagemTela(String mensagem) {
        Message msg = new Message();
        Bundle b = new Bundle();

        msg.what = MENSAGEM;
        b.putString("mensagem", mensagem);
        msg.setData(b);
        presenterImporta.getHandler().sendMessage(msg);
    }

    @SuppressLint("InvalidWakeLockTag")
    public void getManterTelaAtiva() {
        //mante a tela ligada
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //Mantem a CPU do dispositivo ligada, para concluir algum trabalho antes que o dispositivo entre no modo de suspensão.
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        watever = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "watever");
        watever.acquire();
    }

    private IAtualizaMensagemProgress getIntanceAtualizaMsgView() {
        return new IAtualizaMensagemProgress() {
            @Override
            public void atualizarProgresso(String mensagem) {
                getMensagemProgress(mensagem);

            }
        };
    }

    private void getMensagemProgress(final String mensagem) {
        getRunOnUpdateView(context,new RecebimentoDadosApi.IUIThreadAtualiza() {
            @Override
            public void start() {
                atualizarMensagemTela(mensagem);
            }
        });
    }


}
