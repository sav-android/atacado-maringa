package br.net.sav.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.AdicionaisDAO;
import br.net.sav.dao.AliquotaIcmsDAO;
import br.net.sav.dao.CidadeDAO;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.ComissaoDAO;
import br.net.sav.dao.ComissaoVDAO;
import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.CondicaoPagamentoClienteDAO;
import br.net.sav.dao.CondicaoProdutoDAO;
import br.net.sav.dao.ContatoDAO;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.DescontoFDAO;
import br.net.sav.dao.DescontoSDAO;
import br.net.sav.dao.EmpresaFilialDAO;
import br.net.sav.dao.EnderecoCobrancaDAO;
import br.net.sav.dao.FormaPagamentoClienteDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.FornecedorDAO;
import br.net.sav.dao.GorduraFDAO;
import br.net.sav.dao.GorduraSDAO;
import br.net.sav.dao.GrupoProdutoDAO;
import br.net.sav.dao.LimiteDAO;
import br.net.sav.dao.MarcaDAO;
import br.net.sav.dao.NaoVendaDAO;
import br.net.sav.dao.NotificacaoDao;
import br.net.sav.dao.ObjetivoDAO;
import br.net.sav.dao.ObservacaoClienteDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.PendenciaDAO;
import br.net.sav.dao.PlanoVisitaDAO;
import br.net.sav.dao.PrecoDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.dao.RamoDAO;
import br.net.sav.dao.RestricaoTCTDAO;
import br.net.sav.dao.SituacaoEspecialDAO;
import br.net.sav.dao.TabelaClienteDAO;
import br.net.sav.dao.TabelaClienteNovoDAO;
import br.net.sav.dao.TabelaDAO;
import br.net.sav.dao.TabelaPrecoOperacaoTrocaDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.dao.TipoPedidoTipoContribuinteDAO;
import br.net.sav.dao.TipoTrocaDAO;
import br.net.sav.dao.UltimaDAO;
import br.net.sav.dao.UltimaItemDAO;
import br.net.sav.dao.UsuarioDAO;
import br.net.sav.dao.VendaMesFornecedorDAO;
import br.net.sav.dao.VendaVendedorDAO;
import br.net.sav.util.StringUtils;
import br.net.sav.util.dao.UsuarioEmailDAO;
import br.net.sav.utils.ConfiguracaoFtpActivityUtils;
import br.net.sav.utils.VerificaFotosUtils;

import static android.app.Activity.RESULT_OK;
import static br.net.sav.AtualizarActivity.MENSAGEM;
import static br.net.sav.atacadomaringa.TransmissaoActivity.verificaAtualizacaoDados;

public class ImportacaoPorLinhaTexto {

    private IAoImportaDadoPresenter presenterImporta;
    private Activity context;
    private SQLiteDatabase db;
    private PowerManager.WakeLock watever;

    public ImportacaoPorLinhaTexto(Activity context, IAoImportaDadoPresenter presenterImporta) {
        this.presenterImporta = presenterImporta;
        this.context = context;
    }


    public void lerTextos() {

        try {
            try {
                BufferedReader br = null;
                FileInputStream fis = null;
                String linha = "";
                List<String> linhas = null;

                String caminhoArquivos = Environment.getExternalStorageDirectory().getPath() + "/sav/recebe";

                File dir = new File(caminhoArquivos);
                if (!dir.exists())
                    return;

                // Importar primeiro o arquivo de par�metros se existir
                FileFilter ff = new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.getName().toUpperCase().startsWith("PAR");
                    }
                };
                File[] arquivosAux = dir.listFiles(ff);
                if (arquivosAux != null) {
                    for (File f : arquivosAux) {
                        db = new DBHelper(context).getWritableDatabase();
                        try {
                            if (f.getName().toUpperCase().startsWith("PAR")) {
                                linhas = new ArrayList<String>();
                                fis = new FileInputStream(f);
                                br = new BufferedReader(new InputStreamReader(fis, "iso-8859-1"));

                                while ((linha = br.readLine()) != null)
                                    linhas.add(linha);

                                atualizarMensagemTela(context.getString(R.string.importando_arquivo_de_parametros));
                                new ParametroDAO(context, db).importar(linhas);
                                Utils.apagarArquivo(f.getPath());
                            }
                        } finally {
                            //if (db.isOpen())
                            //db.close(); singleton();
                        }
                    }
                }

                // Importar outros arquivos prim�rios AP�S o par�metro
                ff = new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.getName().toUpperCase().startsWith("TAB") || f.getName().toUpperCase().startsWith("ATA") || f.getName().toUpperCase().startsWith("PAR");
                    }
                };
                arquivosAux = dir.listFiles(ff);
                if (arquivosAux != null) {
                    for (File f : arquivosAux) {
                        db = new DBHelper(context.getBaseContext()).getWritableDatabase();
                        try {
                            String nomeArquivo = f.getName().toUpperCase();

                            if (nomeArquivo.startsWith("PAR")) {
                                linhas = new ArrayList<String>();
                                fis = new FileInputStream(f);
                                br = new BufferedReader(new InputStreamReader(fis, "iso-8859-1"));

                                while ((linha = br.readLine()) != null)
                                    linhas.add(linha);

                                atualizarMensagemTela(context.getString(R.string.importando_arquivo_de_parametros)); //adicionar cliente para importar primeiro
                                new ParametroDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(f.getPath());
                            } else if (nomeArquivo.startsWith("TAB") || nomeArquivo.startsWith("ATA")) {
                                linhas = new ArrayList<String>();
                                fis = new FileInputStream(f);
                                br = new BufferedReader(new InputStreamReader(fis, "iso-8859-1"));

                                while ((linha = br.readLine()) != null)
                                    linhas.add(linha);

                                atualizarMensagemTela(context.getString(R.string.importando_tabela_de_precos));
                                new TabelaDAO(context.getBaseContext(), db).importar(linhas, f.getName().substring(0, 3).toUpperCase().equals("TAB"));
                                //Utils.apagarArquivo(f.getPath());
                            }
                        } finally {
                            //					if (db.isOpen())
                            //db.close(); singleton();
                        }
                    }
                }

                ff = new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.getName().toUpperCase().startsWith("PRO") ||
                                f.getName().toUpperCase().startsWith("CLI") ||
                                f.getName().toUpperCase().startsWith("ACL") ||
                                f.getName().toUpperCase().startsWith("APR");
                    }
                };
                arquivosAux = dir.listFiles(ff);
                if (arquivosAux != null) {
                    for (File f : arquivosAux) {
                        db = new DBHelper(context.getBaseContext()).getWritableDatabase();
                        try {
                            String nomeArquivo = f.getName().toUpperCase();

                            if (nomeArquivo.startsWith("PRO") || nomeArquivo.startsWith("APR")) {
                                linhas = new ArrayList<String>();
                                fis = new FileInputStream(f);
                                br = new BufferedReader(new InputStreamReader(fis, "iso-8859-1"));

                                while ((linha = br.readLine()) != null)
                                    linhas.add(linha);

                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_produto));
                                new ProdutoDAO(context.getBaseContext(), db).importar(linhas, nomeArquivo.startsWith("PRO"));
                                Utils.apagarArquivo(f.getPath());
                            } else if (nomeArquivo.startsWith("CLI") || nomeArquivo.startsWith("ACL")) {
                                linhas = new ArrayList<String>();
                                fis = new FileInputStream(f);
                                br = new BufferedReader(new InputStreamReader(fis, "iso-8859-1"));

                                while ((linha = br.readLine()) != null)
                                    linhas.add(linha);

                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_clientes));
                                new ClienteDAO(context.getBaseContext(), db).importar(linhas, nomeArquivo.startsWith("CLI"));
                                //Utils.apagarArquivo(f.getPath());
                            }
                        } finally {
                            //	if (db.isOpen())
                            //db.close(); singleton();
                        }
                    }
                }

                // Importar os outros arquivos
                File[] arquivos = dir.listFiles();

                for (File arquivo : arquivos) {
                    db = new DBHelper(context.getApplicationContext()).getWritableDatabase();
                    try {
                        if (arquivo.isFile()) {
                            String iniciais = arquivo.getName().substring(0, 3).toUpperCase();
                            String nomeArquivo = arquivo.getName().toUpperCase(Locale.getDefault());

                            if (nomeArquivo.endsWith("RES")) {
                                atualizarMensagemTela(context.getString(R.string.importando_relatorios_da_empresa));

                                Utils.copiarArquivo(arquivo.getPath(), Environment.getExternalStorageDirectory().getPath() + "/sav/resumo/" + arquivo.getName());
                                Utils.apagarArquivo(arquivo.getPath());
                                continue;
                            }
                            if (nomeArquivo.endsWith("NOT")) {
                                atualizarMensagemTela("Importando detalhes da notificação.");

                                Utils.copiarArquivo(arquivo.getPath(), Environment.getExternalStorageDirectory().getPath() + "/sav/notificacao_detalhes/" + arquivo.getName());
                                Utils.apagarArquivo(arquivo.getPath());
                                continue;
                            }
                            if (nomeArquivo.endsWith("JPG") || nomeArquivo.endsWith("JPEG")) {
                                atualizarMensagemTela("Importando Imagens de Produtos");

                                Utils.copiarArquivo(arquivo.getPath().toLowerCase(), Environment.getExternalStorageDirectory().getPath() + "/sav/fotos/" + arquivo.getName());
                                Utils.apagarArquivo(arquivo.getPath());
                                continue;
                            }
                            if (nomeArquivo.equalsIgnoreCase("config.properties")) {
                                atualizarMensagemTela(context.getString(R.string.importando_configuracoes_do_aplicativo));

                                ConfiguracaoFtpActivityUtils.carregarConfiguracoesArquivoProperties(context.getBaseContext(), arquivo.getPath());
                                Utils.apagarArquivo(arquivo.getPath());
                                continue;
                            }

                            linhas = new ArrayList<String>();
                            fis = new FileInputStream(arquivo);
                            br = new BufferedReader(new InputStreamReader(fis, StringUtils.CHARSET_ISO_8859_1));

                            while ((linha = br.readLine()) != null) {
                                linhas.add(linha);
                            }
                            br.close();

                            if (arquivo.getName().toUpperCase().startsWith("SCRIPT") && arquivo.getName().toUpperCase().endsWith("SQL")) {
                                for (String linhaScript : linhas) {
                                    try {
                                        db.execSQL(linhaScript);
                                    } catch (Exception e) {
                                        Log.e(getClass().getSimpleName(), e.getMessage());
                                    }
                                }
                                Utils.apagarArquivo(arquivo.getPath());
                                continue;
                            }

//							if(iniciais.equalsIgnoreCase("TOK")) {
//								atualizarMensagemTela("Importando Token API");
//								new ParametroDAO(getBaseContext(), db).importarToken(linhas);
//								Utils.apagarArquivo(arquivo.getPath());
//							}else
                            if (iniciais.equals("CON")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_contato));
                                new ContatoDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (nomeArquivo.startsWith("CLI") || nomeArquivo.startsWith("ACL")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_clientes));
                                new ClienteDAO(context.getBaseContext(), db).importar(linhas, nomeArquivo.startsWith("CLI"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (nomeArquivo.startsWith("TAB") || nomeArquivo.startsWith("ATA")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_de_precos));
                                new TabelaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("TAB"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("ADI")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_dados_adicionais));
                                new AdicionaisDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("ADI"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("END")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_endereco_cobranca));
                                new EnderecoCobrancaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("END"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("FOR") || iniciais.equals("AFO")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_fornecedor));
                                new FornecedorDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("FOR"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("GRP")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_grupo_produto));
                                new GrupoProdutoDAO(context.getBaseContext()).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("OBJ") || iniciais.equals("ATR")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_obj_meta));
                                new ObjetivoDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("OBS") || iniciais.equals("AOB")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_observacoes_cliente_bloqueado));
                                new ObservacaoClienteDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("OBS"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("PAR")) {
                                atualizarMensagemTela(context.getString(R.string.importando_arquivo_de_parametros));
                                new ParametroDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("PEN") || iniciais.equals("APN")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_pendencias_financeiras));
                                new PendenciaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("PEN"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("NOT")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_Notificacao));
                                new NotificacaoDao(context.getBaseContext(), db).importar(linhas, iniciais.equals("NOT"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("PLV")) {
                                atualizarMensagemTela("Importando tabela plano de visitas");
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.startsWith("PRE") || iniciais.startsWith("APE")) {
                                atualizarMensagemTela(context.getString(R.string.importando_precos_dos_produtos));
                                new PrecoDAO(context.getBaseContext(), db).importar(linhas, iniciais.startsWith("PRE"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("RAM") || iniciais.equals("ARA")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_ramo_atividade));
                                new RamoDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("RAM"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TBC") || iniciais.equals("ATC")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_precos_por_cliente));
                                new TabelaClienteDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("TBC"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TBN")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_precos_para_clientes_novos));
                                new TabelaClienteNovoDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TPE") || iniciais.equals("ATP")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tipo_pedido));
                                new TipoPedidoDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("TPE"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("ICM")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_tributacao_icms));
                                new AliquotaIcmsDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("PUL")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_ultimas_compras));
                                new UltimaDAO(context, db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("ULT")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_itens_ultimas_compras));
                                new UltimaItemDAO(context, db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("VAL")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_validade_tabela_de_preco));
                                new TabelaDAO(context.getBaseContext(), db).importarVAL(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("USU")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_de_usuarios));
                                new UsuarioDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("USU"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("LIM") || iniciais.equals("ALI")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_limite_credito));
                                new LimiteDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("LIM"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("EMP")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_empresa));
                                new EmpresaFilialDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("CPG") || iniciais.equals("ACP")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_condicoes_pagamento));
                                new CondicaoDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("CPG"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("GOF")) {
                                atualizarMensagemTela("Importando tabela gordura fornecedor");
                                new GorduraFDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("GOS")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_gordura_por_secao));
                                new GorduraSDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("DEF")) {
                                atualizarMensagemTela("Importando tabela desconto fornecedor");
                                new DescontoFDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("DES")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_desconto_por_secao));
                                new DescontoSDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("ESP")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_situacao_especial));
                                new SituacaoEspecialDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TTR") || iniciais.equals("ATT")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tipo_troca));
                                new TipoTrocaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("TTR"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("QDI") || iniciais.equals("AQD")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_plano_visita));
                                new PlanoVisitaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("QDI"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("UEM") || iniciais.equals("AUE")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_premissoes_email));
                                new UsuarioEmailDAO(context.getBaseContext()).importar(linhas, iniciais.equals("UEM"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("VDA")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_vendas_mes));
                                new VendaVendedorDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("VDF")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_venda_mes_fornecedor));
                                new VendaMesFornecedorDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("CPP")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_condicao_produto));
                                new CondicaoProdutoDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("CID")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_cidade));
                                new CidadeDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("FPG")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_forma_pg));
                                new FormaPagamentoDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("MAR") || iniciais.equals("AMA")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_marca));
                                new MarcaDAO(context.getBaseContext(), db).importar(linhas, iniciais.equals("MAR"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("RET") || iniciais.equalsIgnoreCase(context.getString(R.string.ret))) {
                                atualizarMensagemTela(context.getString(R.string.importando_retorno_de_pedido));
                                new PedidoDAO(context.getBaseContext(), db).confirmarPedidoRetornado(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("PDE")) {
                                atualizarMensagemTela("Importando tabela confirmar pedidos");
                                new PedidoDAO(context.getBaseContext(), db).confirmarPedido(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (arquivo.getName().toUpperCase().endsWith(".MSG")) {
                                atualizarMensagemTela("Importando Mensagens");
                                new br.net.sav.util.dao.EmailDAO(context.getBaseContext()).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("COV")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_comissao_v));
                                new ComissaoVDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("COM")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_comissao));
                                new ComissaoDAO(context.getBaseContext()).importar(linhas, iniciais.equals("COM"));
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("CCL") || iniciais.equals("ACC")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_condicao_pagamento_cliente));
                                new CondicaoPagamentoClienteDAO(context.getBaseContext()).importar(linhas, iniciais.equals("CCL"), db);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("FCL") || iniciais.equals("AFC")) {
                                atualizarMensagemTela(context.getString(R.string.importacao_tabela_f_p_cliente));
                                new FormaPagamentoClienteDAO(context.getBaseContext()).importar(linhas, iniciais.equals("FCL"), db);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TPO")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_preco_operacao));
                                new TabelaPrecoOperacaoTrocaDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("NVE")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_motivo_nao_venda));
                                new NaoVendaDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TTC")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_restricoes_tipo_pedido_tipo_contribuintes));
                                new TipoPedidoTipoContribuinteDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            } else if (iniciais.equals("TCT")) {
                                atualizarMensagemTela(context.getString(R.string.importando_tabela_restricao_tipo_pedido_condicao_pagamento_forma_pagamento));
                                new RestricaoTCTDAO(context.getBaseContext(), db).importar(linhas);
                                Utils.apagarArquivo(arquivo.getPath());
                            }
                        }
                    } finally {
                        //if (db.isOpen())
                        //db.close(); singleton();
                    }
                }


                if (context.getIntent().getExtras() != null) {
                    Utils.apagarArquivosDiretorio(context.getIntent().getStringExtra("path"));
                    context.setResult(RESULT_OK);
                }

                Utils.apagarArquivosDiretorio(caminhoArquivos);
                verificaAtualizacaoDados = false;
            } catch (Exception e) {
                Log.d("ImportacaoActivity", e.getMessage());
            }
        } catch (Exception e) {
            Log.d("ImportacaoActivity", e.getMessage());
        } finally {
            //		if (db.isOpen())
            //db.close(); singleton();
            verificaAtualizacaoDados = false;
        }
    }

    private void atualizarMensagemTela(String mensagem) {
        Message msg = new Message();
        Bundle b = new Bundle();

        msg.what = MENSAGEM;
        b.putString("mensagem", mensagem);
        msg.setData(b);
        presenterImporta.getHandler().sendMessage(msg);
    }

    @SuppressLint("InvalidWakeLockTag")
    public void getManterTelaAtiva() {
        //mante a tela ligada
        context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //Mantem a CPU do dispositivo ligada, para concluir algum trabalho antes que o dispositivo entre no modo de suspensão.
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        watever = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "watever");
        watever.acquire();
    }

    public interface IAoImportaDadoPresenter {
        Handler getHandler();
    }

    public void getVerificarFotosFtp(){
        new VerificaFotosUtils(context, new IAtualizaMensagemProgress() {
            @Override
            public void atualizarProgresso(String mensagem) {
                getMensagemProgress(mensagem);
            }
        }).verificacaoFotos();
    }

    private void getMensagemProgress(final String mensagem) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                atualizarMensagemTela(mensagem);
            }
        });
    }


}
