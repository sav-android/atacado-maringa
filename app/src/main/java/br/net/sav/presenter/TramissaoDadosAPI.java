package br.net.sav.presenter;

import android.app.Activity;
import android.graphics.Color;
import android.os.Message;

import java.security.NoSuchProviderException;

import javax.mail.MessagingException;

import br.net.sav.ComboController.service.ComboCMService;
import br.net.sav.atacadomaringa.R;
import br.net.sav.central_manager.SincronizacaoService;
import br.net.sav.central_manager.SincronizacaoServiceCliente;
import br.net.sav.dao.GeracaoTextoDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.presenter.TransmissaoDadosPresente.IAoTransmitirDadosPresente;

import static br.net.sav.IntegradorWeb.services.CentralManagerService.isSucessoAPI;
import static br.net.sav.IntegradorWeb.services.CentralManagerService.setSucessoAPI;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ATUALIZA_COMBOS_CM;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ENVIANDO_DADOS_CLIENTES_GWEB;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ENVIANDO_DADOS_PEDIDOS_GWEB;
import static br.net.sav.atacadomaringa.TransmissaoActivity.verificaAtualizacaoDados;

public class TramissaoDadosAPI {

    private Parametro mParametro;
    private Activity ctx;
    private GeracaoTextoDAO mGeracaoTextoDAO;
    private IAoTransmitirDadosPresente presenterDados;
    private int mQuantidadePeidosEnviados = 0;

    public TramissaoDadosAPI(Activity ctx, Parametro mParametro, GeracaoTextoDAO mGeracaoTextoDAO, IAoTransmitirDadosPresente presenterDados) {
        this.mParametro = mParametro;
        this.ctx = ctx;
        this.mGeracaoTextoDAO = mGeracaoTextoDAO;
        this.presenterDados = presenterDados;
    }

    public void getStartTramissaoAPI() throws NoSuchProviderException, MessagingException {
        enviaDadosPedidosGWeb();
        enviaDadosClientesGWeb();
        atualizaCombosCM();
    }

    public void enviaDadosPedidosGWeb() {
        presenterDados.atualizarProgresso("Enviando Pedidos Gweb");
        Message msg = new Message();
        msg.what = ENVIANDO_DADOS_PEDIDOS_GWEB;
        presenterDados.getHandler().sendMessage(msg);
        getEnviarDadosPedido();
    }

    public void enviaDadosClientesGWeb() {
        presenterDados.atualizarProgresso("Enviando Cliente Gweb");
        Message msg = new Message();
        msg.what = ENVIANDO_DADOS_CLIENTES_GWEB;
        presenterDados.getHandler().sendMessage(msg);
        getEnviarDadosClientes();
    }

    public void getEnviarDadosPedido() {
        try {
            if (mParametro != null)
                if (mParametro.isAtivo())
                    new SincronizacaoService()
                            .iniciarSincronizacao(ctx);
            presenterDados.atualizarProgresso("Enviando Dados Gweb Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Enviando Dados Gweb Erro: " + e.getMessage());
        }
    }

    public void getEnviarDadosClientes() {
        try {
            if (mParametro != null)
                if (mParametro.isAtivo())
                    new SincronizacaoServiceCliente()
                            .iniciarSincronizacao(ctx);
            presenterDados.atualizarProgresso("Enviando Dados Gweb Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Enviando Dados Gweb Erro: " + e.getMessage());
        }
    }

    private void atualizaCombosCM() {
        presenterDados.atualizarProgresso("Atualizando Combos CM");
        try {
            Message msg = new Message();
            msg.what = ATUALIZA_COMBOS_CM;
            presenterDados.getHandler().sendMessage(msg);
            if (!verificaAtualizacaoDados)
                new ComboCMService()
                        .iniciarSincronizacao(ctx);
            presenterDados.atualizarProgresso("Atualizando Combos CM Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Atualizando Combos CM Erro: " + e.getMessage());
        }
    }

    public void getStatusViewSucessoOuFalhaAPI() {
        presenterDados.getTextViewStatus().setTextColor(isSucessoAPI() ? Color.CYAN : Color.RED);
        presenterDados.getTextViewStatus().setText(isSucessoAPI()  ? String.format(ctx.getString(R.string.processo_concluido_com_sucesso_pedidos_enviados),
                quantidadesDePedidosEnviados(isSucessoAPI()))
                : ctx.getString(R.string.falha_no_processo_de_transmissao));
    }

    public int quantidadesDePedidosEnviados(Boolean sucesso) {
        return !sucesso ? 0 : mQuantidadePeidosEnviados;
    }

    public boolean isEnviou(){
        return isSucessoAPI();
    }

    public void setQuantidadeDePedidosParaSerEnviado(int size){
        mQuantidadePeidosEnviados = size;
    }


    public void getIniciarDadosParaEnvio() {
        setSucessoAPI();
    }
}
