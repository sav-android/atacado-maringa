package br.net.sav.presenter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.ReadOnlyFolderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.StoreClosedException;
import javax.mail.internet.InternetAddress;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.ControleAcesso;
import br.net.sav.atacadomaringa.ImportacaoActivity;
import br.net.sav.atacadomaringa.R;
import br.net.sav.atacadomaringa.TransmissaoActivity;
import br.net.sav.dao.GeracaoTextoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Produto;
import br.net.sav.presenter.TransmissaoDadosPresente.IAoTransmitirDadosPresente;
import br.net.sav.util.dao.EmailDAO;
import br.net.sav.util.dao.ProvedorDAO;
import br.net.sav.util.dao.UsuarioEmailDAO;
import br.net.sav.util.modelo.Email;
import br.net.sav.util.modelo.Mail;
import br.net.sav.util.modelo.Provedor;
import br.net.sav.util.modelo.UsuarioEmail;
import br.net.sav.utils.ShearedPreferenceUltils;

import static br.net.sav.atacadomaringa.TransmissaoActivity.COMPACTANDO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.DESCOMPACTANDO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ENVIANDO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ENVIANDO_EMAILS_TEXTUAIS;
import static br.net.sav.atacadomaringa.TransmissaoActivity.GERANDO_DADOS;
import static br.net.sav.atacadomaringa.TransmissaoActivity.IMPORTACAO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.RECEBENDO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.VERIFICANDO_FOTOS_PRODUTO;

public class TransmissaoDadosArquivos {


    private Provedor mProvedor;
    private Parametro mParametro;
    private boolean mExisteArquivosEnvio;
    private GeracaoTextoDAO mGeracaoTextoDAO;
    private boolean mEnviou;
    private Activity ctx;
    private int mQtdeEmailsRecebidos;
    private boolean mSucesso;
    private IAoTransmitirDadosPresente presenterDados;

    public TransmissaoDadosArquivos(Activity ctx,Parametro mParametro,GeracaoTextoDAO mGeracaoTextoDAO, IAoTransmitirDadosPresente presenterDados) {
        this.mProvedor = new ProvedorDAO(ctx.getBaseContext()).get();
        this.mParametro = mParametro;
        this.mGeracaoTextoDAO = mGeracaoTextoDAO;
        this.ctx = ctx;
        this.presenterDados = presenterDados;
    }

    public void getEnviarEmail() {
        if (mExisteArquivosEnvio) { // Enviar
            Mail mail = new Mail(mProvedor.getUsuarioPop(), mProvedor.getSenhaPop());
            mail.set_displayName(mParametro != null ? mParametro.getNome() : " ");
            mail.set_from(mProvedor.getUsuarioPop());
            mail.set_to(new String[]{mProvedor.getEmailCentral()});
            mail.set_subject(String.format("Dados Representante: %04d", mParametro.equals(null) ? 0 : mParametro.getIdVendedor()));
            mail.set_body("Palmtop ->>> Matriz\r\n");
            try {
                mEnviou = mail.send(true, false);
            } catch (Exception e) {
                Log.d("EnviandoEmail", e.getMessage());
                presenterDados.atualizarProgresso("Enviando Email Erro: " + e.getMessage());
            }
        }
    }



    public void getEnviarEmailTexto() {
        List<Email> emails = new EmailDAO(ctx.getBaseContext()).emailsExportar();
        for (Email email : emails) {
            String[] destAux = email.getCC().split(";");
            String[] destinatarios = new String[destAux.length + 1];
            destinatarios[0] = email.getDestinatario();
            for (int i = 0; i < destAux.length; i++) {
                destinatarios[i + 1] = destAux[i];
            }

            Mail mail = new Mail(mProvedor.getUsuarioPop(), mProvedor.getSenhaPop());
            mail.set_from(mProvedor.getUsuarioPop());
            mail.set_to(destinatarios);
            mail.set_subject(email.getAssunto());
            mail.set_body(email.getMensagem());
            try {
                presenterDados.atualizarProgresso("Enviando Email Texto Finalizado");

                mEnviou = mail.send(true, false);

            } catch (Exception e) {
                Log.d("EnviandoEmailTextual", e.getMessage());
                presenterDados.atualizarProgresso("Enviando Email Texto Erro: " + e.getMessage());
            }
        }
    }

    public void getGerarDados() {
        try {
            Utils.apagarArquivosDiretorio(Environment.getExternalStorageDirectory().getPath() + "/sav/envia/");
            Utils.apagarArquivosDiretorio(Environment.getExternalStorageDirectory().getPath() + "/sav/email/envia");
            mGeracaoTextoDAO.gerarTextos();
            presenterDados.atualizarProgresso("Gerando Dados Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Gerando Dados Erro: " + e.getMessage());
        }
    }

    public void getCompactarDados() {
        try {
            Utils.apagarArquivosDiretorio(Environment.getExternalStorageDirectory().getPath() + "/sav/email/envia/");
            String[] arquivos = Utils.arquivosEnvio();
            mExisteArquivosEnvio = arquivos.length > 0;
            if (mExisteArquivosEnvio) {
                Utils.Compactar(arquivos, Environment.getExternalStorageDirectory().getPath() + "/sav/email/envia/envia.zip");
            }
            presenterDados.atualizarProgresso("Compactando Dados Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Compactando Dados Erro: " + e.getMessage());
        }
    }

    public void apagarArquivosPastaRecebe() {
        presenterDados.atualizarProgresso("Apagando Arquivos Pasta Recebe");
        try {
            Utils.apagarArquivosDiretorio(Environment.getExternalStorageDirectory() + "/sav/recebe/");
            presenterDados.atualizarProgresso("Apagando Arquivos Pasta Recebe Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Apagando Arquivos Pasta Recebe Erro: " + e.getMessage());
        }
    }





    public boolean isaSucesso() {
        return mSucesso;
    }

    public void setSucessoArquivoFalso() {
        mSucesso = false;
    }

    public void setEnviou() {
        mSucesso = true;
    }

    public boolean isProvedorNull(){
        return mProvedor == null;
    }

    public void setQtdeEmailRecebidos() {
        mQtdeEmailsRecebidos = 0;
    }

    public int getQntdEmailsRecebidos() {
        return mQtdeEmailsRecebidos;
    }

    public boolean isEnviou() {
        return mEnviou;
    }

    public void setVerificaAtualizacaoFalso() {
        TransmissaoActivity.verificaAtualizacaoDados = false;
    }
    public void criarPastas() {
        Utils.criarDiretorios();
    }

    public void setEnviou_e_ExisteArquicosEnvio_falso() {
        mEnviou = mExisteArquivosEnvio = false;
    }

    public void getAtualizarFotosProdutos() {
        try {
            Parametro parametro = new ParametroDAO(ctx.getBaseContext(), null).get();
            if (!Produto.buscarListaDeFotos().isEmpty() && parametro != null) {
                Produto.atualizaFotos(ctx.getBaseContext(), Produto.buscarListaDeFotos(),
                        ShearedPreferenceUltils.getQuantidade(ctx.getBaseContext()));
            }
            presenterDados.atualizarProgresso("Atualizando Fotos Produtos Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Atualizando Fotos Produtos Erro: " + e.getMessage());
        }
    }



    public void getDescompactarDados(IAoDescompactar descompactar) {
        try {
            File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/sav/email/recebe/");
            if (dir.exists()) {
                File[] files = dir.listFiles();
                File aux;
                int i = 0, j = 0;

                for (i = 0; i < files.length - 1; i++) {
                    try {
                        Long nrArquivo = Long.parseLong(files[i].getName().substring(0, 5).trim());
                        for (j = i + 1; j < files.length; j++) {
                            Long nrProximo = Long.parseLong(files[j].getName().substring(0, 5).trim());
                            if (nrProximo < nrArquivo) {
                                aux = files[i];
                                files[i] = files[j];
                                files[j] = aux;
                            } else
                                continue;
                        }
                        presenterDados.atualizarProgresso("Descompactando Dados Finalizado");
                    } catch (Exception e) {
                        Log.e(ctx.getString(R.string.transmissao_activity), ctx.getString(R.string.arquivo_invalido) + e.getMessage());
                        presenterDados.atualizarProgresso("Descompactando Dados Erro: " + e.getMessage() + " ");
                        continue;
                    }
                }

                //TODO criarInterface para este metodo
                //getMensageHandlerDescompactar(files);
                descompactar.fazer(files);
            }
            presenterDados.atualizarProgresso("Descompactando Dados Finalizado");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(ctx.getString(R.string.transmissaoactivity_descompactardados), "descompactar." + e.getMessage());
            presenterDados.atualizarProgresso("Descompactando Dados Erro: " + e.getMessage());
        }
    }

    public void getReceberEmail() throws MessagingException {
        javax.mail.Message message = null;
        javax.mail.Message[] messages = null;
        Session session = null;
        Store store = null;
        Folder folder = null;
        Object messagecontentObject = null;
        String sender = null;
        String subject = null;
        Multipart multipart = null;
        String contentType = null;

        // String host = "cerpromobile.com.br";
        String host = mProvedor.getUsuarioPop().substring(mProvedor.getUsuarioPop().indexOf("@") + 1, mProvedor.getUsuarioPop().length());
        String user = mProvedor.getUsuarioPop();// .substring(0,
        // provedor.getUsuarioPOP().indexOf("@"));
        String password = mProvedor.getSenhaPop();

        Properties props = new Properties();
        props.put("mail.pop.host", host);
        props.put("mail.debug", "true");
        session = Session.getInstance(props);

        try {
            store = session.getStore("pop3");
            store.connect(host, 110, user, password);

            folder = store.getDefaultFolder();
            folder = folder.getFolder("inbox");
            folder.open(Folder.READ_WRITE);

            messages = folder.getMessages();
            mQtdeEmailsRecebidos = messages.length;

            //Gravar a data da ultima recep��o mesmo se n�o exsitir nenhum email (obrigar conex�o diaria)
            ControleAcesso.salvarDataUltimaRecepcao(ctx);

            for (int messageNumber = 0; messageNumber < messages.length; messageNumber++) {
                message = messages[messageNumber];

                messagecontentObject = message.getContent();

                if (messagecontentObject instanceof Multipart) {
                    sender = ((InternetAddress) message.getFrom()[0]).getPersonal();
                    if (sender == null) {
                        sender = ((InternetAddress) message.getFrom()[0]).getAddress();
                    }

                    subject = message.getSubject();
                    multipart = (Multipart) message.getContent();

                    for (int i = 0; i < multipart.getCount(); i++) {
                        BodyPart bodyPart = multipart.getBodyPart(i);
                        contentType = bodyPart.getContentType();

                        if (!contentType.startsWith("text/plain")) {
                            if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()))
                                continue;

                            if (subject.equals("--MATRIZ-PALMTOP-- SAV")) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                String fileName = String.format("%05d_%s.zip", messageNumber, sdf.format(new Date()));

                                InputStream is = bodyPart.getInputStream();
                                File f = new File(Environment.getExternalStorageDirectory().getPath() + "/sav/email/recebe/" + fileName);
                                FileOutputStream fo = new FileOutputStream(f);
                                byte[] buf = new byte[4096];
                                int bytesRead;

                                while ((bytesRead = is.read(buf)) != -1)
                                    fo.write(buf, 0, bytesRead);

                                fo.close();
                            }
                        } else {
                            FileOutputStream out = null;

                            try {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                Date dataEnvio;
                                String mensagem, aux;
                                UsuarioEmail usuario;

                                sender = ((InternetAddress) message.getFrom()[0]).getAddress();
                                if (sender == null)
                                    sender = ((InternetAddress) message.getFrom()[0]).getPersonal();

                                subject = message.getSubject();
                                dataEnvio = message.getSentDate();
                                mensagem = bodyPart.getContent().toString();
                                usuario = new UsuarioEmailDAO(ctx.getBaseContext()).getByEmail(sender);
                                if (usuario != null) {
                                    out = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/recebe", sdf.format(dataEnvio) + ".msg"));

                                    aux = String.format("ORIGEM: %s\r\n", sender);
                                    out.write(aux.getBytes());

                                    aux = String.format("DATA: %s\r\n", sdf.format(dataEnvio));
                                    out.write(aux.getBytes());

                                    aux = String.format("ASSUNTO: %s\r\n", subject);
                                    out.write(aux.getBytes());

                                    aux = "MENSAGEM:\r\n";
                                    out.write(aux.getBytes());
                                    out.write(mensagem.getBytes());
                                }
                                presenterDados.atualizarProgresso("Recebendo Email Finalizado");
                            } catch (Exception e) {
                                presenterDados.atualizarProgresso("Recebendo Email Erro: " + e.getMessage());
                                e.printStackTrace();
                            } finally {
                                if (out != null) {
                                    out.flush();
                                    out.close();
                                }
                            }
                        }
                    }
                } else {
                    // Ler o e-mail textual e gerar um arquivo com a extens�o
                    // .msg na pasta recebe para a importa��o considerar como
                    // uma mensagem textual e import�-la
                    FileOutputStream out = null;
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                        Date dataEnvio;
                        String mensagem, aux;
                        UsuarioEmail usuario;

                        sender = ((InternetAddress) message.getFrom()[0]).getAddress();
                        if (sender == null)
                            sender = ((InternetAddress) message.getFrom()[0]).getPersonal();

                        subject = message.getSubject();
                        dataEnvio = message.getSentDate();
                        mensagem = message.getContent().toString();
                        usuario = new UsuarioEmailDAO(ctx.getBaseContext()).getByEmail(sender);
                        if (usuario != null) {
                            out = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getPath() + "/sav/recebe", sdf.format(dataEnvio) + ".msg"));

                            aux = String.format("ORIGEM: %s\r\n", sender);
                            out.write(aux.getBytes());

                            aux = String.format("DATA: %s\r\n", sdf.format(dataEnvio));
                            out.write(aux.getBytes());

                            aux = String.format("ASSUNTO: %s\r\n", subject);
                            out.write(aux.getBytes());

                            aux = "MENSAGEM:\r\n";
                            out.write(aux.getBytes());
                            out.write(mensagem.getBytes());
                        }
                        presenterDados.atualizarProgresso("Recebendo Email Finalizado");
                    } catch (Exception e) {
                        e.printStackTrace();
                        presenterDados.atualizarProgresso("Recebendo Email Erro: " + e.getMessage());
                    } finally {
                        if (out != null) {
                            out.flush();
                            out.close();
                        }
                    }
                }

                // Apaga a mensagem da caixa de entrada
                message.setFlag(Flags.Flag.DELETED, true);
            }
            presenterDados.atualizarProgresso("Recebendo Email Finalizado");
        } catch (AuthenticationFailedException e) {
            mSucesso = false;
            Log.d("Transmissao", ctx.getString(R.string.erro_na_autenticacao_login_senha));
            presenterDados.atualizarProgresso(ctx.getString(R.string.erro_na_autenticacao_login_senha)+":"+e.getMessage());
        } catch (FolderClosedException e) {
            mSucesso = false;
            Log.d("Transmissao", "Erro ao ler a caixa de entrada.");
            presenterDados.atualizarProgresso("Erro ao ler a caixa de entrada."+":"+e.getMessage());
        } catch (FolderNotFoundException e) {
            mSucesso = false;
            Log.d("Transmissao", ctx.getString(R.string.erro_caixa_de_entrada_nao_encontrada));
            presenterDados.atualizarProgresso(ctx.getString(R.string.erro_caixa_de_entrada_nao_encontrada)+":"+e.getMessage());
        } catch (ReadOnlyFolderException e) {
            mSucesso = false;
            Log.d("Transmissao", "Erro, folder somente de leitura.");
            presenterDados.atualizarProgresso("Erro, folder somente de leitura."+":"+e.getMessage());
        } catch (StoreClosedException e) {
            mSucesso = false;
            Log.d("Transmissao", "Erro ao acessar mensagem/objeto.");
            presenterDados.atualizarProgresso("Erro ao acessar mensagem/objeto."+":"+e.getMessage());
        } catch (Exception e) {
            mSucesso = false;
            Log.d("Transmissao", ctx.getString(R.string.nao_foi_possivel_processar_a_leitura_dos_dados));
            presenterDados.atualizarProgresso(ctx.getString(R.string.nao_foi_possivel_processar_a_leitura_dos_dados)+":"+e.getMessage());
        } finally {
            if (folder != null) {
                if (folder.isOpen())
                    folder.close(true);
            }
            if (store != null) {
                if (store.isConnected())
                    store.close();
            }
        }
    }

    public void compactarDados() {
        presenterDados.atualizarProgresso("Compactando Dados");
        Message msg = new Message();
        msg.what = COMPACTANDO;
        presenterDados.getHandler().sendMessage(msg);
        getCompactarDados();
    }



    public void descompactarDados() {
        presenterDados.atualizarProgresso("Descompactando Dados");
        getDescompactarDados(new TransmissaoDadosArquivos.IAoDescompactar() {
            @Override
            public void fazer(File[] files) {
                getMensageHandlerDescompactar(files);
            }
        });
    }

    public void getMensageHandlerDescompactar(File[] files) {
        Message msg = new Message();
        msg.what = DESCOMPACTANDO;
        presenterDados.getHandler().sendMessage(msg);

        if (files.length > 0) {
            setVerificarAtualizacaoDados();
            Utils.descompactar(files[0].getPath(), Environment.getExternalStorageDirectory().getPath() + "/sav/recebe/");
            Utils.apagarArquivo(Environment.getExternalStorageDirectory().getPath() + "/sav/email/recebe/" + files[0].getName());
            importarArquivosRecebidos(Environment.getExternalStorageDirectory() + "/sav/recebe/");
        } else {
            presenterDados.getHandler().sendEmptyMessage(0);
            presenterDados.getProgresDialog().dismiss();
        }
    }

    private void importarArquivosRecebidos(String path) {
        if (new File(path).listFiles().length > 0) {
            Intent it = new Intent(ctx.getBaseContext(), ImportacaoActivity.class);
            it.putExtra("path", path);
            ctx.startActivityForResult(it, IMPORTACAO);
        }
    }

    public void enviarEmail() {
        presenterDados.atualizarProgresso("Enviando Email");
        try {
            Message msg = new Message();
            msg.what = ENVIANDO;
            presenterDados.getHandler().sendMessage(msg);
            getEnviarEmail();
            presenterDados.atualizarProgresso("Enviando Email Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Enviando Email Erro: " + e.getMessage());
        }
    }



    /**
     * Este m�todo serve para enviar e-mails textuais que n�o foram enviados no momento da digita��o por falta de conex�o
     */
    public void enviarEmailTexto() {
        presenterDados.atualizarProgresso("Enviando Email Texto");
        Message msg = new Message();
        msg.what = ENVIANDO_EMAILS_TEXTUAIS;
        presenterDados.getHandler().sendMessage(msg);

        try {
            getEnviarEmailTexto();
            presenterDados.atualizarProgresso("Enviando Email Texto Finalizado");
        } catch (Exception e) {
            e.printStackTrace();
            presenterDados.atualizarProgresso("Enviando Email Texto Erro: " + e.getMessage());
        }
    }


    public void receberEmail() throws NoSuchProviderException, MessagingException {
        presenterDados.atualizarProgresso("Recebendo Email");
        Message msg = new Message();
        msg.what = RECEBENDO;
        presenterDados.getHandler().sendMessage(msg);
        getReceberEmail();
    }



    public void gerarDados() {
        presenterDados.atualizarProgresso("Gerando Dados");
        Message msg = new Message();
        msg.what = GERANDO_DADOS;
        presenterDados.getHandler().sendMessage(msg);
        getGerarDados();
    }






    public void AtualizandoFotosProduto() {
        presenterDados.atualizarProgresso("Atualizando Fotos Produtos");
        Message msg = new Message();
        msg.what = VERIFICANDO_FOTOS_PRODUTO;
        presenterDados.getHandler().sendMessage(msg);
        getAtualizarFotosProdutos();
    }
    public void getStartTramissaoArquivos() throws NoSuchProviderException, MessagingException {
        criarPastas();
        apagarArquivosPastaRecebe();
        gerarDados();
        compactarDados();
        enviarEmail();
        enviarEmailTexto();
        // AtualizandoFotosProduto();
        receberEmail();
        descompactarDados();
    }

    public void getStatusViewSucessoOuFalhaArquivos() {
        presenterDados.getTextViewStatus().setTextColor(isaSucesso() ? Color.CYAN : Color.RED);
        presenterDados.getTextViewStatus().setText(isaSucesso()  ? String.format(ctx.getString(R.string.processo_concluido_com_sucesso_emails_recebidos),
                getQntdEmailsRecebidos(),
                quantidadesDePedidosEnviados(isEnviou()))
                : ctx.getString(R.string.falha_no_processo_de_transmissao));
    }

    public void setVerificarAtualizacaoDados() {
        TransmissaoActivity.verificaAtualizacaoDados = true;
    }

    public void getIniciarDadosArquivos() {
        setEnviou_e_ExisteArquicosEnvio_falso();
        setVerificaAtualizacaoFalso();
        setEnviou();
    }

    public int quantidadesDePedidosEnviados(Boolean enviou) {
        return !enviou ? 0 : mGeracaoTextoDAO.getQuantidadePedidosEnviados();
    }

    public interface IAoDescompactar{
        void fazer(File[] files);
    }
}
