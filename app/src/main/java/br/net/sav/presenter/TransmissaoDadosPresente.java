package br.net.sav.presenter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import br.net.sav.AtualizacaoActivity;
import br.net.sav.IntegradorWeb.controle_acesso.VerificaRegistroVendedor;
import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.interfaces.IAoRegistrarVendedor;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.GeracaoTextoDAO;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.service.VerificaAtualizacao;
import br.net.sav.util.Logs;
import br.net.sav.utils.ToastUtils;

import static br.net.sav.AtualizarActivity.MENSAGEM;
import static br.net.sav.atacadomaringa.MenuPrincipal.NOME_ARQUIVO;
import static br.net.sav.atacadomaringa.TransmissaoActivity.ATUALIZAR_LISTA_STATUS;
import static br.net.sav.utils.ShearedPreferenceUltils.aparelho;
import static br.net.sav.utils.ShearedPreferenceUltils.getPreferString;

public class TransmissaoDadosPresente {

    private Activity ctx;
    private IAoTransmitirDadosPresente presenterDados;
    private PedidoDAO dbPedido;
    private Parametro mParametro;
    private VerificaAtualizacao atualizarVersao;
    private boolean existeVersao;
    private TramissaoDadosAPI mDadosDeTransmissao;
    private GeracaoTextoDAO mGeracaoTextoDAO;

    public TransmissaoDadosPresente(Activity ctx, IAoTransmitirDadosPresente presenterDados) {
        this.ctx = ctx;
        this.presenterDados = presenterDados;
    }

    public void getPopularDados() {
        dbPedido = new PedidoDAO(ctx.getBaseContext(), null);
        mParametro = new ParametroDAO(ctx.getBaseContext(), null).get();
        mGeracaoTextoDAO = new GeracaoTextoDAO(ctx.getBaseContext());

        getIniciandoDados();
    }

    private void getIniciandoDados() {
        mDadosDeTransmissao = new TramissaoDadosAPI(ctx, mParametro, mGeracaoTextoDAO, new IAoTransmitirDadosPresente() {
            @Override
            public Handler getHandler() {
                return presenterDados.getHandler();
            }

            @Override
            public ProgressDialog getProgresDialog() {
                return presenterDados.getProgresDialog();
            }

            @Override
            public TextView getTextViewStatus() {
                return presenterDados.getTextViewStatus();
            }

            @Override
            public void atualizarProgresso(String mensagem) {
                atualizaListaStatus(mensagem);
            }
        });
    }

    public void getVerificaAtualizacaoDeVersao() {
        atualizarVersao = new VerificaAtualizacao(ctx.getApplicationContext());
        existeVersao = atualizarVersao.verifificaVersaoAtual();
    }


    public void getStartTransmissao() throws NoSuchProviderException, MessagingException {
        mDadosDeTransmissao.getStartTramissaoAPI();
        marcarEnviados();
        apagarAposPrazo();
    }


    public void getIniciarDados() {
       mDadosDeTransmissao.getIniciarDadosParaEnvio();
    }

    public void setSucessoFalso() {
      //  mDadosDeTransmissao.setSucessoArquivoFalso();
    }

    public void getVerificarVendedorAPI() {
        if (mParametro.isAtivo() && mParametro.getToken() != null && mParametro.getIdVendedorCM() > 0
                && Build.MODEL.trim().equals(getPreferString(ctx.getBaseContext(), aparelho, "").trim())) {

            new VerificaRegistroVendedor()
                    .verifica(mParametro,
                            new IAoRegistrarVendedor() {

                                @Override
                                public void aoRegistrar(Vendedor vendedor) {
                                    getVerificaRetornoRegistroVendedor(vendedor);

                                }

                                @Override
                                public void aoFalhar(String mensagemErro) {

                                }
                            });

        } else if (mParametro.getToken() != null && mParametro.getIdVendedorCM() == 0) {
            new VerificaRegistroVendedor()
                    .verifica(mParametro,
                            new IAoRegistrarVendedor() {

                                @Override
                                public void aoRegistrar(Vendedor vendedor) {
                                    getVerificaRetornoRegistroVendedor(vendedor);

                                }

                                @Override
                                public void aoFalhar(String mensagemErro) {

                                }
                            });

        }
    }

    public void getVerificaRetornoRegistroVendedor(Vendedor vendedor) {
        mParametro.setDataUltimaVerificacaoCM(new Date());
        mParametro.setAtivo(vendedor.isAtivo());
        mParametro.setIdVendedorCM(vendedor.getId());
        new ParametroDAO(ctx.getBaseContext(), null).update(mParametro);

        new VerificaRegistroVendedor()
                .atualizaVendedor(ctx.getBaseContext(),
                        mParametro.getIdVendedorCM().toString(),
                        mParametro, vendedor.isAtivo());
    }

    public void getAtualizarVersao() {
        atualizarVersao.iniciarMsg(ctx, ctx.getContentResolver());
    }

    public boolean isExisteVersao() {
        return existeVersao;
    }


    public boolean isParametroDiferenteNull() {
        return mParametro != null;
    }

    public TramissaoDadosAPI getmDadosDeTransmissao() {
        return mDadosDeTransmissao;
    }


    public boolean getValidarVendedorDesativado() {
        if (mParametro != null) {
            if (!mParametro.isAtivo() && mParametro.getToken() != null && mParametro.getIdVendedorCM() > 0) {
                ToastUtils.mostrarMensagem(ctx.getBaseContext(), ctx.getString(R.string.vendedor_desativado_favor_fazer_uma_trnasicao));
                return true;
            }
            if (mParametro.isAtivo() && mParametro.getToken() != null && mParametro.getIdVendedorCM() > 0
                    && !Build.MODEL.trim().equals(getPreferString(ctx.getBaseContext(), aparelho, "").trim())) {
                ToastUtils.mostrarMensagem(ctx.getBaseContext(), ctx.getString(R.string.licenca_desativada));
                return true;
            }
        }
        return false;
    }

    public List<Pedido> getListaPedidosNaoEnviadoEAbertos() {
        return dbPedido.listaPedidos(dbPedido.NAO_ENVIADOS_E_ABERTOS);
    }

    public void updateVersion() {
        Intent it = new Intent("Atualizacao");
        it.putExtra("nomeApk", NOME_ARQUIVO);
        it.putExtra(AtualizacaoActivity.AUTHORITY, "br.net.sav.atacadomaringa.fileprovider");
        ctx.startActivity(it);
    }

    public void dialogSair() {
        AlertDialog.Builder msg = new AlertDialog.Builder(ctx, (Build.VERSION.SDK_INT >= 28) ? R.style.DialogStyle : 0);
        msg.setMessage(R.string.pedidos_transmitir);
        msg.setTitle(R.string.atencao);
        msg.setIcon(android.R.drawable.ic_dialog_alert);
        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                ctx.finish();
            }
        });

        AlertDialog alert = msg.create();
        alert.show();
    }

    public void apagarAposPrazo() {
        presenterDados.atualizarProgresso("Apagando Apos Prazo");
        Message msg = new Message();
        msg.what = MENSAGEM;
        Bundle b = new Bundle();
        b.putString("mensagem", "Apagando pedidos antigos");
        msg.setData(b);
        presenterDados.getHandler().sendMessage(msg);
        getApagarAposPrazo();
    }
    public void getApagarAposPrazo() {
        try {
            mGeracaoTextoDAO.apagarAposPrazo();
            presenterDados.atualizarProgresso("Apagando Apos Prazo Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Apagando Apos Prazo Erro: " + e.getMessage());
        }
    }
    public void marcarEnviados() {
        presenterDados.atualizarProgresso("Marcando Enviados");
        try {
            if (isEnviouDados()) {
                mGeracaoTextoDAO.marcarEnviados(new Date());
            }
            presenterDados.atualizarProgresso("Marcando Enviados Finalizado");
        } catch (Exception e) {
            presenterDados.atualizarProgresso("Marcando Enviados Erro: " + e.getMessage());
        }
    }

    private boolean isEnviouDados(){
       return mDadosDeTransmissao.isEnviou();
    }



    public void atualizaListaStatus(String texto) {
        Logs.gravarLog(new Date().toString() + " " +  texto + "\n");
        Message msg = new Message();
        msg.what = ATUALIZAR_LISTA_STATUS;
        Bundle b = new Bundle();
        b.putString("mensagem", texto + "\n");
        msg.setData(b);
        presenterDados.getHandler().sendMessage(msg);
    }

    public void getStatusFalhaOuSucessoView(){
        mDadosDeTransmissao.getStatusViewSucessoOuFalhaAPI();
    }

    public interface IAoTransmitirDadosPresente
    {
        Handler getHandler();
        ProgressDialog getProgresDialog();
        TextView getTextViewStatus();
        void atualizarProgresso(String mensagem);
    }

}
