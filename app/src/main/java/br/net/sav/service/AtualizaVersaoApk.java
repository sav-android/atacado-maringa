package br.net.sav.service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import br.net.sav.AtualizacaoActivity;
import br.net.sav.AtualizarActivity;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.util.ManipulacaoDiretorios;
import br.net.sav.util.VersoesSav;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

import static br.net.sav.atacadomaringa.MenuPrincipal.NOME_ARQUIVO;
import static br.net.sav.atacadomaringa.MenuPrincipal.SENHA;
import static br.net.sav.atacadomaringa.MenuPrincipal.USUARIO;

public class AtualizaVersaoApk extends AsyncTask<Void, Void, Integer> {

    static int SERVICO_ATUALIZACA_IMPOSSIVEL = 0;
    static int IMPOSSIVEL_PEGAR_VERSAO = 1;
    static int APP_ATUALIZADO = 2;
    static int IMPOSSIVEL_VERSAO_SERVIDOR = 3;
    static int ERRO_DESCONHECIDO = 4;
    static int FINALIZOU_CORRETO = 5;
    static int NOTIFICATION_CODE = 101;
    NotificationCompat.Builder mBuilder;
    NotificationManager mNotificationManager;
    Context mContext;
    private boolean baixarAtualizacao = false;
    public static final String CONTENT_TYPE_APK = "application/vnd.android.package-archive";

    public AtualizaVersaoApk(Context context, Boolean baixarAtualizacao){
        this.mContext =  context;
        mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(mContext.getString(R.string.app_name))
                .setContentText(mContext.getString(R.string.atualizacao_disponivel));
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        this.baixarAtualizacao = baixarAtualizacao;
    }

    public void baixarAtualizacao(){
        try {
            AtualizacaoEndPointInterface downloadService = getAtualizacaoService();

            String fileUrl = BASE_URL_CERPROMOBILE + "getfile.php?usuario=" + mContext.getString(R.string.config_usuario_site) + "&senha=" + mContext.getString(R.string.config_senha_site);

            Call<ResponseBody> call = downloadService.downloadFile(fileUrl);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Log.i("Conectando", "servidor conectado e arquivo existe");
                        new TarefaDownloadFile(response.body()).execute();
                    } else {
                        Log.i("Falha", "falha ao conectar ao servidor");
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("Erro", t.getMessage());
                }
            });
        }catch (Exception e){

        }
    }
    public interface AtualizacaoEndPointInterface {

        @Streaming
        @GET
        Call<ResponseBody> downloadFile(@Url String fileUrl);

        @GET
        Call<ResponseBody> getVersionCode(@Url String url);
    }

    static final String BASE_URL_CERPROMOBILE = "http://cerpromobile.com.br:8080/sav/android/";
    private AtualizacaoEndPointInterface getAtualizacaoService() {
        try {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL_CERPROMOBILE).build();
            return retrofit.create(AtualizacaoEndPointInterface.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Void... params) {
        try {

            String url = BASE_URL_CERPROMOBILE + "getversion.php?usuario=" + mContext.getString(R.string.config_usuario_site);
            AtualizacaoEndPointInterface atualizacaoService = getAtualizacaoService();

            if (atualizacaoService == null) {
                //throw new Exception("N?o foi poss?vel obter o servi?o de atualiza??es!");
                return SERVICO_ATUALIZACA_IMPOSSIVEL;
            }

            ResponseBody body = atualizacaoService.getVersionCode(url).execute().body();
            if (body == null) {
                //throw new Exception("N?o foi poss?vel recuperar o n?mero da ?ltima vers?o no servidor!");
                return IMPOSSIVEL_PEGAR_VERSAO;
            }

            try {
                InputStream in = body.byteStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String linha = reader.readLine();

                if (linha != null && !linha.isEmpty()) {
                    int newVersion = Integer.parseInt(linha.trim());
                    int currentVersion = VersoesSav.currentVersionCode(mContext);
                    Log.d("Nova Versao ", newVersion+"");
                    Log.d("currentVersion ", currentVersion+"");
                    if (newVersion <= currentVersion) {
                        //throw new Exception("O aplicativo j? est? atualizado!");
                        return APP_ATUALIZADO;
                    }
//                    else {
//                        ParametroDAO parametroDao = new ParametroDAO(mContext, null);
//                        Parametro parametro = parametroDao.get();
//                        if (parametro != null) {
//                            parametro.setVersaoAtualServidor(newVersion);
//                            parametroDao.alterar(parametro);
//                            Log.d("VERSAO", ""+parametro.getVersaoAtualServidor()+", "+parametroDao.buscar().getVersaoAtualServidor());
//                        }
//                    }
                }
            } catch (NumberFormatException e) {
                //throw new Exception("N?o foi poss?vel recuperar o n?mero da ?ltima vers?o no servidor!");
                return IMPOSSIVEL_VERSAO_SERVIDOR;
            } catch (Exception e) {
                return ERRO_DESCONHECIDO;
            }
        } catch (Exception e) {
            return ERRO_DESCONHECIDO;
        }
        return FINALIZOU_CORRETO;
    }

    @Override
    protected void onPostExecute(Integer result) {
        if(result == FINALIZOU_CORRETO){
            if (baixarAtualizacao) {
                baixarAtualizacao();
            }
        }
    }

    class TarefaDownloadFile extends AsyncTask<String, Long, String> {

        ResponseBody body;

        public TarefaDownloadFile(ResponseBody body) {
            this.body = body;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBuilder.build().flags|= Notification.FLAG_NO_CLEAR;
            mBuilder.setProgress(100, 0, false);
            mBuilder.setContentText(mContext.getString(R.string.baixando_atualizacao));
            mNotificationManager.notify(NOTIFICATION_CODE, mBuilder.build());
        }

        @Override
        protected String doInBackground(String... params) {
            if (!writeResponseToDisk(body)) {
                return mContext.getString(R.string.falha_ao_baixar_apk);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            try {
                Long progressoAtual = values[0];
                Long progressoTotal = values[1];

                int percent = 0;
                if (progressoAtual > 0) {
                    percent = (int) ((progressoAtual * 100) / progressoTotal);
                }
                mBuilder.setContentText(mContext.getString(R.string.baixando_atualizacao)+" "+percent+"%");
                mBuilder.setProgress(100, percent, false);
                // Displays the progress bar for the first time.
                mNotificationManager.notify(NOTIFICATION_CODE, mBuilder.build());

            } catch (Exception e) {
                Log.e("Erro TDF", e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            mBuilder.setProgress(0, 0, false);
            if(result == null) {
                mBuilder.setAutoCancel(true);
                //mBuilder.build().flags|= Notification.FLAG_NO_CLEAR;
                mBuilder.build().flags|= Notification.FLAG_ONLY_ALERT_ONCE;
                mBuilder.build().flags|= Notification.FLAG_AUTO_CANCEL;
                mBuilder.setContentText(mContext.getString(R.string.atualizacao_disponivel));
                String caminhoArquivos = getCaminhoDiretorioDownload();
                File arquivo = new File(caminhoArquivos + mContext.getString(R.string.config_nome_aplicativo));
                if (arquivo.exists()) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setDataAndType(Uri.fromFile(arquivo), CONTENT_TYPE_APK);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,
//                            intent, PendingIntent.FLAG_UPDATE_CURRENT);
//                    mBuilder.setContentIntent(contentIntent);

                    if (android.os.Build.VERSION.SDK_INT >= 24){
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri apkUri = FileProvider.getUriForFile(mContext, "br.net.sav.atacadomaringa.fileprovider", arquivo);
                        intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                        intent.setDataAndType(apkUri, CONTENT_TYPE_APK);
                        intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,
                                intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        mBuilder.setContentIntent(contentIntent);
                    }else {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(arquivo), CONTENT_TYPE_APK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0,
                                intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        mBuilder.setContentIntent(contentIntent);
                    }
                }
            }else{
                mBuilder.setContentText(mContext.getString(R.string.erro_atualizacao));
            }
            mNotificationManager.notify(NOTIFICATION_CODE, mBuilder.build());
        }

        private boolean writeResponseToDisk(ResponseBody body) {
            try {
                String destPath = getCaminhoDiretorioDownload();
                File newFile = new File(destPath + mContext.getString(R.string.config_nome_aplicativo));

                InputStream inputStream = null;
                OutputStream outputStream = null;

                try {
                    byte[] fileReader = new byte[4096];

                    long fileSize = body.contentLength();
                    long fileSizeDownloaded = 0;

                    inputStream = body.byteStream();
                    outputStream = new FileOutputStream(newFile);

                    while (true) {
                        int read = inputStream.read(fileReader);

                        if (read == -1) {
                            break;
                        }

                        outputStream.write(fileReader, 0, read);

                        fileSizeDownloaded += read;

                        publishProgress(fileSizeDownloaded, fileSize);
                    }
                    outputStream.close();

                    return true;
                } catch (IOException e) {
                    return false;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                return false;
            } catch (Exception e1) {
                return false;
            }
        }

        private void instalarAtualizacoes() {
            try {

                String caminhoArquivos = getCaminhoDiretorioDownload();
                File dir = new File(caminhoArquivos);
                if (!dir.exists()) {
                    return;
                }

                File arquivo = new File(caminhoArquivos + mContext.getString(R.string.config_nome_aplicativo));
                if (arquivo.exists()) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(arquivo), CONTENT_TYPE_APK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
            } catch (Exception e) {
                Log.d("Erro","instalarAtaulizacao()");
            } finally {
                Log.d("Process ","Processo concluido");
            }
        }

        private String getCaminhoDiretorioDownload() {
            String caminhoArquivos = Environment.getExternalStorageDirectory().getPath() + ManipulacaoDiretorios.DIRETORIO_ATUALIZACOES;
            try {
                if (!ManipulacaoDiretorios.existeDiretorio(caminhoArquivos)) {
                    caminhoArquivos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
                }
            } catch (Exception e) {
                Log.e("Erro ", "getCaminhoDiretorioDownload()."+e.getMessage());
            }
            return caminhoArquivos;
        }
    }

}
