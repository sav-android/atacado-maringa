package br.net.sav.service;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.enumerador.TipoOperacao;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.TipoPedido;

public class BonificacaoHelper {
    public static final int ALTERAR = 1;
    public static final int EXCLUIR = 2;

    private Pedido mPedido;
    private Context mContext;
    private TipoPedido tipoPedido;

    public BonificacaoHelper(Context ctx, Pedido pedido){
        mPedido = pedido;
        mContext = ctx;

        tipoPedido = new TipoPedidoDAO(mContext, null).get(pedido.getIDTPedido());

    };


    public boolean verificarSeEhBonificacao(int excluir_alterar) {

        if (tipoPedido.getTipoOperacao().equals(TipoOperacao.BONIFICACAO.toString())) {

            Long id = buscarVendaComBonificacao().getIDPedido();

            if (ALTERAR == excluir_alterar) {
                mostrarMensagem(mContext.getString(R.string.nao_pode_alterar_bonificacao));
                return  true;
            }

            if (EXCLUIR == excluir_alterar) {
                mostrarMensagem(mContext.getString(R.string.nao_pode_excluir_bonificacao) + "" + id);
                return true;
            }
        }
        return false;
    }

    private void mostrarMensagem(String string){
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();

    }

    public boolean verificarSeExisteBonificacaoVinculado() {
        if(buscarPedidoVinculado().getVinculoPedido() != 0){
            mostrarMensagem(mContext.getString(R.string.Nao_e_posivel_excluir_pedido_bonificacao_vinculado));
            return true;
        }
        return false;
    }

    public void msgExcluida(boolean msg){
        if (msg)
            mostrarMensagem(mContext.getString(R.string.pedido_excluido_com_sucesso));
        else
            mostrarMensagem(mContext.getString(R.string.erro_ao_excluir_o_pedido));
    }

    public void excluirPedidoComBonificacao(){
        boolean msg = false;
        List<Pedido> pedidos = new ArrayList<Pedido>();
        pedidos.add(mPedido);
        pedidos.add(buscarPedidoVinculado());

        if (!pedidos.isEmpty())
            for (Pedido pedido : pedidos){
                msg = new PedidoDAO(mContext, null).excluirPedido(pedido);
            }

        msgExcluida(msg);

    }
    public void excluirBonificacao(){
        boolean msg = false;

        if(new PedidoDAO(mContext, null).excluirPedido(mPedido)){
            Pedido pedido = buscarVendaComBonificacao();

            pedido.setVinculoBonificacao(0);
            pedido.setSaldoBonificar(0);
            new PedidoDAO(mContext, null).update(pedido,null);
            msg = true;
        }


        msgExcluida(msg);

    }

    public Pedido buscarPedidoVinculado(){
        return new PedidoDAO(mContext, null).getPedidoVinculado(mPedido);
    }

    private Pedido buscarVendaComBonificacao(){
        return new PedidoDAO(mContext, null).getBuscarVendaDaBonificacao(mPedido);
    }
}
