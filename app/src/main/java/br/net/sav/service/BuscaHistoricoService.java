package br.net.sav.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.net.sav.IntegradorWeb.dto.Pedido;
import br.net.sav.IntegradorWeb.repository.PedidoRepository;
import br.net.sav.IntegradorWeb.retrofit.service.AoFinalizarRequisicaoListener;
import br.net.sav.IntegradorWeb.services.ServiceHistoricoCM;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.PedidoDAO;
import br.net.sav.modelo.Parametro;
import br.net.sav.util.modelo.DataParameterDTO;

public class BuscaHistoricoService extends ServiceHistoricoCM {


    @Override
    protected DataParameterDTO getDataParameterDTO() {
        Parametro parametro = new ParametroDAO(getBaseContext(), null).get();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, /*parametro.getQtdeDiasGuardarPedido()*/ 35* -1);
        Date dataInicial = cal.getTime();
        Date dataFinal = new Date();

        return new DataParameterDTO(parametro.getTokenApi(),dataInicial,dataFinal);
    }

    @Override
    protected AoFinalizarRequisicaoListener<List<Pedido>> getlistener() {
        return new AoFinalizarRequisicaoListener<List<Pedido>>() {
            @Override
            public void quandoSucesso(List<Pedido> resultado) {
                new PedidoDAO(getBaseContext(), null).insereHistoricoDePedidos(resultado);
            }

            @Override
            public void quandoErro(String erro) {
            }
        };
    }

    @Override
    protected void buscarHistoricoPedido(Object object, AoFinalizarRequisicaoListener<List<Pedido>> listener) {
        new PedidoRepository().buscarTodosPedidos(object, listener);

    }


}
