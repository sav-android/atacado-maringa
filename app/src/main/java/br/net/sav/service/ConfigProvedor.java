package br.net.sav.service;

import android.content.Context;

import br.net.sav.util.modelo.Provedor;

public class ConfigProvedor {
    private static Context mContext;
    public static final String ATACADO_CENTRAL =  "atacadomga_central";
    public static final String ATACADO_USUARIO =  "atacadomga";
    public static final String ATACADO_SENHA =  "atacadomga";
    public static final String ATACADO_TESTE_USUARIO = "";
    private static final String ATACADO_TESTE_SENHA = "";
    private static final String ATACADO_TESTE_CENTRAL = "";

    public ConfigProvedor(Context mContext){
        this.mContext = mContext;
    }


    public static Provedor setProvedor(int numero) {
        Provedor provedor = new Provedor();
        provedor.setUsuarioPop(ATACADO_USUARIO + numero);
        provedor.setSenhaPop(ATACADO_SENHA);
        provedor.setEmailCentral(ATACADO_CENTRAL);

        return provedor;
    }

//    public static Provedor setProvedorTeste() {
//        Provedor provedor = new Provedor();
//        provedor.setUsuarioPop(ATACADO_TESTE_USUARIO);
//        provedor.setSenhaPop(ATACADO_TESTE_SENHA);
//        provedor.setEmailCentral(ATACADO_TESTE_CENTRAL);
//
//        return provedor;
//    }
}
