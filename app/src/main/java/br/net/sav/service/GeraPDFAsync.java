package br.net.sav.service;

import android.os.AsyncTask;

public class GeraPDFAsync extends AsyncTask<Void,Void,String> {
    private ExecultaTask task;

    public GeraPDFAsync(ExecultaTask task){
        this.task = task;
    }

    @Override
    protected String doInBackground(Void... voids) {

        return task.iniciar();
    }

    @Override
    protected void onPostExecute(String s) {
        task.resultado(s);

    }

    public interface ExecultaTask{
        String iniciar();
        void resultado(String nome);
    }

}
