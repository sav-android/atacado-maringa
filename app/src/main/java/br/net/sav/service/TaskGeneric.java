package br.net.sav.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import br.net.sav.atacadomaringa.ItemTabActivity;
import br.net.sav.atacadomaringa.R;

public class TaskGeneric extends AsyncTask<Void, Void, String> {
    private ProgressDialog progress;
    private IObjectIntance objectIntance;
    private Object object;
    private String msg ;
    private Context mActivity;
    public TaskGeneric(Context mActivity, String msg, IObjectIntance objectIntance){
        this.msg = msg;
        this.mActivity = mActivity;
        this.object = objectIntance.addObject();
        this.objectIntance = objectIntance;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        showProgress();
    }
    @Override
    protected String doInBackground(Void... voids) {

        return objectIntance.execute(this.object);
    }

    @Override
    protected void onPostExecute(String s) {
        objectIntance.posExecute(s);

        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    private void showProgress() {
        progress = new ProgressDialog(mActivity);
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.setIcon(mActivity.getResources().getDrawable(R.drawable.icon));
        progress.setTitle(mActivity.getString(R.string.app_name));
        progress.setMessage(msg);
      //  progress.setMessage(mActivity.getString(R.string.por_favor_aguarde_carregando_estruturas_alt));
        if (progress != null && !progress.isShowing()) {
            progress.show();
        }
    }

    public interface IObjectIntance{
        Object addObject();
        String execute(Object object);
        void posExecute(String s);

    }
}
