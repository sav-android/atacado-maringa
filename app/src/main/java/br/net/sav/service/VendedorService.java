package br.net.sav.service;

import android.content.Context;

import br.net.sav.IntegradorWeb.controle_acesso.VerificaRegistroVendedor;
import br.net.sav.IntegradorWeb.dto.Vendedor;
import br.net.sav.IntegradorWeb.interfaces.IAoRegistrarVendedor;

import static br.net.sav.utils.ShearedPreferenceUltils.aparelho;
import static br.net.sav.utils.ShearedPreferenceUltils.setPreferString;

public class VendedorService {

   static public void verificarVendedor(final Context context, Vendedor parametro) {
        if(parametro != null)
            if (parametro.isAtivo() && parametro.getToken() != null && parametro.getId() > 0){
                new VerificaRegistroVendedor()
                        .verifica(parametro,
                                new IAoRegistrarVendedor() {

                                    @Override
                                    public void aoRegistrar(Vendedor vendedor) {
                                        setPreferString(context, vendedor.getAparelho_liberado(),aparelho);
                                    }

                                    @Override
                                    public void aoFalhar(String mensagemErro) {

                                    }
                                });

            }
    }
}
