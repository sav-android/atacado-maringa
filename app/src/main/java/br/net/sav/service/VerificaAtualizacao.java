package br.net.sav.service;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;


import br.net.sav.AtualizacaoActivity;
import br.net.sav.AtualizarActivity;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.MenuPrincipal;
import br.net.sav.atacadomaringa.Principal;
import br.net.sav.atacadomaringa.R;

import static br.net.sav.utils.PermissionUtils.isPermission;
import static br.net.sav.utils.PermissionUtils.setPermissionManageSettings;

public class VerificaAtualizacao extends Activity {
    public static final int NAO_EXISTE_VERSAO 		 = 8 ;
    public static final int VERSAO_IGUAL 			 = 9 ;
    public static final int EXISTE_VERSAO 			 = 10;
    public static final int ATUALIZAR_VERSAO         = 4;
    public static final int REQUEST_CODE_WRITE_SETTINGS = 9995;

    private static final String USUARIO = "atacadomaringa2";
    private static final String SENHA = "43efSyb1";
    private static final String NOME_ARQUIVO = "Atacado Maringa.apk";
    public static Context mContext;


    public VerificaAtualizacao(Context mContext){
        this.mContext = mContext;
    }

    public static int existsNewVersion(String usuario) throws PackageManager.NameNotFoundException {
        int currentVersion = 0 , newVersion = 0, retorno = 0;

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        newVersion = Utils.ultimaVersao(usuario);

        currentVersion = Utils.currentVersionCode(mContext);

        if(newVersion > currentVersion)
            retorno = EXISTE_VERSAO ;
        else if (newVersion == currentVersion)
            retorno = VERSAO_IGUAL ;
        else
            retorno = NAO_EXISTE_VERSAO ;

        return retorno ;
    }

    public void iniciar(Activity activity, ContentResolver resolver){
        Utils.setAutoOrientationEnabled(resolver, false);
        Intent it = new Intent("Atualizar");
        it.addCategory(Principal.CATEGORIA);
        it.putExtra("USUARIO", USUARIO);
        it.putExtra("SENHA", SENHA);
        it.putExtra("NOME_ARQUIVO", NOME_ARQUIVO);
        activity.startActivityForResult(it, ATUALIZAR_VERSAO);
    }


    public void iniciarMsg(final Activity activity, final ContentResolver resolver){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.atencao)
                .setMessage(R.string.vesao_apk_diponivel)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // iniciar(activity, resolver);
                        verificaPermissaoDeAtualizacao(activity, resolver);


                    }
                })
                .setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        activity.finish();
                    }
                })
                .show();
    }

    public static void verificaPermissaoDeAtualizacao(Activity context, final ContentResolver resolver) {
        boolean permission;
        permission = isPermission(context);
        if (permission) {
            Utils.setAutoOrientationEnabled(resolver, false);
            AtualizarActivity.iniciar(context,Principal.CATEGORIA, USUARIO, SENHA, NOME_ARQUIVO,ATUALIZAR_VERSAO);

        } else {
            setPermissionManageSettings(context, REQUEST_CODE_WRITE_SETTINGS);
        }
    }


    public boolean verifificaVersaoAtual() {
        try {
            if (existsNewVersion(USUARIO) == EXISTE_VERSAO) {
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void getRequestIniciarAtualizacao(Activity ctx) {
        if (Settings.System.canWrite(ctx)) {
            Log.d("TAG", "CODE_WRITE_SETTINGS_PERMISSION success");
            Utils.setAutoOrientationEnabled(ctx.getContentResolver(), false);
            AtualizarActivity.iniciar(ctx, Principal.CATEGORIA, USUARIO, MenuPrincipal.SENHA, NOME_ARQUIVO,10);
        }
    }
}
