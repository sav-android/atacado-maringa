package br.net.sav.service;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import br.net.sav.IntegradorWeb.dto.ImportacaoDTO;
import br.net.sav.IntegradorWeb.interfaces.AcaoParaImportacaoDados;
import br.net.sav.IntegradorWeb.repository.ImportacaoRepository;
import br.net.sav.IntegradorWeb.repository.VendedorRepository;

public class VerificaParametro {
    public static void getVerificarParametroVendedorAPI(Context context, String token, final AcaoParaImportacaoDados parametroVendedor, final VendedorRepository.IAoVerificarParametro verificar) {

        new TaskGeneric(context, "verificando dados no central Manager", new TaskGeneric.IObjectIntance() {
            @Override
            public Object addObject() {
                return null;
            }

            @Override
            public String execute(Object object) {
                try {

                    List<ImportacaoDTO> importacaoDTO = new ImportacaoRepository().buscarDadosApiSync("",parametroVendedor.getTabela());

                    if (importacaoDTO != null && !importacaoDTO.isEmpty()) {
                       // parametroVendedor.importarApi(activity, importacaoDTO.getListaLinhaTexto(), null);
                        verificar.encontrou(true);
                    } else {
                        verificar.encontrou(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void posExecute(String s) {

            }
        }).execute();

    }
}
