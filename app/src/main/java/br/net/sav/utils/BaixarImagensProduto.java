package br.net.sav.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.Calendar;

import br.net.sav.atacadomaringa.R;
import br.net.sav.UIHelper;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.ftp.ConexaoFtp;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Produto;

public class BaixarImagensProduto extends ConexaoFtp {

    private static BaixarImagensProduto INSTANCE = null;

    private NotificationCompat.Builder mBuilderNotification;
    private NotificationManager mNotificationManager;

    private int mNotificationId = (int) Calendar.getInstance().getTimeInMillis();

    private BaixarImagensProduto(Context context) {
        super(context);
    }

    public static BaixarImagensProduto getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new BaixarImagensProduto(context);
        }
        return INSTANCE;
    }

    @Override
    protected int getPorta() {
        String sPorta = ConfiguracaoFtpActivityUtils.getPortaServidor(mContext);
        return (!sPorta.isEmpty()) ? Integer.parseInt(sPorta) : 21;
    }

    @Override
    protected String getHost() {
        return ConfiguracaoFtpActivityUtils.getEnderecoServidor(mContext);
    }

    @Override
    protected String getUsuario() {
        return ConfiguracaoFtpActivityUtils.getUsuario(mContext);
    }

    @Override
    protected String getSenha() {
        return ConfiguracaoFtpActivityUtils.getSenha(mContext);
    }

    @Override
    protected String getNomeDiretorioServidor() {
        return ConfiguracaoFtpActivityUtils.getDiretorioFotosProdutos(mContext);
    }

    @Override
    protected String getNomeDiretorioLocal() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED) && !state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)) {
            return Environment.getExternalStorageDirectory().getPath() + "/sav/fotos";
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        String msg = "";
        if (isSomenteTestarConexao()) {
            msg = (result == null) ? mContext.getString(R.string.conexao_bem_sucedida) : result;
            Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
        } else {
            msg = (result == null) ? "Processo concluido com sucesso" : result;
            UIHelper.showNotification(null, mContext, mContext.getString(R.string.sincronizacao_fotos), msg);
        }

        if (!isSomenteTestarConexao() && result == null) {
            ConfiguracaoFtpActivityUtils.setDataUltimoDownloadFotosInMillis(mContext, Calendar.getInstance().getTimeInMillis());
        }

        Parametro parametro = new ParametroDAO(mContext, null).get();

        if (!Produto.buscarListaDeFotos().isEmpty() && parametro != null) {
            Produto.atualizaFotos(mContext, Produto.buscarListaDeFotos(),
                    ShearedPreferenceUltils.getQuantidade(mContext));
        }

        updateNotification(msg, 0, 0, false);

        BaixarImagensProduto.INSTANCE = null;
    }

    @Override
    protected void showProgress() {
        mNotificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilderNotification = new NotificationCompat.Builder(mContext);
        mBuilderNotification.setContentTitle(mTituloProgressDialog);
        mBuilderNotification.setContentText("Aguarde...");
        mBuilderNotification.setSmallIcon(br.net.sav.R.drawable.sync);
        mBuilderNotification.setProgress(0, 0, true);
        mBuilderNotification.setOnlyAlertOnce(true);
//    	mBuilderNotification.setStyle(new NotificationCompat.BigTextStyle().bigText("Aguarde..."));
        mNotificationManager.notify(mNotificationId, mBuilderNotification.build());
    }

    @Override
    protected void onProgressUpdate(String... values) {
        if (values != null && values.length > 0)
            updateNotification(values[0], 0, 0, true);
    }

    @Override
    protected void atualizarProgresso(String fileName, int index, int length) {
        updateNotification("Baixando arquivo " + fileName, length, index + 1, false);
    }

    @Override
    protected void atualizarProgresso(String mensagem) {
        updateNotification(mensagem, 0, 0, true);
    }

    public void executarTesteDeConexao() {
        if (!Status.RUNNING.equals(getStatus())) {
            setSomenteTestarConexao(true);
            setTituloProgressDialog(mContext.getString(R.string.testando_conexao));
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.processo_em_execucao), Toast.LENGTH_SHORT).show();
        }
    }

    public void executarDownloadDasFotos() {
        if (!Status.RUNNING.equals(getStatus())) {
            setTituloProgressDialog("Sincronizando fotos do SAV");
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.sincronizacao_fotos_execucao), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateNotification(String message, int max, int progress, boolean indeterminate) {
        if (mNotificationManager != null && mBuilderNotification != null) {
            mBuilderNotification.setContentText(message);
            mBuilderNotification.setProgress(max, progress, indeterminate);
            mNotificationManager.notify(mNotificationId, mBuilderNotification.build());
        }
    }

}


