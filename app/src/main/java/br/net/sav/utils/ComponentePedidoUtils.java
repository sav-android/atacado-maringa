package br.net.sav.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import java.util.List;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.ClienteListaActivity;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.RestricaoTCTDAO;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.RestricaoTCT;

import static br.net.sav.atacadomaringa.ClienteListaActivity.isSair;

public class ComponentePedidoUtils extends Activity {

    public static boolean confirmaSaida(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(activity.getString(R.string.atencao));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.cancelar_pedidos).setCancelable(false)
                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Utils.apagarObjetoSerializado(activity.getBaseContext());
                        isSair = true;
                        ShearedPreferenceUltils.resetPreferencesSpinner(activity.getBaseContext());
                        activity.setResult(Activity.RESULT_OK);
                        activity.finish();
                    }
                }).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.show();
        return false;
    }

    public static void showDialogLegenda(final Activity activity) {
        Dialog dialog = new Dialog(activity);

        dialog.setContentView(R.layout.dialog_legenda);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setTitle("LEGENDA");

        dialog.show();
    }

    public static boolean isSemRestricaoTCT(Pedido pedido, Context context) {
        List<RestricaoTCT> listaDeRestricoesTCT = new RestricaoTCTDAO(context, null).getAll();
        boolean retorno = true;

        if (listaDeRestricoesTCT != null && listaDeRestricoesTCT.size() > 0) {
            for (int index = 0; index < listaDeRestricoesTCT.size(); index++) {

                //Verifica se esta condi??o de pagamento possui o valor 0 para tipo de pedido (0 = todos os tipos de pedido)
                // ou se o tipo de pedido em quest?o ? igual ao tipo de pedido do atual pedido
                if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == 0
                        || listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {

                    //Verifica se esta restri??o possui o valor 0 para a condi??o de pagamento (0 = todos as condi??es de pagamento)
                    // ou se essa condi??o de pagamento em quest?o ? igual a condi??o de pagamento do pedido atual
                    if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == 0
                            || listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {

                        //Verifica se esta restri??o possui o valor 0 para a forma de pagamento (0 = todos as formas de pagamento)
                        // ou se essa forma de pagamento em quest?o ? igual a forma de pagamento do pedido atual
                        if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == 0
                                || listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDFormaPagamento()) {
                            if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {
                                if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {
                                    Toast.makeText(context, R.string.tipo_pedido_nao_pode_ser_associado_condicao_pagamento, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDTPedido()) {
                                    Toast.makeText(context, R.string.tipo_pedido_nao_pode_ser_associado_forma_pagamento, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == 0
                                        && listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == 0) {
                                    Toast.makeText(context, R.string.tipo_pedido_invalido, Toast.LENGTH_LONG).show();
                                }
                            } else if (listaDeRestricoesTCT.get(index).getCodigoCondicaoPagamento() == pedido.getIDCondicao()) {
                                if (listaDeRestricoesTCT.get(index).getCodigoTipoPedido() == pedido.getIDTPedido()) {
                                    Toast.makeText(context, R.string.condicacao_pagamento_nao_pode_ser_associado_tipo_pedido, Toast.LENGTH_LONG).show();
                                }
                                if (listaDeRestricoesTCT.get(index).getCodigoFormaPagamento() == pedido.getIDFormaPagamento()) {
                                    Toast.makeText(context, R.string.condicacao_pagamento_nao_pode_ser_associado_forma_pagamento, Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, R.string.forma_pagamento_invalida_tct, Toast.LENGTH_LONG).show();
                            }
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
}
