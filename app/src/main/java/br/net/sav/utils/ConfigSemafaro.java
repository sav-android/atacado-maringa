package br.net.sav.utils;

import android.graphics.Color;

import br.net.sav.atacadomaringa.R;
import br.net.sav.modelo.ItemDigitacao;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;

public class ConfigSemafaro {


    public static int getImagemSemaforoItem(ItemDigitacao item) {
        if(item.getRentabilidade() <= 0d) {
            return R.drawable.rentabilidade_vermelho;
        } else if( item.getPercentualRentabilidadeMinima() != 0 && item.getRentabilidade() <= item.getPercentualRentabilidadeMinima()) {
            return R.drawable.rentabilidade_vermelho;
        } else if(item.getPercentualRentabilidadeMedia()!= 0 && item.getRentabilidade() <= item.getPercentualRentabilidadeMedia()) {
            return R.drawable.rentabilidade_laranja;
        } else {
            return R.drawable.rentabilidade_verde;
        }
    }

    public static int getImagemSemaforoPedido(Parametro parametro, Pedido pedido) {
        if(pedido.getRentabilidade() <= 0d) {
            pedido.setPercRentabilidadeLiberado(0);
            return R.drawable.rentabilidade_vermelho;
        } else if( parametro.getPercentualRentabilidadeMinimaDoPedido() != 0 && pedido.getRentabilidade() <= parametro.getPercentualRentabilidadeMinimaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0) {
            return R.drawable.rentabilidade_vermelho;
        } else if(parametro.getPercRentabilidadeMediaDoPedido()!= 0 && pedido.getRentabilidade() <= parametro.getPercRentabilidadeMediaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0) {
            return R.drawable.rentabilidade_laranja;
        } else if(pedido.getRentabilidade() <= pedido.getPercRentabilidadeLiberado()  && pedido.getPercRentabilidadeLiberado() > 0){
            return R.drawable.rentabilidade_vermelho;
        }else if(pedido.getRentabilidade() <= parametro.getPercRentabilidadeMediaDoPedido()  && pedido.getPercRentabilidadeLiberado() > 0){
            return R.drawable.rentabilidade_laranja;
        }else {
            return R.drawable.rentabilidade_verde;
        }
    }

    public static int getTextColorSemafaroItem(ItemDigitacao item) {
        if(item.getRentabilidade() <= 0d) {
            return Color.RED;
        } else if( item.getPercentualRentabilidadeMinima() != 0 && item.getRentabilidade() <= item.getPercentualRentabilidadeMinima()) {
            return Color.RED;
        } else if(item.getPercentualRentabilidadeMedia()!= 0 && item.getRentabilidade() <= item.getPercentualRentabilidadeMedia()) {
            return Color.parseColor("#f48905");
        } else {
            return Color.GREEN;
        }
    }

    public static int getTextColorSemafaroPedido(Parametro parametro, Pedido pedido) {
        if(pedido.getRentabilidade() <= 0d) {
            pedido.setPercRentabilidadeLiberado(0);
            return Color.RED;
        } else if( parametro.getPercentualRentabilidadeMinimaDoPedido() != 0 && pedido.getRentabilidade() <= parametro.getPercentualRentabilidadeMinimaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0) {
            return Color.RED;
        } else if(parametro.getPercRentabilidadeMediaDoPedido()!= 0 && pedido.getRentabilidade() <= parametro.getPercRentabilidadeMediaDoPedido() && pedido.getPercRentabilidadeLiberado() == 0) {
            return Color.parseColor("#f48905");
        } else if(pedido.getRentabilidade() <= pedido.getPercRentabilidadeLiberado()  && pedido.getPercRentabilidadeLiberado() > 0){
            return Color.RED;
        }else if(pedido.getRentabilidade() <= parametro.getPercRentabilidadeMediaDoPedido()  && pedido.getPercRentabilidadeLiberado() > 0){
            return Color.parseColor("#f48905");
        }else {
            return Color.GREEN;
        }
    }
}
