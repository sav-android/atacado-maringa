package br.net.sav.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import br.net.sav.atacadomaringa.R;

public class ConfiguracaoFtpActivityUtils extends Activity {
    public static final String TAG = ConfiguracaoFtpActivityUtils.class.getSimpleName();

    // Chaves de prefer�ncias do arquivo ftp_preferences.xml
    public static final String PREFS_KEY_FREQUENCIA_DOWNLOAD_FOTOS_FTP = "pref_key_frequencia_download_fotos_ftp1";
    public static final String PREFS_KEY_DIRETORIO_FOTOS_PRODUTOS_FTP = "pref_key_diretorio_fotos_produtos_ftp1";
    public static final String PREFS_KEY_DOWNLOAD_SOMENTE_WIFI_FTP = "pref_key_download_somente_wifi_ftp1";
    public static final String PREFS_KEY_ENDERECO_SERVIDOR_FTP = "pref_key_endereco_servidor_ftp1";
    public static final String PREFS_KEY_FAZER_DOWNLOAD_FOTOS = "pref_key_fazer_download_fotos1";
    public static final String PREFS_KEY_PORTA_SERVIDOR_FTP = "pref_key_porta_servidor_ftp1";
    public static final String PREFS_KEY_TESTAR_CONEXAO = "pref_key_testar_conexao1";
    public static final String PREFS_KEY_USUARIO_FTP = "pref_key_usuario_ftp1";
    public static final String PREFS_KEY_SENHA_FTP = "pref_key_senha_ftp1";

    // Chaves de prefer�ncias utilizadas internamente
    public static final String PREFS_KEY_DATA_ULTIMO_DOWNLOAD_FOTOS = "prefs_key_data_ultimo_download_fotos1";

    public static String FREQUENCIA_DOWNLOAD_POR_TRANSMISSAO = "0";
    public static String FREQUENCIA_DOWNLOAD_DIARIO = "1";
    public static String FREQUENCIA_DOWNLOAD_SEMANAL = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new ConfiguracaoFtpActivityUtils.ConfiguracaoFtpPreferenceFragment(this)).commit();
    }

    public static class ConfiguracaoFtpPreferenceFragment extends PreferenceFragment {

        private Context mContext;

        public ConfiguracaoFtpPreferenceFragment(Context context) {
            this.mContext = context;

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.ftp_preferences);

            // Testar conex�o
            Preference btnTestarConexao = (Preference) findPreference(PREFS_KEY_TESTAR_CONEXAO);
            if (btnTestarConexao != null) {
                btnTestarConexao.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        BaixarImagensProduto.getInstance(mContext).executarTesteDeConexao();
                        return true;
                    }
                });
            }

            // Fazer download das fotos
            Preference btnDownloadFotos = (Preference) findPreference(PREFS_KEY_FAZER_DOWNLOAD_FOTOS);
            if (btnDownloadFotos != null) {
                btnDownloadFotos.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        BaixarImagensProduto.getInstance(mContext).executarDownloadDasFotos();
                                showAlert(mContext,mContext.getString(R.string.atencao1),
                                        mContext.getString(R.string.apos_conclusao_download));

                        return true;
                    }
                });
            }
        }

    }

    private static SharedPreferences getPrefs(Context context) {
        SharedPreferences retorno = PreferenceManager.getDefaultSharedPreferences(context);
        return retorno;
    }

    public static String getEnderecoServidor(Context context) {

       // boolean b = preferenceScreen.removePreference(servidorFtp);
        return getPrefs(context).getString(PREFS_KEY_ENDERECO_SERVIDOR_FTP,  context.getString(R.string.pref_ftp_endereco_servidor_default1));
    }

    public static String getPortaServidor(Context context) {
        return getPrefs(context).getString(PREFS_KEY_PORTA_SERVIDOR_FTP, context.getString(R.string.pref_ftp_porta_servidor_default1));
    }

    public static String getUsuario(Context context) {
        return getPrefs(context).getString(PREFS_KEY_USUARIO_FTP,  context.getString(R.string.pref_ftp_usuario_default1));
    }

    public static String getSenha(Context context) {
        return getPrefs(context).getString(PREFS_KEY_SENHA_FTP, context.getString(R.string.pref_ftp_senha_default1));
    }

    public static String getDiretorioFotosProdutos(Context context) {
        return getPrefs(context).getString(PREFS_KEY_DIRETORIO_FOTOS_PRODUTOS_FTP, context.getString(R.string.pref_ftp_diretorio_fotos_default1));
    }

    public static boolean isSomenteDownloadPorWifi(Context context) {
        return getPrefs(context).getBoolean(PREFS_KEY_DOWNLOAD_SOMENTE_WIFI_FTP, false);
    }

    public static String getFrequenciaDownloadFotos(Context context) {
        return getPrefs(context).getString(PREFS_KEY_FREQUENCIA_DOWNLOAD_FOTOS_FTP, FREQUENCIA_DOWNLOAD_POR_TRANSMISSAO);
    }

    public static long getDataUltimoDownloadFotosInMillis(Context context) {
        return getPrefs(context).getLong(PREFS_KEY_DATA_ULTIMO_DOWNLOAD_FOTOS, 0);
    }

    public static void setDataUltimoDownloadFotosInMillis(Context context, long dataUltimoDownload) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(PREFS_KEY_DATA_ULTIMO_DOWNLOAD_FOTOS, dataUltimoDownload);
        editor.commit();
    }

    public static void carregarConfiguracoesArquivoProperties(Context context, String nomeCompletoArquivo) {
        try {
            Properties properties = new Properties();
            InputStream inputStream = new FileInputStream(nomeCompletoArquivo);
            properties.load(inputStream);
            if (properties != null) {
                SharedPreferences prefs = getPrefs(context);
                SharedPreferences.Editor editor = prefs.edit();

                editor.putString(PREFS_KEY_ENDERECO_SERVIDOR_FTP, properties.getProperty("ftp.url", context.getString(R.string.pref_ftp_endereco_servidor_default1)));
                editor.putString(PREFS_KEY_PORTA_SERVIDOR_FTP, properties.getProperty("ftp.porta", context.getString(R.string.pref_ftp_porta_servidor_default1)));
                editor.putString(PREFS_KEY_USUARIO_FTP, properties.getProperty("ftp.usuario", context.getString(R.string.pref_ftp_usuario_default1)));
                editor.putString(PREFS_KEY_DIRETORIO_FOTOS_PRODUTOS_FTP, properties.getProperty("ftp.diretorioFotos", context.getString(R.string.pref_ftp_diretorio_fotos_default1)));
                editor.putString(PREFS_KEY_FREQUENCIA_DOWNLOAD_FOTOS_FTP, FREQUENCIA_DOWNLOAD_POR_TRANSMISSAO);
                editor.putBoolean(PREFS_KEY_DOWNLOAD_SOMENTE_WIFI_FTP,false);


                editor.commit();
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static void showAlert(Context context, String title, String message){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();

    }
}

