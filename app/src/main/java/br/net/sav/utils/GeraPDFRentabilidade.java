package br.net.sav.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;
import br.net.sav.dao.ClienteDAO;
import br.net.sav.dao.DBHelper;
import br.net.sav.dao.ParametroDAO;
import br.net.sav.dao.ProdutoDAO;
import br.net.sav.modelo.Cliente;
import br.net.sav.modelo.Item;
import br.net.sav.modelo.Parametro;
import br.net.sav.modelo.Pedido;
import br.net.sav.modelo.Produto;
import br.net.sav.util.pdf.GeracaoPdfPedido;

import static br.net.sav.utils.MathUtils.arredondar;
import static br.net.sav.utils.MathUtils.getValorBigDecimal;

public class GeraPDFRentabilidade implements Serializable {

    private SQLiteDatabase db;
    private Context ctx;

    public GeraPDFRentabilidade(Context ctx) {
        this.ctx = ctx;
        this.db = null;
    }

    public String gerarPdf(ArrayList<String> linhas, long idPedido) {
        String nomeArquivo = "";
        Font.FontFamily fontfamily = Font.FontFamily.COURIER;
        Document documento = new Document(PageSize.A4.rotate(), 35, 35, 35, 35);
        try {
            String titulo = linhas.get(0);
            nomeArquivo = new GeracaoPdfPedido().gerar(linhas, idPedido);

            FileOutputStream saida = new FileOutputStream(nomeArquivo);
            PdfWriter.getInstance(documento, saida);
            documento.open();

            Font font = new Font(fontfamily);
            font.setStyle(Font.BOLD);
            font.setSize(16);
            titulo += "\n";
            Paragraph p = new Paragraph(titulo, font);
            documento.add(p);

            Font font2 = new Font(fontfamily);

            font2.setSize(10);
            for (String linha : linhas) {
                linha += "\n";
                if (linha.trim().startsWith("VALOR") || linha.startsWith("COD")) {
                    font2.setStyle(Font.BOLD);
                } else {
                    font2.setStyle(Font.NORMAL);
                }
                p = new Paragraph(linha, font2);
                documento.add(p);

            }

            documento.close();
            try {
                saida.close();
            } catch (IOException e) {
                Log.e("erro pdfRodape", e.getMessage());
            }

        } catch (FileNotFoundException e) {
            Log.e("GeradorPdf", e.getMessage());
        } catch (DocumentException e) {
            Log.e("GeradorPdf", e.getMessage());
        }
        return nomeArquivo;

    }


    public ArrayList<String> getLinhasPdfPedido(Pedido pedido, List<Item> itens) {
        ArrayList<String> linhas = new ArrayList<String>();
        SQLiteDatabase dbLocal = (db == null) ? new DBHelper(ctx).getReadableDatabase() : db;
        try {
            Double valorTotal = 0d;

            // Buscar parametro
            Parametro parametro = new ParametroDAO(ctx, dbLocal).get();
            // Buscar cliente
            Cliente cliente = new ClienteDAO(ctx, dbLocal).get(pedido.getIDCliente());

            // Primeira linha � o t�tulo do pdf
            linhas.add("DADOS PARA GERAR SENHA DE RENTABILIDADE!");

            linhas.add("\nDATA PEDIDO...: " + Utils.sdfDataPtBr.format(pedido.getDataPedido()));
            linhas.add(String.format("VENDEDOR......: %d ", parametro.getIdVendedor()));
            linhas.add(String.format("CLIENTE.......: %d ", cliente.getIdCliente()));
            linhas.add(String.format("PEDIDO........: %d ", pedido.getIDPedido()));
            linhas.add(String.format("RENTABILIDADE GERADA DO PEDIDO: %.2f%%", arredondar(getValorBigDecimal(pedido.getRentabilidade())), 2));
            linhas.add(linhaGrid(126));
            linhas.add(String.format("%-8s %-68s %-6s %-10s %10s  %8s %9s", "CODIGO", ctx.getString(R.string.descricao), "QTD", "DESCONTO", "VALOR UNT.", "RENT.", "TOTAL"));
            linhas.add(linhaGrid(126));
            for (Item item : itens) {
                Produto produto = new ProdutoDAO(ctx, db).get(item.getIDProduto());

                String idProduto = String.valueOf(item.getIDProduto()).length() > 8 ? String.valueOf(item.getIDProduto()).substring(0, 8) : String.valueOf(item.getIDProduto());
                String descricaoProduto = produto.getDescricao() != null ? produto.getDescricao().length() > 62 ? produto.getDescricao().substring(0, 62) : produto.getDescricao() : item.getDescricao();

                StringBuilder sb = new StringBuilder();
                sb.append(String.format("%-8s|", idProduto));
                sb.append(String.format("%-68s|", descricaoProduto));
                sb.append(String.format("%6s|", String.valueOf(item.getQuantidade())));
                sb.append(String.format("%10s|", String.format("%.2f", item.getDesconto())));
                sb.append(String.format("%10s|", String.format("%.2f", item.getValorUnitPraticado())));
                sb.append(String.format("%8s|", String.format("%.2f%%", item.getRentabilidade())));
                sb.append(String.format("%10s", String.format("%.2f", item.getTotalLiquido())));

                linhas.add(sb.toString());

                valorTotal += item.getTotalLiquido();
            }
            linhas.add(linhaGrid(126));

            linhas.add(String.format("%127s", "VALOR TOTAL: " + String.format("%.2f", valorTotal)));


        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        } finally {
            if (db == null) {
                //db.close(); singletone();
            }
        }
        return linhas;
    }

    public String linhaGrid(int cont) {
        String valorOriginal = "-";
        for (int i = 0; i < cont; i++) {
            valorOriginal += "-";
        }
        return valorOriginal;
    }
}
