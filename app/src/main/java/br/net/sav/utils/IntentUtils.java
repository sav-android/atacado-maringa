package br.net.sav.utils;

import android.content.Context;
import android.content.Intent;

public class IntentUtils {
    public static void _startActivitySincronizacao(Context ctx, String intent, String category) {
        Intent it = new Intent(intent);
        it.putExtra("SINCRONIZACAO", true);
        it.addCategory(category);
        ctx.startActivity(it);
    }
}
