package br.net.sav.utils;

public class ManipulacaoCpfCnpj {
	public static boolean validaCpf(String value) {
		boolean retorno = false;
		try {
			int d1, d2;
			int digito1, digito2, resto;
			int digitoCPF;
			String nDigResult;
			String strCpf = tirarMascaraCpf(value);
			d1 = d2 = 0;
			digito1 = digito2 = resto = 0;

			for (int nCount = 1; nCount < strCpf.length() - 1; nCount++) {
				digitoCPF = Integer.valueOf(strCpf.substring(nCount - 1, nCount)).intValue();

				d1 = d1 + (11 - nCount) * digitoCPF;
				d2 = d2 + (12 - nCount) * digitoCPF;
			}
			;
			resto = (d1 % 11);

			if (resto < 2)
				digito1 = 0;
			else
				digito1 = 11 - resto;

			d2 += 2 * digito1;

			resto = (d2 % 11);

			if (resto < 2)
				digito2 = 0;
			else
				digito2 = 11 - resto;

			String nDigVerific = strCpf.substring(strCpf.length() - 2, strCpf.length());

			nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

			if (nDigVerific.equals(nDigResult))
				retorno = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	public static boolean validaCnpj(String value) {
		boolean retorno = false;
		try {
			int soma = 0, dig;
			String strCnpj = tirarMascaraCnpj(value);
			String cnpjCalc = strCnpj.substring(0, 12);

			if (strCnpj.length() != 14)
				return false;
			char[] chrCnpj = strCnpj.toCharArray();

			for (int i = 0; i < 4; i++)
				if (chrCnpj[i] - 48 >= 0 && chrCnpj[i] - 48 <= 9)
					soma += (chrCnpj[i] - 48) * (6 - (i + 1));

			for (int i = 0; i < 8; i++)
				if (chrCnpj[i + 4] - 48 >= 0 && chrCnpj[i + 4] - 48 <= 9)
					soma += (chrCnpj[i + 4] - 48) * (10 - (i + 1));
			dig = 11 - (soma % 11);
			cnpjCalc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);

			soma = 0;
			for (int i = 0; i < 5; i++)
				if (chrCnpj[i] - 48 >= 0 && chrCnpj[i] - 48 <= 9)
					soma += (chrCnpj[i] - 48) * (7 - (i + 1));

			for (int i = 0; i < 8; i++)
				if (chrCnpj[i + 5] - 48 >= 0 && chrCnpj[i + 5] - 48 <= 9)
					soma += (chrCnpj[i + 5] - 48) * (10 - (i + 1));
			dig = 11 - (soma % 11);
			cnpjCalc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);
			if (strCnpj.equals(cnpjCalc))
				retorno = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
	public static String formataCpf(String cpf) {
		return cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
	}

	public static String formataCnpj(String cnpj) {
		return cnpj.substring(0, 2) + "." + cnpj.substring(2, 5) + "." + cnpj.substring(5, 8) + "/" + cnpj.substring(8, 12) + "-" + cnpj.substring(12, 14);
	}

    public static String formataCpfOuCnpj(String cpfCnpj) {
        if (cpfCnpj != null) {
            if (cpfCnpj.trim().length() == 11) {
                return formataCpf(cpfCnpj);
            } else if (cpfCnpj.trim().length() == 14) {
                return formataCnpj(cpfCnpj);
            }
        }
        return "";
    }

	public static String tirarMascaraCnpj(String cnpjComMascara) {
		if (cnpjComMascara != null) {
			String retorno = cnpjComMascara.replace(".", "");
			retorno = retorno.replace('/', ' ');
			retorno = retorno.replace('-', ' ');
			retorno = retorno.replaceAll(" ", "");
			return retorno;
		}
		return "";
	}

	public static String tirarMascaraCpf(String cpfComMascara) {
		if (cpfComMascara != null) {
			String retorno = cpfComMascara.replace('.', ' ');
			retorno = retorno.replace('-', ' ');
			retorno = retorno.replaceAll(" ", "");
			return retorno;
		}
		return "";
	}
	
	public static boolean isCnpj(String value) {
		boolean retorno = false;
		if (value != null && !value.trim().isEmpty()) {
			String str = tirarMascaraCnpj(value);
			return str.trim().length() == 14;
		}
		return retorno;
	}
	
	public static boolean isCpf(String value){
		boolean retorno = false;
		if (value != null && !value.trim().isEmpty()){
			String str = tirarMascaraCpf(value);
			return str.trim().length() == 11;
		}
		return retorno;
	}
}
