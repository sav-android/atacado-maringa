package br.net.sav.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ManipulacaoEmail {

	public static boolean isValidarEmail( String emailText ) {
		boolean retorno = false;

		

		if (emailText == null && emailText.trim().isEmpty()) {
			return false;
		}

		String emailPattern = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";

		Pattern pattern = Pattern.compile(emailPattern,
				Pattern.CASE_INSENSITIVE);

		Matcher matcher = pattern.matcher(emailText);

		retorno = matcher.matches();

		return retorno;
	}
	
}
