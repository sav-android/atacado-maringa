package br.net.sav.utils;

import android.util.Log;
import android.widget.EditText;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Random;

public class MathUtils {
    public static final String MATH = "^[0-9]*";
    public static final String EXPRESSAO_REGULAR_NUMEROS = "^[0-9]*$";
    public static final String EXPRESSAO_REGULAR_NUMEROS_DECIMAIS_BR = "^[+-]?(d+|d{1,3}(.d{3})*)(,d*)?$";
    public static final String EXPRESSAO_REGULAR_NUMEROS_DECIMAIS_USA = "\\d{1,3}(,\\d{3})*(\\.\\d{2})?";

    public static final String TAG = Math.class.getClass().getSimpleName();

    public static final BigDecimal DEZ = new BigDecimal("10");
    public static final BigDecimal CEM = new BigDecimal("100");
    public static final BigDecimal MIL = new BigDecimal("1000");
    public static final BigDecimal DEZ_MIL = new BigDecimal("10000");

    public static final int ESCALA_1 = 1;
    public static final int ESCALA_2 = 2;
    public static final int ESCALA_3 = 3;
    public static final int ESCALA_4 = 4;
    public static final int SEM_ESCALA = -1;
    // Moeda
    public static final String FORMATO_MOEDA_QUATRO_CASAS_DECIMAIS = "##,###,###0.0000";

    public strictfp static Double arredondar(double valor, int escala) {
        try{
            return new BigDecimal(String.valueOf(valor)).setScale(escala, BigDecimal.ROUND_HALF_UP).doubleValue();
        } catch (Exception e) {
            Log.e(TAG + "#arredondar()", "Erro ao arredondar: " + e.getMessage());
            return valor;
        }
    }

    public static final DecimalFormat dfMilharQuatroCasas = new DecimalFormat(FORMATO_MOEDA_QUATRO_CASAS_DECIMAIS);

    public static BigDecimal arredondar(BigDecimal pValor) {
        return arredondar(pValor, 2);
    }

    public static BigDecimal arredondar(BigDecimal pValor, int pEscala) {
        try {
            if (pValor != null) {
                return pValor.setScale(pEscala, BigDecimal.ROUND_HALF_UP);
            }
            return pValor;
        } catch (Exception e) {
            mostrarLog(e);
            return pValor;
        }
    }

    public strictfp static Double truncar(double valor, int escala) {
        try {
            double x = java.lang.Math.pow(10, escala);
            double vlrAux = (valor * x);
            long cast = (long) vlrAux;
            double retorno = (cast / x);

            return retorno;
        } catch (Exception e) {
            Log.e(TAG, "Erro ao truncar: " + e.getMessage());
        }
        return valor;
    }

    public strictfp static float truncar(float valor, int escala) {
        try {
            double x = java.lang.Math.pow(10, escala);
            float vlrAux = (float) (valor * x);
            long cast = (long) vlrAux;
            float retorno = (float) (cast / x);

            return retorno;
        } catch (Exception e) {
            Log.e(TAG, "Erro ao truncar: " + e.getMessage());
        }
        return valor;
    }

    public static BigDecimal truncar(BigDecimal valor, int escala) {
        BigDecimal retorno = BigDecimal.ZERO;
        if (valor != null)
            retorno = valor.setScale(escala, RoundingMode.DOWN);
        return retorno;
    }

    public static boolean contemSomenteNumeros(String texto) {
        return texto.matches("^[0-9]*");
    }

    private static BigDecimal getValorSobra(double pValor, double pDivisor) {
        BigDecimal bdValor = new BigDecimal(String.valueOf(pValor));
        BigDecimal bdDivisor = new BigDecimal(String.valueOf(pDivisor));
        BigDecimal bdSobra = bdValor.remainder(bdDivisor);
        return bdSobra;
    }

    public static boolean isMultiplo(double pValor, double pMultiplo) {
        BigDecimal sobra = getValorSobra(pValor, pMultiplo);
        return (sobra.signum() == 0);
    }

    public static int proximoMultiplo(int valor, int multiplo) {
        int valorSobra = valor % multiplo;

        if (valorSobra == 0)
            return valor + multiplo;
        else {
            return (valor - valorSobra) + multiplo;
        }
    }

    public static float proximoMultiplo(float valor, int multiplo) {
        float retorno = 0f;
        BigDecimal bdSobra = getValorSobra(valor, multiplo);

        if (bdSobra.signum() == 0)
            retorno = valor + multiplo;
        else {
            retorno = (valor - bdSobra.floatValue()) + multiplo;
        }
        return truncar(retorno, 2);
    }

    public static int multiploAnterior(int pValor, int pMultiplo) {
        int valorSobra = pValor % pMultiplo;

        if (valorSobra == 0) {
            return pValor - pMultiplo;
        } else {
            return pValor - valorSobra;
        }
    }

    public static double proximoMultiplo(double valor, double multiplo) {
        double retorno = 0d;
        BigDecimal valorSobra = getValorSobra(valor, multiplo);

        if (valorSobra.signum() == 0)
            retorno = valor + multiplo;
        else {
            retorno = (valor - valorSobra.doubleValue()) + multiplo;
        }
        return arredondar(retorno, 2);
    }

    public static float multiploAnterior(float pValor, int pMultiplo) {
        BigDecimal valorSobra = getValorSobra(pValor, pMultiplo);

        if (valorSobra.signum() == 0) {
            return (float) (pValor - pMultiplo);
        } else {
            return (float) (pValor - valorSobra.floatValue());
        }
    }

    public static double multiploAnterior(double pValor, double pMultiplo) {
        double retorno = 0d;
        BigDecimal valorSobra = getValorSobra(pValor, pMultiplo);

        if (valorSobra.signum() == 0) {
            retorno = (pValor - pMultiplo);
        } else {
            retorno = (pValor - valorSobra.doubleValue());
        }
        return arredondar(retorno, 2);
    }

    public static boolean isMath(EditText pEditText) {
        return isMath(getText(pEditText));
    }

    public static boolean isMath(String pTexto) {
        return pTexto.matches(MATH);
    }

    public strictfp static double doubleValue(String sValor) {
        double retorno = 0d;
        if (sValor != null && !sValor.trim().isEmpty()) {
            try {
                retorno = Double.valueOf(sValor.replace(",", "."));
            } catch (NumberFormatException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return retorno;
    }

    public static BigDecimal getValorBigDecimal(Object valor) {
        return getValorBigDecimal(valor, SEM_ESCALA);
    }

    public static BigDecimal getValorBigDecimal(Object valor, int escala) {
        BigDecimal retorno = BigDecimal.ZERO;
        try {
            if (valor != null) {
                if (valor instanceof BigDecimal) {
                    retorno = (BigDecimal) valor;
                } else if (valor instanceof Long
                        || valor.getClass().equals(long.class)) {
                    retorno = new BigDecimal((Long) valor);
                } else if (valor instanceof Double
                        || valor.getClass().equals(double.class)) {
                    retorno = new BigDecimal(String.valueOf(valor).replace(",", "."));
                } else if (valor instanceof Integer
                        || valor.getClass().equals(int.class)) {
                    retorno = new BigDecimal((Integer) valor);
                } else if (valor instanceof Short
                        || valor.getClass().equals(short.class)) {
                    retorno = new BigDecimal((Short) valor);
                } else if (valor instanceof String) {
                    String str = (String) valor;
                    if (str.startsWith(",") || str.startsWith(".")) {
                        str = "0" + str;
                    }
                    if (str.endsWith(",") || str.endsWith(".")) {
                        str = str.substring(0, str.length() - 1);
                    }
                    retorno = new BigDecimal(str);
                }
            }
            if (escala != SEM_ESCALA) {
                retorno = retorno.setScale(escala, BigDecimal.ROUND_HALF_UP);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return retorno;
    }

    public static BigDecimal divide(BigDecimal divisor, BigDecimal dividendo) {
        BigDecimal retorno = BigDecimal.ZERO;
        try {
            if (dividendo == null || BigDecimal.ZERO.equals(dividendo) || divisor == null || BigDecimal.ZERO.equals(divisor)) {
                return retorno;
            }
            Double aux = divisor.doubleValue() / dividendo.doubleValue();
            retorno = new BigDecimal(aux.toString());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return retorno;
    }

    public static int getNumeroAleatorio(int min, int max) {
        Random random = new Random();
        int retorno = random.nextInt((max - min) + 1) + min;
        return retorno;
    }

    public static boolean isFracionado(double valor) {
        int parteInteira = (int) valor;
        double parteFracioada = (valor - parteInteira);
        return parteFracioada > 0d;
    }

    public static double getValorSemParteFracionada(double valor) {
        try {
            int parteInteira = (int) valor;
            return getValorBigDecimal(parteInteira).doubleValue();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return valor;
    }
    public static String getText(EditText pEditText) {
        return getText(pEditText, false);
    }

    /**
     * Busca o texto escrito em um especifico EditText.
     * @param pEditText  EditText que terá seu texto buscado.
     * @return  		 Texto que está escrito no 'pEditText'.
     */
    public static String getText(EditText pEditText, boolean upperCase) {
        if (pEditText != null) {
            String retorno = pEditText.getText().toString().trim();
            if (upperCase)
                retorno = converterParaMaiusculo(retorno);
            return retorno;
        }
        return "";
    }

    public static String converterParaMaiusculo(String pTexto) {
        return pTexto != null ? pTexto.toUpperCase().trim() : "";
    }

    public static void mostrarLog(Exception e) {
        mostrarLog(null, e);
    }

    public static void mostrarLog(String pTag, Exception e) {
        String tag = pTag != null ? pTag : "ErroSav:";
        try {
            Log.e(tag , (e != null && e.getMessage() != null) ? e.getMessage() : "Exception null");
        } catch (Exception ex) {
            Log.e(tag, "erro");
        }
    }
}
