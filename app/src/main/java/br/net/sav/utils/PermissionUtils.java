package br.net.sav.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtils {

    private static int requestCodes;
    private static IPermission iPermission;

    /**
     * Solicita as permiss�es
     * esta lista de permiss�es � s� para o funcionamentos do aplicativo
     * caso queira adicionar outras permiss�es, tera que adicionar em tempo de execu��o
     */
    public static String[] listPermissions(String... permissions) {

        if (permissions != null && permissions.length > 0)
            return permissions;


        return new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };
    }

    /**
     * validando as permiss�es
     *
     * @param activity
     * @param requestCode
     * @param mPermission
     * @return
     */
    public static boolean validate(Activity activity, int requestCode, IPermission mPermission) {
        List<String> list = new ArrayList<>();
        requestCodes = requestCode;
        iPermission = mPermission;

        for (String permission : listPermissions(iPermission.parametroPemission())) {

            if (isCheckSelfPermission(activity, permission)) {
                list.add(permission);
            }
        }

        if (list.isEmpty()) {
            return true;
        }
        // Lista de permiss�es que falta acesso.
        String[] newPermissions = new String[list.size()];
        list.toArray(newPermissions);

        // Solicita permiss�o
        ActivityCompat.requestPermissions(activity, newPermissions, requestCodes);



        return false;
    }

    public static boolean isCheckSelfPermission(Activity activity, String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED;
    }

    /**
     * alerta caso usuario n�o abilite a permiss�o
     *
     * @param activity
     */
    public static void alertAndFinish(final Activity activity) {
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(iPermission.titleApp()).setMessage(iPermission.msgPemissionNaoConsedida());
            // Add the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions(activity, listPermissions(), requestCodes);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }

    /**
     * verifica se as permiss�es foram consedidas
     *
     * @param activity
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public static void onRequestPermissionsResultUtils(Activity activity, int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == requestCodes) {
            if (verifyPermissions(grantResults)) {
                Toast.makeText(activity, iPermission.msgPermissonConsedida(), Toast.LENGTH_SHORT).show();
                return;
            } else {
                alertAndFinish(activity);
            }
        }
    }

    /**
     * retorna true ou false para permiss�o
     *
     * @param grantResults
     * @return
     */
    public static boolean verifyPermissions(int[] grantResults) {
        if (grantResults.length < 1) {
            return false;
        }

        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public interface IPermission {
        String[] parametroPemission();

        String titleApp();

        String msgPermissonConsedida();

        String msgPemissionNaoConsedida();
    }

    static public void permissionSLL(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            try {
                ProviderInstaller.installIfNeeded(activity);
            } catch (GooglePlayServicesRepairableException e) {
                GooglePlayServicesUtil.showErrorNotification(e.getConnectionStatusCode(), activity);
            } catch (GooglePlayServicesNotAvailableException e) {
            }
        }
    }


    public static void setPermissionManageSettings(Activity context, int REQUEST_CODE_WRITE_SETTINGS) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + context.getPackageName()));
            context.startActivityForResult(intent, REQUEST_CODE_WRITE_SETTINGS);
        } else {
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_SETTINGS}, REQUEST_CODE_WRITE_SETTINGS);
        }
    }

    public static boolean isPermission(Activity context) {
        boolean permission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = Settings.System.canWrite(context);
        } else {
            permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED;
        }
        return permission;
    }
}
