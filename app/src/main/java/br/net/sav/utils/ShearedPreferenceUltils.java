package br.net.sav.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.net.sav.dao.CondicaoDAO;
import br.net.sav.dao.FormaPagamentoDAO;
import br.net.sav.dao.TipoPedidoDAO;
import br.net.sav.modelo.FormaPagamento;
import br.net.sav.modelo.Pedido;

import static android.content.Context.MODE_PRIVATE;

import static br.net.sav.ftp.ConexaoFtp.FOTO_ATUALIZADA;

public class ShearedPreferenceUltils {
	public final static String aparelho = "APARELHO";

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");;

	public static SharedPreferences getPrefs(Context context) {
		SharedPreferences retorno = PreferenceManager.getDefaultSharedPreferences(context);
		return retorno;
	}

	public static int getQuantidade(Context context) {
		return getPrefs(context).getInt("quantidade", 0);
	}
	public static int getFiltroItens(Context context) {
		return getPrefs(context).getInt("filtroItens", 0);
	}

	public static void setFiltroItens(Context context, int numero) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt("filtroItens", numero);
		editor.commit();
	}

	public static void setQuantidade(Context context, int quantidade) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt("quantidade", quantidade);
		editor.commit();
	}

	public static void setData(Context context, Date data) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("dateNow", sdf.format(data));
		editor.commit();
	}

	public static String getPreferString(Context context, String referencia, String initialValue) {
		return getPrefs(context).getString(referencia, initialValue);
	}
	public static int getPreferInt(Context context, String referencia, int initialValue) {
		return getPrefs(context).getInt(referencia, initialValue);
	}

	public static void setPreferString(Context context, String valor, String referencia) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(referencia, valor );
		editor.commit();
	}

	public static void setPreferInt(Context context, String referencia, int valor) {
		SharedPreferences prefs = getPrefs(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(referencia, valor );
		editor.commit();
	}

	public static String getData(Context context) {
		return getPrefs(context).getString("dateNow", "");
	}

	public static Boolean getFotoAtualizada(Context context) {
		return getPrefs(context).getBoolean(FOTO_ATUALIZADA, false);
	}

	public static void setFotoAtualizada(Context context, boolean valor) {
		SharedPreferences.Editor editor = getPrefs(context).edit();
		editor.putBoolean(FOTO_ATUALIZADA, valor);
	}

	public static boolean isTipoPedidoValido(SharedPreferences prefs, Pedido pedido, Context context) {
		return prefs.getBoolean("clicouTipoPedido", false) && !(new TipoPedidoDAO(context, null).get(pedido.getIDTPedido()).getDescricao().toUpperCase().contains("MUDAR"));
	}

	public static  boolean isCondicaoPagamentoValido(SharedPreferences prefs, Pedido pedido, Context context) {
		return prefs.getBoolean("clicouCondPagamento", false) && !(new CondicaoDAO(context, null).get(pedido.getIDCondicao()).getDescricao().toUpperCase().contains("MUDAR"));
	}

	public static  boolean isFormaPagamentoValido(SharedPreferences prefs, Pedido pedido, Context context) {
		FormaPagamento is = new FormaPagamentoDAO(context, null).get(pedido.getIDFormaPagamento());
		return prefs.getBoolean("clicouFormaPagamento", false) && !(new FormaPagamentoDAO(context, null).get(pedido.getIDFormaPagamento()).getDescricao().toUpperCase().contains("MUDAR")
				|| pedido.isAlterarPedido() && (new FormaPagamentoDAO(context, null).get(pedido.getIDFormaPagamento()).getDescricao().toUpperCase().contains("MUDAR")));
	}

	public static void sharedPreference(String keyChild, boolean value, Context context) {
		SharedPreferences.Editor editor = context.getSharedPreferences("PERMITIR_GRAVAR", MODE_PRIVATE).edit();
		editor.putBoolean(keyChild, value);
		editor.apply();
	}

	public static void resetPreferencesSpinner(Context ctx) {
		sharedPreference("clicouTipoPedido", false, ctx);
		sharedPreference("clicouCondPagamento", false, ctx);
		sharedPreference("clicouFormaPagamento", false, ctx);
	}
}
