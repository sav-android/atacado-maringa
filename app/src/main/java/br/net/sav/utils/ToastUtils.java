package br.net.sav.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.net.sav.atacadomaringa.R;

import static br.net.sav.Utils.currentVersionName;

public class ToastUtils {

    public static void mostrarMensagem(Context context, String pMensagem) {
        Toast.makeText(context, pMensagem, Toast.LENGTH_LONG).show();
    }

    public static void tamanhoListViewDialogUtils(ListView listView, int quantidade) {
        if (listView != null) {
            ListAdapter listAdapter = listView.getAdapter();
            int numberOfItems = listAdapter.getCount();
            if (numberOfItems > 0) {

                int contador = quantidade;

                View item = listAdapter.getView(0, null, listView);
                if (numberOfItems == 2) {
                    contador = contador * 2;
                } else if (numberOfItems > 2) {
                    contador = contador * 3;
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, contador);
                listView.setLayoutParams(params);
            }
        }
    }

    public static void showDialogSobreAtacado(final Context ctx, String titulo)
            throws PackageManager.NameNotFoundException {
        final String urlApk = "http://cerpromobile.com.br:8080/sav/android/getfile.php?usuario=atacadomaringa2&senha=43efSyb1";
        Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.layout_sobre_atacado);
        dialog.setTitle(titulo);

        final TextView txvVersao = (TextView) dialog
                .findViewById(R.idSobre.txvVersao);
        TextView txtClick =(TextView) dialog.findViewById(R.id.idsApk);

        txtClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in=new Intent(Intent.ACTION_VIEW, Uri.parse(urlApk));
                ctx.startActivity(in);
            }

        });

        txvVersao.setText(ctx.getString(R.string.utils_versao_sav) + currentVersionName(ctx));

        dialog.show();
    }

    public static String getTempoCronometro(long millis) {
        if (millis > 0) {
            long millise = System.currentTimeMillis() - millis;

            int segundos = (int) (millise / 1000);

            int minutos = segundos / 60;

            int horas = minutos / 60;

            segundos = segundos % 60;

            return String.format("%d:%02d:%02d", horas, minutos, segundos);
        }
        return "0";
    }
}
