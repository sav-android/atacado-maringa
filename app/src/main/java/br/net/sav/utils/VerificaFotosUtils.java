package br.net.sav.utils;

import android.content.Context;
import android.util.Log;

import java.util.Calendar;

import br.net.sav.IntegradorWeb.interfaces.IAtualizaMensagemProgress;
import br.net.sav.Utils;
import br.net.sav.atacadomaringa.R;

public class VerificaFotosUtils {

    private final Context context;
    private final IAtualizaMensagemProgress atualizaMsg;

    public VerificaFotosUtils(Context context, IAtualizaMensagemProgress atualizaMsg) {
        this.context = context;
        this.atualizaMsg = atualizaMsg;
    }

    public void verificacaoFotos() {
        
        atualizaMsg.atualizarProgresso(context.getString(R.string.verifica_fotos));
        try {
            long dataUltimoInMillis = ConfiguracaoFtpActivityUtils.getDataUltimoDownloadFotosInMillis(context);
            boolean somenteWifi = ConfiguracaoFtpActivityUtils.isSomenteDownloadPorWifi(context);
            String frequencia = ConfiguracaoFtpActivityUtils.getFrequenciaDownloadFotos(context);

            if (somenteWifi && !Utils.isConectadoViaWifi(context)) {
                return;
            }

            if (dataUltimoInMillis == 0) {
                iniciarSincronizacaoFotos();
            } else {
                Calendar dataAtual = Utils.truncarData(Calendar.getInstance());
                Calendar dataUltimoDownload = Calendar.getInstance();
                dataUltimoDownload.setTimeInMillis(dataUltimoInMillis);
                Utils.truncarData(dataUltimoDownload);

                if (ConfiguracaoFtpActivityUtils.FREQUENCIA_DOWNLOAD_POR_TRANSMISSAO.equals(frequencia))
                    iniciarSincronizacaoFotos();
                else if (ConfiguracaoFtpActivityUtils.FREQUENCIA_DOWNLOAD_DIARIO.equals(frequencia) && dataUltimoDownload.before(dataAtual))
                    iniciarSincronizacaoFotos();
                else if (ConfiguracaoFtpActivityUtils.FREQUENCIA_DOWNLOAD_SEMANAL.equals(frequencia)) {
                    dataUltimoDownload.add(Calendar.DAY_OF_MONTH, 7);
                    if (dataUltimoDownload.compareTo(dataAtual) <= 0)
                        iniciarSincronizacaoFotos();
                }
            }
            atualizaMsg.atualizarProgresso(context.getString(R.string.verificacaofinalizada));
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
            atualizaMsg.atualizarProgresso(String.format(context.getString(R.string.erro_ao_verificar_fotos), e.getMessage()));
            
        }
    }

    private void iniciarSincronizacaoFotos() {
        BaixarImagensProduto.getInstance(context).executarDownloadDasFotos();
    }
}
